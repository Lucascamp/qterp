<div class="modal fade" id="modalSub" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header modal-header-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title" id="myModalLabel"><font color='#ffffff'>Subtrair do item</font></h4>
      </div>
      <div class="modal-body" align="center">

        <div class="alert alert-danger messagesub">
            Impossível deixar o estoque negativo!
        </div>

        Quantidade Disponível:&nbsp;&nbsp;<b><span id='dispsub'></span></b>
        
        {!! Form::hidden('id', '', array('id' => 'almoxarifadosub')) !!}
        {!! Form::text('subtrair', null, ['class' => 'form-control', 'id' => 'subtrair']) !!}

      </div>
      <div class="modal-footer" align="center">
        <button type="button" class="subsave btn btn-danger">Subtrair</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

