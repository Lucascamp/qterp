<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header modal-header-primary">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title" id="myModalLabel"><font color='#ffffff'>Adicionar ao item</font></h4>
      </div>
      <div class="modal-body" align="center">
        
        {!! Form::hidden('id', '', array('id' => 'almoxarifadoadd')) !!}
        {!! Form::text('adicionar', null, ['class' => 'form-control', 'id' => 'adicionar']) !!}

      </div>
      <div class="modal-footer" align="center">
        <button type="button" class="addsave btn btn-primary">Adicionar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

