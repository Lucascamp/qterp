<div class="modal fade" id="modalItemAlmoxarifado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-primary">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title" id="myModalLabel"><font color='#ffffff'>Incluir no almoxarifado</font></h4>
      </div>
      <div class="modal-body" align="center">

      <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Unidade(*)</td>
                <td>
                    {!! Form::select('unidade', ['' => 'Selecione'] + $unidades, null, ['class' => 'form-control chosen-select', 'style' => 'width: 100%', 'id'=>'unidalm']) !!}
                </td>
            </tr>
            <tr>  
                <td class="tblabel">Itens que a unidade não possui(*)</td>
                <td>
                    {!! Form::select('item', ['' => 'Selecione a Unidade'], null , ['class' => 'form-control chosen-select', 'style' => 'width: 100%', 'id'=>'itemaddalm']) !!}
                </td>
            </tr>
            <tr>  
                <td class="tblabel">Quantidade(*)</td>
                <td>
                    {!! Form::text('total', null, ['class' => 'form-control', 'id' => 'totalalm']) !!}
                </td>
            </tr>
        </table>

      </div>
      <div class="modal-footer" align="center">
        <button type="button" class="additmalmsave btn btn-primary">Cadastrar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

