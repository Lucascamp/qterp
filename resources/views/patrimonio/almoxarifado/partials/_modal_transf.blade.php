<div class="modal fade" id="modalTransf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header modal-header-success">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title" id="myModalLabel"><font color='#ffffff'>Transferência</font></h4>
      </div>
      <div class="modal-body" align="center">

      <div class="alert alert-danger messagesub">
        Impossível deixar o estoque negativo!
      </div>

      {!! Form::hidden('id', '', array('id' => 'almoxarifadotransf')) !!}

      <table class="table table-bordered tabela">
            <tr>
                <td colspan="2">
                    Unidade de origem: &nbsp; <span id='origemtransf'></span>
                </td>
            </tr>
            <tr>  
                <td colspan="2">
                    Descrição do item: &nbsp; <span id='itensorigem'></span>
                </td>
            </tr>
            <tr>  
                <td colspan="2">
                    Total disponível: &nbsp; <span id='totalitem'></span>
                </td>
            </tr>
            <tr>  
                <td class="tblabel">Quantidade(*)</td>
                <td>
                    {!! Form::text('transferir', null, ['class' => 'form-control', 'id' => 'transferir']) !!}
                </td>
            </tr>
            <tr>    
                <td class="tblabel">Destino(*)</td>
                <td>
                    {!! Form::select('destino', $unidades, null, ['class' => 'form-control chosen-select', 'style' => 'width: 100%', 'id'=>'itensdestino']) !!}
                </td>
            </tr>
        </table>

      </div>
      <div class="modal-footer" align="center">
        <button type="button" class="transfsave btn btn-success">Transferir</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

