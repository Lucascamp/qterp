<div class="modal fade" id="modalCadastraItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Cadastrar Novo Item</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        {!! Form::open(array('route' => 'item.salvar', 'class'=>'form-inline')) !!}
        {!! Form::hidden('created_by', '1') !!}
        {!! Form::token() !!}
        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Código Protheus(*)</td>
                <td colspan="3">
                    {!! Form::text('codigo', null, array('class'=>'form-control', 'style'=>'width:100%')) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel">Descrição(*)</td>
                <td colspan="3">
                    {!! Form::text('descricao', null, array('class'=>'form-control', 'style'=>'width:100%')) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel">Unidade de Medida(*)</td>
                <td colspan="3">
                    {!! Form::text('unidade_medida', null, array('class'=>'form-control', 'style'=>'width:100%')) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel">Valor em R$(*)</td>
                <td colspan="3">
                    {!! Form::text('valor', null, array('class'=>'form-control', 'style'=>'width:100%')) !!}
                </td>
            </tr>
        </table>
    </div>
    <div class="modal-footer" id="footerm">
        {!! Form::submit('Cadastrar Item', array('class' => 'btn btn-primary')) !!}
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {!! Form::close() !!}
    </div>
</div>
</div>
</div>