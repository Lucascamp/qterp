@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Almoxarifado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li class="active">
                <strong>Almoxarifado</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        @if (in_array(1,$permissoes_opcoes))   
            <div class="title-action">
                <button type="button" data-target="#modalCadastraItem" data-toggle="modal" class="btn btn-primary">Novo Item</button>
                <button type="button" data-target="#modalItemAlmoxarifado" data-toggle="modal" class="btn btn-primary additmBtn">Incluir itens no almoxarifado</button>
            </div>
        @endif
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            @if($unidade)
                <h5>Todos os Itens da unidade <strong>{{ $filter_unidade[$unidade] }}</strong></h5>
            @else
                <h5>Todos os Itens dos almoxarifados</h5>
            @endif    
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">

    <div class="col-sm-12">

    <input type="hidden" id="token" value="{{ csrf_token() }}">

    <table class="table tabela">
        {!! Form::open(array('route' => 'almoxarifado.filtro', 'method' => 'get')) !!}
        <tr>
            <td>
                {!! Form::select('filter_descricao', $filter_descricao, $descricao, array('id'=>'descricaoAlm', 'class'=>'chosen-select')) !!}
            </td>
            @if (in_array(4,$permissoes_opcoes))
                <td>
                    {!! Form::select('filter_unidade', $filter_unidade , $unidade, array('id'=>'unidadeAlm', 'class'=>'chosen-select')) !!}
                </td>
            @endif

            <td>
                <button type="submit" data-tooltip="tooltip" class="btn btn-primary fa fa-filter" style="width: 41px; height:34px;" title="Filtrar tabela"></button>
                
                <button type="button" data-tooltip="tooltip" class="btn btn-info fa fa-eraser" style="width: 41px; height:34px;" id="limparalm" title="Limpar"></button>

                {!! Form::close() !!}
            </td>
        </tr>
    </table>

    <table class="table table-bordered tabela" id='tablealmoxarifado'>
        <thead>
            <tr>
                <th width="10%">Unidade</th>
                <th>Código Protheus</th>
                <th>Descrição</th>
                <th>Unidade de medida</th>
                <th>Preço Médio</th>
                <th>Total</th>
                <th>Valor</th>
                <th>Adicionar</th>
                <th>Consumir</th>
                <th>Transferir</th>
            </tr>
        </thead>

        <tbody>
            @foreach($almoxarifados as $almoxarifado)
            <tr>
                <td>{{ $almoxarifado->unidade->unidade }}</td>
                <td>{{ $almoxarifado->item->codigo }}</td>
                <td>{{ $almoxarifado->item->descricao }}</td>
                <td>{{ $almoxarifado->item->unidade_medida }}</td>
                <td>R$ {{ number_format($almoxarifado->item->valor,'2',',','.') }}</td>
                <td class='td_{{ $almoxarifado->id }}'>
                    <span id='total_{{ $almoxarifado->id }}'>{{ $almoxarifado->total }}</span>
                </td>
                <td class='td_{{ $almoxarifado->id }}'>R$&nbsp; 
                    <span id='valor_{{ $almoxarifado->id }}'>{{ number_format($almoxarifado->total*$almoxarifado->item->valor,'2',',','.') }}</span>
                </td>

                <td>
                     @if (in_array(1,$permissoes_opcoes))
                    <button type="button" data-target="#modalAdd" data-toggle="modal" data-tooltip="tooltip" class="btn btn-primary fa fa-plus addBtn" style="width: 41px; height:34px;" title="Adicionar" data_value="{{ $almoxarifado->id }}"></button>
                    @endif
                </td>
                <td>
                    <button type="button" data-target="#modalSub" data-toggle="modal" data-tooltip="tooltip" class="btn btn-danger fa fa-minus subBtn" style="width: 41px; height:34px;" title="Subtrair" data_value="{{ $almoxarifado->id }}" data_value2="{{ $almoxarifado->total }}"></button>
                </td>
                <td>
                    <button type="button" data-target="#modalTransf" data-toggle="modal" data-tooltip="tooltip" class="btn btn-success fa fa-exchange transfBtn" style="width: 41px; height:34px;" title="Transferência" 
                    data_value="{{ $almoxarifado->id }}"
                    data_value2="{{ $almoxarifado->unidade->unidade }}"
                    data_value3="{{ $almoxarifado->item->descricao }}">
                </button>
            </td>
        </tr>
        @endforeach 
    </tbody> 
</table>
</div>
</div>
</div>
</div>

@include('patrimonio.almoxarifado.partials._modal_cadastro_item')
@include('patrimonio.almoxarifado.partials._modal_add')
@include('patrimonio.almoxarifado.partials._modal_sub')
@include('patrimonio.almoxarifado.partials._modal_transf')
@include('patrimonio.almoxarifado.partials._modal_item_almoxarifado')

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop