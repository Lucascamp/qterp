@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Almoxarifado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li>
                <a href="/almoxarifado">Almoxarifado</a>
            </li>
            <li class="active">
                <strong>Relatório de Consumo</strong>
            </li>
        </ol>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <label class="control-label">Data inicio:</label>
                    <input type="date" id="data_ini" class="form-control">
                </div>
                <div class="col-lg-2">
                    <label class="control-label">Data fim:</label>
                    <input type="date" id="data_fim" class="form-control">
                </div>
                <div class="col-lg-2">                    
                    <label class="control-label">Unidade:</label>
                    {!! Form::select('filtro_unidade', $unidades, null, array('id'=>'unidade_rel_consumo', 'class'=>'chosen-select')) !!}
                </div>
                <div class="col-lg-2">                    
                    <a href="#" class="btn btn-info" id="bt_gerar_rel_consumo"><i class="fa fa-file-text"></i> Gerar</a>
                </div>    
            </div>
        </div>
        <br>

        <div class="row">
            <table class="table table-bordered tabela" id="tabelaConsumo">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Codigo Protheus</th>
                        <th>Quantidade Consumida</th>
                        <th>Valor Médio</th>
                        <th>Unidade</th>
                    </tr>
                </thead>
                <tbody id="bodyconsumo">

                </tbody>
            </table>
        </div>
    </div>        
    <br><br><br>
</div>


<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop