@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li>
                <a href="/almoxarifado">Almoxarifado</a>
            </li>
            <li class="active">
                <strong>Histórico</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">

            <div class="btn-group">
             {{--  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div> --}}
           {{--  <ul class="dropdown-menu">
                <li><a href="#" id="btboletim">Imprimir BM</a></li>
            </ul> --}}
        </div>
    </div>
</div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Selecione unidade para exibir o histórico</h5>
            </div>
            <div class="ibox-content">
                <input type="hidden" id="token" value="{{ csrf_token() }}">

                <div class="col-sm-12">

                <table class="table">
                    <tr>
                        <td class="tblabel">
                            {!! Form::select('unidhisttalm', $unidades, null ,array('id' => 'unidhisttalm', 'class'=>'form-control chosen-select')) !!}
                        </td>

                        <td class="tblabel">
                            {!! Form::select('itemhisttalm', ['' => 'Selecione Item'], null ,array('id' => 'itemhisttalm', 'class'=>'form-control chosen-select')) !!}
                        </td>

                        <td class="tblabel">
                           <div class="pull-right">
                                <a class="btn btn-primary" id="bthistalm">Carregar Histórico</a> 
                            </div>
                        </td>
                    </tr>
                </table>

                </div>

                <div class="ibox-content">
                    <table class="table table-bordered tabela">
                        <thead> 
                         <tr>
                            <th>Data</th>
                            <th>Unidade de destino</th>
                            <th>Item</th>
                            <th>Quantidade</th>
                            <th>Operação</th>    
                            <th>Total</th>
                        </tr>
                    </thead>    
                    <tbody id="tbhistalm">   

                    </tbody>
                </table>    
            </div>
        </div>
    </div>
</div>
</br>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop
