@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2>Calibração</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li class="active">
                <strong>Equipamentos Calibráveis</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>Equipamentos Calibráveis</h5>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>

    <input type="hidden" id="token" value="{{ csrf_token() }}">

    <div class="ibox-content">
        <table class="table tabela">
            {!! Form::open(array('route' => 'calibracao.filtro', 'method' => 'get')) !!}
            <tr>
                <td>
                    {!! Form::select('filter_descricao', $filter_descricao, $descricao, array('id'=>'descricao', 'class'=>'chosen-select')) !!}
                </td>

                <td>
                    {!! Form::select('filter_patrimonio', $filter_patrimonio , $patrimonio, array('id'=>'patrimonio', 'class'=>'chosen-select')) !!}
                </td>
                <td>
                    {!! Form::select('filter_serial', $filter_serial , $serial, array('id'=>'serial', 'class'=>'chosen-select')) !!}
                </td>

                <td>
                    {!! Form::select('filter_vencimento', $filter_vencimento , $vencimento, array('id'=>'vencimento', 'class'=>'chosen-select')) !!}
                </td>
                <td>
                    {!! Form::select('filter_local', $filter_local , $local_equip, array('id'=>'localizacao', 'class'=>'chosen-select')) !!} 
                </td>
                <td>
                    <button type="submit" data-tooltip="tooltip" class="btn btn-primary fa fa-filter" style="width: 41px; height:34px;" title="Filtrar tabela"></button>
                
                    <button type="button" data-tooltip="tooltip" class="btn btn-info fa fa-eraser" style="width: 41px; height:34px;" id="limpar" title="Limpar"></button>

                    {!! Form::close() !!}
                </td>
            </tr>
        </table>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <div class="clear"><br></div>

        {{$equipamentos->count()}} itens encontrados

        <table class="table table-bordered tabela">
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th>Aplicação</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Serial</th>
                    <th>Patrimônio</th>
                    <th>Descrição do uso</th>
                    <th>Frequência</th>
                    <th>Localização</th>
                    <th>Laboratório</th>
                    <th>Certificado</th>
                    <th>Data Calibração</th>
                    <th>Arquivo Certificado</th>
                    <th>Primeiro Uso</th>
                    <th>Vencimento</th>
                    <th colspan="3">Ações</th>
                </tr>
            </thead>

            <tbody>
            @if (isset($equipamentos))
                @foreach ($equipamentos as $equipamento)
                <tr>
                <?php 
                    $class = 'null';

                    if(isset($latestCalibracao[$equipamento->id]))
                    {
                        $data_calibracao = App\Calibracao::toCarbon($latestCalibracao[$equipamento->id]->data_calibracao); 

                        if(isset($latestCalibracao[$equipamento->id]->primeiro_uso))
                        {
                            $primeiro_uso = App\Calibracao::toCarbon($latestCalibracao[$equipamento->id]->primeiro_uso);
                            $vencimento = App\Calibracao::toCarbon($latestCalibracao[$equipamento->id]->vencimento);
                            $class = App\Calibracao::getTDClass($vencimento);
                        }
                    }
                ?>
                    <td>{{{ $equipamento->descricao }}}</td>
                    <td>{{{ $equipamento->aplicacao }}}</td>
                    <td>{{{ $equipamento->marca }}}</td>
                    <td>{{{ $equipamento->modelo }}}</td>
                    <td>{{{ $equipamento->serial }}}</td>
                    <td>{{{ $equipamento->patrimonio }}}</td>
                    <td>{{{ $equipamento->descricao_uso }}}</td>
                    <td>{{{ $equipamento->frequencia }}}</td>

                    @if(isset($localizacao[$equipamento->id]))
                        <td>{{{ $centro_custo[$localizacao[$equipamento->id]->centrocusto_id] }}}</td>
                    @else
                        <td>-</td>
                    @endif 

                @if(isset($latestCalibracao[$equipamento->id]))
                    <td>{{{ $latestCalibracao[$equipamento->id]->laboratorio }}}</td>
                    <td>{{{ $latestCalibracao[$equipamento->id]->certificado }}}</td>
                    <td>{{{ $data_calibracao->format('d/m/Y') }}}</td>

                    
                    @if ($_SERVER['HTTP_HOST'] == "10.155.64.9")
                        <td><a href="http://10.155.64.9/{{$latestCalibracao[$equipamento->id]->filepath}}">{{ substr($latestCalibracao[$equipamento->id]->filepath, 20) }}</a></td>
                    @else
                        <td><a href="http://201.48.246.252/{{$latestCalibracao[$equipamento->id]->filepath}}">{{ substr($latestCalibracao[$equipamento->id]->filepath, 20) }}</a></td>
                    @endif
                    

                        @if(!is_numeric($equipamento->frequencia))

                            <td>
                            @if(!isset($latestCalibracao[$equipamento->id]->primeiro_uso))
                            
                                <button type="button" data-target="#modaluso" data-toggle="modal" data-tooltip="tooltip" class="btn btn-warning fa fa-wrench usoBtn" style="width: 41px; height:34px;" title="Primeiro Uso"
                                    data_value="{{ $latestCalibracao[$equipamento->id]->id }}" data_value2="{{ $equipamento->id }}" ></button>
                            @else
                                {{{ $primeiro_uso->format('d/m/Y') }}}
                            @endif
                            </td>
                                
                            <td>Enquanto Integro</td>

                        @elseif(isset($latestCalibracao[$equipamento->id]->primeiro_uso))
                           <td>{{{ $primeiro_uso->format('d/m/Y') }}}</td>
                            <td class='{{ $class }}'>{{{ $vencimento->format('d/m/Y') }}}</td>

                        @else
                            @if(isset($latestCalibracao[$equipamento->id]))
                            <td>
                            
                                <button type="button" data-target="#modaluso" data-toggle="modal" data-tooltip="tooltip" class="btn btn-warning fa fa-wrench usoBtn" style="width: 41px; height:34px;" title="Primeiro Uso"
                                data_value="{{ $latestCalibracao[$equipamento->id]->id }}" data_value2="{{ $equipamento->id }}" ></button>
                           
                            </td>

                        <td>-</td>
                        @endif
                    @endif  

                @else
                    <td colspan="6"> Não a calibragem criada para este equipamento </td>    
                @endif     

                <td>
                    <button type="button" data-target="#modalHistoricoCalibracao" data-toggle="modal" data-tooltip="tooltip" class="btn btn-primary fa fa-exchange historicoCalibBtn" style="width: 41px; height:34px;" title="Exibir Histórico"
                        data_value="{{ $equipamento->id }}"></button>
                </td>  

                <td> 
                    <a href="{{ route('calibracao.cadastrar', array($equipamento->id)) }}" data-toggle="tooltip" title="Criar Calibração" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-success">
                        <i class="fa fa-plus"></i></a>
                </td>   

                @if($class != 'red')
                    <td>    
                        @if(isset($latestCalibracao[$equipamento->id]))

                            <a href="{{ route('calibracao.editar', array($latestCalibracao[$equipamento->id]->id)) }}" data-toggle="tooltip" title="Editar" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-info"><i class="fa fa-pencil"></i></a>

                        @else
                        -    
                        @endif  
                    </td>    
                @else
                    @if($equipamento->frequencia == 'UNICA') 
                        <td>-</td>   
                    @else
                        <td><font color="red">Calibração vencida!</font></td>  
                    @endif         
                @endif  
                   
                </tr>
                @endforeach
                @else
                <tr> 
                    <td colspan="20" class="center">
                        Nenhum equipamento encontrado.
                    </td>
                </tr>
                @endif    
            </tbody> 
        </table>
    </div>
</div>
</div>

@include('patrimonio.calibracao.partials._modal_historico')

@include('patrimonio.calibracao.partials._modal_uso')

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop