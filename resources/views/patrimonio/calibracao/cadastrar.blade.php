@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

{!! HTML::style('css/parecer.css') !!}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Calibração</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li>
                <a href="/calibracao">Calibração</a>
            </li>
            <li class="active">
                <strong>Cadastrar</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Calibração<small>(*)campos obrigatorios</small></h5>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('route' => 'calibracao.salvar', 'class'=>'form-inline', 'files' => true)) !!}
                    
                    {!! Form::hidden('created_by', '1') !!}
                    {!! Form::hidden('calibracao_id', '1') !!}
                    <input type="hidden" id="token" value="{{ csrf_token() }}">

                <table class="table table-responsive table-rounded zebra2">
                <tr>
                    <th>
                        Equipamento
                    </th>

                    <th>
                        Patrimônio
                    </th>

                    <th>
                        Serial
                    </th>

                    <th>
                        Marca
                    </th>

                    <th>
                        Modelo
                    </th>

                    <th>
                        Frenquência
                    </th>
                </tr>
                <tr>
                    <td>
                        {!! Form::label('descricao', $equipamento->descricao) !!}
                    </td>

                    <td>
                        {!! Form::label('patrimonio', $equipamento->patrimonio) !!}
                    </td>

                    <td>
                        {!! Form::label('serial', $equipamento->serial) !!}
                    </td>

                    <td>
                        {!! Form::label('Marca', $equipamento->marca) !!}
                    </td>

                    <td>
                        {!! Form::label('Modelo', $equipamento->modelo) !!}
                    </td>

                    <td>
                        {!! Form::label('Frequencia', $equipamento->frequencia.' meses') !!}
                    </td>
                </tr>
                </table>

                <hr>

                <?php 
                            // if(isset($calibracao))
                            // {
                            //     $data_calibracao = Calibracao::toCarbon($calibracao->data_calibracao);
                            //     $primeiro_uso = $calibracao->primeiro_uso;
                            // }
                ?>

                {!! Form::hidden('equipamento_id', $equipamento->id) !!}    
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo" colspan="8">Calibração</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Laboratório(*)</td>
                        <td>
                            {!! Form::text('laboratorio', null, array('class'=>'form-control')) !!}
                        </td>

                        <td class="tblabel">Certificado(*)</td>
                        <td>
                            {!! Form::text('certificado', null, array('class'=>'form-control')) !!}
                        </td>
                    </tr>
                    <tr>    
                        <td class="tblabel">Data Calibração(*)</td>
                        <td>
                            <input type="date" name="data_calibracao" class="form-control" id="data" value="isset($data_calibracao) ? ($data_calibracao->format('d/m/Y')) : null">
                        </td>

                        <td class="tblabel">Data Primeiro Uso(*)</td>
                        <td>
                            <input type="date" name="primeiro_uso" class="form-control" id="data" value="isset($primeiro_uso) ? ($primeiro_uso->format('d/m/Y')) : null">
                        </td>
                    </tr>
                    <tr> 
                        <td class="tblabel">Arquivo</td>
                        <td>
                            {!! Form::file('arquivo_calibracao', ['id'=>'arquivo_calibracao']) !!}
                        </td>
                    </tr>
                    <tr>    
                        <td class="tblabel">Comentário</td>
                        <td colspan="7">
                            {!! Form::textarea('comentario', null, array('style' => 'width: 100%', 'rows' => '3')) !!}
                        </td>
                    </tr>

                    <tr>    
                        <td class="tblabel">Parecer</td>
                        <td>
                           <div class="parecer">
                            <input type="checkbox" name="parecer" class="parecer-checkbox onoff" id="parecer" value="1" checked> 
                                <label class="parecer-label" for="parecer">
                                    <span class="parecer-inner"></span>
                                    <span class="parecer-switch"></span>
                                </label>
                            </div>
                        </td>
                        
                        <td class="tblabel">Responsável</td>
                        <td>
                            {!! isset($calibracao->created_by) ? ($calibracao->created_by) : Auth::user()->nome !!}
                        </td>
                    </tr>
                    
                </table>

                    <div class="pull-right">
                    {!! Form::submit('Cadastrar calibração', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}

                </div>
                <br><br>
                @if ($errors->any())

                <br><br>
                    <div class="alert-group">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>    
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop
