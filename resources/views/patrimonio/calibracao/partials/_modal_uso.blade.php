<div class="modal fade" id="modaluso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header modal-header-warning">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title" id="myModalLabel"><font color='#ffffff'>Adicionar primeiro uso</font></h4>
      </div>
      <div class="modal-body" align="center">
        
        {!! Form::open(array('route' => 'calibracao.updateUso', 'class'=>'form-inline')) !!}

        {!! Form::hidden('cod_calibracao', '', array('id' => 'cod_calibracao')) !!}
        {!! Form::hidden('cod_equipamento', '', array('id' => 'cod_equipamento')) !!}

        {!! Form::label('primeiro_uso', 'Primeiro uso:') !!}
        <input type="date" name="primeiro_uso" class="form-control" id="data">

      </div>
      <div class="modal-footer" align="center">
        {!! Form::submit('Cadastrar Primeiro Uso', array('class' => 'btn btn-warning')) !!}
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

