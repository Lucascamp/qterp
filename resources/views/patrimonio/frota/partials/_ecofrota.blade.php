<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Cartão Ecofrotas</td>
    </tr>
    <tr>
        <td class="tblabel">Número do cartão(*)</td>
        <td>
            {!! Form::text('numero', isset($carro->ecofrota->numero) ? $carro->ecofrota->numero : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Tipo(*)</td>
        <td>
            {!! Form::text('tipo', isset($carro->ecofrota->tipo) ? $carro->ecofrota->tipo : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Recarga(*)</td>
        <td>
            {!! Form::text('recarga', isset($carro->ecofrota->recarga) ? $carro->ecofrota->recarga : null, array('class'=>'form-control')) !!}
        </td>
    </tr>
</table>