<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Dados do veiculo</td>
    </tr>
    <tr>
        <td class="tblabel">Placa(*)</td>
        <td>
        {!! Form::text('placa', isset($carro->placa) ? $carro->placa : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Marca(*)</td>
        <td>
        {!! Form::text('marca', isset($carro->marca) ? $carro->marca : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Modelo(*)</td>
        <td>
        {!! Form::text('modelo', isset($carro->modelo) ? $carro->modelo : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Ano(*)</td>
        <td>
        {!! Form::text('ano', isset($carro->ano) ? $carro->ano : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Cor(*)</td>
        <td>
        {!! Form::text('cor', isset($carro->cor) ? $carro->cor : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Combustível(*)</td>
        <td>
        {!! Form::select('combustivel', ['' => 'Selecione',
                                         'Etanol' => 'Etanol',
                                         'Gasolina' => 'Gasolina',
                                         'Etanol/Gasolina' => 'Etanol/Gasolina',
                                         'Diesel' => 'Diesel'],
                        isset($carro->combustivel) ? $carro->combustivel : null, array('class'=>'form-control chosen-select')) !!}
        </td>

        <td class="tblabel">Kilometragem(*)</td>
        <td>
        {!! Form::text('km', isset($carro->km) ? $carro->km : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Chassi(*)</td>
        <td>
        {!! Form::text('chassi', isset($carro->chassi) ? $carro->chassi : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Renavan(*)</td>
        <td>
        {!! Form::text('renavan', isset($carro->renavan) ? $carro->renavan : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Responsável(*)</td>
        <td>
        {!! Form::text('responsavel', isset($carro->responsavel) ? $carro->responsavel : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Proprietário(*)</td>
        <td>
        {!! Form::text('proprietario', isset($carro->proprietario) ? $carro->proprietario : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Rastreador(*)</td>
        <td>
            <div class="simnaoswitch">
                <input type="checkbox" name="rastreador" class="simnaoswitch-checkbox onoff" id="rastreador" value="1"
                <?php if (isset($carro->rastreador) and $carro->rastreador == 1){?> checked="checked" <?php } ?> > 
                <label class="simnaoswitch-label" for="rastreador">
                    <span class="simnaoswitch-inner"></span>
                    <span class="simnaoswitch-switch"></span>
                </label>
            </div>
        </td>
    </tr>

    <tr>
        <td class="tblabel" colspan="2">Veículo comprado ou alugado?</td>
        <td colspan="6">
            <div class="alugadoswitch">
                <input type="checkbox" name="alugado" class="alugadoswitch-checkbox onoff" id="alugado" value="1"
                <?php if (isset($carro->alugado) and $carro->alugado == 1){?> checked="checked" <?php } ?> > 
                <label class="alugadoswitch-label" for="alugado">
                    <span class="alugadoswitch-inner"></span>
                    <span class="alugadoswitch-switch"></span>
                </label>
            </div>
        </td>
    </tr>

    <tr class='tralugado'>
        <td class="tblabel">Data de Aluguel(*)</td>
        <td>
        @if(isset($carro->data_aluguel))
            <input type="date" id='data_aluguel' name="data_aluguel" class="form-control" value="{{ $carro->data_aluguel->format('Y-m-d') }}">
        @else    
            <input type="date" id='data_aluguel' name="data_aluguel" class="form-control">
        @endif    
        </td>

        <td class="tblabel">Prazo Contratual(*)</td>
        <td>
        {!! Form::text('prazo_contrato', isset($carro->prazo_contrato) ? $carro->prazo_contrato : null, array('class'=>'form-control', 'id'=>'prazo_contrato')) !!}
        </td>

        <td class="tblabel">Termino Contrato(*)</td>
        <td>
        @if(isset($carro->termino_contrato))
            <input type="date" id='termino_contrato' name="termino_contrato" class="form-control" value="{{ $carro->termino_contrato->format('Y-m-d') }}">
        @else    
            <input type="date" id='termino_contrato' name="termino_contrato" class="form-control">
        @endif    
        </td>

        <td class="tblabel">Valor Locação(*)</td>
        <td>
        {!! Form::text('valor_locacao', isset($carro->valor_locacao) ? $carro->valor_locacao : null, array('class'=>'form-control', 'id'=>'valor_locacao')) !!}
        </td>

    </tr>
    <tr class='tralugado'>

        <td class="tblabel">Multa Recisão(*)</td>
        <td>
        {!! Form::text('multa_recisao', isset($carro->multa_recisao) ? $carro->multa_recisao : null, array('class'=>'form-control', 'id'=>'multa_recisao')) !!}
        </td>

        <td class="tblabel">Data Reajuste(*)</td>
        <td>
        @if(isset($carro->data_reajuste))
            <input type="date" id='data_reajuste' name="data_reajuste" class="form-control" value="{{ $carro->data_reajuste->format('Y-m-d') }}">
        @else    
            <input type="date" id='data_reajuste' name="data_reajuste" class="form-control">
        @endif    
        </td>

        <td class="tblabel">Tipo Reajuste(*)</td>
        <td>
        {!! Form::text('tipo_reajuste', isset($carro->tipo_reajuste) ? $carro->tipo_reajuste : null, array('class'=>'form-control', 'id'=>'tipo_reajuste')) !!}
        </td>

        <td class="tblabel">Mês Reajuste(*)</td>
        <td>
        {!! Form::text('mes_reajuste', isset($carro->mes_reajuste) ? $carro->mes_reajuste : null, array('class'=>'form-control', 'id'=>'mes_reajuste')) !!}
        </td>

    </tr>
    <tr class='tralugado'>    
        <td class="tblabel">Ano Reajuste(*)</td>
        <td>
        {!! Form::text('ano_reajuste', isset($carro->ano_reajuste) ? $carro->ano_reajuste : null, array('class'=>'form-control', 'id'=>'ano_reajuste')) !!}
        </td>
    </tr>    

    <tr class='trcomprado'>
        <td class="tblabel">Data Compra(*)</td>
        <td>
            @if(isset($carro->data_compra))
            <input type="date" id='data_compra' name="data_compra" class="form-control" value="{{ $carro->data_compra->format('Y-m-d') }}">
            @else    
            <input id='data_compra' type="date" name="data_compra" class="form-control">
            @endif    
        </td>
        

        <td class="tblabel">Valor Compra(*)</td>
        <td>
            {!! Form::text('valor_compra', isset($carro->valor_compra) ? $carro->valor_compra : null, array('class'=>'form-control', 'id'=>'valor_compra')) !!}
        </td>
        
        <td colspan="4"></td>
    </tr>
</table>
