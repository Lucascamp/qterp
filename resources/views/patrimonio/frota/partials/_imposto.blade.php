<table class="table table-bordered tabela add_imposto_table">
    <tr>
        <td class="tbtitulo" colspan="8">Impostos</td>
    </tr>
    @if(isset($carro))
        @foreach($carro->imposto as $imposto)
            <tr>
                <td class="tblabel">Tipo do imposto(*)</td>
                <td>
                    {!! Form::text('tipo_imposto_edit['.$imposto->id.'][]', $imposto->tipo_imposto, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>

                <td class="tblabel">Valor R$(*)</td>
                <td>
                    {!! Form::text('valor_imposto_edit['.$imposto->id.'][]', $imposto->valor_imposto, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>

                <td class="tblabel">Data de pagamento(*)</td>
                <td>
                @if(isset($imposto->data_pagamento_imposto))
                    <input type="date" name="data_pagamento_imposto_edit[{{$imposto->id}}][]" class="form-control" value="{{ $imposto->data_pagamento_imposto->format('Y-m-d') }}">
                @else    
                    <input type="date" name="data_pagamento_imposto_edit[{{$imposto->id}}][]" class="form-control">
                @endif    
                </td>
            </tr>     
            <tr>    
                <td class="tblabel">Restrição(*)</td>
                <td>
                    {!! Form::text('restricao_edit['.$imposto->id.'][]', $imposto->restricao, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>

                <td class="tblabel">Tipo de Restrição(*)</td>
                <td>
                    {!! Form::text('tipo_restricao_edit['.$imposto->id.'][]', $imposto->tipo_restricao, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>
            </tr>
            <td class="tbtitulo" colspan="8"></td>
        @endforeach

    <tr>
        <td colspan="8">
            <div><button class="add_imposto_button btn btn-warning">Adicionar Novo Imposto</button></div>
        </td>
    </tr> 
    @else 
    <tr>
        <td colspan="8">
            <div><button class="add_imposto_button btn btn-warning">Adicionar Novo Imposto</button></div>
        </td>
    </tr>  
    <tr>
        <td class="tblabel">Tipo do imposto(*)</td>
        <td>
            {!! Form::text('tipo_imposto[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
        </td>

        <td class="tblabel">Valor R$(*)</td>
        <td>
            {!! Form::text('valor_imposto[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
        </td>

        <td class="tblabel">Data de pagamento(*)</td>
        <td>
            <input type="date" name="data_pagamento_imposto[]" class="form-control">
        </td>
    </tr>     
    <tr>    
        <td class="tblabel">Restrição(*)</td>
        <td>
            {!! Form::text('restricao[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
        </td>

        <td class="tblabel">Tipo de Restrição(*)</td>
        <td>
            {!! Form::text('tipo_restricao[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
        </td>
    </tr>
    @endif
</table>