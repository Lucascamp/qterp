<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Seguro</td>
    </tr>
    <tr>
        <td class="tblabel">Seguradora(*)</td>
        <td>
        {!! Form::text('seguradora', isset($carro->seguro->seguradora) ? $carro->seguro->seguradora : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Corretor(*)</td>
        <td>
        {!! Form::text('corretor', isset($carro->seguro->corretor) ? $carro->seguro->corretor : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Número da apólice(*)</td>
        <td>
        {!! Form::text('apolice', isset($carro->seguro->apolice) ? $carro->seguro->apolice : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Valor do seguro(*)</td>
        <td>
        {!! Form::text('valor_seguro', isset($carro->seguro->valor_seguro) ? $carro->seguro->valor_seguro : null, array('class'=>'form-control')) !!}
        </td>
    </tr>
    <tr>    
        <td class="tblabel">Data Inicial(*)</td>
        <td>
            @if(isset($carro->seguro->data_inicial))
                <input type="date" name="data_inicial" class="form-control" value="{{ $carro->seguro->data_inicial->format('Y-m-d') }}">
            @else    
                <input type="date" name="data_inicial" class="form-control">
            @endif    
        </td>

        <td class="tblabel">Data Final(*)</td>
        <td>
            @if(isset($carro->seguro->data_final))
                <input type="date" name="data_final" class="form-control" value="{{ $carro->seguro->data_final->format('Y-m-d') }}">
            @else    
                <input type="date" name="data_final" class="form-control">
            @endif    
        </td>

        <td class="tblabel">Vistoria(*)</td>
        <td>
        {!! Form::text('vistoria', isset($carro->seguro->vistoria) ? $carro->seguro->vistoria : null, array('class'=>'form-control')) !!}
        </td>

        <td colspan="2"></td>
        
    </tr>
</table>