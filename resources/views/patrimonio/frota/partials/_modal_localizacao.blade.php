<div class="modal fade" id="modalLocalizacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Movimentação de frota</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        {!! Form::open(array('route' => 'frota.movimentar', 'class'=>'form-inline')) !!}
        {!! Form::hidden('created_by', '1') !!}
        {!! Form::token() !!}
        {!! Form::hidden('carro_id', '', array('id' => 'frota_id')); !!}
        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Unidade(*)</td>
                <td colspan="3">
                    {!! Form::select('unidade', $unidades, null ,array( 'class' => 'form-control chosen-select', 'style' => 'width: 100%')) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel">Centro de custo(*)</td>
                <td colspan="3">
                    {!! Form::select('centrocusto_id', $centro_custo, null ,array( 'class' => 'form-control chosen-select', 'style' => 'width: 100%')) !!}
                </td>
            </tr>
            <tr>     
                <td class="tblabel">Data movimentação(*)</td>
                <td>
                    <input type="date" name="data" class="form-control">
                </td>
            </tr>
        </table>
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}

        {!! Form::close() !!}
    </div>
</div>
</div>
</div>