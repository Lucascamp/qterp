<table class="table table-bordered tabela add_manutencao_table">
    <tr>
        <td class="tbtitulo" colspan="8">Manutenção</td>
    </tr>
    @if(isset($carro))
        @foreach($carro->manutencao as $manutencao)
        <tr>
            <td class="tblabel">Descrição da manutenção(*)</td>
            <td>
                {!! Form::text('descricao_manutencao_edit['.$manutencao->id.'][]', $manutencao->descricao_manutencao, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
            </td>

            <td class="tblabel">Valor R$(*)</td>
            <td>
                {!! Form::text('valor_manutencao_edit['.$manutencao->id.'][]', $manutencao->valor_manutencao, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
            </td>

            <td class="tblabel">Data de Manutenção(*)</td>
            <td>
                @if(isset($manutencao->data_manutencao))
                <input type="date" name="data_manutencao_edit[{{$manutencao->id}}][]" class="form-control" value="{{ $manutencao->data_manutencao->format('Y-m-d') }}">
                @else    
                <input type="date" name="data_manutencao_edit[{{$manutencao->id}}][]" class="form-control">
                @endif    
            </td>
        </tr> 
        <tr> 
           <td class="tbtitulo" colspan="8"></td>
        </tr>     
        @endforeach
    <tr>
        <td colspan="8">
            <div><button class="add_manutencao_button btn btn-danger">Adicionar Manutenção</button></div>
        </td>
    </tr>   
    @else 
    <tr>
        <td colspan="8">
            <div><button class="add_manutencao_button btn btn-danger">Adicionar Manutenção</button></div>
        </td>
    </tr>
        <tr>
            <td class="tblabel">Descrição da manutenção(*)</td>
            <td>
                {!! Form::text('descricao_manutencao[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
            </td>

            <td class="tblabel">Valor R$(*)</td>
            <td>
                {!! Form::text('valor_manutencao[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
            </td>

            <td class="tblabel">Data de Manutenção(*)</td>
            <td>
            <input type="date" name="data_manutencao[]" class="form-control">
            </td>
        </tr>    
    @endif 
</table>