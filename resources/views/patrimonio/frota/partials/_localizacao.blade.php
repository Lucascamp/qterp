<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Localização</td>
    </tr>

    <tr>
        <td class="tblabel">Unidade(*)</td>
        <td>
            {!! Form::select('unidade', $unidades,
             isset($localizacao->unidade) ? $localizacao->unidade : null,
             array( 'class' => 'form-control chosen-select')) !!}
        </td>
        <td class="tblabel">Centro de custo atual(*)</td>
        <td>
            {!! Form::select('centrocusto_id', $centro_custo,
            isset($localizacao->centrocusto_id) ? $localizacao->centrocusto_id : null, 
            array( 'class' => 'form-control chosen-select')) !!}
        </td>

        <td class="tblabel">Data(*)</td>
        <td>
            @if(isset($localizacao->data))
            {!! Form::hidden('localizacao_id', $localizacao->id) !!}
            <input type="date" name="data" class="form-control" value="{{ $localizacao->data->format('Y-m-d') }}">
            @else    
            <input type="date" name="data" class="form-control">
            @endif    
        </td>
    </tr>
</table>