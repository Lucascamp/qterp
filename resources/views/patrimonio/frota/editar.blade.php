@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Equipamentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li>
                <a href="/frota">Frota</a>
            </li>
            <li class="active">
                <strong>Editar Veículo</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Veículo<small>(*)campos obrigatorios</small></h5>
                </div>
                <div class="ibox-content">
                    {!!  Form::open(array('method' => 'PATCH', 'route' => array('frota.atualizar', $carro->id), 'class'=>'form-inline')) !!}
                    
                    {!! Form::hidden('carro_id', $carro->id) !!}

                    <input type="hidden" id="token" value="{{ csrf_token() }}">

                    @include('patrimonio.frota.partials._dados')
                    @include('patrimonio.frota.partials._localizacao')
                    @include('patrimonio.frota.partials._seguro')
                    @include('patrimonio.frota.partials._ecofrota')
                    @include('patrimonio.frota.partials._imposto')
                    @include('patrimonio.frota.partials._manutencao')

                    <div class="pull-right">
                    {!! Form::submit('Editar Veículo', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}
                </div>
                <br><br>
                @if ($errors->any())

                <br><br>
                    <div class="alert-group">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                            @endforeach
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</br>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop
