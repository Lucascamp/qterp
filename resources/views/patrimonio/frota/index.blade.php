@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Frota</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li class="active">
                <strong>Frota</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            @if (in_array(3,$permissoes_opcoes))
                <a href="/frota/cadastrar" class="btn btn-primary">Adicionar carro a frota</a>
            @endif
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>Frota cadastrada</h5>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <div class="clear"><br></div>

        <table class="table table-bordered tabela">
            <thead>
                <tr>
                    <th>Placa</th>
                    <th>Modelo</th>
                    <th>Combustível</th>
                    <th>Ano</th>
                    <th>Cor</th>
                    <th>Chassi</th>
                    <th>Renavam</th>
                    <th>KM</th>
                    <th>Localização</th>
                    <th colspan="5">Ações</th>
                </tr>
            </thead>

            <tbody>
             @if (isset($carros))
             @foreach ($carros as $carro)
             <tr>
                <td>{{{ $carro->placa }}}</td>
                <td>{{{ $carro->modelo }}}</td>
                <td>{{{ $carro->combustivel }}}</td>
                <td>{{{ $carro->ano }}}</td>
                <td>{{{ $carro->cor }}}</td>
                <td>{{{ $carro->chassi }}}</td>
                <td>{{{ $carro->renavan }}}</td>
                <td>{{{ $carro->km }}}</td>

                @if(count($carro->localizacao) > 0)
                <?php 
                    $count = count($carro->localizacao)-1;
                    $ultimo_local = $carro->localizacao[$count];
                ?>  
                    <td>{{{ $ultimo_local->unidade }}}</td>
                @else
                    <td>-</td>
                @endif 
                @if (in_array(3,$permissoes_opcoes))
                <td>
                     <button type="button" data-target="#modalFrotaInfo" data-toggle="modal" data-tooltip="tooltip" class="btn btn-primary fa fa-file-o infoFrotaBtn" style="width: 41px; height:34px;" title="Mais informações" data_value="{{ $carro->id }}"></button>
                </td>   

                <td> 
                    <a href="{{ route('frota.editar', array($carro->id)) }}" data-toggle="tooltip" title="Editar" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-info dim">
                        <i class="fa fa-pencil"></i></a>
                </td>  

                <td>
                    <button type="button" data-target="#modalLocalizacao" data-toggle="modal" data-tooltip="tooltip" class="btn btn-warning fa fa-automobile localizacaoBtn" style="width: 41px; height:34px;" title="Alterar Localização"
                        data_value="{{ $carro->id }}"></button>
                </td>

                <td>
                    <button type="button" data-target="#modalExcluir" data-toggle="modal" data-tooltip="tooltip" class="btn btn-danger fa fa-close excluirBtn" style="width: 41px; height:34px;" title="Excluir"
                        data_value="{{ $carro->id }}" data_value2="{{ $carro->placa }}"></button>
                </td>
                @else
                    <td></td>
                @endif
            </tr>
            @endforeach
            @else
                <tr> 
                    <td colspan="20" class="center">
                        Nenhum veiculo encontrado.
                    </td>
                </tr>
                @endif    
            </tbody> 
        </table>
    </div>
</div>
</div>

@include('patrimonio.frota.partials._modal_info')
@include('patrimonio.frota.partials._modal_localizacao')
@include('patrimonio.frota.partials._modal_excluir')

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop