@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Equipamentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li class="active">
                <strong>Equipamentos</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            @if (in_array(2,$permissoes_opcoes))  
            <a href="/equipamento/cadastrar" class="btn btn-primary">Novo Equipamento</a>
            {!! link_to_route('equipamentos.excel', 'Exportar Todos para excel', null, array('class' => 'btn btn-success')) !!}
            @endif
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>Todos os Equipamentos Cadastrados</h5>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">
        <table class="table tabela">
            {!! Form::open(array('route' => 'equipamentos.filtro', 'method' => 'get')) !!}
            <tr>
                <td>
                    {!! Form::select('filter_descricao', $filter_descricao, $descricao, array('id'=>'descricao', 'class'=>'chosen-select')) !!}
                </td>

                <td>
                    {!! Form::select('filter_patrimonio', $filter_patrimonio , $patrimonio, array('id'=>'patrimonio', 'class'=>'chosen-select')) !!}
                </td>
                <td>
                    {!! Form::select('filter_serial', $filter_serial , $serial, array('id'=>'serial', 'class'=>'chosen-select')) !!}
                </td>
                <td>
                    {!! Form::select('filter_modelo', $filter_modelo , $modelo, array('id'=>'modelo', 'class'=>'chosen-select')) !!}
                </td>
                <td>
                    {!! Form::select('filter_local', $filter_local , $local_equip, array('id'=>'localizacao', 'class'=>'chosen-select')) !!} 
                </td>
                <td>
                    <button type="submit" data-tooltip="tooltip" class="btn btn-primary fa fa-filter" style="width: 41px; height:34px;" title="Filtrar tabela"></button>
                
                    <button type="button" data-tooltip="tooltip" class="btn btn-info fa fa-eraser" style="width: 41px; height:34px;" id="limparequip" title="Limpar"></button>



                    {!! Form::close() !!}
                </td>
            </tr>
        </table>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        {!! $equipamentos->appends(array(
                            'filter_descricao' => Input::get('filter_descricao'),
                            'filter_patrimonio' => Input::get('filter_patrimonio'),
                            'filter_serial' => Input::get('filter_serial'),
                            'filter_modelo' => Input::get('filter_modelo'),
                            'filter_local' => Input::get('filter_local')
        ))->render() !!}

        <div class="clear"><br></div>

        {{ $total_equips }} itens encontrados

        <table class="table table-bordered tabela">
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th>Aplicacação</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Serial</th>
                    <th>Patrimônio</th>
                    <th>Utilização</th>
                    <th>Fornecedor</th>
                    <th>Data Movimentação</th>
                    <th>Localização</th>
                    <th>Projeto</th>
                    <th>Usuário Receptor</th>
                    <th>Funcionário Responsável</th>
                    <th>Propietário</th>
                    <th>Regime</th>
                    <th colspan="5">Ações</th>
                </tr>
            </thead>

            <tbody>
             @if (isset($equipamentos))
             @foreach ($equipamentos as $equipamento)
             <tr>
                <td>{{{ $equipamento->descricao }}}</td>
                <td>{{{ $equipamento->aplicacao }}}</td>
                <td>{{{ $equipamento->marca }}}</td>
                <td>{{{ $equipamento->modelo }}}</td>
                <td>{{{ $equipamento->serial }}}</td>
                <td>{{{ $equipamento->patrimonio }}}</td>
                <td>{{{ $equipamento->descricao_uso }}}</td>
                <td>{{{ $equipamento->fornecedor }}}</td>

                @if(isset($localizacao[$equipamento->id]->data_movimentacao))
                    <td>{{{ $localizacao[$equipamento->id]->data_movimentacao->format('d/m/Y') }}}</td>
                    <td>{{{ $centro_custo[$localizacao[$equipamento->id]->centrocusto_id] }}}</td>
                    <td>{{{ $localizacao[$equipamento->id]->projeto }}}</td>
                @else
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                @endif        

                @if(!empty($localizacao[$equipamento->id]->usuario_receptor))
                    <td>{{{ $usuarios[$localizacao[$equipamento->id]->usuario_receptor] }}}</td>
                @else
                    <td>-</td>
                @endif  

                @if(!empty($localizacao[$equipamento->id]->funcionario_responsavel) and isset($localizacao[$equipamento->id]->funcionario_responsavel))
                   
                    <td>{{{$funcionarios[$localizacao[$equipamento->id]->funcionario_responsavel]}}}</td>     
                   
                @else
                    <td>-</td>
                @endif 
                
                <td>{{{ $equipamento->proprietario }}}</td>
                <td>{{{ $equipamento->regime }}}</td>

                <td>
                    @if(isset($localizacao[$equipamento->id]))

                        @if($localizacao[$equipamento->id]->recebido == '1')
                        <font color ='green'>Recebido</font>

                        @elseif(isset($localizacao[$equipamento->id]->usuario_receptor) and $localizacao[$equipamento->id]->usuario_receptor ==  Auth::user()->id)
                        <button type="button" data-tooltip="tooltip" class="btn btn-success fa fa-check receberBtn" style="width: 41px; height:34px;" title="Receber" data_value="{{ $localizacao[$equipamento->id]->id }}"></button>
                        
                        @elseif(!isset($localizacao[$equipamento->id]->usuario_receptor))
                        -   

                        @else
                        <font color ='red'>Aguardando Recebimento</font>

                        @endif

                    @else
                    -    
                    
                    @endif      
                </td> 
                <td>
                    <button type="button" data-target="#modalHistorico" data-toggle="modal" data-tooltip="tooltip" class="btn btn-primary fa fa-exchange historicoBtn" style="width: 41px; height:34px;" title="Exibir Histórico"
                        data_value="{{ $equipamento->id }}"></button>
                </td>  
                <td> 
                    <a href="{{ route('equipamento.editar', array($equipamento->id)) }}" data-toggle="tooltip" title="Editar" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-info dim">
                        <i class="fa fa-pencil"></i></a>
                    </td>  

                    <td>
                        <button type="button" data-target="#modalLocalizacao" data-toggle="modal" data-tooltip="tooltip" class="btn btn-warning fa fa-automobile localizacaoBtn" style="width: 41px; height:34px;" title="Alterar Localização"
                        data_value="{{ $equipamento->id }}"></button>
                    </td>

                    <td>
                        @if (in_array(2,$permissoes_opcoes)) 
                        <button type="button" data-target="#modalExcluir" data-toggle="modal" data-tooltip="tooltip" class="btn btn-danger fa fa-close excluirBtn" style="width: 41px; height:34px;" title="Excluir"
                        data_value="{{ $equipamento->id }}" data_value2="{{ $equipamento->descricao }}"></button>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr> 
                    <td colspan="20" class="center">
                        Nenhum equipamento encontrado.
                    </td>
                </tr>
                @endif    
            </tbody> 
        </table>
    </div>
</div>
</div>

@include('patrimonio.equipamento.partials._modal_historico')

@include('patrimonio.equipamento.partials._modal_localizacao')

@include('patrimonio.equipamento.partials._modal_excluir')

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop