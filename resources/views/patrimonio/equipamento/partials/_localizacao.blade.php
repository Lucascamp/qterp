<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Localização</td>
    </tr>
    <tr>
        <td class="tblabel">Local Atual(*)</td>
        <td>
        {!! Form::select('centrocusto_id', $centro_custo, isset($localizacao) ? $localizacao->centrocusto_id : null ,array( 'class' => 'form-control chosen-select')) !!}
        </td>

        <td class="tblabel">Projeto Atual(*)</td>
        <td>
       		{!! Form::text('projeto', isset($localizacao->projeto) ? $localizacao->projeto : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Data movimentação(*)</td>
        <td>
        @if(isset($localizacao->data_movimentacao))
            <input type="date" name="data_movimentacao" class="form-control" value="{{ $localizacao->data_movimentacao->format('Y-m-d') }}">
        @else    
            <input type="date" name="data_movimentacao" class="form-control">
        @endif    
        </td>
        <tr>
            <td class="tblabel">Funcionário Responsável(*)</td>
            <td>
                {!! Form::select('funcionario_responsavel', $funcionarios, null ,array( 'class' => 'form-control chosen-select')) !!}
            </td>

            <td class="tblabel">Usuário Receptor(*)</td>
            <td colspan="3">
                {!! Form::select('usuario_receptor', $usuarios, null ,array( 'class' => 'form-control chosen-select')) !!}
            </td>
        </tr>
        <tr>
            <td class="tblabel">Motivo da movimentação(*)</td>
            <td colspan="5">
                {!! Form::text('motivo', null, array('class'=>'form-control', 'style' => 'width: 100%')) !!}
            </td>
        </tr>
    </tr>
</table>