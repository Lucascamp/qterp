<div class="modal fade" id="modalManutencao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sl">
    <div class="modal-content">
      <div class="modal-header" style="background: #ed5565">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Mandar equipamento para Manutenção</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        
        
        {!! Form::hidden('created_by', $id_user) !!}
        {!! Form::hidden('equipamento_id', '', array('id' => 'equipamento_id_manu')); !!}

        <p>Observações:</p>
        {!! Form::text('motivo_manu', null, array('id'=>'obs_manu' ,  'class'=>'form-control', 'style' => 'width: 100%')) !!}
        <p>Data Movimentação:</p>
        <input type="date" name="data_movimentacao" id="data_movimento_manu" class="form-control">
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="bt_salvar_manutencao" >Salvar</button>
        
    </div>
</div>
</div>
</div>