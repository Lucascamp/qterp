<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Dados do equipamento</td>
    </tr>
    <tr>
        <td class="tblabel">Descrição(*)</td>
        <td>
        {!! Form::text('descricao', isset($equipamento) ? $equipamento->descricao : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Aplicação(*)</td>
        <td>
        {!! Form::text('aplicacao', isset($equipamento) ? $equipamento->aplicacao : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Marca(*)</td>
        <td>
        {!! Form::text('marca', isset($equipamento) ? $equipamento->marca : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Modelo(*)</td>
        <td>
        {!! Form::text('modelo', isset($equipamento) ? $equipamento->modelo : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Serial(*)</td>
        <td>
        {!! Form::text('serial', isset($equipamento) ? $equipamento->serial : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Número de patrimônio(*)</td>
        <td>
        {!! Form::text('patrimonio', isset($equipamento) ? $equipamento->patrimonio : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Descrição de uso(*)</td>
        <td>
        {!! Form::text('descricao_uso', isset($equipamento) ? $equipamento->descricao_uso : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Data Compra(*)</td>
        <td>
        @if(isset($equipamento->data_compra))
            <input type="date" name="data_compra" class="form-control" value="{{ $equipamento->data_compra->format('Y-m-d') }}">
        @else    
            <input type="date" name="data_compra" class="form-control">
        @endif    
        </td>
    </tr>

    <tr>
        <td class="tblabel">Nota Fiscal(*)</td>
        <td>
        {!! Form::text('nota_fiscal', isset($equipamento) ? $equipamento->nota_fiscal : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Valor Compra(*)</td>
        <td>
        {!! Form::text('valor_compra', isset($equipamento) ? $equipamento->valor_compra : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Fornecedor(*)</td>
        <td>
        {!! Form::text('fornecedor', isset($equipamento) ? $equipamento->fornecedor : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Taxa Depreciação(*)</td>
        <td>
        {!! Form::text('taxa_depreciacao', isset($equipamento) ? $equipamento->taxa_depreciacao : null, array('class'=>'form-control')) !!}
        </td>
    </tr>
</table>

<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Propiedade</td>
    </tr>
    <tr>
        <td class="tblabel">Propietário(*)</td>
        <td>
        {!! Form::text('proprietario', isset($equipamento) ? $equipamento->proprietario : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Regime(*)</td>
        <td>
        {!! Form::select('regime', array(
                    '' => 'Selecione o Regime',
                    'Empréstimo' => 'Empréstimo',
                    'Alugado' => 'Alugado',
                    'Transfência' => 'Transfência'
                    ), isset($equipamento) ? $equipamento->regime : null, array('class'=>'form-control')) !!}


        </td>
    </tr>
</table>