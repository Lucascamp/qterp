<div class="modal fade" id="modalLocalizacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Movimentação de equipamento</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        
        {!! Form::hidden('created_by', $id_user) !!}
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        {!! Form::hidden('equipamento_id', '', array('id' => 'equipamento_id')); !!}
        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Destino(*)</td>
                <td colspan="3">
                    {!! Form::select('unidade_destino', $unidades_movi, null ,array( 'id'=>'unidade_destino' , 'class' => 'form-control chosen-select', 'style' => 'width: 100%')) !!}
                </td>
            </tr>
            <tr>    
                <td class="tblabel">Projeto(*)</td>
                <td>
                    {!! Form::text('projeto', null, array('id'=>'projeto' , 'class'=>'form-control')) !!}
                </td>
            </tr>
            <tr>     
                <td class="tblabel">Data movimentação(*)</td>
                <td>
                    <input type="date" name="data_movimentacao" id="data_movimento" class="form-control">
                </td>
            </tr>
            <tr>
                <td class="tblabel">Funcionário destinatário</td>
                <td>
                    {!! Form::select('funcinario_destino', $funcionarios, null ,array('id'=>'funcinario_destino' , 'class' => 'form-control chosen-select')) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel">Motivo da movimentação(*)</td>
                <td colspan="3">
                    {!! Form::text('motivo', null, array('id'=>'motivo' ,  'class'=>'form-control', 'style' => 'width: 100%')) !!}
                </td>
            </tr>
        </table>
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" data-dismiss="modal" id="bt_salvar_movimentacao" >Salvar</button>

        
    </div>
</div>
</div>
</div>