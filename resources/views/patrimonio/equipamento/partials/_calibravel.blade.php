<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Calibração</td>
    </tr>
    <tr>
        <td class="tblabel">Calibrável(*)</td>
        <td>
        	<div class="simnaoswitch">
        		<input type="checkbox" name="calibravel" class="simnaoswitch-checkbox onoff" id="calibravel" value="1"
        		<?php if (isset($equipamento->calibravel) and $equipamento->calibravel == 1){?> checked="checked" <?php } ?> > 
        		<label class="simnaoswitch-label" for="calibravel">
        			<span class="simnaoswitch-inner"></span>
        			<span class="simnaoswitch-switch"></span>
        		</label>
        	</div>
        </td>

        <td class="tblabel">Frequência(*)</td>
        <td>
       		{!! Form::text('frequencia', isset($equipamento->frequencia) ? $equipamento->frequencia : null, array('class'=>'form-control')) !!}
        </td>
    </tr>
</table>
