@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Equipamentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li class="active">
                <strong>Equipamentos</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            @if (in_array(2,$permissoes_opcoes))  
            <a href="/equipamento/cadastrar" class="btn btn-primary">Novo Equipamento</a>
            @endif
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <span id="titulo"></span>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">
        <table class="table tabela">
            <tr>
                <td>
                    {!! Form::select('filtro_unidade', $unidades, $unidade, array('id'=>'unidade', 'class'=>'chosen-select')) !!}
                </td>
                <td>
                    <a href="#" class="btn btn-info" id="bt_filtro_pendente"><i class="fa fa-filter"></i> Filtrar apenas pendentes</a>
                    <a href="#" class="btn btn-primary" id="bt_filtro_todos"><i class="fa fa-filter"></i> Mostrar todos</a>
                </td>
            </tr>
        </table>
        <div class="clear"><br></div>
        <table class="table table-bordered tabela" id="tabela_equipamentos">
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th>Modelo</th>
                    <th>Serial</th>
                    <th>Patrimônio</th>
                    <th>Data <br>Movimentação</th>
                    <th>Localização</th>
                    <th>Funcionário <br>Remetente</th>
                    <th>Funcionário <br>destinatario</th>
                    <th>Status</th>
                    <th WIDTH="275">Ações</th>
                </tr>
            </thead>

            <tbody>
                <tr> 
                    <td>Carregando</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody> 
        </table>
    </div>
</div>
</div>

@include('patrimonio.equipamento.partials._modal_historico')

@include('patrimonio.equipamento.partials._modal_localizacao')

@include('patrimonio.equipamento.partials._modal_excluir')

@include('patrimonio.equipamento.partials._modal_manutencao')

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop