@extends('layouts.master')

@section('menu')
@include('patrimonio.menu')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Gestão de patrimônio</strong>
            </li>
        </ol>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="text-center animated fadeInRightBig">
                <h3 class="font-bold">Gestão de patrimônio</h3>
                <div class="error-desc">
                    @foreach ($areas as $area)
                        <a href="{{$area['rota']}}" class="btn btn-primary m-t">{{$area['descricao']}}</a>
                    @endforeach
                    @if (Config::get('app.atualizavel'))
                        <br>
                        <br>
                        <br>
                        <h3 class="font-bold">Sincronizações</h3>
                        <a href="#" class="btn btn-primary m-t" id="bt_sinc_equipamentos">Receber dados de Equipamentos</a>
                        <a href="#" class="btn btn-primary m-t" id="bt_sinc_locequipamentos">Enviar dados de Equipamentos</a>
                        <a href="#" class="btn btn-primary m-t" id="bt_sinc_almoxarifado">Receber dados de Almoxarifado</a>
                        <a href="#" class="btn btn-primary m-t" id="bt_sinc_histoalmoxarifado">Enviar dados de Almoxarifado</a>
                        <a href="#" class="btn btn-primary m-t" id="bt_sinc_frota">Receber dados de frota</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
