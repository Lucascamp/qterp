<?php use Carbon\Carbon; ?>
<div class="col-lg-4">
    <div class="ibox float-e-margins ">
        <div class="ibox-title">
            <h5>SGI - <small>Inspeções / Auditorias a serem realizados</small></h5>
        </div>
        <div class="ibox-content no-margins " style="overflow-y: auto; height: 250px;">
            <div class="row">
                <table class="table table-bordered tabela">
                    <thead>
                        <th>Unidade</th>
                        <th>Inspeção - Auditoria</th>
                        <th>Data</th>
                        <th>Opções</th>
                    </thead>
                    <tbody>
                        @foreach ($marcacoes as $key => $marcacao)
                            <tr>
                                <td>{{$unidades[$marcacao->unidade_id]}}</td>
                                <td>{{$insp_aud[$marcacao->insp_aud_id]}}</td>
                                <td>{{ Carbon::createFromFormat('Y-m-d H:i:s', $marcacao->data_ini)->format('d/m/Y')}}</td>
                                <td><a href="/sgi/formulario/{{$marcacao->insp_aud_id}}/{{$marcacao->id}}" class="btn btn-info" title="PREENCHER"><i class="fa fa-file-text-o"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
</div>