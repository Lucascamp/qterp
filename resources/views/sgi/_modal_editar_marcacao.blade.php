<div class="modal fade" id="modalEditarMarcacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Marcação de Inspeção ou Auditoria</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        
        {!! Form::hidden('created_by', $id_user) !!}
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="id_marcacao" value="">

        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Unidade(*)</td>
                <td colspan="3">
                     {!! Form::select('unidade', $unidades, null,array('id' => 'filtro_unidade_marcacao_edt', 'size'=>'30', 'class'=>'chosen-select')) !!}
                </td>
            </tr>
            
            <tr>
                <td class="tblabel">Inspeção - Auditoria(*)</td>
                <td colspan="3">
                   {!! Form::select('insp_aud', $insp_aud, null,array('id' => 'filtro_insp_aud_marcacao_edt', 'size'=>'30', 'class'=>'chosen-select')) !!}
                </td>
            </tr>
            <tr>    
                <td class="tblabel">Responsavel(*)</td>
                <td>
                    {!! Form::select('funcionario', $funcionarios, null,array('id' => 'filtro_funcionario_marcacao_edt', 'size'=>'30', 'class'=>'chosen-select')) !!}
                </td>
            </tr>
            <tr>    
                <td class="tblabel">Data inicio(*)</td>
                <td>
                    <input type="date" class="form-control" id="data_ini_marcacao_edt">
                </td>
            </tr>
            <tr>    
                <td class="tblabel">Data fim(*)</td>
                <td>
                    <input type="date" class="form-control" id="data_fim_marcacao_edt">
                </td>
            </tr>
        </table>
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="bt_salvar_marcacao_insp_aud_edt" >Salvar</button>

        
    </div>
</div>
</div>
</div>