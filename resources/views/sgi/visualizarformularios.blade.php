@extends('layouts.master')

@section('menu')

@include('sgi.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>SGI</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/sgi">SGI</a>
            </li>
            <li>
                <a href="/sgi/inspecoesauditorias">Inspeções e Auditorias</a>
            </li>
            <li class="active">
                <strong>Formularios Preenchidos</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <span id="titulo">Formulario {{$insp_aud->tipo}} - {{$insp_aud->descricao}}</span>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        <div class="clear"><br></div>
        <table class="table table-bordered tabela">

        </table>
    </div>
</div>
</div>


<script src="/js/jquery2.1.js"></script>
{!! HTML::script('js/sgi/inspecoesauditorias.js') !!}

@stop