<div class="modal fade" id="modaleditarFormulario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Edição de Formulario Inspeção e Auditoria</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        
        {!! Form::hidden('created_by', $id_user) !!}
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="id_formulario" value="">
        
        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Tipo(*)</td>
                <td colspan="3">
                    <span id="tipoedt"></span>
                </td>
            </tr>
            <tr>    
                <td class="tblabel">Descrição(*)</td>
                <td>
                    {!! Form::text('descricao', null, array('id'=>'descricaoedt' , 'class'=>'form-control')) !!}
                </td>
            </tr>
        </table>
        <table class="table table-bordered tabela">
            <tr>
                <td colspan="4" class="tblabel">Itens(*)</td>
                <td class="tblabel">
                    <button type="button" class="btn btn-success" id="bt_add_item_edt"><span aria-hidden="true"><i class="fa fa-plus"></i></span></button>
                </td>
            </tr>
            <tr>
                <td width="30">Nº</td>
                <td width="250">Inspeções e Verificações</td>
                <td width="250">Caracterização</td>
                <td width="250">Documentos e Evidencias</td>
                <td align="center"><div><i class="fa fa-sliders"></i></div></td>
            </tr>
            <tbody id="itens_editar_formulario">
            </tbody>
            
        </table> 
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="bt_salvar_formulario_edt" >Salvar</button>

        
    </div>
</div>
</div>
</div>