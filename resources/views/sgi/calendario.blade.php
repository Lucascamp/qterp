@extends('layouts.master')

@section('menu')

@include('sgi.menu')

@stop

@section('main')

<?php use Carbon\Carbon; ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h2>SGI</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/sgi">SGI</a>
            </li>
            <li>
                <a href="/sgi/inspecoesauditorias">Inspeções Auditoria</a>
            </li>
            <li class="active">
                <strong>Calendario de Inspeções e Auditorias</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">
            @if (in_array(5,$permissoes_opcoes))  
                <button type="button" data-target="#modalNovaMarcacao" data-toggle="modal" data-tooltip="tooltip" title="Criar Marcação" class="btn btn-primary">Nova marcação</button>
            @endif
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <span id="titulo">Calendarios de Inspeções e Auditorias</span>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
        <div class="ibox-content">
            <div class="clear"><br></div>
            <div class="row">
                @if (in_array(5,$permissoes_opcoes))
                    <div class="col-lg-3">
                        <legend>Unidade</legend>
                        {!! Form::select('unidade_marcacao', $unidades, null,array('id' => 'filtro_unidade_marcacao', 'size'=>'30', 'class'=>'chosen-select')) !!}
                    </div>
                @else
                    <input type="hidden" id="filtro_unidade_marcacao" value="{{ $unidade_id_user }}">
                @endif
                <div class="col-lg-3">
                    <legend>Data Inicio</legend>
                    <input type="date" class="form-control" id="data_ini_marcacao">
                </div>
                <div class="col-lg-3">
                    <legend>Data Fim</legend>
                    <input type="date" class="form-control" id="data_fim_marcacao" value="">
                </div>
            </div>
            <br>
            <div class="row">
                <table class="table table-bordered tabela">
                    <thead>
                        <th>unidade</th>
                        <th>Responsável</th>
                        <th>Inspeção - Auditoria</th>
                        <th>Data a ser realizado</th>
                        <th>Status</th>
                        <th>Opções</th>
                    </thead>
                    <tbody id="tabela_marcacoes">
                        @foreach ($marcacoes as $key => $marcacao)
                            <?php 
                                $data_ini = Carbon::createFromFormat('Y-m-d H:i:s', $marcacao->data_ini);
                                $data_fim = Carbon::createFromFormat('Y-m-d H:i:s', $marcacao->data_fim);
                                $hoje =  Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d'.' 00:00:00' ));
                            ?>  
                            <tr>
                                
                                <td>{{$unidades[$marcacao->unidade_id]}}</td>
                                <td>{{$funcionarios[$marcacao->responsavel_id]}}</td>
                                <td>{{$insp_aud[$marcacao->insp_aud_id]}}</td>
                                <td>
                                    @if ($data_ini->diffInDays($data_fim) == 0)
                                        {{ $data_ini->format('d/m/Y') }}                                       
                                    @else
                                        {{ $data_ini->format('d/m/Y') }}
                                        a 
                                        {{ $data_fim->format('d/m/Y') }}
                                    @endif                               
                                </td>
                                <td>
                                    @if ($marcacao->status == 1)
                                        <font color ='red'>{{'Atrasado a '.$data_ini->diffInDays($hoje).' Dias'}}</font>
                                    @elseif ($marcacao->status == 0) 
                                        {{'Faltam '.$data_ini->diffInDays($hoje). ' Dias'}}
                                    @elseif ($marcacao->status == 2)
                                        <font color ='green'> Realizado </font>
                                    @endif
                                </td>
                                <td>
                                    @if ($marcacao->status != 2)
                                        <a href="/sgi/formulario/{{$marcacao->insp_aud_id}}/{{$marcacao->id}}" class="btn btn-info" title="PREENCHER"><i class="fa fa-file-text-o"></i></a>    
                                    @else
                                        @if (isset($formularios_marcacao[$marcacao->id]))
                                            <a href="/sgi/formulariop/{{$formularios_marcacao[$marcacao->id]}}" class="btn btn-info" title="VISUALIZAR"><i class="fa fa-eye"></i></a>
                                        @endif
                                    @endif
                                    @if (in_array(5,$permissoes_opcoes))
                                        @if ($marcacao->status == 2)
                                            
                                        @else
                                            <button type="button" class="btn btn-success btneditarmarcacao" id="btnedtmarcacao{{$marcacao->id}}" title="EDITAR"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-danger btnexcluirmarcacao" id="btnexcluirmarcacao{{$marcacao->id}}" title="EXCLUIR" ><i class=" fa fa-trash-o"></i></button>
                                        @endif
                                       
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    <tbody>
                </table>

            </div>
        </div>
    </div>
    
</div>
</div>

@include('sgi._modal_nova_marcacao')
@include('sgi._modal_editar_marcacao')

<script src="/js/jquery2.1.js"></script>
{!! HTML::script('js/sgi/inspecoesauditorias.js') !!}

@stop