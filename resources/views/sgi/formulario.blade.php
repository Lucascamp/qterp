@extends('layouts.master')

@section('menu')

@include('sgi.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2>SGI</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/sgi">SGI</a>
            </li>
            <li>
                <a href="/sgi/inspecoesauditorias">Inspeções e Auditorias</a>
            </li>
            @if (isset($formulario))
                <li>
                    <a href="/sgi/inspecoesauditorias/formularios">Formularios Preenchidos</a>
                </li>
            @endif
            <li class="active">
                <strong>Formulario {{$insp_aud->descricao}}</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <span id="titulo">Formulario {{$insp_aud->tipo}} - {{$insp_aud->descricao}}</span>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    
    <div class="ibox-content">
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="id_formulario" value="{{$id_formulario}}">

        @if (isset($marcacao))
            <input type="hidden" id="id_marcacao" value="{{$id_marcacao}}">
        @endif
        
        <div class="clear"><br></div>
        <table class="table table-bordered tabela">
            <tbody>
                <tr>
                    <td class="tbtitulo" >{{$insp_aud->tipo}} <br> {{$insp_aud->descricao}}</td>
                    <td colspan="7">Legenda:      (C) Conforme - (NC) Não Conforme - (NA) Não Aplicavel <br> (0) Não Evidenciado - (1) Parcialmente Evidenciado - (2) Evidenciado</td>
                </tr>
                <tr>
                    <td rowspan="4" colspan="2">
                        @if (isset($formulario))
                            {!! Form::text('Projeto', $formulario->projeto, array('id'=>'projeto' , 'class'=>'form-control' ,'placeholder' => 'Projeto','disabled')) !!}</br>    
                        @else
                            {!! Form::text('Projeto', null, array('id'=>'projeto' , 'class'=>'form-control' ,'placeholder' => 'Projeto')) !!}</br>
                        @endif
                        @if (isset($formulario))
                            {!! Form::text('local', $formulario->local, array('id'=>'local' , 'class'=>'form-control','placeholder' => 'Local', 'disabled')) !!}</br>    
                        @else
                            {!! Form::text('local', null, array('id'=>'local' , 'class'=>'form-control','placeholder' => 'Local')) !!}</br>
                        @endif

                        @if (isset($marcacao))
                            {!! Form::text('responsavel', $funcionarios[$marcacao->responsavel_id], array('id'=>'responsavel' , 'class'=>'form-control','placeholder' => 'Responsavel','disabled')) !!}</br>
                        @elseif (isset($formulario))
                            {!! Form::text('responsavel', $formulario->responsavel, array('id'=>'responsavel' , 'class'=>'form-control','placeholder' => 'Responsavel','disabled')) !!}</br>
                        @else
                            {!! Form::text('responsavel',null, array('id'=>'responsavel' , 'class'=>'form-control','placeholder' => 'Responsavel')) !!}</br>
                        @endif

                        @if (isset($formulario))
                            {!! Form::text('equipe',$formulario->equipe, array('id'=>'equipe' , 'class'=>'form-control','placeholder' => 'Equipe Auditada','disabled')) !!}
                        @else
                            {!! Form::text('equipe',null, array('id'=>'equipe' , 'class'=>'form-control','placeholder' => 'Equipe Auditada')) !!}
                        @endif
                        
                    </td>
                    <td rowspan="4">Verificado Por: 
                        @if (isset($formulario))
                            {!! Form::text('verificado_por', $formulario->verificado_por , array('id'=>'verificado_por' , 'class'=>'form-control' ,'disabled')) !!}</br>    
                        @else
                            {!! Form::text('verificado_por', null , array('id'=>'verificado_por' , 'class'=>'form-control')) !!}</br>    
                        @endif



                        @if (isset($marcacao))
                            {!! Form::select('unidade', $unidades,$marcacao->unidade_id,array('id' => 'unidade_insp_aud', 'size'=>'30', 'class'=>'chosen-select', 'disabled')) !!}
                        @elseif (isset($formulario))
                            {!! Form::select('unidade', $unidades,$formulario->unidade,array('id' => 'unidade_insp_aud', 'size'=>'30', 'class'=>'chosen-select', 'disabled')) !!}
                        @else
                            {!! Form::select('unidade', $unidades,0,array('id' => 'unidade_insp_aud', 'size'=>'30', 'class'=>'chosen-select')) !!}
                        @endif
                        
                    </td>

                    @if(isset($formulario))
                        <?php 
                            $data = substr($formulario->data, 0,10);
                        ?>
                        <td rowspan="4" colspan="2">data:<input type="date" class="form-control" id="data" disabled value="{{$data}}"></br></td>    
                    @else
                        <td rowspan="4" colspan="2">data:<input type="date" class="form-control" id="data" value=""></br></td>
                    @endif

                    @if(isset($formulario))
                        <td rowspan="4" colspan="3">Documentos Referencia: <textarea disabled rows="10" cols="40" id="documentos_referencia">{{$formulario->doc_referencia}}</textarea></td>
                    @else
                        <td rowspan="4" colspan="3">Documentos Referencia: <textarea rows="10" cols="40" id="documentos_referencia"></textarea></td>
                    @endif

                    
                </tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr class="tbtitulo">
                    <td>ITEM</td>
                    <td>INSPEÇÕES E VERIFICAÇÕES</td>
                    <td>CARACTERIZAÇÃO</td>
                    <td>DOCUMENTOS E EVIDENCIAS</td>
                    <td>N/A</td>
                    <td>NC</td>
                    <td>C</td>
                    <td>NOTAS / OBSERVAÇÕES</td>
                </tr>
                @foreach ($itens as $item)
                    <tr class="linhaItem" id="linhaItem{{$item->numero}}">
                        <td>{{$item->numero}}</td>
                        <td>{{$item->inspecao_verificacao}}</td>
                        <?php $caracterizacao = explode(';',  $item->caracterizacao); ?>
                        <td>
                            @foreach ($caracterizacao as $carac)
                                {{$carac}} <br>  
                            @endforeach
                        </td>
                        <?php $documentos_evidenciais = explode(';',  $item->documentos_evidenciais); ?>
                        <td>
                            @foreach ($documentos_evidenciais as $docs)
                                {{$docs}} <br>  
                            @endforeach
                        </td>
                        @if (isset($itens_formulario[$item->numero]->na))
                            <td class=""> {{$itens_formulario[$item->numero]->na}} </td>
                        @else
                            <td id="na{{$item->numero}}" class="na desmarcado"> </td>
                        @endif
                        
                        @if (isset($itens_formulario[$item->numero]->nc))
                            <td class=""> {{$itens_formulario[$item->numero]->nc}} </td>
                        @else
                            <td id="nc{{$item->numero}}" class="nc desmarcado"> </td>
                        @endif

                        @if (isset($itens_formulario[$item->numero]->c))
                            <td class=""> {{$itens_formulario[$item->numero]->c}} </td>
                        @else
                             <td id="c{{$item->numero}}" class="c desmarcado">   </td>
                        @endif

                        @if (isset($itens_formulario[$item->numero]->nota))
                            <td><textarea rows="3" disabled cols="30">{{$itens_formulario[$item->numero]->nota}} </textarea></td>
                        @else
                            <td><textarea rows="3" cols="30" class="notas" id="nota{{$item->numero}}"></textarea></td>
                        @endif

                        
                       
                        
                    </tr>
                @endforeach
            </tbody> 
        </table>
        @if (!isset($formulario))
            <button type="button" class="btn btn-primary" id="bt_salvar_formulario_unidade" title="Salvar"><i class="fa fa-save"></i> Salvar</button>    
        @endif
    </div>
    </div>
</div>
</div>


<script src="/js/jquery2.1.js"></script>
{!! HTML::script('js/sgi/inspecoesauditorias.js') !!}

@stop