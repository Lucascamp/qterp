@extends('layouts.master')

@section('menu')

@include('sgi.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>SGI</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/sgi">SGI</a>
            </li>
            <li>
                <a href="/sgi/inspecoesauditorias">Inspeções Auditoria</a>
            </li>
            <li class="active">
                <strong>Formularios Preenchidos</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <span id="titulo">Formularios de Inspeções e Auditorias Preenchidos</span>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
        <div class="ibox-content">
            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <div class="clear"><br></div>
            <div class="row">
                @if (in_array(5,$permissoes_opcoes))
                    <div class="col-lg-3">
                        <legend>Unidade</legend>
                        {!! Form::select('unidade', $unidades, null,array('id' => 'filtro_unidade', 'size'=>'30', 'class'=>'chosen-select')) !!}
                    </div>
                @else
                    <input type="hidden" id="filtro_unidade" value="{{ $unidade_id_user }}">
                @endif
                <div class="col-lg-3">
                    <legend>Data Inicio</legend>
                    <input type="date" class="form-control" id="data_ini">
                </div>
                <div class="col-lg-3">
                    <legend>Data Fim</legend>
                    <input type="date" class="form-control" id="data_fim" value="">
                </div>
            </div>
            <br>
            <div class="row">
                <table class="table table-bordered tabela">
                    <thead>
                        <th>Unidade</th>
                        <th>Inspeção - Auditoria</th>
                        <th>Data</th>
                        <th>Opções</th>
                    </thead>
                    <tbody id="tabela_formularios_preenchidos">
                        @foreach ($insp_aud as $item)
                            <tr>
                                <td>{{isset($unidades[$item->unidade]) ? $unidades[$item->unidade] : '-'}}</td>
                                <td>{{$insp_aud_nomes[$item->insp_aud_id]}}</td>
                                <?php 
                                    $data = substr($item->data, 0,10);
                                    $data = explode('-', $data);
                                    $data = $data[2].'-'.$data[1].'-'.$data[0];
                                 ?>
                                <td>{{$data}}</td>
                                <td>
                                    <a href="/sgi/formulariop/{{$item->id}}" class="btn btn-info" title="VISUALIZAR"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    <tbody>
                </table>

            </div>
        </div>
    </div>
    
</div>
</div>


<script src="/js/jquery2.1.js"></script>
{!! HTML::script('js/sgi/inspecoesauditorias.js') !!}

@stop