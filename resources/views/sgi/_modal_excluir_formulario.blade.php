<div class="modal fade" id="modalexcluirFormulario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Cadastro de Formulario Inspeção e Auditoria</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        
        {!! Form::hidden('created_by', $id_user) !!}
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        
        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Tipo(*)</td>
                <td colspan="3">
                    <select class="form-control" id="tipo">
                        <option value="0">SELECIONE</option>
                        <option value="INSPECAO">INSPEÇÃO</option>
                        <option value="AUDITORIA">AUDITORIA</option>
                    </select>
                </td>
            </tr>
            <tr>    
                <td class="tblabel">Descrição(*)</td>
                <td>
                    {!! Form::text('descricao', null, array('id'=>'descricao' , 'class'=>'form-control')) !!}
                </td>
            </tr>
        </table>
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="bt_salvar_formulario" >Salvar</button>

        
    </div>
</div>
</div>
</div>