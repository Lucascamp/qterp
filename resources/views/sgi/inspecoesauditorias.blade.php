@extends('layouts.master')

@section('menu')

@include('sgi.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>SGI</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/sgi">SGI</a>
            </li>
            <li class="active">
                <strong>Inspeções Auditorias</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            @if (in_array(5,$permissoes_opcoes))  
                <button type="button" data-target="#modalNovoFormulario" data-toggle="modal" data-tooltip="tooltip" class="btn btn-primary">Novo Formulario</button>
            @endif
                <a href="/sgi/inspecoesauditorias/formularios" class="btn btn-primary" title="FORMULARIOS"><i class="fa fa-file-text"></i> Formularios Preenchidos</a>
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <span id="titulo">Formularios de Inspeções e Auditorias Cadastrados</span>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        <div class="clear"><br></div>
        <table class="table table-bordered tabela" id="tabela_insp_aud">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Descrição</th>
                    <th>Data Criação</th>
                    <th>Data Atualização</th>
                    <th WIDTH="275">Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($inspe_aud as $element)
                    <tr> 
                        <td>{{$element->tipo}}</td>
                        <td>{{$element->descricao}}</td>
                        <td>{{$element->created_at}}</td>
                        <td>{{$element->updated_at}}</td>
                        <td>
                            <button type="button" class="btn btn-info btnviewformulario" id="btnview{{$element->id}}" title="PREENCHER"><i class="fa fa-file-text-o"></i></button>
                            @if (in_array(5,$permissoes_opcoes))
                                <button type="button" class="btn btn-success btneditarformulario" id="btnedt{{$element->id}}" title="EDITAR"><i class="fa fa-pencil"></i></button>
                                <button type="button" class="btn btn-danger btnexcluirformulario" id="btnexc{{$element->id}}" title="EXCLUIR" ><i class=" fa fa-trash-o"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach 
            </tbody> 
        </table>
    </div>
</div>
</div>

@include('sgi._modal_novo_formulario')
@include('sgi._modal_editar_formulario')
@include('sgi._modal_excluir_formulario')


<script src="/js/jquery2.1.js"></script>
{!! HTML::script('js/sgi/inspecoesauditorias.js') !!}

@stop