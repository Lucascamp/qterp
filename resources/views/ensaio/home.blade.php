@extends('layouts.master')

@section('menu')
@include('ensaio.menuensaio')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Gamagrafia</strong>
            </li>
        </ol>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="text-center animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <h3 class="font-bold">Módulo Ensaio</h3>
                    <div class="error-desc">
                        @foreach ($areas as $area)
                            <a href="{{$area['rota']}}" class="btn btn-primary m-t">{{$area['descricao']}}</a>
                        @endforeach

                        @if (Config::get('app.atualizavel'))
                            <br>
                            <br>
                            <br>
                            <h3 class="font-bold">Sincronizações</h3>
                            <a href="#" class="btn btn-primary m-t" id="bt_sinc_gamagrafia">Sincronizar Gamagrafia</a>
                            <a href="#" class="btn btn-primary m-t" id="bt_sinc_fontes">Sincronizar Fontes</a>
                        @endif
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
