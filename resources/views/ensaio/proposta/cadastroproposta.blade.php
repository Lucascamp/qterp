@extends('layouts.master')

@section('menu')
@include('ensaio.menuensaio')
@stop


@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Propostas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/proposta">Propostas</a>
            </li>
            <li class="active">
                <strong>Cadastro de Proposta</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Propostas Comerciais <small>(*)campos obrigatorios</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="get" class="form-inline">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
        <table class="table table-bordered tabela" id="editable">
            <thead>
            </thead>
            <tbody>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo" colspan="8">Proposta</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Cod Proposta(*)</td>
                        <td class="tblabel"><input type="text" class=" tbinput form-control" id="codproposta"></td>
                        <td class="tblabel">Data(*)</td>
                        <td class="tblabel"><input type="date" class="form-control" id="data"></td>
                        <td class="tblabel">Validade(*)</td>
                        <td class="tblabel"><input type="text" class="form-control tbinput" id="validade" placeholder="Em Dias"></td>
                        <td class="tblabel">Cod Proposta Anterior</td>
                        <td class="tblabel"><input type="text" class="form-control  tbinput" id="propostaantiga"></td>
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="8">Cliente</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Cliente(*)</td>
                        <td class="tblabel">
                            <Select class="form-control chosen-select" id="cliente">
                                <option value="0">Selecione o cliente</option> 
                                @foreach ($clientes as $cliente)
                                    <option value="{{$cliente['id']}}">{{$cliente['nome']}}</option>     
                                @endforeach
                                
                                
                            </select>
                        </td>
                        <td class="tblabel">Nome reduzido</td>
                        <td class="tbitem tblabel" ><p id="nome_reduz"></p></td>
                        <td class="tblabel">Estado</td>
                        <td class="tbitem tblabel"><p id="estado"></p></td>
                        <td class="tblabel">Município</td>
                        <td class="tbitem tblabel"><p id="municipio"></p></td>
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="6">Projeto</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Projeto</td>
                        <td class="tblabel"><input type="text" id="servico" class="form-control"></td>
                        <td class="tblabel">Estado</td>
                        <td class="tblabel">
                            <select id="serEstado" class="form-control chosen-select">
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas </option>
                                <option value="AP">Amapá </option>
                                <option value="AM">Amazonas </option>
                                <option value="BA">Bahia </option>
                                <option value="CE">Ceará </option>
                                <option value="DF">Distrito Federal </option>
                                <option value="ES">Espírito Santo </option>
                                <option value="GO">Goiás </option>
                                <option value="MA">Maranhão </option>
                                <option value="MT">Mato Grosso </option>
                                <option value="MS">Mato Grosso do Sul </option>
                                <option value="MG">Minas Gerais </option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba </option>
                                <option value="PR">Paraná </option>
                                <option value="PE">Pernambuco </option>
                                <option value="PI">Piauí </option>
                                <option value="RJ">Rio de Janeiro </option>
                                <option value="RN">Rio Grande do Norte </option>
                                <option value="RS">Rio Grande do Sul </option>
                                <option value="RO">Rondônia </option>
                                <option value="RR">Roraima </option>
                                <option value="SC">Santa Catarina </option>
                                <option value="SP">São Paulo </option>
                                <option value="SE">Sergipe </option>
                                <option value="TO">Tocantins </option>
                            </select>
                        </td>
                        <td class="tblabel">Municipio</td>
                        <td class="tblabel"><input type="text" id="serMunicipio" class="form-control"></td>
                    </tr>
                    <tr>
                        <td class="tbsubtitulo" colspan="2">Solicitação</td>
                        <td class="tbsubtitulo" colspan="2">Aprovação</td>
                        <td class="tbsubtitulo" colspan="2">Projeto</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Contato</td>
                        <td class="tblabel"><input type="text" id="contatosoli" class="form-control tbinputEM"></td>
                        <td class="tblabel">Contato</td>
                        <td class="tblabel"><input type="text" id="contatoapro" class="form-control tbinputEM"></td>
                        <td class="tblabel">Contato</td>
                        <td class="tblabel"><input type="text" id="contatopro" class="form-control tbinputEM"></td>
                    </tr>
                    <tr>
                        <td class="tblabel">Email</td>
                        <td class="tblabel"><input type="text" id="emailsoli" class="form-control tbinputEM"></td>
                        <td class="tblabel">Email</td>
                        <td class="tblabel"><input type="text" id="emailapro" class="form-control tbinputEM"></td>
                        <td class="tblabel">Email</td>
                        <td class="tblabel"><input type="text" id="emailpro" class="form-control tbinputEM"></td>
                    </tr>
                    <tr>
                        <td class="tblabel">Telefone</td>
                        <td class="tblabel"><input type="text" id="telsoli" class="form-control mask-telefone tbinputEM"></td>
                        <td class="tblabel">Telefone</td>
                        <td class="tblabel"><input type="text" id="telapro" class="form-control mask-telefone tbinputEM"></td>
                        <td class="tblabel">Telefone</td>
                        <td class="tblabel"><input type="text" id="telpro" class="form-control mask-telefone tbinputEM"></td>
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="2">Forma de Cobrança</td>
                    </tr>
                    <tr>
                        <td class="tbsubtitulo">Radiografias feitas na Qualitec</td>
                        <td class="tbsubtitulo">Radiografias feitas no Cliente</td>
                    </tr>
                    <tr>
                        <td class="tblabel">
                            <div class="checkbox-inline">
                                <input type="checkbox" class="check" value="diaQ" id="diaQ">
                                <label for="diaQ">
                                    Diaria
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <input type="checkbox" class="check"  value="filmeQ" id="filmeQ">
                                <label for="filmeQ">
                                    Filme
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <input type="checkbox" class="check" value="juntaQ" id="juntaQ">
                                <label for="juntaQ">
                                    Junta
                                </label>
                            </div>
                        </td>
                        <td class="tblabel">
                            <div class="checkbox-inline">
                                <input type="checkbox" class="check" value="diaC" id="diaC">
                                <label for="diaC">
                                    Diaria
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <input type="checkbox" class="check" value="filmeC" id="filmeC">
                                <label for="filmeC">
                                    Filme
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                <input type="checkbox" class="check" value="juntaC" id="juntaC">
                                <label for="juntaC">
                                    Junta
                                </label>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="3">Jornada</td>
                    </tr>
                    <tr>
                        <td class="tbsubtitulo">Jornadas</td>
                        <td class="tbsubtitulo">Valores Horas Normais</td>
                        <td class="tbsubtitulo">Valores Horas Extras</td>
                    </tr>
                    @foreach ($jornadas as $jornada)
                        <tr>    
                            <td class="tblabel">{{$jornada['descricao']}}</td>
                            <td class="tblabel">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">R$</span><input type="text" class="jornadanormal tbinput form-control" id="normal{{$jornada['id']}}">
                                </div>
                            </td>
                            <td class="tblabel">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">R$</span><input type="text" class="jornadaextra tbinput form-control" id="extra{{$jornada['id']}}">
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo"  colspan="8">Filmes</td>
                    </tr>
                    <tr>
                        <td class="tblabel" colspan="4">Filme de Reparo</td>
                        <td colspan="4"><div class="input-group m-b"><span class="input-group-addon">R$</span><input type="text" id="filmedereparo" class="form-control"></div></td>
                    </tr>
                    <?php 
                        $cont=0;
                        $tam = count($filmes)
                    ?>
                    <tr>
                        <td class="tbtitulo"  colspan="8">Filmes Classe I</td>
                    </tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($filmes[$i]['classe'] == 0)
                            @if($cont >= 4)
                                <?php $cont = 0; ?>
                                <tr>
                            @endif
                                <td  class="tblabel">{{$filmes[$i]['descricao']}}</td>
                                <td class="tblabel"><div class="input-group m-b"><span class="input-group-addon">R$</span><input type="text" id="filme{{$filmes[$i]['id']}}"  class="filme tbinput form-control"></div></td>
                            @if($cont >= 4)
                                </tr>
                            @endif
                            <?php $cont++ ?>
                        @endif
                    @endfor 
                    </tr>
                    <?php 
                        $cont=0;
                        $tam = count($filmes)
                    ?>
                    <tr>
                        <td class="tbtitulo"  colspan="8">Filmes Classe II</td>
                    </tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($filmes[$i]['classe'] == 1)
                            @if($cont >= 4)
                                <?php $cont = 0; ?>
                                <tr>
                            @endif
                                <td  class="tblabel">{{$filmes[$i]['descricao']}}</td>
                                <td class="tblabel"><div class="input-group m-b"><span class="input-group-addon">R$</span><input type="text" id="filme{{$filmes[$i]['id']}}"  class="filme tbinput form-control"></div></td>
                            @if($cont >= 4)
                                </tr>
                            @endif
                            <?php $cont++ ?>
                        @endif
                    @endfor 
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo"  colspan="8">Juntas</td>
                    </tr>
                   {{--  <tr>
                        <td class="tblabel" colspan="4">Valor por Chapa</td>
                        <td colspan="4" ><div class="input-group m-b"><span class="input-group-addon">R$</span><input type="text" id="valorchapa" class="form-control"></div></td>
                    </tr> --}}
                    <?php 
                        $cont=0;
                        $tam = count($juntas)
                    ?>
                    <tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($cont >= 4)
                            <?php $cont = 0; ?>
                            <tr>
                        @endif
                            <td  class="tblabel">{{$juntas[$i]['descricao']}}</td>
                            <td class="tblabel"><div class="input-group m-b"><span class="input-group-addon">R$</span><input type="text" id="junta{{$juntas[$i]['id']}}" class="junta tbinput form-control"></div></td>
                        @if($cont >= 4)
                            </tr>
                        @endif
                        <?php $cont++ ?>
                    @endfor 
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo"  colspan="8">Outros</td>
                    </tr>
                    <tr>
                        <?php 
                        $cont=0;
                        $tam = count($servicos)
                    ?>
                    <tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($cont >= 4)
                            <?php $cont = 0; ?>
                            <tr>
                        @endif
                            <td  class="tblabel">{{$servicos[$i]['descricao']}}</td>
                            <td class="tblabel"><div class="input-group m-b"><span class="input-group-addon">R$</span><input type="text" id="servico{{$servicos[$i]['id']}}" class="servico tbinput form-control"></div></td>
                        @if($cont >= 4)
                            </tr>
                        @endif
                        <?php $cont++ ?>
                    @endfor
                    </tr>
                    <tr>
                        <td  class="tblabel">ISS</td>
                            <td class="tblabel"><div class="input-group m-b"><span class="input-group-addon">%</span><input type="text" id="iss" class="tbinput form-control"></div></td>
                    </tr>
                </table>
            </tbody>
        </table>
                    </form>
                    <a class="btn btn-primary" id="bt_salvar_proposta">Salvar</a> 
                </div>
            </div>
        
        </div>
    </div>
</div>
</br>
                </br>

@stop
