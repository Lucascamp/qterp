@extends('layouts.master')

@section('menu')
@include('ensaio.menuensaio')
@stop


@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Edição de Proposta</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/proposta">Propostas</a>
            </li>
            <li class="active">
                <strong>Edição de Proposta</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Proposta:  {{$proposta['cod_proposta']}} </h5>
                    <div class="ibox-tools">
                        <small>(*)campos obrigatorios</small>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="get" class="form-inline">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" id="id_proposta" value="{{ $proposta['id'] }}">
                        <input type="hidden" id="cliente_id" value="{{ $proposta['cliente_id'] }}">
        <table class="table table-bordered tabela" id="editable">
            <thead>
            </thead>
            <tbody>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo" colspan="8">Proposta</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Cod Proposta(*)</td>
                        <td class="tblabel"><input type="text" class="tbinput form-control" id="codproposta" value="{{$proposta['cod_proposta']}}"></td>
                        <td class="tblabel">Data(*)</td>
                        <td class="tblabel"><input type="date" class=" tblabel form-control" id="data" value="{{$proposta['data']}}"></td>
                        <td class="tblabel">Validade(*)</td>
                        <td class="tblabel"><input type="text" class="form-control tbinput" id="validade" placeholder="Em Dias" value="{{$proposta['validade']}}"></td>
                        <td class="tblabel">Cod Proposta Anterior</td>
                        <td class="tblabel"><input type="text" class="form-control tbinput" id="propostaantiga" value="{{$proposta['cod_proposta_anterior']}}"></td>
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="8">Cliente</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Cliente(*)</td>
                        <td class="tblabel">
                            <Select class="form-control" id="cliente">
                                <option>Selecione o cliente</option> 
                                @foreach ($clientes as $cliente)
                                    <option value="{{$cliente['id']}}">{{$cliente['nome']}}</option>     
                                @endforeach
                            </select>
                        </td>
                        <td class="tblabel">Nome reduzido</td>
                        <td class="tbitem tblabel" ><B id="nome_reduz"></B></td>
                        <td class="tblabel">Estado</td>
                        <td class="tbitem tblabel"><b id="estado"></b></td>
                        <td class="tblabel">Município</td>
                        <td class="tbitem tblabel"><b id="municipio"></b></td>
                    </tr>
                </table>
                <?php if(count($projetos) == 1){ ?>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="6">Projeto</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Serviço</td>
                        <td class="tblabel" ><input type="text" id="servico" class="form-control" value="{{$projetos[0]['servico']}}"></td>
                        <td class="tblabel">Estado</td>
                        <td class="tblabel" ><input type="text" id="serEstado" class="form-control" value="{{$projetos[0]['estado']}}"></td>
                        <td class="tblabel">Municipio</td>
                        <td class="tblabel" ><input type="text" id="serMunicipio" class="form-control" value="{{$projetos[0]['municipio']}}"></td>
                    </tr>
                    <tr>
                        <td class="tbsubtitulo" colspan="2">Solicitação</td>
                        <td class="tbsubtitulo" colspan="2">Aprovação</td>
                        <td class="tbsubtitulo" colspan="2">Projeto</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Contato</td>
                        <td class="tblabel"><input type="text" id="contatosoli" class="form-control tbinputEM" value="{{$projetos[0]['solicitacao_contato']}}"></td>
                        <td class="tblabel">Contato</td>
                        <td class="tblabel"><input type="text" id="contatoapro" class="form-control tbinputEM " value="{{$projetos[0]['aprovacao_contato']}}"></td>
                        <td class="tblabel">Contato</td>
                        <td class="tblabel"><input type="text" id="contatopro" class="form-control tbinputEM" value="{{$projetos[0]['projeto_contato']}}"></td>
                    </tr>
                    <tr>
                        <td class="tblabel">Email</td>
                        <td class="tblabel"><input type="text" id="emailsoli" class="form-control tbinputEM" value="{{$projetos[0]['solicitacao_email']}}"></td>
                        <td class="tblabel">Email</td>
                        <td class="tblabel"><input type="text" id="emailapro" class="form-control tbinputEM" value="{{$projetos[0]['aprovacao_email']}}"></td>
                        <td class="tblabel">Email</td>
                        <td class="tblabel"><input type="text" id="emailpro" class="form-control tbinputEM" value="{{$projetos[0]['projeto_email']}}"></td>
                    </tr>
                    <tr>
                        <td class="tblabel">Telefone</td>
                        <td class="tblabel"><input type="text" id="telsoli" class="mask-telefone form-control tbinputEM" value="{{$projetos[0]['solicitacao_tel']}}"></td>
                        <td class="tblabel">Telefone</td>
                        <td class="tblabel"><input type="text" id="telapro" class="mask-telefone form-control tbinputEM" value="{{$projetos[0]['aprovacao_tel']}}"></td>
                        <td class="tblabel">Telefone</td>
                        <td class="tblabel"><input type="text" id="telpro" class="mask-telefone form-control tbinputEM" value="{{$projetos[0]['projeto_tel']}}"></td>
                    </tr>
                </table>
                <?php }?>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="2">Forma de Cobrança</td>
                    </tr>
                    <tr>
                        <td class="tbsubtitulo">Radiografias feitas na Qualitec</td>
                        <td class="tbsubtitulo">Radiografias feitas no Cliente</td>
                    </tr>
                    <tr>
                        <td class="tblabel">
                            <div class="checkbox-inline">
                                @if (in_array('diaQ', $formas_cobranca))
                                    <input type="checkbox" class="check" checked value="diaQ" id="diaQ">
                                @else
                                        <input type="checkbox" class="check" value="diaQ" id="diaQ">
                                @endif
                                <label for="diaQ">
                                    Diaria
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                 @if (in_array('filmeQ', $formas_cobranca))
                                    <input type="checkbox" class="check" checked value="filmeQ" id="filmeQ">
                                 @else
                                    <input type="checkbox" class="check"  value="filmeQ" id="filmeQ">
                                 @endif
                                
                                <label for="filmeQ">
                                    Filme
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                @if (in_array('juntaQ', $formas_cobranca))
                                    <input type="checkbox" class="check" checked value="juntaQ" id="juntaQ">
                                 @else
                                    <input type="checkbox" class="check" value="juntaQ" id="juntaQ">
                                 @endif
                                
                                <label for="juntaQ">
                                    Junta
                                </label>
                            </div>
                        </td>
                        <td class="tblabel">
                            <div class="checkbox-inline">
                                @if (in_array('diaC', $formas_cobranca))
                                <input type="checkbox" class="check" checked value="diaC" id="diaC">
                                 @else
                                 <input type="checkbox" class="check" value="diaC" id="diaC">
                                 @endif
                                
                                <label for="diaC">
                                    Diaria
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                @if (in_array('filmeC', $formas_cobranca))
                                <input type="checkbox" class="check" checked value="filmeC" id="filmeC">
                                 @else
                                 <input type="checkbox" class="check" value="filmeC" id="filmeC">
                                 @endif
                                
                                <label for="filmeC">
                                    Filme
                                </label>
                            </div>
                            <div class="checkbox-inline">
                                @if (in_array('juntaC', $formas_cobranca))
                                <input type="checkbox" class="check" checked value="juntaC" id="juntaC">
                                 @else
                                 <input type="checkbox" class="check" value="juntaC" id="juntaC">
                                 @endif
                                
                                <label for="juntaC">
                                    Junta
                                </label>
                            </div>
                        </td>
                    </tr>
                </table>

                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="3">Jornada</td>
                    </tr>
                    <tr>
                        <td class="tbsubtitulo">Jornadas</td>
                        <td class="tbsubtitulo">Valores Horas Normais</td>
                        <td class="tbsubtitulo">Valores Horas Extras</td>
                    </tr>
                    @foreach ($jornadas as $jornada)
                        <tr>    
                            <td class="tblabel">{{$jornada['descricao']}}</td>
                            <td class="tblabel">
                                <div class="input-group m-b tblabel">
                                    <span class="input-group-addon tblabel">R$</span>
                                    @if(isset($prop_jornadas[$jornada['id']]))
                                        <input type="text" class="jornadanormal tblabel tbinput form-control" id="normal{{$jornada['id']}}" value="{{$prop_jornadas[$jornada['id']]['valor_normal']}}">
                                    @else
                                        <input type="text" class="jornadanormal tblabel tbinput form-control" id="normal{{$jornada['id']}}" value="0">
                                    @endif
                                </div>
                            </td>
                            <td class="tblabel">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">R$</span>
                                    @if(isset($prop_jornadas[$jornada['id']]))
                                        <input type="text" class="jornadaextra tbinput form-control" id="extra{{$jornada['id']}}" value="{{$prop_jornadas[$jornada['id']]['valor_extra']}}">
                                    @else
                                        <input type="text" class="jornadaextra tbinput form-control" id="extra{{$jornada['id']}}" value="0">
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo"  colspan="8">Filmes</td>
                    </tr>
                    <tr>
                        <td class="tblabel" colspan="4">Filme de Reparo</td>
                        <td class="tblabel" colspan="4"><div class="input-group m-b"><span class="input-group-addon">R$</span><input type="text" id="filmedereparo"  value="{{$proposta['filmedereparo']}}" class="form-control"></div></td>
                    </tr>
                    <?php 
                        $cont=0;
                        $tam = count($filmes)
                    ?>
                    <tr>
                    <tr>
                        <td class="tbtitulo"  colspan="8">Filmes Classe I</td>
                    </tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($filmes[$i]['classe'] == 0)
                            @if($cont >= 4)
                                <?php $cont = 0; ?>
                                <tr>
                            @endif
                                <td  class="tblabel">{{$filmes[$i]['descricao']}}</td>
                                <td >
                                    <div class="input-group m-b">
                                        <span class="input-group-addon">R$</span>
                                        @if(isset($prop_filmes[$filmes[$i]['id']]))
                                            <input type="text" id="filme{{$filmes[$i]['id']}}" class="filme tbinput form-control" value="{{$prop_filmes[$filmes[$i]['id']]['valor']}}">
                                        @else
                                            <input type="text" id="filme{{$filmes[$i]['id']}}" class="filme tbinput form-control" value="0">
                                        @endif
                                    </div>
                                </td>
                            @if($cont >= 4)
                                </tr>
                            @endif
                            <?php $cont++ ?>
                        @endif
                    @endfor 
                    </tr>
                    <?php 
                        $cont=0;
                        $tam = count($filmes)
                    ?>
                    <tr>
                        <td class="tbtitulo"  colspan="8">Filmes Classe II</td>
                    </tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($filmes[$i]['classe'] == 1)
                            @if($cont >= 4)
                                <?php $cont = 0; ?>
                                <tr>
                            @endif
                                <td  class="tblabel">{{$filmes[$i]['descricao']}}</td>
                                <td >
                                    <div class="input-group m-b">
                                        <span class="input-group-addon">R$</span>
                                        @if(isset($prop_filmes[$filmes[$i]['id']]))
                                            <input type="text" id="filme{{$filmes[$i]['id']}}" class="filme tbinput form-control" value="{{$prop_filmes[$filmes[$i]['id']]['valor']}}">
                                        @else
                                            <input type="text" id="filme{{$filmes[$i]['id']}}" class="filme tbinput form-control" value="0">
                                        @endif
                                    </div>
                                </td>
                            @if($cont >= 4)
                                </tr>
                            @endif
                            <?php $cont++ ?>
                        @endif
                    @endfor 
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo"  colspan="8">Juntas</td>
                    </tr>
                   
                    <?php 
                        $cont=0;
                        $tam = count($juntas)
                    ?>
                    <tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($cont >= 4)
                            <?php $cont = 0; ?>
                            <tr>
                        @endif
                            <td  class="tblabel">{{$juntas[$i]['descricao']}}</td>
                            <td >
                                <div class="input-group m-b">
                                    <span class="input-group-addon">R$</span>
                                    @if(isset($prop_juntas[$juntas[$i]['id']]))
                                        <input type="text" id="junta{{$juntas[$i]['id']}}" class="junta tbinput form-control" value="{{$prop_juntas[$juntas[$i]['id']]['valor']}}">
                                    @else
                                         <input type="text" id="junta{{$juntas[$i]['id']}}" class="junta tbinput form-control" value="0">
                                    @endif
                                </div>
                            </td>
                        @if($cont >= 4)
                            </tr>
                        @endif
                        <?php $cont++ ?>
                    @endfor 
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo"  colspan="8">Outros</td>
                    </tr>
                    <tr>
                        <?php 
                        $cont=0;
                        $tam = count($servicos)
                    ?>
                    <tr>
                    @for ($i = 0; $i < $tam; $i++)
                        @if($cont >= 4)
                            <?php $cont = 0; ?>
                            <tr>
                        @endif
                            <td  class="tblabel">{{$servicos[$i]['descricao']}}</td>
                            <td >
                                <div class="input-group m-b">
                                    <span class="input-group-addon">R$</span>
                                    @if(isset($prop_servicos[$servicos[$i]['id']]))
                                        <input type="text" id="servico{{$servicos[$i]['id']}}" class="servico tbinput form-control" value="{{$prop_servicos[$servicos[$i]['id']]['valor']}}">
                                    @else
                                        <input type="text" id="servico{{$servicos[$i]['id']}}" class="servico tbinput form-control" value="0">
                                    @endif
                                </div>
                            </td>
                        @if($cont >= 4)
                            </tr>
                        @endif
                        <?php $cont++ ?>
                    @endfor 
                    </tr>
                     <tr>
                        <td  class="tblabel">ISS</td>
                            <td class="tblabel"><div class="input-group m-b"><span class="input-group-addon">%</span>
                                @if(isset($proposta['iss']))
                                        <input type="text" id="iss" class="tbinput form-control" value="{{$proposta['iss']}}">
                                    @else
                                        <input type="text" id="iss" class="tbinput form-control">
                                    @endif
                            </div></td>
                    </tr>
                </table>
            </tbody>
        </table>
                    </form>
                    <a class="btn btn-primary" id="bt_salvar_proposta">Salvar</a> 
                </div>
            </div>
       
        </div>
    </div>
</div>
</br>
                </br>

@stop
