@extends('layouts.master')

@section('menu')
@include('ensaio.menuensaio')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Propostas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li class="active">
                <strong>Propostas</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="/proposta/cadastrar" class="btn btn-primary">Nova Proposta</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Propostas </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <div id="mensagens">
                        </div>
                        <table class="table table-bordered tabela">
                            <thead>
                            </thead>
                            <tbody>
                                <table class="table table-bordered tabela" >
                                    <thead>
                                        <tr>
                                            <th class="tbtitulo" colspan="6">Propostas existentes</td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td class="tbsubtitulo">Cod Proposta</td>
                                        <td class="tbsubtitulo">Cliente</td>
                                        <td class="tbsubtitulo">Data</td>
                                        <td class="tbsubtitulo">Validade</td>
                                        <td class="tbsubtitulo">Ultima Atualização</td>
                                        <td class="tbsubtitulo">Opções</td>
                                    </tr>
                                    <tbody id="listaPropostasH">
                                        @if (count($propostas) > 0)
                                            @foreach ($propostas as $proposta)
                                            <tr>
                                                <td class="tblabel">{{$proposta->cod_proposta}}</td>
                                                <td class="tblabel">{{$clientes[$proposta->cliente_id]}}</td>
                                                <td class="tblabel">{{$proposta->data}}</td>
                                                <td class="tblabel">{{$proposta->validade}}</td>
                                                <td class="tblabel">{{$proposta->updated_at}}</td>
                                                <td class="tblabel"><a href="/proposta/editarproposta/{{$proposta->id}}">Editar</a> | excluir</td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <td colspan="6" class="tblabel" >Nenhuma Proposta Cadastrada</td>
                                        @endif
                                    </tbody>
                                </table>
                            </tbody>
                        </table>
                        </br>                                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
