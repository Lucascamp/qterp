@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Procedimentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/procedimento">Procedimentos</a>
            </li>
            <li class="active">
                <strong>Cadastrar</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <button class="btn btn-primary" id="bt_novo_procedimento">Novo Procedimento</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Procedimentos </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <div id="mensagens">
                        </div>
                            <table class="table table-bordered table_procedimento" >
                                <tr>
                                    <th class="tbtitulo" colspan="14">Cadastro de Procedimento</td>
                                </tr>
                                <tr>
                                    <table class="table table-bordered table_procedimento" >
                                        <tr>
                                            <td>PROCEDIMENTO</td>
                                            <td>NORMA</td>
                                            <td>CRITERIO DE ACEITE</td>
                                            <td>OBJETO</td>
                                            <td>APLICAÇÃO</td>
                                            <td>METAL BASE</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select type="text" class="form-control" id="procedimento">
                                                    <option value="0" >Selecione</option>
                                                    @foreach ($procedimentos as $key => $element)
                                                        <option value="{{$key}}">{{$element}}</option>
                                                    @endforeach
                                                </select>
                                            </td>                                            
                                            <td><input type="text" class="form-control" id="norma"></td>
                                            <td><input type="text" class="form-control" id="criterio_aceite"></td>
                                            <td><input type="text" class="form-control" id="objeto"></td>
                                            <td><input type="text" class="form-control" id="aplicacao"></td>
                                            <td><input type="text" class="form-control" id="metal_base"></td>
                                            
                                        </tr>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="table table-bordered table_procedimento">
                                        <tr>
                                            <td>TECNICA</td>
                                            
                                            <td>DFF</td>
                                            <td>TIPO IQI</td>
                                            <td>FIO IQI</td>
                                            <td colspan="2">CLASSE FILME</td>
                                            <td>QUANT FILMES</td>
                                            <td colspan="2">DIMENSÃO FILME</td>
                                            <td>REFORÇO</td>
                                            <td colspan="2">DIAMETRO</td>
                                            <td colspan="2">ESPESSURA</td>
                                            <td>FONTE DE RADIAÇÃO</td>
                                            <td colspan="2">FOCO FONTE</td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" id="tecnica"></td>
                                            
                                            <td><input type="text" class=" input_numero form-control" id="dff"></td>
                                            <td><input type="text" class="form-control" id="tipo_iqi"></td>
                                            <td><input type="text" class="input_numero form-control" id="fio_iqi"></td>
                                            <td colspan="2">
                                                <select class="form-control" id="classe_filme">
                                                    <option>I</option>
                                                    <option>Esp</option>
                                                    <option>II</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class="input_numero form-control" id="quantidade_filme"></td>
                                            <td colspan="2">
                                                <select class="form-control" id="dimensao">
                                                    <option>Dimensões</option>
                                                    @foreach ($dimensoes as $dimensao)
                                                        <option value="{{$dimensao[0]->legenda}}">{{$dimensao[0]->descricao}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control input_numero" id="reforco"></td>
                                            <td><input type="text" class="input_numero form-control" id="diametro_ini"></td>
                                            <td><input type="text" class="input_numero form-control" id="diametro_fim"></td>
                                            <td><input type="text" class="input_numero form-control" id="espessura_ini"></td>
                                            <td><input type="text" class="input_numero form-control" id="espessura_fim"></td>
                                            <td>
                                                <select class=" input_select form-control" id="fonte_radiacao">
                                                    <option>GAMA</option>
                                                    <option>RAIO X</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class=" input_numero form-control" id="foco_fonte_ini"></td>
                                            <td><input type="text" class="input_numero form-control" id="foco_fonte_fim"></td>
                                            
                                        </tr>
                                    </table>
                                </tr>
                            </table>
                        </br>   
                        <a class="btn btn-primary" id="bt_salvar_procedimento">Cadastrar Procedimento</a>                          
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</div>
</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom">Modal title</h4>
      </div>
      <div class="modal-body" id="bodym">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
<script src="/js/jquery2.1.js"></script>
<script src="/js/jquery-ui.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/procedimentos.js"></script>
@stop
