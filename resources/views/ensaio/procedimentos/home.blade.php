@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Procedimentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li class="active">
                <strong>Procedimentos</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Procedimentos </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <div id="mensagens">
                        </div>

                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Parametrizações Cadastradas</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a href="#" id="bt_novo_parametroprocedimento" title="Nova Parametrização">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div>
                                        <div class="pull-right text-right"></div>
                                    </div>
                                    <table class="table table-bordered tabela" >
                                        <tr>
                                            <td class="tbsubtitulo">ID</td>
                                            <td class="tbsubtitulo">Procedimento</td>
                                            <td class="tbsubtitulo">Diametro</td>
                                            <td class="tbsubtitulo">Comprimento</td>
                                            <td class="tbsubtitulo">Espessura</td>
                                            <td class="tbsubtitulo">Tecnica</td>
                                            <td class="tbsubtitulo">Tipo Fonte</td>
                                            <td class="tbsubtitulo">Foco Fonte</td>
                                            <td class="tbsubtitulo">Tipo IQI</td>
                                            <td class="tbsubtitulo">Fio IQI</td>
                                            <td class="tbsubtitulo">DFF</td>
                                            <td class="tbsubtitulo">Classe Filme</td>
                                            <td class="tbsubtitulo">Quantidade Filmes</td>
                                            <td class="tbsubtitulo">Dimensão Filme</td>
                                            <td class="tbsubtitulo">Opções</td>
                                        </tr>
                                        @if(count($parametros) > 0)
                                            @foreach ($parametros as $parametro)
                                                <tr> 
                                                    <td class="tbinput_parametros">{{$parametro->id}}</td>
                                                    <td class="tbinput_parametros">{{$procedimentos[$parametro->procedimento_id]}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->diametro_ini}} < T ≤ {{$parametro->diametro_fim}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->comprimento_ini}} < T ≤ {{$parametro->comprimento_fim}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->espessura_ini}} < T ≤ {{$parametro->espessura_fim}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->tecnica}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->tipo_fonte}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->foco_fonte}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->tipo_iqi}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->fio_iqi}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->dff}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->classe_filme}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->quantidade_filmes}}</td>
                                                    <td class="tbinput_parametros">{{$parametro->dimensao_filme}}</td>
                                                    <td class="tbinput_parametros"><a href="#" class="bt_editar_parametro" id="{{$parametro->id}}">Editar</a></td>
                                                </tr>        
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9"><center>Nenhuma Parametro Cadastrado</center></td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Procedimentos</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a href="#" id="bt_novo_procedimento" title="Novo Procedimento">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div>
                                        <div class="pull-right text-right"></div>
                                    </div>
                                    <table class="table table-bordered tabela" >
                                        <tr>
                                            <td class="tbsubtitulo">ID</td>
                                            <td class="tbsubtitulo">Procedimento</td>
                                            <td class="tbsubtitulo">Opções</td>
                                        </tr>
                                        @if(count($procedimentos) > 0)
                                            @foreach ($procedimentos as $key => $procedimento)
                                                <tr> 
                                                    <td class="tbinput">{{$key}}</td>
                                                    <td class="tbinput textoPro{{$key}}">{{$procedimento}}</td>
                                                    <td class="tbinput"><a href="#" class="bt_editar_procedimento" id="{{$key}}">Editar</a></td>
                                                </tr>        
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9"><center>Nenhum procedimento cadastrado</center></td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Grupos de Material Base</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a href="#" id="bt_novo_grupo" title="Novo Grupo">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div>
                                        <div class="pull-right text-right"></div>
                                    </div>
                                    <table class="table table-bordered tabela" >
                                        <tr>
                                            <td class="tbsubtitulo">ID</td>
                                            <td class="tbsubtitulo">Grupo</td>
                                            <td class="tbsubtitulo">Opções</td>
                                        </tr>
                                        @if(count($grupos) > 0)
                                            @foreach ($grupos as $grupo)
                                                <tr> 
                                                    <td class="tbinput">{{$grupo->id}}</td>
                                                    <td class="tbinput textoGru{{$grupo->id}}">{{$grupo->grupo}}</td>
                                                    <td class="tbinput"><a href="#" class="bt_editar_grupo" id="{{$grupo->id}}">Editar</a></td>
                                                </tr>        
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9"><center>Nenhum Grupo Cadastrado</center></td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Normas</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a href="#" id="bt_nova_norma" title="Nova Norma">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div>
                                        <div class="pull-right text-right"></div>
                                    </div>
                                    <table class="table table-bordered tabela_normas" >
                                        <tr>
                                            <td class="tbsubtitulo">ID</td>
                                            <td class="tbsubtitulo">Norma</td>
                                            <td class="tbsubtitulo">Criterio Aceite</td>
                                            <td class="tbsubtitulo">Aplicação</td>
                                            <td class="tbsubtitulo">Opções</td>
                                        </tr>
                                        @if(count($normas) > 0)
                                            @foreach ($normas as $norma)
                                                <tr> 
                                                    <td class="tbinput">{{$norma->id}}</td>
                                                    <td class="tbinput textoNor{{$norma->id}}">{{$norma->norma}}</td>
                                                    <td class="tbinput">{{$norma->criterio_aceite}}</td>
                                                    <td class="tbinput">{{$norma->aplicacao}}</td>
                                                    <td class="tbinput"><a href="#" class="bt_editar_norma" id="{{$norma->id}}">Editar</a></td>
                                                </tr>        
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9"><center>Nenhuma Norma Cadastrada</center></td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>

                        </br>                                
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom">Modal title</h4>
      </div>
      <div class="modal-body" id="bodym">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer" id="footerm">
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
<script src="/js/jquery2.1.js"></script>
<script src="/js/jquery-ui.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/procedimentos.js"></script>

@stop
