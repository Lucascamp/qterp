@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Procedimento {{$procedimento->procedimento}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/procedimento">Procedimentos</a>
            </li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Procedimentos </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" id="idPro" value="{{ $procedimento->id }}">
                        <div id="mensagens">
                        </div>
                            <table class="table table-bordered table_procedimento" >
                                <tr>
                                    <th class="tbtitulo" colspan="14">Editar dados do Procedimento</td>
                                </tr>
                                <tr>
                                    <table class="table table-bordered table_procedimento" >
                                        <tr>
                                            <td>PROCEDIMENTO</td>
                                            <td>NORMA</td>
                                            <td>OBJETO</td>
                                            <td>APLICAÇÃO</td>
                                            <td>METAL BASE</td>
                                            <td colspan="2">DIAMETRO</td>
                                            <td colspan="2">ESPESSURA</td>
                                            <td>FONTE DE RADIAÇÃO</td>
                                            <td colspan="2">FOCO FONTE</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" id="procedimento" value="{{isset($procedimento->procedimento) ? $procedimento->procedimento : ''}}"></td>
                                            <td><input type="text" class="form-control" id="norma" value="{{isset($procedimento->norma) ? $procedimento->norma : ''}}"></td>
                                            <td><input type="text" class="form-control" id="objeto" value="{{isset($procedimento->objeto) ? $procedimento->objeto : ''}}"></td>
                                            <td><input type="text" class="form-control" id="aplicacao" value="{{isset($procedimento->aplicacao) ? $procedimento->aplicacao : ''}}"></td>
                                            <td><input type="text" class="form-control" id="metal_base" value="{{isset($procedimento->metal_base) ? $procedimento->metal_base : ''}}"></td>
                                            <td><input type="text" class="input_numero form-control" id="diametro_ini" value="{{isset($procedimento->diametro_ini) ? $procedimento->diametro_ini : ''}}"></td>
                                            <td><input type="text" class="input_numero form-control" id="diametro_fim" value="{{isset($procedimento->diametro_fim) ? $procedimento->diametro_fim : ''}}"></td>
                                            <td><input type="text" class="input_numero form-control" id="espessura_ini" value="{{isset($procedimento->espessura_ini) ? $procedimento->espessura_ini : ''}}"></td>
                                            <td><input type="text" class="input_numero form-control" id="espessura_fim" value="{{isset($procedimento->espessura_fim) ? $procedimento->espessura_fim : ''}}"></td>
                                            <td>
                                                <select class=" input_select form-control" id="fonte_radiacao">
                                                    @if(isset($procedimento->procedimento))
                                                        <option>{{$procedimento->fonte_radiacao}}</option>    
                                                    @endif
                                                    <option>GAMA</option>
                                                    <option>RAIO X</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class=" input_numero form-control" id="foco_fonte_ini" value="{{isset($procedimento->foco_fonte_ini) ? $procedimento->foco_fonte_ini : ''}}"></td>
                                            <td><input type="text" class="input_numero form-control" id="foco_fonte_fim" value="{{isset($procedimento->foco_fonte_fim) ? $procedimento->foco_fonte_fim : ''}}"></td>
                                            
                                        </tr>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="table table-bordered table_procedimento">
                                        <tr>
                                            <td>TECNICA</td>
                                            <td>CRITERIO DE ACEITE</td>
                                            <td>DFF</td>
                                            <td>TIPO IQI</td>
                                            <td>FIO IQI</td>
                                            <td colspan="2">CLASSE FILME</td>
                                            <td>QUANT FILMES</td>
                                            <td colspan="2">DIMENSÃO FILME</td>
                                            <td>REFORÇO</td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" id="tecnica" value="{{isset($procedimento->tecnica) ? $procedimento->tecnica : ''}}"></td>
                                            <td><input type="text" class="form-control" id="criterio_aceite" value="{{isset($procedimento->criterio_aceite) ? $procedimento->criterio_aceite : ''}}"></td>
                                            <td><input type="text" class=" input_numero form-control" id="dff" value="{{isset($procedimento->dff) ? $procedimento->dff : ''}}"></td>
                                            <td><input type="text" class="form-control" id="tipo_iqi" value="{{isset($procedimento->tipo_iqi) ? $procedimento->tipo_iqi : ''}}"></td>
                                            <td><input type="text" class="input_numero form-control" id="fio_iqi" value="{{isset($procedimento->fio_iqi) ? $procedimento->fio_iqi : ''}}"></td>
                                            <td colspan="2">
                                                <select class="form-control" id="classe_filme">
                                                    @if(isset($procedimento->classe_filme))
                                                        <option>{{$procedimento->classe_filme}}</option>    
                                                    @endif
                                                    <option>I</option>
                                                    <option>II</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class="input_numero form-control" id="quantidade_filme" value="{{isset($procedimento->quantidade_filmes) ? $procedimento->quantidade_filmes : ''}}"></td>
                                            <td colspan="2">
                                                <select class="form-control" id="dimensao">
                                                    @if(isset($procedimento->dimensao_filme))
                                                        <option>{{$procedimento->dimensao_filme}}</option>    
                                                    @else
                                                        <option>selecione</option>    
                                                    @endif

                                                    @foreach ($dimensoes as $dimensao)
                                                        <option value="{{$dimensao[0]->legenda}}">{{$dimensao[0]->descricao}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control input_numero" id="reforco" value="{{isset($procedimento->reforco) ? $procedimento->reforco : ''}}"></td>
                                        </tr>
                                    </table>
                                </tr>
                            </table>
                        </br>   
                        <a class="btn btn-primary" id="bt_salvar_procedimento">Salvar Procedimento</a>                          
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
<script src="/js/jquery2.1.js"></script>
<script src="/js/procedimentos.js"></script>
@stop
