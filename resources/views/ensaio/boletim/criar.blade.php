@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/boletim">Boletim Medição</a>
            </li>
            <li class="active">
                <strong>Criar</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        

       
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Selecionar dados para criar Boletim Mensal<small>(*)campos obrigatorios</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                    <form action="/boletim/carregar" method="POST">
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    @include('ensaio.boletim.partials._cabecalho')

                    <div class="pull-right">
                    <button class="btn btn-primary" id="bt_carregar_boletim">Carregar Boletim Medição</button>
                    </form>
<br><br><br>
<br>
<br>
<br><br><br>
<br>
<br>
<br>
<br>
<br>
                    
                </div>

        
                @if (isset($erro) && $erro)
                <br><br><br>
                        <div class="alert alert-danger">
                            {{$msg}}
                        </div>
                    @endif
            </div>
        </div>
    </div>
</br>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
<script src="/handsontable/dist/handsontable.full.js"></script>
@stop
