@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Boletim de Medição</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li class="active">
                <strong>Boletim de Medição</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="/boletim/criar" class="btn btn-primary">Novo Boletim de Medição</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Boletim de Medição </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                            <table class="table table-bordered tabela">
                            <thead>
                            </thead>
                            <tbody>
                                <table class="table table-bordered tabela" >
                                    <thead>
                                        <tr>
                                            <th class="tbtitulo" colspan="6">Boletins existentes</td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td class="tbsubtitulo">Código B.M.</td>
                                        <td class="tbsubtitulo">Cliente</td>
                                        <td class="tbsubtitulo">Periodo</td>
                                        <td class="tbsubtitulo">GOs</td>
                                        <td class="tbsubtitulo">Valor</td>
                                        <td class="tbsubtitulo" colspan="2">Ações</td>
                                    </tr>
                                    <tbody id="listaPropostasH">
                                        @if (count($bms) > 0)
                                            @foreach ($bms as $bm)
                                            <tr>
                                                <td class="tblabel">{{$bm->id}}</td>
                                                <td class="tblabel">{{$clientes[$bm->cliente_id]}}</td>
                                                <?php 
                                                    $datei = date_create($bm->periodo_ini);
                                                    $datef = date_create($bm->periodo_fim);
                                                 ?>
                                                <td class="tblabel">{{date_format($datei, 'd/m/Y')}} a {{date_format($datef, 'd/m/Y')}}</td>
                                                <?php $arrayids = explode (';',$bm->guia_id) ?>
                                                <td class="tblabel">
                                                    @foreach ($arrayids as $key => $element)
                                                        @if(count($arrayids) == $key+1)
                                                            <a href="/guia/{{$element}}/visualizar">{{$element}}</a> 
                                                        @else
                                                            <a href="/guia/{{$element}}/visualizar">{{$element}}</a> - 
                                                        @endif
                                                    @endforeach
                                                </td>
                                                
                                                <td class="tblabel">R$ {{number_format($bm->valor_total, 2, ',', '.')}}</td>
                                                <td class="tblabel">
                                                    <a href="#" data-toggle="tooltip" title="Editar" data-placement="top" class="legenda btn btn-info dim">
                                                    <i class="fa fa-pencil"></i></a>

                                                    <a href="{{ route('boletim.visualizar', array($bm->id)) }}" data-toggle="tooltip" title="Visualizar" data-placement="top" class="legenda btn btn-success dim">
                                                    <i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" class="tblabel" >Nenhum Boletim Cadastrado</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</div>
</div>
@stop
