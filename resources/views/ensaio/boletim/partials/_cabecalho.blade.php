<table class="table table-bordered tabela">
    <tr>
        <td class="tblabel">
            {!! Form::select('cliente_id', $clientes, null ,array('id' => 'clienteBM', 'class'=>'form-control chosen-select')) !!}
        </td>
        <td class="tblabel">
            {!! Form::select('proposta_id', ['' => 'Selecione a proposta'], null ,array('id' => 'propostaBM', 'class'=>'form-control')) !!}
        </td>
        <td class="tblabel">Data Início(*)</td>
        <td>
        <input type="date" name="inicio" class="form-control" id="data_inicio">
        </td>

        <td class="tblabel">Data Final(*)</td>
        <td>
        <input type="date" name="fim" class="form-control" id="data_fim">
    </tr>
</table>