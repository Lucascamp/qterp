	<tr>	
		<td colspan="2"><b>Total  de Quantidades</b></td>
		<td><span id="quantss"></span></td>
		<td><span id="quantdf"></span></td>
		<td><span id="quantsse"></span></td>
		<td><span id="quantdfe"></span></td>
		<td><span id="quantkm"></span></td>

		@foreach($filmes as $filme)
			<td><span id="quant{{ $filme->legenda }}"></span></td>
        @endforeach

        <td><span id="diametroA"></span></td>
        <td><span id="diametroB"></span></td>
        <td><span id="diametroC"></span></td>
        <td><span id="diametroD"></span></td>
        <td><span id="diametroE"></span></td>
        <td><span id="diametroF"></span></td>
        <td><span id="diametroG"></span></td>
        <td><span id="diametroH"></span></td>
	</tr>

	<tr>
		<td colspan="2"><b>Preço Unitário</b></td>
		<td><span id="valorss"></span></td>
		<td><span id="valordf"></span></td>
		<td><span id="valorsse"></span></td>
		<td><span id="valordfe"></span></td>
		<td><span id="valorkm"></span></td>

		@foreach($filmes as $filme)
			<td><span id="valor{{ $filme->legenda }}"></span></td>
        @endforeach
        <td><span id="valordiametroA"></span></td>
    	<td><span id="valordiametroB"></span></td>
    	<td><span id="valordiametroC"></span></td>
    	<td><span id="valordiametroD"></span></td>
    	<td><span id="valordiametroE"></span></td>
    	<td><span id="valordiametroF"></span></td>
    	<td><span id="valordiametroG"></span></td>
    	<td><span id="valordiametroH"></span></td>
	</tr>

	<tr>
		<td colspan="2"><b>Somas Parciais</b></td>
		<td><span id="resultadoss"></span></td>
		<td><span id="resultadodf"></span></td>
		<td><span id="resultadosse"></span></td>
		<td><span id="resultadodfe"></span></td>
		<td><span id="resultadokm"></span></td>
		
		@foreach($filmes as $filme)
			<td><span id="resultado{{ $filme->legenda }}"></span></td>
        @endforeach
        
        	<td><span id="totaldiametroA"></span></td>
        	<td><span id="totaldiametroB"></span></td>
        	<td><span id="totaldiametroC"></span></td>
        	<td><span id="totaldiametroD"></span></td>
        	<td><span id="totaldiametroE"></span></td>
        	<td><span id="totaldiametroF"></span></td>
        	<td><span id="totaldiametroG"></span></td>
        	<td><span id="totaldiametroH"></span></td>
	</tr>

	<tr>
		<td colspan="20" align="right"><b>Total</b></td>
		<td colspan="4"><span id="total"></span></td>
	</tr>

	<tr>
		<td colspan="8">__________________________________________________________________________________________
		<br><center>Coordernador do projeto</center></td>
		<td colspan="8">__________________________________________________________________________________________
		<br><center>Aprovação do cliente</center></td>
		<td colspan="8" rowspan="4">Observações</td>
	</tr>
	<tr>
		<td colspan="8" align="left">Nome Coordenador</td>
		<td colspan="8" align="left">Nome</td>	
	</tr>
	<tr>
		<td colspan="8" align="left">Envio em: </td>
		<td colspan="8" align="left">Aprovado em: </td>
	</tr>
	<tr>
		<td colspan="24" align="left">CONFORME CLÁUSULA CONTRATUAL,  O NÃO PRONUNCIAMENTO EM 03 ( TRES ) DIAS ÚTEIS, IMPLICARÁ NA ACEITAÇÃO TÁCITA DESTE RESUMO DE SERVIÇOS.</td>
	</tr>




