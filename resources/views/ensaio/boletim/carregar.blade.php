@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/boletim">Boletim Medição</a>
            </li>
            <li class="active">
                <strong>Criar BM</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
            <ul class="dropdown-menu">
                <li><a href="/boletim/criar" >Novo Boletim</a></li>
            </ul>
        </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>BM</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="inicio" value="{{ $inicio }}">
                <input type="hidden" id="fim" value="{{ $fim }}">
                <input type="hidden" id="stringids" value="{{ $stringids }}">
                <input type="hidden" id="cliente_id" value="{{ $cliente->id }}">
                <input type="hidden" id="proposta_id" value="{{ $proposta->id }}">
                
                <div class="ibox-content">
                    <div class="area_impressao" name="area_impressao" id="area_impressao">
                        <table class="table table-bordered tabela2 negrito">
                            <tbody id="tablehead" >   
                                <tr>
                                    <td rowspan="2" colspan="4" align="center"><img src="/img/img_logo_proposta.png" width="100px" height="30px"></td>
                                    <td rowspan="2" colspan="14">R. Petrovale, 450 – Distrito Industrial Marsil – 32400-000 - Ibirité - MG</td>
                                    <td colspan="4">BM: <span id="id_bm"></span></td>
                                </tr>
                                <tr>
                                    <td colspan="4">PR: <span id="id_pr">{{$proposta->cod_proposta}}</span></td>
                                </tr>

                                <tr>
                                    <td colspan="2">Cliente:</td>
                                    <td colspan="8"><span id="nome">{{$cliente->nome}}</span></td>
                                    <td colspan="3">Período:<span id="inicio"></span> {{$inicio->format('d-m-Y')}} a {{$fim->format('d-m-Y')}}<span id="fim"></span></td> 
                                    <td colspan="2">Projeto:</td>
                                    <td colspan="7"><span id="projeto"> {{$projeto->servico}}</span></td>
                                </tr>

                                <tr>
                                    <td colspan="2">Endereço:</td>
                                    <td colspan="10"><span id="endereco"> {{$cliente->endereco}}</span></td>
                                    <td colspan="2">Local:</td>
                                    <td colspan="8"><span id="local"></span> {{$cliente->estado}} - {{$cliente->municipio}}</td>
                                </tr>

                                <tr>
                                    <td colspan="2">Solicitante:</td>
                                    <td colspan="5"><span id="nome_solicitante"> {{$projeto->solicitacao_contato}}</span></td>
                                    <td colspan="6"><span id="email_solicitante"> {{$projeto->solicitacao_email}}</span></td>
                                    <td colspan="3"><span id="tel_solicitante"> {{$projeto->solicitacao_tel}}</span></td>
                                    <td colspan="2">Referência</td>
                                    <td colspan="4"><span id="referencia"></span></td>
                                </tr>

                                <tr>
                                    <td colspan="2">Contato:</td>
                                    <td colspan="5"><span id="nome_contato"> {{$projeto->aprovacao_contato}}</span></td>
                                    <td colspan="6"><span id="email_contato"> {{$projeto->aprovacao_email}}</span></td>
                                    <td colspan="3"><span id="tel_contato"> {{$projeto->aprovacao_tel}}</span></td>
                                    <td colspan="2">Proposta:</td>
                                    <td colspan="4"><span id="proposta"></span></td>
                                </tr>

                                @include('ensaio.boletim.partials._tablebody')

                                <tbody id="tableboletim" align="center">
                                    <?php $soma_km = 0; 
                                        $soma_dim = [];
                                        $diam = [];
                                    ?>
                                        @foreach ($array_dimensoes as $filme)
                                            <?php $soma_dim[$filme] = 0; ?>
                                        @endforeach
                                        @foreach ($array_diametros as $dia)
                                            <?php $diam[$dia] = 0 ?>
                                        @endforeach
                                    @foreach ($guias as $guia)
                                        <tr>
                                            <td>{{$guia->data->format('d-m-Y')}}</td>
                                            <td>{{$guia->id}}</td>
                                            <td>{{$guia->entrada->format('H:i')}}</td>
                                            <td>{{$guia->saida->format('H:i')}}</td>
                                            @if($guia->diaria_normal)
                                                <td>{{$guia->diaria_normal}}</td>
                                            @else
                                                <td>-</td>
                                            @endif

                                            @if($guia->diaria_dom)
                                                <td>{{$guia->diaria_dom}}</td>
                                            @else
                                                <td>-</td>
                                            @endif

                                            @if($guia->extra_normal)
                                                <td>{{$guia->extra_normal}}</td>
                                            @else
                                                <td>-</td>
                                            @endif

                                            @if($guia->extra_dom)
                                                <td>{{$guia->extra_dom}}</td>
                                            @else
                                                <td>-</td>
                                            @endif
                                            <?php $soma_km += $guia->km_final - $guia->km_inicial ?>
                                            <td colspan="2">{{$guia->km_final - $guia->km_inicial}}</td>

                                            {{-- filmes --}}
                                            @if (count($array_dimensoes) == 0)
                                                <td colspan="6">-</td>
                                            @else
                                                @foreach ($array_dimensoes as $filme)
                                                    @if (isset($programacoes[$guia->id]))
                                                        <?php if(count($array_dimensoes) == 1){$valor = 6;}?>
                                                        <?php if(count($array_dimensoes) == 2){$valor = 3;}?>
                                                        <?php if(count($array_dimensoes) == 3){$valor = 2;}?>
                                                        <?php $soma = 0; ?>
                                                        @foreach ($programacoes[$guia->id] as $programacao)
                                                            @if($programacao->filme_dimensao == $filme)
                                                                <?php $soma +=  $programacao->quantidade_filmes?>
                                                            @else

                                                            @endif
                                                        @endforeach

                                                        @if($soma > 0)
                                                            <td colspan="{{(count($array_dimensoes) <= 3) ? $valor : 1}}">{{$soma}}</td>
                                                            <?php $soma_dim[$filme] += $soma ?>
                                                        @else
                                                            <td colspan="{{(count($array_dimensoes) <= 3) ? $valor : 1}}" >-</td>
                                                        @endif
                                                    @else
                                                        <td colspan="{{(count($array_dimensoes) <= 3) ? $valor : 1}}">-</td>
                                                    @endif
                                                @endforeach
                                            @endif
                                            {{-- diametros --}}
                                            @if (count($array_diametros) == 0)
                                                <td colspan="6">-</td>
                                            @else
                                                <?php if(count($array_diametros) == 1){$valor = 6;}?>
                                                <?php if(count($array_diametros) == 2){$valor = 3;}?>
                                                <?php if(count($array_diametros) == 3){$valor = 2;}?>  
                                                @foreach ($array_diametros as $dia)
                                                    <?php $soma = 0;?>
                                                    @foreach ($diametros[$guia->id] as $diametro)
                                                        @if($diametro->junta_id == $dia)
                                                            <?php $soma +=  $diametro->quantidade?>
                                                        @else

                                                        @endif
                                                    @endforeach
                                                    @if($soma > 0)
                                                        <td colspan="{{(count($array_diametros) <= 3) ? $valor : 1}}">{{$soma}}</td>
                                                        <?php $diam[$dia] += $soma ?>
                                                    @else
                                                        <td colspan="{{(count($array_diametros) <= 3) ? $valor : 1}}">-</td>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>

                               <tr> 
        <td colspan="4"><b>Total  de Quantidades</b></td>
        <td align="center" ><span id="quantss"> {{ $quant_diaria_normal}}</span></td>
        <td align="center" ><span id="quantdf"> {{ $quant_diaria_dom}}</span></td>
        <td align="center" ><span id="quantsse"> {{ $quant_extra_normal}}</span></td>
        <td align="center" ><span id="quantdfe"> {{ $quant_extra_dom}}</span></td>
        <td colspan="2" align="center" ><span id="quantkm"> {{ $soma_km}}</span></td>
        @if (count($array_dimensoes) == 0)
                                                <td colspan="6">-</td>
                                            @else
        @foreach($array_dimensoes as $filme)
            <?php if(count($array_dimensoes) == 1){$valor = 6;}?>
                <?php if(count($array_dimensoes) == 2){$valor = 3;}?>
                <?php if(count($array_dimensoes) == 3){$valor = 2;}?>
            <td colspan="{{(count($array_dimensoes) <= 3) ? $valor : 1}}" align="center" ><span id="quant{{ $filme }}">{{$soma_dim[$filme]}}</span></td>
        @endforeach
        @endif
        @if (count($array_diametros) == 0)
                                                <td colspan="6">-</td>
                                            @else
        @foreach ($array_diametros as $dia)
         <?php if(count($array_diametros) == 1){$valor = 6;}?>
                                                        <?php if(count($array_diametros) == 2){$valor = 3;}?>
                                                        <?php if(count($array_diametros) == 3){$valor = 2;}?>
            <td colspan="{{(count($array_diametros) <= 3) ? $valor : 1}}" align="center"><span id="diametro{{$dia}}">{{$diam[$dia]}}</span></td>
        @endforeach
        @endif
    </tr>

    <tr>
        <td colspan="4"><b>Preço Unitário</b></td>
        <td><span id="valorss">R$ {{number_format($valores_diarias['normal']->valor_normal, 2, ',', '.')}}</span></td>
        <td><span id="valordf">R$ {{number_format($valor_diaria_domingo->valor_normal, 2, ',', '.')}}</span></td>
        <td><span id="valorsse">R$ {{number_format($valores_diarias['normal']->valor_extra, 2, ',', '.')}}</span></td>
        <td><span id="valordfe">R$ {{number_format($valor_diaria_domingo->valor_extra, 2, ',', '.')}}</span></td>
        <td colspan="2"><span id="valorkm">R$ {{number_format($valor_km, 2, ',', '.')}}</span></td>
        @if (count($array_dimensoes) == 0)
                                                <td colspan="6">-</td>
                                            @else
        @foreach($array_dimensoes as $filme)
            <?php if(count($array_dimensoes) == 1){$valor = 6;}?>
                <?php if(count($array_dimensoes) == 2){$valor = 3;}?>
                <?php if(count($array_dimensoes) == 3){$valor = 2;}?>
            <td colspan="{{(count($array_dimensoes) <= 3) ? $valor : 1}}"><span id="valor{{ $filme }}">R$ {{number_format($valores[$filme], 2, ',', '.')}}</span></td>
        @endforeach
        @endif
        @if (count($array_diametros) == 0)
                                                <td colspan="6">-</td>
                                            @else
        @foreach ($array_diametros as $dia)
         <?php if(count($array_diametros) == 1){$valor = 6;}?>
                                                        <?php if(count($array_diametros) == 2){$valor = 3;}?>
                                                        <?php if(count($array_diametros) == 3){$valor = 2;}?>
            <td colspan="{{(count($array_diametros) <= 3) ? $valor : 1}}"><span id="valordiametro{{$dia}}">R$ {{number_format($valores_diametros[$dia], 2, ',', '.')}}</span></td>
        @endforeach
        @endif
    </tr>
<?php  $total = 0?>
    <tr>
        <td colspan="4"><b>Somas Parciais</b></td>
        <td><span id="resultadoss">R$ {{number_format($quant_diaria_normal * $valores_diarias['normal']->valor_normal, 2, ',', '.') }}</span></td>
        <?php $total +=  $quant_diaria_normal * $valores_diarias['normal']->valor_normal?>
        <td><span id="resultadodf">R$ {{number_format($quant_diaria_dom *$valor_diaria_domingo->valor_normal, 2, ',', '.') }}</span></td>
        <?php $total +=  $quant_diaria_dom * $valor_diaria_domingo->valor_normal?>
        <td><span id="resultadosse">R$ {{number_format( $quant_extra_normal * $valores_diarias['normal']->valor_extra, 2, ',', '.') }}</span></td>
        <?php $total += $quant_extra_normal * $valores_diarias['normal']->valor_extra?>
        <td><span id="resultadodfe">R$ {{number_format($quant_extra_dom * $valor_diaria_domingo->valor_extra, 2, ',', '.')}}</span></td>
        <?php $total +=$quant_extra_dom * $valor_diaria_domingo->valor_extra ?>
        <td colspan="2"><span id="resultadokm">R$ {{number_format($soma_km * $valor_km, 2, ',', '.')}}</span></td>
        <?php $total += $soma_km * $valor_km?>
        @if (count($array_dimensoes) == 0)
                                                <td colspan="6">-</td>
                                            @else
        @foreach($array_dimensoes as $filme)
        <?php if(count($array_dimensoes) == 1){$valor = 6;}?>
                <?php if(count($array_dimensoes) == 2){$valor = 3;}?>
                <?php if(count($array_dimensoes) == 3){$valor = 2;}?>
            <td colspan="{{(count($array_dimensoes) <= 3) ? $valor : 1}}" ><span id="resultado{{ $filme }}">R$ {{number_format($soma_dim[$filme]*$valores[$filme], 2, ',', '.') }}</span></td>
            <?php $total += $soma_dim[$filme]*$valores[$filme] ?>
        @endforeach
        @endif
        @if (count($array_diametros) == 0)
                                                <td colspan="6">-</td>
                                            @else
        @foreach ($array_diametros as $dia)
         <?php if(count($array_diametros) == 1){$valor = 6;}?>
                                                        <?php if(count($array_diametros) == 2){$valor = 3;}?>
                                                        <?php if(count($array_diametros) == 3){$valor = 2;}?>
            <td colspan="{{(count($array_diametros) <= 3) ? $valor : 1}}"><span id="totaldiametro{{$dia}}">R$ {{number_format($diam[$dia] * $valores_diametros[$dia], 2, ',', '.') }}</span></td>
            <?php $total +=$diam[$dia] * $valores_diametros[$dia] ?>
        @endforeach
        @endif
    </tr>

    <tr>
        <td colspan="8" align="center"><b></b></td>
        <td align="center" colspan="2"><b>Total Medição</b></td>
        <td align="center" colspan="2"><span id="total">R$ {{number_format($total, 2, ',', '.')}}</span></td>
        <td align="center" colspan="2"><b>ISS</b></td>
        <td align="center" colspan="2"><b>% {{number_format($proposta['iss'], 2, ',', '.')}}</b></td>
        <?php $iss =( $total / 100 ) * $proposta['iss']?>
        <td align="center" colspan="2" ><b>R$ {{number_format($iss, 2, ',', '.')}}</b></td>
        <td align="center"><b>Total</b></td>
        <?php $total += $iss ?>
        <td align="center" colspan="4"><span id="total">R$ {{number_format($total, 2, ',', '.')}}</span></td>
        <input type="hidden" id="valor_total" value="{{ $total }}">
    </tr>

    <tr>
        <td colspan="8">__________________________________________________________________________________________
        <br><center>Coordernador do projeto</center></td>
        <td colspan="8">__________________________________________________________________________________________
        <br><center>Aprovação do cliente</center></td>
        <td colspan="6" rowspan="5">Observações</td>
    </tr>
    <tr>
        <td colspan="8" align="left">Nome Coordenador</td>
        <td colspan="8" align="left">Nome</td>  
    </tr>
    <tr>
        <td colspan="8" align="left">Envio em: </td>
        <td colspan="8" align="left">Aprovado em: </td>
    </tr>
    <tr>
        <td colspan="16" align="left">CONFORME CLÁUSULA CONTRATUAL,  O NÃO PRONUNCIAMENTO EM 03 ( TRES ) DIAS ÚTEIS, IMPLICARÁ NA ACEITAÇÃO TÁCITA DESTE RESUMO DE SERVIÇOS.</td>
    </tr>
                            </tbody>
                        </table>    
                        <div id="bt_s"><a id="bt_salvar_bm" class="btn btn-primary">salvar</a></div>
                    </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>
</br>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop
