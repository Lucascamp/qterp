<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{ $usuario->nome }}</strong>
                            </span> 
                            <!-- <span class="text-muted text-xs block"><b class="caret"></b></span>  -->
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="/adm/trocarsenha">Trocar Senha</a></li>
                        <li class="divider"></li>
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    QT-ERP
                </div>
            </li>
            @foreach ($areas as $area)
                <li>
                    <a href="" title="{{$area['descricao']}}"><i class="fa {{str_replace('/','_',$area['rota'])}}-icone"></i> <span class="nav-label">{{$area['descricao']}}</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{$area['rota']}}">Home {{$area['descricao']}}</a></li>
                        @foreach ($permissoes as $permissao)
                            @if($area['id'] == $permissao['areamodulo_id'])
                                <li><a href="{{$permissao['rota']}}">{{$permissao['descricao']}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</nav>