@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h2>Filmes de Reparo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias Operacionais</a>
            </li>
            <li class="active">
                <strong>Criar go para filme de reparo</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">

        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox-title">
                    <h5><i class="fa fa-sliders"></i>Filme de Reparo </h5>
                    <div class="ibox-tools"></div>
                </div>
                <div class="ibox-content">
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                    <div id="mensagens">
                    </div>
                    
                    <table class="table table-bordered tabela">
                        <thead>
                        </thead>
                        <tbody>
                            <table class="table table-bordered tabela" >
                                <thead>
                                    <tr>
                                        <th class="tbtitulo" colspan="8">Filmes reprovados</td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="tbsubtitulo">Código G.O.</td>
                                    <td class="tbsubtitulo">Cliente</td>
                                    <td class="tbsubtitulo">Junta</td>
                                    <td class="tbsubtitulo">Identificacao</td>
                                    <td class="tbsubtitulo">Spool</td>
                                    <td class="tbsubtitulo">Data</td>
                                    <td class="tbsubtitulo" colspan="2">Ações</td>
                                </tr>
                                <tbody id="listaPropostasH">
                                    @if (count($juntasreprovadas) > 0)
                                        @foreach ($juntasreprovadas as $juntareprovada)
                                        <tr>
                                            <td class="tblabel">{{$juntareprovada->guia_id}}</td>
                                            <td class="tblabel">{{$nomescliente[$juntareprovada->guia_id]}}</td>
                                            <td class="tblabel">{{$juntareprovada->junta}}</td>
                                            <td class="tblabel">{{$juntareprovada->identificacao}}</td>
                                            <td class="tblabel">{{$juntareprovada->spool}}</td>
                                            <td class="tblabel">{{$juntareprovada->updated_at}}</td>
                                            <td class="tblabel">
                                                <a href="/guia/{{$juntareprovada->id}}/gofilmereparo" data-toggle="tooltip" title="Gerar GO" data-placement="top" class="legenda btn btn-info dim">
                                                <i class="fa fa-pencil"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="tblabel" >Nenhum Filme Reprovado</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </tbody>
                    </table>
                    </br>                                
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop