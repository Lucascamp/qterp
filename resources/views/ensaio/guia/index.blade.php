@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Guia Operacional</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li class="active">
                <strong>Guia Operacional</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="/guia/cadastrar" class="btn btn-primary">Nova Guia Operacional</a>
             <a href="#" class="btn btn-primary" id="bt_go_manual">GO Manual</a>
             <a href="/guia/filmereparo" class="btn btn-primary" id="bt_go_manual">Filme de reparo</a>
        </div>
        <div class="title-action">
           
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Guias Operacionais </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <div id="mensagens">
                        </div>                            
                        <table class="table table-bordered tabela" id="tabelaListaGuias">
                            <thead>
                                <tr>
                                    <th class="tbtitulo" colspan="6">Guias existentes</td>
                                </tr>
                                <tr>
                                    <td class="tbsubtitulo">Código G.O.</td>
                                    <td class="tbsubtitulo">Unidade</td>
                                    <td class="tbsubtitulo">Cliente</td>
                                    <td class="tbsubtitulo">Data</td>
                                    <td class="tbsubtitulo">Ações</td>
                                </tr>
                            </thead>
                            <tbody id="listaPropostasH">
                                @if (count($guias) > 0)
                                    @foreach ($guias as $guia)
                                    <tr>
                                        <td class="tblabel">{{$guia->id}}</td>
                                        <td class="tblabel">{{$guia->unidade}}</td>
                                        <td class="tblabel">{{$clientes[$guia->cliente_id]}}</td>
                                        <td class="tblabel">{{$guia->data->format('d/m/Y')}}</td>
                                        <td class="tblabel">
                                            <a href="{{ route('guia.editar', array($guia->id)) }}" data-toggle="tooltip" title="Editar" data-placement="top" class="legenda btn btn-info dim">
                                            <i class="fa fa-pencil"></i></a>

                                            <a href="{{ route('guia.visualizar', array($guia->id)) }}" data-toggle="tooltip" title="Visualizar" data-placement="top" class="legenda btn btn-success dim">
                                            <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="tblabel" >Nenhuma Guia Cadastrada</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        </br>                                
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</div>
</div>




<div class="modal fade" id="ModalMnual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-red">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center><h1 class="modal-title" id="titulom">Modal title</h1></center>
      </div>
      <div class="modal-body" id="bodym">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <a class="btn btn-primary" href="{{ route('guia.manual')}}">Prosseguir</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop
