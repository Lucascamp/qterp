@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h2>Filmes de Reparo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias Operacionais</a>
            </li>
            <li class="active">
                <strong>Criar go para filme de reparo</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">

        
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
               <div class="ibox-title">
                    <h5>Cadastro de dados da Guia Operacional para filme de reparo<small>(*)campos obrigatorios</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" id="idFilme" value="{{ $programacao->id}}">

                    <table class="table table-bordered tabela">
                        <tr>
                            <td class="tblabel">Unidade(*)</td>
                            <td><input type="text" name="unidade" class="form-control" id="unidade" value="{{$programacao->unidade}}" disabled></td>
                            <td class="tblabel">Data(*)</td>
                            <td><input type="date" name="data"  id="data" class="form-control"></td>
                        </tr>
                    </table>
                    <table class="table table-bordered tabela">
                        <tr>
                            <td  class="tbtitulo" colspan="6">Cliente</td>
                        </tr>
                        <tr>
                            <td class="tblabel">Cliente(*)</td>
                            <td colspan="3">
                                <input type="text" name="cliente" id="cliente" class="form-control" value="{{$dadoscliente->nome}}" disabled>
                            </td>
                            <td class="tblabel">Proposta(*)</td>
                            <td>
                                <input type="text" name="proposta" id="proposta" class="form-control" value="{{$proposta->cod_proposta}}" disabled>
                            </td>
                        </tr>
                        <tr>    
                            <td class="tblabel">Nome</td>
                            <td class="tbitem"><b><p id="nome_reduz">
                                    {{ $dadoscliente->nome_reduz }}
                                </p>
                            </b></td>

                            <td class="tblabel">Código</td>
                            <td class="tbitem"><b><p id="codigo">
                                    {{ $dadoscliente->codigo }}
                               </p>  
                           </b></td>

                           <td class="tblabel">CNPJ</td>
                           <td class="tbitem"><b><p id="cnpj">
                                    {{ $dadoscliente->cnpj }}
                               </p>    
                           </b></td>
                        </tr>

                        <tr>    
                            <td class="tblabel">Endereço</td>
                            <td class="tbitem"><b><p id="endereco">
                                    {{ $dadoscliente->endereco }} - {{ $dadoscliente->bairro }} - {{ $dadoscliente->municipio }}
                                </p>
                            </b></td>
                            <td class="tblabel">Estado</td>
                            <td class="tbitem"><b><p id="estado">
                                {{ $dadoscliente->estado }}
                                </p>
                            </b></td>
                            <td class="tblabel">Contato</td>
                            <td class="tbitem"><b><p id="contato">
                                    {{ $dadoscliente->ddd }} - {{ $dadoscliente->tel }}
                                </p>
                            </b></td>
                        </tr>
                    </table>

                    @include('ensaio.guia.partials._funcionarios', $funcionarios)

                    @include('ensaio.guia.partials._fontes', $fontes)

                    <table class="table table-bordered tabela">
                        <tr>
                            <td  class="tbtitulo" colspan="4">Programação</td>
                        </tr>
                        <tr>
                            <td class="tbsubtitulo">Local</td>
                            <td class="tbsubtitulo">Contratante/Fabricante</td>
                            <td class="tbsubtitulo">Data</td>
                            <td class="tbsubtitulo">Controle CLiente</td>
                        </tr>
                        <tr>
                            <td class="tblabel">
                                <select  id="local" class ="form-control">
                            @if(isset($local_ensaio))
                                <option value="{{$local_ensaio}}">{{$local_ensaio}}</option>
                            @endif
                            <option value="QUALITEC">QUALITEC</option>
                            <option value="CLIENTE">CLIENTE</option>
                            
                        </select>
                            </td>
                            <td class="tblabel"><input type="text" id="contratante_fabricante" class="form-control" value="{{isset($contratante_fabricante) ? $contratante_fabricante : ''}}"></td>
                            <td class="tblabel"><input type="text" id="data_prog" class="form-control" value="{{isset($data_prog) ? $data_prog : ''}}"></td>
                            <td class="tblabel"><input type="text" id="controle_cliente" class="form-control" value="{{isset($controle_cliente) ? $controle_cliente : ''}}"></td>
                        </tr>
                        <tr>
                            <td class="tbsubtitulo">Escolha os filmes</td>

                            <td>
                            @foreach ($filmes as $key => $value)
                                @if ($value->resultado == 'R')
                                    <button class="btn btn-danger frbt" id="frbt{{$value->numero_filme}}" type="button">F{{$value->numero_filme}}</button>    
                                @else
                                    <button class="btn btn-primary frbt" id="frbt{{$value->numero_filme}}" type="button">F{{$value->numero_filme}}</button>
                                @endif
                                
                            @endforeach
                            </td>

                            <td  class="tbsubtitulo">Filmes escolhidos</td>
                            <td id="fresco"></td>
                        </tr>          
                        


                    </table>

                    @if ($errors->any())
                        <div class="alert-group">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ implode('', $errors->all(':message')) }}
                            </div>
                        </div>
                    @endif

                    <div class="pull-right">
                    
                    <a class="btn btn-primary" id="bt_salvar_guia_fr" title="Carregue as tecnicas para liberar o cadastro.">Cadastrar Guia Operacional</a> 

                </div>
                <br><br>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop