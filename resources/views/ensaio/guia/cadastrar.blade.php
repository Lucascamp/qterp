@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Guia Operacional</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias Operacionais</a>
            </li>
            <li class="active">
                <strong>Cadastro</strong>
            </li>
        </ol>
    </div>
    
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de dados da Guia Operacional<small>(*)campos obrigatorios</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('route' => 'guia.carregar', 'class'=>'form-inline', 'files' => true)) !!}
                    {!! Form::hidden('created_by', '1') !!}

                    <input type="hidden" id="token" value="{{ csrf_token() }}">

                    @include('ensaio.guia.partials._guia')

                    @include('ensaio.guia.partials._clientes', $clientes)

                    @include('ensaio.guia.partials._funcionarios', $funcionarios)

                    @include('ensaio.guia.partials._fontes', $fontes)

                    @include('ensaio.guia.partials._projeto')

                    @include('ensaio.guia.partials._programacao')
                    @if ($errors->any())
                        <div class="alert-group">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ implode('', $errors->all(':message')) }}
                            </div>
                        </div>
                    @endif

                    <div class="pull-right">
                    
                    <a class="btn btn-primary" id="bt_carregar_procedimento" >Carregar dados de Procedimento</a> 
                    <a class="btn btn-primary" id="bt_salvar_guia" disabled title="Carregue as tecnicas para liberar o cadastro.">Cadastrar Guia Operacional</a> 

                </div>
                <br><br>
                </div>
            </div>
        </div>
    </div>
</br>
</br>
</br>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom">Modal title</h4>
      </div>
      <div class="modal-body" id="bodym">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop
