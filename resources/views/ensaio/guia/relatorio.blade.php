@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Visualiação {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guia Operacional</a>
            </li>
             <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>Relatorio</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">
                <a href="#" class="btn btn-primary" id="bt_editar_relatorio">Editar Relatorios</a>
        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
            <ul class="dropdown-menu">
                <li><a href="#" id="impriRelatorio">Imprimir Relatorio</a></li>
                <li class="divider"></li>
                <li><a href="{{ route('guia.editar', array($guia->id)) }}">Editar GO</a></li>
                <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/etiqueta">Etiquetas</a></li>
                <li><a href="/guia/{{$guia->id}}/capa">Capa</a></li>
                <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/criarrelatorio">Criar Relatorio</a></li>
            </ul>
        </div>
        </div>
    </div>
</div>
<div class="row">
     <input type="hidden" id="token" value="{{ csrf_token() }}">
     <input type="hidden" id="guia_id" value="{{ $guia->id }}">
     @foreach ($gorelatorios as $element)
          <input type="hidden" class="numero_relatorio" value="{{ $element }}">
     @endforeach
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox-content">

<div class="area_impressao" name="area_impressao" id="area_impressao">
        <?php $ind = 0; $itemrel = [];?>
        
        @foreach($gorelatorios as $chave => $rel)
            <?php $prognorma = count($programacoes[$rel]);

                $tipo = $programacoes[$rel][0]['tipo'];
            ?>

            @include('ensaio.guia.relatorio.header', array($rel,$relatorio,$tipo))
            @include('ensaio.guia.relatorio.table_header')

            <?php
                $pagina= intval(ceil($prognorma/15));

                $contlinhas=1;
                $numeropagina = 1;
                $auxitem = [];
                $progs = [];
                
            ?>
            
            @foreach ($programacoes[$rel] as $key => $programacao)
                <?php 
                    $filmes = intval($programacao['quantidade_filmes']);
                    if(isset($nfilme) and $nfilme < $filmes)
                        $nfilme++;
                    else
                        $nfilme = 1;
                ?>
                <?php $auxitem[] =  $programacao['item'];?>
                @if($contlinhas < 16)
                        <?php $progs[] =  $programacao?>
                        @include('ensaio.guia.relatorio.row', array($programacao))
                @elseif($contlinhas >= 16)
                    <?php $contlinhas=1;?>
                    @include('ensaio.guia.relatorio.table_footer', array($progs))

                    <div style="float:right; width: 160px; height: 20px; z-index: 1" id="contpag">{{isset($relatorio) ? 'Revisão: '.$relatorio[$rel]->versao.' - ' : ''}}  Página {{ $numeropagina }} de {{ $pagina }}</div>

                    @if($pagina>1)
                        <?php $numeropagina++; ?> 
                    @endif

                    <div class="page-break"></div>
                        
                    @include('ensaio.guia.relatorio.header')

                    <table class="zebra3"  style="min-width: 1083px" width="100%"cellspacing="0">
                
                    @include('ensaio.guia.relatorio.table_header')

                    @include('ensaio.guia.relatorio.row', array($programacao))     
                @endif

                    <?php $contlinhas++; ?>
                
            @endforeach
            <?php $auxitem = array_unique($auxitem);
             $itemrel[$gorelatorios[$ind]] = $auxitem; ?>
           
            @foreach (range(1, (17-$contlinhas)) as $index)
               @if (count(range(1, (17-$contlinhas))) == 1) 
                @else
                    @include('ensaio.guia.relatorio.row_fake')
                @endif
            @endforeach

            @include('ensaio.guia.relatorio.table_footer')

            <div style="float:right; width: 160px; height: 20px; z-index: 1" id="contpag">{{ 'Revisão: '.$relatorio[$rel]->versao.' - '}} Página {{ $numeropagina }} de {{ $pagina }}</div>

            <div class="page-break"></div>
            <?php $ind++; ?>

        @endforeach 
            @foreach ($itemrel as $key => $item)
                <?php $str = implode('+', $item);?>
                <input type="hidden" class="itensrelatorio" id="itensrelatorio{{$key}}" value="{{ $str }}">
            @endforeach
</div> <!-- area de impressao -->







                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="Modalrelatorio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-red">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center><h1 class="modal-title" id="titulom">Modal title</h1></center>
      </div>
      <div class="modal-body" id="bodym">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="bt_edt_rel">Salvar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop