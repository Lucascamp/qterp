@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Layouts</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/guia/cadastrolayout">Layouts</a>
            </li>
            <li class="active">
                <strong>Edição de Layout das Programações</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Layouts</h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" id="idLay" value="{{ $layout->id }}">
                        <div id="mensagens">
                        </div>
                        <table class="table table-bordered tabela">
                            <tr>
                                <td colspan="10" class="tbtitulo">Dados do Cliente</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <select class=" input_select form-control" id="clientes">
                                        @if(isset($layout->cliente_id))
                                            <option value="{{$layout->cliente_id}}">{{$clientes[$layout->cliente_id]}}</option>
                                        @endif
                                        @foreach ($clientes as $key => $element)
                                            <option value="{{$key}}">{{$element}}</option>    
                                        @endforeach
                                    </select>
                                </td>
                                <td colspan="3" id="nome_reduz">Nome: </td>
                                <td colspan="1" id="estado">Estado: </td>
                                <td colspan="3" id="municipio">Municipio: </td>
                            </tr>
                            <tr>
                                <td colspan="10" class="tbtitulo">Dados do Layout</td>
                            </tr>
                            <tr>
                                <td>Programação</td>
                                <td><input type="text" class="form-control" id="programacao" value="{{isset($layout->programacao) ? $layout->programacao : ''}}"></td>
                                <td>Obra</td>
                                <td><input type="text" class="form-control" id="obra" value="{{isset($layout->obra) ? $layout->obra : ''}}"</td>
                                <td>Local Ensaio</td>
                                <td><input type="text" class="form-control" id="local_ensaio" value="{{isset($layout->local_ensaio) ? $layout->local_ensaio : ''}}"></td>
                                <td>Data</td>
                                <td><input type="text" class="form-control" id="data" value="{{isset($layout->data) ? $layout->data : ''}}"></td>
                                <td>Contratante/Fabricante</td>
                                <td><input type="text" class="form-control" id="contratante_fabricante" value="{{isset($layout->contratante_fabricante) ? $layout->contratante_fabricante : ''}}"></td>
                            </tr>
                            <tr>
                                <td>item</td>
                                <td><input type="text" class="form-control" id="item" value="{{isset($layout->item) ? $layout->item : ''}}"></td>
                                <td>Identificacao / Desenho / Isométrico</td>
                                <td><input type="text" class="form-control" id="iden_dese_iso" value="{{isset($layout->iden_dese_iso) ? $layout->iden_dese_iso : ''}}"></td>
                                <td>Mapa / Spool</td>
                                <td><input type="text" class="form-control" id="mapa_spool" value="{{isset($layout->mapa_spool) ? $layout->mapa_spool : ''}}"></td>
                                <td>Programção Fabricação</td>
                                <td><input type="text" class="form-control" id="prog_fabri" value="{{isset($layout->prog_fabri) ? $layout->prog_fabri : ''}}"></td>
                                <td>Junta</td>
                                <td><input type="text" class="form-control" id="junta" value="{{isset($layout->junta) ? $layout->junta : ''}}"></td>
                            </tr>
                            <tr>
                                <td>Posição de Soldagem</td>
                                <td><input type="text" class="form-control" id="posicao_soldagem" value="{{isset($layout->posicao_soldagem) ? $layout->posicao_soldagem : ''}}"></td>
                                <td>Material / Metal base</td>
                                <td><input type="text" class="form-control" id="metal_base" value="{{isset($layout->metal_base) ? $layout->metal_base : ''}}"></td>
                                <td>Diametro</td>
                                <td><input type="text" class="form-control" id="diametro" value="{{isset($layout->diametro) ? $layout->diametro : ''}}"></td>
                                <td>Espessura Nominal</td>
                                <td><input type="text" class="form-control" id="espessura" value="{{isset($layout->espessura) ? $layout->espessura : ''}}"></td>
                                <td>Reforço</td>
                                <td><input type="text" class="form-control" id="reforco" value="{{isset($layout->reforco) ? $layout->reforco : ''}}"></td>
                            </tr>
                            <tr>
                                <td>Espessura Menor</td>
                                <td><input type="text" class="form-control" id="espessura_menor" value="{{isset($layout->espessura_menor) ? $layout->espessura_menor : ''}}"></td>
                                <td>Espessura Maior</td>
                                <td><input type="text" class="form-control" id="espessura_maior" value="{{isset($layout->espessura_maior) ? $layout->espessura_maior : ''}}"></td>
                                <td>Soldador / Sinete Raiz</td>
                                <td><input type="text" class="form-control" id="Sold_sinete_raiz" value="{{isset($layout->Sold_sinete_raiz) ? $layout->Sold_sinete_raiz : ''}}"></td>
                                <td>Processo Raiz</td>
                                <td><input type="text" class="form-control" id="processo_raiz" value="{{isset($layout->processo_raiz) ? $layout->processo_raiz : ''}}"></td>
                                <td>Soldador / Sinete Ench/Acab</td>
                                <td><input type="text" class="form-control" id="sold_sinete_acab" value="{{isset($layout->sold_sinete_acab) ? $layout->sold_sinete_acab : ''}}"></td>
                            </tr>
                            <tr>
                                <td>Processo Ench/Acab</td>
                                <td><input type="text" class="form-control" id="processo_acab" value="{{isset($layout->processo_acab) ? $layout->processo_acab : ''}}"></td>
                                <td>Metal de Adição</td>
                                <td><input type="text" class="form-control" id="metal_adicao" value="{{isset($layout->metal_adicao) ? $layout->metal_adicao : ''}}"></td>
                                <td>Chanfro</td>
                                <td><input type="text" class="form-control" id="chanfro" value="{{isset($layout->chanfro) ? $layout->chanfro : ''}}"></td>
                                <td>Nivel / Classe Inspeção</td>
                                <td><input type="text" class="form-control" id="nivel_inspecao" value="{{isset($layout->nivel_inspecao) ? $layout->nivel_inspecao : ''}}"></td>
                                <td>Norma</td>
                                <td><input type="text" class="form-control" id="norma" value="{{isset($layout->norma) ? $layout->norma : ''}}"></td>
                            </tr>
                            <tr>
                                <td>Observações</td>
                                <td><input type="text" class="form-control" id="observacao" value="{{isset($layout->observacao) ? $layout->observacao : ''}}"></td>
                                <td>Inicio Descrição</td>
                                <td><input type="text" class="form-control" id="inicio_descricao" value="{{isset($layout->inicio_descricao) ? $layout->inicio_descricao : ''}}"></td>
                                <td>Criterio Aceite</td>
                                <td><input type="text" class="form-control" id="criterio_aceite" value="{{isset($layout->criterio_aceite) ? $layout->criterio_aceite : ''}}"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                        <a class="btn btn-primary" id="bt_salvar_layout">Salvar Layout</a>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop
