@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>GO: {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias</a>
            </li>
            <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>Inclusão de Dados</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
            <ul class="dropdown-menu">
                <li><a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a></li>
                <li><a href="{{ route('guia.editar', array($guia->id)) }}">Editar GO</a></li>
                <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/etiqueta">Etiqueta</a></li>
                <li><a href="/guia/{{$guia->id}}/capa">Capa</a></li>
                <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                <li><a href="/guia/{{$guia->id}}/relatorio">Relatorio</a></li>
            </ul>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="">
                <div class="ibox-content">
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" id="idguia" value="{{ $guia->id }}">
                    <table class="table table-bordered tabela" id="editable">
                        <thead>
                        </thead>
                        <tbody>
                            <table class="table table-bordered tabela">
                                <tr>
                                    <td class="tbtitulo" colspan="8">Jornada</td>
                                </tr>
                                <tr>
                                    <td class="tblabel">Hora entrada</td>

                                    <td class="tblabel"><input type="text" class=" mask-datetime form-control" id="hora_entrada" value="{{isset($entrada)?$entrada:'00-00-0000 00:00'}}"></td>
                                    <td class="tblabel">Hora Saida</td>
                                    <td class="tblabel"><input type="text" class=" mask-datetime form-control" id="hora_saida" value="{{isset($saida)?$saida:'00-00-0000 00:00'}}"></td>

                                    <td class="tblabel">Horas Contratadas</td>
                                    <td class="tblabel"><input type="time" class="form-control tbinput" id="horas_contratadas" value="{{isset($guia->horas_contratadas)?$guia->horas_contratadas:'00:00'}}"></td>
                                </tr>
                            </table>
                            <table class="table table-bordered tabela">
                                <tr>
                                    <td class="tbtitulo" colspan="8">Monitor de Area</td>
                                </tr>
                                <tr>
                                    <td class="tblabel">Monitor</td>

                                    <td class="tblabel">{!! Form::select('monitor_id', $monitor_id, isset($guia->monitor_id)?$guia->monitor_id : null, array('id' => 'monitor_id', 'class'=>'form-control chosen-select')) !!}</td>
                                    
                                </tr>
                            </table>
                            <table class="table table-bordered tabela">
                                
                                <tr>
                                    <td class="tbtitulo" colspan="10">Juntas</td>
                                </tr>
                                <tr>
                                    <td class="tbsubtitulo">Item</td>
                                    <td class="tbsubtitulo">Situação</td>
                                    <td class="tbsubtitulo">Junta</td>
                                    <td class="tbsubtitulo">Identificacao</td>
                                    <td class="tbsubtitulo">Spool</td>
                                    <td class="tbsubtitulo">Quant. Filmes</td>
                                    <td class="tbsubtitulo">Filmes Batidos</td>
                                    <td class="tbsubtitulo">Filmes Perdidos</td>
                                    <td class="tbsubtitulo">Tipo de Exposição</td>
                                    
                                    <td class="tbsubtitulo">Tempo Fonte Exposta</td>
                                </tr>

                                @foreach ($programacoes as $programacao)
                                    
                                    <tr  class="programacao zebras" id="{{$programacao->id}}">
                                        <input type="hidden" id="qfil{{$programacao->id}}" value="{{ $programacao->quantidade_filmes }}">
                                        <td class="tblabel">{{$programacao->item}}</td>
                                        <td class="tblabel">
                                            <select class="form-control tbinput situacao" id="situacao{{$programacao->id}}">
                                                <option>{{isset($programacao->executado) ? $programacao->executado : 'Selecione'}}</option>
                                                <option>OK</option>
                                                <option>N Realizada</option>
                                            </select>
                                        </td>
                                        <td class="tblabel">{{$programacao->junta}}</td>
                                        <td class="tblabel">{{$programacao->identificacao}}</td>
                                        <td class="tblabel">{{$programacao->spool}}</td>
                                        <td class="tblabel">{{$programacao->quantidade_filmes}}</td>
                                        <td class="tblabel"><input type="text" class="form-control tbinput filmebatido" id="filmes_batidos{{$programacao->id}}" value="{{isset($programacao->filmes_batidos)?$programacao->filmes_batidos:'0'}}"></td>
                                        <td class="tblabel"><input type="text" class="form-control tbinput" id="filmes_perdidos{{$programacao->id}}" value="{{isset($programacao->filmes_perdidos)?$programacao->filmes_perdidos:'0'}}" disabled></td>
                                        <td class="tblabel">
                                                
                                            <select class="form-control tbinput" id="tipoex{{$programacao->id}}">
                                                <option>{{isset($programacao->tipo_exposicao) ? $programacao->tipo_exposicao : 'Selecione'}}</option>
                                                <option>Normal</option>
                                                <option>Panoramico</option>
                                            </select>
                                        </td>
                                        
                                        <td class="tblabel"><input type="text" class="mask-time3 form-control tbinput" id="tempofonte{{$programacao->id}}" value="{{isset($programacao->tempo_exposicao)?$programacao->tempo_exposicao:'00:00:00'}}"></td>
                                    </tr>    
                                @endforeach
                                

                            </table>
                            <table class="table table-bordered tabela">
                                <tr>
                                    <td class="tbtitulo"  colspan="16">Juntas</td>
                                </tr>
                                <?php 
                                    $cont=0;
                                    $tam = count($juntas)
                                ?>
                                <tr>
                                    @for ($i = 0; $i < $tam; $i++)
                                        @if($cont >= 5)
                                            <?php $cont = 0; ?>
                                            <tr>
                                        @endif
                                        <td  class="tblabel">{{$juntas[$i]['descricao']}}</td>
                                        <td class="tblabel"><input type="text" id="junta{{$juntas[$i]['id']}}" class="junta tbinput form-control" value="{{ isset($producao[$juntas[$i]['id']] ) ? $producao[$juntas[$i]['id']] : '0'}}"></td>
                                        @if($cont >= 4)
                                            </tr>
                                        @endif
                                        <?php $cont++ ?>
                                    @endfor 
                                </tr>
                            </table>
                            <table class="table table-bordered tabela">
                                <tr>
                                    <td class="tbtitulo" colspan="8">Informações Adicionais</td>
                                </tr>
                                <tr>
                                    <td class="tbsubtitulo" colspan="8">TRANSLADO / MOBILIZACAO</td>
                                </tr>
                                <tr>
                                    <td class="tblabel">Placa</td>
                                    <td class="tblabel"><input type="text" class="mask-placa form-control tbinput" id="placa" value="{{isset($guia->placa)?$guia->placa:''}}"></td>
                                    <td class="tblabel">KM Inicial</td>
                                    <td class="tblabel"><input type="text" class="form-control tbinput" id="km_inicial" value="{{isset($guia->km_inicial)?$guia->km_inicial:''}}"></td>
                                    <td class="tblabel">KM Final</td>
                                    <td class="tblabel"><input type="text" class="form-control tbinput" id="km_final" value="{{isset($guia->km_final)?$guia->km_final:''}}"></td>
                                    <td class="tblabel">KM Percorrido</td>
                                    <td class="tblabel"><input type="text" class="form-control tbinput" id="km_percorrido" disabled></td>
                                </tr>
                                <tr>
                                    <td class="tbsubtitulo" colspan="8">Informações para Diario de Obra</td>
                                </tr>
                                <tr>
                                    <td class="tblabel">Ocorrencias</td>
                                    <td class="tblabel"  colspan="2"><input type="text" class="form-control" id="Ocorrencias" value="{{isset($guia->ocorrencias)?$guia->ocorrencias:''}}"></td>
                                    <td class="tblabel">Ultimo Relatorio Cliente</td>
                                    <td class="tblabel"  colspan="2"><input type="text" class="form-control" id="Relatorios" value="{{isset($guia->relatorios)?$guia->relatorios:''}}"></td>
                                    <td class="tblabel">Protocolos</td>
                                    <td class="tblabel" ><input type="text" class="form-control" id="Protocolos" value="{{isset($guia->protocolos)?$guia->protocolos:''}}"></td>
                                </tr>
                                <tr>
                                    <td class="tbsubtitulo" colspan="8">Observações para Relatorio</td>
                                </tr>
                                <tr>
                                    <td class="tblabel" colspan="8"><input type="text" class="form-control" id="observacoes" value="{{isset($guia->observacoes)?$guia->observacoes:''}}"></td>
                                </tr>

                            </table>
                        </tbody>
                    </table>
                     <a class="btn btn-primary" id="bt_salvar_dados">Salvar</a> 
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop