		</tbody>
	</table>
	<table border="0"  width="100%" class="legenda_relatorio" cellspacing="0">
		<tr>
			<?php $obs = []; ?>
			<td width="18%" rowspan="10" valign="top"><b>OBSERVAÇÕES:</b><br><br>{{ isset($guia->observacoes) ? $guia->observacoes : null }} <br>
				
				@foreach ($progs as $element)
					
					@if (!empty($element['observacao']))
						<?php
							$obs[] = $element['observacao']; 
						?>
					@endif
				@endforeach
				
				<?php $obs = array_unique($obs);?>

				@if (!empty($obs))
					@foreach ($obs as $element)
						{{$element}}<br>
					@endforeach
				@endif
				

			</td>
			<td rowspan="10" width="1%" align="center">
				C<BR>
				Ó<BR>
				D<BR>
				I<BR>
				G<BR>
				O<BR>
				S<BR>
				<td align="center" heigth="10" width="14%" ><b>TÉCNICA DE ENSAIO</b></td>
				<td align="center" width="1%">&nbsp;</td>
				<td colspan="2" align="center"><b>DIMENSÕES DO FILME</b></td>
				<td colspan="5"  align="center"><b>DESCONTINUIDADES</b></td>
				<td colspan="6"  align="center"><b>RESULTADOS</b></td>
			</tr>
			<tr>
				<td rowspan="2" bordercolor="#808080" style="border-style: solid; border-width: 1px" width="14%">PSVS: (Parede Simples Vista Simples)</td>
				<td rowspan="2" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">A</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">3.½&quot; x 5.½&quot;</td>
				<td align="center" bordercolor="#808080" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">MO</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="13%">MORDEDURA EXTERNA</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">IE</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="12%">INCLUSÃO ESCÓRIA</td>
				<td align="center" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">	NR</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5" >REPARO PARCIAL</td>
			</tr>
			<tr>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">B</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">3.½&quot; x 8.½&quot;</td>
				<td align="center" bordercolor="#808080" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">FF</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="13%">FALTA DE FUSÃO</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">PO</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="12%">POROSIDADE</td>
				<td align="center" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">	NE</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5">APÓS ESMERILHAMENTO</td>
			</tr>
			<tr>
				<td rowspan="2" bordercolor="#808080" style="border-style: solid; border-width: 1px" width="14%">PDVS: (Parede Dupla Vista Simples)</td>
				<td rowspan="2" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">C</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">3.½&quot; x 17&quot;</td>
				<td align="center" bordercolor="#808080" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">FP</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="13%">FALTA DE PENETRAÇÃO</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">	TR</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="12%">TRINCA</td>
				<td align="center" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">NT</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5">REPARO TOTAL</td>
			</tr>
			<tr>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">D</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">4.½&quot; x 8.½&quot;</td>
				<td align="center" bordercolor="#808080" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">	CO</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="13%">CONCAVIDADE</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">	PV</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="12%">PORO VERMICULAR</td>
				<td align="center" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">	NX</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5">REPETIÇÃO POR ERRO OPERACIONAL</td>
			</tr>
			<tr>
				<td rowspan="2" bordercolor="#808080" style="border-style: solid; border-width: 1px" width="14%">PDVD: (Parede Dupla Vista Dupla)</td>
				<td rowspan="2" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">E</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">4.½&quot; x 17&quot;</td>
				<td align="center" bordercolor="#808080" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">EL</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="13%">ESCÓRIA EM LINHA</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">MR</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="12%">MORDEDURA NA RAIZ</td>
				<td align="center" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">RX</td>
				<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5">CONFIRMAÇÃO DE DEFEITO</td>
			</tr>
			<tr>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">F</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">7&quot; x 8.½&quot;</td>
				<td align="center" width="1%">&nbsp;</td>
				<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">
					N</td>
					<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="13%">
						SEM DESCONTINUIDADE</td>
						<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="1%">
							HB</td>
							<td style="border-style: solid; border-width: 1px" bordercolor="#808080" width="12%">
								PASSE OCO</td>
								<td align="center" width="1%">&nbsp;</td>
								<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">AM</td>
								<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5">AUMENTO DE AMOSTRAGEM</td>
							</tr>
							<tr>
								<td rowspan="2" bordercolor="#808080" style="border-style: solid; border-width: 1px" width="14%">PSVS/P: (Vista Panorâmica)</td>
								<td rowspan="3" width="1%">&nbsp;</td>
								<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">G</td>
								<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">7&quot; x 17&quot;</td>
								<td align="center" bordercolor="#808080" width="1%">&nbsp;</td>
								<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px" width="1%">	
									IT</td>
									<td bordercolor="#808080" style="border-style: solid; border-width: 1px" width="13%">
										INCLUSÃO DE TUNGSTÊNIO</td>
										<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px" width="1%">
											EP</td>
											<td bordercolor="#808080" style="border-style: solid; border-width: 1px" width="12%">
												EXCESSO DE PENETRAÇÃO</td>
												<td align="center" width="1%">&nbsp;</td>
												<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">EX</td>
												<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5">RADIOGRAFIA PARA DELIMITAÇÃO DE DEFEITO</td>
											</tr>
											<tr>
												<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">H</td>
												<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">8&quot; x 14&quot;</td>
												<td align="center" width="1%" style="border-right-style: none; border-right-width: medium">&nbsp;</td>
												<td align="center" bordercolor="#808080" style="border-left-style:none; border-left-width:medium; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px" width="27%" colspan="4">	
													ABREVIAÇÕES</td>
													<td align="center" width="1%" style="border-left-style: none; border-left-width: medium">&nbsp;</td>
													<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">	DEC</td>
													<td style="border-style: solid; border-width: 1px" bordercolor="#808080" colspan="5">ENSAIO COMPLEMENTAR</td>
												</tr>
												<tr>
													<td bordercolor="#808080" width="14%">&nbsp;</td>
													<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="2%">I</td>
													<td align="center" style="border-style: solid; border-width: 1px" bordercolor="#808080" width="8%">14&quot; x 17&quot;</td>
													<td width="1%">&nbsp;</td>
													<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px" width="27%" colspan="4">	
														ESP( ESPESSURA), ACAB(ACABAMENTO), 
														ENCH(ENCHIMENTO)</td>
														<td width="1%">&nbsp;</td>
														<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px">A</td>
														<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px">APROVADO</td>
														<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px">R</td>
														<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px">REPROVADO</td>
														<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px">NC</td>
														<td align="center" bordercolor="#808080" style="border-style: solid; border-width: 1px">NÃO CONCLUSIVO</td>
														
													</tr>
												</table>
											</table>

											<table border="0" width="100%" height="50" style="font-size: 11px; border-top: 1px solid black; border-bottom: 1px solid black;" class="rodape_relatorio" cellspacing="0">
												<tr>
													<td valign="bottom" align="center" width="250" height="20"><b>INSPETOR NÍVEL II:</b> {{$funcionariosnome[$equipe[0]->ins]}}</td>
													<td valign="bottom" align="center" width="171" height="20" style="border-right-style: solid; border-right-width: 1px"><b>DATA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</b></td>
													<td valign="bottom" align="center" width="165" height="20" style="border-left-style: solid; border-left-width: 1px"><b>CONTROLE DA QUALIDADE</b></td>	
													<td valign="bottom" align="center" width="164" height="20" style="border-right-style: solid; border-right-width: 1px"><b>DATA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</b></td>	
													<td valign="bottom" align="center" width="161" height="20" style="border-left-style: solid; border-left-width: 1px"><b>FISCALIZAÇÃO</b></td>
													<td valign="bottom" align="center" width="161" height="12"><b>DATA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</b></td>
												</tr>
												<tr>
													<td valign="top" align="center" colspan="2" style="border-right-style: solid; border-right-width: 1px">&nbsp;</td>
													<td valign="top" align="center" width="329" colspan="2" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px" rowspan="2">&nbsp;</td>	
													<td valign="top" align="center" width="322" colspan="2" style="border-left-style: solid; border-left-width: 1px" rowspan="2">&nbsp;</td>
												</tr>
												<tr>
													<td valign="bottom" align="center" colspan="2" style="border-right-style: solid; border-right-width: 1px">
														<p align="left"><span style="font-size: 9px !important">SNQC/END N°: 
															{{$funcionariossnqc[$equipe[0]->ins]}}</span></td>
														</tr>
													</table>
