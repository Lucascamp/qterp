@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Visualiação {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guia Operacional</a>
            </li>
             <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>Relatorio</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">
            <a href="#" class="btn btn-primary" id="bt_salvar_novorelatorio">Salvar Relatorio</a>
        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
            <ul class="dropdown-menu">
                <li><a href="#" id="impriRelatorio">Imprimir Relatorio</a></li>
                <li class="divider"></li>
                <li><a href="{{ route('guia.editar', array($guia->id)) }}">Editar GO</a></li>
                <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/etiqueta">Etiquetas</a></li>
                <li><a href="/guia/{{$guia->id}}/capa">Capa</a></li>
                <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/relatorio">Relatorios</a></li>
            </ul>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    <input type="hidden" id="guia_id" value="{{ $guia->id }}">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>DADOS DE CABEÇALHO</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-gears"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Padrão</a></li>
                                        <li><a href="#">Limpar</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="ibox-content">
                                
                                    <div class="col-md-4">
                                        <label for="cliente">Cliente</label>                                      
                                        <input type="text" placeholder="Cliente" class="form-control col-md-4" id="cliente" value="{{ $guia->cliente->nome }}">
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label for="cliente">Projeto</label>                                      
                                        <input type="text" class="form-control" placeholder="Projeto" id="projeto" value="{{ $guia->projeto }}">
                                    </div>

                                    <div class="col-md-3">
                                        <label for="cliente">Contratante/Fabricante</label>                                      
                                        <input type="text" class="form-control" placeholder="Contratante/Fabricante" id="contratante" value="{{ $guia->contratante_fabricante }}">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="cliente">Local</label>                                      
                                        <input type="text" class="form-control" placeholder="Local" id="local" value="{{ $guia->local_ensaio }}">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="cliente">Norma</label>
                                        {!! Form::select('norma', $normas,null,array('id' => 'select_norma',  'class'=>'form-control')) !!}   
                                    </div>

                                    <div class="col-md-2">
                                        <label for="cliente">Criterio Aceitação</label>                                      
                                        <input type="text" class="form-control" placeholder="Criterio Aceitação" id="criterio" value="">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="cliente">Programação Cliente</label>                                      
                                        <input type="text" class="form-control" placeholder="N° programação" id="progcliente" value="{{ $guia->programacao_cliente }}">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="cliente">N° Relatorio Cliente</label>     

                                        <input type="text" class="form-control" placeholder="N° Relatorio" id="numerorelatoriocliente" value="{{isset($num_rel_cli) ? $num_rel_cli : ''}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="cliente">Data</label>                                      
                                        <input type="date" class="form-control" placeholder="Data" id="datarel">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="col-lg-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>RELATORIO</h5>
                                <div class="ibox-tools">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-gears"></i>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li id="limpar"><a href="#">Limpar</a></li>
                                            <li><a href="#" id="bt_incluir_todas">Incluir Todas</a></li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="zebra3" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th height="25">Nº Fil.</th>
                                            <th height="25">Pos. Fil.</th>
                                            <th height="25">Ident.</th>
                                            <th height="25">Spool</th>
                                            <th height="25">Junta</th>
                                            <th height="25">Procedimento</th>
                                            <th width="100" height="25">Descontinuidade<br><small>(tam)</small></th>
                                            <th height="25">Laudo</th>
                                        </tr>
                                    </thead>    
                                    <tbody id="tbody">
                                       <td colspan="20" height="70" class="drop exc">Arraste um item para preencher o relatorio</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>









                    <div class="col-lg-2" id="area_itens">
                        @foreach ($itens as $item)                       
                            <div class="ibox float-e-margins itens" id="item{{$item['id']}}" >
                                <div class="ibox-title" id="titulo{{$item['id']}}">
                                    <h5>{{$item['identificacao']}} - {{$item['junta']}}</h5>
                                </div>
                            </div>
                         @endforeach
                    </div>












                
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="Modalrelatorio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-red">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center><h1 class="modal-title" id="titulom">Modal title</h1></center>
      </div>
      <div class="modal-body" id="bodym">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="bt_edt_rel">Salvar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>


@stop