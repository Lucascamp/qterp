	<br>
	<table style="min-width: 1060px;" width="100%" class="header" id="relatorio" >
		<tr>
			<td width="10%" rowspan="7"><h1 style="text-align: left">
				<img border="0" src="/img/img_logo_proposta.png" width="135" height="45">
			</td>
			<td rowspan="7" width="225pt" ><h1 style="text-align: center">RELATÓRIO DE RADIOGRAFIA</h1></td>
			</tr>
			<tr>
				<?php 
					$A = explode ("-",$relatorio[$rel]->data);
					$V_data = $A[2] . "/". $A[1] . "/" . $A[0];	
				 ?>
				<input type="hidden" id="num_rel_cli" value="{{ $guia->relatorios }}">
				<td width="140" style="font-size: 10px;" valign="top"><b>Nº RELATÓRIO:  </b> {{$rel}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</td>
			</tr>
			<tr><td style="font-size: 10px;" valign="top"><b>DATA: </b>{{$V_data}}</td></tr>						
			<tr><td style="font-size: 10px; " valign="top"><b>Nº GO:</b> {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</td></tr>
			<tr><td width="140" style="font-size: 10px; " valign="top"><b>Nº REL. CLIENTE: </b>{{$relatorio[$rel]->numero_rel_cliente}}</td></tr>
			<tr><td width="140" style="font-size: 10px; " valign="top"><b>PROPOSTA:</b> {{ $guia->proposta->cod_proposta }}</td></tr>
			<tr><td style="font-size: 10px; " valign="top"><b>SOLICITAÇÃO: </b>{{$relatorio[$rel]->solicitacao_cliente}}</td></tr>
		</table>


		<table style="width: 100%" class="cabecalho_relatorio">
			<tr height="10pt"></tr>
			<tr>
				<td width="33%"><b>CLIENTE: </b>{{ substr($guia->cliente->nome, 0, 50) }}</td>
				<td width="33%"><b>PROJETO: </b>{{ $relatorio[$rel]->projeto }}</td>
				<td><b>PROCEDIMENTO: </b> {{implode(' + ',$procedimentos[$rel])}}</td>
			</tr>

			<tr>
				<td><b>CONTRATANTE/FABRICANTE: </b>{{ $relatorio[$rel]->contratante_fabricante}}</td>
				<td><b>LOCAL: </b>{{ $relatorio[$rel]->local}}</td>
				@if ($tipo == 2)
					<td><b>NORMA / CRITERIO DE ACEITACAO: </b>{{isset($relatorio[$rel]->norma) ? $relatorio[$rel]->norma : '' }} / {{!empty($relatorio[$rel]->criterio) ? $relatorio[$rel]->criterio : '-'}}</td>
				@else	
					<td><b>NORMA / CRITERIO DE ACEITACAO: </b>{{isset($nomenormas[$relatorio[$rel]->norma]) ? $nomenormas[$relatorio[$rel]->norma] : '' }} / {{!empty($relatorio[$rel]->criterio) ? $relatorio[$rel]->criterio : '-'}}</td>
				@endif
				
			</tr>
		</table>

		<table border="0" width="100%"  class="cabecalho_relatorio">
			<tr>
				<td height="15"><b>QTD FILMES POR CASSETE: </b><br>{{ $guia->quantidade_filmes_cassete }}</td>
				<td><b>CONDIÇÃO SUPERFICIAL: </b><br>{{ $guia->condicao_superficial }}</td>
				<td><b>ECRAN ANT.: </b><br>{{ $guia->ecran_anterior }}&quot;</td>
				<td><b>ECRAN POST.: </b><br>{{ $guia->ecran_posterior }}&quot;</td>
				<td><b>TEMPERATURA DA PEÇA </b>(graus):<br>{{ $guia->temperatura }}</td>
				<td><b>TEMPERATURA REVELAÇÃO</b>(graus):<br> {{ $guia->temperatura_revelacao }}</td>
				<td><b>TEMPO REVELAÇÃO</b>(min):<br> {{ $guia->tempo_revelacao }}</td>
			</tr>
			<tr>
				<td><b>ISOTOPO: </b><br>{{ $guia->fonte->isotopo }}</td>
				<td><b>IRRADIADOR: </b><br>{{ $guia->fonte->irradiador }}</td>
				<td height="20"><b>Nº FONTE: </b><br>{{ $guia->fonte->numero }}</td>
				<td><b>ATIVIDADE: </b><br>{{ $guia->fonte_atividade }}</td>
				<td><b>FOCO: </b><br>{{ $guia->fonte->foco }}</td>
				<td><b>OPERADOR: </b>SNQC/END N°: <br>{{$funcionariossnqc[$equipe[0]->op1]}} - {{$funcionariosnome[$equipe[0]->op1]}}</td>
				<td colspan="2">&nbsp;</td>
			</tr>
		</table>