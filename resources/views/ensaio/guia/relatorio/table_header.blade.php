<table class="zebra3" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th height="25">Nº Fil.</th>
			<th height="25">Pos. Fil.</th>
			<th height="25">Ident.</th>
			<th height="25">Spool</th>
			<th height="25">Junta</th>
			<th height="25">Pos. Sold.</th>
			<th width="20" height="15">Ø ou Comp.<br></th>
			<th width="30" height="50">Esp.<br>(mm)</th>
			<th width="20" height="25">Ref.<br>(mm)</th>
			<th height="25">Metal base</th>
			<th width="20" height="25">Dim. Filme</th>
			{{-- <th height="25">Metal adição</th> --}}
			<th width="50" height="25">Sinete raiz</th>
			<th width="50" height="25">Sinete Ench-Acab</th>
			<th height="25">Processo</th>
			<th height="20">DFF(mm)</th>
			<th width="70" height="25">Tipo IQI</th>
			<th width="20" height="25">Classe Filme</th>
			<th height="25">Técnica</th>
			<th width="100" height="25">Descontinuidade<br><small>(tam)</small></th>
			<th height="25">Laudo</th>
			
			{{-- <th height="10">Chanfro</th> --}}
		</tr>
	</thead>	
	<tbody>
