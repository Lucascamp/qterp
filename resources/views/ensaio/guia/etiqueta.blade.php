@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

 

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Etiquetas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias</a>
            </li>
            <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>Etiquetas</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
            <ul class="dropdown-menu">
                <li><a href="#" id="btetiqueta">Imprimir Etiqueta</a></li>
                <li class="divider"></li>
                <li><a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a></li>
                <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/capa">Capas</a></li>
                <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                <li><a href="/guia/{{$guia->id}}/relatorio">Relatorio</a></li>
            </ul>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="">
                <div class="ibox-content">

                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Configurações</h5>
                        </div>
                        <div class="ibox-content">
                            <select id="colunas">
                                <option value="2">2 Coluna</option>
                                <option value="1">1 Coluna</option>
                            </select>
                            <select id="modelo">
                                <option value="1">Modelo 1</option>
                                <option value="2">Modelo 2</option>
                            </select>
                        </div>
                    </div>
                


 <input type="hidden" id="token" value="{{ csrf_token() }}">
 <input type="hidden" id="guia" value="{{  $guia->id }}">



                    <div class="area_impressao" name="area_impressao" id="area_impressao">
                        <br>
                        <div id="table">
                        <table class="etiqueta" cellspacing="0" cellpadding="0" id="tableetiquetas">
                        <?php $contfilmes = 0; $contcolunas = 1; $linhas=1; ?>
                        @foreach ($programacoes as $programacao)
                            <?php 
                                $filmes = intval($programacao->quantidade_filmes);        
                                // if(isset($filme) and $filme > $filmes)
                                //     $filme=1;
                                // elseif(isset($filme))
                                //     $filme++;
                                // else
                                //     $filme=1;
                            ?>
                            @if($contcolunas == 1)
                                <tr>
                            @endif
                                <td>
                                    <div class="bordaredonda" style="border-style:solid; border-width:1px ; position: relative; margin-left: 35px; width: 8cm; height: 1.6cm; z-index: 2;" id="etiqueta">
                                        <div style="position: absolute; width:  80px; height: 13px; z-index: 15; left:   3px; top:  2px;" id="label_logo"><img width="61" height="13" src="/img/img_logo_etiqueta.png"></div>
                                        <div style="position: absolute; width: 120px; height: 22px; z-index: 15; left:  67px; top:  1px; font-size: 6pt" id="label_cliente"><b>CLIENTE: </b>{{ substr($guia->cliente->nome,0,31) }}</div>
                                        <div style="position: absolute; width: 110px; height: 22px; z-index: 15; left: 190px; top:  1px; font-size: 6pt" id="label_obra"><b>OBRA: </b>{{substr($programacao->obra,0,25)}}</div>
                                        <div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 260px; top:  1px; font-size: 6pt" id="label_filme"></div>
                                        <div style="position: absolute; width: 127px; height: 13px; z-index: 15; left:   3px; top: 24px; font-size: 6pt" id="label_data"><b>DATA: </b>{{ $guia->data->format('d/m/Y') }}</div>
                                        <div style="position: absolute; width: 100px; height: 13px; z-index: 15; left:  75px; top: 24px; font-size: 6pt" id="label_espessura"><b>ESP: </b>{{$programacao->espessura}}</div>
                                        <div style="position: absolute; width: 180px; height: 13px; z-index: 15; left: 125px; top: 24px; font-size: 5pt" id="label_identificacao"><b>IDENT / SPOOL: </b>{{substr($programacao->identificacao.' / '.$programacao->spool,0,30)}}</div>
                                        <div style="position: absolute; width:  94px; height: 13px; z-index: 15; left:   3px; top: 34px ; font-size: 6pt" id="label_posicao"><b>POS. S.: </b>{{$programacao->posicao_soldagem}}</div>
                                        <div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  65px; top: 34px; font-size: 6pt" id="label_junta"><b>JUN: </b>{{$programacao->junta}}</div>
                                        <div style="position: absolute; width: 128px; height: 13px; z-index: 15; left: 145px; top: 34px; font-size: 6pt" id="label_soldador"><b>SOLD: </b>{{$programacao->sinete_raiz}} | {{$programacao->sinete_acabamento}}</div>
                                        <div style="position: absolute; width:  94px; height: 13px; z-index: 15; left: 249px; top: 34px; font-size: 6pt" id="label_diametro"><b>Ø: </b>{{$programacao->diametro}}</div>
                                        <div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  3px;  top: 46px; font-size: 6pt" id="label_operador"><b>OPER: </b>{{ $funcionarios[$equipe[0]->op1] }}</div>
                                        <div style="position: absolute; width:  109px; height: 13px; z-index: 15; left: 65px; top: 46px; font-size: 6pt" id="label_material"><b>MAT: </b>{{ substr($programacao->metal_base, 0, 19) }}</div>
                                        <div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 170px; top: 46px; font-size: 6pt" id="label_filme"><b>GO: {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</div>
                                        <div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 238px; top: 46px; font-size: 6pt" id="label_go"><b>SEQ.</b>{{$programacao->isometrico}}</div>
                                        <div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 280px; top: 46px; font-size: 6pt" id="label_go"><b>F </b>{{$programacao->numero_filme}}</div>
                                    </div>
                                    @if($linhas % 14 == 0)
                                        <div class="page-break"></div><br>
                                    @endif
                                </td>    
                                @if($contcolunas > 1)
                                    </tr>
                                    <?php $contcolunas=1; $linhas++; ?>

                                @elseif($contcolunas < 2)
                                    </td>
                                    <?php $contcolunas++; ?>

                                @endif

                                    <?php $contfilmes++; ?>                             

                            @endforeach 
                        </table> 
                        </div>   
                    </div> <!-- fim area de impressao-->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop