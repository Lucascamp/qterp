@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Relatório Diário de Obra</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias</a>
            </li>
            <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>RDO</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
                <ul class="dropdown-menu">
                    <li><a href="#" id="btRDO">Imprimir RDO</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a></li>
                    <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                    <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                    <li class="divider"></li>
                    <li><a href="/guia/{{$guia->id}}/etiqueta">Etiquetas</a></li>
                    <li><a href="/guia/{{$guia->id}}/capa">Capas</a></li>
                    <li><a href="/guia/{{$guia->id}}/relatorio">Relatorio</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox-content">
                    
                        <div align="center"><br><br>
                            <div class="area_impressao" name="area_impressao" id="area_impressao">
                            <table border="0" style="border-left:1px solid #808080; border-right:1px solid #808080; border-top:1px solid #808080; border-bottom:1px solid #808080; width: 1090px; height: 1400px; ">
                                <tr>
                                    <td colspan="2" rowspan="4"  align="center"><img border="0" src="/img/img_logo_proposta.png" width="135" height="45"></td>
                                    <td colspan="7" rowspan="4"  align="center"><h1>RELATÓRIO DIÁRIO DE OBRA</h1></td>
                                    <td width="1"></td>
                                    <td width="144">COD RDO:</td>
                                    <td align="center" width="1">{{ $guia->id }}/</font>{{ $guia->unidade }}/{{ $guia->data->format('Y') }}</td>
                                </tr>
                                <tr>
                                    <td width="1"></td>
                                    <td width="144">DATA:</td>
                                    <td align="center" width="1">{{ $guia->entrada->format('d/m/Y') }}</td>
                                </tr>
                                <tr>
                                    <td width="1"></td>
                                    <td width="144"></td>
                                    <td align="center" width="1"></td>
                                </tr>
                                <tr>
                                    <td width="1"></td>
                                    <td width="10">PROPOSTA:</td>
                                    <td width="140" align="center" width="1">{{ $guia->proposta->cod_proposta }}</td>
                                </tr>
                                <tr>
                                    <td width="45"></td>
                                    <td colspan="5"><b>CLIENTE:</b>{{ $guia->cliente->nome }}</td>
                                    <td colspan="3"><b>LOCAL: </b>{{ $guia->local_ensaio }}</td>
                                    <td width="1"></td>
                                    <td width="144"></td>
                                    <td align="center" width="1"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="4"><b>PROJETO: </b>{{$guia->projeto}}</td>
                                    <td colspan="4"></td>
                                    <td width="1"></td>
                                    <td width="144"></td>
                                    <td width="1"></td>
                                </tr>
                                <tr>
                                    <td height="20">&nbsp;</td>
                                    <td width="137"></td>
                                    <td></td>
                                    <td width="123"></td>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td colspan="2"></td>
                                    <td width="1"></td>
                                    <td width="144"></td>
                                    <td width="1"></td>
                                </tr>
                                <tr>
                                    <td  align="center"><b>ITEM</b></td>
                                    <td colspan="8"  align="center"><b></b></td>
                                    <td></td>
                                    <td colspan="2"  align="center"><b>REGISTRO FISCALIZAÇÃO</b><p></td>
                                </tr>
                                <tr>
                                    <td align="center">1</td>
                                    <td colspan="4"><b>EQUIPE</b></td>
                                    <td colspan="4"><b>FUNÇÕES</b></td>
                                    <td rowspan="34" valign="top" width="1" ></td>
                                    <td colspan="2" rowspan="34" valign="top" style="border-style: solid; border-width: 1px"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="4">1. 
                                        @if(empty($equipe[0]->op1))
                                            -
                                        @else
                                            {{ $funcionarios[$equipe[0]->op1] }}
                                        @endif
                                    </td>
                                    <td colspan="2">Operador 1</td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="4">2. 
                                        @if(empty($equipe[0]->op2))
                                            -
                                        @else
                                            {{ $funcionarios[$equipe[0]->op2] }}
                                        @endif
                                    </td>
                                    <td colspan="2">Operador 2</td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="4">3. 
                                        @if(empty($equipe[0]->aux))
                                            -
                                        @else
                                            {{ $funcionarios[$equipe[0]->aux] }}
                                        @endif
                                    </td>
                                    <td colspan="2">Auxiliar</td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="4">4. 
                                        @if(empty($equipe[0]->ins))
                                            -
                                        @else
                                            {{ $funcionarios[$equipe[0]->ins] }}
                                        @endif
                                    </td>
                                    <td colspan="2">Inspetor</td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center" height="20"></td>
                                    <td width="137">&nbsp;</td>
                                    <td></td>
                                    <td width="123"></td>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center">2</td>
                                    <td colspan="8"><b>HORÁRIO</b></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="4" align="center"></td>
                                    <td colspan="4" align="center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td align="center" colspan="2">ENTRADA</td>
                                    <td align="center" colspan="2">SAIDA</td>
                                    <td align="center" colspan="2" style="border-bottom-style: none; border-bottom-width: medium">&nbsp;</td>
                                    <td align="center" colspan="2" style="border-bottom-style: none; border-bottom-width: medium">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="2" style="border-style: solid; border-width: 1px" height="30"  align="center">
                                        {{$guia->entrada->format('d/m/Y H:i:s')}}
                                    </td>
                                    <td colspan="2" style="border-style: solid; border-width: 1px"  align="center">
                                        {{$guia->saida->format('d/m/Y H:i:s')}}
                                    </td>
                                    <td colspan="2" style="border-style: none; border-width: medium; "  align="center"></td>
                                    <td colspan="2" style="border-style: none; border-width: medium; "  align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="2" align="center" style="border-bottom-style: none; border-bottom-width: medium">&nbsp;</td>
                                    <td colspan="2" align="center" style="border-bottom-style: none; border-bottom-width: medium">&nbsp;</td>
                                    <td colspan="2" align="center" style="border-top-style: none; border-top-width: medium; border-bottom-style: none; border-bottom-width: medium">&nbsp;</td>
                                    <td colspan="2" align="center" style="border-top-style: none; border-top-width: medium; border-bottom-style: none; border-bottom-width: medium">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center">3</td>
                                    <td colspan="8"><b>PRODUÇÃO</b></td>
                                </tr>
                                <?php
                                $conttr = 1;
                                ?>

                                @foreach($juntas as $key => $junta)
                                    @if($conttr == 1)
                                        <tr>
                                            <td align="center"></td>
                                    @elseif($conttr == 5)
                                        <?php  $conttr=1; ?>
                                        </tr>
                                        <tr>
                                            @if($junta['descricao'] != 'chapa')
                                                <td align="center"></td>
                                            @endif  
                                    @endif
                                    @if($junta['descricao'] == 'outros')
                                        <td align="right" width="137">Outros</td>
                                    @elseif($junta['descricao'] != 'Chapa')
                                        <td align="right" width="137">{{$junta['descricao']}}</td>  
                                    @else      
                                        <td align="right" width="137">Chapas</td>
                                    @endif     
                                    <td width="96" style="border-style: solid; border-width: 1px" height="30" align="center">
                                        {{ isset($producao[$junta['id']]) ? $producao[$junta['id']] : '0'}}
                                    </td>
                                    <?php $conttr++;  ?>

                                @endforeach
                                </tr>
                                <tr>
                                    <td align="center" height="20">&nbsp;</td>
                                    <td width="137"></td>
                                    <td></td>
                                    <td width="123"></td>
                                    <td></td>
                                    <td width="114"></td>
                                    <td></td>
                                    <td width="107"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center" width="8">4</td>
                                    <td colspan="8"><b>TOTAL DE FILME RADIOGRAFADO</b></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="5">Classe I</td>
                                    <td align="center" colspan="5">Classe II</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="3">
                                        <table>
                                            <?php $cont = 0 ?>
                                            @foreach ($filmes as $filme)
                                                @if($cont >= 4)
                                                    <?php $cont = 0; ?>
                                                    <tr>
                                                @endif
                                                @if($filme['classe'] == 0)
                                                    <td align="center" width="127">{{$filme['descricao']}}</td>
                                                    <td width="70" align="center" style="border-style: solid; border-width: 1px" height="30">
                                                        {{ isset($filclasseI[$filme['legenda']]) ? $filclasseI[$filme['legenda']] : '0' }}
                                                    </td>
                                                @endif
                                                @if($cont >= 4)
                                                    </tr>
                                                @endif
                                                <?php $cont++ ?>
                                            @endforeach
                                        </table>
                                    </td>
                                    <td></td>
                                    <td colspan="3">
                                        <table>
                                            <?php $cont = 0 ?>
                                            @foreach ($filmes as $filme)
                                                @if($cont >= 4)
                                                    <?php $cont = 0; ?>
                                                    <tr>
                                                @endif
                                                @if($filme['classe'] == 1)
                                                    <td align="center" width="127">{{$filme['descricao']}}</td>
                                                    <td width="70" align="center" style="border-style: solid; border-width: 1px" height="30">
                                                        {{ isset($filclasseII[$filme['legenda']]) ? $filclasseII[$filme['legenda']] : '0' }}
                                                    </td>
                                                @endif
                                                @if($cont >= 4)
                                                    </tr>
                                                @endif
                                                <?php $cont++ ?>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">5</td>
                                    <td colspan="8"><b>OCORRÊNCIA(S)</b></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="8" rowspan="3" style="border-style: solid; border-width: 1px" height="100">
                                        {{$guia->ocorrencias}}
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center">6</td>
                                    <td colspan="8"><b> RELATÓRIOS </b></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                        <td colspan="9" >
                            <table border="1" style="font-size:12px; text-align: center">
                                <th style="font-size:12px; text-align: center" colspan="2" width="100" >N° REL</th>
                                <th style="font-size:12px; text-align: center" width="100" >N° REL CLIENTE</th>
                                <th style="font-size:12px; text-align: center" align="center" style="font-size:12px" colspan="2" width="300" >PROGRAMACAO</th>
                                <th style="font-size:12px; text-align: center" align="center" style="font-size:12px" colspan="2" width="150" >QUANT. JUNTAS</th>
                                <th style="font-size:12px; text-align: center" align="center" style="font-size:12px" colspan="2" width="150" >QUANT. FILMES</th>
                                <th style="font-size:12px; text-align: center" align="center" style="font-size:12px" colspan="2" width="301" >OBRA</th>
                                <?php $totaljunta = 0; $totalfilme = 0; ?>

                                @foreach ($rels as $key => $element)
                                <?php 
                                    $arrayaux = explode('+', $element->itens);
                                    $quantijunta = count($arrayaux);
                                    $totaljunta += $quantijunta;
                                    $totalfilme += $quantidade[$key]->quanti;
                                ?>
                                    <tr>
                                        <td align="center" colspan="2">{{$element->numero_relatorio}}</td>
                                        <td align="center" >{{$element->numero_rel_cliente}}</td>
                                        <td align="center" colspan="2">{{$element->solicitacao_cliente}}</td>
                                        <td align="center" colspan="2">{{$quantijunta}}</td>
                                        <td align="center" colspan="2">{{$quantidade[$key]->quanti}}</td>
                                        <td align="center" colspan="2">{{$element->projeto}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td align="center" colspan="5">TOTAL</td>
                                    
                                    <td align="center" colspan="2">{{$totaljunta}}</td>
                                    <td align="center" colspan="2">{{$totalfilme}}</td>
                                    <td align="center" colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center">7</td>
                                    <td colspan="8"><b>OBSERVAÇÕES</b></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                    <td colspan="8" rowspan="3" style="border-style: solid; border-width: 1px" height="100">
                                        {{$guia->protocolos}}
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"  height="80" valign="top" style="border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px"><b>RESPONSÁVEL QUALITEC</b></td>
                                    <td align="center" colspan="4"  height="80" valign="top" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px"><b>RESPONSÁVEL CONTRATANTE</b></td>
                                    <td align="center" colspan="4"  height="80" valign="top" style="border-left-style: solid; border-left-width: 1px; border-top-style: solid; border-top-width: 1px"><b>RESPONSÁVEL FISCALIZAÇÃO</b></td>
                                </tr>
                            </table>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop