<table class="table table-bordered tabela">
    <tr>
        <td  class="tbtitulo" colspan="2">Equipe</td>
    </tr>
    
    <?php 
        $fields = ['op1' => 'Operador 1', 'op2' => 'Operador 2','aux' => 'Auxiliar', 'ins' => 'Inspetor'];
        $num=0;

    ?>

    @foreach ($fields as  $key => $field)    
       <tr>
            <td class="tblabel">
                <center>{{ $field }}</center>
            </td>
            <td class="tblabel">
                <select id="{{$key}}" name="{{$key}}" class="chosen-select">
                    @if(isset($input[$key]) && $input[$key] > 0)
                        <option value="{{$input[$key]}}">{{$funcionarios[$input[$key]]}}</option>
                    @elseif(isset($equipe[0]->$key))
                        <option value="{{$equipe[0]->$key}}">{{$funcionarios[$equipe[0]->$key]}}</option>
                    @endif
                    @foreach ($funcionarios as $key => $funcionario)
                        <option value="{{$key}}">{{$funcionario}}</option>
                    @endforeach
                </select>
            </td>
        </tr>
    @endforeach
</table>