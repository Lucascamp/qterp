<table class="table table-bordered tabela">
    <tr>
        <td  class="tbtitulo" colspan="4">Fonte</td>
    </tr>

    <tr>
        <td class="tblabel">Fonte</td>
        <td class="tblabel" colspan="3">
            @if(isset($input['fonte_id']))
                {!! Form::select('fonte_id', $fontes, $input['fonte_id'], array('id' => 'fonte', 'class'=>'form-control chosen-select')) !!}
            @else
            
                {!! Form::select('fonte_id', $fontes, isset($guia) ? $guia->fonte->id : null ,array('id' => 'fonte', 'class'=>'form-control chosen-select')) !!}
            @endif
        </td>
    </tr>

    <tr>    
        <td class="tblabel">Isótopo</td>
        <td class="tbitem"><b><p id="isotopo">
            @if(isset($guia->fonte))
                {{ $guia->fonte->isotopo }}
            @endif    
        </p></b></td>
        <td class="tblabel">Foco</td>

        <td class="tbitem"><b><p id="foco">
            @if(isset($guia->fonte))
                {{ $guia->fonte->foco }}
            @endif    
        </p></b></td>
    </tr>

    <tr>    
        <td class="tblabel">Atividade</td>
        <td class="tbitem"><b><p id="atividade">
            @if(isset($guia->fonte))
                {{ $guia->fonte_atividade }}
            @endif    
        </p></b></td>
        {!! Form::hidden('fonte_atividade', isset($guia->fonte_atividade) ? $guia->fonte_atividade : null, array('id' => 'fonte_atividade')) !!}

        <td class="tblabel">Irradiador</td>
        <td class="tbitem"><b><p id="irradiador">
            @if(isset($guia->fonte))
                {{ $irradiadores_todos[$guia->fonte->irradiador] }}
            @endif 
        </p></b></td>
    </tr>
</table>