<table class="table table-bordered tabela">
    <tr>
        <td  class="tbtitulo" colspan="6">Cliente</td>
    </tr>
    <tr>
        <td class="tblabel">Cliente(*)</td>
        <td colspan="3">
            @if(isset($input))
                {!! Form::select('cliente_id', $clientes, isset($input['cliente_id']) ? $input['cliente_id'] : null ,array('id' => 'cliente', 'class'=>'chosen-select')) !!}    
            @else
                {!! Form::select('cliente_id', $clientes, isset($guia->cliente->id) ? $guia->cliente->id : null ,array('id' => 'cliente', 'class'=>'chosen-select')) !!}
            @endif
            
        </td>
        <td class="tblabel">Proposta(*)</td>
        <td>
            @if(isset($input))

                 {!! Form::select('proposta_id', isset($propostas) ? $propostas : ['0' => 'Propostas'] ,isset($input['proposta_id']) ? $input['proposta_id'] : null ,array('id' => 'proposta', 'class'=>'chosen-select')) !!}
            @else
                {!! Form::select('proposta_id', isset($propostas) ? $propostas : ['0' => 'Propostas'], 
            isset($guia->cliente) ? $guia->cliente->proposta_id : null ,array('id' => 'proposta', 'class'=>'chosen-select')) !!}
            @endif
            
        </td>
    </tr>
    <tr>    
        <td class="tblabel">Nome</td>
        <td class="tbitem"><b><p id="nome_reduz">
            @if(isset($guia->cliente))
                {{ $guia->cliente->nome_reduz }}
            @endif    
            </p>
        </b></td>

        <td class="tblabel">Código</td>
        <td class="tbitem"><b><p id="codigo">
           @if(isset($guia->cliente))
                {{ $guia->cliente->codigo }}
           @endif  
           </p>  
       </b></td>

       <td class="tblabel">CNPJ</td>
       <td class="tbitem"><b><p id="cnpj">
           @if(isset($guia->cliente))
                {{ $guia->cliente->cnpj }}
           @endif    
           </p>    
       </b></td>
    </tr>

    <tr>    
        <td class="tblabel">Endereço</td>
        <td class="tbitem"><b><p id="endereco">
            @if(isset($guia->cliente))
                {{ $guia->cliente->endereco }} - {{ $guia->cliente->bairro }} - {{ $guia->cliente->municipio }}
            @endif  
            </p>
        </b></td>

        <td class="tblabel">Estado</td>
        <td class="tbitem"><b><p id="estado">
            @if(isset($guia->cliente))
            {{ $guia->cliente->estado }}
            @endif
            </p>
        </b></td>

        <td class="tblabel">Contato</td>
        <td class="tbitem"><b><p id="contato">
            @if(isset($guia->cliente))
                {{ $guia->cliente->ddd }} - {{ $guia->cliente->tel }}
            @endif  
            </p>
        </b></td>
    </tr>
</table>