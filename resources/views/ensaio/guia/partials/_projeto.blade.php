<table class="table table-bordered tabela">
    <tr>
        <td  class="tbtitulo" colspan="4">Projeto</td>
    </tr>

    <tr>
        <td class="tblabel">Projeto</td>
        <td class="tblabel" colspan="3">
        	@if(isset($input))
                {!! Form::text('projeto', isset($input['projeto']) ? $input['projeto'] : null, ['class' => 'form-control', 'id' => 'projeto']) !!}
            @else
                {!! Form::text('projeto', isset($guia) ? $guia->projeto : null, array('class'=>'form-control','id'=>'projeto')) !!}
            @endif
        	 
        </td>
    </tr>

</table>