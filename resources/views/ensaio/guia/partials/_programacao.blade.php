<table class="table table-bordered tabela">
    <tr>
        <td  class="tbtitulo" colspan="6">Programação</td>
    </tr>

    <tr>
        <td class="tblabel">Itens</td>
        <td class="tblabel">
            {!! Form::text('itens', null ,array('id' => 'quantitens', 'class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Selecione o arquivo:</td>
        <td class="tblabel">
            @if(isset($input['arquivo']))
                <span>programação carregada:</span> {{$input['arquivo']->getClientOriginalName()}}</br></br>
                 {!! Form::file('arquivo',array('id' => 'arquivo')) !!}
            @else
                {!! Form::file('arquivo',array('id' => 'arquivo')) !!}
            @endif
            {!! Form::submit('Carregar Arquivo', array('id' => 'bt_ler_arquivo', 'class' => 'btn btn-primary')) !!}
            {!! Form::close()!!}
        </td>
        <td>
            Programação em: 
        </td>
        <td>
            <div class="col-sm-10">
                @if(isset($importprogramacao))
                    @if ($input['optionsRadios'] == 'mili')
                        <label> <input type="radio" checked="" value="mili" id="mili" name="optionsRadios" class="radio"> Milimetros</label>
                        <br>
                        <label> <input type="radio" value="pol" id="pol" name="optionsRadios" class="radio"> Polegadas</label>
                    @else
                        <label> <input type="radio"  value="mili" id="mili" name="optionsRadios" class="radio"> Milimetros</label>
                        <br>
                        <label> <input type="radio" checked="" value="pol" id="pol" name="optionsRadios" class="radio"> Polegadas</label>
                    @endif
                @else
                    <label> <input type="radio" checked="" value="mili" id="mili" name="optionsRadios" class="radio"> Milimetros</label>
                    <br>
                    <label> <input type="radio" value="pol" id="pol" name="optionsRadios" class="radio"> Polegadas</label>
                @endif
            </div>
        </td>
    </tr>

    <tr>
       
            <table class="table table-bordered tabela2">                <tr>
                    <td class="tbtitulo" colspan="5">Dados da Programação</td>
                </tr>
                <tr>
                    <td class="tbsubtitulo">Local</td>
                    <td class="tbsubtitulo">Contratante/Fabricante</td>
                    <td class="tbsubtitulo">Data</td>
                    <td class="tbsubtitulo">Controle CLiente</td>
                </tr>
                <tr>
                    
                    <td class="tblabel">
                        
                        <select  id="local" class ="form-control">
                            
                            <option value="QUALITEC">QUALITEC</option>
                            <option value="CLIENTE">CLIENTE</option>
                            
                        </select>
                        
                    </td>
                    <td class="tblabel"><input type="text" id="contratante_fabricante" class="form-control" value="{{isset($contratante_fabricante) ? $contratante_fabricante : ''}}"></td>
                    <td class="tblabel"><input type="date" id="data_prog" class="form-control" value="{{isset($data_prog) ? $data_prog : ''}}"></td>
                    <td class="tblabel"><input type="text" id="controle_cliente" class="form-control" value="{{isset($controle_cliente) ? $controle_cliente : ''}}"></td>
                </tr>
            </table>

    </tr>
    <tr>
    <table class="table table-bordered tabela2">
        <thead>
            <tr text-aling="center">
                <th>Tipo de Peça</th>
                <th>Item</th>
                <th>Identificação</th>
                <th>Spool</th>
                <th>Obra</th>
                <th>Junta</th>
                <th>Pos. Sold.</th>
               {{--  <th>Reforço</th> --}}
                <th>Metal base</th>
                <th id="cabDiCo">Diâmetro / Comp. (mm)</th>
                <th>Esp. (mm)</th>
                <th>Chanfro</th>
                <th>Sold Raiz</th>
                <th>Proc. Raiz</th>
                <th>Sold. Acab.</th>
                <th>Proc. Acab. / Ench</th>
                <th>Norma</th>
                <th>Criterio de Aceitação</th>
                <th>Nivel Insp.</th>
                <th>Obs.</th>
                <th>Tecnica</th>
            </tr>
        </thead>
        <tbody id="replicador">
           <tr>
                <td></td>
                <td>Rep.</td>
                <td>{!! Form::text('repidentificacao', null, ['size' => '17', 'class' => 'repetidor', 'id' => 'identificacao']) !!}</td>
                <td>{!! Form::text('repspool', null, ['size' => '3', 'class' => 'repetidor', 'id' => 'spool' ]) !!}</td>
                <td>{!! Form::text('repobra', null, ['size' => '10', 'class' => 'repetidor', 'id' => 'obra']) !!}</td>
                <td>{!! Form::text('repjunta', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'junta']) !!}</td>
                <td>{!! Form::text('repposicao_soldagem', null, ['size' => '3', 'class' => 'repetidor', 'id' => 'posicao_soldagem']) !!}</td>
                {{-- <td>{!! Form::text('repreforco', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'reforco']) !!}</td> --}}
                <td>{!! Form::text('repmetal_base', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'metal_base']) !!}</td>
                <td>{!! Form::text('repdiametro', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'diametro']) !!}</td>
                <td>{!! Form::text('repespessura', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'espessura']) !!}</td>
                <td>{!! Form::text('repchanfro', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'chanfro']) !!}</td>
                <td>{!! Form::text('repsinete_raiz', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'sinete_raiz']) !!}</td>
                <td>{!! Form::text('repprocesso_raiz', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'processo_raiz']) !!}</td>
                <td>{!! Form::text('repsinete_acabamento', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'sinete_acabamento']) !!}</td>
                <td>{!! Form::text('repprocesso_acabamento', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'processo_acabamento']) !!}</td>

                <td>
                        <select name="repnorma" size="30" id="norma" class="repetidorNorma chosen-select">
                            @foreach ($normas as $key =>  $element)
                                <option value="{{$key}}">{{$element}}</option>
                            @endforeach
                        </select>
                    </td>
                {{-- <td>{!! Form::select('repnorma', $normas, null,array('id' => 'norma', 'size'=>'30', 'class'=>'repetidorNorma chosen-select')) !!}</td> --}}

                {{-- <td>{!! Form::text('repnorma', null, ['size' => '15', 'class' => 'repetidor', 'id' => 'norma']) !!}</td> --}}
                {{-- <td>{!! Form::text('repaplicacao', null, ['size' => '10', 'class' => 'repetidor', 'id' => 'aplicacao']) !!}</td> --}}
                <td>{!! Form::text('repcriterio_aceite', null, ['size' => '15', 'class' => 'repetidor', 'id' => 'criterio_aceite']) !!}</td>
                <td>{!! Form::text('repnivel_inspecao', null, ['size' => '5', 'class' => 'repetidor', 'id' => 'nivel_inspecao']) !!}</td>
                <td>{!! Form::text('repobservacao', null, ['size' => '15', 'class' => 'repetidor', 'id' => 'observacao']) !!}</td>
                <td id="repTec"></td>
            </tr> 
        </tbody>
        @if(isset($programacoes))
        <tbody>
            @foreach($programacoes as $programacao)
                <tr id="linha{{$programacao->item}}">
                    <input type="hidden" id="item{{$programacao->item}}" class="item" value="{{$programacao->item}}'">
                    <td>{!! Form::select('tipo', $tipo, $programacao->tipo ,array('id' => 'tipo'.$programacao->item, 'class'=>'tipo')) !!}</td>
                    <td>{{ $programacao->item }}</td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][identificacao]', $programacao->identificacao,['size' => '5','class'=>'identificacao','id'=>'identificacao{{$programacao->item}}']) !!}</td> --}}
                    <td><input type="text" size="17" class="identificacao" id="identificacao{{$programacao->item}}" value="{{$programacao->identificacao}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][spool]', $programacao->spool, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="3" class="spool" id="spool{{$programacao->item}}" value="{{$programacao->spool}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][obra]', $programacao->obra, ['size' => '15']) !!}</td> --}}
                    <td><input type="text" size="10" class="obra" id="obra{{$programacao->item}}" value="{{$programacao->obra}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][junta]', $programacao->junta, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="junta" id="junta{{$programacao->item}}" value="{{$programacao->junta}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][posicao_soldagem]', $programacao->posicao_soldagem, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="3" class="posicao_soldagem" id="posicao_soldagem{{$programacao->item}}" value="{{$programacao->posicao_soldagem}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][reforco]', $programacao->reforco, ['size' => '5']) !!}</td> --}}
                    {{-- <td><input type="text" size="5" class="reforco" id="reforco{{$programacao->item}}" value="{{$programacao->reforco}}"></td> --}}
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][metal_base]', $programacao->metal_base, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="metal_base" id="metal_base{{$programacao->item}}" value="{{$programacao->metal_base}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][diametro]', $programacao->diametro, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="diametro" id="diametro{{$programacao->item}}" value="{{$programacao->diametro}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][espessura]', $programacao->espessura, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="espessura" id="espessura{{$programacao->item}}" value="{{$programacao->espessura}}"></td>
                    <td><input type="text" size="5" class="chanfro" id="chanfro{{$programacao->item}}" value="{{$programacao->chanfro}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][sinete_raiz]', $programacao->sinete_raiz, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="sinete_raiz" id="sinete_raiz{{$programacao->item}}" value="{{$programacao->sinete_raiz}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][processo_raiz]', $programacao->processo_raiz, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="processo_raiz" id="processo_raiz{{$programacao->item}}" value="{{$programacao->processo_raiz}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][sinete_acabamento]', $programacao->sinete_acabamento, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="sinete_acabamento" id="sinete_acabamento{{$programacao->item}}" value="{{$programacao->sinete_acabamento}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][processo_acabamento]', $programacao->processo_acabamento, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="processo_acabamento" id="processo_acabamento{{$programacao->item}}" value="{{$programacao->processo_acabamento}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][norma]', $programacao->norma, ['size' => '5']) !!}</td> --}}
                    <?php $arraynorma = explode('+',$programacao['norma']); ?>
                    <td>{!! Form::select('norma', $normas,$arraynorma,array('id' => 'norma'.$programacao["item"], 'size'=>'30', 'class'=>'norma chosen-select','multiple')) !!}</td>
                    {{-- <td><input type="text" size="15" class="norma" id="norma{{$programacao->item}}" value="{{$programacao->norma}}"></td> --}}
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][aplicacao]', $programacao->aplicacao, ['size' => '5']) !!}</td> --}}
                    {{-- <td><input type="text" size="10" class="aplicacao" id="aplicacao{{$programacao->item}}" value="{{$programacao->aplicacao}}"></td> --}}
                    <td><input type="text" size="15" class="criterio_aceite" id="criterio_aceite{{$programacao->item}}" value="{{$programacao->criterio_aceitacao}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][nivel_inspecao]', $programacao->nivel_inspecao, ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="nivel_inspecao" id="nivel_inspecao{{$programacao->item}}" value="{{$programacao->nivel_inspecao}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao->id.'][observacao]', $programacao->observacao, ['size' => '15']) !!}</td> --}}
                    <td><input type="text" size="15" class="observacao" id="observacao{{$programacao->item}}" value="{{$programacao->observacao}}"></td>
                    <td class="tecnica" id="tecnica{{$programacao['item']}}">{{$programacao->tecnica}}</td>
                </tr>    
            @endforeach
            {!! Form::hidden('itenscadastrados', $programacoes[count($programacoes)-1]->item ,array('id' => 'itenscadastrados', 'class'=>'form-control')) !!}
        </tbody>
        @elseif(isset($importprogramacao))
              <tbody>
            @foreach($importprogramacao as $programacao)
                <tr id="linha{{$programacao['item']}}">
                    <input type="hidden" id="item{{$programacao['item']}}" class="item" value="{{$programacao['item']}}'">
                    <td>
                        <select class="tipo" id="tipo{{$programacao['item']}}">
                            <option value="0">Tubular</option><option value="1">Plano</option>{{-- <option value="2">Multiforme</option> --}}
                        </select>
                    </td>
                    <td>{{ $programacao['item'] }}</td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][identificacao]', $programacao['identificacao'],['size' => '5','class'=>'identificacao','id'=>'identificacao{{$programacao['item']}}']) !!}</td> --}}
                    <td><input type="text" size="17" class="identificacao" id="identificacao{{$programacao['item']}}" value="{{$programacao['iden_dese_iso']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][spool]', $programacao['spool'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="3" class="spool" id="spool{{$programacao['item']}}" value="{{$programacao['mapa_spool']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][obra]', $programacao['obra'], ['size' => '15']) !!}</td> --}}
                    <td><input type="text" size="10" class="obra" id="obra{{$programacao['item']}}" value="{{$programacao['obra']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][junta]', $programacao['junta'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="junta" id="junta{{$programacao['item']}}" value="{{$programacao['junta']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][posicao_soldagem]', $programacao['posicao_soldagem'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="3" class="posicao_soldagem" id="posicao_soldagem{{$programacao['item']}}" value="{{$programacao['posicao_soldagem']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][reforco]', $programacao['reforco'], ['size' => '5']) !!}</td> --}}
                    {{-- <td><input type="text" size="5" class="reforco" id="reforco{{$programacao['item']}}" value="{{$programacao['reforco']}}"></td> --}}
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][metal_base]', $programacao['metal_base'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="metal_base" id="metal_base{{$programacao['item']}}" value="{{$programacao['metal_base']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][diametro]', $programacao['diametro'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="diametro" id="diametro{{$programacao['item']}}" value="{{$programacao['diametro']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][espessura]', $programacao['espessura'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="espessura" id="espessura{{$programacao['item']}}" value="{{$programacao['espessura']}}"></td>
                    <td><input type="text" size="5" class="chanfro" id="chanfro{{$programacao['item']}}" value="{{$programacao['chanfro']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][sinete_raiz]', $programacao['sinete_raiz'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="sinete_raiz" id="sinete_raiz{{$programacao['item']}}" value="{{$programacao['sold_sinete_raiz']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][processo_raiz]', $programacao['processo_raiz'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="processo_raiz" id="processo_raiz{{$programacao['item']}}" value="{{$programacao['processo_raiz']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][sinete_acabamento]', $programacao['sinete_acabamento'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="sinete_acabamento" id="sinete_acabamento{{$programacao['item']}}" value="{{$programacao['sold_sinete_acab']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][processo_acabamento]', $programacao['processo_acabamento'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="processo_acabamento" id="processo_acabamento{{$programacao['item']}}" value="{{$programacao['processo_acab']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][norma]', $programacao['norma'], ['size' => '5']) !!}</td> --}}

<?php $arraynorma = explode('+',$programacao['norma']);?>
                    <td>{!! Form::select('norma', $normas,$arraynorma,array('id' => 'norma'.$programacao["item"], 'size'=>'30', 'class'=>'norma chosen-select','multiple')) !!}</td>
                        {{-- {!! Form::select('norma', $normas,$programacao['norma'],array('id' => 'norma'.$programacao["item"], 'size'=>'30', 'class'=>'norma chosen-select','multiple')) !!}</td> --}}
                    {{-- <td><input type="text" size="15" class="norma" id="norma{{$programacao['item']}}" value="{{$programacao['norma']}}"></td> --}}
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][aplicacao]', $programacao['aplicacao'], ['size' => '5']) !!}</td> --}}
                    {{-- <td><input type="text" size="10" class="aplicacao" id="aplicacao{{$programacao['item']}}" value=""></td> --}}
                    <td><input type="text" size="15" class="criterio_aceite" id="criterio_aceite{{$programacao['item']}}" value="{{$programacao['criterio_aceite']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][nivel_inspecao]', $programacao['nivel_inspecao'], ['size' => '5']) !!}</td> --}}
                    <td><input type="text" size="5" class="nivel_inspecao" id="nivel_inspecao{{$programacao['item']}}" value="{{$programacao['nivel_inspecao']}}"></td>
                    {{-- <td>{!! Form::text('progs['.$programacao['id'].'][observacao]', $programacao['observacao'], ['size' => '15']) !!}</td> --}}
                    <td><input type="text" size="15" class="observacao" id="observacao{{$programacao['item']}}" value="{{$programacao['observacao']}}"></td>
                    <td class="tecnica" id="tecnica{{$programacao['item']}}"></td>
                </tr>    
            @endforeach
            {!! Form::hidden('itenscadastrados', $importprogramacao[count($importprogramacao)-1]['item'] ,array('id' => 'itenscadastrados', 'class'=>'form-control')) !!}
        </tbody>
        @endif

        
        
        
            <tbody id="tableprogs">   

            </tbody>
        
    </table>

    @if(!isset($programacoes))
        {!! Form::hidden('itenscadastrados', 0 ,array('id' => 'itenscadastrados', 'class'=>'form-control')) !!}
    @endif    

</tr>
</table>
