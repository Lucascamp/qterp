<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Guia Operacional {{isset($guia) ? $guia->id.'/'.$guia->unidade.'/'.$guia->data->format('Y') :'' }}</td>
    </tr>
    <tr>
        <td class="tblabel">Unidade(*)</td>
        <td>
            @if(isset($input))
                {!!Form::select('unidade', $unidades, isset($input['unidade']) ? $input['unidade'] : $unidade, array('id' => 'unidade', 'class'=>'form-control chosen-select'))!!}
            @else
                {!!Form::select('unidade', $unidades, isset($guia) ? $guia->unidade : $unidade, array('id' => 'unidade', 'class'=>'form-control chosen-select'))!!}
            @endif
        </td>
        <td class="tblabel">Data(*)</td>
        <td>
        @if(isset($input))
            <input type="date" name="data" class="form-control" id="data" value="{{ isset($input['data']) ? $input['data'] : '' }}">
        @else
            @if(isset($guia))
                <input type="date" name="data" class="form-control" id="data" value="{{ $guia->data->format('Y-m-d') }}">
            @else    
           	    <input type="date" name="data"  id="data" class="form-control">
            @endif
        @endif
        </td>
    </tr>
</table>