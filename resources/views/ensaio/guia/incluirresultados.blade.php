@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>GO: {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias</a>
            </li>
            <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>Inclusão de Resultados</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
            <ul class="dropdown-menu">
                <li><a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a></li>
                <li><a href="{{ route('guia.editar', array($guia->id)) }}">Editar GO</a></li>
                <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/etiqueta">Etiqueta</a></li>
                <li><a href="/guia/{{$guia->id}}/capa">Capa</a></li>
                <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                <li><a href="/guia/{{$guia->id}}/relatorio">Relatorio</a></li>
            </ul>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="">
                <div class="ibox-content">
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" id="idguia" value="{{ $guia->id }}">
                    <table class="table table-bordered tabela" id="editable">
                        <thead>
                        </thead>
                        <tbody>
                            <table class="table table-bordered tabela">
                                <tr>
                                    <td class="tbtitulo" colspan="14">Revelação</td>
                                </tr>
                                <tr>
                                    <td>QUANTIDADE FILMES POR CASSETE:</td>
                                    <td width="60" ><input type="text" class="form-control" id="quantidadefilmescassete" value="1"></td>
                                    <td>ECRAN ANTERIOR:</td>
                                    <td width="80" align="center"><input type="text" class="form-control" id="ecranant" value="{{($programacoes[0]->espessura < 50 ) ? '0,005' : '0,010'}}"></td>
                                    <td>ECRAN POSTERIOR:</td>
                                    <td width="80" align="center"><input type="text" class="form-control" id="ecranpos"  value="0,010"></td>
                                    <td>TEMPERATURA PEÇA:(graus)</td>
                                    <td align="center"><input type="text" class="form-control" id="tempec"  value="Ambiente"></td>                                    
                                </tr>
                                <tr>
                                    <td>CONDIÇÃO SUPERFICIAL:</td>
                                    <td align="center"><input type="text" class="form-control" id="condsupe"  value="Escovada"></td>
                                    <td>TEMPERATURA REVELAÇÃO:(graus)</td>
                                    <td align="center"><input type="text" class="form-control" id="temrev"  value="20"></td>
                                    <td>TEMPO REVELAÇÃO:(min)</td>
                                    <td align="center"><input type="text" class="form-control" id="temporev"  value="5 a 8"></td>
                                </tr>
                            </table>
                            <table class="table table-bordered tabela">
                                <tr>
                                    <td class="tbtitulo" colspan="14">Filmes</td>
                                </tr>
                                <tr>
                                    <td class="tbsubtitulo">Item</td>
                                    <td class="tbsubtitulo">ID Filme</td>
                                    <td class="tbsubtitulo">Pos.<br>Filme</td>
                                    <td class="tbsubtitulo">Identificação</td>
                                    <td class="tbsubtitulo">Junta</td>
                                    <td class="tbsubtitulo">Diâmetro</td>
                                    <td class="tbsubtitulo">Esp.</td>
                                    <td class="tbsubtitulo">Sinete Raiz</td>
                                    <td class="tbsubtitulo">Sinete Acabamento</td>
                                    <td class="tbsubtitulo">Descontinuidade</td>
                                    <td class="tbsubtitulo" width="10">Tamanho Desc</td>
                                    {{-- <td class="tbsubtitulo">Cod Inspeção</td> --}}
                                    <td class="tbsubtitulo">Resultado</td>
                                    <td class="tbsubtitulo">Observação</td>
                                </tr>
                                @foreach ($programacoes as $programacao)
                                    @if($programacao->executado == 'OK')
                                        <input type="hidden" id="qfil{{$programacao->id}}" value="{{ $programacao->quantidade_filmes }}">
                                        <tr class="programacao" id="{{$programacao->id}}">
                                            <td class="tblabel">{{$programacao->item}}</td>
                                            <td class="tblabel">{{$programacao->isometrico}}</td>
                                            <td class="tblabel">F{{$programacao->numero_filme}}</td>
                                            <td class="tblabel">{{$programacao->identificacao}}</td>
                                            <td class="tblabel">{{$programacao->junta}}</td>
                                            <td class="tblabel">{{$programacao->diametro}}</td>
                                            <td class="tblabel">{{$programacao->espessura}}</td>
                                            <td class="tblabel">{{$programacao->sinete_raiz}}</td>
                                            <td class="tblabel">{{$programacao->sinete_acabamento}}</td>
                                            <td class="tblabel">
                                                <?php 
                                                    $arraydesc = explode(' - ',$programacao->descontinuidade);
                                                ?>
{!! Form::select('norma', $descontinuidades,!empty($programacao->descontinuidade) ?  $arraydesc : 'N',array('id' => 'descontinuidade'.$programacao->id,  'class'=>'chosen-select','multiple')) !!}
                                               
                                            </td>
                                            <td width="100"><input type="text" class="form-control" id="tam_desc{{$programacao->id}}" value="{{isset($programacao->tamanho_descontinuidade)?$programacao->tamanho_descontinuidade:''}}"></td>
                                            <td class="tblabel">
                                                <?php $ap = 'A'; $aprovado = 'Aprovado' ?>
                                                <select id="resultado{{$programacao->id}}" class="form-control">
                                                    <option value="{{!empty($programacao->resultado) ? $programacao->resultado : $ap }}">
                                                        {{ !empty($programacao->resultado) ? $programacao->resultado : $aprovado }}
                                                    </option>
                                                    <option value="A">Aprovado</option>
                                                    <option value="R">Reprovado</option>
                                                    <option value="NC">Não Conclusivo</option>
                                                </select>
                                            </td>
                                            <td class="tblabel"><input type="text" class="form-control tbinput" id="observacao{{$programacao->id}}" value="{{isset($programacao->observacao)?$programacao->observacao:'0'}}"></td>
                                        </tr>
                                    @endif    
                                @endforeach
                            </table>
                        </tbody>
                    </table>
                     <a class="btn btn-primary" id="bt_salvar_resultados">Salvar</a> 
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop