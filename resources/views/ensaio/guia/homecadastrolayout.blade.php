@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Guia Operacional</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li class="active">
                <strong>Cadastro de Layout das Programações</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            <a href="/guia/cadastrarlayout" class="btn btn-primary">Novo Layout</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Layouts </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <div id="mensagens">
                        </div>
                        <table class="table table-bordered tabela">
                            <thead>
                            </thead>
                            <tbody>
                                <table class="table table-bordered tabela" >
                                    <thead>
                                        <tr>
                                            <th class="tbtitulo" colspan="6">Layouts Existentes</td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td class="tbsubtitulo">Id Layout</td>
                                        <td class="tbsubtitulo">Cliente</td>
                                        <td class="tbsubtitulo">Data</td>
                                        <td class="tbsubtitulo" colspan="2">Ações</td>
                                    </tr>
                                    <tbody id="listaPropostasH">
                                        @if (count($layouts) > 0)
                                            @foreach ($layouts as $layout)
                                            <tr>
                                                <td class="tblabel">{{$layout->id}}</td>
                                                <td class="tblabel">{{$clientes[$layout->cliente_id]}}</td>
                                                <td class="tblabel">{{$layout->created_at->format('d/m/Y')}}</td>
                                                <td class="tblabel">
                                                    <a href="/guia/{{$layout->id}}/editarlayout" data-toggle="tooltip" title="Editar" data-placement="top" class="legenda btn btn-info dim">
                                                    <i class="fa fa-pencil"></i></a>

                                                    <button data-toggle="tooltip" title="Excluir" data-placement="top" class="legenda btn btn-success bt_excluir_layout" id="{{$layout->id}}">
                                                    <i class="fa fa-eraser"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" class="tblabel" >Nenhum Layout Cadastrado</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </tbody>
                        </table>
                        </br>                                
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</div>
</div>
@stop
