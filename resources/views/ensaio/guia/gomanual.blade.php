@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Guia Operacional</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias Operacionais</a>
            </li>
            <li class="active">
                <strong>Cadastro</strong>
            </li>
        </ol>
    </div>
    
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de dados da Guia Operacional</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content no-padding full-width">
                  <input type="hidden" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" id="id_guia" value="{{ isset($guia_id)?$guia_id: null}}">
                  @include('ensaio.guia.partials._guia')

                    @include('ensaio.guia.partials._clientes', $clientes)

                    @include('ensaio.guia.partials._funcionarios', $funcionarios)

                    @include('ensaio.guia.partials._fontes', $fontes)

                    @include('ensaio.guia.partials._projeto')

                    <table class="table table-bordered tabela">
                    <tr>
       
            <table class="table table-bordered tabela2">                <tr>
                    <td class="tbtitulo" colspan="5">Dados da Programação</td>
                </tr>
                 <tr>
                    <td class="tbsubtitulo">Motivo entrada manual</td><td colspan="4"><input type="text" id="obsmanual" class="form-control" value="{{ isset( $obsmanual ) ? $obsmanual : '' }}"></td>
                </tr>
                <tr>
                    <td class="tbsubtitulo">Local</td>
                    <td class="tbsubtitulo">Contratante/Fabricante</td>
                    <td class="tbsubtitulo">Data</td>
                    <td class="tbsubtitulo">Controle CLiente</td>
                </tr>
                
                <tr>
                    <td class="tblabel">
                        <select  id="local" class ="form-control">
                            @if(isset($local_ensaio))
                                <option value="{{$local_ensaio}}">{{$local_ensaio}}</option>
                            @endif
                            <option value="QUALITEC">QUALITEC</option>
                            <option value="CLIENTE">CLIENTE</option>
                            
                        </select>
                    </td>
                    <td class="tblabel"><input type="text" id="contratante_fabricante" class="form-control" value="{{isset($contratante_fabricante) ? $contratante_fabricante : ''}}"></td>
                    <td class="tblabel"><input type="date" id="data_prog" class="form-control" value="{{isset($data_prog) ? $data_prog : ''}}"></td>
                    <td class="tblabel"><input type="text" id="controle_cliente" class="form-control" value="{{isset($controle_cliente) ? $controle_cliente : ''}}"></td>
                </tr>
            </table>

    </tr>
</table>


                    <div id="example1">

                    </div>
                    <div class="col-lg-12">
                    <p>
                        <button id="save" class="btn btn-primary">Salvar</button>
                    </p>
                    </div>     
                </div>
               
                </div>
            </div>
        </div>
    </div>
</br>
</br>
</br>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
<script src="/js/jquery2.1.js"></script>
 <script src="/js/gomanual.js"></script>
@stop
