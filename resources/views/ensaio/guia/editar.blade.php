@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Edição {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias Operacionais</a>
            </li>
             <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6">
        <div class="title-action">
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a></li>
                    <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                    <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                    <li><a href="#" id="impriGO">Imprimir GO</a></li>
                    <li class="divider"></li>
                    <li><a href="/guia/{{$guia->id}}/etiqueta">Etiquetas</a></li>
                    <li><a href="/guia/{{$guia->id}}/capa">Capa</a></li>
                    <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                    <li><a href="/guia/{{$guia->id}}/relatorio">Relatorio</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição de dados da Guia Operacional<small>(*)campos obrigatorios</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                 {!! Form::open(array('route' => 'guia.carregar', 'class'=>'form-inline', 'files' => true)) !!}
                    {!! Form::hidden('created_by', '1') !!}

                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" id="id_guia" value="{{ $guia->id }}">

                    <table class="table table-bordered tabela" id="editable">
                        <tbody>

                            @include('ensaio.guia.partials._guia')

                            @include('ensaio.guia.partials._clientes', $clientes)

                            @include('ensaio.guia.partials._funcionarios', array($funcionarios,$equipe))

                            @include('ensaio.guia.partials._fontes', $fontes)

                            @include('ensaio.guia.partials._projeto')

                            @include('ensaio.guia.partials._programacao', $programacoes)

                        </tbody>
                    </table>

                    <div class="pull-right">
                    
                    <a class="btn btn-primary" id="bt_carregar_procedimento" >Carregar dados de Procedimento</a> 
                    <a class="btn btn-primary" id="bt_salvar_guia" disabled>Salvar Alterações</a>
                    
                    
                    </div>

                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop
