@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Capas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
             <li>
                <a href="/ensaio/guia">Guias</a>
            </li>
            <li>
                <a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a>
            </li>
            <li class="active">
                <strong>Capas</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
                <ul class="dropdown-menu">
                    <li><a href="#" id="btcapa">Imprimir Capas</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('guia.visualizar', array($guia->id)) }}">Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</a></li>
                    <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                    <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                    <li class="divider"></li>
                    <li><a href="/guia/{{$guia->id}}/etiqueta">Etiquetas</a></li>
                    <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                    <li><a href="/guia/{{$guia->id}}/relatorio">Relatorio</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox-content">


                   
<?php
    $cont_impressao = count($programacoes);

    $contador = 0;
 ?>
<div class="area_impressao" name="area_impressao" id="area_impressao">
@foreach ($programacoes as $programacao)
<?php

    $filmes = intval($programacao->quantidade_filmes);

    $multiplicador = 0;
    

    if($filmes > 2)
        $multiplicador = $filmes-2;

        $pixels = $multiplicador*9;
        
?>
<div class="bordaredonda" style="margin-top: 5px; border-style:solid; border-width:1px; position: relative; width: 7.8cm; height: {{ 495+$pixels.'pt;' }}; z-index: 2; left: 28%;" id="capa" >

    <div style="position: absolute; width:  88px; height: 18px; z-index: 15; left:   3px; top: 4pt" id="logo"><img width="88" height="18" src="/img/img_logo_capa.png"></div>

    <div style="position: absolute; width: 193px; height: 18px; z-index: 15; left:  94px; top: 4pt; font-size: 6pt" id="endereco"><center>Qualitec Engenharia da Qualidade LTDA<br>Tel:+55(31)3288-1000-www.qualitecend.com.br</center></div>

    <div style="position: absolute; width: 282px; height: 23px; z-index: 15; left:   5px; top:  20pt; border-style:solid; border-width:1px;" id="label_cliente"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>CLIENTE:</b> <br>{{ $guia->cliente->nome }}</p></div>

    <div style="position: absolute; width: 282px; height: 23px; z-index: 27; left:   5px; top:  38pt; border-style:solid; border-width:1px;" id="label_contratante"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>CONTRATANTE/FABRICANTE: </b><br> {{$guia->contratante_fabricante}}</p></div>

    <div style="position: absolute; width: 70px; height: 23px; z-index: 15; left: 156px; top:  56pt; border-style:solid; border-width:1px" id="label_controlecliente"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>PROG.CLIENTE: </b><br>{{$guia->programacao_cliente}}</p></div>

    <div style="position: absolute; width: 60px; height: 23px; z-index: 15; left: 227px; top:  56pt; border-style:solid; border-width:1px" id="label_progfab"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>PROG.FABRIC: </b><br>{{$guia->programacao_fabricante}}</p></div>

    <div style="position: absolute; width: 150px; height: 23px; z-index: 15; left:   5px; top:  56pt; border-style:solid; border-width:1px" id="label_obra"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>OBRA:<br></b>{{substr($programacao->obra,0,35)}}</p></div>

    <div style="position: absolute; width: 282px; height: 23px; z-index: 15; left:   5px; top: 74pt; border-style:solid; border-width:1px;" id="label_identificacao"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>IDENTIFICAÇÃO / DESENHO / SPOOL</b><br>{{$programacao->identificacao}} / {{$programacao->spool}}</p><center><span style="font-size: 9px;"></span></center></div>

    <div style="position: absolute; width: 282px; height: 23px; z-index: 15; left:   5px; top: 92pt; border-style:solid; border-width:1px;" id="label_spool"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>MET. BASE: </b><br>{{$programacao->metal_base}}</p></div>

    <div style="position: absolute; width: 135px; height: 23px; z-index: 15; left: 5px; top: 128pt; border-style:solid; border-width:1px" id="label_material"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>JUNTA:</b><br>{{$programacao->junta}}</p></div>

    <div style="position: absolute; width:  45px; height: 23px; z-index: 28; left:  141px; top: 128pt; border-style:solid; border-width:1px" id="label_posicao"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>POS.SOL.:</b><br>{{$programacao->posicao_soldagem}}</p></div>

    <div style="position: absolute; width:  42px; height: 23px; z-index: 30; left:  5px; top: 147px; border-style:solid; border-width:1px" id="label_diametro"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>DIAM.Ø: </b><br>{{$programacao->diametro}}</p></div>

    <div style="position: absolute; width:  55px; height: 23px; z-index: 33; left:  49px; top: 147px; border-style:solid; border-width:1px" id="label_tecnica"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>TÉCNICA:</b><br>{{$programacao->tecnica}}</p></div>

    <div style="position: absolute; width:  87px; height: 23px; z-index: 26; left:  105px; top: 147px; border-style:solid; border-width:1px" id="label_procsold"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>PROC.SOLD.:</b> <br>{{$programacao->processo_raiz}}| {{$programacao->processo_acabamento}}</p></div>

    <div style="position: absolute; width:  50px; height: 23px; z-index: 15; left: 193px; top: 147px; border-style:solid; border-width:1px;" id="label_chanfro"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>CHANF:</b><br>{{$programacao->chanfro}}</p></div>

    <div style="position: absolute; width: 100px; height: 23px; z-index: 15; left: 187px; top: 128pt; border-style:solid; border-width:1px" id="label_soldador"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>SOLD. R | E/A: </b><br>{{$programacao->sinete_raiz}} | {{$programacao->sinete_acabamento}}</p></div>

    <div style="position: absolute; width:  42px; height: 23px; z-index: 15; left: 245px;  top: 147px; border-style:solid; border-width:1px" id="label_espessura"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>ESP.: </b><br>
    {{$programacao->espessura}}
    </p>
    </div>

    <div style="position: absolute; width:  81px; height: 23px; z-index: 15; left:   5px; top: 146pt; border-style:solid; border-width:1px;" id="label_fonte"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>FONTE:</b><br>{{ $guia->fonte->numero }}</p></div>

    <div style="position: absolute; width:  78px; height: 23px; z-index: 23; left:  87px; top: 146pt; border-style:solid; border-width:1px;" id="label_isotopo"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>ISÓTOPO: </b><br>{{ $guia->fonte->isotopo }}</p></div>

    <div style="position: absolute; width:  60px; height: 23px; z-index: 25; left: 166px; top: 146pt; border-style:solid; border-width:1px;" id="label_foco"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>FOCO: </b><br>{{ $guia->fonte->foco }}</p></div>

    <div style="position: absolute; width:  60px; height: 23px; z-index: 24; left: 227px; top: 146pt; border-style:solid; border-width:1px;" id="label_atividade"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>ATIVIDADE:  </b><br>{{ $guia->fonte_atividade }}</p></div>

    <div style="position: absolute; width: 282px; height:{{ 152+$pixels.'pt;' }} z-index: 22; left:   5px; top: 219px; border-style:solid; border-width:1px;" id="label_descontinuidades">
    <p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>DESCONTINUIDADES ENCONTRADAS</b></p>
        <table>
        <?php 
            $cont=0;
            $tam = count($descontinuidades)
        ?>
            @for ($i = 0; $i < $tam; $i++)
                @if($cont >= 2)
                    <?php $cont = 0; ?>
                    <tr>
                @endif
                    <td  class="" id="layer1"><p style="font-size: 5pt;height: 8pt;line-height: 8pt; padding: 3pt 2pt 2pt 2pt;">{{$descontinuidades[$i]['descricao']}} - {{$descontinuidades[$i]['legenda']}}</p></td>
                    <td class="tblabel"><div style="border-style:solid; border-width:1px; width: 15px; height: 15px; z-index: 1; padding-left:4px; padding-right:4px; padding-top:1px; padding-bottom:1px" id="box">&nbsp;</div></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                @if($cont >= 4)
                    </tr>
                @endif
                <?php $cont++ ?>
            @endfor
        </table>
    </div>
    <div style="position: absolute; width: 280px; height: 100px; z-index: 21; left: 6px; top: 390px" id="label_destontinuidadesfilme">

 <table>
    <?php $c=0;?>
    @foreach ($isome[$programacao->item] as $key => $element)
        @if($c >= 2)
            <?php $c = 0; ?>
            <tr>
        @endif
            <td>
                &nbsp; <font style='font-size: 6pt;'> F{{ $element->numero_filme }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
            </td>
        @if($c >= 4)
            </tr>
        @endif
        <?php $c++ ?>
    @endforeach
 </table>

    {{--  @foreach (range(1, $filmes) as $filme)
        &nbsp; <font style='font-size: 6pt;'> F{{ $filme }} </font><br>
     @endforeach  --}}

    <table width="98%">
    </table>
    </div>
    
    <div style="position: absolute; width: 83px; height: 23px; z-index: 20; left: 204px; top: {{ 317+$pixels.'pt;' }} border-style:solid; border-width:1px;" id="label_nivel"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>NÍVEL DE INSP:</b><br>{{$programacao->nivel_inspecao}}</p></div>
    <?php $arraynorma = array(); ?>
                <?php $normas = explode('+',$programacao->norma); ?>

                @foreach ($normas as $element)
                    @if (isset($nomenormas[$element]))
                        <?php $arraynorma[] = $nomenormas[$element]; ?>
                    @else
                        <?php $arraynorma[] = $element; ?>
                    @endif
                @endforeach

    <?php $stringnorma = implode(' + ', $arraynorma); ?>
    <div style="position: absolute; width: 198px; height: 23px; z-index: 20; left: 5px; top: {{ 317+$pixels.'pt;' }} border-style:solid; border-width:1px;" id="label_norma"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>NORMA:</b><br>{{$stringnorma}}</p></div>

    <div style="position: absolute; width: 282px; height: 23px; z-index: 15; left: 5px; top: {{ 335+$pixels.'pt;' }} border-style:solid; border-width:1px;" id="label_operador"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>OPERADOR:</b>
    
    <br>{{$funcionarios[$equipe[0]->op1]}} / SNQC:{{$funcionariossnqc[$equipe[0]->op1]}}
    
    </p></div>

    <div style="position: absolute; width: 282px; height: 54px; z-index: 18; left: 5px; top: {{ 353+$pixels.'pt;' }} border-style:solid; border-width:1px;" id="label_inspetor"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>INSPETOR DA QUALITEC - SNQC</b><br>{{$funcionarios[$equipe[0]->ins]}} - {{$funcionariossnqc[$equipe[0]->ins]}}</p>

        <div style="position: absolute; width: 74px; height: 64px; z-index: 1; left: 208px; top: 1px; font-size:6pt" id="label_laudo"><b>LAUDO:</b>
            <div style="position: absolute; width: 71px; height: 18px; z-index: 2; left: 3px; top: 36px" id="layer9">REPROV.    
                <div style="border-style:solid; border-width:1px; position: absolute; width: 12px; height: 12px; z-index: 2; left: 43px; top: -5px; padding-left:4px; padding-right:4px; padding-top:1px; padding-bottom:1px" id="box7">&nbsp;</div>
            </div>
            <div style="position: absolute; width: 70px; height: 18px; z-index: 2; left: 3px; top: 15px" id="layer8">APROV.     
                <div style="border-style:solid; border-width:1px; position: absolute; width: 12px; height: 12px; z-index: 1; left: 43px; top: -3px; padding-left:4px; padding-right:4px; padding-top:1px; padding-bottom:1px" id="box6">&nbsp;</div>
            </div>
        </div>
        <div style="position: absolute; width: 49px; height: 57px; z-index: 1; left: 150px; top: 1px; font-size:6pt" id="label_data"><b>DATA:</b>   </div>
    </div>
    <div style="position: absolute; width: 282px; height: 54px; z-index: 17; left: 5px; top: {{ 395+$pixels.'pt;' }} border-style:solid; border-width:1px;"  id="label_fiscalizacao"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>INSPETOR DA FISCALIZAÇÃO</b></p>

        <div style="position: absolute; width: 49px; height: 56px; z-index: 1; left: 150px; top: 1px; font-size:6pt" id="label_data0"><b>DATA:  </b></div>
        <div style="position: absolute; width: 74px; height: 61px; z-index: 1; left: 208px; top: 1px; font-size:6pt" id="label_laudo0"><b>LAUDO:</b>
            <div style="position: absolute; width: 72px; height: 15px; z-index: 2; left: 2px; top: 36px" id="layer10">REPROV.       
                <div style="border-style:solid; border-width:1px; position: absolute; width: 12px; height: 12px; z-index: 1; left: 43px; top: -5px; padding-left:4px; padding-right:4px; padding-top:1px; padding-bottom:1px" id="box8">&nbsp;</div>
            </div>
            <div style="position: absolute; width: 71px; height: 19px; z-index: 2; left: 2px; top: 15px" id="layer11">APROV.                    
                <div style="border-style:solid; border-width:1px; position: absolute; width: 12px; height: 12px; z-index: 1; left: 43px; top: -3px; padding-left:4px; padding-right:4px; padding-top:1px; padding-bottom:1px" id="box9">&nbsp;</div>
            </div>
        </div>
    </div>

    <div style="position: absolute; width: 282px; height: 47px; z-index: 16; left: 5px; top: {{ 437+$pixels.'pt;' }} border-style:solid; border-width:1px;" id="label_observacao"><p style="background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;"><b>OBSERVAÇÃO:</b><br>{{$programacao->observacao}}</p></div>
    
    <div style="position: absolute; width:  282px; height: 10px; z-index: 15; left:  5px; top: {{ 473+$pixels.'pt;' }}" id="label_item"><p style="text-align: center; background-color: #f0f0f0; font-size: 6pt;height: 8pt;line-height: 8pt; padding: 0 0 1pt 1pt;border-style:solid; border-width:1px"><b>CÓDIGO RELATÓRIO: </b></p></div>

    <div style="position: absolute; width:  90px; height: 11px; z-index: 15; left:  110px; top: {{ 482+$pixels.'pt;' }} font-size:6pt" id="label_go"><b>CÓD.GO: </b>{{ $guia->id }}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</div>

    <div style="position: absolute; width:  53px; height: 11px; z-index: 15; left: 5px; top: {{ 482+$pixels.'pt;' }} font-size:6pt" id="label_item"><b>ITEM: </b>{{$programacao->item}}</div>

    <div style="position: absolute; width: 120px; height: 13px; z-index: 15; left: 200px; top: {{ 482+$pixels.'pt;' }} font-size:6pt" id="label_filme"><b>SEQ.FILME: </b>@if ($isometrico[$programacao->item]->menor != $isometrico[$programacao->item]->maior)
        {{$isometrico[$programacao->item]->menor}} a {{$isometrico[$programacao->item]->maior}}
    @endif
    @if ($isometrico[$programacao->item]->menor == $isometrico[$programacao->item]->maior)
       {{$isometrico[$programacao->item]->menor}}
    @endif

</div>
</div>

<div class="page-break"></div>


@endforeach 

</div> <!-- fim area de impressao-->

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop