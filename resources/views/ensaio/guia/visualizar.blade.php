@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')


<input type="hidden" id="token" value="{{ csrf_token() }}">
<input type="hidden" id="idguia" value="{{ $guia->id }}">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Visualiação {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/ensaio">Ensaio</a>
            </li>
            <li>
                <a href="/ensaio/guia">Guias Operacionais</a>
            </li>
            <li class="active">
                <strong>Guia {{$guia->id}}/{{$guia->unidade}}/{{ $guia->data->format('Y') }}</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div>
            <ul class="dropdown-menu">
                <li><a href="{{ route('guia.editar', array($guia->id)) }}">Editar GO</a></li>
                <li><a href="{{ route('guia.incluirdados', array($guia->id)) }}">Incluir Dados de Retorno</a></li>
                <li><a href="{{ route('guia.incluirresultados', array($guia->id)) }}">Incluir Resultados</a></li>
                @if ($refazer)
                    <li><a href="#" id="bt_go_n_real">Criar GO com juntas N Realizadas</a></li>
                @endif
                <li><a href="#" id="impriGO">Imprimir GO</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/etiqueta">Etiquetas</a></li>
                <li><a href="/guia/{{$guia->id}}/capa">Capa</a></li>
                <li><a href="/guia/{{$guia->id}}/rdo">RDO</a></li>
                <li class="divider"></li>
                <li><a href="/guia/{{$guia->id}}/criarrelatorio">Criar Relatorio</a></li>
                <li><a href="/guia/{{$guia->id}}/relatorio">Relatorios</a></li>
                <li class="divider"></li>
                <li><a href="/guia/cadastrar">Nova Guia Operacional</a></li>
            </ul>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox-content">


                    <br>
                    <div class="area_impressao" name="area_impressao" id="area_impressao">
    <table style="min-width: 1083px" width="100%" class="header">
        <tr>
            <td width="142"><h1 style="text-align: left">
                <img border="0" src="/img/img_logo_proposta.png" width="135" height="45"></td>
            <td >
                <h1 style="text-align: center">GUIA OPERACIONAL</h1>
            </td>
            <td width="250"  align="right">
                <b>COD GO:</b><font style="font-size: 18px; ">{{ $guia->id }}</font>/{{$guia->unidade}}/{{ $guia->data->format('Y') }}<br>
                <b>DATA: </b>{{ $guia->data->format('d/m/Y') }}
            </td>
            <td width="80" align="center"><div id="qrcode'.$pag_cont.'"></div></td>
        </tr>
    </table>   
    <table style="min-width: 1083px" width="100%" class="header_vi">
        <tr>
            <td width="42%"><b>CLIENTE: </b>{{ $guia->cliente->cnpj }} - {{ $guia->cliente->codigo }} - {{ $guia->cliente->nome }}</td>

            <td><b>LOCAL:</b> {{$guia->local_ensaio}} </td>
            
        </tr>
        <tr>
            <td width="42%"><b>PROJETO:</b> {{isset($guia->projeto) ? $guia->projeto : ''}}</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td width="30%">
                <table border="0" width="420" class="header_vi">
                    <tr>
                        <td ><b></b></td>
                        <td ><b>EQUIPE</b></td>
                        <td width="100"><b>FUNÇÕES</b></td>
                        <td align="center"><b>Tempo Exposição</b></td>
                    </tr>
                        <tr>
                            <td width="5"><b>1</b></td>
                            @if(empty($equipe[0]->op1))
                                <td ></td>
                            @else
                                <td>{{ $funcionarios[$equipe[0]->op1]}}</td>
                            @endif
                            <td width="50">Operador 1</td>
                            <td width="50" align="center">{{$tempo_exposicao}}</td>
                        </tr>
                        <tr>
                            <td width="5"><b>2</b></td>
                            @if(empty($equipe[0]->op2))
                                <td ></td>
                            @else
                                <td>{{ $funcionarios[$equipe[0]->op2]}}</td>
                            @endif
                            <td width="50">Operador 2</td>
                            <td width="50" align="center">{{$tempo_exposicao}}</td>
                        </tr> 
                        <tr>
                            <td width="5"><b>3</b></td>
                            @if(empty($equipe[0]->aux))
                                <td ></td>
                            @else
                                <td>{{ $funcionarios[$equipe[0]->aux]}}</td>
                            @endif
                            <td width="50">Auxiliar</td>
                            @if(empty($equipe[0]->aux))
                                <td ></td>
                            @else
                              <td width="50" align="center">{{$tempo_exposicao}}</td>
                            @endif
                            
                        </tr> 
                        <tr>
                            <td width="5"><b>4</b></td>
                            @if(empty($equipe[0]->ins))
                                <td ></td>
                            @else
                                <td>{{ $funcionarios[$equipe[0]->ins]}}</td>
                            @endif
                            <td width="50">Inspetor</td>
                            <td width="50" align="center"></td>
                        </tr> 
                </table>
            </td>
            <td width="20%">
                <table border="0" width="200" cellspacing="1" cellpadding="0" class="header_vi">
                    <tr>
                        <td width="136"><b>MONITOR AREA: </b></td>
                        <td width="130">{{ isset($monitores[$guia->monitor_id]) ? $monitores[$guia->monitor_id] : ' '}}</td>
                    </tr>
                    <tr>
                        <td width="136"><b>FONTE: </b></td>
                        <td width="130">{{ isset($guia->fonte->numero) ? $guia->fonte->numero : '-'}}</td>
                    </tr>
                    <tr>
                        <td width="136"><b>ISOTOPO:</b></td>
                        <td width="130">{{ isset($guia->fonte->isotopo) ? $guia->fonte->isotopo : '-'}}</td>
                    </tr>
                    <tr>
                        <td width="136"><b>IRRADIADOR:</b></td>
                        <td width="130">{{ isset($irradiadores[$guia->fonte->irradiador]) ? $irradiadores[$guia->fonte->irradiador] : '-' }}</td>
                    </tr>
                    <tr>
                        <td width="136"><b>ATIVIDADE:</b></td>
                        <td width="130">{{ isset($guia->fonte_atividade) ? $guia->fonte_atividade : '-' }}</td>
                    </tr>
                    <tr>
                        <td width="136"><b>FOCO:</b></td>
                        <td width="130">{{ isset($guia->fonte->foco) ? $guia->fonte->foco : '-' }}</td>
                    </tr>
                </table>

            </td>
            <td >
                <table border="0" width="75%" cellpadding="0" class="header_vi" >
                    <tr>
                        <td align="center"><b>ENTRADA</b></td>
                        <td align="center"><b>SAIDA</b></td>
                    </tr>
                    <tr>
                        <td align="center">{{isset($entrada) ? $entrada->format('d/m/Y H:i:s') : ''}}</td>
                        <td align="center">{{isset($saida) ? $saida->format('d/m/Y H:i:s') : ''}}</td>
                    </tr>
                </table>
                <table border="0" width="75%" cellpadding="0" height="71" class="header_vi">
                    <tr>
                        <td colspan="2" align="center"><b>TRANSLADO / MOBILIZACAO</b></td>
                    </tr>
                    <tr>
                        <td align="center"><b>PLACA</b></td>
                        <td align="center">{{$guia->placa}}</td>
                    </tr>
                    <tr>
                        <td align="center"><b>KM INICIAL<b></td>
                        <td align="center">{{$guia->km_inicial}}</td>
                    </tr>
                    <tr>
                        <td align="center"><b>KM FINAL<b></td>
                        <td align="center">{{$guia->km_final}}</td>
                    </tr>
                    <tr>
                        <td align="center"><b>KM PERCORRIDO<b></td>
                        <td align="center">{{$guia->km_final - $guia->km_inicial}}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <table class="zebra3"  style="min-width: 1083px" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th colspan="20" height="15">PROGRAMAÇÕES</th>
            </tr>
            <tr>
                <th height="28">ITEM</th>
                <th width="110" height="28">IDENTIFICAÇÃO</th>
                <th height="28">N° JUNTA</th>
                <th height="28">Ø</th>
                <th height="28">ESPE.</th>
                <th height="28">NORMA</th>
                <th height="28">PROCEDIM.</th>
                <th height="28">TÉCNICA</th>
                <th height="28">IQI</th>
                <th height="28">TEMPO<BR>EXPOSIÇÃO</th>
                <th height="28">DFF</th>
                <th height="28">QTD FILMES</th>
                <th height="28">FILMES BATIDOS</th>
                <th height="28">CLASSE FILME</th>
                <th height="28">DIMENSÃO FILME</th>
                <th height="28">OBS.</th>
                <th height="28">EXECUTADO</th>
                <th height="28">QTD APROVADO</th>
                <th height="28">QTD REPROVADO</th>
                <th height="28">DESCONT.</th>
            </tr>
        </thead>
        <tbody>

            <?php
                $linhas = 12;
                $quantjuntas = count($programacoes);
                $pagina= intval(ceil($quantjuntas/($linhas-1)));
                $contlinhas=1;
                $numeropagina = 1;                
            ?>

            @foreach ($programacoes as $programacao)
                <?php $descs = array(); ?>
                @foreach ($descontinuidadePro as $element)
                    @if($element->item == $programacao->item)
                        <?php $descs[] =  $element->descontinuidade;
                         $descs = array_unique($descs);?>
                    @endif
                @endforeach
                <?php $stringdesc = implode(' - ',$descs) ?>

                <?php $arraynorma = array(); ?>
                <?php $normas = explode('+',$programacao->norma); ?>

                @foreach ($normas as $element)
                    @if (isset($nomenormas[$element]))
                        <?php $arraynorma[] = $nomenormas[$element]; ?>
                    @else
                        <?php $arraynorma[] = $element; ?>
                    @endif
                @endforeach

                <?php $stringnorma = implode(' + ', $arraynorma); ?>
                @if($contlinhas < $linhas)
                    <tr>                    
                        <td style="text-align: center" height="25">{{$programacao->item}}</td>
                        <td style="font-size: 8px">{{$programacao->identificacao}}</td>
                        <td>{{$programacao->junta}}</td>
                        <td>  &nbsp;{{$programacao->diametro}}&nbsp;  </td>
                        <td>{{$programacao->espessura}}</td>
                        <td>{{$stringnorma}}</td>
                        <td>{{$programacao->procedimento}}</td>
                        <td>{{$programacao->tecnica}}</td>
                        <td>{{$programacao->tipo_iqi . ' (' . $programacao->fio_iqi . ')'}}</td>
                        <td>{{ ($programacao->tempo_exposicao == '00:00:00') ? ' ' : $programacao->tempo_exposicao }}</td>
                        <td> &nbsp;{{$programacao->dff}}&nbsp;</td>
                        <td>{{$programacao->quantidade_filmes}}</td>
                        <td>{{ ($programacao->filmes_batidos == 0) ? ' ' : $programacao->filmes_batidos }}</td>
                        <td>{{$programacao->filme_classe}}</td>
                        <td>{{$programacao->filme_dimensao}}</td>
                        <td>{{$programacao->observacao}}</td>
                        <td>{{$programacao->executado}}</td>
                        <td>{{$programacao->quantidade_aprovado}}</td>
                        <td>{{$programacao->quantidade_reprovado}}</td>
                        <td>{{$stringdesc}}</td>
                    </tr>
                @elseif($contlinhas >= $linhas)
                    <?php $contlinhas=1;?>
                        </tbody>
                    </table>
                    <br>
                    <div style="float:right; width: 160px; height: 20px; z-index: 1" id="contpag"> Página {{ $numeropagina }} de {{ $pagina }}</div>
                    <br>
                    <br>
                    <br>

                    @if($pagina>1)
                        <?php $numeropagina++; ?> 
                    @endif
                    <div class="page-break"></div>
                    <br>
                    <br>
                    <table class="zebra3"  style="min-width: 1083px" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th colspan="20" height="15">PROGRAMAÇÕES</th>
                            </tr>
                            <tr>
                                <th height="28">ITEM</th>
                                <th width="110" height="28">IDENTIFICAÇÃO</th>
                                <th height="28">N° JUNTA</th>
                                <th height="28">Ø</th>
                                <th height="28">ESPE.</th>
                                <th height="28">NORMA</th>
                                <th height="28">PROCEDIM.</th>
                                <th height="28">TÉCNICA</th>
                                <th height="28">IQI</th>
                                <th height="28">TEMPO<BR>EXPOSIÇÃO</th>
                                <th height="28">DFF</th>
                                <th height="28">QTD FILMES</th>
                                <th height="28">FILMES BATIDOS</th>
                                <th height="28">CLASSE FILME</th>
                                <th height="28">DIMENSÃO FILME</th>
                                <th height="28">OBS.</th>
                                <th height="28">EXECUTADO</th>
                                <th height="28">QTD APROVADO</th>
                                <th height="28">QTD REPROVADO</th>
                                <th height="28">DESCONT.</th>
                            </tr>
                        </thead>
                        <tbody>

                    
                     <tr>                    
                        <td style="text-align: center" height="25">{{$programacao->item}}</td>
                        <td style="font-size: 8px">{{$programacao->identificacao}}</td>
                        <td>{{$programacao->junta}}</td>
                        <td>  &nbsp;{{$programacao->diametro}}&nbsp;  </td>
                        <td>{{$programacao->espessura}}</td>
                        <td>{{$stringnorma}}</td>
                        <td>{{$programacao->procedimento}}</td>
                        <td>{{$programacao->tecnica}}</td>
                        <td>{{$programacao->tipo_iqi . ' (' . $programacao->fio_iqi . ')'}}</td>
                        <td>{{ ($programacao->tempo_exposicao == '00:00:00') ? ' ' : $programacao->tempo_exposicao }}</td>
                        <td> &nbsp;{{$programacao->dff}}&nbsp;</td>
                        <td>{{$programacao->quantidade_filmes}}</td>
                        <td>{{ ($programacao->filmes_batidos == 0) ? ' ' : $programacao->filmes_batidos }}</td>
                        <td>{{$programacao->filme_classe}}</td>
                        <td>{{$programacao->filme_dimensao}}</td>
                        <td>{{$programacao->observacao}}</td>
                        <td>{{$programacao->executado}}</td>
                        <td>{{$programacao->quantidade_aprovado}}</td>
                        <td>{{$programacao->quantidade_reprovado}}</td>
                        <td>{{$stringdesc}}</td>
                    </tr>
                @endif
                <?php $contlinhas++; ?>
            @endforeach
        </table>
        <br>
        <table border="0" style="min-width: 1083px" width="100%" class="legenda_relatorio">
            <tr>
                <td width="50%">
                    <table style="border-style: solid; border-width: 1px;"  class="legenda_relatorio">
                        <tr>
                            <td colspan="{{count($descontinuidades)}}"><p align="center"><b>Descontinuidades</b></td>
                        </tr>
                        <tr height="30" style="border-style: solid; border-width: 1px;">
                           @foreach ($descontinuidades as $descontinuidade)
                                <td style="border-style: solid; border-width: 1px;" align="center"> {{$descontinuidade['legenda']}}: {{$descontinuidade['descricao']}} </td>
                            @endforeach
                        </tr>
                    </table>
                </td>
                <td>&nbsp;</td>
                <td  width="50%">
                    <table style="border-style: solid; border-width: 1px;" class="legenda_relatorio">
                        <tr valign="middle">
                            <td colspan="{{count($filmes)}}" ><p align="center"><b>Dimensão Filme</b></td>
                        </tr>
                        <tr height="30">
                            @foreach ($filmes as $filme)
                                <td style="border-style: solid; border-width: 1px;" align="center">&nbsp;{{$filme[0]['legenda']}} : {{$filme[0]['descricao']}} </td>
                            @endforeach
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
        <table border="0"  style="min-width: 1083px" width="100%" height="100" style="border-top: 1px solid black; border-bottom: 1px solid black;">
            <tr>
                <td rowspan="2" colspan="6" height="42" valign="top" style="border-bottom: 1px solid black; ">
                    <b>OCORRÊNCIAS:</b> {{$guia->ocorrencias}}
                </td>
            </tr>
            <tr>
                <td colspan="2" height="20" valign="top" style="border-bottom: 1px solid black; "></td>
            </tr>
            <tr>
                <td valign="top"><b>ASSINATURA NIVEL 1</b></td>
                <td valign="top"><b>ASSINATURA NIVEL 2</b></td>
            </tr>
        </table>
    </tbody>
</table>
<div style="float:right; width: 160px; height: 20px; z-index: 1" id="contpag">Página {{ $numeropagina }} de {{ $pagina }}</div>
</div>




                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop