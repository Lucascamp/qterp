<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <!-- <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form> -->
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        Modulos
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        @foreach ($modulos as $element)
                            @if($element['status']==1)
                                <li><a href="{{$element['rota']}}">{{$element['descricao']}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                <!-- <li>
                <span class="m-r-sm text-muted welcome-message">Welcome to INSPINIA+ Admin Theme.</span>
                </li> -->
                <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope"></i>  <span class="label label-warning">2</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                <li>
                <div class="dropdown-messages-box">
                <a href="profile.html" class="pull-left">
                {{-- <img alt="image" class="img-circle" src="img/a7.jpg"> --}}
                </a>
                <div class="media-body">
                <small class="pull-right">24/02/2015</small>
                <strong>Mike Loreipsum</strong><br> Assunto da Mensagem 
                <small class="text-muted">3 dias atras as 7:58 pm - 24.02.2015</small>
                </div>
                </div>
                </li>
                <li>
                <div class="dropdown-messages-box">
                <a href="profile.html" class="pull-left">
                {{-- <img alt="image" class="img-circle" src="img/a7.jpg"> --}}
                </a>
                <div class="media-body">
                <small class="pull-right">24/02/2015</small>
                <strong>Mike Loreipsum</strong> <br>Assunto da Mensagem 
                <small class="text-muted">3 dias atras as 7:58 pm - 24.02.2015</small>
                </div>
                </div>
                </li>
                <li class="divider"></li>
                <li>
                <div class="text-center link-block">
                <a href="#">
                <i class="fa fa-envelope"></i> <strong>Ver todas as mensagens</strong>
                </a>
                </div>
                </li>
                </ul>
                </li>
                <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-bell"></i>  <span class="label label-danger">1</span>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                <li>
                <a href="#">
                <div>
                <i class="fa fa-envelope fa-fw"></i> Você tem 3 notificações
                <span class="pull-right text-muted small">4 horas atras</span>
                </div>
                </a>
                </li>
                <li class="divider"></li>
                <li>
                <div class="text-center link-block">
                <a href="#">
                <strong>Ver todas as notificações</strong>
                <i class="fa fa-angle-right"></i>
                </a>
                </div>
                </li>
                </ul>
                </li>

               
            <li>
                <a href="/logout"><i class="fa fa-sign-out"></i>Log out</a>
            </li>
        </ul>
    </nav>
</div>