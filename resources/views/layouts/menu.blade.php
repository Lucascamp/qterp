<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">Usuario</strong>
                            </span> 
                            <!-- <span class="text-muted text-xs block"><b class="caret"></b></span>  -->
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="/adm/trocarsenha">Trocar Senha</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    QT-ERP
                </div>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Modulos</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="index.html">Ponto</a></li>
                    <li><a href="index.html">Ensaio</a></li>
                    <li><a href="index.html">Radio Proteção</a></li>
                    <li><a href="index.html">Equipamentos</a></li>
                </ul>
            </li>

        </ul>
    </div>
</nav>