<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="minimalize-styl-2 btn btn-primary btn btn-primary " title="Pagina Inicial" href="/"><i class="fa fa-home"></i></a>
            </div>
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " title="Minimizar Menu" href="#"><i class="fa fa-bars"></i> </a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        Modulos
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        @foreach ($modulos as $element)
                            @if($element['status']==1)
                                <li><a href="{{$element['rota']}}">{{$element['descricao']}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="/logout"><i class="fa fa-sign-out"></i>Log out</a>
                </li>
            </ul>       
        </nav>
    </div>
