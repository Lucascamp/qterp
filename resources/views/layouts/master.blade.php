
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>QT-ERP</title>
    <link rel="icon" href="/img/qualitec_09032015_1.ico" />

    <link href="/css/bootstrap.css" rel="stylesheet">
    {{-- <link href="/css/font.css" rel="stylesheet"> --}}
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    {{-- <link href="/css/dataTables.responsive.css" rel="stylesheet">
    <link href="/css/dataTables.tableTools.min.css" rel="stylesheet"> --}}

    <link href="/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/css/dataTables.responsive.css" rel="stylesheet">
    <link href="/css/dataTables.tableTools.min.css" rel="stylesheet">

    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/toastr.min.css" rel="stylesheet">
    <link href="/css/proposta.css" rel="stylesheet">
    <link href="/css/usuario.css" rel="stylesheet">
    <link href="/css/icones.css" rel="stylesheet">
    <link href="/css/impressao_retrato.css" rel="stylesheet">
    <link href="/css/impressao_capa.css" rel="stylesheet">
    <link href="/css/font1.css" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="/handsontable/dist/handsontable.full.css">
    {!! HTML::style('css/chosen.min.css') !!}
    {!! HTML::style('css/main.css') !!}

    <!-- <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet"> -->

</head>
<body class="pace-done">
	<div class="pace  pace-inactive">
		<div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
	  		<div class="pace-progress-inner"></div>
		</div>
		<div class="pace-activity"></div>
	</div>
	<div id="wrapper">
		@yield('menu')
		@include('layouts.header')
		@yield('main')
		@include('layouts.footer')
	</div>

</body>

    <script src="/js/jquery2.1.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/metismenu.js"></script>
    <script src="/js/slimscroll.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/js/inspinia.js"></script>
    <script src="/js/pace.js"></script>

    <!-- iCheck -->
    <script src="/js/icheck.js"></script>
    
    <script src="/js/jquery.maskedinput.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/js/jeditables.js"></script>
    <script src="/js/dataTables.bootstrap.js"></script>
    <script src="/js/dataTables.responsive.js"></script>
    <script src="/js/dataTables.tableTools.min.js"></script>
    
    <script src="/js/metismenu.js"></script>
    <script src="/js/slimscroll.js"></script>
    <script src="/js/jquery.flot.js"></script>
    <script src="/js/jquery.flot.tooltip.min.js"></script>
    <script src="/js/jquery.flot.spline.js"></script>
    <script src="/js/Chart.min.js"></script>
    <script src="/js/toastr.min.js"></script>
    <script src="/js/jquery.PrintArea.js"></script>
    
    <script src="/js/proposta.js"></script>
    <script src="/js/usuario.js"></script>

    <script src="/js/main.js"></script>
    <script src="/js/admin/modulos.js"></script>
    <script src="/js/admin/ensaio.js"></script>
    
    <script src="/handsontable/dist/handsontable.full.js"></script>
    
    {!! HTML::script('js/chosen.jquery.js') !!}
    {!! HTML::script('js/bootstrap-dialog.js') !!}
    {!! HTML::script('js/ensaio/guia.js') !!}
    {!! HTML::script('js/ensaio/boletim.js') !!}
    {!! HTML::script('js/patrimonio/equipamentos.js') !!}
    {!! HTML::script('js/patrimonio/calibracao.js') !!}
    {!! HTML::script('js/patrimonio/almoxarifado.js') !!}
    {!! HTML::script('js/patrimonio/frota.js') !!}
    {!! HTML::script('js/radioprotecao/fonte.js') !!}
    {!! HTML::script('js/jquery.blockUI.min.js') !!}
    {!! HTML::script('js/fontesporfrente.js') !!}
    

</html>