@extends('layouts.master')

@section('menu')

@include('ensaio.menuensaio')

@stop

@section('main')



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Sincronização</h2>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            <a href="\" class="btn btn-primary">Voltar</span></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <div class="ibox-content">
                    <div class="alert alert-danger">
                        <center>{{$result}}</center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop