@extends('layouts.master')

@section('menu')
@include('patrimonio.menu')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Radioproteção</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">Radioproteção</h3>
                <div class="error-desc">
                    <a href="/fonte/folha" class="btn btn-primary m-t">Fontes</a>
                    <a href="/fontesporfrente" class="btn btn-primary m-t">Relatório Fontes por Frente de trabalho</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
