@extends('layouts.master')

@section('menu')

@include('radioprotecao.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Fontes</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/radioprotecao">Radioproteção</a>
            </li>
            <li class="active">
                <strong>Fontes por Frente de Trabalho</strong>
            </li>
        </ol>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}"/>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
    
    
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="h4">Gerar Relatórios de Fontes por Frente de Trabalho</span>
            </div>    
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <label class="control-label">Data Inicial</label>
                                <div class=""><input type="date" class="form-control" id="data_ini"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Data Final</label>
                                <div class=""><input type="date" class="form-control" id="data_fim"></div>
                            </div>
                            
                            <a class="btn btn-success pull-right" id="bt_carregar">Carregar</a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                    <table class="table table-bordered tabela" id="table_gerar_relatorio">
                        <thead>
                            <th>Empresa</th>
                            <th>Endereço</th>
                            <th>Cidade</th>
                            <th>UF</th>
                            <th>Resp. Empresa</th>
                            <th>Resp. Prestadora</th>
                            <th>Irradiador</th>
                            <th>Nº Detector</th>
                            <th>Inicío</th>
                            <th>Término</th>
                            <th>Frente F/M</th>
                            <th>Nº Fonte</th>
                            <th>Período D/N</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                    <a href="#" class="btn btn-success pull-right">Salvar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop