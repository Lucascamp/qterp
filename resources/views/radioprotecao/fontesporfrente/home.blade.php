@extends('layouts.master')

@section('menu')

@include('radioprotecao.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Fontes</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/radioprotecao">Radioproteção</a>
            </li>
            <li class="active">
                <strong>Fontes por Frente de Trabalho</strong>
            </li>
        </ol>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">
<div class="wrapper wrapper-content">
    
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="h4">Relatórios Fontes por Frente de Trabalho</span>
                <div class="pull-right">
                    <a href="/fontesporfrente/gerar" class="btn-sm btn-success no-margins "><i class="fa fa-plus"></i> Novo</a>
                </div>
            </div>
        
            @if ($errors->any())
            <div class="alert-group">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </div>
            </div>
            @endif
            
    
            <div class="ibox-content">
                <table class="table table-bordered" id="table_rel_fpft">
                    <thead>
                        <th>Mês de referência</th>
                        <th>Período</th>
                        <th>Data da Criação</th>
                        <th>Ações</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop