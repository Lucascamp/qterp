@extends('layouts.master')

@section('menu')

@include('radioprotecao.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Fontes</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/radioprotecao">Radioproteção</a>
            </li>
            <li class="active">
                <strong>Fontes</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="/fonte/cadastrar" class="btn btn-primary">Cadastrar Fonte</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>Fontes em operação</h5>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">

    <table class="table tabela">
        {!! Form::open(array('route' => 'fonte.filtro', 'method' => 'get')) !!}
        <tr>
            <td>
                {!! Form::select('filter_isotopo', $filter_isotopo, $isotopo, array('id'=>'isotopo', 'class'=>'chosen-select')) !!}
            </td>

            <td>
                {!! Form::select('filter_modelo', $filter_modelo , $modelo, array('id'=>'modelo', 'class'=>'chosen-select')) !!}
            </td>
            <td>
                {!! Form::select('filter_unidade', $filter_unidades , $unidade, array('id'=>'unidade', 'class'=>'chosen-select')) !!}
            </td>
            <td>
                <button type="submit" data-tooltip="tooltip" class="btn btn-primary fa fa-filter" style="width: 41px; height:34px;" title="Filtrar tabela"></button>

                <button type="button" data-tooltip="tooltip" class="btn btn-info fa fa-eraser" style="width: 41px; height:34px;" id="limparfontes" title="Limpar"></button>

                {!! Form::close() !!}
            </td>
        </tr>
    </table>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

       
        <div class="pull-right">
            <table border='0' width="100%">
                <tr>
                    <td>Legenda:&nbsp;&nbsp;</td>
                    <td style="background-color:#9DDEFF;" align='center'><font size='2'>&nbsp;Abaixo de 20 Ci&nbsp;</font></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="background-color:#FFFF99;" align='center'><font size='2'>&nbsp;Abaixo de 10 Ci&nbsp;</font></td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <br>
        
        <table class="table table-bordered tabela" id="tableindex">
            <thead>
                <tr>
                    <th>Número da Fonte</th>
                    <th>Irradiador</th>
                    <th>Atividade inicial</th>
                    <th>Atividade Atual</th>
                    <th>Limite de uso</th>
                    <th>Limite de troca</th>
                    <th>Pasta</th>
                    <th>Modelo</th>
                    <th>Cabo</th>
                    <th>Gabarito de teste</th>
                    <th>Foco</th>
                    <th>Unidade</th>
                    <th>Data movimentação</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
             @if (isset($fontes))
             @foreach ($fontes as $fonte)
             <tr>
                <td>{{ $fonte->numero }}</td>
                <td>{{ isset($irradiadores[$fonte->irradiador]) ? $irradiadores[$fonte->irradiador] : '-' }}</td>
                
                @if($fonte->data_inicial != null)
                    <td>{{ $fonte->atividade_inicial }} Ci em {{ $fonte->data_inicial->format('d/m/Y') }}</td>

                    <td <?php 
                        
                        if ((intval($cargasAtuais[$fonte->id]) > 10) and (intval($cargasAtuais[$fonte->id]) < 20))
                        {?>
                        style="background-color:#9DDEFF"
                        <?php 
                        }
                        elseif(intval($cargasAtuais[$fonte->id]) < 10) {?>
                            style="background-color:#FFFF99"
                        <?php } ?>    
                            
                        >{{ $cargasAtuais[$fonte->id] }} Ci </td>
                     
                @elseif($fonte->atividade_inicial != 0)
                    <td> {{ $fonte->atividade_inicial }} </td>
                    <td> - </td>
                @else   
                    <td> - </td>
                    <td> - </td>
                @endif

                @if($fonte->data_limite_uso == null)
                    <td> - </td>
                @else
                    <td>{{{ $fonte->data_limite_uso->format('d/m/Y') }}}</td>
                @endif

                @if($fonte->data_limite_troca==null)
                    <td> - </td>
                @else
                    <td>{{{ $fonte->data_limite_troca->format('d/m/Y') }}}</td>
                @endif

                <td>{{ $fonte->pasta }}</td>
                <td>{{ $fonte->modelo }}</td>
                <td>{{ $fonte->cabo }}</td>
                <td>{{ $fonte->gabarito }}</td>
                <td>{{ $fonte->foco }}</td>
                
                @if(isset($local_irradiador[$fonte->irradiador]))
                    @if (isset($status_irradiador[$fonte->irradiador]) && $status_irradiador[$fonte->irradiador] == 0)
                        <td>{{isset($unid[$local_irradiador[$fonte->irradiador]]) ? $unid[$local_irradiador[$fonte->irradiador]] : '-'}}</td>
                    @elseif(isset($status_irradiador[$fonte->irradiador]) && $status_irradiador[$fonte->irradiador] == 1)
                        <td>Fonte em transito para a unidade {{$unid[$movimentacao[$fonte->irradiador]->unidade_destino]}}</td>
                    @endif
                @else
                    <td>-</td>
                @endif

                @if(isset($movimentacao[$fonte->irradiador]))
                    <td>{{$movimentacao[$fonte->irradiador]->data_movimentacao}}</td>
                @else
                    <td>-</td>
                @endif
                
               
                <td>{{ $fonte->status }}</td>

                <td width = 220px>
                    <button type="button" data-target="#modalHistorico" data-toggle="modal" data-tooltip="tooltip" class="btn btn-primary fa fa-exchange historicoFonteBtn" style="width: 41px; height:34px;" title="Exibir Histórico"
                        data_value="{{ $fonte->id }}"></button>
                
                    <button type="button" data-target="#modalCalculo" data-toggle="modal" data-tooltip="tooltip" class="btn btn-default fa fa-calculator calculoFonteBtn" style="width: 41px; height:34px;" title="Cálculo de atividade"
                        data_value="{{ $fonte->id }}"></button>
                
                    <a href="{{ route('fonte.editar', array($fonte->id)) }}" data-toggle="tooltip" title="Editar" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-info dim"><i class="fa fa-pencil"></i></a>
                
                    @if (isset($status_irradiador[$fonte->irradiador]) && $status_irradiador[$fonte->irradiador] == 0)
                       <button type="button" data-target="#modalLocalizacao" data-toggle="modal" data-tooltip="tooltip" class="btn btn-warning fa fa-automobile localizacaoBtn" id="fonte_id{{$fonte->id}}" style="width: 41px; height:34px;" title="Alterar Localização" data_value="{{ $fonte->irradiador }}"></button>
                    @elseif(isset($status_irradiador[$fonte->irradiador]) && $status_irradiador[$fonte->irradiador] == 1)
                       <button data-tooltip="tooltip" class="btn btn-success fa fa-check receberBtn" style="width: 41px; height:34px;" title="Receber" id="btrec{{$fonte->irradiador}}"></button></td>
                    @endif
                    
                </td>
            </tr>
            @endforeach
            @else
                <tr> 
                    <td colspan="20" class="center">
                        Nenhuma fonte encontrada.
                    </td>
                </tr>
                @endif    
            </tbody> 
        </table>
    </div>
</div>
</div>

@include('radioprotecao.fonte.partials._modal_historico')

@include('radioprotecao.fonte.partials._modal_calculo')

@include('radioprotecao.fonte.partials._modal_excluir')

@include('radioprotecao.fonte.partials._modal_localizacao')

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop