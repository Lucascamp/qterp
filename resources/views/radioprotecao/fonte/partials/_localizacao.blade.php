<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Localização</td>
    </tr>
    <tr>
        <td class="tblabel">Unidade(*)</td>
        <td>
        {!! Form::select('unidade', $unidades, isset($localizacao) ? $localizacao->unidade : null ,array( 'class' => 'form-control')) !!}
        </td>

        <td class="tblabel">Data movimentação(*)</td>
        <td>
            <input type="date" name="data" class="form-control" value="{{ isset($localizacao) ? $localizacao->data->format('Y-m-d') : null }}">
        </td>
    </tr>
    <tr>

        <td class="tblabel">Observações</td>
        <td  colspan="3">
            {!! Form::text('observacoes', isset($localizacao->observacoes) ? $localizacao->observacoes : null, array('class'=>'form-control', 'style'=>'width:100%')) !!}
        </td>
    </tr>
</table>