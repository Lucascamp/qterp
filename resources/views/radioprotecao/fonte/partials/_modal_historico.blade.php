<div class="modal fade" id="modalHistorico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Histórico da fonte</font></h4>
    </div>
    <div class="modal-body" id="bodym">
    <div style="width: 100%; height: 65%; overflow-y: scroll;">
        <table class="table table-bordered tabela2">
            <thead> 
                 <tr>
                    <th>Número da fonte</th>
                    <th>Irradiador</th>
                </tr>
            </thead>    
            <tbody>   
                    <td><span id='numero_fonte'></span></td>
                    <td><span id='irradiador_fonte'></span></td>
            </tbody>
        </table> 
        <table class="table table-bordered tabela2">
            <thead> 
             <tr>
                <th>Data Movimentação</th>
                <th>Unidade</th>
                <th>Observações</th>
            </tr>
            </thead>    
            <tbody id="tablehistorico">   
                <td colspan="7"><font size="3"><center>Carregando...</center></font></td>
            </tbody>
        </table>    
        </div>
    </div>
    <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </div>
</div>
</div>
</div>