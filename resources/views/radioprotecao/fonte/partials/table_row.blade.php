<tr class="{{ $class }}">
	<td>{{{ $fonte->numero ?: '-' }}}</td>
	<td>{{{ isset($irradiadores[$fonte->irradiador]) ? $irradiadores[$fonte->irradiador] : '-' }}}</td>
	
	@if($class == 'iridio' && isset($cargasAtuais[$fonte->id]))
		<td <?php 
                if ((intval($cargasAtuais[$fonte->id]) > 9) and (intval($cargasAtuais[$fonte->id]) < 20))
                    {?>
                    	class="abaixo10";
                        <?php 
                        }
                elseif(intval($cargasAtuais[$fonte->id]) < 10) {?>
                        class="abaixo20";
                        <?php } ?>    
                 >{{ $cargasAtuais[$fonte->id] }}
        </td>

        <td <?php 
                if ((intval($cargasAtuais30dias[$fonte->id]) > 9) and (intval($cargasAtuais30dias[$fonte->id]) < 20))
                    {?>
                    	class="abaixo10";
                        <?php 
                        }
                elseif(intval($cargasAtuais30dias[$fonte->id]) < 10) {?>
                        class="abaixo20";
                        <?php } ?>    
                 >{{ $cargasAtuais30dias[$fonte->id] }}
        </td>

	@elseif($class == 'raio-x')
		<td>{{{ $fonte->atividade_inicial }}}</td>
		<td>{{{ $fonte->atividade_inicial }}}</td>	

	@elseif($class == 'cobalto')
		<td>{{{ $fonte->atividade_inicial }}}</td>
		<td>{{{ $fonte->atividade_inicial }}}</td>		
	
	@else	
		@if(isset($cargasAtuais[$fonte->id]))
			<td>{{{ $cargasAtuais[$fonte->id] }}}</td>
			<td>{{{ $cargasAtuais30dias[$fonte->id] }}} </td>
		@else
			<td> - </td>
			<td> - </td>
		@endif	
	@endif
	
	@if(isset($cargasAtuais[$fonte->id]))
		<td>{{{ number_format(($cargasAtuais[$fonte->id] * 0.037), 2) }}}</td>
	@else
		<td> - </td>
	@endif	

	<td>{{{ $fonte->pasta ?: '-' }}}</td>
	<td>{{{ $fonte->isotopo ?: '-' }}}</td>
	<td>{{{ $fonte->modelo ?: '-' }}}</td>
	<td>{{{ $fonte->cabo ?: '-' }}}</td>
	<td>{{{ $fonte->gabarito ?: '-' }}}</td>
	<td>{{{ $fonte->foco ?: '-' }}}</td>
	@if(isset($local_irradiador[$fonte->id]))
		<td>{{ $unidade_fonte[$local_irradiador[$fonte->id]] }}</td>
	@else
		<td>-</td>
	@endif
	<td>{{{ isset($localizacao[$fonte->id]->observacoes) ? $localizacao[$fonte->id]->observacoes : '-' }}}</font></td>
</tr>