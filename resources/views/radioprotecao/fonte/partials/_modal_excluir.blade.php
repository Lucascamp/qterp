<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sl">
    <div class="modal-content">
      <div class="modal-header" style="background: #ed5565">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Excluir Fonte</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        {!! Form::open(array('route' => 'fonte.destroy', 'class'=>'form-inline')) !!}
        {!! Form::token() !!}
        {!! Form::hidden('fonte_id', '', array('id' => 'fonte_id_excluir')); !!}
        <p>Tem certeza que deseja excluir a fonte com irradiador <span id='fontedescricao'></span> ?</p>
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {!! Form::submit('Excluir', array('class' => 'btn btn-danger')) !!}

        {!! Form::close() !!}
    </div>
</div>
</div>
</div>