<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Dados da fonte</td>
    </tr>
    <tr>
        <td class="tblabel">Número(*)</td>
        <td>
        {!! Form::text('numero', isset($fonte) ? $fonte->numero : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Irradiador(*)</td>
        <td>
            @if (isset($fonte))
                {!! Form::select('irradiador', $irradiadores, $fonte->irradiador, array('class' => 'chosen-select','disabled')) !!}
            @else
                {!! Form::select('irradiador', $irradiadores, null, array('class' => 'chosen-select')) !!}
            @endif
        </td>

        <td class="tblabel">Cabo(*)</td>
        <td>
        {!! Form::text('cabo', isset($fonte) ? $fonte->cabo : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Foco(*)</td>
        <td>
        {!! Form::text('foco', isset($fonte) ? $fonte->foco : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Modelo(*)</td>
        <td>
        {!! Form::select('modelo', array(
            '' => 'Selecione',
            'Gamammat SE' => 'Gamammat SE',
            'Gamammat CS' => 'Gamammat CS',
            'Gamammat M4-SE' => 'Gamammat M4-SE',
            'Gamammat M10-IR' => 'Gamammat M10-IR',
            'Gamammat TSI 3/1' => 'Gamammat TSI 3/1',
            'Gamammat TSI 5/1' => 'Gamammat TSI 5/1',
            'Sentinel 880 Delta' => 'Sentinel 880 Delta',
            'Sentinel 880 Sigma' => 'Sentinel 880 Sigma',
            'Unitron 110AB' => 'Unitron 110AB',
            'Raio X - GE' => 'Raio X - GE',
            'Raio X - Super Lilliput' => 'Raio X - Super Lilliput'), isset($fonte) ? $fonte->modelo : null, array('class' => 'chosen-select')) !!}
        </td>

        <td class="tblabel">Gabarito de teste(*)</td>
        <td>
        {!! Form::text('gabarito', isset($fonte) ? $fonte->gabarito : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Isótopo Radioativo(*)</td>
        <td>
             {!! Form::select('isotopo', array('' => 'Selecione',
                                                'Césio - 137' => 'Césio - 137',
                                                'Cobalto - 60' => 'Cobalto - 60',
                                                'Selênio - 75' => 'Selênio - 75',
                                                'Iridio - 192' => 'Iridio - 192',
                                                'Raio - x' => 'Raio - x'), isset($fonte) ? $fonte->isotopo : null, array('class' => 'chosen-select')) !!}
        </td>

        <td class="tblabel">Atividade inicial em Ci(*)</td>
        <td>
            {!! Form::text('atividade_inicial', isset($fonte) ? $fonte->atividade_inicial : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Data Inicial(*)</td>
        <td>
        <input type="date" name="data_inicial" class="form-control" id="data" value="{{ isset($fonte->data_inicial) ? $fonte->data_inicial->format('Y-m-d') : null }}">
        </td>

        <td class="tblabel">Meia vida(*)</td>
        <td>
        {!! Form::text('meia_vida', isset($fonte) ? $fonte->meia_vida : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Limite de uso(*)</td>
        <td>
        {!! Form::text('limite_uso', isset($fonte) ? $fonte->limite_uso : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Limite de troca(*)</td>
        <td>
        {!! Form::text('limite_troca', isset($fonte) ? $fonte->limite_troca : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Relatório vistoria(*)</td>
        <td>
        {!! Form::text('cod_vistoria', isset($fonte) ? $fonte->cod_vistoria : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Data Vistoria(*)</td>
        <td>
        <input type="date" name="data_vistoria" class="form-control" id="data" value="{{ isset($fonte->data_vistoria) ? $fonte->data_vistoria->format('Y-m-d') : null }}">
        </td>

        <td class="tblabel">Pasta(*)</td>
        <td>
        {!! Form::text('pasta', isset($fonte) ? $fonte->pasta : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Status(*)</td>
        <td>
            {!! Form::select('status', array(
                '' => 'Selecione',
                'ATIVO' => 'ATIVO',
                'DESATIVADO' => 'DESATIVADO',
                'SUSPENSO' => 'SUSPENSO'), isset($fonte) ? $fonte->status : null, array('class' => 'chosen-select')) 
                !!}
        </td>
    </tr>

</table>
