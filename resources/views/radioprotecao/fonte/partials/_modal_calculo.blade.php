<div class="modal fade" id="modalCalculo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Calculo de atividade por data</font></h4>
    </div>
    <div class="modal-body" id="bodym">
    <div style="width: 100%; height: 65%; overflow-y: scroll;">
        <table class="table table-bordered tabela2">
            <thead> 
                 <tr>
                    <th>Número da fonte</th>
                    <th>Irradiador</th>
                    <th>Atividade Inicial</th>
                    <th>Data Inicial</th>
                    <th>Meia Vida</th>
                </tr>
            </thead>    
            <tbody>   
                    <td><span id='numero_calculo'></span></td>
                    <td><span id='irradiador_calculo'></span></td>
                    <td><span id='atividade_calculo'></span></td>
                    <td><span id='data_calculo'></span></td>
                    <td><span id='meiavida_calculo'></span></td>
            </tbody>
        </table> 

        {!! Form::hidden('calc', '', array('id' => 'calc')); !!}
        
        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Data para calculo(*)</td>
                <td>
                    <input type="date" name="data" id='data' class="form-control">
                </td>
                <td width="50%"><font size='2'>Atividade da fonte:&nbsp;&nbsp;<span id='atividade'></span></font></td>
            </tr>
        </table>

        <button type="button" class="btn btn-primary" id='calculoAtividadeBtn'>Calcular</button>

        </div>
    </div>
    <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </div>
</div>
</div>
</div>