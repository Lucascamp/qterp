<div class="modal fade" id="modalLocalizacaFonte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Movimentação de fonte</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        {!! Form::open(array('route' => 'fonte.movimentar', 'class'=>'form-inline')) !!}
        {!! Form::hidden('created_by', '1') !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        {!! Form::hidden('fonte_id', '', array('id' => 'bunda')); !!}
        
        <table class="table table-bordered tabela">
            <tr>
                <td class="tblabel">Unidade(*)</td>
                <td>
                    {!! Form::select('unidade', $unidades, null,array( 'class' => 'form-control')) !!}
                </td>
                <td class="tblabel">Data movimentação(*)</td>
                <td>
                    <input type="date" name="data" class="form-control">
                </td>
            </tr>
            <tr>
                <td class="tblabel">Observações</td>
                <td  colspan="3">
                    {!! Form::text('observacoes', null, array('class'=>'form-control', 'style'=>'width:100%')) !!}
                </td>
            </tr>
        </table>

    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}

        {!! Form::close() !!}
    </div>
</div>
</div>
</div>