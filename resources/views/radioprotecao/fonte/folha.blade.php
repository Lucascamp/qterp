@extends('layouts.master')

@section('menu')

@include('radioprotecao.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Fontes</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/radioprotecao">Radioproteção</a>
            </li>
            <li class="active">
                <strong>Folha de informações</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <button id="btfolha" class="btn btn-primary">Imprimir folha de informações</button>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="ibox-content">

    <table class="table tabela">
        {!! Form::open(array('route' => 'folha.filtro', 'method' => 'get')) !!}
        <tr>
            <td>
                {!! Form::select('filter_isotopo', $filter_isotopo, $isotopo, array('id'=>'isotopo', 'class'=>'chosen-select')) !!}
            </td>

            <td>
                {!! Form::select('filter_modelo', $filter_modelo , $modelo, array('id'=>'modelo', 'class'=>'chosen-select')) !!}
            </td>
            <td>
                {!! Form::select('filter_unidade', $filter_unidades , $unidade, array('id'=>'unidade', 'class'=>'chosen-select')) !!}
            </td>
            <td>
                <button type="submit" data-tooltip="tooltip" class="btn btn-primary fa fa-filter" style="width: 41px; height:34px;" title="Filtrar tabela"></button>
                
                {!! Form::close() !!}
            </td>
        </tr>
    </table>

    <input type="hidden" id="token" value="{{ csrf_token() }}">
   
    <div class="area_impressao" name="area_impressao" id="area_impressao">  
    <br>  
        <table class="tabelafolhafontes" border="1">
            <thead>
                <tr>
                    <td colspan="2" width="140" height="50" align="center">
                        <img border="0" src="../img/img_logo_proposta.png" width="130" height="40">
                    </td>
                    <td colspan="10" align="center">
                        <font style="font-size: 22px;" face="Arial">Folha de informações sobre <b>Fontes Radioativas e Blindagens</font></b>
                    </td>
                    <td colspan="2" align="right">
                        <font style="font-size: 12px;" face="Arial">Data: {{ $now->format('d/m/Y') }}</font>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><font style="font-size: 12px;" face="Arial">Emitido por: {{ Auth::user()->nome }} </font></td>
                    <td colspan="10"><font style="font-size: 12px;" face="Arial">Departamento: Radioproteção</font></td>
                    <td colspan="2" align="left"><font style="font-size: 12px;" face="Arial">Visto:</font></td>
                </tr>
                <tr>
                <th rowspan="2" width="7%">Número<br> da Fonte</th>
                <th rowspan="2">Irradiador</th>
                <th colspan="2">Atividade CI</th>
                <th rowspan="2">Atividade<br>TBq</th>
                <th rowspan="2">Pasta</th>
                <th rowspan="2" width="8%">Isótopo</th>
                <th rowspan="2" width="13%">Modelo</th>
                <th rowspan="2">Cabo</th>
                <th rowspan="2">Gabarito<br> de teste</th>
                <th rowspan="2" width="6%">Foco</th>
                <th rowspan="2">UN</th>
                <th rowspan="2" width="13%">Observações</th>
            </tr>
            <tr>
                <th>{{ $now->format('d/m')}}</th>
                <th>{{ $em30dias->format('d/m')}}</th>
            <tr>
            </thead>

            <tbody>
                <!-- #### CRAWLERS #### -->            
                @foreach ($cargasAtuais as $key => $carga)
                    @foreach ($fontes as $keyfonte => $fonte)
                            
                        @if($fonte->cabo == 'Crawler' && $fonte->id == $key)
                            
                            @include('radioprotecao.fonte.partials.table_row', array($fonte, $class='crawler'))  
                            <?php unset($fontes[$keyfonte]); unset($cargasAtuais[$key]); ?>
                                
                        @elseif($fonte->cabo == 'Crawler') 
                            @include('radioprotecao.fonte.partials.table_row', array($fonte, $class='crawler'))  
                            <?php unset($fontes[$keyfonte]); ?>
                                
                        @endif

                    @endforeach
                @endforeach

                <!-- #### COMANDOS #### -->

                @foreach ($cargasAtuais as $key => $carga)
                    @foreach ($fontes as $keyfonte => $fonte)

                        @if($fonte->cabo == 'Comando')
                            @if($fonte->id == $key)
                                @include('radioprotecao.fonte.partials.table_row', array($fonte, $class='comando'))  
                                <?php unset($fontes[$keyfonte]); unset($cargasAtuais[$key]); ?>
                            @endif
                        @endif  

                    @endforeach
                @endforeach    



                <!-- #### SELENIO #### -->

                @foreach ($cargasAtuais as $key => $carga)
                    @foreach ($fontes as $keyfonte => $fonte)

                        @if(strstr($fonte->isotopo, 'nio') && ($fonte->cabo != 'Crawler') && ($fonte->cabo != 'Comando'))
                            @if($fonte->id == $key)
                                @include('radioprotecao.fonte.partials.table_row', array($fonte, $class='selenio'))  
                                <?php unset($fontes[$keyfonte]); unset($cargasAtuais[$key]); ?>
                            @endif
                        @endif  

                    @endforeach
                @endforeach 

                <!-- #### IRIDIO #### -->

                @foreach ($cargasAtuais as $key => $carga)
                    @foreach ($fontes as $keyfonte => $fonte)

                        @if(strstr($fonte->isotopo, 'dio') && ($fonte->cabo != 'Crawler') && ($fonte->cabo != 'Comando'))
                            @if($fonte->id == $key && isset($cargasAtuais[$key]))
                                @include('radioprotecao.fonte.partials.table_row', array($fonte, $class='iridio'))
                                <?php unset($fontes[$keyfonte]); unset($cargasAtuais[$key]); ?>     
                            @endif  
                        @endif
                                
                    @endforeach
                @endforeach

                <!-- #### COBALTO #### -->

                @foreach ($cargasAtuais as $key => $carga)
                    @foreach ($fontes as $keyfonte => $fonte)

                        @if(strstr($fonte->isotopo, 'obalto') && ($fonte->cabo != 'Crawler') && ($fonte->cabo != 'Comando'))
                                @include('radioprotecao.fonte.partials.table_row', array($fonte, $class='cobalto'))
                                <?php unset($fontes[$keyfonte]); unset($cargasAtuais[$key]); ?> 
                        @endif
                                
                    @endforeach
                @endforeach

                <!-- #### RAIO-X #### -->
                    
                @foreach ($cargasAtuais as $key => $carga)
                    @foreach ($fontes as $keyfonte => $fonte)

                        @if(strstr($fonte->numero, 'aio'))
                            @include('radioprotecao.fonte.partials.table_row', array($fonte, $class='raio-x'))
                            <?php unset($fontes[$keyfonte]); unset($cargasAtuais[$key]); ?> 
                        @endif
                                
                    @endforeach
                @endforeach
                
        </tbody> 
        </table>
        <table>
        <tr>
            <td><font style="font-size: 10px;" face="Arial">LEGENDA:</font></td>
            <td class="crawler"><font style="font-size: 10px;" face="Arial">&nbsp; Crawler &nbsp; </font></td>
            <td class="comando"><font style="font-size: 10px;" face="Arial">&nbsp; Comando Crawler &nbsp; </font></td>
            <td class="selenio"><font style="font-size: 10px;" face="Arial">&nbsp; Selênio &nbsp; </font></td>
            <td class="iridio"><font style="font-size: 10px;" face="Arial">&nbsp; Irídio &nbsp; </font></td>
            <td class="abaixo10"><font style="font-size: 10px;" face="Arial">&nbsp; Irídio entre 10 e 20 Ci &nbsp; </font></td>
            <td class="abaixo20"><font style="font-size: 10px;" face="Arial">&nbsp; Irídio Abaixo de 10 Ci &nbsp; </font></td>
            <td class="cobalto"><font style="font-size: 10px;" face="Arial">&nbsp; Cobalto &nbsp; </font></td>
            <td class="raio-x"><font style="font-size: 10px;" face="Arial">&nbsp; Raio-x &nbsp; </font></td>
        </tr>
        </table>
    </div>    
    </div>
</div>
</div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop