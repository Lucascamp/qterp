@extends('layouts.master')

@section('menu')

@include('radioprotecao.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Fontes</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/radioprotecao">Radioproteção</a>
            </li>
            <li>
                <a href="/fonte">Fontes</a>
            </li>
            <li class="active">
                <strong>Cadastrar</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de fonte<small>(*)campos obrigatórios</small></h5>
                </div>
                <div class="ibox-content" style="height:650px">
                    {!! Form::open(array('route' => 'fonte.salvar', 'class'=>'form-inline', 'files' => true)) !!}
                    
                    {!! Form::hidden('created_by', '1') !!}
                    <input type="hidden" id="token" value="{{ csrf_token() }}">

                    @include('radioprotecao.fonte.partials._dados')
                    

                    <div class="pull-right">
                    {!! Form::submit('Cadastrar Fonte', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}
                </div>
                <br><br>
                @if ($errors->any())

                <br><br>
                    <div class="alert-group">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                            @endforeach
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>    
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop
