@extends('layouts.master')

@section('menu')
@include('rh.menu')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Recursos Humanos</strong>
            </li>
        </ol>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="text-center animated fadeInRightBig">
                <h3 class="font-bold">Recursos Humanos</h3>
                <div class="error-desc">
                    @foreach ($areas as $area)
                        <a href="{{$area['rota']}}" class="btn btn-primary m-t">{{$area['descricao']}}</a>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
