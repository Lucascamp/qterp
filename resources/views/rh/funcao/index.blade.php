@extends('layouts.master')

@section('menu')

@include('rh.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Funções</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/rh">Recursos humanos</a>
            </li>
            <li class="active">
                <strong>Funções</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">
            <a href="/funcao/cadastrar" class="btn btn-primary">Nova função</a>
        </div>
    </div>
</div>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>Funções Cadastradas</h5>
        </div>
        @if ($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="ibox-content">

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <div class="clear"><br></div>

        <table class="table table-bordered tabela">
            <thead>
                <tr>
                    <th>Função</th>
                    <th>Macroprocesso</th>
                    <th>Dependência</th>
                    <th>Ultima Alteração</th>
                    <th>Feito por</th>
                    <th colspan="3">Ações</th>
                </tr>
            </thead>

            <tbody>
             @if (isset($funcoes))
             @foreach ($funcoes as $funcao)
             <tr>
                <td>{{{ $funcao->funcao }}}</td>
                <td>{{{ $funcao->macroprocesso }}}</td>
                <td>{{{ $funcao->dependencia }}}</td>
                <td>{{{ $funcao->updated_at }}}</td>
                <td>{{{ isset($funcao->updated_by) ? $funcao->updated_by :  $funcao->created_by }}}</td>
                <td> 

                    <a href="{{ route('funcao.editar', array($funcao->id)) }}" data-toggle="tooltip" title="Editar" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-info dim">
                        <i class="fa fa-pencil"></i></a>
                    </td>

                    <td> 
                     <a href="{{ route('funcao.perfil', array($funcao->id)) }}" data-toggle="tooltip" title="Perfil" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-warning dim">
                        <i class="fa fa-file"></i></a>
                    </td>  

                    <td>
                        <button type="button" data-target="#modalExcluir" data-toggle="modal" data-tooltip="tooltip" class="btn btn-danger fa fa-close excluirBtn" style="width: 41px; height:34px;" title="Excluir"
                        data_value="{{ $funcao->id }}" data_value2="{{ $funcao->descricao }}"></button>
                    </td>

                </tr>
                @endforeach
                @else
                <tr> 
                    <td colspan="20" class="center">
                        Nenhuma função encontrada.
                    </td>
                </tr>
                @endif    
            </tbody> 
        </table>
    </div>
</div>
</div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop