@extends('layouts.master')

@section('menu')

@include('rh.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Perfil</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/rh">Recursos Humanos</a>
            </li>
            <li>
                <a href="/funcao">Funções</a>
            </li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <li><a href="#" id="btRDO">Imprimir RDO</a></li>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div align="center"><br><br>
                        <div class="area_impressao" name="area_impressao" id="area_impressao">
                            <table border="0" style="border-left:1px solid #808080; border-right:1px solid #808080; border-top:1px solid #808080; border-bottom:1px solid #808080; width: 1090px; height: 1400px; ">
                                <tr>
                                    <td colspan="3" style="border-style: solid; border-width: 1px">    
                                        Pela presente Ordem de serviço objetivamos informar os trabalhadores que executam suas atividades laborais nesse setor, conforme estabelece a NR-1, item 1.7, sobre as condições de segurança e saúde às quais estão expostos, como medida preventiva e ,tendo como parâmetro os agentes físicos, químicos, e biológicos citados na NR-9 - Programa de Prevenção de Riscos Ambientais(Lei nº 6514 de 22/12/1977,Portaria nº 3214 de 08/06/1978), bem como os procedimentos de aplicação da NR-6  - Equipamento de Proteção Individual – EPI de forma a padronizar comportamentos para prevenir acidentes e/ou doenças ocupacionais.
                                    </td>
                                </tr>
                                <tr>   
                                    <td align="left"   style="border-style: solid; border-width: 1px">    
                                        func nome 
                                        func cpf
                                        func admissao
                                    </td>
                                </tr> 

                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">Descrição das Atividades:</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='left'>{{ $funcao->funcao }}</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='center' style="border-style: solid; border-width: 1px">Riscos:</td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left'>Risco Fisico: {{ $funcao->risco_fisico }}</td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left'>Risco Quimico: {{ $funcao->risco_quimico }}</td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left'>Risco Biologico: {{ $funcao->risco_biologico }}</td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left'>Risco Ergonomico: {{ $funcao->risco_ergonomico }}</td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left'>Risco Acidentes: {{ $funcao->risco_acidente }}</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='center' style="border-style: solid; border-width: 1px">Equipamentos de Proteção Individual (EPI) Necessários e/ou Utilizados:</td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left'>{{ $funcao->epi }}</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='center' style="border-style: solid; border-width: 1px">Medidas Preventivas para os Riscos de Ambientais:</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='left'>{{ $funcao->medidas_preventivas }}</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='center' style="border-style: solid; border-width: 1px">Obrigações do colaborador:</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">Obrigações específicas da função:</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='left'>{{ $funcao->responsabilidade_operativa }}</td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">Obrigações gerais
                                    </td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">1.     Cumprir os compromissos expressamente assumidos no contrato individual de trabalho, com zelo, atenção e competência profissional;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">2.     Atuar atendendo os parâmetros estabelecidos na Política de Gestão Integrada da Qualitec e no Código de Ética e Política Anticorrupção da Applus;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">3.     Cumprir com as leis e normas ambientais de segurança, setoriales e tecnicas
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">4.     Obedecer às ordens e instruções emitidas pelos superiores hierárquicos;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">5.     Sugerir medidas para maior eficiência do serviço;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">6.     Observar a máxima disciplina no local de trabalho;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">7.     Zelar pela ordem e asseio no local de trabalho;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">8.     Relatar as falhas para chefia imediata e o SGI.
                                    </td>
                                </tr>

                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">9.     Zelar pela boa conservação das instalações, equipamentos e máquinas, comunicando as anormalidades notadas;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">10.  Manter na vida privada e profissional conduta compatível com a dignidade do cargo ocupado e com a reputação do quadro de pessoal da Empresa;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">11.  Usar adequadamente o uniforme definido para o trabalho. Para os casos em que não seja exigido o uniforme, utilizar trajes adequados ao ambiente de trabalho ( não utilizar chinelos, camisas sem manga, bermudas, etc),
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">12.  Usar os meios de identificação pessoal estabelecidos;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">13.  O colaborador da Qualitec deve conhecer e respeitar as normas e exigências dos nossos clientes, pois é onde realizamos a maioria de nossos trabalhos;
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">14.  Informar ao Setor de Pessoal qualquer modificação em seus dados pessoais, tais como estado civil, militar, aumento ou redução de pessoas na família, eventual mudança de residência, etc.
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">15.  Respeitar todos os colaboradores da QUALITEC
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">16.  Atender as normas de segurança, saúde e meio ambiente:
                                        · Zelar pela sua segurança e saúde e a de outras pessoas que possam ser afetadas por suas ações ou omissões no trabalho;<br>
                                        · Responsabilizar-se junto com a empresa pelo cumprimento das disposições legais e regulamentares, inclusive quanto aos procedimentos internos de segurança, saúde e meio ambiente;<br>
                                        · Utilizar adequadamente os meios e EPI`s (Equipamento de proteção individual), fornecidos pelo empregador;<br>
                                        · Responsabilizar-se, integralmente, pela guarda, conservação e devolução dos EPI`s que me forem entregues;<br>
                                        · Submeter-se aos exames médicos previstos nas Normas Regulamentadoras – NR-07;<br>
                                        · Colaborar com a empresa na aplicação das Normas Regulamentadoras – NR’s;<br>
                                        · Contribuir para a preservação do meio ambiente e seguir as instruções do PGR – Programa de Gerenciamento de Resíduos;<br>
                                        · Todo acidente ocorrido no local de trabalho ou fora dele a serviço da empresa deverá ser imediatamente comunicado ao seu supervisor e ao setor de segurança do trabalho, o qual deverá ser tradado conforme o plano de emergência de cada unidade;<br>
                                        · Reportar atos e condições inseguras identificadas durante a operação;<br>
                                        · Paralise imediatamente os trabalhos em caso de riscos graves e iminentes e comunique ao seu supervisor imediato e/ou ao setor de segurança do trabalho;<br>
                                        · Prestigie as reuniões da CIPA;<br>
                                        · Ter a atenção voltada para o trabalho a ser executado, familiarizando com o local e equipamentos do processo;<br>
                                        · Realizar inspeções nas máquinas, ferramentas e equipamentos, verificando  sempre se as mesmas estão em perfeitas condições de uso;<br>
                                        · Para o transporte manual de cargas mantenha a coluna ereta, flexionando os joelhos, não tencione a coluna vertebral, concentre o peso nas musculaturas das pernas e braços;<br>
                                        · Não executar qualquer atividade ou operar equipamentos para quais não tenha sido treinado, habilitado e autorizado;<br>
                                        · Cumprir com os procedimentos de trabalho seguro;<br>
                                        · Fazer uso adequado das substâncias químicas manuseadas e disponibilizar os resíduos de maneira correta;<br>
                                        · Somente pessoal autorizado pode trabalhar com fontes radioativas. Em atividades com fontes radioativas, cumprir estritamente com os procedimentos de segurança e os controles de radiação e uso contínuo do filme dosimétrico;<br>
                                        <div style="border-top-style: solid; border-width: 1px" class="page-break"></div>
                                        Nota: o medidor individual de radiação (BIP) é entregue ao colaborador mediante recibo pelo departamento de radioproteção e se for transferido ao outro colaborador na obra deverá ser sob recibo e uma via desse recibo será entregue ao departamento de radioproteção. 
                                        Os filmes dosimétricos são para uso individual e obrigatória. São trocados mensalmente e o colaborador é responsável por guardá-lo na caixa de filmes dosimétricos após a jornada de trabalho;<br>
                                        · Reportar ao chefe imediato o uso de medicamentos que podem afetar sua concentração e equilíbrio;<br>
                                        · Não fumar nos locais de trabalho e sim somente nas áreas designadas;<br>
                                        · Não prestar nenhum tipo de serviços sob efeito de álcool ou drogas. É proibido na empresa e nas áreas onde a empresa atua: Consumir, Vender, Ter ou Estar sobre os efeitos de álcool, substâncias alucinógenas ou qualquer outra droga que afete o comportamento;<br>
                                        · Utilizar os veículos respeitando a legislação de trânsito vigente e as normas internas de Utilização de Veículos<br>
                                        · Reportar qualquer dano, falha ou mal funcionamento dos veículos;<br>
                                        · Não manusear celular ou usar fones de ouvido durante a realização de suas atividades.
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">17.  Atender as normas de qualidade na prestação de serviços:
                                    · Respeitar os procedimentos e normas definidas pelos clientes em suas instalações;<br>
                                    · Ingressar somente nas áreas permitidas para a realização dos serviços;<br>
                                    · Reportar ao chefe imediato qualquer reclamação ou queixa do cliente;<br>
                                    · Manter confidencialidade das informações relacionadas ao cliente, suas instalações  ou serviços prestados a ele.

                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">18.  Respeitar os colegas de trabalho evitar qualquer tipo de agressão ou abuso físico ou verbal e reporta-los caso aconteçam
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">19.  Responder por prejuízo causados à Empresa, quer por dolo ou culpa (negligência, imperícia ou imprudência), caracterizando-se a responsabilidade por danos e avarias em materiais sob sua guarda ou sujeitos à sua fiscalização;<br>
                                    Nota: A responsabilidade administrativa não exime o colaborador da responsabilidade civil ou criminal cabível. <br>
                                    As indenizações e reposições por prejuízos causados são descontadas dos salários.
                                    </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left' style="border-style: solid; border-width: 1px">Declaração:</td>
                                </tr>
                                <tr> 
                                    <td colspan="3" align='left'>Recebi treinamento de segurança e saúde no trabalho, bem com todos os equipamentos de proteção individual para neutralizar a ação dos agentes nocivos presentes no meu ambiente de trabalho.<br>
                                    Serei cobrado, conforme ampara legal, com relação ao uso destes equipamentos e estou ciente de que a não utilização é passível de Sansões Legais.
                                    <br>

                                    </td>
                                </tr>

                                <tr>
                                    <td><br><br><br>Local/Data: _______________, de ___/___/___</td>
                                </tr>
                                <tr>
                                    <td><br><br><br>Assinatura do colaborador:____________________________________________________<br><br></td>

                                </tr>


                                


                                



                            </table>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</br>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop
