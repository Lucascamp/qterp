@extends('layouts.master')

@section('menu')

@include('rh.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Equipamentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/rh">Recursos Humanos</a>
            </li>
            <li>
                <a href="/funcao">Funções</a>
            </li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
    {{-- <div class="col-sm-4">
        <div class="title-action">
            <a href="#" class="btn btn-primary">Opções</a>
        </div>
    </div> --}}
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Função<small>(*)campos obrigatorios</small></h5>
                </div>
                <div class="ibox-content">
                    {!!  Form::open(array('method' => 'PATCH', 'route' => array('funcao.atualizar', $funcao->id), 'class'=>'form-inline')) !!}
                    {!! Form::hidden('updated_by', $usuario->id) !!}

                    <input type="hidden" id="token" value="{{ csrf_token() }}">

                    @include('rh.funcao.partials._dados')
                    
                    <div class="pull-right">
                    {!! Form::submit('Editar Função', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}
                </div>
                <br><br>
                @if ($errors->any())

                <br><br>
                    <div class="alert-group">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                            @endforeach
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</br>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>

@stop
