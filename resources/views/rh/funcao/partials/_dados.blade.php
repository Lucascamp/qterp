<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Dados da função</td>
    </tr>

    <tr>
        <td class="tblabel">Função(*)</td>
        <td>
        {!! Form::text('funcao', isset($funcao) ? $funcao->funcao : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Macroprocesso(*)</td>
        <td>
        {!! Form::text('macroprocesso', isset($funcao) ? $funcao->macroprocesso : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Dependência(*)</td>
        <td>
        {!! Form::text('dependencia', isset($funcao) ? $funcao->dependencia : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Função Operativa(*)</td>
        <td>
        {!! Form::text('funcao_operativa', isset($funcao) ? $funcao->funcao_operativa : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Função SMS</td>
        <td>
        {!! Form::text('funcao_sms', isset($funcao) ? $funcao->funcao_sms : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Responsabilidade Operativa Específica</td>
        <td>
        {!! Form::text('responsabilidade_operativa', isset($funcao) ? $funcao->responsabilidade_operativa : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Responsabilidade Específica SMS</td>
        <td>
        {!! Form::text('responsabilidade_sms', isset($funcao) ? $funcao->responsabilidade_sms : null, array('class'=>'form-control')) !!}
        </td>
        <td class="tblabel" colspan="2"></td>
    </tr>

</table>

<table class="table table-bordered tabela">
    <tr>
        <td class="tbtitulo" colspan="8">Riscos</td>
    </tr>

    <tr>
        <td class="tblabel">Fisico</td>
        <td>
        {!! Form::text('risco_fisico', isset($funcao) ? $funcao->risco_fisico : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Quimico</td>
        <td>
        {!! Form::text('risco_quimico', isset($funcao) ? $funcao->risco_quimico : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Biologico</td>
        <td>
        {!! Form::text('risco_biologico', isset($funcao) ? $funcao->risco_biologico : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Ergonomico</td>
        <td>
        {!! Form::text('risco_ergonomico', isset($funcao) ? $funcao->risco_ergonomico : null, array('class'=>'form-control')) !!}
        </td>
    </tr>

    <tr>
        <td class="tblabel">Acidente</td>
        <td>
        {!! Form::text('risco_acidente', isset($funcao) ? $funcao->risco_acidente : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Equipamentos de proteção individual</td>
        <td>
        {!! Form::text('epi', isset($funcao) ? $funcao->epi : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel">Medidas Preventivas</td>
        <td>
        {!! Form::text('medidas_preventivas', isset($funcao) ? $funcao->medidas_preventivas : null, array('class'=>'form-control')) !!}
        </td>

        <td class="tblabel" colspan="2"></td>

    </tr>

</table>


