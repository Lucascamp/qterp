@extends('layouts.master')

@section('menu')

@include('patrimonio.menu')

@stop

@section('main')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Bem Vindo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/patrimonio">Gestão de Patrimônio</a>
            </li>
            <li>
                <a href="/equipamento">Equipamentos</a>
            </li>
            <li class="active">
                <strong>Histórico</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-7">
        <div class="title-action">

            <div class="btn-group">
             {{--  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opções<span class="caret"></span></button><div class="dropdown-backdrop"></div> --}}
           {{--  <ul class="dropdown-menu">
                <li><a href="#" id="btboletim">Imprimir BM</a></li>
            </ul> --}}
        </div>
    </div>
</div>
</div>

<div class="row">
    <div class="wrapper wrapper-content">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Selecionar o equipamento para exibir o histórico</h5>
            </div>
            <div class="ibox-content">
                <input type="hidden" id="token" value="{{ csrf_token() }}">

                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tblabel">
                            {!! Form::select('descricao', $descricoes, null ,array('id' => 'descricao', 'class'=>'form-control')) !!}
                        </td>

                        <td class="tblabel">
                            {!! Form::select('aplicacao', ['' => 'Selecione'], null ,array('id' => 'aplicacao', 'class'=>'form-control')) !!}
                        </td>

                        <td class="tblabel">
                            {!! Form::select('patrimonio', ['' => 'Selecione'], null ,array('id' => 'patrimonio', 'class'=>'form-control')) !!}
                        </td>
                    </tr>
                </table>

                <div class="pull-right">
                    <a class="btn btn-primary" id="bt_carregar_historico">Carregar Histórico</a> 
                </div>

                <br><br><br><br>

                <div class="ibox-content">
                    {{-- <div class="area_impressao" name="area_impressao" id="area_impressao"> --}}
                    <table class="table table-bordered tabela2">
                        <thead> 
                         <tr>
                            <th>Data Movimentação</th>
                            <th>Centro de custo</th>
                            <th>Projeto</th>
                            <th>Funcionário Responsável</th>
                            <th>Usuário Receptor</th>
                            <th>Recebido</th>
                            <th>Motivo</th>
                        </tr>
                    </thead>    
                    <tbody id="tablehistorico">   

                    </tbody>
                </table>    
                {{-- </div> --}}
            </div>
        </div>
    </div>
</div>
</br>
</br>
</br>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop
