<div class="modal fade" id="modalHistoricoCalibracao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #026d3b">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Histórico de Calibração</font></h4>
    </div>
    <div class="modal-body" id="bodym">
    <div style="width: 100%; height: 65%; overflow-y: scroll;">
    <table class="table table-bordered tabela2">
        <thead> 
             <tr>
                <th>Descrição</th>
                <th>Patrimônio</th>
                <th>Serial</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Frequência</th>
            </tr>
        </thead>    
        <tbody>   
                <td><span id='descricao_equip'></span></td>
                <td><span id='patrimonio_equip'></span></td>
                <td><span id='serial_equip'></span></td>
                <td><span id='marca_equip'></span></td>
                <td><span id='modelo_equip'></span></td>
                <td><span id='frequencia_equip'></span></td>
        </tbody>
    </table> 
    <table class="table table-bordered tabela2">
        <thead> 
             <tr>
                <th>Laboratório</th>
                <th>Certificado</th>
                <th>Data Calibração</th>
                <th>Primeiro Uso</th>
                <th>Vencimento</th>
            </tr>
        </thead>    
        <tbody id="tablehistorico">   
                <td colspan="5"><font size="3"><center>Carregando...</center></font></td>
        </tbody>
        </table>    
        </div>
    </div>
    <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </div>
</div>
</div>
</div>