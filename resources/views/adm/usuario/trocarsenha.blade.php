<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>QTERP | Trocar Senha</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
         <link href="/css/bootstrap.css" rel="stylesheet">
        <link href="/css/font.css" rel="stylesheet">
        <link href="/css/custom.css" rel="stylesheet">
        <link href="/css/animate.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/dataTables.responsive.css" rel="stylesheet">
        <link href="/css/dataTables.tableTools.min.css" rel="stylesheet">
        <link href="/css/proposta.css" rel="stylesheet">
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">{{$usuario->nome}}</div>
            @if (!empty($erro))
                        <div class="no-margin alert alert-danger">
                            {{$erro}}
                        </div>
                    @endif
            <form role="form" method="POST" action="/adm/salvarnovasenha">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="password" class="form-control" name="senha_velha" placeholder="Senha Antiga" value="{{ old('usuario') }}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="senha_nova" placeholder="Nova Senha">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="conf_senha" placeholder="Confirmar Nova Senha">
                    </div>
                    <!-- <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div> -->
                </div>
                <div class="footerLogin">
                    <button type="submit" class="btn bg-olive btn-block">Salvar</button>

                    <!-- <p><a href="#">I forgot my password</a></p>

                    <a href="register.html" class="text-center">Register a new membership</a> -->
                </div>
            </form>

            <!-- <div class="margin text-center">
                <span>Sign in using social networks</span>
                <br/>
                <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
                <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

            </div> -->
        </div>

        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>
