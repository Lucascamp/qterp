@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Configurações de Usuário</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/adm">Admin</a>
            </li>
            <li>
               Usuário
            </li>
            <li class="active">
                <strong>Cadastro</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <!-- <h3 class="font-bold">Módulos</h3>
                <div class="error-desc">
                </div> -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Edição de Usuarios </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <div id="mensagens">
                        </div>
                        <table class="table table-bordered tabela">
                            <thead>
                            </thead>
                            <tbody>
                                <table class="table table-bordered tabela" >
                                    <thead>
                                        <tr>
                                            <th class="tbtitulo" colspan="6">Usuarios existentes</td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td class="tbsubtitulo">ID</td>
                                        <td class="tbsubtitulo">Nome</td>
                                        <td class="tbsubtitulo">Email</td>
                                        <td class="tbsubtitulo">login</td>
                                        <td class="tbsubtitulo">Data de Criação</td>
                                        <td class="tbsubtitulo">Opções</td>
                                    </tr>
                                    <tbody id="listaUsuarios">
                                        
                                    </tbody>
                                </table>
                            </tbody>
                        </table>
                        </br>                                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
