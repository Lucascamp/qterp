@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Configurações de Usuário</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/adm">Admin</a>
            </li>
            <li>
               Usuário
            </li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <!-- <h3 class="font-bold">Jornada</h3> -->
                <!-- <div class="error-desc"> -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><i class="fa fa-sliders"></i> Editando usuário</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            <input type="hidden" id="id" value="{{ $id }}">
                            <table class="table table-bordered tabela" id="editable">
                                <thead>
                                </thead>
                                <tbody>
                                    <table class="table table-bordered tabela">
                                        <tr>
                                            <td class="tbtitulo" colspan="12">Dados do Usuario</td>
                                        </tr>
                                        <tr>
                                            <td class="tblabel">Funcionário</td>
                                            <td><input type="text" id="funcionario" value="{{$usuarioE['nome']}}" class="tbinput form-control"></td>
                                             <td class="tblabel">Login</td>
                                            <td><input type="text" id="login" value="{{$usuarioE['login']}}"  class="tbinput form-control"></td>
                                            <td class="tblabel">Email</td>
                                            <td><input type="text" id="email" value="{{$usuarioE['email']}}" class="tbinputEM form-control"></td>
                                            <td class="tblabel">Senha</td>
                                            <td><input type="password" id="senha" placeholder="senha" class="tbinputPW form-control"></td>
                                            <td class="tblabel">Confirmar Senha</td>
                                            <td><input type="password" id="confsenha" placeholder="senha" class="tbinputPW form-control"></td>
                                            <td class="tblabel">Unidade</td>
                                            <td><input type="text" id="un" value="{{$usuarioE['unidade']}}" class="tbinputUN form-control"></td>
                                        </tr>
                                    </table>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Permissões </h5>
                                                    <div class="ibox-tools">
                                                        <a class="collapse-link">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="ibox-content">
                                                    @foreach ($modulos as $modulo)
                                                        <div class="col-lg-12">
                                                            <div>
                                                                <div class="divsubtitulo">{{$modulo['descricao']}}</div>
                                                                <div class="row">
                                                                @foreach ($areas as $area)
                                                                    @if($area['modulo_id'] == $modulo['id'])
                                                                        <div class="col-lg-4">
                                                                            <div class="divlabel divsubtitulo2">{{$area['descricao']}}</div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                                </div>
                                                                @foreach ($areas as $area)
                                                                    @if($area['modulo_id'] == $modulo['id'])
                                                                        <div class="col-lg-4">
                                                                            @foreach ($permissoes as $permissao)
                                                                                @if($permissao['areamodulo_id'] == $area['id'])
                                                                                    <div class="checkbox i-checks">
                                                                                        <label class="">
                                                                                            <div class="icheckbox_square-green">
                                                                                                @if(in_array($permissao['id'], $perm_user))
                                                                                                    <input type="checkbox" class="checkpermi" id="{{$permissao['id']}}" checked>
                                                                                                @else
                                                                                                    <input type="checkbox" class="checkpermi" id="{{$permissao['id']}}">
                                                                                                @endif
                                                                                            </div>{{$permissao['descricao']}} 
                                                                                        </label>
                                                                                    </div>
                                                                                @endif
                                                                            @endforeach
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                     @endforeach
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Permissão Opções </h5>
                                                    <div class="ibox-tools">
                                                        <a class="collapse-link">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="ibox-content">
                                                    @foreach ($permissao_opcoes as $perop)
                                                        @if (isset($permissoes_opcoes_usuario) && in_array($perop['id'],$permissoes_opcoes_usuario))
                                                            <input type="checkbox" class="checkop" id="perop{{$perop['id']}}" checked> {{$perop['descricao']}} </br>
                                                        @else
                                                            <input type="checkbox" class="checkop" id="perop{{$perop['id']}}"> {{$perop['descricao']}} </br>
                                                        @endif
                                                        
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tbody>
                            </table>
                            <div class="pull-right">
                                <div class="btn btn-primary" id="bt_alterar_Usuario">Salvar</div>
                            </div>
                            </br>
                            
                            <form role="form" class="form-inline">
                            </form>
                        </div>
                    </div>
                <!-- <a href="" class="btn btn-primary m-t">Ensaio - Gamagrafia</a> -->
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
@stop
