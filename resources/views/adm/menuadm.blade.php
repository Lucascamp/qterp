<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{$usuario->nome}}</strong>
                            </span> 
                            <!-- <span class="text-muted text-xs block"><b class="caret"></b></span>  -->
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <!-- <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li> -->
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    QT-ERP
                </div>
            </li>
            <li>
                <a href=""><i class="fa fa-file-text"></i> <span class="nav-label">Ensaio</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/adm/ensaio/jornada">Jornada</a></li>
                    <li><a href="/adm/ensaio/filmes">Filmes</a></li>
                    <li><a href="/adm/ensaio/juntas">Juntas</a></li>
                    <li><a href="/adm/ensaio/servicos">Serviços</a></li>
                    <li><a href="/adm/ensaio/descontinuidades">Descontinuidades</a></li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-child"></i> <span class="nav-label">Usuário</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/adm/user/cadastrar">Cadastrar</a></li>
                    <li><a href="/adm/user/editar">Editar</a></li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-cube"></i> <span class="nav-label">Modulos</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/adm/modulos">Home</a></li>
                    <li><a href="/adm/modulos/cadastro">Cadastrar</a></li>
                    <li><a href="/adm/modulos/editar">Editar</a></li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-at"></i> <span class="nav-label">Intranet</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/intranet/administrativo">Ex. Administrativo</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
