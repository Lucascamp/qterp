@extends('layouts.master')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Informações Almoxarifado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Infos</strong>
            </li>
        </ol>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">
<br>

<div class="row">
<div class="col-lg-12">
        <h1>Consumo nos ultimos 7 dias (verificação no servidor da unidade)</h1>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <table width="100%" border=1>
                        <tr>
                            <td align="center" colspan="2">MG</td>
                            <td align="center" colspan="2">BA</td>
                            <td align="center" colspan="2">RN</td>
                            <td align="center" colspan="2">MC</td>
                            <td align="center" colspan="2">PE</td>
                            <td align="center" colspan="2">RJ</td>
                        </tr>
                        <tr>
                            <?php 
                                $contlinhas = 0;
                            ?>
                        @foreach ($gasto_serv as $key => $unidade)
                            @if ($unidade == 'ERRO')
                                <?php continue; ?>
                            @endif
                            @foreach ($unidade as $k => $element)
                                @if (count($unidade) > $contlinhas)
                                    <?php $contlinhas = count($unidade); ?>
                                @endif
                            @endforeach
                        @endforeach
                        @foreach ($gasto_serv as $key => $unidade)
                            <td colspan="2">
                                <table width="100%" border=1 >
                                @if (count($unidade) > 0 && $unidade != 'ERRO')
                                    <tr>
                                        <td align="center">ITEM</td>
                                        <td align="center">QUANT</td>
                                    </tr>
                                    @foreach ($unidade as $k => $element)

                                        <tr>
                                            <td align="center" title="{{$itens[$element->item_id]}}" style="font-size: 10px">{{substr($itens[$element->item_id],0,25)}}{{(strlen($itens[$element->item_id]) > 25)? '...' : ''}}</td>
                                            <td align="center" style="font-size: 10px">{{$element->quantidade}}</td>
                                        </tr>
                                    @endforeach
                                    @if (count($unidade) < $contlinhas)
                                        <?php $aux = $contlinhas - count($unidade)?>
                                        @for ($i = 0; $i < $aux ; $i++)
                                            <tr>
                                                <td style="font-size: 10px" align="center">-</td>
                                                <td style="font-size: 10px" align="center">-</td>
                                            </tr>
                                        @endfor
                                    @endif
                                @else
                                    <tr>
                                        <td align="center">ITEM</td>
                                        <td align="center">QUANT</td>
                                    </tr>
                                    @for ($i = 0; $i < $contlinhas ; $i++)
                                        <tr>
                                            <td style="font-size: 10px" align="center">-</td>
                                            <td style="font-size: 10px" align="center">-</td>
                                        </tr>
                                    @endfor
                                @endif
                                </table>
                            </td>
                        @endforeach
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
</div>
</div>
    <div class="row">
        <div class="col-lg-12">
        <h1>Ultimo registro</h1>
        @foreach ($ult_atualizacao as $key => $quantidade)
            @if ($quantidade != 'ERRO')
                
            
            <div class="col-lg-2">
                <div class="ibox">
                    <div class="ibox-content">
                        <h5 class="m-b-md">{{$key}}</h5>
                        <h2 class="text-navy">
                            {{$quantidade}}
                        </h2>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
        <h1>Ultima atualização Central</h1>
        @foreach ($ult_atualizacao_central as $key => $quantidade)
            @if ($quantidade)
                
            
            <div class="col-lg-2">
                <div class="ibox">
                    <div class="ibox-content">
                        <h5 class="m-b-md">{{$key}}</h5>
                        <h2 class="text-navy">
                            {{$quantidade}}
                        </h2>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
        </div>
    </div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
<script src="/js/jquery2.1.js"></script>
<script src="/js/jquery.sparkline.min.js"></script>
{{-- <script src="/js/admin/monitor.js"></script> --}}

@stop
