@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>Edição e Configuração dos Módulos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/adm">Admin</a>
            </li>
            <li>
                <a href="/adm/modulos">Módulos</a>
            </li>
            <li>
                <a href="/adm/modulos/editar">Edição</a>
            </li>
            <li class="active">
                <strong>Modulo {{$modulo->descricao}}</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <!-- <h3 class="font-bold">Módulos</h3>
                <div class="error-desc">
                </div> -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><i class="fa fa-sliders"></i> Cadastro de Módulos </h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" id="idmodulo" value="{{ $id }}">
                        <input type="hidden" id="statusmodulo" value="{{ $modulo->status }}">
                        <div id="mensagens">
                        </div>
                        <table class="table table-bordered tabela">
                            <thead>
                            </thead>
                            <tbody id="tbodycadastro">
                                <table class="table table-bordered tabela">
                                    <tr>
                                        <td class="tbtitulo" colspan="7">Editar Módulo {{$modulo->descricao}} </td>
                                    </tr>
                                    <tr>
                                        <td class="tblabel">Descrição</td>
                                        <td><input type="text" class="form-control" id="descricaoEditar" value="{{$modulo->descricao}}"></td>
                                        <td class="tblabel">Rota</td>
                                        <td><input type="text" class="form-control" id="rotaEditar" value="{{$modulo->rota}}"></td>
                                        <td class="tblabel">Status</td>
                                        @if($modulo->status == 0)
                                            <td class="tblabel" id="btstatus"><div class="btn btn-primary" id="bt_ativar_modulo">Ativar</div></td>
                                        
                                        @else
                                            <td class="tblabel" id="btstatus"><div class="btn btn-danger" id="bt_desativar_modulo">Desativar</div></td>
                                        
                                        @endif
                                        <td class="tblabel"><div class="btn btn-primary" id="bt_salvar_editar_modulo">Salvar Alterações</div></td>
                                    </tr>
                                </table>
                                <table class="table table-bordered tabela">
                                    <tr>
                                        <td class="tbtitulo" colspan="7">Áreas do Módulo {{$modulo->descricao}} </td>
                                    </tr>
                                    @foreach ($areas as $element)
                                        <tr>
                                            <td class="tblabel">Descrição</td>
                                            <td><input type="text" class="form-control" id="descricao_area{{$element->id}}" value="{{$element->descricao}}"></td>
                                            <td class="tblabel">Rota</td>
                                            <td><input type="text" class="form-control" id="rota_area{{$element->id}}" value="{{$element->rota}}"></td>
                                            <td class="tblabel">
                                                <div class="btn btn-primary bt_salvar_editar_area"  value="{{$element->id}}" >Salvar</div>
                                                <div class="btn btn-warning bt_permissoes" title="Inserir Permissões" value="{{$element->id}}">Permissões</div>
                                                <div class="btn btn-danger bt_excluir_area"  value="{{$element->id}}">Excluir</div>

                                            </td>
                                        </tr>
                                        <tr id="permissoesti"></tr>
                                        <tr id="permissoes"></tr> 
                                    @endforeach
                                    <tr>
                                        <td class="tblabel">Descrição</td>
                                        <td><input type="text" class="form-control" id="descricao_area"></td>
                                        <td class="tblabel">Rota</td>
                                        <td><input type="text" class="form-control" id="descricao_rota"></td>
                                        <td class="tblabel"><div class="btn btn-primary" id="bt_salvar_area">Inserir</div></td>
                                    </tr>
                                </table>
                            </tbody>
                        </table>
                        </br>                                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom">Modal title</h4>
      </div>
      <div class="modal-body" id="bodym">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer" id="footerm">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop
