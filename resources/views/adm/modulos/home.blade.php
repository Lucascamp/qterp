@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Cadastro e Configuração dos Modulos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/adm">Admin</a>
            </li>
            <li class="active">
                <strong>Módulos</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">Módulos</h3>
                <div class="error-desc">
                    Área para cadastro e configuração do sistema
                    <a href="/adm/modulos/cadastro" class="btn btn-primary m-t">Cadastrar Módulos</a>
                    <a href="/adm/modulos/editar" class="btn btn-primary m-t">Editar Módulos</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
