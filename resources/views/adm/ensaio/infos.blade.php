@extends('layouts.master')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Informações Gamagrafia</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Infos</strong>
            </li>
        </ol>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">
<br>

<div class="col-lg-12">
    <div class="row">
        <h1>Quantidade de GOs por Servidor</h1>
        @foreach ($countgos as $key => $quantidade)
            <div class="col-lg-2">
                <div class="ibox">
                    <div class="ibox-content">
                        <h5 class="m-b-md">{{$key}}</h5>
                        <h2 class="text-navy">
                            {{$quantidade}}
                        </h2>
                        <small><b>{{isset($ult_atualizacao[$key]) ? $ult_atualizacao[$key] : null}}</b></small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <h1>Quantidade de Relatorios por Servidor</h1>
        @foreach ($countrels as $key => $quantidade)
            <div class="col-lg-2">
                <div class="ibox">
                    <div class="ibox-content">
                        <h5 class="m-b-md">{{$key}}</h5>
                        <h2 class="text-navy">
                            {{$quantidade}}
                        </h2>
                        <small><b>{{isset($ult_atualizacao[$key]) ? $ult_atualizacao[$key] : null}}</b></small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+=' mini-navbar'</script>
<script src="/js/jquery2.1.js"></script>
<script src="/js/jquery.sparkline.min.js"></script>
{{-- <script src="/js/admin/monitor.js"></script> --}}

@stop
