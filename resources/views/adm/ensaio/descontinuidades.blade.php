@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Configurações do modulo Ensaio</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/adm">Admin</a>
            </li>
            <li>
               Ensaio
            </li>
            <li class="active">
                <strong>Descontinuidades</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <!-- <h3 class="font-bold">Jornada</h3> -->
                <!-- <div class="error-desc"> -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><i class="fa fa-sliders"></i> Descontinuidades</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            <div id="mensagens">
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="tblabel">#</th>
                                    <th class="tblabel">Descricao</th>
                                    <th class="tblabel">Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($descontinuidades as $descondinuidade)
                                        <tr>
                                            <td class="tblabel">{{$descondinuidade['id']}}</td>
                                            <td class="tblabel"><input type="text" class="form-control" id="descricao_descondinuidade{{$descondinuidade['id']}}" value="{{$descondinuidade['descricao']}}"></td>
                                            <td class="tblabel">
                                                <a class="btn btn-primary bt_alterar_descondinuidade" value="{{$descondinuidade['id']}}">Salvar</a> 
                                                <a class="btn btn-danger bt_excluir_descondinuidade" value="{{$descondinuidade['id']}}">Excluir</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfooter>
                                    <tr>
                                        <td class="tblabel">ID</td>
                                        <td><input type="text" class="form-control" placeholder="Descrição" id="descricao"/></td>
                                        <td class="tblabel"><a href="#" class="btn btn-primary" id="bt_salva_descontinuidade">Salvar</a></td>
                                    </tr>
                                </tfooter>
                            </table>
                            <div class="pull-right">
                                
                            </div>
                            </br>
                            
                            <form role="form" class="form-inline">
                            </form>
                        </div>
                    </div>
                <!-- <a href="" class="btn btn-primary m-t">Ensaio - Gamagrafia</a> -->
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
@stop
