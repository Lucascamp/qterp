@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h2>Configurações do modulo Ensaio</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/adm">Admin</a>
            </li>
            <li>
               Ensaio
            </li>
            <li class="active">
                <strong>Normas e Procedimentos</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <!-- <h3 class="font-bold">Jornada</h3> -->
                <!-- <div class="error-desc"> -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><i class="fa fa-sliders"></i> Normas e Procedimentos</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            <div id="mensagens">
                            </div>
                            <table class="table table-bordered tabela" id="editable">
            <thead>
            </thead>
            <tbody>
                <table class="table table-bordered tabela">
                    <tr>
                        <td class="tbtitulo" colspan="8">Aplicação</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Descrição</td>
                        <td class="tblabel"><input type="text" class="mask-codproposta form-control" id="codproposta"></td>
                    </tr>
                </table>
                <table class="table table-bordered tabela">
                    <tr>
                        <td  class="tbtitulo" colspan="8">Normas</td>
                    </tr>
                    <tr>
                        <td class="tblabel">Descrição</td>
                        <td class="tblabel">
                            <Select class="form-control" id="cliente">
                                <option>Selecione o cliente</option> 
                                
                                
                                
                            </select>
                        </td>
                        <td class="tblabel">Nome reduzido</td>
                        <td class="tbitem tblabel" ><p id="nome_reduz"></p></td>
                        <td class="tblabel">Estado</td>
                        <td class="tbitem tblabel"><p id="estado"></p></td>
                        <td class="tblabel">Município</td>
                        <td class="tbitem tblabel"><p id="municipio"></p></td>
                    </tr>
                </table>
                
                
            </tbody>
        </table>
                            <div class="pull-right">
                                
                            </div>
                            </br>
                            
                            <form role="form" class="form-inline">
                            </form>
                        </div>
                    </div>
                <!-- <a href="" class="btn btn-primary m-t">Ensaio - Gamagrafia</a> -->
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
@stop
