@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Configurações do modulo Ensaio</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/adm">Admin</a>
            </li>
            <li>
               Ensaio
            </li>
            <li class="active">
                <strong>Filmes</strong>
            </li>
        </ol>
    </div>
    <!-- <div class="col-sm-8">
        <div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
                <!-- <h3 class="font-bold">Jornada</h3> -->
                <!-- <div class="error-desc"> -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><i class="fa fa-sliders"></i> Filmes</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            <div id="mensagens">
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="tblabel">#</th>
                                    <th class="tblabel">Descricao</th>
                                    <th class="tblabel">Classe</th>
                                    <th class="tblabel">Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($filmes as $filme)
                                        <tr>
                                            <td class="tblabel">{{$filme['id']}}</td>
                                            <td class="tblabel"><input type="text" class="form-control" id="descricao_filme{{$filme['id']}}" value="{{$filme['descricao']}}"></td>
                                            <td class="tblabel">
                                                <select class="form-control" id="classe{{$filme['id']}}">
                                                    @if($filme['classe']=='0')
                                                        <option value="0">Classe I</option>
                                                        <option value="1">Classe II</option>
                                                    @else
                                                        <option value="1">Classe II</option>
                                                        <option value="0">Classe I</option>
                                                    @endif
                                                </select>
                                            </td>
                                            <td class="tblabel">
                                                <a class="btn btn-primary bt_alterar_filme" value="{{$filme['id']}}">Salvar</a> 
                                                <a class="btn btn-danger bt_excluir_filme" value="{{$filme['id']}}">Excluir</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfooter>
                                    <tr>
                                        <td class="tblabel">ID</td>
                                        <td><input type="text" class="form-control" placeholder="Descrição" id="descricao"/></td>
                                        <td>
                                            <select class="form-control" id="classe">
                                                <option>Selecione a Classe</option>
                                                <option value="0">Classe I</option>
                                                <option value="1">Classe II</option>
                                            </select>
                                        </td>
                                        <td class="tblabel"><a href="#" class="btn btn-primary" id="bt_salva_filme">Salvar</a></td>
                                    </tr>
                                </tfooter>
                            </table>
                            <div class="pull-right">
                                
                            </div>
                            </br>
                            
                            <form role="form" class="form-inline">
                            </form>
                        </div>
                    </div>
                <!-- <a href="" class="btn btn-primary m-t">Ensaio - Gamagrafia</a> -->
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
@stop
