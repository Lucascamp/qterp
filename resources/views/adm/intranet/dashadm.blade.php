@extends('layouts.masterintra')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row  border-bottom white-bg dashboard-header">

                    <div class="col-sm-3">
                        <h2>Bem Vindo {{$usuario->nome}}</h2>
                        <small>Você tem 16 mensagem e 8 notificações.</small>
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    12
                                </span>
                                <span class="label label-success">1</span> Solicitações Pendentes
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    359
                                </span>
                                <span class="label label-info">2</span> Marcações Incorretas
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    4
                                </span>
                                <span class="label label-primary">3</span> unidades Pendentes
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    498
                                </span>
                                <span class="label label-default">4</span> Marcações Pendentes
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <small class="text-muted m-b block">Quantidade de Marcações RJ</small>
                        <div class="flot-chart dashboard-chart">
                            <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                        </div>
                        <div class="row text-left">
                            <div class="col-xs-4">
                                <div class=" m-l-md">
                                <span class="h4 font-bold m-t block">R$ 406.100,00</span>
                                <small class="text-muted m-b block">Indicadores diversos</small>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <span class="h4 font-bold m-t block">150,40 %</span>
                                <small class="text-muted m-b block">Indicadores diversos</small>
                            </div>
                            <div class="col-xs-4">
                                <span class="h4 font-bold m-t block">333</span>
                                <small class="text-muted m-b block">Indicadores diversos</small>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="statistic-box">
                        <h4>
                            Marcações Ponto
                        </h4>
                        <p>
                            
                        </p>
                            <div class="row text-center">
                                
                                <div class="col-lg-6">
                                    <canvas id="doughnutChart" width="150" height="78" style="width: 78px; height: 78px;"></canvas>
                                    <h5>RJ</h5>
                                </div>
                            </div>
                            <div class="m-t">
                                <button class="btn btn-primary btn-block m-t"><i class=""></i>Ver todas as marcações</button>
                            </div>

                        </div>
                    </div>

            </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                        <div class="row">
                        <div class="col-lg-4">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Novas circulares Qualitec</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Ver todas</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div>
                                        <div class="pull-right text-right">
                                        </div>
                                        <h4>Circular 0001
                                            <br>
                                            <small class="m-r"><a href="#"> Resumo sobre circular </a> </small>
                                        </h4>
                                        <h4>Circular 0002
                                            <br>
                                            <small class="m-r"><a href="#"> Resumo sobre circular </a> </small>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Aniversariantes do mês</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content no-padding">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <p><a class="text-info">Denilson da Silveira</a> Unidade BA</p>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> 01/Fevereiro </small>
                                        </li>
                                        <li class="list-group-item">
                                            <p><a class="text-info">Jiumar Nolasco</a> Unidade BA</p>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> 08/Fevereiro</small>
                                        </li>
                                        <li class="list-group-item">
                                            <p><a class="text-info">Edson Osório</a> Unidade RN</p>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> 10/Fevereiro</small>
                                        </li>
                                        <li class="list-group-item">
                                            <p><a class="text-info">Jorge Rabelo</a> Unidade BA</p>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> 12/Fevereiro</small>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Informações sobre o Ponto</h5>
                                        <div class="ibox-tools">
                                            <span class="label label-warning-light">2 Lembretes</span>
                                           </div>
                                    </div>
                                    <div class="ibox-content">

                                        <div>
                                            <div class="feed-activity-list">

                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-left">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-right">dia 20/março</small>
                                                        Abrir novo <strong>Periodo</strong><br>
                                                        <small class="text-muted">Ultimo periodo - Fevereiro 21.02.2015</small>

                                                    </div>
                                                </div>

                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-left">
                                                    </a>
                                                    <div class="media-body well alert-danger">
                                                        <small class="pull-right">dia limite 28/fevereiro</small>
                                                        <strong> Validação do Ponto </strong><br>
                                                        <small class="text-muted">Ultimo periodo validado - janeiro 21.01.2015</small>

                                                    </div>
                                                </div>
                                            </div>

                                            <button class="btn btn-primary btn-block m-t"><i class=""></i>Ir para o modulo Ponto</button>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        <div class="col-lg-4">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Solicitações de Equipamento</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content ibox-heading">
                                    <h3>Lembrete sobre equipamento</h3>
                                    <small><i class="fa fa-map-marker"></i> Unidade RJ </small>
                                </div>
                                <div class="ibox-content inspinia-timeline">

                                    <div class="timeline-item">
                                        <div class="row">
                                            <div class="col-xs-3 date">
                                                <i class="fa fa-briefcase"></i>
                                                10:00 am
                                                <br>
                                                <small class="text-navy">3 horas atras</small>
                                            </div>
                                            <div class="col-xs-7 content no-top-border">
                                                <p class="m-b-xs"><strong>Unidade BA</strong></p>

                                                <p>Descrição sobre a solicitaçãod e equipamentos</p>

                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="timeline-item">
                                        <div class="row">
                                            <div class="col-xs-3 date">
                                                <i class="fa fa-file-text"></i>
                                                12:00 pm
                                                <br>
                                                <small class="text-navy">1 hora atras</small>
                                            </div>
                                            <div class="col-xs-7 content">
                                                <p class="m-b-xs"><strong>Unidade RJ</strong></p>
                                                <p>Solicitação de impressoras</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="timeline-item">
                                        <div class="row">
                                            <div class="col-xs-3 date">
                                                <i class="fa fa-coffee"></i>
                                                25-02-2015 - 8:00 am
                                                <br>
                                            </div>
                                            <div class="col-xs-7 content">
                                                <p class="m-b-xs"><strong>Unidade RN</strong></p>
                                                <p>
                                                    Solicitação de maquinas para inicio de trabalho
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </div>
                </div>
                <div class="footer">
                    <div class="pull-right">
                    </div>
                    <div>
                        <strong>Copyright</strong> Qualitec© 2015
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
<script src="/js/admin/intranet.js"></script>
<script type="text/javascript">
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.success('Notificação de teste', 'Bem vindo ao QTERP');
    }, 1300);
</script>

@stop
