@extends('layouts.master')

@section('menu')
@include('adm.menuadm')
@stop

@section('main')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Configuração do Sistema</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Admin</strong>
            </li>
        </ol>
    </div>
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="text-center animated fadeInRightBig">
                <div class="ibox float-e-margins">
                    <h3 class="font-bold">Admin</h3>
                    <div class="error-desc">
                        Área para cadastro e configuração do sistema </br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
        <div class="col-lg-2">
            <div class="ibox">
                <div class="ibox-content">
                <h5 class="m-b-md">Server MG status</h5>
                <h2 class="text-navy" id="serverMG">
                </h2>
                <small ></small>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="ibox">
                <div class="ibox-content">
                <h5 class="m-b-md">Server BA status</h5>
                <h2 class="text-navy" id="serverBA">
                </h2>
                <small></small>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="ibox">
                <div class="ibox-content">
                <h5 class="m-b-md">Server RN status</h5>
                <h2 class="text-navy" id="serverRN">
                </h2>
                <small></small>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="ibox">
                <div class="ibox-content">
                <h5 class="m-b-md">Server MC status</h5>
                <h2 class="text-navy" id="serverMC">
                </h2>
                <small></small>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="ibox">
                <div class="ibox-content">
                <h5 class="m-b-md">Server PE status</h5>
                <h2 class="text-navy" id="serverPE">
                </h2>
                <small></small>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-content">
                    <h5>Ultimos BACKUPs</h5>
                    <table class="table table-stripped small m-t-md">
                        <tbody>
                            <tr>
                                <td class="no-borders">
                                    <i class="fa fa-circle" id="bu_c_MG"></i>
                                </td>
                                <td class="no-borders">
                                   <span id="bu_MG">MG</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-circle" id="bu_c_BA"></i>
                                </td>
                                <td>
                                    <span id="bu_BA">BA</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-circle" id="bu_c_RN"></i>
                                </td>
                                <td>
                                    <span id="bu_RN">RN</span>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <i class="fa fa-circle" id="bu_c_MC"></i>
                                </td>
                                <td>
                                    <span id="bu_MC">MC</span>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <i class="fa fa-circle" id="bu_c_PE"></i>
                                </td>
                                <td>
                                   <span id="bu_PE">PE</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ultimas Sincronizaçoes</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-refresh"></i></a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="/adm/sincronizarusuarios">Sincronizar Usuarios</a></li>
                            <li><a href="/adm/sincronizarmodulos">Sincronizar Modulos</a></li>
                            <li><a href="/adm/sincronizarunidades">Sincronizar Unidades</a></li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="pull-right text-right">
                    </div>
                    @foreach ($logs as $log)
                        <h4>{{$log->unidade}}<small class="m-r"> ->{{$log->data}} -> {{substr($log->log, 0,50)}} </small> </h4>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-content">
                    <h5>Erros</h5>
                    <table class="table table-stripped small m-t-md">
                        <tbody id="tbodyerros">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-content">
                    <h5>Tempo criar dump</h5>
                    <div id="sparkline1"><canvas width="207" height="60" style="display: inline-block; width: 207px; height: 60px; vertical-align: top;"></canvas></div>
                </div>
            </div>
        </div>
         <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-content">
                    <h5>Tempo backup</h5>
                    <div id="sparkline2"><canvas width="207" height="60" style="display: inline-block; width: 207px; height: 60px; vertical-align: top;"></canvas></div>
                </div>
            </div>
        </div>
         <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-content">
                    <h5>Tempo Sinc Gamagrafia</h5>
                    <div id="sparkline3"><canvas width="207" height="60" style="display: inline-block; width: 207px; height: 60px; vertical-align: top;"></canvas></div>
                </div>
            </div>
        </div>
         <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-content">
                    <h5>Tempo Sinc dados</h5>
                    <div id="sparkline4"><canvas width="207" height="60" style="display: inline-block; width: 207px; height: 60px; vertical-align: top;"></canvas></div>
                </div>
            </div>
        </div>
    </div>
</div>


    
</div>

<script src="/js/jquery2.1.js"></script>
<script src="/js/jquery.sparkline.min.js"></script>
<script src="/js/admin/monitor.js"></script>

@stop
