﻿@extends('layouts.master')

@section('menu')
<?php use Carbon\Carbon; ?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{$usuario->nome}}</strong>
                            </span> 
                            <!-- <span class="text-muted text-xs block"><b class="caret"></b></span>  -->
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        
                        <li><a href="/adm/trocarsenha">Trocar Senha</a></li>
                        <li class="divider"></li>
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    QT-ERP
                </div>
            </li>
            <li>
                <input type="hidden" id="token" value="{{ csrf_token() }}">
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Modulos</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @foreach ($modulos as $element)
                        @if($element['status']==1 )
                            <li><a href="{{$element['rota']}}">{{$element['descricao']}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
        </ul>

    </div>

</nav>

@stop

@section('main')

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="text-center animated fadeInRightBig">
                <h3 class="font-bold">Módulos Disponíveis</h3>
                <div class="error-desc">
                    @foreach ($modulos as $element)
                        @if($element['status']==1)
                            <a href="{{$element['rota']}}" class="btn btn-primary m-t">{{$element['descricao']}}</a>
                        @endif    
                    @endforeach
                </div>
            </div>
            <br>
            <br>
            <div class="row  border-bottom gray-bg dashboard-header">
                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-content">
                        @if($usuario->id == 1 or $usuario->id == 10)
                        <center>
                        {!! Form::open(array('route' => 'arquivo.ferias', 'files' => true)) !!}
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            {!! Form::file('arquivo_ferias', ['id'=>'arquivo_ferias']) !!}
                            <br>
                            {!! Form::submit('Guardar arquivo', array('class' => 'btn btn-primary')) !!}
                        {!! Form::close() !!}
                        @endif
                        <a class="btn btn-success" href="http://10.155.64.9/arquivos/arquivo_ferias.xlsx">Arquivo de férias</a>
                        </center>
                        </div>

                    </div>
                </div>

                @include('widgets.widget_sgi')







            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        @if (Config::get('app.atualizavel'))
            
            <div class="text-center animated fadeInRightBig">
                <h3 class="font-bold">Sincronizações</h3>
                <a href="#" class="btn btn-primary m-t" id="bt_sinc_dados">Sincronizar Dados</a>
            </div>
        @endif
    </div>
</div>


                    
@stop
