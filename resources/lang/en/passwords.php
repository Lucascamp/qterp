<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "A senha deve ter pelo menos seis caracteres e ser igual ao da confirmação.",
	"user" => "E-mail inexistente.",
	"token" => "This password reset token is invalid.",
	"sent" => "We have e-mailed your password reset link!",
	"reset" => "Your password has been reset!",

];
