<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Filme extends Model {

		/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'filmes';

}
