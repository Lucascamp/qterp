<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SoftDeletes;

class AlmoxarifadoHistorico extends Model
{
    protected $dates = ['deleted_at'];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'almoxarifado_historico';

	protected $fillable = array('unidade_id','item_id','data','total','quantidade','operacao','created_by','updated_by');

	public function getDates()
	{
		return array(
			'created_at', 
			'updated_at', 
			'data',
			);
	}

	/**
	 * Define the relationship with calibracao	 
	 */
	public function unidade()
	{
		return $this->belongsTo('App\Unidade', 'unidade_id', 'id');
	}

	/**
	 * Define the relationship with calibracao	 
	 */
	public function item()
	{
		return $this->belongsTo('App\ItemAlmoxarifado', 'item_id', 'id');
	}
	
}
