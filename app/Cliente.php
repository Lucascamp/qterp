<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'clientes';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	public function guia()
	{
		return $this->hasMany('App\Guia', 'cliente_id');
	}
}
