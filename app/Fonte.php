<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Fonte extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'fontes';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	protected $fillable = ['created_by','numero','irradiador','cabo','foco','modelo','gabarito','isotopo','atividade_inicial','data_inicial',
  		'meia_vida','limite_uso','limite_troca','cod_vistoria','data_vistoria','pasta','status','observacoes'];

	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data_inicial',
			'data_vistoria',
			'data_limite_uso',
			'data_limite_troca'
			);
	}

	public function guia()
	{
		return $this->hasMany('App\Guia', 'fonte_id');
	}


	public function setDataInicialAttribute($value)
	{
		if(isset($value))
			$this->attributes['data_inicial']  = Carbon::createFromFormat('Y-m-d', $value);
		else 
			return null;
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataVistoriaAttribute($value)
	{
		if(isset($value))
			$this->attributes['data_vistoria']  = Carbon::createFromFormat('Y-m-d', $value);
		else 
			return null;
	}

	public function setDataLimiteUsoAttribute($value)
	{
		$this->attributes['data_limite_uso']  = Carbon::createFromFormat('Y-m-d H:i:s', $value);
	}

	public function setDataLimiteTrocaAttribute($value)
	{
		$this->attributes['data_limite_troca']  = Carbon::createFromFormat('Y-m-d H:i:s', $value);
	}

	/**
	 * return a day worked time in minutes
	 * @return integer minutes worked
	 */
	public function getLimite($limite, $atividade_inicial, $meia_vida, $data_inicial)
	{		
		$i=1;

		$carga = $atividade_inicial;

		while($carga > $limite)
		{
			$carga = number_format($atividade_inicial/pow(2,($i)/$meia_vida) ,2);

			$i++;
		}
		
		$datainicio = Carbon::createFromFormat('Y-m-d', $data_inicial);

		$data = $datainicio->addDays($i-1);

		return $data;
	}

	function sort($a,$b)
	{
		if ($a==$b) return 0;
		return ($a<$b)?-1:1;
	}
}
