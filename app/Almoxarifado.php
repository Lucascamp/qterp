<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SoftDeletes;

class Almoxarifado extends Model
{
    protected $dates = ['deleted_at'];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'almoxarifado';

	protected $fillable = array('unidade_id','item_id','total','created_by','updated_by');

	public function getDates()
	{
		return array(
			'created_at', 
			'updated_at', 
			);
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setCreatedAtCompraAttribute($value)
	{
		$this->attributes['created_at']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	/**
	 * Define the relationship with calibracao	 
	 */
	public function unidade()
	{
		return $this->belongsTo('App\Unidade', 'unidade_id', 'id');
	}

	/**
	 * Define the relationship with calibracao	 
	 */
	public function item()
	{
		return $this->belongsTo('App\ItemAlmoxarifado', 'item_id', 'id');
	}
	
}
