<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SoftDeletes;

class Equipamento extends Model
{
    protected $dates = ['deleted_at'];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'equipamentos';

	protected $fillable = array('descricao', 'aplicacao', 'marca', 'modelo', 'serial', 'patrimonio', 'descricao_uso',
								'data_compra', 'nota_fiscal', 'valor_compra', 'fornecedor', 'taxa_depreciacao', 'calibravel', 'frequencia',
								'proprietario','regime','filepath','created_by', 'updated_by');

	public function getDates()
	{
		return array(
			'created_at', 
			'updated_at', 
			'data_compra'
			);
	}

	/**
	 * Define the relationship with calibracao	 
	 */
	public function calibracao()
	{
		return $this->hasMany('App\Calibracao', 'equipamento_id');
	}

	/**
	 * Define the relationship with localizacao	 
	 */
	public function localizacao()
	{
		return $this->hasMany('App\Localizacao', 'equipamento_id');
	}


	/*== Convert  Dates to Save on DB ==*/
	public function setDataCompraAttribute($value)
	{
		$this->attributes['data_compra']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	public function filterLocalizacao($local)
	{
        $equipamentos = DB::table('equipamento_localizacao')->where('centrocusto_id', $local)
        													   ->whereNull('deleted_at')
        													   ->lists('equipamento_id');

        foreach ($equipamentos as $equipamento) 
        {
        	$localizacao_atual[$equipamento] = DB::table('equipamento_localizacao')
        												->where('equipamento_id', $equipamento)
        	 											->orderBy('data_movimentacao', 'DESC')
        	 											->first();

        	if($localizacao_atual[$equipamento]->centrocusto_id != $local)
        		unset($localizacao_atual[$equipamento]);
        }

        $equipamentos = array_keys($localizacao_atual);

        return $equipamentos;
    }
}
