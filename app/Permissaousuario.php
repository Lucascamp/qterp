<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class Permissaousuario extends Model {
	static function Modulo($id_user){
		$Modulos = DB::table('modulos')
                            ->leftJoin('areasmodulos', 'modulos.id', '=', 'areasmodulos.modulo_id')
                            ->leftJoin('permissoes', 'permissoes.areamodulo_id', '=', 'areasmodulos.id')
                            ->leftJoin('permissaousuarios', 'permissoes.id', '=', 'permissaousuarios.permissao_id')
                            ->select(
                            	'modulos.*' 
                            )
                            ->where('permissaousuarios.usuario_id','=',$id_user)
                            ->groupby('descricao')
                            ->get();
        $modulos = array();
        foreach ($Modulos as $modulo) {
        	$modulos[] = get_object_vars($modulo);
        }
        return $modulos;
	}


	static function Area($id_user,$id_modulo){
		$Areas = DB::table('areasmodulos')
                            ->leftJoin('permissoes', 'permissoes.areamodulo_id', '=', 'areasmodulos.id')
                            ->leftJoin('permissaousuarios', 'permissoes.id', '=', 'permissaousuarios.permissao_id')
                            ->select(
                            	'areasmodulos.*' 
                            )
                            ->where('permissaousuarios.usuario_id','=',$id_user)
                            ->where('areasmodulos.modulo_id','=',$id_modulo)
                            ->groupby('descricao')
                            ->get();
        $areas = array();
        foreach ($Areas as $area) {
        	$areas[] = get_object_vars($area);
        }
        return $areas;
	}

	static function Permissao($id_user,$id_modulo){
		$Permissoes = DB::table('areasmodulos')
                            ->leftJoin('permissoes', 'permissoes.areamodulo_id', '=', 'areasmodulos.id')
                            ->leftJoin('permissaousuarios', 'permissoes.id', '=', 'permissaousuarios.permissao_id')
                            ->select(
                            	'permissoes.*' 
                            )
                            ->where('permissaousuarios.usuario_id','=',$id_user)
                            ->where('areasmodulos.modulo_id','=',$id_modulo)
                            ->get();
        $permissoes = array();
        foreach ($Permissoes as $permissao) {
        	$permissoes[] = get_object_vars($permissao);
        }
        return $permissoes;
	}
}
