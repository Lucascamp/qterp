<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoftDeletes;

class Segurocarro extends Model
{
	protected $dates = ['deleted_at'];
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'carro_seguro';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = ['carro_id','seguradora','corretor','apolice','valor_seguro','data_inicial','data_final',
							'vistoria','created_by','updated_by'];

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data_inicial',
			'data_final'
			);
	}
	
	/**
	 * Define the relationship with Funcionario	 
	 */
	public function carro()
	{
		return $this->belongsTo('Carro', 'id');
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataInicialAttribute($value)
	{
		$this->attributes['data_inicial']  = Carbon::createFromFormat('Y-m-d', $value);
	}
	public function setDataFinalAttribute($value)
	{
		$this->attributes['data_final']  = Carbon::createFromFormat('Y-m-d', $value);
	}
}