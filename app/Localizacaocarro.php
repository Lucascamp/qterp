<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoftDeletes;

class Localizacaocarro extends Model
{
	protected $dates = ['deleted_at'];
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'carro_localizacao';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = ['data','carro_id','unidade','centrocusto_id','created_by','updated_by'];


	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data'
			);
	}

	/**
	 * Define the relationship with item contabil	 
	 */
	public function centrocusto()
	{
		return $this->belongsTo('centrocusto', 'id_centrocusto');
	}

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function carro()
	{
		return $this->belongsTo('Carro', 'id');
	}


	/*== Convert  Dates to Save on DB ==*/
	public function setDataAttribute($value)
	{
		$this->attributes['data']  = Carbon::createFromFormat('Y-m-d', $value);
	}
	
}