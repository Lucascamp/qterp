<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoftDeletes;

class Ecofrota extends Model
{
	protected $dates = ['deleted_at'];
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'carro_ecofrota';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = ['carro_id','numero','recarga','tipo','created_by','updated_by'];

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			);
	}

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function carro()
	{
		return $this->belongsTo('Carro', 'id');
	}

	// /*== Convert  Dates to Save on DB ==*/
	// public function setDataPagamentoImpostoAttribute($value)
	// {
	// 	$this->attributes['data_pagamento_imposto']  = Carbon::createFromFormat('Y-m-d', $value);
	// }
}