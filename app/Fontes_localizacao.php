<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fontes_localizacao extends Model {

	protected $dates = ['deleted_at'];

	protected $softDelete = true;

	protected $table = 'fontes_localizacao';

}
