<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Guia extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'guias';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		'data' 			   		=> 'required|date_format:"d/m/Y"',							
		'unidade' 		   		=> 'required',
		'proposta_id' 			=> 'required',
		'cliente_id' 		   	=> 'required|numeric',
		'equipe_id' 		   	=> 'required',
		'fonte_id'    			=> 'required',
		'fonte_atividade'    	=> 'required',
		'created_by' 			=> 'required',
		'updated_by' 			=> 'required'
	);

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = array('unidade', 'data', 'proposta_id', 'cliente_id', 'fonte_id',
								'fonte_atividade', 'created_by', 'updated_by','programacao_cliente',
								'local_ensaio','data_prog','contratante_fabricante','projeto');

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data',
			'entrada',
			'saida',
			);
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataAttribute($value)
	{
		$this->attributes['data']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setEntradaAttribute($value)
	{
		$this->attributes['entrada']  = Carbon::createFromFormat('d-m-Y H:i', $value);
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setSaidaAttribute($value)
	{
		$this->attributes['saida']  = Carbon::createFromFormat('d-m-Y H:i', $value);
	}

	/**
	 * Define the relationship with clientes	 
	 */
	public function cliente()
	{
		return $this->belongsTo('App\Cliente', 'cliente_id', 'id');
	}

	public function funcionarios()
   	{
      	return $this->belongsToMany('App\Funcionario', 'funcionario_guia');
   	}

   	public function proposta()
   	{
      	return $this->belongsTo('App\Proposta', 'proposta_id');
   	}

   	public function fonte()
   	{
      	return $this->belongsTo('App\Fonte', 'fonte_id');
   	}

   	public function programacoes()
   	{
      	return $this->belongsToMany('App\Programacao', 'guia_id');
   	}
}