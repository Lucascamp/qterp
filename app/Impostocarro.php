<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoftDeletes;

class Impostocarro extends Model
{
	protected $dates = ['deleted_at'];
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'carro_imposto';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = ['carro_id','tipo_imposto','valor_imposto','data_pagamento_imposto','restricao',
							'tipo_restricao','created_by','updated_by'];

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data_pagamento_imposto'
			);
	}

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function carro()
	{
		return $this->belongsTo('Carro', 'id');
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataPagamentoImpostoAttribute($value)
	{
		$this->attributes['data_pagamento_imposto']  = Carbon::createFromFormat('Y-m-d', $value);
	}
	
}