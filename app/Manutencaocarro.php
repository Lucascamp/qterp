<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoftDeletes;

class Manutencaocarro extends Model
{
	protected $dates = ['deleted_at'];
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'carro_manutencao';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = ['carro_id','data_manutencao','descricao_manutencao','valor_manutencao','created_by',
						   'updated_by'];

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data_manutencao'
			);
	}

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function carro()
	{
		return $this->belongsTo('Carro', 'id');
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataManutecaoAttribute($value)
	{
		$this->attributes['data_manutencao']  = Carbon::createFromFormat('Y-m-d', $value);
	}
}