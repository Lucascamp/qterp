<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Programacao extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'programacoes';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = array('guia_id', 'unidade', 'item', 'identificacao', 'spool', 'junta','obra', 'norma',
								'posicao_soldagem', 'diametro', 'espessura', 'reforco',
								'metal_base', 'metal_adicao', 'sinete_raiz', 'processo_raiz',
								'sinete_acabamento', 'processo_acabamento', 'nivel_inspencao',
								'procedimento', 'tecnica', 'tempo_exposicao', 'dff', 'tipo_iqi', 'fio_iqi',
								'chanfro', 'filme_dimensao', 'filme_classe', 'observacao',
								'created_by', 'updated_by','isometrico','quantidade_filmes','aplicacao','cod_inspecao',
								'descontinuidade','quantidade_aprovado','quantidade_reprovado','nivel_inspecao',
								'tipo_exposicao','resultado','criterio_aceitacao','tipo','numero_filme','tamanho_descontinuidade');

}