<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\Teste_cron',
		'App\Console\Commands\sincfontes',
		'App\Console\Commands\sincdados',
		'App\Console\Commands\sincgama',
		'App\Console\Commands\criadumpgama',
		'App\Console\Commands\backup',
		'App\Console\Commands\sincequipamento',
		'App\Console\Commands\criadumpalmoxarifado',
		'App\Console\Commands\sincalmoxarifado',
		'App\Console\Commands\criadumpequipamento',
		'App\Console\Commands\sinclocequipamento',
		'App\Console\Commands\sincitemalmoxarifado',
		'App\Console\Commands\sinccalibracao',
		'App\Console\Commands\sincfrota',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();
	}

}

