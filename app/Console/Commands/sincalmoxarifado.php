<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sincalmoxarifado extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sincalmoxarifado';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sincronizacao do modulo de almoxarifado';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$sec = explode(" ",microtime());
		$tempo_ini_t= $sec[1] + $sec[0];

		\DB::connection('LOG')->table('logs')
							  ->insert(array('log'=>'SINC ALMOXARFADO INICIADO','unidade'=>'CENTRAL'));

		

		$dumps = \DB::connection('LOG')->table('dumps')
									   ->where('status',0)
									   ->where('tipo','ALMOXARIFADO')
									   ->where('dump_status','OK')
									   ->get();

		$truncate = 'truncate almoxarifado;
					 truncate almoxarifado_historico;';
		
		foreach ($dumps as $key => $dump) 
		{

			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'iniciando sinc na unidade '.$dump->unidade,'unidade'=>$dump->unidade));
			$stringDump = $dump->dump;

			$this->info('inicio');
			\DB::connection('ESPELHO')->getPdo()->exec( $truncate );
			\DB::connection('ESPELHO')->getPdo()->exec( $stringDump );
			\DB::connection('ESPELHO')->getPdo()->exec( 'UNLOCK TABLES' );

			$id_unidade = \DB::connection('CENTRAL')->table('unidade')->where('unidade', $dump->unidade)->pluck('id');

			$data_sinc = \DB::connection('LOG')->table('dumps')
											   ->select(\DB::raw('MAX(data) as data'))
											   ->where('status',1)
											   ->where('tipo','ALMOXARIFADO')
											   ->where('unidade', $dump->unidade)
											   ->get();

			if($data_sinc[0]->data){
				$data_sinc = substr($data_sinc[0]->data,0,10);
			}else{
				$data_sinc = false;
			}
			

			$this->info('dump');
			$this->info('data atualizacao: '.$data_sinc);

			$this->almoxarifado($id_unidade,$data_sinc);
			$this->historico_almoxarifado($id_unidade,$data_sinc);

			ini_set('memory_limit', '256M');
			
			\DB::connection('LOG')->table('dumps')->where('id',$dump->id)->update(array('status'=>1));
			//descomentar depois do teste

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc na unidade '.$dump->unidade.' finalizado','tempo_total'=>$tempo,'unidade'=>$dump->unidade));
		}
		if(count($dumps) > 0){
			$this->info('atualizando unidades');

			$backupfile = public_path().'/dumps/'.'dump_AL'.str_random(6).'_'.date("Ymd").'_CENTRAL.sql';					  
			ini_set('memory_limit', '256M');
			system("mysqldump -h 10.155.64.134 -uroot -pusbw --lock-tables qterp almoxarifado almoxarifado_historico > $backupfile");
			$truncate = 'truncate almoxarifado; truncate almoxarifado_historico;';
			$dump = file_get_contents($backupfile);

			$unidades = \Config::get('app.unidades');

			foreach ($dumps as $key => $dumpuni) {
				if(in_array($dumpuni->unidade, $unidades)){
					$this->info('Iniciando: '.$dumpuni->unidade);
					try{
						\DB::connection($dumpuni->unidade)->getPdo()->exec( $truncate );
					 	\DB::connection($dumpuni->unidade)->getPdo()->exec( $dump );
					 	\DB::connection($dumpuni->unidade)->getPdo()->exec( 'UNLOCK TABLES' );
					 	$this->info('Finalizando: '.$dumpuni->unidade);
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$dumpuni->unidade);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$dumpuni->unidade));
						continue;
					}
				}
			}
		}
		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini_t);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'SINC ALMOXARIFADO FINALIZADO','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
	}

	public function almoxarifado($unidade,$data)
	{
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		$atualizados = 0;
		$inseridos = 0;
		$this->info('almoxarifado: '.$unidade);
		if($data){
			$espelhoalmoxarifado = \DB::connection('ESPELHO')->table('almoxarifado')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$espelhoalmoxarifado = \DB::connection('ESPELHO')->table('almoxarifado')->get();
		}
		
		$centralalmoxarifado = \DB::connection('CENTRAL')->table('almoxarifado')->lists('item_id');
		var_dump(count($espelhoalmoxarifado));

		if(!empty($centralalmoxarifado))
		{
			$this->info('almoxarifado');
			foreach ($espelhoalmoxarifado as $key => $almoxarifado)
			{
				$existe = \DB::connection('CENTRAL')->table('almoxarifado')
														 ->where('item_id',$almoxarifado->item_id)	
														 ->where('unidade_id',$almoxarifado->unidade_id)
														 ->first();

				if(!empty($existe)){								 
					// if(in_array($almoxarifado->item_id, $centralalmoxarifado))
					// {
						if($almoxarifado->updated_at > $existe->updated_at)
						{
							$insert = (array) $almoxarifado;
						
							unset($insert['id']);
							\DB::connection('CENTRAL')->table('almoxarifado')->where('item_id',$almoxarifado->item_id)
																		 ->where('unidade_id',$almoxarifado->unidade_id)
																		 ->update($insert);
							$atualizados+=1;												 
						}												 
					// }
				}
				else{	
					$insert = (array) $almoxarifado;
					unset($insert['id']);
					\DB::connection('CENTRAL')->table('almoxarifado')->insert($insert);
					$inseridos+=1;
				}					
			}
		}
		else
		{
			foreach ($espelhoalmoxarifado as $key => $almoxarifado) 
			{
				$existe = \DB::connection('CENTRAL')->table('almoxarifado')
													->where('item_id',$almoxarifado->item_id)	
													->where('unidade_id',$almoxarifado->unidade_id)
													->first();

				if(empty($existe))
				{										 
					$insert = (array) $almoxarifado;
					unset($insert['id']);
					\DB::connection('CENTRAL')->table('almoxarifado')->insert($insert);
					$inseridos+=1;
				}
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		$this->info('almoxarifado '.$tempo);
		$this->info('inseridos '.$inseridos);
		$this->info('atualizados '.$atualizados);
	}

	public function historico_almoxarifado($unidade,$data)
	{
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		$this->info('almoxarifado inicio '.$unidade);
		$atualizados = 0;
		$inseridos = 0;
		
		if($data){
			$espelhoalmoxarifado = \DB::connection('ESPELHO')->table('almoxarifado_historico')->where('created_at','>',$data)->where('updated_at','>',$data)->get();
		}else{
			$espelhoalmoxarifado = \DB::connection('ESPELHO')->table('almoxarifado_historico')->get();
		}

		$this->info('espeloalmoxarifado: '.count($espelhoalmoxarifado));
		
		$centralalmoxarifado = \DB::connection('CENTRAL')->table('almoxarifado_historico')->lists('id');
		
		$this->info('centralalmoxarifado: '.count($centralalmoxarifado));

		if(!empty($centralalmoxarifado)){
			foreach ($espelhoalmoxarifado as $key => $alm) {
				$existe = \DB::connection('CENTRAL')->table('almoxarifado_historico')
														 ->where('item_id',$alm->item_id)	
														 ->where('unidade_id',$alm->unidade_id)
														 ->where('total',$alm->total)
														 ->where('quantidade',$alm->quantidade)
														 ->where('operacao',$alm->operacao)
														 ->where('created_at',$alm->created_at)
														 ->where('updated_at',$alm->updated_at)
														 ->first();
				if($existe){

				}else{
					$insert = (array) $alm;
					$this->info('inserindo id '. $insert['id']);
					unset($insert['id']);
					\DB::connection('CENTRAL')->table('almoxarifado_historico')->insert($insert);
					$inseridos+=1;
				}
			}
		}else{
			foreach ($espelhoalmoxarifado as $key => $alm) {
				$insert = (array) $alm;
				$this->info('id '. $insert['id']);
				unset($insert['id']);
				\DB::connection('CENTRAL')->table('almoxarifado_historico')->insert($insert);
				$inseridos+=1;
			}
		}
		

		// if(!empty($centralalmoxarifado))
		// {
		// 	foreach ($espelhoalmoxarifado as $key => $almoxarifado) 
		// 	{
		// 		$existe = \DB::connection('CENTRAL')->table('almoxarifado_historico')
		// 												 ->where('item_id',$almoxarifado->item_id)	
		// 												 ->where('unidade_id',$almoxarifado->unidade_id)
		// 												 ->first();
		// 		if(!empty($existe)){
		// 		// if(in_array($almoxarifado->item_id, $centralalmoxarifado))
		// 		// {
		// 			if($almoxarifado->updated_at > $existe->updated_at)
		// 			{
		// 			$insert = (array) $almoxarifado;
		// 			unset($insert['id']);
		// 			\DB::connection('CENTRAL')->table('almoxarifado_historico')->where('item_id',$almoxarifado->item_id)
		// 															 ->where('unidade_id',$almoxarifado->unidade_id)
		// 															 ->update($insert);
		// 			$atualizados+=1;												 
		// 			}												 
		// 		}
		// 		else{	
		// 			$insert = (array) $almoxarifado;
		// 			unset($insert['id']);
		// 			\DB::connection('CENTRAL')->table('almoxarifado_historico')->insert($insert);
		// 			$inseridos+=1;
		// 		}
		// 	}
		// }
		// else
		// {
		// 	foreach ($espelhoalmoxarifado as $key => $almoxarifado) 
		// 	{
		// 		$existe = \DB::connection('CENTRAL')->table('almoxarifado_historico')
		// 											->where('item_id',$almoxarifado->item_id)	
		// 											->where('unidade_id',$almoxarifado->unidade_id)
		// 											->first();
		// 		if(empty($existe))
		// 		{										 
		// 			$insert = (array) $almoxarifado;
		// 			unset($insert['id']);
		// 			\DB::connection('CENTRAL')->table('almoxarifado_historico')->insert($insert);
		// 			$inseridos+=1;
		// 		}
		// 	}
		// }

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		$this->info('almoxarifado '.$tempo);
		$this->info('inseridos '.$inseridos);
		$this->info('atualizados '.$atualizados);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
