<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sincequipamento extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sincequipamento';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sincroniza tabela de equipamentos';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		$this->AtualizaEquipamentosCentral();
		$this->AtualizaEquipamentos();
		$this->AtualizaEquipamentos();

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sincronizacao de Equipamentos concluida','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
	}

	public function getUnidades(){
		return \Config::get('app.unidades');
	}

	public function AtualizaEquipamentosCentral(){	
		$this->info('Inicio metodo AtualizaEquipamentosCentral');	
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			$EquipMG = \DB::connection('MG')->table('equipamentos')->get();
		}catch(\Exception $e) {
			$this->info('erro con unidade: MG');
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
		}
		\DB::table('equipamentos')->truncate();
		foreach ($EquipMG as $key => $value) {
			$insert = (array) $value;				
			$this->info('equip '.$insert['id'] );
			\DB::table('equipamentos')->insert($insert);
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'equipamentos Central Atualizados','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
		$this->info('FIM ATUALIZACAO EQUIPAMENTOS CENTRAL');
	}

	public function AtualizaEquipamentos(){
		$this->info('Inicio metodo AtualizaEquipamentos');	
		$unidades = $this->getUnidades();
		$msg = 'Equipamentos atualizados com sucesso';
		foreach ($unidades as $key => $un) {
			$this->info('inicio equip'.$un);	
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];
			
			// $data_sinc = \DB::connection('LOG')->table('logs')
			// 	->select(\DB::raw('MAX(data) as data'))
			// 	->where('log','Equipamentos atualizados com sucesso')
			// 	->where('unidade',$un)
			// 	->get();
				
			// if($data_sinc[0]->data){
			// 	$data_sinc = substr($data_sinc[0]->data,0,10);
			// }else{
			// 	$data_sinc = false;
			// }

			// if($data_sinc){
			// 	$EquipLocal = \DB::table('equipamentos')->where('created_at','>',$data_sinc)->orWhere('updated_at','>',$data_sinc)->get();
			// }else{
				$EquipLocal = \DB::table('equipamentos')->get();
			// }

			// $this->info('data ultima atu '.$data_sinc);	
			$this->info('total de equips '.count($EquipLocal));	
			try{
				// $equipamentos = \DB::connection($un)->table('equipamentos')->lists('id');
				// $equipamentos    = \DB::connection($un)->table('equipamentos')->get();
				$equipamentos_id = \DB::connection($un)->table('equipamentos')->lists('updated_at','id');
			}catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
			foreach ($EquipLocal as $key => $equip) {
				$this->info('equip '.$equip->id);	
				// if(in_array($equip->id, $equipamentos)){
				if(!isset($equipamentos_id[$equip->id])){
					$this->info('inserindo na unidade: '.$un);
					$insert = (array) $equip;
					try{
						\DB::connection($un)->table('equipamentos')->insert($insert);
						$msg = 'Equipamentos atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				} else if($equip->updated_at > $equipamentos_id[$equip->id]){
					$this->info('passando para unidade: '.$un);
					$insert = (array) $equip;
					try{
						\DB::connection($un)->table('equipamentos')->where('id',$equip->id)->update($insert);
						$msg = 'Equipamentos atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else if($equip->updated_at <= $equipamentos_id[$equip->id]){
					$this->info('pegando da unidade: '.$un);
					try{
						$equipa = \DB::connection($un)->table('equipamentos')->where('id',$equip->id)->first();
						$insert = (array) $equipa;
						\DB::table('equipamentos')->where('id',$equip->id)->update($insert);
						$msg = 'Equipamentos atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}		
				}					
			}			
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
		}	
	}


































	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
