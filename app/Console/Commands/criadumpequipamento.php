<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class criadumpequipamento extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'criadumpequipamento';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Cria os dumps das tabelas de equipamentos nas unidades';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->criardump();		
	}

	public function getUnidades(){
		return \Config::get('app.unidades');
	}
	public function getConfig($un){
		$configMG = ['host' => '10.155.64.9','user' => 'root', 'pass' => 'root'];
		$configBA = ['host' => '10.155.65.5','user' => 'root', 'pass' => 'usbw'];
		$configRN = ['host' => '10.155.65.72','user' => 'root', 'pass' => 'usbw'];
		$configMC = ['host' => '10.166.3.4','user' => 'root', 'pass' => 'usbw'];
		$configPE = ['host' => '10.155.65.132','user' => 'root', 'pass' => 'usbw'];
		$configRJ = ['host' => '10.155.74.2','user' => 'root', 'pass' => 'usbw'];
		$configs  = ['MG' => $configMG , 'BA' => $configBA , 'RN' => $configRN , 'MC' => $configMC , 'PE' => $configPE, 'RJ' => $configRJ];
		$config = $configs[$un];
		return $config;
	}
	
	public function criardump(){
		$sec = explode(" ",microtime());
		$tempo_ini_t= $sec[1] + $sec[0];
		
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'DUMP EQUIPAMENTOS INICIADO','unidade'=>'CENTRAL'));

		$unidades = $this->getUnidades();
			
		foreach ($unidades as $key => $un) {
			$this->info('inicio dump equip: '.$un);
			// if($un == 'PORTALEXT'){
				// ini_set('memory_limit', '256M');
				// $sec = explode(" ",microtime());
				// $tempo_ini= $sec[1] + $sec[0];
				// \DB::connection('LOG')->table('logs')->insert(array('log'=>'Criando dumps','unidade'=>'CENTRAL'));
				// $dumps = \DB::connection('PORTALEXT')->table('dumps')->where('status',0)->where('dump_status','like','%OK PARTE 0%')->get();
				// $auxdump = '';
				// foreach ($dumps as $key => $value) {
				// 	$auxdump .= $value->dump;
				// 	$partes_dumps = \DB::connection('PORTALEXT')->table('dumps')
				// 		->where('status',0)
				// 		->where('dump_status','like','%OK ID '.$value->id.'%')
				// 		->get();
				// 	foreach ($partes_dumps as $key => $parte) {
				// 		$auxdump .= $parte->dump;
				// 	}
				// 	if(strlen($auxdump) > 0){
				// 		$aux = strstr($auxdump, '-- Dump completed on ');
				// 		if(strlen($aux) > 0){
				// 			$status_dump = 'OK';
				// 		}else{
				// 			$status_dump = 'DUMP INCOMPLETO';
				// 		}
				// 		\DB::connection('LOG')->table('dumps')->insert(array('dump'=>$auxdump,'unidade' => $value->unidade,'tipo' =>$value->tipo,'quantidade_carac' => strlen($auxdump), 'dump_status' => $status_dump));
				// 		$msg = 'Salvo com sucesso';
				// 		$sec = explode(" ",microtime());
				// 		$tempo_fim= $sec[1] + $sec[0];
				// 		$tempo = number_format($tempo_fim-$tempo_ini);
				// 		\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$value->unidade));
				// 	}else{
				// 		$msg = 'ERRO: dump não criado';
				// 		$sec = explode(" ",microtime());
				// 		$tempo_fim= $sec[1] + $sec[0];
				// 		$tempo = number_format($tempo_fim-$tempo_ini);
				// 		\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$value->unidade,'tipo' => 'ERRO DUMP'));
				// 	}
				// }
				// \DB::connection('PORTALEXT')->table('dumps')->truncate();
			// 	continue;
			// }else{
				$conf = $this->getConfig($un);	
			// }
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'criando dump equipamento '.$un,'unidade'=>$un));
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$backupfile = public_path().'/dumps/'.'dump_equip_'.str_random(6).'_'.date("Ymd").'_'.$un.'.sql';
			try{
				system("mysqldump -h ".$conf['host']." -u".$conf['user']." -p".$conf['pass'].
					" --lock-tables qterp ".
					"equipamento_localizacao ".
					"equipamento_historico ".
					"> $backupfile");

				ini_set('memory_limit', '256M');
				$dump = file_get_contents($backupfile);
				if(strlen($dump) > 0){
					try{
						$aux = strstr($dump, '-- Dump completed on ');
						if(strlen($aux) > 0){
							$status_dump = 'OK';
						}else{
							$status_dump = 'DUMP INCOMPLETO';
						}
						\DB::connection('LOG')->table('dumps')->insert(array('dump'=>$dump,'unidade' => $un,'tipo' =>'EQUIPAMENTOS','quantidade_carac' => strlen($dump), 'dump_status' => $status_dump));
						$msg = 'Dump criado com sucesso '.$un;
						$sec = explode(" ",microtime());
						$tempo_fim= $sec[1] + $sec[0];
						$tempo = number_format($tempo_fim-$tempo_ini);
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
					}catch(\Exception $e) {
						$msg = 'ERRO: '.$e->getMessage();
						$sec = explode(" ",microtime());
						$tempo_fim= $sec[1] + $sec[0];
						$tempo = number_format($tempo_fim-$tempo_ini);
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DUMP EQ'));
					}
				}else{
					$msg = 'ERRO: dump não criado';
					$sec = explode(" ",microtime());
					$tempo_fim= $sec[1] + $sec[0];
					$tempo = number_format($tempo_fim-$tempo_ini);
					\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DUMP EQ'));
				}			
				
				unset($dump);
				$this->info('concluido dumps '.$un);
			}catch(\Exception $e) {
				$msg = 'ERRO: '.$e->getMessage();
				$this->info('erro: '.$un);
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DUMP EQ'));
				continue;
			}
		}
		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini_t);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'DUMP EQUIPAMENTOS FINALIZADO','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
	}






















	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
