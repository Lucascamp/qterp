<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sinclocequipamento extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sinclocequipamento';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Le os aruivos de Dump de localizacao de equipamentos e insere no Banco Central';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$this->info('Inicio sincronizacao local equipamentos');

		$sec = explode(" ",microtime());
		$tempo_ini_t= $sec[1] + $sec[0];

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Inicio sincronizacao local equipamentos','unidade'=>'CENTRAL'));
		
		$dumps = \DB::connection('LOG')->table('dumps')->where('status',0)->where('tipo','EQUIPAMENTOS')->where('dump_status','OK')->get();
		$truncate = 'truncate equipamento_historico;';
		// $data_sinc = \DB::connection('LOG')->table('dumps')->select(\DB::raw('MAX(data) as data'))->where('status',1)->where('tipo','EQUIPAMENTOS')->get();
		// if($data_sinc[0]->data){
		// 	$data_sinc = $data_sinc[0]->data;
		// }else{
		// 	$data_sinc = false;
		// }

		foreach ($dumps as $key => $dump) {
			$data_sinc = \DB::connection('LOG')->table('dumps')->select(\DB::raw('MAX(data) as data'))->where('status',1)->where('tipo','EQUIPAMENTOS')->where('unidade',$dump->unidade)->get();
			if($data_sinc[0]->data){
				$data_sinc = $data_sinc[0]->data;
			}else{
				$data_sinc = false;
			}
			$this->info('inicio unidade: '.$dump->unidade);
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'iniciando insert EQ','unidade'=>$dump->unidade));
			$stringDump = $dump->dump;

			\DB::connection('ESPELHO')->getPdo()->exec( $truncate );
			\DB::connection('ESPELHO')->getPdo()->exec( $stringDump );
			\DB::connection('ESPELHO')->getPdo()->exec( 'UNLOCK TABLES' );

			$this->EquipamentoLocalizacao($dump->unidade,$data_sinc);
			
			\DB::connection('LOG')->table('dumps')->where('id',$dump->id)->update(array('status'=>1));

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc local equipamento na unidade finalizado','tempo_total'=>$tempo,'unidade'=>$dump->unidade));
		}

		if(count($dumps) > 0){
			$this->info('espelho');
			$this->espelhacentral($dumps);
		}
		
		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini_t);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'sincronizacao local equipamentos finalizado ','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
		$this->info('fim Sincronizacao local equipamentos');
	}

	public function EquipamentoLocalizacao($unidade,$data){
		$this->info('sincronizando');
		if($data){
			$espelholoc = \DB::connection('ESPELHO')->table('equipamento_historico')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$espelholoc = \DB::connection('ESPELHO')->table('equipamento_historico')->get();
		}

		foreach ($espelholoc as $key => $loc) {
			$insert = (array) $loc;
			$this->info('id '. $insert['id']);
			unset($insert['id']);

			\DB::connection('CENTRAL')->table('equipamento_historico')->insert($insert);
		}
	}


	public function espelhacentral($dumps){
		$this->info('inicio espelhamento local equipamentos');
		$configCentral = ['host' => '10.155.64.134','user' => 'root', 'pass' => 'usbw'];
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'criando dump espelhoCentral','unidade'=>'CENTRAL'));
		
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		$backupfile = public_path().'/dumps/'.'dump_equip_CENTRAL_'.str_random(6).'_'.date("Ymd").'_'.'CENTRAL'.'.sql';
		try{
			system("mysqldump -h ".$configCentral['host']." -u".$configCentral['user']." -p".$configCentral['pass'].
				" --lock-tables qterp ".
				"equipamento_localizacao ".
				"equipamento_historico ".
				"> $backupfile");

			ini_set('memory_limit', '256M');
			$dump = file_get_contents($backupfile);
			
			if(strlen($dump) > 0){
				$aux = strstr($dump, '-- Dump completed on ');
				$trunc = 'truncate equipamento_historico;';
				if(strlen($aux) > 0){
					$unidades = $this->getUnidades();
					foreach ($dumps as $key => $dumpuni) {
						if(in_array($dumpuni->unidade, $unidades)){
							try{
								$this->info('loc'.$dumpuni->unidade);
								\DB::connection($dumpuni->unidade)->getPdo()->exec( $trunc );
								\DB::connection($dumpuni->unidade)->getPdo()->exec( $dump );
								\DB::connection($dumpuni->unidade)->getPdo()->exec( 'UNLOCK TABLES' );

								$msg = 'espelho central concluido';
								$this->info('espelho central concluido');
								$sec = explode(" ",microtime());
								$tempo_fim= $sec[1] + $sec[0];
								$tempo = number_format($tempo_fim-$tempo_ini);
								\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$dumpuni->unidade));	
							
							}catch(\Exception $e) {
								$msg = 'ERRO: '.$e->getMessage();
								$this->info('erro' .$e->getMessage());
								$sec = explode(" ",microtime());
								$tempo_fim= $sec[1] + $sec[0];
								$tempo = number_format($tempo_fim-$tempo_ini);
								\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>'CENTRAL','tipo' => 'ERRO DUMP ESCENTRAL'));
								continue;
							}
						}
					}
				}else{
					$msg = 'ERRO: dump criado com erro';
					$sec = explode(" ",microtime());
					$tempo_fim= $sec[1] + $sec[0];
					$tempo = number_format($tempo_fim-$tempo_ini);
					\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>'CENTRAL','tipo' => 'ERRO DUMP ESCENTRAL'));
				}
			}else{
				$msg = 'ERRO: dump não criado';
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>"CENTRAL",'tipo' => 'ERRO DUMP ESCENTRAL'));
			}			
			unset($dump);
			$this->info('concluido dumps');
		}catch(\Exception $e) {
			$msg = 'ERRO: '.$e->getMessage();
			$this->info('erro');
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>'CENTRAL','tipo' => 'ERRO DUMP ESCENTRAL'));
			continue;
		}
	}


	public function getUnidades(){
		return \Config::get('app.unidades');
	}



































	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
