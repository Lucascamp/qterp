<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sinccalibracao extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sinccalibracao';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'sincroniza a tabela de calibracao e os arquivos pdf';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	

		$this->AtualizaCalibCentral();
		$this->AtualizaCalib();
	}


	public function getUnidades(){
		return \Config::get('app.unidades');
	}

	public function AtualizaCalibCentral(){		
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			$CalibMG = \DB::connection('MG')->table('calibracao')->get();
		}catch(\Exception $e) {
			$this->info('erro con unidade: MG');
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
		}
		$Item = \App\Calibracao::all()->lists('id');
		
		foreach ($CalibMG as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Item)){
				\DB::table('calibracao')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('calibracao')->insert($insert);
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Calibracao Atualizado','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));

		$this->info('FIM ATUALIZACAO Calibracao CENTRAL');	
	}

	public function AtualizaCalib(){

		$unidades = $this->getUnidades();
		foreach ($unidades as $key => $un) {
			
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$itensLocal = \DB::table('calibracao')->get();
			try{
				$itens = \DB::connection($un)->table('calibracao')->lists('id');
			}catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
			foreach ($itensLocal as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $itens)){
					try{
						\DB::connection($un)->table('calibracao')->where('id',$value['id'])->update($insert);
						$msg = 'calibracao atualizado com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('calibracao')->insert($insert);
						$msg = 'calibracao atualizado com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}
			
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
		}
		
	}



















	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
