<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sincfontes extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sincfontes';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'sincroniza as fontes';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->AtualizaFontesCentral();
		$this->AtualizaFontes();
	}


	public function getUnidades(){
		return \Config::get('app.unidades');
	}

	public function AtualizaFontes(){

		$unidades = $this->getUnidades();
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$fontesLocal = \DB::table('fontes')->get();
			try{
				$fonte = \DB::connection($un)->table('fontes')->lists('id');
			}catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
			foreach ($fontesLocal as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $fonte)){
					try{
						\DB::connection($un)->table('fontes')->where('id',$value['id'])->update($insert);
						$msg = 'Fontes atualizadas com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('fontes')->insert($insert);
						$msg = 'Fontes atualizadas com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}
			
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));


			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$localFontesLocal =  \DB::table('fontes_localizacao')->get();
			try{
				$localfonte = \DB::connection($un)->table('fontes_localizacao')->lists('id');
			}catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
			
			foreach ($localFontesLocal as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $localfonte)){
					try{
						\DB::connection($un)->table('fontes_localizacao')->where('id',$value['id'])->update($insert);
						$msg = 'Local de fontes atualizadas com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('fontes_localizacao')->insert($insert);
						$msg = 'Local de fontes atualizadas com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}
			
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
		}
		
	}

	public function AtualizaFontesCentral(){		
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			$fontesMG = \DB::connection('MG')->table('fontes')->get();
		}catch(\Exception $e) {
			$this->info('erro con unidade: MG');
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
		}
		$fonte = \App\Fonte::all()->lists('id');
		
		foreach ($fontesMG as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $fonte)){
				\DB::table('fontes')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('fontes')->insert($insert);
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Fontes Atualizadas','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));


		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		try{
			$localFontesMG = \DB::connection('MG')->table('fontes_localizacao')->get();
		}catch(\Exception $e) {
			$this->info('erro con unidade: CENTRAL');
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
		}
		$localfonte = \App\Fontes_localizacao::all()->lists('id');
		
		foreach ($localFontesMG as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $localfonte)){
				\DB::table('fontes_localizacao')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('fontes_localizacao')->insert($insert);
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Local das fontes atualizados','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));	

		$this->info('FIM ATUALIZACAO FONTES CENTRAL');	
	}







	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
