<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sincdados extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sincdados';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sincroniza a tabela de clientes,usuarios,modulos,unidades';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Iniciando Sinc Dados ','unidade'=>'CENTRAL'));
		
		$this->AtualizaClientes();
		$this->AtualizaUsuarios();
		$this->AtualizaModulos();
		$this->AtualizaUnidades();

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc Dados Finalizado','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
	}

	public function getUnidades(){
		return \Config::get('app.unidades');
	}

	public function AtualizaClientes(){
		$unidades = $this->getUnidades();
		$this->info('inicio clientes');
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$ClientesLocal = \App\Cliente::all()->toArray();
			try{
				\DB::connection($un)->table('clientes')->truncate();
				foreach ($ClientesLocal as $key => $value) {
					$insert = (array) $value;
					\DB::connection($un)->table('clientes')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO Clientes: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}
		
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'cron sinc clientes concluida','tempo_total'=>$tempo,'unidade'=>$un));
			$this->info('concuido clientes '.$un);
		}
	}

	public function AtualizaUsuarios(){
		$unidades = $this->getUnidades();
		$this->info('inicio Usuarios');
		$UsuariosLocal = \App\User::all()->toArray();
		$permissoes = \App\permissaousuario::all()->toArray();
		$permissao_opcoes = \App\Permissaoopcoes::all()->toArray();
		$permissao_opcoes_usuario = \App\Permissaoopcoesusuario::all()->toArray();
		$funcionarios = \App\Funcionario::all()->toArray();
		$responsaveis = \DB::table('responsaveis_unidade')->get();
		
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			try{
				\DB::connection($un)->table('usuarios')->truncate();
				foreach ($UsuariosLocal as $key => $value) {
					$insert = (array) $value;
					\DB::connection($un)->table('usuarios')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO usuarios: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}
			try{
				\DB::connection($un)->table('permissaousuarios')->truncate();
				foreach ($permissoes as $key => $permissao) {
					$insert = $permissao;
					\DB::connection($un)->table('permissaousuarios')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO permissoes: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}

			try{
				\DB::connection($un)->table('permissao_opcoes')->truncate();
				foreach ($permissao_opcoes as $key => $permissao) {
					$insert = $permissao;
					\DB::connection($un)->table('permissao_opcoes')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO permissao_opcoes: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}

			try{
				\DB::connection($un)->table('permissao_opcoes_usuario')->truncate();
				foreach ($permissao_opcoes_usuario as $key => $permissao) {
					$insert = $permissao;
					\DB::connection($un)->table('permissao_opcoes_usuario')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO permissao_opcoes_usuario: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}

			try{
				\DB::connection($un)->table('funcionarios')->truncate();
				foreach ($funcionarios as $key => $item) {
					$insert = $item;
					\DB::connection($un)->table('funcionarios')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO funcionarios: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}
			try{
				\DB::connection($un)->table('responsaveis_unidade')->truncate();
				foreach ($responsaveis as $key => $value) {
					$insert = (array) $value;
					\DB::connection($un)->table('responsaveis_unidade')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO responsaveis_unidade: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'cron sinc responsaveis_unidade concluida','tempo_total'=>$tempo,'unidade'=>$un));
			$this->info('concluido usuarios '.$un);
		}
	}

	public function AtualizaModulos(){
		$unidades = $this->getUnidades();
		$this->info('inicio Modulos');
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		$Modulos      = \App\Modulo::all()->toArray();
		$AreasModulos = \App\Areasmodulos::all()->toArray();
		$Permissoes   = \App\Permissoes::all()->toArray();
			
		foreach ($unidades as $key => $un) {
			try{
				\DB::connection($un)->table('modulos')->truncate();
				foreach ($Modulos as $key => $modulo) {
					$insert = $modulo;
					\DB::connection($un)->table('modulos')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO Modulos: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}
			try{
				\DB::connection($un)->table('areasmodulos')->truncate();
				foreach ($AreasModulos as $key => $areasmodulo) {
					$insert = $areasmodulo;
					\DB::connection($un)->table('areasmodulos')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO AreasModulos: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}
			try{
				\DB::connection($un)->table('permissoes')->truncate();
				foreach ($Permissoes as $key => $permissoes) {
					$insert = $permissoes;
					\DB::connection($un)->table('permissoes')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO Permissoes: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'cron sinc modulos concluida','tempo_total'=>$tempo,'unidade'=>$un));
			$this->info('concuido modulos '.$un);
		}
	}

	public function AtualizaUnidades(){
		$unidades = $this->getUnidades();
		$this->info('inicio Unidades');
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$UnidadesLocal = \App\Unidade::all()->toArray();
			$CentroCusto = \App\Centrocusto::all()->toArray();
			try{
				\DB::connection($un)->table('unidade')->truncate();
				foreach ($UnidadesLocal as $key => $unidade) {
					$insert = $unidade;
					\DB::connection($un)->table('unidade')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO unidades: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}

			try{
				\DB::connection($un)->table('centro_custo')->truncate();
				foreach ($CentroCusto as $key => $item) {
					$insert = $item;
					\DB::connection($un)->table('centro_custo')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO centro_custo: '.$e,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DADOS'));
				continue;
			}
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'cron sinc unidades concluida','tempo_total'=>$tempo,'unidade'=>$un));
			$this->info('concluido unidades '.$un);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
