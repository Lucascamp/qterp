<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sincitemalmoxarifado extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sincitemalmoxarifado';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'sincroniza os itens nas unidadas';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'SINC ITENS ALMOXARIFADO INICIADO','unidade'=>'CENTRAL'));
		
		$this->AtualizaItensCentral();
		$this->AtualizaItens();
		
		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'SINC ITENS ALMOXARIFADO FINALIZADO','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
	}


	public function getUnidades(){
		return \Config::get('app.unidades');
	}

	public function AtualizaItensCentral(){		
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			$itensMG = \DB::connection('MG')->table('item_almoxarifado')->get();
		}catch(\Exception $e) {
			$this->info('erro con unidade: MG');
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
		}
		$Item = \App\ItemAlmoxarifado::all()->lists('id');
		
		foreach ($itensMG as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Item)){
				\DB::table('item_almoxarifado')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('item_almoxarifado')->insert($insert);
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Itens almoxarifado atualizados na CENTRAL','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));

		$this->info('FIM ATUALIZACAO ITENS CENTRAL');	
	}

	public function AtualizaItens(){

		$unidades = $this->getUnidades();
		foreach ($unidades as $key => $un) {
			
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$itensLocal = \DB::table('item_almoxarifado')->get();
			try{
				$itens = \DB::connection($un)->table('item_almoxarifado')->lists('id');
			}catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
			foreach ($itensLocal as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $itens)){
					try{
						\DB::connection($un)->table('item_almoxarifado')->where('id',$value['id'])->update($insert);
						$msg = 'itens almoxarifado atualizados com sucesso '.$un;
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('item_almoxarifado')->insert($insert);
						$msg = 'itens almoxarifado atualizados com sucesso '.$un;
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}
			
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
		}
		
	}

	








	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
