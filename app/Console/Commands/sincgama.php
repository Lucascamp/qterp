<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sincgama extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sincgama';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sincronizacao do sistema de gamagrafia';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$sec = explode(" ",microtime());
		$tempo_ini_t= $sec[1] + $sec[0];
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'iniciando sincronizacao gamagrafia','unidade'=>'CENTRAL'));


		$this->dadosGamagrafia();


		$dumps = \DB::connection('LOG')->table('dumps')->where('status',0)->where('tipo','GAMAGRAFIA')->where('dump_status','OK')->get();
		$truncate = 'truncate equipes; truncate guiaproducao; truncate guias; truncate programacoes; truncate gorelatorios; truncate sequencialunidade;'.
			' truncate sequencialfilmes; truncate bm; truncate obsmanual; truncate relatorios; truncate programacaorelatorios; truncate projetos;'.
			' truncate proposta_filme; truncate proposta_jornada; truncate proposta_junta; truncate proposta_servico; truncate propostas;';

		foreach ($dumps as $key => $dump) {
			$this->info('inicio sincronizacao dados producao');
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'iniciando sinc na unidade','unidade'=>$dump->unidade));
			$stringDump = $dump->dump;
			
			\DB::connection('ESPELHO')->getPdo()->exec( $truncate );
			\DB::connection('ESPELHO')->getPdo()->exec( $stringDump );
			\DB::connection('ESPELHO')->getPdo()->exec( 'UNLOCK TABLES' );

			$data_sinc = \DB::connection('LOG')->table('dumps')->select(\DB::raw('MAX(data) as data'))
				->where('status',1)
				->where('tipo','GAMAGRAFIA')
				->where('unidade',$dump->unidade)
				->get();

			if($data_sinc[0]->data){
				$data_sinc = substr($data_sinc[0]->data,0,10);
			}else{
				$data_sinc = false;
			}
						
			$this->guias($dump->unidade,$data_sinc);
			
			$this->equipes($dump->unidade,$data_sinc);
			
			$this->guiaproducao($dump->unidade,$data_sinc);

			$this->programacaorelatorios($dump->unidade,$data_sinc);
			
			$this->relatorios($dump->unidade,$data_sinc);
			
			$this->obsmanual($dump->unidade,$data_sinc);
			
			$this->bm($dump->unidade,$data_sinc);
			
			$this->sequencialfilmes($dump->unidade,$data_sinc);
			
			$this->sequencialunidade($dump->unidade,$data_sinc);
			
			$this->gorelatorios($dump->unidade,$data_sinc);
			
			$this->programacoes($dump->unidade,$data_sinc);

			$this->projetos($dump->unidade,$data_sinc);
			
			$this->proposta_filme($dump->unidade,$data_sinc);
			
			$this->proposta_jornada($dump->unidade,$data_sinc);
			
			$this->proposta_junta($dump->unidade,$data_sinc);
			
			$this->proposta_servico($dump->unidade,$data_sinc);
			
			$this->propostas($dump->unidade,$data_sinc);
			
			
			\DB::connection('LOG')->table('dumps')->where('id',$dump->id)->update(array('status'=>1));

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc na unidade finalizado','tempo_total'=>$tempo,'unidade'=>$dump->unidade));
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini_t);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'finalizado sincronizacao gamagrafia','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
	}

	public function getUnidades(){
		return \Config::get('app.unidades');
	}

	public function dadosGamagrafia(){
		$this->info('Inicio metodo dadosGamagrafia');
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Inicio metodo dadosGamagrafia','unidade'=>'CENTRAL'));
		$unidades = $this->getUnidades();

		$descontinuidades = \DB::table('descontinuidades')->get();
		$filmes = \DB::table('filmes')->get();
		$jornadas = \DB::table('jornadas')->get();
		$juntas = \DB::table('juntas')->get();
		$polegadasmilimetros = \DB::table('polegadasmilimetros')->get();

		foreach ($unidades as $key => $un) {
			
			try{
				$Descontinuidades = \DB::connection($un)->table('descontinuidades')->lists('id');
				$Filmes = \DB::connection($un)->table('filmes')->lists('id');
				$Jornadas = \DB::connection($un)->table('jornadas')->lists('id');
				$Juntas = \DB::connection($un)->table('juntas')->lists('id');
				$Polegadasmilimetros = \DB::connection($un)->table('polegadasmilimetros')->lists('id');
			}catch(\Exception $e) {
				$this->info('erro con uni lists: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
		
			foreach ($descontinuidades as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Descontinuidades)){
					try{
						\DB::connection($un)->table('descontinuidades')->where('id',$value['id'])->update($insert);
						$msg = 'descontinuidades atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('descontinuidades')->insert($insert);
						$msg = 'descontinuidades atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($filmes as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Filmes)){
					try{
						\DB::connection($un)->table('filmes')->where('id',$value['id'])->update($insert);
						$msg = 'filmes atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('filmes')->insert($insert);
						$msg = 'filmes atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($jornadas as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Jornadas)){
					try{
						\DB::connection($un)->table('jornadas')->where('id',$value['id'])->update($insert);
						$msg = 'jornadas atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('jornadas')->insert($insert);
						$msg = 'jornadas atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($juntas as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Juntas)){
					try{
						\DB::connection($un)->table('juntas')->where('id',$value['id'])->update($insert);
						$msg = 'juntas atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('juntas')->insert($insert);
						$msg = 'juntas atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($polegadasmilimetros as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Polegadasmilimetros)){
					try{
						\DB::connection($un)->table('polegadasmilimetros')->where('id',$value['id'])->update($insert);
						$msg = 'polegadasmilimetros atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con uni table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('polegadasmilimetros')->insert($insert);
						$msg = 'polegadasmilimetros atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con table: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}
		}
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Fim metodo dadosGamagrafia','unidade'=>'CENTRAL'));
		$this->info('Fim metodo dadosGamagrafia');
	}

	public function guias($unidade,$data){
		$this->info('Inicio guia');
		if($data){
			$espelhoguias = \DB::connection('ESPELHO')->table('guias')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$espelhoguias = \DB::connection('ESPELHO')->table('guias')->get();
		}
		$aux = [];
		foreach ($espelhoguias as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralguias = \DB::connection('CENTRAL')->table('guias')->whereIn('unidade',$aux)->lists('id');
		foreach ($espelhoguias as $key => $guia) {
			
			if(in_array($guia->id, $centralguias)){
				$insert = (array) $guia;
				\DB::connection('CENTRAL')->table('guias')->where('id',$guia->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $guia;
				\DB::connection('CENTRAL')->table('guias')->insert($insert);
			}					
		}
		$this->info('Fim guia');
	}

	public function equipes($unidade,$data){
		$this->info('Inicio Equipe');
		if($data){
			$eslpelhoequipes = \DB::connection('ESPELHO')->table('equipes')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoequipes = \DB::connection('ESPELHO')->table('equipes')->get();
		}
		$aux = [];
		foreach ($eslpelhoequipes as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralequipes = \DB::connection('CENTRAL')->table('equipes')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoequipes as $key => $equipe) {
			if(in_array($equipe->id, $centralequipes)){
				$insert = (array) $equipe;
				\DB::connection('CENTRAL')->table('equipes')->where('id',$equipe->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $equipe;
				\DB::connection('CENTRAL')->table('equipes')->insert($insert);
			}			
		}
		
		$this->info('Fim equipe ');
	}

	public function guiaproducao($unidade,$data){
		$this->info('Inicio guiaproducao');
		if($data){
			$eslpelhoguiaproducao = \DB::connection('ESPELHO')->table('guiaproducao')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoguiaproducao = \DB::connection('ESPELHO')->table('guiaproducao')->get();
		}
		$aux = [];
		foreach ($eslpelhoguiaproducao as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralguiaproducao = \DB::connection('CENTRAL')->table('guiaproducao')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoguiaproducao as $key => $guiaproducao) {
			if(in_array($guiaproducao->id, $centralguiaproducao)){
				$insert = (array) $guiaproducao;
				\DB::connection('CENTRAL')->table('guiaproducao')->where('id',$guiaproducao->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $guiaproducao;
				\DB::connection('CENTRAL')->table('guiaproducao')->insert($insert);
			}			
		}
		$this->info('Fim guiaproducao');
	}

	public function programacaorelatorios($unidade,$data){
		$this->info('Inicio programacaorelatorios');
		if($data){
			$eslpelhoprogramacaorelatorios = \DB::connection('ESPELHO')->table('programacaorelatorios')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoprogramacaorelatorios = \DB::connection('ESPELHO')->table('programacaorelatorios')->get();
		}
		$aux = [];
		foreach ($eslpelhoprogramacaorelatorios as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralprogramacaorelatorios = \DB::connection('CENTRAL')->table('programacaorelatorios')->where('unidade',$aux)->lists('id');
		foreach ($eslpelhoprogramacaorelatorios as $key => $programacaorelatorios) {
			if(in_array($programacaorelatorios->id, $centralprogramacaorelatorios)){
				$insert = (array) $programacaorelatorios;
				\DB::connection('CENTRAL')->table('programacaorelatorios')->where('id',$programacaorelatorios->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $programacaorelatorios;
				\DB::connection('CENTRAL')->table('programacaorelatorios')->insert($insert);
			}			
		}
		$this->info('Fim programacaorelatorios');
	}

	public function relatorios($unidade,$data){
		$this->info('Inicio relatorios');
		if($data){
			$eslpelhorelatorios = \DB::connection('ESPELHO')->table('relatorios')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhorelatorios = \DB::connection('ESPELHO')->table('relatorios')->get();
		}
		$aux = [];
		foreach ($eslpelhorelatorios as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralrelatorios = \DB::connection('CENTRAL')->table('relatorios')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhorelatorios as $key => $relatorios) {
			if(in_array($relatorios->id, $centralrelatorios)){
				$insert = (array) $relatorios;
				\DB::connection('CENTRAL')->table('relatorios')->where('id',$relatorios->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $relatorios;
				\DB::connection('CENTRAL')->table('relatorios')->insert($insert);
			}			
		}
		$this->info('Fim relatorios');
	}

	public function obsmanual($unidade,$data){
		$this->info('Inicio obsmanual');
		if($data){
			$eslpelhoobsmanual = \DB::connection('ESPELHO')->table('obsmanual')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoobsmanual = \DB::connection('ESPELHO')->table('obsmanual')->get();
		}
		$aux = [];
		foreach ($eslpelhoobsmanual as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralobsmanual = \DB::connection('CENTRAL')->table('obsmanual')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoobsmanual as $key => $obsmanual) {
			if(in_array($obsmanual->id, $centralobsmanual)){
				$insert = (array) $obsmanual;
				\DB::connection('CENTRAL')->table('obsmanual')->where('id',$obsmanual->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $obsmanual;
				\DB::connection('CENTRAL')->table('obsmanual')->insert($insert);
			}			
		}
		$this->info('Fim obsmanual');
	}

	public function bm($unidade,$data){
		$this->info('Inicio bm');
		if($data){
			$eslpelhobm = \DB::connection('ESPELHO')->table('bm')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhobm = \DB::connection('ESPELHO')->table('bm')->get();
		}
		$aux = [];
		foreach ($eslpelhobm as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralbm = \DB::connection('CENTRAL')->table('bm')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhobm as $key => $bm) {
			if(in_array($bm->id, $centralbm)){
				$insert = (array) $bm;
				\DB::connection('CENTRAL')->table('bm')->where('id',$bm->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $bm;
				\DB::connection('CENTRAL')->table('bm')->insert($insert);
			}			
		}
		$this->info('Fim bm');
	}

	public function sequencialfilmes($unidade,$data){
		$this->info('Inicio sequencialfilmes');
		if($data){
			$eslpelhosequencialfilmes = \DB::connection('ESPELHO')->table('sequencialfilmes')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhosequencialfilmes = \DB::connection('ESPELHO')->table('sequencialfilmes')->get();
		}
		$aux = [];
		foreach ($eslpelhosequencialfilmes as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralsequencialfilmes = \DB::connection('CENTRAL')->table('sequencialfilmes')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhosequencialfilmes as $key => $sequencialfilmes) {
			if(in_array($sequencialfilmes->id, $centralsequencialfilmes)){
				$insert = (array) $sequencialfilmes;
				\DB::connection('CENTRAL')->table('sequencialfilmes')->where('id',$sequencialfilmes->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $sequencialfilmes;
				\DB::connection('CENTRAL')->table('sequencialfilmes')->insert($insert);
			}			
		}
		$this->info('Fim sequencialfilmes');
	}

	public function sequencialunidade($unidade,$data){
		$this->info('Inicio sequencialunidade');
		if($data){
			$eslpelhosequencialunidade = \DB::connection('ESPELHO')->table('sequencialunidade')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhosequencialunidade = \DB::connection('ESPELHO')->table('sequencialunidade')->get();
		}
		$aux = [];
		foreach ($eslpelhosequencialunidade as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);

		$centralsequencialunidade = \DB::connection('CENTRAL')->table('sequencialunidade')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhosequencialunidade as $key => $sequencialunidade) {
			if(in_array($sequencialunidade->id, $centralsequencialunidade)){
				$insert = (array) $sequencialunidade;
				\DB::connection('CENTRAL')->table('sequencialunidade')->where('id',$sequencialunidade->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $sequencialunidade;
				\DB::connection('CENTRAL')->table('sequencialunidade')->insert($insert);
			}			
		}
		$this->info('Fim sequencialunidade');
	}

	public function gorelatorios($unidade,$data){
		$this->info('Inicio gorelatorios');
		if($data){
			$eslpelhogorelatorios = \DB::connection('ESPELHO')->table('gorelatorios')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhogorelatorios = \DB::connection('ESPELHO')->table('gorelatorios')->get();
		}
		$aux = [];
		foreach ($eslpelhogorelatorios as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
			$centralgorelatorios = \DB::connection('CENTRAL')->table('gorelatorios')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhogorelatorios as $key => $gorelatorios) {
			if(in_array($gorelatorios->id, $centralgorelatorios)){
				$insert = (array) $gorelatorios;
				\DB::connection('CENTRAL')->table('gorelatorios')->where('id',$gorelatorios->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $gorelatorios;
				\DB::connection('CENTRAL')->table('gorelatorios')->insert($insert);
			}			
		}
		$this->info('Fim gorelatorios');
	}

	public function programacoes($unidade,$data){
		$this->info('Inicio programacoes');
		if($data){
			$eslpelhoprogramacoes = \DB::connection('ESPELHO')->table('programacoes')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoprogramacoes = \DB::connection('ESPELHO')->table('programacoes')->get();
		}
		$aux = [];
		foreach ($eslpelhoprogramacoes as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralprogramacoes = \DB::connection('CENTRAL')->table('programacoes')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoprogramacoes as $key => $programacoes) {
			if(in_array($programacoes->id, $centralprogramacoes)){
				$insert = (array) $programacoes;
				\DB::connection('CENTRAL')->table('programacoes')->where('id',$programacoes->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $programacoes;
				\DB::connection('CENTRAL')->table('programacoes')->insert($insert);
			}			
		}
		$this->info('Fim programacoes');
	}

	public function projetos($unidade,$data){
		$this->info('Inicio projetos');
		if($data){
			$eslpelhoprojetos = \DB::connection('ESPELHO')->table('projetos')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoprojetos = \DB::connection('ESPELHO')->table('projetos')->get();
		}
		$aux = [];
		foreach ($eslpelhoprojetos as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralprojetos = \DB::connection('CENTRAL')->table('projetos')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoprojetos as $key => $projetos) {
			if(in_array($projetos->id, $centralprojetos)){
				$insert = (array) $projetos;
				\DB::connection('CENTRAL')->table('projetos')->where('id',$projetos->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $projetos;
				\DB::connection('CENTRAL')->table('projetos')->insert($insert);
			}			
		}
		$this->info('Fim projetos');
	}

	public function proposta_filme($unidade,$data){
		$this->info('Inicio proposta_filme');
		if($data){
			$eslpelhoproposta_filme = \DB::connection('ESPELHO')->table('proposta_filme')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoproposta_filme = \DB::connection('ESPELHO')->table('proposta_filme')->get();
		}
		$aux = [];
		foreach ($eslpelhoproposta_filme as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralproposta_filme = \DB::connection('CENTRAL')->table('proposta_filme')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoproposta_filme as $key => $proposta_filme) {
			if(in_array($proposta_filme->id, $centralproposta_filme)){
				$insert = (array) $proposta_filme;
				\DB::connection('CENTRAL')->table('proposta_filme')->where('id',$proposta_filme->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $proposta_filme;
				\DB::connection('CENTRAL')->table('proposta_filme')->insert($insert);
			}			
		}
		$this->info('Fim proposta_filme');
	}

	public function proposta_jornada($unidade,$data){
		$this->info('Inicio proposta_jornada');
		if($data){
			$eslpelhoproposta_jornada = \DB::connection('ESPELHO')->table('proposta_jornada')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoproposta_jornada = \DB::connection('ESPELHO')->table('proposta_jornada')->get();
		}
		$aux = [];
		foreach ($eslpelhoproposta_jornada as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralproposta_jornada = \DB::connection('CENTRAL')->table('proposta_jornada')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoproposta_jornada as $key => $proposta_jornada) {
			if(in_array($proposta_jornada->id, $centralproposta_jornada)){
				$insert = (array) $proposta_jornada;
				\DB::connection('CENTRAL')->table('proposta_jornada')->where('id',$proposta_jornada->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $proposta_jornada;
				\DB::connection('CENTRAL')->table('proposta_jornada')->insert($insert);
			}			
		}
		$this->info('Fim proposta_jornada');
	}

	public function proposta_junta($unidade,$data){
		$this->info('Inicio proposta_junta');
		if($data){
			$eslpelhoproposta_junta = \DB::connection('ESPELHO')->table('proposta_junta')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoproposta_junta = \DB::connection('ESPELHO')->table('proposta_junta')->get();
		}
		$aux = [];
		foreach ($eslpelhoproposta_junta as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralproposta_junta = \DB::connection('CENTRAL')->table('proposta_junta')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoproposta_junta as $key => $proposta_junta) {
			if(in_array($proposta_junta->id, $centralproposta_junta)){
				$insert = (array) $proposta_junta;
				\DB::connection('CENTRAL')->table('proposta_junta')->where('id',$proposta_junta->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $proposta_junta;
				\DB::connection('CENTRAL')->table('proposta_junta')->insert($insert);
			}			
		}
		$this->info('Fim proposta_junta');
	}

	public function proposta_servico($unidade,$data){
		$this->info('Inicio proposta_servico');
		if($data){
			$eslpelhoproposta_servico = \DB::connection('ESPELHO')->table('proposta_servico')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhoproposta_servico = \DB::connection('ESPELHO')->table('proposta_servico')->get();
		}
		$aux = [];
		foreach ($eslpelhoproposta_servico as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralproposta_servico = \DB::connection('CENTRAL')->table('proposta_servico')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhoproposta_servico as $key => $proposta_servico) {
			if(in_array($proposta_servico->id, $centralproposta_servico)){
				$insert = (array) $proposta_servico;
				\DB::connection('CENTRAL')->table('proposta_servico')->where('id',$proposta_servico->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $proposta_servico;
				\DB::connection('CENTRAL')->table('proposta_servico')->insert($insert);
			}			
		}
		$this->info('Fim proposta_servico');
	}

	public function propostas($unidade,$data){
		$this->info('Inicio propostas');
		if($data){
			$eslpelhopropostas = \DB::connection('ESPELHO')->table('propostas')->where('created_at','>',$data)->orWhere('updated_at','>',$data)->get();
		}else{
			$eslpelhopropostas = \DB::connection('ESPELHO')->table('propostas')->get();
		}
		$aux = [];
		foreach ($eslpelhopropostas as $key => $value) {
			$aux[] = $value->unidade;
		}	
		$aux[] = $unidade;
		$aux = array_unique($aux);
		$centralpropostas = \DB::connection('CENTRAL')->table('propostas')->whereIn('unidade',$aux)->lists('id');
		foreach ($eslpelhopropostas as $key => $propostas) {
			if(in_array($propostas->id, $centralpropostas)){
				$insert = (array) $propostas;
				\DB::connection('CENTRAL')->table('propostas')->where('id',$propostas->id)->where('unidade',$unidade)->update($insert);
			}else{
				$insert = (array) $propostas;
				\DB::connection('CENTRAL')->table('propostas')->insert($insert);
			}			
		}
		$this->info('Fim propostas');
	}








	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
