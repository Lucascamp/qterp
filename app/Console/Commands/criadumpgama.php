<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class criadumpgama extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'criadumpgama';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'cria dumps de gamagrafia dos servidores';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->criardump();
	}

	public function getUnidades(){
		return \Config::get('app.unidades');
		// return array('MG','BA','RN','MC','PE');
	}
	public function getConfig($un){
		$configMG = ['host' => '10.155.64.9','user' => 'root', 'pass' => 'root'];
		$configBA = ['host' => '10.155.65.5','user' => 'root', 'pass' => 'usbw'];
		$configRN = ['host' => '10.155.65.72','user' => 'root', 'pass' => 'usbw'];
		$configMC = ['host' => '10.166.3.4','user' => 'root', 'pass' => 'usbw'];
		$configPE = ['host' => '10.155.65.132','user' => 'root', 'pass' => 'usbw'];
		$configRJ = ['host' => '10.155.74.2','user' => 'root', 'pass' => 'usbw'];
		$configs  = ['MG' => $configMG , 'BA' => $configBA , 'RN' => $configRN , 'MC' => $configMC , 'PE' => $configPE, 'RJ' => $configRJ];
		$config = $configs[$un];
		return $config;
	}
	public function criardump(){
		$sec = explode(" ",microtime());
		$tempo_ini_t= $sec[1] + $sec[0];
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'DUMP GAMAGRAFIA INICIO','unidade'=>'CENTRAL'));

		$unidades = $this->getUnidades();			
		foreach ($unidades as $key => $un) {
			$this->info('inicio dump: '.$un);
			
			$conf = $this->getConfig($un);	
			
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'criando dump '.$un,'unidade'=>$un));
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$backupfile = public_path().'/dumps/'.'dump_'.str_random(6).'_'.date("Ymd").'_'.$un.'.sql';
			try{
				system("mysqldump -h ".$conf['host']." -u".$conf['user']." -p".$conf['pass'].
					" --lock-tables qterp ".
					"equipes ".
					"guiaproducao ".
					"guias ".
					"programacoes ".
					"gorelatorios ".
					"sequencialunidade ".
					"sequencialfilmes ".
					"bm ".
					"obsmanual ".
					"relatorios ".
					"programacaorelatorios ".
					"projetos ".
					"proposta_filme ".
					"proposta_jornada ".
					"proposta_junta ".
					"proposta_servico ".
					"propostas ".
					"> $backupfile");

				ini_set('memory_limit', '256M');
				$dump = file_get_contents($backupfile);
				if(strlen($dump) > 0){
					try{
						$aux = strstr($dump, '-- Dump completed on ');
						if(strlen($aux) > 0){
							$status_dump = 'OK';
						}else{
							$status_dump = 'DUMP INCOMPLETO';
						}
						\DB::connection('LOG')->table('dumps')->insert(array('dump'=>$dump,'unidade' => $un,'tipo' =>'GAMAGRAFIA','quantidade_carac' => strlen($dump), 'dump_status' => $status_dump));
						$msg = 'Dump criado com sucesso '.$un;
						$sec = explode(" ",microtime());
						$tempo_fim= $sec[1] + $sec[0];
						$tempo = number_format($tempo_fim-$tempo_ini);
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
					}catch(\Exception $e) {
						$msg = 'ERRO: '.$e->getMessage();
						$sec = explode(" ",microtime());
						$tempo_fim= $sec[1] + $sec[0];
						$tempo = number_format($tempo_fim-$tempo_ini);
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DUMP'));
					}
				}else{
					$msg = 'ERRO: dump não criado';
					$sec = explode(" ",microtime());
					$tempo_fim= $sec[1] + $sec[0];
					$tempo = number_format($tempo_fim-$tempo_ini);
					\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DUMP'));
				}			
				
				unset($dump);
				$this->info('concuido dumps '.$un);
			}catch(\Exception $e) {
				$msg = 'ERRO: '.$e->getMessage();
				$this->info('erro: '.$un);
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un,'tipo' => 'ERRO DUMP'));
				continue;
			}
		}
		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini_t);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'DUMP GAMAGRAFIA FINALIZADO','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));
	}



























	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
