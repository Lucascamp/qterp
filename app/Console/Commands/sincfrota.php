<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class sincfrota extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sincfrota';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'sincroniza as tabelas do modulo frota';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->AtualizaFrotaCentral();
		$this->AtualizaItens();
	}


	public function getUnidades(){
		return \Config::get('app.unidades');
	}

	public function AtualizaFrotaCentral(){		
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			$carro = \DB::connection('MG')->table('carro')->get();
			$ecofrota = \DB::connection('MG')->table('carro_ecofrota')->get();
			$imposto = \DB::connection('MG')->table('carro_imposto')->get();
			$localizacao = \DB::connection('MG')->table('carro_localizacao')->get();
			$manutencao = \DB::connection('MG')->table('carro_manutencao')->get();
			$seguro = \DB::connection('MG')->table('carro_seguro')->get();
		}catch(\Exception $e) {
			$this->info('erro con unidade: MG');
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
		}

		$Carro = \App\Carro::all()->lists('id');
		$Ecofrota = \App\Ecofrota::all()->lists('id');
		$Impostocarro = \App\Impostocarro::all()->lists('id');
		$Localizacaocarro = \App\Localizacaocarro::all()->lists('id');
		$Manutencaocarro = \App\Manutencaocarro::all()->lists('id');
		$Segurocarro = \App\Segurocarro::all()->lists('id');
		
		foreach ($carro as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Carro)){
				\DB::table('carro')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('carro')->insert($insert);
			}
		}
		foreach ($ecofrota as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Ecofrota)){
				\DB::table('carro_ecofrota')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('carro_ecofrota')->insert($insert);
			}
		}
		foreach ($imposto as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Impostocarro)){
				\DB::table('carro_imposto')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('carro_imposto')->insert($insert);
			}
		}
		foreach ($localizacao as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Localizacaocarro)){
				\DB::table('carro_localizacao')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('carro_localizacao')->insert($insert);
			}
		}
		foreach ($manutencao as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Manutencaocarro)){
				\DB::table('carro_manutencao')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('carro_manutencao')->insert($insert);
			}
		}
		foreach ($seguro as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $Segurocarro)){
				\DB::table('carro_seguro')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('carro_seguro')->insert($insert);
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Itens Atualizadas','tempo_total'=>$tempo,'unidade'=>'CENTRAL'));

		$this->info('FIM ATUALIZACAO FROTA CENTRAL');	
	}

	public function AtualizaItens(){

		$unidades = $this->getUnidades();
		$carro = \DB::table('carro')->get();
		$ecofrota = \DB::table('carro_ecofrota')->get();
		$imposto = \DB::table('carro_imposto')->get();
		$localizacao = \DB::table('carro_localizacao')->get();
		$manutencao = \DB::table('carro_manutencao')->get();
		$seguro = \DB::table('carro_seguro')->get();

		foreach ($unidades as $key => $un) {
			
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];
			try{
				$Carro = \DB::connection($un)->table('carro')->lists('id');
				$Ecofrota = \DB::connection($un)->table('carro_ecofrota')->lists('id');
				$Impostocarro = \DB::connection($un)->table('carro_imposto')->lists('id');
				$Localizacaocarro = \DB::connection($un)->table('carro_localizacao')->lists('id');
				$Manutencaocarro = \DB::connection($un)->table('carro_manutencao')->lists('id');
				$Segurocarro = \DB::connection($un)->table('carro_seguro')->lists('id');
			}catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
		
			foreach ($carro as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Carro)){
					try{
						\DB::connection($un)->table('carro')->where('id',$value['id'])->update($insert);
						$msg = 'carro atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('carro')->insert($insert);
						$msg = 'carro atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($ecofrota as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Ecofrota)){
					try{
						\DB::connection($un)->table('carro_ecofrota')->where('id',$value['id'])->update($insert);
						$msg = 'carro_ecofrota atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('carro_ecofrota')->insert($insert);
						$msg = 'carro_ecofrota atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($imposto as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Impostocarro)){
					try{
						\DB::connection($un)->table('carro_imposto')->where('id',$value['id'])->update($insert);
						$msg = 'carro_imposto atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('carro_imposto')->insert($insert);
						$msg = 'carro_imposto atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($localizacao as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Localizacaocarro)){
					try{
						\DB::connection($un)->table('carro_localizacao')->where('id',$value['id'])->update($insert);
						$msg = 'carro_localizacao atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('carro_localizacao')->insert($insert);
						$msg = 'carro_localizacao atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($manutencao as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Manutencaocarro)){
					try{
						\DB::connection($un)->table('carro_manutencao')->where('id',$value['id'])->update($insert);
						$msg = 'carro_manutencao atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('carro_manutencao')->insert($insert);
						$msg = 'carro_manutencao atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}

			foreach ($seguro as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $Segurocarro)){
					try{
						\DB::connection($un)->table('carro_seguro')->where('id',$value['id'])->update($insert);
						$msg = 'carro_seguro atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{
					try{
						\DB::connection($un)->table('carro_seguro')->insert($insert);
						$msg = 'carro_seguro atualizados com sucesso';
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}
			}
			
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
		}
		
	}










	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
