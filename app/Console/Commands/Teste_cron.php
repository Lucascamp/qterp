<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Teste_cron extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'teste';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Teste Cron';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// \DB::connection('teste')->table('teste')->insert(
		// 						array(
		// 							'teste'  	=> 'ola teste',
		// 							)
		// 						);
		// $this->AtualizaClientes();
		$this->AtualizaFontesCentral();
		// $this->AtualizaFontes();
		
	}
	public function getUnidades(){
		return array('MG','BA','RN','MC','PE');
	}

	public function AtualizaFontes(){

		$unidades = $this->getUnidades();
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$fontesLocal = \DB::connection('CENTRAL')->table('fontes')->get();
			try{
				$fonte = \DB::connection($un)->table('fontes')->lists('id');
			}catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}
			

			$msg = '';
			foreach ($fontesLocal as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $fonte)){
					try{
						\DB::connection($un)->table('fontes')->where('id',$value['id'])->update($insert);
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;;
					}
					$msg .= 'Fonte '.$value['id'].' Atualizado;';
				}else{
					try{
						\DB::connection($un)->table('fontes')->insert($insert);
					}catch(\Exception $e) {
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;;
					}
					$msg .='Fonte'. $value['id'].' Inserido;';
				}
			}

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));


			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$localFontesLocal =  \DB::connection('CENTRAL')->table('fontes_localizacao')->get();
			$localfonte = \DB::connection($un)->table('fontes_localizacao')->lists('id');
			$msg = '';
			foreach ($localFontesLocal as $key => $value) {
				$value = (array) $value;
				$insert = $value;
				if(in_array($value['id'], $localfonte)){
					\DB::connection($un)->table('fontes_localizacao')->where('id',$value['id'])->update($insert);
					$msg .= 'Local '.$value['id'].' Atualizado;';
				}else{
					\DB::connection($un)->table('fontes_localizacao')->insert($insert);
					$msg .='Local'. $value['id'].' Inserido;';
				}
			}
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
		}
		
	}

	public function AtualizaFontesCentral(){
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		
		try{
			$fontesMG = \DB::connection('MG')->table('fontes')->get();
		}catch(\Exception $e) {
			$this->info('erro con unidade: MG');
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'MG'));
		}
		$fonte = \App\Fonte::all()->lists('id');
		
		foreach ($fontesMG as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $fonte)){
				\DB::table('fontes')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('fontes')->insert($insert);
			}
		}
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Fontes Atualizadas','unidade'=>'CENTRAL'));

		dd($fontesMG);

		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		$fontesLocal = \App\Fonte::all()->toArray();
		try{
			$fonte = \DB::connection('CENTRAL')->table('fontes')->lists('id');
		}catch(\Exception $e) {
			$this->info('erro con unidade: '.$un);
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
			continue;
		}
			
		$msg = '';
		foreach ($fontesLocal as $key => $value) {
			$insert = $value;
			if(in_array($value['id'], $fonte)){
				try{
					\DB::connection('CENTRAL')->table('fontes')->where('id',$value['id'])->update($insert);
				}catch(\Exception $e) {
					$this->info('erro con unidade: '.'CENTRAL');
					$msg = 'ERRO: '.$e->getMessage();
					\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
					break;;
				}
				$msg .= 'Fonte '.$value['id'].' Atualizado;';
			}else{
				try{
					\DB::connection('CENTRAL')->table('fontes')->insert($insert);
				}catch(\Exception $e) {
					$this->info('erro con unidade: '.'CENTRAL');
					$msg = 'ERRO: '.$e->getMessage();
					\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'CENTRAL'));
					break;;
				}
				$msg .='Fonte'. $value['id'].' Inserido;';
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>'CENTRAL'));


		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];


		$localFontesLocal =  \App\Fontes_localizacao::all()->toArray();
		$localfonte = \DB::connection('CENTRAL')->table('fontes_localizacao')->lists('id');
		$msg = '';
		foreach ($localFontesLocal as $key => $value) {
			$insert = $value;
			if(in_array($value['id'], $localfonte)){
				\DB::connection('CENTRAL')->table('fontes_localizacao')->where('id',$value['id'])->update($insert);
				$msg .= 'Local '.$value['id'].' Atualizado;';
			}else{
				\DB::connection('CENTRAL')->table('fontes_localizacao')->insert($insert);
				$msg .='Local'. $value['id'].' Inserido;';
			}
		}
		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>'CENTRAL'));	

		$this->info('FIM ATUALIZACAO FONTES CENTRAL');	
	}


	public function AtualizaClientes(){
		$unidades = $this->getUnidades();
		$this->info('inicio clientes');
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];

			$ClientesLocal = \App\Cliente::all()->toArray();
			try {
    			$Clientes = \DB::connection($un)->table('clientes')->lists('id');
			} catch(\Exception $e) {
				$this->info('erro con unidade: '.$un);
				$msg = 'ERRO: '.$e->getMessage();
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
				continue;
			}

			$msg = '';
			foreach ($ClientesLocal as $key => $value) {
				$this->info('inicio cliente '.$value['id']);
				$insert = $value;
				if(in_array($value['id'], $Clientes)){
					try{
						\DB::connection($un)->table('clientes')->where('id',$value['id'])->update($insert);
						$msg .= 'C '.$value['id'].' A; ';
					}catch(\Exception $e){
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
				}else{

					try{
						\DB::connection($un)->table('clientes')->where('id',$value['id'])->insert($insert);
						$msg .='C'. $value['id'].' I; ';
					}catch(\Exception $e){
						$this->info('erro con unidade: '.$un);
						$msg = 'ERRO: '.$e->getMessage();
						\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$un));
						break;
					}
					
				}
				$this->info('fim cliente '.$value['id']);
			}

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$un));
			$this->info('concuido clientes '.$un);
		}
	}
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['name', InputArgument::OPTIONAL, 'O nome que deverá ser exibido.','Word'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
