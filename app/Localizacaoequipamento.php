<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoftDeletes;

class Localizacaoequipamento extends Model
{
	protected $dates = ['deleted_at'];
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'equipamento_localizacao';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = ['equipamento_id', 'centrocusto_id', 'projeto', 'funcionario_responsavel', 'usuario_receptor', 
								'recebido' ,'motivo' , 'data_movimentacao', 'created_by', 'updated_by'];


	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(		
		'id_centrocusto' => 'required',
		'projeto_atual'	 => 'required',
		);

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data_movimentacao'
			);
	}

	/**
	 * Define the relationship with item contabil	 
	 */
	public function centrocusto()
	{
		return $this->belongsTo('centrocusto', 'id_centrocusto');
	}

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function funcionario()
	{
		return $this->belongsTo('Funcionario', 'id');
	}

	/**
	 * Define the relationship with equipamento	 
	 */
	public function equipamento()
	{
		return $this->belongsTo('Equipamento', 'id');
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataMovimentacaoAttribute($value)
	{
		$this->attributes['data_movimentacao']  = Carbon::createFromFormat('Y-m-d', $value);
	}
	
}