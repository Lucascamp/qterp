<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SoftDeletes;

class Carro extends Model
{
    protected $dates = ['deleted_at'];

	protected $table = 'carro';

	protected $fillable = ['placa','marca','modelo','combustivel','ano','cor','chassi','km','data_compra','valor_compra',
							'rastreador','renavan','responsavel','proprietario','alugado','data_aluguel','prazo_contrato',
							'termino_contrato','valor_locacao','multa_recisao','data_reajuste','tipo_reajuste','mes_reajuste',
							'ano_reajuste','created_by','updated_by'];

	public function getDates()
	{
		return ['created_at','updated_at','data_compra','data_aluguel','termino_contrato','data_reajuste'];
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataCompraCompraAttribute($value)
	{
		$this->attributes['data_compra']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	public function setDataAluguelCompraAttribute($value)
	{
		$this->attributes['data_aluguel']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	public function setTerminoContratoCompraAttribute($value)
	{
		$this->attributes['termino_contrato']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	public function setDataReajusteCompraAttribute($value)
	{
		$this->attributes['data_reajuste']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	public function imposto()
	{
		return $this->hasMany('App\Impostocarro', 'carro_id');
	}

	public function manutencao()
	{
		return $this->hasMany('App\Manutencaocarro', 'carro_id');
	}

	public function seguro()
	{
		return $this->hasOne('App\Segurocarro', 'carro_id');
	}

	public function ecofrota()
	{
		return $this->hasOne('App\Ecofrota', 'carro_id');
	}

	public function localizacao()
	{
		return $this->hasMany('App\Localizacaocarro', 'carro_id');
	}
}
