<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposta extends Model {

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'propostas';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	public function guia()
	{
		return $this->hasMany('App\Guia', 'proposta_id');
	}

	public function filme()
   	{
      	return $this->belongsToMany('App\Filme', 'proposta_filme');
   	}

   	public function jornada()
   	{
      	return $this->belongsToMany('App\Jornada', 'proposta_jornada');
   	}

}
