<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SoftDeletes;

class Calibracao extends Model
{
	protected $dates = ['deleted_at'];
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'calibracao';

	protected $fillable = array('equipamento_id', 'laboratorio', 'certificado', 'data_calibracao', 'parecer', 'comentario', 'documentacao',
								'primeiro_uso', 'vencimento','filepath','created_by', 'updated_by', 'deleted_by');


	/**
	 * Define the relationship with clientes	 
	 */
	public function equipamento()
	{
		return $this->belongsTo('App\Equipamento', 'equipamento_id', 'id');
	}

	public function getDates()
	{
		return array(
			'created_at', 
			'updated_at', 
			'data_calibracao',
			'primeiro_uso',
			'vencimento',
			);
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataCalibracaoAttribute($value)
	{
		$this->attributes['data_calibracao']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setPrimeiroUsoAttribute($value)
	{
		if(!empty($value))
			$this->attributes['primeiro_uso']  = Carbon::createFromFormat('Y-m-d', $value);
	}

	/*== Convert  Dates to Save on DB ==*/
    public function setVencimentoAttribute($value)
    {
        $this->attributes['vencimento']  = Carbon::createFromFormat('Y-m-d', $value);
    }

	public function latest($equipamentos)
	{
		foreach ($equipamentos as $equip) 
		{
			$latest[$equip->id] = DB::table('calibracao')
								->where('equipamento_id', $equip->id)
								->where('parecer', '1')
								->orderBy('id', 'Desc')
								->first();
		}
		
		if(isset($latest))
			return $latest;
	}

	public static function toCarbon($date)
	{
		return Carbon::createFromFormat('Y-m-d', $date); 
	}

	public static function getTDClass($vencimento)
	{
		$now = Carbon::now();

		if($now > $vencimento)	
			return 'red';
		elseif($now < $vencimento and $vencimento->diffInDays($now) > 30 and $vencimento->diffInDays($now) < 60)
			return 'yellow';
		elseif($now < $vencimento and $vencimento->diffInDays($now) > 0 and $vencimento->diffInDays($now) < 30)
			return 'orange';
	}

	public function filtrovencimento($vencimento, $dia_vencimento)
	{
		if($vencimento == 60)
			$now = Carbon::now()->addDays(30);
		else
			$now = Carbon::now();

		if($vencimento == 'zero')
		{
			$vencidos = DB::table('calibracao')
							->where('vencimento', '<', $now)
							->where('vencimento', '<>', '0000-00-00')
							->whereNull('deleted_at')
							->get();
		}
		elseif($vencimento == 'calibrados')
			$vencidos = DB::table('calibracao')->where('vencimento', '>',$now)->get();

		else	
			$vencidos = DB::table('calibracao')->whereBetween('vencimento', array($now, $dia_vencimento))->get();

		$equipamentos = array();

		foreach ($vencidos as $vencido) 
		{
			if($vencimento == 'zero')
			{
				$check = DB::table('calibracao')->where('vencimento', '>', $now)
												   ->where('equipamento_id', $vencido->equipamento_id)
												   ->get();

				if(!$check)
					array_push($equipamentos, $vencido->equipamento_id);
			}
			else
				array_push($equipamentos, $vencido->equipamento_id);
		}

		return $equipamentos;
	}
}
