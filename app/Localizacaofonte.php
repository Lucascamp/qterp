<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SoftDeletes;

class Localizacaofonte extends Model
{

	protected $dates = ['deleted_at'];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'fontes_localizacao';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = ['fonte_id','centrocusto_id','unidade','frente_servico','data','observacoes','created_by','updated_by'];

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(		
		'centrocusto_id' => 'required',
		'frente_servico' => 'required',
		'projeto_atual'	 => 'required',
		);

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data'
			);
	}

	public function fonte()
	{
		return $this->belongsTo('Fonte', 'id');
	}

	public function centrocusto()
	{
		return $this->belongsTo('centrocusto', 'centrocusto_id', 'id');
	}

	public function setDataAttribute($value)
	{
		$this->attributes['data']  = Carbon::createFromFormat('Y-m-d', $value);
	}
	
}