<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SoftDeletes;

class ItemAlmoxarifado extends Model
{
    protected $dates = ['deleted_at'];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'item_almoxarifado';

	protected $fillable = array('descricao','codigo','unidade_medida','valor','created_by','updated_by');

	public function getDates()
	{
		return array(
			'created_at', 
			'updated_at', 
			);
	}

	/**
	 * Define the relationship with calibracao	 
	 */
	// public function calibracao()
	// {
	// 	return $this->hasMany('App\Calibracao', 'equipamento_id');
	// }

	/**
	 * Define the relationship with localizacao	 
	 */
	// public function localizacao()
	// {
	// 	return $this->hasMany('App\Localizacao', 'equipamento_id');
	// }

	
}
