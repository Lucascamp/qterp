<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateFonte extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'numero'				=> 'required',
			'irradiador'		   	=> 'required',
			'pasta'		   			=> 'required',
			'modelo'		   		=> 'required',
			'status' 	   			=> 'required',	
			'data_inicial'	   		=> 'date_format:"Y-m-d"',
			'data_vistoria'			=> 'date_format:"Y-m-d"',
			// 'unidade'			=> 'required',
			// 'data'				=> 'required|date_format:"Y-m-d"',
		];
	}

}
