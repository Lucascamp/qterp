<?php

Route::get('/', 'WelcomeController@index');
Route::get('/logout',function(){
	Auth::logout();
	return Redirect::away('/');
});
Route::get('/login',function(){
	Auth::logout();
	return Redirect::away('/');
});

include('rotas/rotasensaio.php');
include('rotas/rotasadm.php');
include('rotas/rotaspatrimonio.php');
include('rotas/rotasradio.php');
include('rotas/rotasrh.php');

Route::get('home',  array('as' => 'home', 'uses' => 'HomeController@index'));

Route::post('/arquivoferias', array('as' => 'arquivo.ferias', 'uses' => 'HomeController@arquivoferias'));

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
