<?php 
Route::get('/rh', 'rh\RhController@index');

/* Funcoes */
Route::get('/funcao',  array('as' => 'funcao.index', 'uses' => 'rh\FuncaoController@index'));

Route::get('/funcao/cadastrar',array('as' => 'funcao.cadastrar', 'uses' => 'rh\FuncaoController@cadastrar'));
Route::post('/funcao', array('as' => 'funcao.salvar', 'uses' => 'rh\FuncaoController@salvar'));

Route::get('/funcao/{id}/editar',array('as' => 'funcao.editar', 'uses' => 'rh\FuncaoController@editar'));	
Route::patch('/funcao/{id}', array('as' => 'funcao.atualizar','uses' => 'rh\FuncaoController@atualizar'));

Route::get('/funcao/{id}/perfil',array('as' => 'funcao.perfil', 'uses' => 'rh\FuncaoController@perfil'));

/* Asos */
Route::get('/aso',  array('as' => 'aso.index', 'uses' => 'rh\AsoController@index'));

Route::get('/aso/cadastrar',array('as' => 'aso.cadastrar', 'uses' => 'rh\AsoController@cadastrar'));
Route::post('/aso', array('as' => 'aso.salvar', 'uses' => 'rh\AsoController@salvar'));

Route::get('/aso/{id}/editar',array('as' => 'aso.editar', 'uses' => 'rh\AsoController@editar'));	
Route::patch('/aso/{id}', array('as' => 'aso.atualizar','uses' => 'rh\AsoController@atualizar'));


