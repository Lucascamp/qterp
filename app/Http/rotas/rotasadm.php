<?php 
Route::when('*', 'csrf', array('post'));
Route::get('/adm', 'adm\AdmController@index');

Route::get('/adm/modulos', 'adm\ModulosAdmController@index');
Route::get('/adm/modulos/cadastro', 'adm\ModulosAdmController@cadastro');
Route::get('/adm/modulos/editar', 'adm\ModulosAdmController@editar');
Route::get('/adm/modulos/editar/{id}', 'adm\ModulosAdmController@editarModulo');
Route::post('/adm/modulos/create', array('as'=>'modulos.create','uses'=>'adm\ModulosAdmController@salvarModulo'));
Route::post('/adm/modulos/listarmodulos', array('as'=>'modulos.listar','uses'=>'adm\ModulosAdmController@listarModulos'));
Route::post('/adm/modulos/alterarModulo','adm\ModulosAdmController@alterarModulo');
Route::post('/adm/modulos/salvararea','adm\ModulosAdmController@salvarArea');
Route::post('/adm/modulos/alterararea','adm\ModulosAdmController@alterarArea');
Route::post('/adm/modulos/excluirarea','adm\ModulosAdmController@excluirArea');
Route::post('/adm/modulos/listarpermissoes','adm\ModulosAdmController@listarPermissoes');
Route::post('/adm/modulos/salvarpermissao','adm\ModulosAdmController@salvarPermissoes');
Route::post('/adm/modulos/alterarpermissao','adm\ModulosAdmController@alterarPermissoes');
Route::post('/adm/modulos/excluirpermissao','adm\ModulosAdmController@excluirPermissoes');



Route::get('/adm/ensaio/jornada', 'adm\EnsaioAdmController@jornada');
Route::post('/adm/ensaio/jornada/cadastrar','adm\EnsaioAdmController@salvarJornada');
Route::post('/adm/ensaio/jornada/alterar','adm\EnsaioAdmController@alterarJornada');
Route::post('/adm/ensaio/jornada/excluir','adm\EnsaioAdmController@excluirJornada');

Route::get('/adm/ensaio/filmes', 'adm\EnsaioAdmController@filmes');
Route::post('/adm/ensaio/filmes/cadastrar','adm\EnsaioAdmController@salvarFilme');
Route::post('/adm/ensaio/filmes/alterar','adm\EnsaioAdmController@alterarFilme');
Route::post('/adm/ensaio/filmes/excluir','adm\EnsaioAdmController@excluirFilme');

Route::get('/adm/ensaio/juntas', 'adm\EnsaioAdmController@juntas');
Route::post('/adm/ensaio/juntas/cadastrar','adm\EnsaioAdmController@salvarJunta');
Route::post('/adm/ensaio/juntas/alterar','adm\EnsaioAdmController@alterarJunta');
Route::post('/adm/ensaio/juntas/excluir','adm\EnsaioAdmController@excluirJunta');


Route::get('/adm/ensaio/servicos', 'adm\EnsaioAdmController@servicos');
Route::post('/adm/ensaio/servicos/cadastrar','adm\EnsaioAdmController@salvarServico');
Route::post('/adm/ensaio/servicos/alterar','adm\EnsaioAdmController@alterarServico');
Route::post('/adm/ensaio/servicos/excluir','adm\EnsaioAdmController@excluirServico');

Route::get('/adm/ensaio/descontinuidades', 'adm\EnsaioAdmController@descontinuidades');
Route::post('/adm/ensaio/descontinuidades/cadastrar','adm\EnsaioAdmController@salvarDescontinuidade');
Route::post('/adm/ensaio/descontinuidades/alterar','adm\EnsaioAdmController@alterarDescontinuidade');
Route::post('/adm/ensaio/descontinuidades/excluir','adm\EnsaioAdmController@excluirDescontinuidade');

Route::get('/adm/ensaio/procedimentos', 'adm\EnsaioAdmController@procedimentos');



Route::get('/adm/user/cadastrar', 'adm\UsuarioAdmController@cadastrar');
Route::post('/adm/user/salvarusuario', 'adm\UsuarioAdmController@salvarUsuario');
Route::get('/adm/user/editar', 'adm\UsuarioAdmController@listaEditar');
Route::post('/adm/user/listarusuarios', 'adm\UsuarioAdmController@listarUsuarios');
Route::get('/adm/user/editar/{id}', 'adm\UsuarioAdmController@editarUsuario');
Route::post('/adm/user/alterarusuario', 'adm\UsuarioAdmController@alterarUsuario');


Route::get('/intranet/administrativo', 'adm\AdmController@indexoperacional');

Route::get('/teste/db', 'adm\AdmController@teste');



Route::get('/adm/trocarsenha', 'adm\UsuarioAdmController@trocarsenha');
Route::post('/adm/salvarnovasenha', 'adm\UsuarioAdmController@salvarnovasenha');

Route::get('/adm/sincronizarusuarios', 'Sincronizacao@SincronizacaoUsuarios');
Route::get('/adm/sincronizarmodulos', 'Sincronizacao@SincronizacaoModulos');
Route::get('/adm/sincronizarunidades', 'Sincronizacao@SincronizacaoUnidades');

Route::post('/adm/excluirUsuario', 'adm\UsuarioAdmController@excluirUsuario');


Route::post('/adm/monitor/verificastatus', 'adm\AdmController@verificaStatus');
Route::post('/adm/monitor/verificabackup', 'adm\AdmController@verificaBackup');
Route::post('/adm/monitor/verificacriardump', 'adm\AdmController@verificaCriaDump');
Route::post('/adm/monitor/verificacriarbackup', 'adm\AdmController@verificaCriaBackup');
Route::post('/adm/monitor/verificasincgama', 'adm\AdmController@verificaSincGama');
Route::post('/adm/monitor/verificasincdados', 'adm\AdmController@verificaSincDados');
Route::post('/adm/monitor/verificaerros', 'adm\AdmController@verificaErros');


Route::get('/adm/gamainfos', 'adm\AdmController@infos');
Route::get('/adm/almoxarifadoinfos', 'adm\AdmController@almoxarifadoinfos');