<?php 
Route::get('/patrimonio', 'patrimonio\PatrimonioController@index');

/* Equipamentos */
Route::get('/equipamento',  array('as' => 'equipamento.index', 'uses' => 'patrimonio\EquipamentoController@equipamentos'));
Route::get('/equipamento/cadastrar',array('as' => 'equipamento.cadastrar', 'uses' => 'patrimonio\EquipamentoController@cadastrar'));
Route::post('/equipamento', array('as' => 'equipamento.salvar', 'uses' => 'patrimonio\EquipamentoController@salvar'));
Route::get('/equipamento/{equipamento}/editar',array('as' => 'equipamento.editar', 'uses' => 'patrimonio\EquipamentoController@editar'));	
Route::patch('/equipamento/{equipamento}', array('as' => 'equipamento.atualizar','uses' => 'patrimonio\EquipamentoController@atualizar'));
Route::post('/equipamento/movimentar', array('as' => 'equipamento.movimentar', 'uses' => 'patrimonio\EquipamentoController@movimentar'));
Route::post('/excluir/{equipamento}', array('as' => 'equipamento.destroy','uses' => 'patrimonio\EquipamentoController@destroy'));

Route::post('/equipamento/movimentar_equipamento', 'patrimonio\EquipamentoController@movimentar_equipamento');
Route::post('/equipamento/receber_equipamento', 'patrimonio\EquipamentoController@receber_equipamento');
Route::post('/equipamento/carregartable', 'patrimonio\EquipamentoController@carregarTable');
Route::post('/equipamento/enviar_manutencao', 'patrimonio\EquipamentoController@enviar_manutencao');



Route::get('equipamentos/filtro', 'patrimonio\EquipamentoController@index');
Route::post('equipamentos/filtro', array('as' => 'equipamentos.filtro', 'uses' => 'patrimonio\EquipamentoController@index'));

Route::post('/dadosDescricao','patrimonio\EquipamentoController@dadosDescricao');

Route::post('/receber','patrimonio\EquipamentoController@receber');

Route::post('/modalHistorico','patrimonio\EquipamentoController@carregarHistoricoModal');

Route::get('equipamentos/excel', array('as' => 'equipamentos.excel', 'uses' => 'patrimonio\EquipamentoController@excel'));

//Route::get('/equipamento/duplicados',array('as' => 'equipamento.duplicados', 'uses' => 'patrimonio\EquipamentoController@duplicados'));

/* Calibração */

Route::get('/calibracao',  array('as' => 'calibracao.index', 'uses' => 'patrimonio\CalibracaoController@index'));
Route::get('/calibracao/{equipamento}/cadastrar',array('as' => 'calibracao.cadastrar', 'uses' => 'patrimonio\CalibracaoController@cadastrar'));
Route::post('/calibracao', array('as' => 'calibracao.salvar', 'uses' => 'patrimonio\CalibracaoController@salvar'));
Route::get('/calibracao/{equipamento}/editar',array('as' => 'calibracao.editar', 'uses' => 'patrimonio\CalibracaoController@editar'));	
Route::patch('/calibracao/{calibracao}', array('as' => 'calibracao.atualizar','uses' => 'patrimonio\CalibracaoController@atualizar'));

Route::post('/uso', array('as' => 'calibracao.updateUso', 'uses' => 'patrimonio\CalibracaoController@updateUso'));

Route::get('calibracao/filtro', 'patrimonio\CalibracaoController@index');
Route::post('calibracao/filtro', array('as' => 'calibracao.filtro', 'uses' => 'patrimonio\CalibracaoController@index'));

Route::post('/modalHistoricoCalib','patrimonio\CalibracaoController@carregarHistoricoModal');

/* Almoxarifado */

Route::get('/almoxarifado',  array('as' => 'almoxarifado.index', 'uses' => 'patrimonio\AlmoxarifadoController@index'));
Route::get('almoxarifado/filtro', 'patrimonio\AlmoxarifadoController@index');
Route::post('almoxarifado/filtro', array('as' => 'almoxarifado.filtro', 'uses' => 'patrimonio\AlmoxarifadoController@index'));

Route::get('/almoxarifado/relatorioconsumo', 'patrimonio\AlmoxarifadoController@relatorioconsumo');
Route::post('/almoxarifado/carregatabelaconsumo', 'patrimonio\AlmoxarifadoController@carregatabelaconsumo');





Route::post('/itemCadastrar', array('as' => 'item.salvar', 'uses' => 'patrimonio\AlmoxarifadoController@salvarItem'));

Route::post('/adicionar', 'patrimonio\AlmoxarifadoController@adicionarItem');
Route::post('/subtrair', 'patrimonio\AlmoxarifadoController@subtrairItem');

Route::post('/totalTransf', 'patrimonio\AlmoxarifadoController@totalTransf');
Route::post('/transferir', 'patrimonio\AlmoxarifadoController@transferirItem');

Route::post('/additmalm', 'patrimonio\AlmoxarifadoController@additmalm');
Route::post('/additmalmsave', 'patrimonio\AlmoxarifadoController@additmalmsave');

Route::get('/almoxarifado/historico', array('as' => 'almoxarifado.historico', 'uses' => 'patrimonio\AlmoxarifadoController@historico'));
Route::post('/almoxarifado/historico','patrimonio\AlmoxarifadoController@carregarHistorico');
Route::post('/dadosItemUnidade','patrimonio\AlmoxarifadoController@dadosItemUnidade');

/* Frota */

Route::get('/frota',  array('as' => 'frota.index', 'uses' => 'patrimonio\FrotaController@index'));
Route::get('/frota/cadastrar',array('as' => 'frota.cadastrar', 'uses' => 'patrimonio\FrotaController@cadastrar'));
Route::post('/frota', array('as' => 'frota.salvar', 'uses' => 'patrimonio\FrotaController@salvar'));

Route::get('/frota/{carro}/editar',array('as' => 'frota.editar', 'uses' => 'patrimonio\FrotaController@editar'));	
Route::patch('/frota/{carro}', array('as' => 'frota.atualizar','uses' => 'patrimonio\FrotaController@atualizar'));

Route::post('/frota/movimentar', array('as' => 'frota.movimentar', 'uses' => 'patrimonio\FrotaController@movimentar'));
Route::post('/excluirveiculo', array('as' => 'frota.destroy','uses' => 'patrimonio\FrotaController@destroy'));

Route::post('/modalFrotaInfo','patrimonio\FrotaController@infoFrota');


Route::post('/patrimonio/sincronizacaoequipamentos','Sincronizacao@SincronizacaoReceberEquipamentos');
Route::post('/patrimonio/sincronizacaolocequipamentos','Sincronizacao@SincronizacaoEnviarLocEquipamentos');
Route::post('/patrimonio/sincronizacaoalmoxarifado','Sincronizacao@SincronizacaoReceberAlmoxarifado');
Route::post('/patrimonio/sincronizacaohistoricoalmoxarifado','Sincronizacao@SincronizacaoEnviarHistoricoAlmoxarifado');

Route::post('/patrimonio/sincronizacaofrota','Sincronizacao@SincronizacaoFrota');




// Route::get('almoxarifado/filtro', 'patrimonio\AlmoxarifadoController@index');
// Route::post('almoxarifado/filtro', array('as' => 'almoxarifado.filtro', 'uses' => 'patrimonio\AlmoxarifadoController@index'));

