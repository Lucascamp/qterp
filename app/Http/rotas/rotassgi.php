<?php 
Route::when('*', 'csrf', array('post'));
Route::get('/sgi', 'sgi\sgiController@index');
Route::get('/sgi/inspecoesauditorias', 'sgi\sgiController@inspecoesauditorias');

Route::post('/sgi/inspecoesauditorias/salvar_formulario', 'sgi\sgiController@salvar_formulario');
Route::post('/sgi/inspecoesauditorias/carregar_formulario_edt', 'sgi\sgiController@carregar_formulario_edt');

Route::get('/sgi/formulario/{id}', 'sgi\sgiController@visualizar_formulario');
Route::post('/sgi/inspecoesauditorias/salvar_formulario_unidade', 'sgi\sgiController@salvarFormularioUnidade');

Route::get('sgi/inspecoesauditorias/formularios', 'sgi\sgiController@formularios');
Route::get('/sgi/formulariop/{id}', 'sgi\sgiController@visualizar_formulario_preenchido');

Route::post('/sgi/inspecoesauditorias/carregar_tabela_preenchido', 'sgi\sgiController@carregar_tabela_preenchido');

Route::post('/sgi/inspecoesauditorias/excluir_formulario', 'sgi\sgiController@excluir_formulario');

Route::get('/sgi/inspecoesauditorias/calendario', 'sgi\sgiController@calendario');
Route::post('/sgi/inspecoesauditorias/salvar_marcacao', 'sgi\sgiController@salvar_marcacao');

Route::post('/sgi/inspecoesauditorias/carregar_marcacao', 'sgi\sgiController@carregar_marcacao');
Route::post('/sgi/inspecoesauditorias/excluir_marcacao', 'sgi\sgiController@excluir_marcacao');

Route::post('/sgi/inspecoesauditorias/carregar_tabela_marcacao', 'sgi\sgiController@carregar_tabela_marcacao');

Route::get('/sgi/formulario/{id}/{marcacao}', 'sgi\sgiController@visualizar_formulario');

