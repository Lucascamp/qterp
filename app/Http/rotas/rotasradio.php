<?php 
Route::get('/radioprotecao', 'radioprotecao\RadioprotecaoController@index');

/* Equipamentos */
Route::get('/fonte',  array('as' => 'fonte.index', 'uses' => 'radioprotecao\FonteController@index'));
Route::get('/fonte/cadastrar',array('as' => 'fonte.cadastrar', 'uses' => 'radioprotecao\FonteController@cadastrar'));
Route::post('/fonte', array('as' => 'fonte.salvar', 'uses' => 'radioprotecao\FonteController@salvar'));
Route::get('/fonte/{fonte}/editar',array('as' => 'fonte.editar', 'uses' => 'radioprotecao\FonteController@editar'));	
Route::patch('/fonte/{fonte}', array('as' => 'fonte.atualizar','uses' => 'radioprotecao\FonteController@atualizar'));
Route::post('/fonte/movimentar', array('as' => 'fonte.movimentar', 'uses' => 'radioprotecao\FonteController@movimentar'));

Route::post('fonte/excluir/{fonte}', array('as' => 'fonte.destroy','uses' => 'radioprotecao\FonteController@destroy'));

Route::get('/fonte/folha',  array('as' => 'fonte.folha', 'uses' => 'radioprotecao\FonteController@folha'));

Route::get('fonte/filtro', 'radioprotecao\FonteController@index');
Route::post('fonte/filtro', array('as' => 'fonte.filtro', 'uses' => 'radioprotecao\FonteController@index'));

Route::get('folha/filtro', 'radioprotecao\FonteController@folha');
Route::post('folha/filtro', array('as' => 'folha.filtro', 'uses' => 'radioprotecao\FonteController@folha'));

Route::post('/modalHistoricoFonte','radioprotecao\FonteController@carregarHistoricoModal');

Route::post('/fonte/calculo', array('as' => 'fonte.calculo', 'uses' => 'radioprotecao\FonteController@calculo'));
Route::post('/fonteDados','radioprotecao\FonteController@fonteDados');



Route::get('/fontesporfrente', 'radioprotecao\RadioprotecaoController@relatorio_fontes_frente');
Route::get('/fontesporfrente/gerar', 'radioprotecao\RadioprotecaoController@gerar_relatorio');
Route::post('/fontesporfrente/carregarrelatorio', 'radioprotecao\RadioprotecaoController@carregar_relatorio');