<?php 
Route::get('/ensaio',  array('as' => 'ensaio', 'uses' => 'ensaio\EnsaioController@index'));

/* Propostas */
Route::get('/ensaio/proposta', 'ensaio\PropostaController@index');
Route::get('/proposta/cadastrar', 'ensaio\PropostaController@cadastrar');
Route::post('/proposta/salvar','ensaio\PropostaController@salvarProposta');
Route::post('/proposta/dadoscliente','ensaio\PropostaController@dadosCliente');
Route::post('/proposta/getpropostas', 'ensaio\PropostaController@getPropostas');
Route::get('/proposta/editarproposta/{id}', 'ensaio\PropostaController@editarProposta');

/* Guias */
Route::get('/ensaio/guia',  array('as' => 'guia.index', 'uses' => 'ensaio\GuiaController@index'));
Route::post('/guia/getguias','ensaio\GuiaController@getguias');
Route::get('/guia/cadastrar',array('as' => 'guia.cadastrar', 'uses' => 'ensaio\GuiaController@cadastrar'));
Route::post('/guia/dadoscliente','ensaio\GuiaController@dadosCliente');
Route::post('/guia/dadosproposta','ensaio\GuiaController@dadosPropostas');
Route::post('/guia/dadosfonte','ensaio\GuiaController@dadosFonte');
Route::post('/guia', array('as' => 'guia.salvar', 'uses' => 'ensaio\GuiaController@salvar'));
Route::post('/guia/dadosProjeto','ensaio\GuiaController@dadosProjeto');

Route::post('/guia/salvarguia','ensaio\GuiaController@salvarGuia');
Route::post('/guia/carregar', array('as' => 'guia.carregar', 'uses' => 'ensaio\GuiaController@carregar'));
Route::post('/guia/carregarprocedimento', array('as' => 'guia.carregarprocedimento', 'uses' => 'ensaio\GuiaController@carregarProcedimento'));

Route::get('/guia/manual',array('as' => 'guia.manual', 'uses' => 'ensaio\GuiaController@gomanual'));
Route::post('/guia/salvarmanual','ensaio\GuiaController@salvarmanual');

Route::get('/guia/{guia}/editar',array('as' => 'guia.editar', 'uses' => 'ensaio\GuiaController@editar'));
Route::patch('/guia/{guia}', array('as' => 'guia.atualizar','uses' => 'ensaio\GuiaController@atualizar'));

Route::get('/guia/{guia}/visualizar',array('as' => 'guia.visualizar', 'uses' => 'ensaio\GuiaController@visualizar'));
Route::get('/guia/{guia}/incluirdados',array('as' => 'guia.incluirdados', 'uses' => 'ensaio\GuiaController@incluirdados'));
Route::get('/guia/{guia}/incluirresultados',array('as' => 'guia.incluirresultados', 'uses' => 'ensaio\GuiaController@incluirresultados'));
Route::post('/guia/{guia}/gonaoreal',array('as' => 'guia.gonaoreal', 'uses' => 'ensaio\GuiaController@gonaoreal'));
Route::post('/guia/salvardadosretorno','ensaio\GuiaController@salvardadosretorno');
Route::post('/guia/salvardadosresultado','ensaio\GuiaController@salvardadosresultado');
Route::get('/guia/{guia}/capa',array('as' => 'guia.capa', 'uses' => 'ensaio\GuiaController@capa'));
Route::get('/guia/{guia}/etiqueta',array('as' => 'guia.etiqueta', 'uses' => 'ensaio\GuiaController@etiqueta'));

Route::get('/guia/{guia}/rdo',array('as' => 'guia.rdo', 'uses' => 'ensaio\GuiaController@rdo'));
Route::get('/guia/{guia}/relatorio',array('as' => 'guia.relatorio', 'uses' => 'ensaio\GuiaController@relatorio'));
Route::get('/guia/{guia}/criarrelatorio',array('as' => 'guia.relatorio', 'uses' => 'ensaio\GuiaController@novorel'));
Route::post('/guia/getdadosrelatorios','ensaio\GuiaController@getdadosrelatorios');
Route::post('/guia/getprogramacoesrelatorios','ensaio\GuiaController@getprogramacoesrelatorios');
Route::post('/guia/salvarNovoRelatorio','ensaio\GuiaController@salvarNovoRelatorio');


Route::get('/guia/cadastrolayout','ensaio\GuiaController@cadastroLayout');
Route::get('/guia/cadastrarlayout','ensaio\GuiaController@cadastrarLayout');
Route::post('/guia/salvarlayout','ensaio\GuiaController@salvarLayout');
Route::get('/guia/{id}/editarlayout','ensaio\GuiaController@editarLayout');
Route::post('/guia/excluirlayout','ensaio\GuiaController@excluirLayout');

/* boletim mensal */
Route::get('/ensaio/boletim', 'ensaio\BoletimController@index');
Route::get('/boletim/criar','ensaio\BoletimController@criar');
Route::post('/boletim/dadosproposta','ensaio\BoletimController@dadosPropostas');
Route::post('/boletim/carregarBM','ensaio\BoletimController@carregarBM');
Route::post('/boletim/salvar','ensaio\BoletimController@salvar');
Route::post('/boletim/carregar','ensaio\BoletimController@carregar');
Route::get('/boletim/{bm}/visualizar',array('as' => 'boletim.visualizar', 'uses' => 'ensaio\BoletimController@visualizar'));


/* Procedimentos */
Route::get('/ensaio/procedimento', array('as' => 'procedimento.home', 'uses' => 'ensaio\ProcedimentoController@index'));
Route::get('/procedimento/criar', 'ensaio\ProcedimentoController@criar');
Route::post('/procedimento/salvarprocedimento', 'ensaio\ProcedimentoController@salvarProcedimento');
Route::post('/procedimento/salvargrupo', 'ensaio\ProcedimentoController@salvarGrupo');
Route::post('/procedimento/dadosnormas','ensaio\ProcedimentoController@dadosnormas');
Route::post('/procedimento/dadosparametro','ensaio\ProcedimentoController@dadosparametro');
Route::post('/procedimento/salvarnorma','ensaio\ProcedimentoController@salvarNorma');
Route::post('/procedimento/salvarparametroprocedimento','ensaio\ProcedimentoController@salvarparametroprocedimento');


Route::get('/procedimento/{id}/excluir','ensaio\ProcedimentoController@excluir');


Route::get('/guia/filmereparo','ensaio\GuiaController@filmereparo');
Route::get('/guia/{id}/gofilmereparo','ensaio\GuiaController@gofilmereparo');
Route::post('/guia/salvarguiafr','ensaio\GuiaController@salvarguiafr');
Route::post('/guia/getnormas','ensaio\GuiaController@getdadosnormas');

Route::post('/guia/salvarrelatorio','ensaio\GuiaController@salvarrelatorio');
Route::post('/guia/getRelatorios','ensaio\GuiaController@getRelatorios');
Route::post('/guia/salvarrelatorioeditado','ensaio\GuiaController@salvarrelatorioeditado');


Route::post('/gamagrafia/sincronizacaogama','Sincronizacao@SincronizacaoGamagrafia');
Route::post('/gamagrafia/sincronizacaofontes','Sincronizacao@SincronizacaoFontes');
Route::post('/gamagrafia/sincronizacaodados','Sincronizacao@SincronizacaoDados');

Route::post('/guia/carregaretiquetas','ensaio\GuiaController@carregarEtiquetas');
Route::post('/guia/getguiamanual','ensaio\GuiaController@getGuiaManual');

Route::post('/guia/getfontes','ensaio\GuiaController@getfontes');
