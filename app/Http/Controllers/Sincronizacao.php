<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class Sincronizacao extends Controller {

	public function __construct()
    {
        $this->middleware('auth');		
    }

    public function getUnidades(){
		return array('RN','PE','MC','BA','MG');
	}

	//metodos admin roda sincronia dos dados nas unidades com servidores
	public function SincronizacaoUsuarios(){
		$unidades = $this->getUnidades();
		// $unidades = array('MG');
		try{
			$usuarios= \DB::connection('CENTRAL')->table('usuarios')->get();
			$permissoes = \DB::connection('CENTRAL')->table('permissaousuarios')->get();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Conectado para sincronizar usuarios','unidade'=>'CENTRAL'));
		}catch(\Exception $e) {
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO: '.$e,'unidade'=>'CENTRAL'));
			return \Redirect::away('/adm');
		}
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];
			try{
				\DB::connection($un)->table('usuarios')->truncate();
				foreach ($usuarios as $key => $usuario) {
					$insert = (array) $usuario;
					\DB::connection($un)->table('usuarios')->insert($insert);
				}
				\DB::connection($un)->table('permissaousuarios')->truncate();
				foreach ($permissoes as $key => $permissao) {
					$insert = (array) $permissao;
					\DB::connection($un)->table('permissaousuarios')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO: '.$e,'tempo_total'=>$tempo,'unidade'=>$un));
				continue;
			}

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc Usuarios concluido con sucesso','tempo_total'=>$tempo,'unidade'=>$un));
		}
		return \Redirect::away('/adm');
	}
	
	public function SincronizacaoModulos(){
		$unidades = $this->getUnidades();
		// $unidades = array('MG');
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			$Modulos      = \DB::connection('CENTRAL')->table('modulos')->get();
			$AreasModulos = \DB::connection('CENTRAL')->table('areasmodulos')->get();
			$Permissoes   = \DB::connection('CENTRAL')->table('permissoes')->get();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Conectado para sincronizar Mod,AreasMod,Perm','unidade'=>'CENTRAL'));
		}catch(\Exception $e) {
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO: '.$e,'unidade'=>'CENTRAL'));
			return \Redirect::away('/adm');
		}
		foreach ($unidades as $key => $un) {
			$msg = '';
			try{
				\DB::connection($un)->table('modulos')->truncate();
				foreach ($Modulos as $key => $modulo) {
					$insert = (array) $modulo;
					\DB::connection($un)->table('modulos')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO Modulos: '.$e,'tempo_total'=>$tempo,'unidade'=>$un));
				continue;
			}
			$msg .= 'Mod., ';
			try{
				\DB::connection($un)->table('areasmodulos')->truncate();
				foreach ($AreasModulos as $key => $areasmodulo) {
					$insert = (array) $areasmodulo;
					\DB::connection($un)->table('areasmodulos')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO AreasModulos: '.$e,'tempo_total'=>$tempo,'unidade'=>$un));
				continue;
			}
			$msg .= 'AreasMod., ';
			try{
				\DB::connection($un)->table('permissoes')->truncate();
				foreach ($Permissoes as $key => $permissoes) {
					$insert = (array) $permissoes;
					\DB::connection($un)->table('permissoes')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO Permissoes: '.$e,'tempo_total'=>$tempo,'unidade'=>$un));
				continue;
			}
			$msg .= 'Perm.';

			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);

			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc '.$msg.' concluido con sucesso','tempo_total'=>$tempo,'unidade'=>$un));
		}
		return \Redirect::away('/adm');
	}

	public function SincronizacaoUnidades(){
		$unidades = $this->getUnidades();
		// $unidades = array('MG');
		try{
			$Uni      = \DB::connection('CENTRAL')->table('unidade')->get();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Conectado para sincronizar Unidades','unidade'=>'CENTRAL'));
		}catch(\Exception $e) {
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO: '.$e,'unidade'=>'CENTRAL'));
			return \Redirect::away('/adm');
		}
		foreach ($unidades as $key => $un) {
			$sec = explode(" ",microtime());
			$tempo_ini= $sec[1] + $sec[0];
			try{
				\DB::connection($un)->table('unidade')->truncate();
				foreach ($Uni as $key => $u) {
					$insert = (array) $u;
					\DB::connection($un)->table('unidade')->insert($insert);
				}
			}catch(\Exception $e) {
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>'ERRO unidade: '.$e,'tempo_total'=>$tempo,'unidade'=>$un));
				continue;
			}
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);

			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc unidades concluido con sucesso','tempo_total'=>$tempo,'unidade'=>$un));
		}
		return \Redirect::away('/adm');
	}



	//metodos de sincronia com o banco central. atualiza o notebook
	public function SincronizacaoGamagrafia(){
		$success = true;
		$unidade = \Auth::user()->unidade;
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		$this->dadosGamagrafia($unidade);

		$backupfile = public_path().'/dumps/'.'dump_gama'.str_random(6).'_'.date("Ymd").'.sql';
		try{
			system("mysqldump -h localhost -uroot -proot".
				" --lock-tables qterp ".
				"equipes ".
				"guiaproducao ".
				"guias ".
				"programacoes ".
				"gorelatorios ".
				"sequencialunidade ".
				"sequencialfilmes ".
				"bm ".
				"obsmanual ".
				"relatorios ".
				"programacaorelatorios ".
				"projetos ".
				"proposta_filme ".
				"proposta_jornada ".
				"proposta_junta ".
				"proposta_servico ".
				"propostas ".
				"> $backupfile");
		}catch(\Exception $e) {
			$msg = 'ERRO: '.$e->getMessage();
			$success = false;
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}
		ini_set('memory_limit', '256M');
		$dump = file_get_contents($backupfile);
		if(strlen($dump) > 0){
			try{
				$aux = strstr($dump, '-- Dump completed on ');
				if(strlen($aux) > 0){
					$status_dump = 'OK';
				}else{
					$status_dump = 'DUMP INCOMPLETO';
				}
				\DB::connection('LOG')->table('dumps')->insert(array('dump'=>$dump,'unidade' => $unidade,'tipo' =>'GAMAGRAFIA','quantidade_carac' => strlen($dump), 'dump_status' => $status_dump));
				$msg = 'Dump criado com sucesso '.$unidade;
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade));
			}catch(\Exception $e) {
				$msg = 'ERRO: '.$e->getMessage();
				$success = false;
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
				$data['result'] = 'Erro de conexão com o banco central nenhum dado sincronizado';
				unset($dump);
				return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
			}
		}else{
			$success = false;
			$msg = 'ERRO: dump não criado';
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}			
		unset($dump);
		return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
	}
	public function dadosGamagrafia($unidade){
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Inicio metodo dadosGamagrafia','unidade'=>$unidade));
		try{
			$descontinuidades = \DB::connection('CENTRAL')->table('descontinuidades')->get();
			$filmes = \DB::connection('CENTRAL')->table('filmes')->get();
			$jornadas = \DB::connection('CENTRAL')->table('jornadas')->get();
			$juntas = \DB::connection('CENTRAL')->table('juntas')->get();
			$polegadasmilimetros = \DB::connection('CENTRAL')->table('polegadasmilimetros')->get();
		}catch(\Exception $e) {
			$msg = 'ERRO: '.$e->getMessage();
			$success = false;
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}
		
		$Descontinuidades = \DB::table('descontinuidades')->lists('id');
		$Filmes = \DB::table('filmes')->lists('id');
		$Jornadas = \DB::table('jornadas')->lists('id');
		$Juntas = \DB::table('juntas')->lists('id');
		$Polegadasmilimetros = \DB::table('polegadasmilimetros')->lists('id');
		
		foreach ($descontinuidades as $key => $value) {
			$value = (array) $value;
			$insert = $value;
			if(in_array($value['id'], $Descontinuidades)){
				\DB::table('descontinuidades')->where('id',$value['id'])->update($insert);
				$msg = 'descontinuidades atualizados com sucesso';
			}else{
				\DB::table('descontinuidades')->insert($insert);
				$msg = 'descontinuidades atualizados com sucesso';
			}
		}

		foreach ($filmes as $key => $value) {
			$value = (array) $value;
			$insert = $value;
			if(in_array($value['id'], $Filmes)){
				\DB::table('filmes')->where('id',$value['id'])->update($insert);
				$msg = 'filmes atualizados com sucesso';
			}else{
				\DB::table('filmes')->insert($insert);
				$msg = 'filmes atualizados com sucesso';
			}
		}

		foreach ($jornadas as $key => $value) {
			$value = (array) $value;
			$insert = $value;
			if(in_array($value['id'], $Jornadas)){
				
					\DB::table('jornadas')->where('id',$value['id'])->update($insert);
					$msg = 'jornadas atualizados com sucesso';
				
			}else{
				
					\DB::table('jornadas')->insert($insert);
					$msg = 'jornadas atualizados com sucesso';
				
			}
		}

		foreach ($juntas as $key => $value) {
			$value = (array) $value;
			$insert = $value;
			if(in_array($value['id'], $Juntas)){
				
					\DB::table('juntas')->where('id',$value['id'])->update($insert);
					$msg = 'juntas atualizados com sucesso';
				
			}else{
				
					\DB::table('juntas')->insert($insert);
					$msg = 'juntas atualizados com sucesso';
				
			}
		}

		foreach ($polegadasmilimetros as $key => $value) {
			$value = (array) $value;
			$insert = $value;
			if(in_array($value['id'], $Polegadasmilimetros)){
				
					\DB::table('polegadasmilimetros')->where('id',$value['id'])->update($insert);
					$msg = 'polegadasmilimetros atualizados com sucesso';
				
			}else{
				
					\DB::table('polegadasmilimetros')->insert($insert);
					$msg = 'polegadasmilimetros atualizados com sucesso';
			
			}
		}
		
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Fim metodo dadosGamagrafia','unidade'=>$unidade));

	}

	public function SincronizacaoFontes(){
		$success = true;
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		$unidade = 'OFF-'.\Auth::user()->unidade;
		try{
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Iniciando sincronização fontes','unidade'=>$unidade));
			$fonteCentral= \DB::connection('CENTRAL')->table('fontes')->get();
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}

		$fonteslocal = \App\Fonte::lists('id');
		foreach ($fonteCentral as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $fonteslocal)){
				\DB::table('fontes')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('fontes')->insert($insert);
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Fontes Atualizadas','tempo_total'=>$tempo,'unidade'=>$unidade));


		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		try{
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Iniciando sincronização Local fontes','unidade'=>$unidade));
			$local = \DB::connection('CENTRAL')->table('fontes_localizacao')->get();
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}

		
		$locLocal = \App\Fontes_localizacao::lists('id');
		foreach ($local as $key => $value) {
			$insert = (array) $value;
			if(in_array($value->id, $locLocal)){
				\DB::table('fontes_localizacao')->where('id',$value->id)->update($insert);
			}else{
				\DB::table('fontes_localizacao')->insert($insert);
			}
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Local de Fontes Atualizadas','tempo_total'=>$tempo,'unidade'=>$unidade));
		return \Response::json(array('success' => $success, 'mensagem' =>  'Dados sobre Fontes sincronizados com sucesso'));
	}
	
	public function SincronizacaoReceberEquipamentos(){
		$msg = 'Equipamentos atualizados com sucesso';
		$unidade = 'OFF-'.\Auth::user()->unidade;
		$success = true;
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Iniciando sincronização equipamentos','unidade'=>$unidade));
			$equipamentos = \DB::connection('CENTRAL')->table('equipamentos')->get();
			$locequipamentos = \DB::connection('CENTRAL')->table('equipamento_localizacao')->get();
			$calibracao = \DB::connection('CENTRAL')->table('calibracao')->get();
			$historico = \DB::connection('CENTRAL')->table('equipamento_historico')->get();
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}
		\DB::table('equipamentos')->truncate();
		foreach ($equipamentos as $key => $equip) {
			$insert = (array) $equip;
			\DB::table('equipamentos')->insert($insert);
		}	
		\DB::table('equipamento_localizacao')->truncate();
		foreach ($locequipamentos as $key => $locequip) {
			$insert = (array) $locequip;
			\DB::table('equipamento_localizacao')->insert($insert);
		}
		\DB::table('calibracao')->truncate();
		foreach ($calibracao as $key => $equip) {
			$insert = (array) $equip;
			\DB::table('calibracao')->insert($insert);
		}
		\DB::table('equipamento_historico')->truncate();
		foreach ($historico as $key => $equip) {
			$insert = (array) $equip;
			\DB::table('equipamento_historico')->insert($insert);
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade));
		return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
	}

	public function SincronizacaoEnviarLocEquipamentos(){
		$success = true;
		$unidade = \Auth::user()->unidade;
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		$backupfile = public_path().'/dumps/'.'dump_locequip'.str_random(6).'_'.date("Ymd").'.sql';
		try{
			system("mysqldump -h localhost -uroot -proot".
				" --lock-tables qterp ".
				"equipamento_localizacao ".
				"equipamento_historico".
				"> $backupfile");
		}catch(\Exception $e) {
			$msg = 'ERRO: '.$e->getMessage();
			$success = false;
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}

		ini_set('memory_limit', '256M');
		$dump = file_get_contents($backupfile);
		if(strlen($dump) > 0){
			try{
				$aux = strstr($dump, '-- Dump completed on ');
				if(strlen($aux) > 0){
					$status_dump = 'OK';
				}else{
					$status_dump = 'DUMP INCOMPLETO';
				}
				\DB::connection('LOG')->table('dumps')->insert(array('dump'=>$dump,'unidade' => $unidade,'tipo' =>'EQUIPAMENTOS','quantidade_carac' => strlen($dump), 'dump_status' => $status_dump));
				$msg = 'Dump criado com sucesso '.$unidade;
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade));
			}catch(\Exception $e) {
				$msg = 'ERRO: '.$e->getMessage();
				$success = false;
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade,'tipo' => 'ERRO DUMP EQ'));
				return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
			}
		}else{
			$msg = 'ERRO: dump não criado';
			$success = false;
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade,'tipo' => 'ERRO DUMP EQUIPAMENTO'));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}			
		unset($dump);
		return \Response::json(array('success' => $success, 'mensagem' =>  $msg));	
	}

	public function SincronizacaoReceberAlmoxarifado(){
		$msg = 'Itens atualizados com sucesso';
		$unidade = 'OFF-'.\Auth::user()->unidade;
		$success = true;
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		try{
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Iniciando sincronização Itens','unidade'=>$unidade));
			$itens = \DB::connection('CENTRAL')->table('item_almoxarifado')->get();
			$almo =  \DB::connection('CENTRAL')->table('almoxarifado')->get();
			$almohisto = \DB::connection('CENTRAL')->table('almoxarifado_historico')->get();
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}
		\DB::table('item_almoxarifado')->truncate();
		foreach ($itens as $key => $item) {
			$insert = (array) $item;
			\DB::table('item_almoxarifado')->insert($insert);
		}

		\DB::table('almoxarifado')->truncate();
		foreach ($almo as $key => $item) {
			$insert = (array) $item;
			\DB::table('almoxarifado')->insert($insert);
		}


		\DB::table('almoxarifado_historico')->truncate();
		foreach ($almohisto as $key => $item) {
			$insert = (array) $item;
			\DB::table('almoxarifado_historico')->insert($insert);
		}


		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);
		\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade));
		return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
	}

	public function SincronizacaoEnviarHistoricoAlmoxarifado(){
		$success = true;
		$unidade = \Auth::user()->unidade;
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];
		$backupfile = public_path().'/dumps/'.'dump_locequip'.str_random(6).'_'.date("Ymd").'.sql';
		try{
			system("mysqldump -h localhost -uroot -proot".
				" --lock-tables qterp ".
				"almoxarifado ".
				"almoxarifado_historico".
				"> $backupfile");
		}catch(\Exception $e) {
			$msg = 'ERRO: '.$e->getMessage();
			$success = false;
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}
		ini_set('memory_limit', '256M');
		$dump = file_get_contents($backupfile);
		if(strlen($dump) > 0){
			try{
				$aux = strstr($dump, '-- Dump completed on ');
				if(strlen($aux) > 0){
					$status_dump = 'OK';
				}else{
					$status_dump = 'DUMP INCOMPLETO';
				}
				\DB::connection('LOG')->table('dumps')->insert(array('dump'=>$dump,'unidade' => $unidade,'tipo' =>'ALMOXARIFADO','quantidade_carac' => strlen($dump), 'dump_status' => $status_dump));
				$msg = 'Dump criado com sucesso '.$unidade;
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade));
				unset($dump);
				return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
			}catch(\Exception $e) {
				$msg = 'ERRO: '.$e->getMessage();
				$success = false;
				$sec = explode(" ",microtime());
				$tempo_fim= $sec[1] + $sec[0];
				$tempo = number_format($tempo_fim-$tempo_ini);
				\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade,'tipo' => 'ERRO DUMP'));
				unset($dump);
				return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
			}
		}else{
			$msg = 'ERRO: dump não criado';
			$success = false;
			$sec = explode(" ",microtime());
			$tempo_fim= $sec[1] + $sec[0];
			$tempo = number_format($tempo_fim-$tempo_ini);
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'tempo_total'=>$tempo,'unidade'=>$unidade,'tipo' => 'ERRO DUMP'));
			unset($dump);
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}			
		// ini_set('memory_limit', '256M');
		// $dump = file_get_contents($backupfile);
		// if(strlen($dump) > 0){
		// 	try{
		// 		$aux = strstr($dump, '-- Dump completed on ');
		// 		if(strlen($aux) > 0){
		// 			$status_dump = 'OK';
		// 		}else{
		// 			$status_dump = 'DUMP INCOMPLETO';
		// 		}		
		// 		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sincronização local almoxarifado DUMP','unidade'=>'OFF-'.$unidade));
				
		// 		$partes = intval(ceil(strlen($dump) / 1027326));
		// 		for ($i=0; $i < $partes; $i++) { 
		// 			$ini = 1027326 * $i;
		// 			$fim = 1027326 * ($i + 1);
		// 			if($i == 0){
		// 				$id = \DB::connection('CENTRAL')->table('dumps')
		// 					->insertGetId(
		// 						array(
		// 							'dump'=>substr($dump, $ini,$fim),
		// 							'unidade' => $unidade,
		// 							'tipo' =>'ALMOXARIFADO',
		// 							'quantidade_carac' => strlen(substr($dump, $ini,$fim)), 
		// 							'dump_status' => $status_dump. ' PARTE '.$i
		// 						)
		// 					);
		// 			}else{
		// 				\DB::connection('CENTRAL')->table('dumps')
		// 					->insertGetId(
		// 						array(
		// 							'dump'=>substr($dump, $ini,$fim),
		// 							'unidade' => $unidade,
		// 							'tipo' =>'ALMOXARIFADO',
		// 							'quantidade_carac' => strlen(substr($dump, $ini,$fim)), 
		// 							'dump_status' => $status_dump. ' ID '.$id.' PARTE '.$i
		// 						)
		// 					);
		// 			}

		// 		}
		// 		$msg = 'Dados enviados!';
		// 		unset($dump);
		// 		return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		// 	}catch(\Exception $e) {
		// 		$msg = 'ERRO: '.$e->getMessage();
		// 		$success = false;
		// 		\DB::connection('CENTRAL')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
		// 		$data['result'] = 'Erro de conexão com o banco central nenhum dado sincronizado';
		// 		unset($dump);
		// 		return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		// 	}
		// }else{
		// 	$success = false;
		// 	$msg = 'ERRO: dump não criado';
		// 	\DB::connection('CENTRAL')->table('logs')->insert(array('log'=>$msg,'unidade'=>'OFF-'.$unidade));
		// 	return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		// }
	}


	//sinc dados
	public function sincusuarios(){
		$success = true;
		$unidade = 'OFF-'.\Auth::user()->unidade;
		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		try{
			$usuarios= \DB::connection('CENTRAL')->table('usuarios')->get();
			$permissoes = \DB::connection('CENTRAL')->table('permissaousuarios')->get();
			$permissao_opcoes = \DB::connection('CENTRAL')->table('permissao_opcoes')->get();
			$permissao_opcoes_usuario = \DB::connection('CENTRAL')->table('permissao_opcoes_usuario')->get();
			$funcionarios = \DB::connection('CENTRAL')->table('funcionarios')->get();
			$responsaveis = \DB::connection('CENTRAL')->table('responsaveis_unidade')->get();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Conectado para sincronizar usuarios','unidade'=>$unidade));
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}
		$userlocal = \App\User::lists('id');

		foreach ($usuarios as $key => $usuario) {
			$insert = (array) $usuario;
			if(in_array($usuario->id, $userlocal)){
				\DB::table('usuarios')->where('id',$usuario->id)->update($insert);
			}else{
				\DB::table('usuarios')->insert($insert);
			}
		}
			
		\DB::table('permissaousuarios')->truncate();
		foreach ($permissoes as $key => $permissao) {
			$insert = (array) $permissao;
			\DB::table('permissaousuarios')->insert($insert);
		}

		\DB::table('permissao_opcoes')->truncate();
		foreach ($permissao_opcoes as $key => $permissao) {
			$insert = (array) $permissao;
			\DB::table('permissao_opcoes')->insert($insert);
		}

		\DB::table('permissao_opcoes_usuario')->truncate();
		foreach ($permissao_opcoes_usuario as $key => $permissao) {
			$insert = (array) $permissao;
			\DB::table('permissao_opcoes_usuario')->insert($insert);
		}

		\DB::table('funcionarios')->truncate();
		foreach ($funcionarios as $key => $item) {
			$insert = (array) $item;
			\DB::table('funcionarios')->insert($insert);
		}

		\DB::table('responsaveis_unidade')->truncate();
		foreach ($responsaveis as $key => $item) {
			$insert = (array) $item;
			\DB::table('responsaveis_unidade')->insert($insert);
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc Usuarios concluido com sucesso','tempo_total'=>$tempo,'unidade'=>$unidade));
		return true;
	}

	public function sincmodulos(){
		$success = true;
		$unidade = 'OFF-'.\Auth::user()->unidade;

		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		try{
			$Modulos      = \DB::connection('CENTRAL')->table('modulos')->get();
			$AreasModulos = \DB::connection('CENTRAL')->table('areasmodulos')->get();
			$Permissoes   = \DB::connection('CENTRAL')->table('permissoes')->get();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Conectado para sincronizar Mod,AreasMod,Perm','unidade'=>$unidade));
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}		
			
		\DB::table('modulos')->truncate();
		foreach ($Modulos as $key => $modulo) {
			$insert = (array) $modulo;
			\DB::table('modulos')->insert($insert);
		}

		\DB::table('areasmodulos')->truncate();
		foreach ($AreasModulos as $key => $areamodulo) {
			$insert = (array) $areamodulo;
			\DB::table('areasmodulos')->insert($insert);
		}

		
		\DB::table('permissoes')->truncate();
		foreach ($Permissoes as $key => $permissao) {
			$insert = (array) $permissao;
			\DB::table('permissoes')->insert($insert);
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc modulos concluido','tempo_total'=>$tempo,'unidade'=>$unidade));
		return true;
	}

	public function sincclientes(){
		$success = true;
		$unidade = 'OFF-'.\Auth::user()->unidade;

		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		try{
			$clientes      = \DB::connection('CENTRAL')->table('clientes')->get();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Conectado para sincronizar clientes','unidade'=>$unidade));
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$unidade));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}		
			
		\DB::table('clientes')->truncate();
		foreach ($clientes as $key => $cliente) {
			$insert = (array) $cliente;
			\DB::table('clientes')->insert($insert);
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc clientes concluido','tempo_total'=>$tempo,'unidade'=>$unidade));
		return true;
	}

	public function sincunidades(){
		$success = true;
		$uni = 'OFF-'.\Auth::user()->unidade;

		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		try{
			$unidades      = \DB::connection('CENTRAL')->table('unidade')->get();
			$centro_custo      = \DB::connection('CENTRAL')->table('centro_custo')->get();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>'Conectado para sincronizar unidades','unidade'=>$uni));
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$uni));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}		
			
		\DB::table('unidade')->truncate();
		foreach ($unidades as $key => $unidade) {
			$insert = (array) $unidade;
			\DB::table('unidade')->insert($insert);
		}

		\DB::table('centro_custo')->truncate();
		foreach ($centro_custo as $key => $item) {
			$insert = (array) $item;
			\DB::table('centro_custo')->insert($insert);
		}

		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc unidades concluido','tempo_total'=>$tempo,'unidade'=>$uni));
		return true;
	}

	public function SincronizacaoDados(){
		$success = true;
		if($this->sincusuarios() && $this->sincmodulos() && $this->sincclientes() && $this->sincunidades()){
			return \Response::json(array('success' => $success, 'mensagem' =>  'Dados sincronizados com sucesso'));
		}else{
			$success = false;
			return \Response::json(array('success' => $success, 'mensagem' =>  'ERRO: dados não sincronizados'));
		}
	}

	public function SincronizacaoFrota(){
		$success = true;
		$uni = 'OFF-'.\Auth::user()->unidade;

		$sec = explode(" ",microtime());
		$tempo_ini= $sec[1] + $sec[0];

		try{
			$carro = \DB::connection('CENTRAL')->table('carro')->get();
			$ecofrota = \DB::connection('CENTRAL')->table('carro_ecofrota')->get();
			$imposto = \DB::connection('CENTRAL')->table('carro_imposto')->get();
			$localizacao = \DB::connection('CENTRAL')->table('carro_localizacao')->get();
			$manutencao = \DB::connection('CENTRAL')->table('carro_manutencao')->get();
			$seguro = \DB::connection('CENTRAL')->table('carro_seguro')->get();
		}catch(\Exception $e) {
			$success = false;
			$msg = 'ERRO: '.$e->getMessage();
			\DB::connection('LOG')->table('logs')->insert(array('log'=>$msg,'unidade'=>$uni));
			return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
		}	
		
		\DB::table('carro')->truncate();
		foreach ($carro as $key => $item) {
			$insert = (array) $item;
			\DB::table('carro')->insert($insert);
		}
		
		\DB::table('carro_ecofrota')->truncate();
		foreach ($ecofrota as $key => $item) {
			$insert = (array) $item;
			\DB::table('carro_ecofrota')->insert($insert);
		}
		
		\DB::table('carro_imposto')->truncate();
		foreach ($imposto as $key => $item) {
			$insert = (array) $item;
			\DB::table('carro_imposto')->insert($insert);
		}
		
		\DB::table('carro_localizacao')->truncate();
		foreach ($localizacao as $key => $item) {
			$insert = (array) $item;
			\DB::table('carro_localizacao')->insert($insert);
		}
		
		\DB::table('carro_manutencao')->truncate();
		foreach ($manutencao as $key => $item) {
			$insert = (array) $item;
			\DB::table('carro_manutencao')->insert($insert);
		}
		
		\DB::table('carro_seguro')->truncate();
		foreach ($seguro as $key => $item) {
			$insert = (array) $item;
			\DB::table('carro_seguro')->insert($insert);
		}
		$msg = 'Sinc frotas com sucesso ';
		$sec = explode(" ",microtime());
		$tempo_fim= $sec[1] + $sec[0];
		$tempo = number_format($tempo_fim-$tempo_ini);

		\DB::connection('LOG')->table('logs')->insert(array('log'=>'Sinc Frotas concluido','tempo_total'=>$tempo,'unidade'=>$uni));
		return \Response::json(array('success' => $success, 'mensagem' =>  $msg));
	}

}
