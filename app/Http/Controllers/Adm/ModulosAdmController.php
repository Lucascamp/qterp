<?php namespace App\Http\Controllers\adm;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class ModulosAdmController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function index(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.modulos.home')->with($data);
	}
	public function cadastro(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.modulos.cadastro')->with($data);
	}
	public function editar(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.modulos.listaeditar')->with($data);
	}
	public function editarModulo($id){
		$data['id']     = $id;
		$modulo = \App\Modulo::find($id);
		$data['modulo'] = $modulo;
		// dd($modulo);
		$areas = DB::table('areasmodulos')
                        ->select('*')
                        ->where('modulo_id','=',$id)
                        ->get();
        $data['areas'] = $areas;
        // dd($areas);
        $data['modulos'] = \App\Modulo::all()->toArray();
        $data['usuario'] = Auth::user();
		return view('adm.modulos.editar')->with($data);
	}
	public function salvarModulo(){
		$input = Input::all();
		
		$insert['descricao'] = $input['modulo'];
		$insert['rota'] = $input['rota'];

		$res = \DB::connection('CENTRAL')->table('modulos')->insert($insert);
		\DB::table('modulos')->insert($insert);

		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarModulo(){
		$input = Input::all();
		
		$insert['descricao'] = $input['modulo'];
		$insert['status'] = $input['status'];
		$insert['rota'] = $input['rota'];

		$res = \DB::connection('CENTRAL')->table('modulos')->where('id',$input['id'])->update($insert);
		\DB::table('modulos')->where('id',$input['id'])->update($insert);

		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Alterado com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao alterar'));
		}
	}
	public function listarModulos(){
		$modulos = \App\Modulo::all()->toArray();
		return Response::json(array('modulos'=>$modulos));
	}
	public function salvarArea(){
		$input = Input::all();
		
		$insert['descricao'] = $input['area'];
		$insert['modulo_id'] = $input['id'];
		$insert['rota'] = $input['rota'];
		
		$res = \DB::connection('CENTRAL')->table('areasmodulos')->insert($insert);
		\DB::table('areasmodulos')->insert($insert);
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarArea(){
		$input = Input::all();
		
		$insert['descricao'] = $input['area'];
		$insert['rota'] = $input['rota'];
		
		$res = \DB::connection('CENTRAL')->table('areasmodulos')->where('id',$input['id'])->update($insert);
		\DB::table('areasmodulos')->where('id',$input['id'])->update($insert);
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function excluirArea(){
		$input = Input::all();
		$area = \App\Areasmodulos::find($input['id']);
		$res = $area->delete();
		$res = \DB::connection('CENTRAL')->table('areasmodulos')->where('id',$input['id'])->delete();
		
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao Excluir'));
		}
	}
	public function listarPermissoes(){
		$input = Input::all();
		$permissoes = DB::table('permissoes')
                        ->select('*')
                        ->where('areamodulo_id','=',$input['id'])
                        ->get();
		return Response::json(array('permissoes'=>$permissoes));
	}
	public function salvarPermissoes(){
		$input = Input::all();
		
		$insert['descricao'] = $input['permissao'];
		$insert['areamodulo_id'] = $input['id'];
		$insert['rota'] = $input['rota'];
		
		$res = \DB::connection('CENTRAL')->table('permissoes')->insert($insert);
		\DB::table('permissoes')->insert($insert);
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarPermissoes(){
		$input = Input::all();
		
		$insert['descricao'] = $input['permissao'];
		$insert['rota'] = $input['rota'];

		$res = \DB::connection('CENTRAL')->table('permissoes')->where('id',$input['id'])->update($insert);
		\DB::table('permissoes')->where('id',$input['id'])->update($insert);
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function excluirPermissoes(){
		$input = Input::all();
		$permissao = \App\Permissoes::find($input['id']);
		$permissao->delete();

		$res= \DB::table('permissoes')->where('id',$input['id'])->delete();

		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao Excluir'));
		}
	}
}
