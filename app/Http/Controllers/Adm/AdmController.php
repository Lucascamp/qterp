<?php namespace App\Http\Controllers\adm;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use App\Permissaousuario;
use App\User;
use App\Http\Controllers\Sincronizacao;

use Illuminate\Http\Request;

class AdmController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function index(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['usuario'] = Auth::user();
		$logs = \DB::connection('LOG')->table('logs')->select(DB::raw('MAX(idlogs) as id'))->whereIn('unidade',array('MG','RN','BA','MC','PE'))->groupby('unidade')->get();
		$arraylog = [];
		foreach ($logs as $key => $log) {
			$aux = \DB::connection('LOG')->table('logs')->select('data','unidade','log')->where('idlogs',$log->id)->get();
			$arraylog[] = $aux[0];
		}

		$data['logs'] = $arraylog;
		
		return view('adm.home')->with($data);
	}
	public function indexoperacional(){
		$data['usuario'] = Auth::user();
		$data['modulos'] = \App\Modulo::all()->toArray();
		return view('adm.intranet.dashadm')->with($data);
	}
	
	public function getConfig(){
		$configMG = ['host' => '10.155.64.9','serv' => 'MG',];
		$configBA = ['host' => '10.155.65.5','serv' => 'BA',];
		$configRN = ['host' => '10.155.65.72','serv' => 'RN',];
		$configMC = ['host' => '10.166.3.4','serv' => 'MC',];
		$configPE = ['host' => '10.155.65.132','serv' => 'PE',];
		$configs  = ['MG' => $configMG , 'BA' => $configBA , 'RN' => $configRN , 'MC' => $configMC , 'PE' => $configPE];
		return $configs;
	}

	public function verificaStatus(){
		$success = true;
		$config = $this->getConfig();
		$arraystatus = [];
		foreach ($config as $key => $value) {
			try{
				$fp = @fsockopen($value['host'],80, $errno, $errstr, 5);
				if($fp >= 1){ 
					$status = 1;
				}
			}catch(\Exception $e) {
				$status = 0;
			}

			$arraystatus[$value['serv']] = $status;
		}
		
		return Response::json(array('success' => $success,'arraystatus'=>$arraystatus));
	}

	public function verificaBackup(){
		$back = \DB::connection('LOG')->table('dumps')->select(DB::raw('MAX(data) as data'),'unidade')->where('tipo','BACKUP')->where('dump_status','OK')->groupby('unidade')->orderby('data','desc')->lists('data','unidade');
		return Response::json(array('success' => true,'res'=>$back));
	}

	public function verificaCriaDump(){
		$log = \DB::connection('LOG')->table('logs')->where('log','dump gamagrafia finalizado ')->get();
		return Response::json(array('success' => true,'logs'=>$log));
	}

	public function verificaCriaBackup(){
		$log = \DB::connection('LOG')->table('logs')->where('log','BACKUP finalizado ')->get();
		foreach ($log as $key => $value) {
			$value->tempo_total = str_replace(',', '', $value->tempo_total);
		}		
		return Response::json(array('success' => true,'logs'=>$log));
	}

	public function verificaSincGama(){
		$log = \DB::connection('LOG')->table('logs')->where('log','finalizado sincronizacao gamagrafia')->get();
		foreach ($log as $key => $value) {
			$value->tempo_total = str_replace(',', '', $value->tempo_total);
		}		
		return Response::json(array('success' => true,'logs'=>$log));
	}

	public function verificaSincDados(){
		$log = \DB::connection('LOG')->table('logs')->where('log','Sinc Dados Finalizado')->get();
		foreach ($log as $key => $value) {
			$value->tempo_total = str_replace(',', '', $value->tempo_total);
		}		
		return Response::json(array('success' => true,'logs'=>$log));
	}

	public function verificaErros(){
		$arrayerros = [];
		$arrayerros['backup'] = \DB::connection('LOG')->table('logs')
			->select(DB::raw('MAX(data) as datam'),'logs.*')
			->where('tipo','ERRO BU')->groupby('unidade')->orderby('data','desc')->get();
		$arrayerros['dump'] = \DB::connection('LOG')->table('logs')
			->select(DB::raw('MAX(data) as datam'),'logs.*')
			->where('tipo','ERRO DUMP')->groupby('unidade')->orderby('data','desc')->get();
		$arrayerros['dados'] = \DB::connection('LOG')->table('logs')
			->select(DB::raw('MAX(data) as datam'),'logs.*')
			->where('tipo','ERRO DADOS')->groupby('unidade')->orderby('data','desc')->get();
		return Response::json(array('success' => true,'logs'=>$arrayerros));
	}

	public function teste(){
	
		// $guias = DB::table('guias')->insert(array('unidade'=>'MG'));
		// DB::enableQueryLog();
		// $queries = DB::getQueryLog();

		// dd($queries);
		// var_dump($guias->toSql());
		// echo "string";

		// $backupfile = 'Autobackup_' . date("Ymd") . '.sql';
		// // system("mysqldump -h 10.155.65.5 -uroot -pusbw --lock-tables qterp > $backupfile");
		// system("mysqldump -h localhost -uroot -proot --lock-tables qterp > $backupfile");
		// ini_set('memory_limit', '256M');
		// $dump = file_get_contents($backupfile);
		// \DB::connection('TESTES')->table('teste')->insert(array('dump'=>$dump));
		
			
		// var_dump($dump);
		// unset($dump);
		// exit();

		// system("mysql -h localhost -uroot -proot espelhoMG < $backupfile");
		// var_dump($backupfile);


		// $sinc = new Sincronizacao;
		// $sinc->SincronizacaoGamagrafia();
	}

	public function infos(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['usuario'] = Auth::user();
		$data['countgos'] = \DB::connection('CENTRAL')->table('guias')->select(DB::raw('COUNT(*) as quant,unidade'))->groupby('unidade')->lists('quant','unidade');
		$data['countrels'] = \DB::connection('CENTRAL')->table('relatorios')->select(DB::raw('COUNT(*) as quant,unidade'))->groupby('unidade')->lists('quant','unidade');
		$data['ult_atualizacao'] = \DB::connection('LOG')
			->table('dumps')
			->select(DB::raw('max(data) as data'),'unidade')
			->where('tipo','GAMAGRAFIA')
			->where('status',1)
			->groupby('unidade')
			->lists('data','unidade');

		return view('adm.ensaio.infos')->with($data);
	}

	public function almoxarifadoinfos(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['usuario'] = Auth::user();
		
		$data['unidades'] = \Config::get('app.unidades');
		$data['gasto_serv'] = [];
		$data['ult_atualizacao'] = [];
		foreach ($data['unidades'] as $key => $value) {
			$id_uni = \DB::table('unidade')->where('unidade',$value)->pluck('id');
			if($value != 'PORTALEXT'){
				try {
					$aux = \DB::connection($value)->table('almoxarifado_historico')
						->where('operacao','Subtrair')
						->where('unidade_id',$id_uni)
						->where("updated_at", '>', \DB::raw("DATE_SUB(CONCAT(CURDATE(), ' 00:00:00'), INTERVAL 7 DAY)"))
						->get();
					$data['gasto_serv'][$value] = $aux;
				}catch (\Exception $e) {
					$$data['gasto_serv'][$value] = array('ERRO'=>'ERRO');
					continue;
				}
			
			}
		}
		$data['itens'] = \DB::table('item_almoxarifado')->lists('descricao','id');
		
		// 	try {
		// 		$data['quantidade_itens'][$value] = \DB::connection($value)
		// 		->table('almoxarifado')->where('unidade_id',$id_uni)->get();
		// 		// ->table('almoxarifado')->select(\DB::raw('SUM(total) as quantidade'))->where('unidade_id',$id_uni)->get();

		// 		$data['ult_atualizacao'][$value] = \DB::connection($value)
		// 		->table('almoxarifado_historico')
		// 		->select(DB::raw('max(updated_at) as data'))
		// 		->where('operacao','Subtrair')
		// 		// ->where('unidade_id',$id_uni)
		// 		->pluck('data');
		// 	} catch (\Exception $e) {
		// 		$data['quantidade_itens'][$value] = array('ERRO'=>'ERRO');
		// 		$data['ult_atualizacao'][$value] = array('ERRO'=>'ERRO');
		// 		continue;
		// 	}
			
		

		$unidades = \DB::table('unidade')->get();
		foreach ($unidades as $key => $value) {

			$data['ult_atualizacao_central'][$value->unidade] = \DB::connection('CENTRAL')
				->table('almoxarifado_historico')
				->select(DB::raw('max(updated_at) as data'))
				->where('unidade_id',$value->id)
				->pluck('data');
		}	

	
		return view('adm.almoxarifadoinfo')->with($data);
	}

	
}
