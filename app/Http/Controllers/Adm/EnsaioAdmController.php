<?php namespace App\Http\Controllers\adm;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use App\Permissaousuario;
use App\User;

class EnsaioAdmController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function jornada(){
		$data['modulos']  = \App\Modulo::all()->toArray();
		$data['jornadas'] = \App\Jornada::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.ensaio.jornada')->with($data);
	}
	public function salvarJornada(){
		$input = Input::all();
		$jornada = new \App\Jornada;
		$jornada->descricao = $input['jornada'];
		$res = $jornada->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarJornada(){
		$input = Input::all();
		$jornada = \App\Jornada::find($input['id']);
		$jornada->descricao = $input['jornada'];
		$res = $jornada->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Alterado com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao alterar'));
		}
	}
	public function excluirJornada(){
		$input = Input::all();
		$jornada = \App\Jornada::find($input['id']);
		$res= $jornada->delete();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao Excluir'));
		}
	}


	public function filmes(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['filmes'] = \App\Filme::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.ensaio.filmes')->with($data);
	}
	public function salvarFilme(){
		$input = Input::all();
		$filme = new \App\Filme;
		$filme->descricao = $input['filme'];
		$filme->classe = $input['classe'];
		$res = $filme->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarFilme(){
		$input = Input::all();
		$filme = \App\Filme::find($input['id']);
		$filme->descricao = $input['filme'];
		$filme->classe = $input['classe'];
		$res = $filme->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Alterado com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao alterar'));
		}
	}
	public function excluirFilme(){
		$input = Input::all();
		$filme = \App\Filme::find($input['id']);
		$res= $filme->delete();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao Excluir'));
		}
	}


	public function juntas(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['juntas'] = \App\Junta::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.ensaio.juntas')->with($data);
	}
	public function salvarJunta(){
		$input = Input::all();
		$junta = new \App\Junta;
		$junta->descricao = $input['junta'];
		$res = $junta->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarJunta(){
		$input = Input::all();
		$junta = \App\Junta::find($input['id']);
		$junta->descricao = $input['junta'];
		$res = $junta->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Alterado com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao alterar'));
		}
	}
	public function excluirJunta(){
		$input = Input::all();
		$junta = \App\Junta::find($input['id']);
		$res= $junta->delete();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao Excluir'));
		}
	}


	public function servicos(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['servicos'] = \App\Servico::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.ensaio.servicos')->with($data);
	}
	public function salvarServico(){
		$input = Input::all();
		$servico = new \App\Servico;
		$servico->descricao = $input['servico'];
		$res = $servico->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarServico(){
		$input = Input::all();
		$servico = \App\Servico::find($input['id']);
		$servico->descricao = $input['servico'];
		$res = $servico->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Alterado com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao alterar'));
		}
	}
	public function excluirServico(){
		$input = Input::all();
		$servico = \App\Servico::find($input['id']);
		$res= $servico->delete();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao Excluir'));
		}
	}

	public function procedimentos(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.ensaio.procedimentos')->with($data);
	}

	public function descontinuidades(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['descontinuidades'] = \App\Descontinuidade::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.ensaio.descontinuidades')->with($data);
	}
	public function salvarDescontinuidade(){
		$input = Input::all();
		$descontinuidade = new \App\Descontinuidade;
		$descontinuidade->descricao = $input['descontinuidade'];
		$res = $descontinuidade->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}
	public function alterarDescontinuidade(){
		$input = Input::all();
		$descontinuidade = \App\Descontinuidade::find($input['id']);
		$descontinuidade->descricao = $input['descontinuidade'];
		$res = $descontinuidade->save();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Alterado com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao alterar'));
		}
	}
	public function excluirDescontinuidade(){
		$input = Input::all();
		$descontinuidade = \App\Descontinuidade::find($input['id']);
		$res= $descontinuidade->delete();
		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao Excluir'));
		}
	}
}
