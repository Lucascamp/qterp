<?php namespace App\Http\Controllers\adm;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Sincronizacao;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use App\Permissaousuario;
use App\User;

class UsuarioAdmController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function cadastrar(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['areas'] = \App\Areasmodulos::all()->toArray();
		$data['permissoes'] = \App\Permissoes::all()->toArray();
		$data['usuario'] = Auth::user();
		$data['permissao_opcoes'] = \App\Permissaoopcoes::all()->toArray();
		return view('adm.usuario.cadastro')->with($data);
	}
	public function listaEditar(){
		$data['modulos'] = \App\Modulo::all()->toArray();
		$data['areas'] = \App\Areasmodulos::all()->toArray();
		$data['permissoes'] = \App\Permissoes::all()->toArray();
		$data['usuario'] = Auth::user();
		return view('adm.usuario.listaeditar')->with($data);
	}
	public function listarUsuarios(){
		$users = \App\User::all()->toArray();
		return Response::json(array('users'=>$users));
	}
	public function editarUsuario($id){
		$data['id']     = $id;
		$usuarioE = \App\User::find($id)->toArray();
		$data['usuarioE']   = $usuarioE;
		$data['perm_user'] = DB::table('permissaousuarios')
                        ->select('permissao_id')
                        ->where('usuario_id','=',$id)
                        ->lists('permissao_id');
        $data['modulos'] = \App\Modulo::all()->toArray();
		$data['areas'] = \App\Areasmodulos::all()->toArray();
		$data['permissoes'] = \App\Permissoes::all()->toArray();
		$data['permissao_opcoes'] = \App\Permissaoopcoes::all()->toArray();
		$permissao_opcoes_usuario = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id)->lists('permissao_opcoes_id');
       	
       	if($permissao_opcoes_usuario){

        	$permissao_opcoes_usuario = (array) $permissao_opcoes_usuario;
        	$data['permissoes_opcoes_usuario'] = $permissao_opcoes_usuario;	
        }
		$data['usuario'] = Auth::user();
        
		return view('adm.usuario.editar')->with($data);
	}



	public function salvarUsuario(){
		$input = Input::all();
		$insert['nome'] = $input['nome'];
		$insert['login'] = $input['login'];
		$insert['email'] = $input['email'];
		$insert['password'] =Hash::make($input['password']);
		$insert['unidade'] = $input['unidade'];
		$insert['created_by'] = Auth::user()->id;
		$insert['created_at'] = date('Y-m-d H:i:s');
		$user_id = \DB::connection('CENTRAL')->table('usuarios')->insertGetId($insert);
		\DB::table('usuarios')->insert($insert);
		$permissoes = $input['permissoes'];
		foreach ($permissoes as $permissao) {
			$insert_p ['usuario_id'] = $user_id;
			$insert_p ['permissao_id'] = $permissao;
			$insert_p ['created_by']  = Auth::user()->id;
			$insert_p ['created_at']  =  date('Y-m-d H:i:s');
			$res = \DB::connection('CENTRAL')->table('permissaousuarios')->insert($insert_p);
			\DB::table('permissaousuarios')->insert($insert_p);
		}

		$permop = isset($input['permop']) ? $input['permop'] : null;
		if($permop)
		foreach ($permop as $P) {
			$ins = [];
			$ins['usuario_id']   = $user_id;
			$ins['permissao_opcoes_id'] = $P;
			$ins['updated_by']   = Auth::user()->id;
			$ins['updated_at']   = date('Y-m-d H:i:s');
			$res = \DB::connection('CENTRAL')->table('permissao_opcoes_usuario')->insert($ins);
			\DB::table('permissao_opcoes_usuario')->insert($ins);
		}

		if($res){
			return Response::json(array('success'=>true,'mensagem'=>'Salvo com sucesso'));
		}else{
			return Response::json(array('success'=>false,'mensagem'=>'Erro ao salvar'));
		}
	}

	public function alterarUsuario(){
		$input = Input::all();	

		$insert['nome'] = $input['nome'];
		$insert['login'] = $input['login'];
		$insert['email'] = $input['email'];
		if($input['password'] != ''){
			$insert['password'] =Hash::make($input['password']);	
		}
		$insert['unidade'] = $input['unidade'];
		$insert['updated_by'] = Auth::user()->id;
		$insert['updated_at'] = date('Y-m-d H:i:s');
		
		\DB::connection('CENTRAL')->table('usuarios')->where('id',$input['id'])->update($insert);
		\DB::table('usuarios')->where('id',$input['id'])->update($insert);
		
		$permissoes=$input['permissoes'];
		

		\DB::connection('CENTRAL')->table('permissaousuarios')->where('usuario_id','=', $input['id'])->delete();
		\DB::table('permissaousuarios')->where('usuario_id','=', $input['id'])->delete();

		foreach ($permissoes as $Per) {
			$user_perm               = new \App\Permissaousuario;
			$insert_p['usuario_id']   = $input['id'];
			$insert_p['permissao_id'] = $Per;
			$insert_p['updated_by']   = Auth::user()->id;
			$insert_p['updated_at']   = date('Y-m-d H:i:s');
			$res = \DB::connection('CENTRAL')->table('permissaousuarios')->insert($insert_p);
			\DB::table('permissaousuarios')->insert($insert_p);
		}

		$permop = isset($input['permop']) ? $input['permop'] : null;
		\DB::connection('CENTRAL')->table('permissao_opcoes_usuario')->where('usuario_id','=', $input['id'])->delete();
		\DB::table('permissao_opcoes_usuario')->where('usuario_id','=', $input['id'])->delete();

		if($permop)
		foreach ($permop as $P) {
			$ins = [];
			$ins['usuario_id']   = $input['id'];
			$ins['permissao_opcoes_id'] = $P;
			$ins['updated_by']   = Auth::user()->id;
			$ins['updated_at']   = date('Y-m-d H:i:s');
			$res = \DB::connection('CENTRAL')->table('permissao_opcoes_usuario')->insert($ins);
			\DB::table('permissao_opcoes_usuario')->insert($ins);
		}

		return Response::json(array('success'=>true,'mensagem'=>'Alterado com sucesso'));
	}

	public function trocarsenha(){
		$id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        return view('adm.usuario.trocarsenha')->with($data);
	}

	public function salvarnovasenha(){
		$input = Input::all();
		
		$user = \DB::table('usuarios')->where('id',Auth::user()->id)->get();
		
		if (Hash::check($input['senha_velha'],  $user[0]->password)){
			if(	strlen($input['conf_senha']) >= 6)  {
				if($input['senha_nova'] == $input['conf_senha']){
					$insert['password']   = Hash::make($input['conf_senha']);
					$insert['updated_by'] = Auth::user()->id;
					$insert['updated_at'] = date('Y-m-d H:i:s');
					\DB::connection('CENTRAL')->table('usuarios')->where('id',Auth::user()->id)->update($insert);
					\DB::table('usuarios')->where('id',Auth::user()->id)->update($insert);
				}else{
					$data['usuario'] = Auth::user();
					$data['erro'] = 'Nova senha não confere com confirmação de senha';
					return view('adm.usuario.trocarsenha')
		            ->with($data);
				}
			}else{
				$data['usuario'] = Auth::user();
					$data['erro'] = 'Nova senha precisa ter no minimo 6 digitos';
					return view('adm.usuario.trocarsenha')
		            ->with($data);
			}
		}else{
			$data['usuario'] = Auth::user();
			$data['erro'] = 'Senha Antiga esta errada';
			return view('adm.usuario.trocarsenha')
            ->with($data);
		}

		\Auth::logout();
     	return \Redirect::away('/');
	}

	public function excluirUsuario(){
		$input = Input::all();		
		
		$insert['deleted_by'] = Auth::user()->id;
		$insert['deleted_at'] = date('Y-m-d H:i:s');
		
		try{
			\DB::connection('CENTRAL')->table('usuarios')->where('id',$input['id'])->update($insert);
			\DB::table('usuarios')->where('id',$input['id'])->update($insert);
			\DB::connection('CENTRAL')->table('permissaousuarios')->where('usuario_id','=', $input['id'])->delete();
			\DB::table('permissaousuarios')->where('usuario_id','=', $input['id'])->delete();
			return Response::json(array('success'=>true,'mensagem'=>'Excluido com sucesso'));
		}catch(\Exception $e) {
			return Response::json(array('success'=>false,'mensagem'=>'ERRO: '.$e));
		}
		
	}
	
}
