<?php namespace App\Http\Controllers\ensaio;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Redirect;
use App\Permissaousuario;
use App\Guia;
use App\Cliente;
use App\Funcionario;
use App\Proposta;
use App\Fonte;
use App\Programacao;
use App\Projeto;
use App\Guiaproducao;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;


class GuiaController extends Controller 
{
        public function __construct(
                                    Guia $guia,
                                    Cliente $cliente,
                                    Funcionario $funcionario,
                                    Proposta $proposta,
                                    Fonte $fonte,
                                    Programacao $programacao,
                                    Projeto $projeto
                                    )
        {
                $this->middleware('auth');
                $this->guia = $guia;
                $this->cliente = $cliente;
                $this->funcionario = $funcionario;
                $this->proposta = $proposta;
                $this->fonte = $fonte;
                $this->programacao = $programacao;
                $this->projeto = $projeto;
        }

    public function index()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['guias'] = $this->guia->get();
        $data['clientes'] = DB::table('clientes')
        ->select('id','nome')
        ->lists('nome','id');
        $data['usuario'] = Auth::user();

        return view('ensaio.guia.index')->with($data);
    }

    public function cadastrar()
    {
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%ensaio%')
                    ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $data['clientes'] = ['0' => 'Selecione'] + $this->cliente
                        ->select(DB::raw('concat (cnpj ," - ",nome," - ",estado," - ",municipio) as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');
        
        $data['funcionarios'] = ['0' => 'Selecione'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');
        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->where('local_atual',$unidade_id_user)
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('id');
            
        $data['fontes'] = ['0' => 'Selecione'] + $this->fonte
            ->where('status','ATIVO')
            ->whereIn('irradiador',$data['irradiadores'])
            ->lists('numero','id');
        $data['unidades'] = \App\Unidade::select('unidade')->lists('unidade','unidade');
        $data['unidade'] = Auth::user()->unidade;
        $data['normas'] =  ['0' => 'Normas'] + \App\Normas::groupby('norma')->lists('norma','id');
        
        return view('ensaio.guia.cadastrar')->with($data);
    }


    public function editar($id)
    {   
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        

        if($guia->status > 0){
            $data['mensagem'] = 'Guia Operacional já executada, não é possivel fazer alterações!';
            $data['rota']     = '/guia/'.$id.'/visualizar';
            return view('erros')->with($data);
        }
        

        $data['guia'] = $guia;
        $data['guia_id'] = $guia->id;

        $data['clientes'] = ['' => 'Selecione'] + $this->cliente
                        ->select(DB::raw('concat (cnpj ," - ",nome," - ",estado," - ",municipio) as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');

        $data['funcionarios'] = ['0' => 'Selecione'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id'); 

        $unidade_id_user = \DB::table('unidade')->where('unidade','like', $guia->unidade)->pluck('id');
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->where('local_atual',$unidade_id_user)
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('id');
            
        $data['fontes'] = ['0' => 'Selecione'] + $this->fonte
            ->where('status','ATIVO')
            ->whereIn('irradiador',$data['irradiadores'])
            ->lists('numero','id');
       
        $data['propostas'] = $this->proposta
                        ->select(DB::raw('concat ("código atual: ",cod_proposta," / antiga: ",cod_proposta_anterior) as proposta,id'))
                        ->where('cliente_id', $guia->cliente_id)
                        ->lists('proposta','id'); 

        $data['programacoes'] = $this->programacao
                        ->where('guia_id', $guia->id)
                        ->where('unidade', $guia->unidade)
                        ->groupby('item')
                        ->get(); 

        $data['equipe'] = \App\Equipe::where('guia_id',$id)->get();
        $data['projeto'] = DB::table('projetos')->select(DB::raw('concat (servico ," - ", estado ) as projeto, id'))
                            ->lists('projeto','id');

        $data['local_ensaio'] = $guia->local_ensaio;
        $data['contratante_fabricante'] =  $guia->contratante_fabricante;
        $data['data_prog'] =  $guia->data_prog;
        $data['controle_cliente'] =   $guia->programacao_cliente;
        $data['tipo'] = array('0'=>'Tubular','1'=>'Plano');
        $data['unidade'] = Auth::user()->unidade;
        $data['unidades'] = \App\Unidade::select('unidade')->lists('unidade','unidade');
        $data['normas'] =  ['0' => 'Normas'] + \App\Normas::groupby('norma')->lists('norma','id');

        $data['irradiadores_todos'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');


        $programacoes = \DB::table('programacoes')->where('guia_id',$id)->first();
        if($programacoes->tipo == 2){
            $data['obsmanual'] = \DB::table('obsmanual')->where('guia_id',$id)->pluck('obs');            
            return view('ensaio.guia.gomanual')->with($data);
        }

        return view('ensaio.guia.editar')->with($data);
    }

    public function dadosCliente()
    {
        $id = Input::get('id');
        if($id != 0){
            $cliente = $this->cliente->find($id)->toArray();

            return Response::json(array('success' => true,'cliente'=>$cliente));    
        }
        
    }

    public function dadosPropostas()
    {   
        $cliente_id = Input::get('cliente_id');

        $proposta = $this->proposta
                        ->select(DB::raw('concat ("código atual: ",cod_proposta," / antiga: ",cod_proposta_anterior) as proposta,id'))
                        ->where('cliente_id', $cliente_id)
                        ->lists('proposta','id');

        return Response::json($proposta);
    }

    public function dadosFonte()
    {   

        $id = Input::get('id');
        if($id != 0){
            $fonte = $this->fonte
                        ->select('isotopo', 'foco', 'atividade_inicial', 'data_inicial', 'meia_vida', 'irradiador')
                        ->where('id', $id)
                        ->first();

        $irradiadores = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');

        $datainicio = $fonte->data_inicial;
                
        $totaldias = $datainicio->diffInDays(Carbon::now())+1;
                
        $carga_inicial = floatval($fonte->atividade_inicial);

        $meiavida= floatval($fonte->meia_vida);

        $carga  = number_format($carga_inicial, 2);
                
        $carga = number_format($carga/pow(2,($totaldias-1)/$meiavida) ,2);

        $fonte = [
                    'atividade' => $carga,
                    'isotopo' => $fonte->isotopo,
                    'foco' => $fonte->foco,
                    'irradiador' => $irradiadores[$fonte->irradiador]
                ];

        return Response::json($fonte);
        }
        
    }

    public function visualizar($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia; 

        $data['funcionarios'] = $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');      
        
        $data['programacoes'] = $this->programacao
                        ->where('guia_id', $guia->id)
                        ->where('unidade', $guia->unidade)
                        ->groupby('item')
                        ->get();

        $data['projeto'] = $this->projeto
                        ->where('proposta_id', $guia->proposta_id)
                        ->get(); 
        $data['monitores'] = \DB::table('equipamentos')->where('descricao','MONITOR AREA')->lists('serial','id');

        $data['descontinuidadePro'] =  $this->programacao
                        ->where('guia_id', $guia->id)
                        ->where('unidade', $guia->unidade)
                        ->get();
                        
        $data['entrada'] = $guia->entrada;
        $data['saida'] = $guia->saida;    
        
        $tem_ex = Carbon::createFromFormat('H:i:s', '00:00:00');
        $data['refazer'] = false;
        foreach ($data['programacoes'] as $key => $value) {
            $tem = Carbon::createFromFormat('H:i:s', $value->tempo_exposicao)->toTimeString();
            $h = substr($tem, 0,2);
            $m = substr($tem, 3,2);
            $s = substr($tem, 6,2);
            if($value->tipo_exposicao == 'Normal'){
                $tem_ex->addSeconds((($h*3600)+($m*60)+$s)*$value->filmes_batidos);    
            }else{
                 $tem_ex->addSeconds(($h*3600)+($m*60)+$s);
            }
            if($value->executado == 'N Realizada'){
                $data['refazer'] = true;
            }
        }
        if($tem_ex->toTimeString() == '00:00:00'){
            $data['tempo_exposicao']= ' ';
        }else{
            $data['tempo_exposicao'] = $tem_ex->toTimeString();
        }

        $data['filmes'] = \App\Filme::all()->groupby('legenda')->toArray();

        $data['descontinuidades'] = \App\Descontinuidade::all()->toArray();

        $data['cod_inspecao'] = \App\Inspecao::all()->toArray();

        $data['equipe'] = \App\Equipe::where('guia_id',$id)->get();
        
        $data['nomenormas'] = \App\Normas::lists('norma','id');
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');
        return view('ensaio.guia.visualizar')->with($data);
    }

    public function gomanual(){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $data['clientes'] = ['0' => 'Selecione'] + $this->cliente
                        ->select(DB::raw('concat (cnpj ," - ",nome," - ",estado," - ",municipio) as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');
        
        $data['funcionarios'] = ['0' => 'Selecione'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');

        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->where('local_atual',$unidade_id_user)
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('id');
            
        $data['fontes'] = ['0' => 'Selecione'] + $this->fonte
            ->where('status','ATIVO')
            ->whereIn('irradiador',$data['irradiadores'])
            ->lists('numero','id');

        $data['unidade'] = Auth::user()->unidade;
        $data['unidades'] = \App\Unidade::select('unidade')->lists('unidade','unidade');
        return view('ensaio.guia.gomanual')->with($data);
    }
    public function salvarmanual(){
        $success = true;
        $proc = array();
        $erros = array();       

        $id_user = Auth::user()->id;
        $input = Input::all();
        
        if($input['guia_id']){
            $guia = \App\Guia::find($input['guia_id']);
            $guia->updated_by = Auth::user()->id;
            $equipe = \App\Equipe::where('guia_id',$input['guia_id'])->first();
            $equipe->updated_by = Auth::user()->id;
            $obs = \App\Obsmanual::where('guia_id',$input['guia_id'])->first();
        }else{
             $guia = new \App\Guia;
             $guia->created_by = $id_user;
             $obs = new \App\Obsmanual;
             $equipe = new \App\Equipe;
             $equipe->created_by = Auth::user()->id;
        }
        $guia->fonte_id = $input['fonte_id'];
        $guia->fonte_atividade = $input['fonte_atividade'];
        $guia->data = $input['datat'];
        $guia->unidade = $input['unidade'];
        $guia->proposta_id =  $input['proposta_id'];
        $guia->cliente_id =   $input['cliente_id'];
        $guia->fonte_id = $input['fonte_id'];
        $guia->fonte_atividade =  $input['fonte_atividade'];
        $guia->programacao_cliente = $input['controle_cliente'];
        $guia->local_ensaio =  $input['local'];
        $guia->data_prog =  $input['data_prog'];
        $guia->projeto =  $input['projeto'];
        $guia->contratante_fabricante =  $input['contratante_fabricante'];
        $guia->save();

        
        $obs->guia_id = $guia->id;
        $obs->obs = $input['obsmanual'];
        $obs->unidade = $input['unidade'];
        $obs->save();
       
        $equipe->guia_id = $guia->id;
        $equipe->op1 = $input['op1'];
        $equipe->op2 = $input['op2'];
        $equipe->aux = $input['aux'];
        $equipe->ins = $input['ins'];
        $equipe->unidade = $input['unidade'];
        $equipe->save();

        foreach ($input['data'] as $key => $value) {
            if($key != 0){
                $insert = [];
                $insert['item'] = $input['data'][$key][0];
                $insert['guia_id'] =  $guia->id;
                $insert['unidade'] =  $guia->unidade;
                $insert['identificacao'] = isset($input['data'][$key][1]) ? $input['data'][$key][1] : '';
                $insert['spool'] =$input['data'][$key][2];
                $insert['obra'] = $input['data'][$key][17];
                $insert['junta'] = $input['data'][$key][3];
                $insert['posicao_soldagem'] = $input['data'][$key][4];
                $insert['metal_base'] = $input['data'][$key][5];
                $insert['diametro'] = $input['data'][$key][6];
                $insert['espessura'] = $input['data'][$key][7];
                $insert['sinete_raiz'] = $input['data'][$key][9];
                $insert['processo_raiz'] = $input['data'][$key][12];
                $insert['sinete_acabamento'] = $input['data'][$key][10];
                $insert['processo_acabamento'] = $input['data'][$key][13];
                $insert['norma'] =$input['data'][$key][15];
                $insert['nivel_inspecao'] = $input['data'][$key][14];
                $insert['procedimento'] = $input['data'][$key][18];
                $insert['tecnica'] =$input['data'][$key][20];
                $insert['dff'] = $input['data'][$key][19];
                $insert['tipo_iqi'] = $input['data'][$key][21];
                $insert['fio_iqi'] = $input['data'][$key][22];
                $insert['chanfro'] = $input['data'][$key][8];
                $insert['filme_dimensao'] = $input['data'][$key][25];
                $insert['filme_classe'] = $input['data'][$key][24];
                $insert['quantidade_filmes'] = $input['data'][$key][23];
                $insert['criterio_aceitacao'] = $input['data'][$key][16];
                $insert['observacao'] = isset($input['data'][$key][27]) ? $input['data'][$key][27] : '';
                $insert['reforco']    = $input['data'][$key][26];
                $insert['tipo'] = 2;

                $programacao = \App\Programacao::where('guia_id', $input['guia_id'])->where('item',$insert['item'])->get();


                $id_cliente = DB::table('guias')->select('cliente_id')->where('id',$guia->id)->first();
                
                $seqfilme = \App\Sequencialfilmes::where('id_cliente','=',$id_cliente->cliente_id)->first();
                
                if(isset($programacao[0])){
                    if($programacao[0]->quantidade_filmes > $insert['quantidade_filmes']){
                        for ($i = $programacao[0]->quantidade_filmes ; $i > $insert['quantidade_filmes']; $i--) { 
                            \App\Programacao::where('guia_id',$input['guia_id'])->where('item',$insert['item'])->where('numero_filme',$i)->delete();
                        }    
                    }    
                }
                

                if(!is_null($seqfilme)){
                    $seqfilme =  \App\Sequencialfilmes::find($seqfilme->id);
                    $auxseq = $seqfilme->sequencial+1;
                    for ($i=1; $i <= $insert['quantidade_filmes']; $i++) {
                        if(count($programacao) > 0){
                            if(isset($programacao[$i-1])){
                                $insert['updated_by'] = Auth::user()->id;
                                $programacao[$i-1]->update($insert);
                            }else{
                                $insert['isometrico'] = $auxseq;
                                $insert['numero_filme'] = $i;
                                $insert['created_by'] = Auth::user()->id;
                                $this->programacao->create($insert);
                                $auxseq++;
                                $seqfilme->sequencial = $seqfilme->sequencial+1;
                            }
                        }else{
                            $insert['isometrico'] = $auxseq;
                            $insert['numero_filme'] = $i;
                            $insert['created_by'] = Auth::user()->id;
                            $this->programacao->create($insert);
                            $auxseq++;
                            $seqfilme->sequencial = $seqfilme->sequencial+1;
                        }
                    }
                   $seqfilme->save();
                }else{
                    for ($i=1; $i <= $insert['quantidade_filmes']; $i++) {
                        if(count($programacao) > 0){
                            $insert['updated_by'] = Auth::user()->id;
                            $programacao[$i-1]->update($insert);
                        }else{
                            $insert['isometrico'] = $i;
                            $insert['numero_filme'] = $i;
                            $insert['created_by'] = Auth::user()->id;
                            $this->programacao->create($insert);
                        }
                    }
                    $seqfilme = new \App\Sequencialfilmes;
                    $seqfilme->id_cliente = $id_cliente->cliente_id;
                    $seqfilme->sequencial = $insert['quantidade_filmes'];
                    $seqfilme->unidade =  $guia->unidade;
                    $seqfilme->save();
                }
            }
        }

        return Response::json(array('success' => $success, 'erros'=>$erros, 'idguia' =>  $guia->id));
    }

    public function getGuiaManual(){
        $id = Input::get('id');
        $data['programacoes'] = \DB::table('programacoes')->where('guia_id',$id)->groupby('item')->get();
        $data['success'] = true;
        return Response::json($data);
    }

    public function dadosProjeto(){
        $id = Input::get('id');
        $projetos = $this->projeto
                        ->select(DB::raw('concat (servico,"- Estado:",estado) as projeto,id'))
                        ->where('proposta_id', $id)
                        ->lists('projeto','id');
        return Response::json($projetos);
    }

    public function capa($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia;

        $data['programacoes'] = $this->programacao
                        ->where('guia_id', $guia->id)
                        ->where('unidade', $guia->unidade)
                        ->groupby('item')
                        ->get();
        $isometricos = array();
        foreach ( $data['programacoes'] as $key => $value) {
            $iso = DB::table('programacoes')->select(DB::raw('max(isometrico) as maior, min(isometrico) as menor'))->where('guia_id', $guia->id)->where('item', $value->item)->first();
            
            $isometrico[$value->item] = $iso;
        }
        foreach ( $data['programacoes'] as $key => $value) {
            $iso = DB::table('programacoes')->where('guia_id', $guia->id)->where('item', $value->item)->get();
            $isome[$value->item] = $iso;
        }
        $data['numero_filme'] = DB::table('programacoes')->where('guia_id', $guia->id)->lists('numero_filme','isometrico');


        $data['isometrico'] = $isometrico;
        $data['isome'] = $isome;
        $data['descontinuidades'] = \App\Descontinuidade::all()->toArray();
        
        $data['equipe'] = \App\Equipe::where('guia_id',$id)->get();
        
        $data['funcionarios'] = $this->funcionario->whereNull('deleted_at')->lists('nome','id');
        $data['funcionariossnqc'] = $this->funcionario->whereNull('deleted_at')->lists('snqc','id');
        
        $data['nomenormas'] = \App\Normas::lists('norma','id');
        return view('ensaio.guia.capa')->with($data);
    }

    public function etiqueta($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia;

        $data['programacoes'] = $this->programacao
                        ->where('guia_id', $guia->id)
                        ->where('unidade', $guia->unidade)
                        ->get();
        
        $data['equipe'] = \App\Equipe::where('guia_id',$id)->get();
        
        $data['funcionarios'] = $this->funcionario->whereNull('deleted_at')->lists('snqc','id');
        
        return view('ensaio.guia.etiqueta')->with($data);
    }

    public function carregarEtiquetas(){
        $input = Input::all();
        $id = $input['id'];
        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $programacoes = $this->programacao
                        ->where('guia_id', $id)
                        ->get();
        $equipe = \App\Equipe::where('guia_id',$id)->get();
        $funcionarios = $this->funcionario->whereNull('deleted_at')->lists('snqc','id');

        return Response::json(array('success' => true,'guia'=>$guia, 'filmes' => $programacoes, 'equipe' => $equipe, 'funcionarios' => $funcionarios));
    }

    public function rdo($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia;
        if($guia->status == 1 || $guia->status == 2){
            $data['programacoes'] = $this->programacao
                            ->where('guia_id', $guia->id)
                            ->where('unidade', $guia->unidade)
                            ->get();

            $rels = DB::table('relatorios')->where('guia_id',$guia->id)->get();
            $qu_filmes = [];
            foreach ($rels as $key => $value) {
                $quanti_filmes = DB::table('programacaorelatorios')->select(DB::raw('count(filmes_batidos) as quanti'),'obra')->where('versao',$value->versao)->where('id_relatorio',$value->numero_relatorio)->get();
                $qu_filmes[$key] = $quanti_filmes[0];
            }
            $data['quantidade'] = $qu_filmes;
            $data['rels'] = $rels;
           
            $data['projeto']= $this->projeto
                            ->where('proposta_id', $guia->proposta_id)
                            ->get();
            
            $data['juntas']   = \App\Junta::all()->toArray();
            
            $data['filmes']   = \App\Filme::all()->toArray();
            
            $data['producao'] = DB::table('guiaproducao')
                                ->select('junta_id','quantidade')
                                ->where('guia_id','=',$id)
                                ->lists('quantidade','junta_id');
            
            $filmesbatidos = DB::table('programacoes')
                                    ->select(DB::raw('filme_dimensao,filme_classe,count(item) as total'))
                                    ->where('guia_id',$id)
                                    ->where('executado','OK')
                                    ->groupby('filme_dimensao')
                                    ->get();
            $filclasseI = array();
            $filclasseII = array();
            foreach ($filmesbatidos as $key => $value) {
                if($value->filme_classe == 'I' || $value->filme_classe == 'Esp'){
                    $filclasseI[$value->filme_dimensao] = $value->total;
                }
                if($value->filme_classe == 'II'){
                    $filclasseII[$value->filme_dimensao] = $value->total;
                }
            }
            $data['filclasseI'] = $filclasseI;
            $data['filclasseII'] = $filclasseII;
            
            $data['equipe'] = \App\Equipe::where('guia_id',$id)->get();
            
            $data['funcionarios'] = $this->funcionario->whereNull('deleted_at')->lists('nome','id');
            
            return view('ensaio.guia.rdo')->with($data);
        }
        $data['rota'] = '/guia/'.$id.'/visualizar'; 
        $data['mensagem'] = 'Guia Operacional não executada, inclua os dados de retorno para gerar o RDO';
        return view('erros')->with($data);
    }

    public function relatorio($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $normas = DB::table('programacoes')->select('norma','item')->where('guia_id','=',$id)->where('executado','ok')->groupby('norma')->lists('norma','item');
        $arrayidsrelatorios = DB::table('gorelatorios')->where('id_go','=',$id)->first();

        // $totalnormas = array();
        // foreach ($normas as $key => $value) {
        //     $auxnormas = explode('+', $value);
        //     if(count($auxnormas)>1){
        //         foreach ($auxnormas as $k => $val) {
        //             if (!in_array($val, $totalnormas)) {
        //                 $totalnormas[] = $val;
        //             }
        //         }
        //     }else{
        //         if (!in_array($auxnormas[0], $totalnormas)) {
        //             $totalnormas[] = $auxnormas[0];
        //         }
        //     }
           
        // }

        // $data['normas'] =  $totalnormas;
        $data['normas'] =  $normas;
        $data['nomenormas'] = \App\Normas::lists('norma','id');

        
        
        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia;

        if($arrayidsrelatorios){
            $data['gorelatorios']  = explode(';', $arrayidsrelatorios->idsrelatorios);

            // $salvo = \App\Relatorio::where('guia_id',$guia->id)->get();
            
            // if(count($salvo) > 0){
                // $data['relatorio'] =  $salvo;
                $arrayrel = [];
                $relatorios = \App\Relatorio::where('guia_id',$guia->id)->get();
                foreach ($relatorios as $key => $value) {
                    $arrayrel[$value->numero_relatorio] = $value;
                }
                $data['relatorio'] = $arrayrel;
                $procedimento = [];
                $auxpr = array();
                // foreach ($salvo as $key => $value) {
                foreach ($data['relatorio'] as $key => $value) {
                    $pr= \App\Programacaorelatorio::where('guia_id', $guia->id)
                                            ->where('unidade', $guia->unidade)
                                            ->where('executado','OK')
                                            ->where('id_relatorio',$value->numero_relatorio)                                       
                                            ->where('versao',$value->versao)
                                            ->orderby('isometrico')
                                            ->get()->toArray();
                    $auxproc = [];
                    foreach ($pr as $key => $p) {
                        $auxproc[] = $p['procedimento'];
                    }
                    $procedimento[$value->numero_relatorio] = $auxproc;
                    $auxpr[$value->numero_relatorio] = $pr;
                }
                foreach ($procedimento as $key => $value) {
                    $procedimento[$key] = array_unique($value);
                }
                $data['procedimentos'] = $procedimento;
                $data['programacoes'] = $auxpr;
                
                
            // }else{
            //     $data['programacoes'] = $this->programacao
            //                 ->where('guia_id', $guia->id)
            //                 ->where('unidade', $guia->unidade)
            //                 ->where('executado','OK')
            //                 ->get()->toArray();
            // }
            

            // $data['projeto']= $this->projeto
            //                 ->where('proposta_id', $guia->proposta_id)
            //                 ->get();

            $data['juntas']   = \App\Junta::all()->toArray();

            $data['filmes']   = \App\Filme::all()->toArray();

            $data['producao'] = DB::table('guiaproducao')
                                ->select('junta_id','quantidade')
                                ->where('guia_id','=',$id)
                                ->lists('quantidade','junta_id');
            
            $filmesbatidos = DB::table('programacoes')
                                    ->select(DB::raw('filme_dimensao,filme_classe,count(item) as total'))
                                    ->where('guia_id',$id)
                                    ->groupby('filme_dimensao')
                                    ->get();
            $filclasseI = array();
            $filclasseII = array();
            foreach ($filmesbatidos as $key => $value) {
                if($value->filme_classe == 'I'){
                    $filclasseI[$value->filme_dimensao] = $value->total;
                }
                if($value->filme_classe == 'II'){
                    $filclasseII[$value->filme_dimensao] = $value->total;
                }
            }
            $data['filclasseI'] = $filclasseI;
            $data['filclasseII'] = $filclasseII;

            $data['descontinuidadePro'] = $this->programacao
                            ->where('guia_id', $guia->id)
                            ->where('unidade', $guia->unidade)
                            ->get();

            
            
            $data['equipe'] = \App\Equipe::where('guia_id',$id)->get();
            
            $data['funcionarios'] = $this->funcionario->whereNull('deleted_at')->lists('matricula_qt','id');
            $data['funcionariossnqc'] = $this->funcionario->whereNull('deleted_at')->lists('snqc','id');
            $data['funcionariosnome'] = $this->funcionario->whereNull('deleted_at')->lists('nome','id');
            return view('ensaio.guia.relatorio')->with($data);
        }
        $data['rota'] = '/guia/'.$id.'/visualizar'; 
        $data['mensagem'] = "Nenhum Relatorio criado,  <b><a href=\"/guia/$guia->id/criarrelatorio\">click aqui</a></b> para criar um relatorio.";
        return view('erros')->with($data);
    }




    public function incluirdados($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia;

        if($guia->status == 0 || $guia->status == 1){
        

            if($guia->entrada){
                $data['entrada'] = $guia->entrada->format('d-m-Y H:i');
                $data['saida'] = $guia->saida->format('d-m-Y H:i');
            }

            $data['programacoes'] = $this->programacao
                            ->where('guia_id', $guia->id)
                            ->where('unidade', $guia->unidade)
                            ->groupby('item')
                            ->get();
            
            $data['projeto']= $this->projeto
                            ->where('proposta_id', $guia->proposta_id)
                            ->get(); 

            $data['juntas']   = \App\Junta::all()->toArray();
            
            $data['filmes']   = \App\Filme::all()->toArray();

            $data['producao'] = DB::table('guiaproducao')
                                ->select('junta_id','quantidade')
                                ->where('guia_id','=',$id)
                                ->lists('quantidade','junta_id');

            $data['now'] = Carbon::now()->format('d/m/Y');

            $unidade_id = \DB::table('unidade')->where('unidade','like', $guia->unidade)->pluck('id');
                
            $data['monitor_id'] = [0 => 'Serial'] + DB::table('equipamentos')
                                ->where('local_atual',$unidade_id)
                                ->where('descricao','MONITOR AREA')
                                ->lists('serial','id');     
            return view('ensaio.guia.incluirdados')->with($data);

        }
        $data['rota'] = '/guia/'.$id.'/visualizar'; 
        $data['mensagem'] = 'Guia Operacional já foi executada, não é possível incluir dados';
        return view('erros')->with($data);

    }

    public function incluirresultados($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia;

        if($guia->status == 2 || $guia->status == 1 ){
            $data['programacoes'] = $this->programacao
                        ->where('guia_id', $guia->id)
                        ->where('unidade', $guia->unidade)
                        ->orderby('item')
                        ->orderby('numero_filme')
                        ->get();

            $data['projeto']= $this->projeto
                            ->where('proposta_id', $guia->proposta_id)
                            ->get(); 

            $data['juntas']   = \App\Junta::all()->toArray();

            $data['filmes']   = \App\Filme::all()->toArray();

            $data['producao'] = DB::table('guiaproducao')
                                ->select('junta_id','quantidade')
                                ->where('guia_id','=',$id)
                                ->lists('quantidade','junta_id');
            
            $data['descontinuidades'] = \App\Descontinuidade::all()->lists('descricao','legenda');
           
            $data['inspecoes'] = \App\Inspecao::all()->toArray();
            
            return view('ensaio.guia.incluirresultados')->with($data); 
        }
            
            $data['rota'] = '/guia/'.$id.'/visualizar'; 
            $data['mensagem'] = 'Guia Operacional não foi executada ainda';
            return view('erros')->with($data);
        
    }

    public function salvardadosretorno(){
        $input             = Input::all();
        
        $guia              = \App\Guia::find($input['id']);
        $guia->entrada     = $input['hora_entrada'];
        $guia->saida       = $input['hora_saida'];
        $guia->horas_contratadas = $input['horas_contratadas'];
        $guia->updated_by  = Auth::user()->id;
        $guia->placa       = $input['placa'];
        $guia->km_inicial  = $input['km_inicial'];
        $guia->km_final    = $input['km_final'];
        $guia->ocorrencias = $input['ocorrencias'];
        $guia->relatorios  = $input['relatorios'];
        $guia->protocolos  = $input['protocolos'];
        $guia->observacoes = $input['observacoes'];
        $guia->status      = 1;
        $guia->monitor_id  = $input['monitor_id'];
        $guia->save();

        $arrayfilmes     = $input['filmes'];
        $juntas          = $input['juntas'];
        $filmes_batidos  = $input['filmes_batidos'];
        $filmes_perdidos = $input['filmes_perdidos'];
        $situacao        = $input['situacao'];
        $tempofonte      = $input['tempofonte'];
        $tipoex          = $input['tipoex'];

        foreach ($arrayfilmes as $key => $value) {
            $prog                  = \App\Programacao::find($value);
            $prog->filmes_batidos  = $filmes_batidos[$value];
            $prog->filmes_perdidos = $filmes_perdidos[$value];
            $prog->tempo_exposicao = $tempofonte[$value];
            $prog->tipo_exposicao  = $tipoex[$value];
            $prog->executado       = $situacao[$value];
            $prog->updated_by      = Auth::user()->id;
            $prog->save();
            $iditem = $prog->item;
            $insert['filmes_batidos']= $filmes_batidos[$value];
            $insert['filmes_perdidos']= $filmes_perdidos[$value];
            $insert['tempo_exposicao']= $tempofonte[$value];
            $insert['tipo_exposicao']= $tipoex[$value];
            $insert['executado']= $situacao[$value];
            $insert['updated_by']= Auth::user()->id;

           \App\Programacao::where('item',$iditem)->where('guia_id',$prog->guia_id)->update($insert);
        }

        if($juntas){
            foreach ($juntas as $key => $value) {
                if($key > 0){
                    $idgp = DB::table('guiaproducao')
                              ->where('guia_id','=',$guia->id)
                              ->where('junta_id','=',$key)
                              ->pluck('id');
                    if($idgp != null){
                         $gp = \App\Guiaproducao::find($idgp);
                         $gp->quantidade = $value;
                         $gp->updated_by = Auth::user()->id;
                    }else{
                        $gp = new \App\Guiaproducao;
                        $gp->guia_id    = $guia->id;
                        $gp->junta_id   = $key;
                        $gp->quantidade = $value;
                        $gp->created_by = Auth::user()->id;
                        $gp->unidade = $guia->unidade;
                    }
                    $gp->save();
                }
            }    
        }
        return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
    }

    public function salvardadosresultado(){
        $input             = Input::all();
       
        $guia = \App\Guia::find($input['id']);
        $guia->quantidade_filmes_cassete = $input['quantidadefilmescassete'];
        $guia->ecran_anterior = $input['ecranant'];
        $guia->ecran_posterior = $input['ecranpos'];
        $guia->temperatura = $input['tempec'];
        $guia->condicao_superficial = $input['condsupe'];
        $guia->temperatura_revelacao = $input['temrev'];
        $guia->tempo_revelacao = $input['temporev'];
        $guia->updated_by  = Auth::user()->id;
        $guia->status = 2;
        $guia->save();


        $normas = DB::table('programacoes')->select('norma','item')->where('guia_id','=',$guia->id)->where('executado','ok')->groupby('norma')->lists('norma','item');
        $totalnormas = array();
        foreach ($normas as $key => $value) {
            $auxnormas = explode('+', $value);
            if(count($auxnormas)>1){
                foreach ($auxnormas as $k => $val) {
                    if (!in_array($val, $totalnormas)) {
                        $totalnormas[] = $val;
                    }
                }
            }else{
                if (!in_array($auxnormas[0], $totalnormas)) {
                    $totalnormas[] = $auxnormas[0];
                }
            }
           
        }
 
        // $gorelatorios =  DB::table('gorelatorios')->where('id_go',$guia->id)->first();
        // if(!is_null($gorelatorios)){

        // }else{
            // $seq =  DB::table('sequencialunidade')->where('unidade', $guia->unidade)->first();

            // if(!is_null($seq)){
            //    $seq =  \App\Sequencialunidade::find($seq->id);
            //    $arrayaux = array();
            //     for ($i=0; $i < count($totalnormas); $i++) { 
            //        $arrayaux[] = ($i+1)+$seq->sequencial;
            //     }
            //    $seq->sequencial = $seq->sequencial+count($totalnormas);
            //    $seq->save();
               
            // }else{
            //     $seq = new \App\Sequencialunidade;
            //     $seq->unidade = $guia->unidade;
                
            //     $arrayaux = array();
            //     for ($i=0; $i < count($totalnormas); $i++) { 
            //        $arrayaux[] = ($i+1);
            //     }
            //     $seq->sequencial = count($totalnormas);
            //     $seq->save();
            // }

            // $stringids = '';
            // $stringids = implode($arrayaux, ';');
            // $gorelatorios = new \App\Gorelatorios;
            // $gorelatorios->id_go = $guia->id;
            // $gorelatorios->idsrelatorios = $stringids;
            // $gorelatorios->save();
        // }

        // $seq =  DB::table('sequencialcliente')->where('id_cliente',$guia->cliente_id)->first();

        // if(!is_null($seq)){
        //    $seq =  \App\Sequencialcliente::find($seq->id);
        //    $arrayaux = array();
        //     for ($i=0; $i < count($normas); $i++) { 
        //        $arrayaux[] = ($i+1)+$seq->sequencial;
        //     }
        //    $seq->sequencial = $seq->sequencial+count($normas);
        //    $seq->save();
           
        // }else{
        //     $seq = new \App\Sequencialcliente;
        //     $seq->id_cliente = $guia->cliente_id;
            
        //     $arrayaux = array();
        //     for ($i=0; $i < count($normas); $i++) { 
        //        $arrayaux[] = ($i+1);
        //     }
        //     $seq->sequencial = count($normas);
        //     $seq->save();
        // }

        // $gorelatorios =  DB::table('gorelatorios')->where('id_go',$guia->id)->first();
        // if(!is_null($gorelatorios)){

        // }else{
        //     $stringids = '';
        //     $stringids = implode($arrayaux, ';');
        //     $gorelatorios = new \App\Gorelatorios;
        //     $gorelatorios->id_go = $guia->id;
        //     $gorelatorios->ids_relatorios = $stringids;
        //     $gorelatorios->save();
        // }
        




        foreach ($input['arraydesc'] as $key => $value) {
            if($value != '' ){
                $prog                               = \App\Programacao::find($key);
                $prog->descontinuidade              = implode($value, ' - ');
                $prog->observacao                   = $input['arrayobs'][$key];
                $prog->tamanho_descontinuidade      = $input['arraytam'][$key];
                $prog->resultado                    = str_replace(' ', '', $input['arrayres'][$key]);
                $prog->updated_by                   = Auth::user()->id;
                $prog->save();
            }
        }


        $aprovados = DB::table('programacoes')
                              ->select(DB::raw('item,count(item) as contador,quantidade_filmes'))
                              ->groupby('item')
                              ->where('resultado','=','A')
                              ->where('guia_id',$input['id'])
                              ->get();       

        foreach ($aprovados as $key => $value) {
            $insert['quantidade_aprovado']= $value->contador;
            $insert['quantidade_reprovado']= $value->quantidade_filmes - $value->contador;
            $prog = DB::table('programacoes')->where('guia_id',$input['id'])->where('item',$value->item)->get();
            foreach ($prog as $key => $value) {
                \App\Programacao::find($value->id)->update($insert);
            }
        }



        $reprovados = DB::table('programacoes')
                              ->select(DB::raw('item,count(item) as contador,quantidade_filmes'))
                              ->groupby('item')
                              ->where('resultado','=','R')
                              ->where('guia_id',$input['id'])
                              ->get();

        if(count($reprovados) > 0){
            foreach ($reprovados as $key => $value) {

                $insert['quantidade_aprovado']= $value->quantidade_filmes - $value->contador;
                $insert['quantidade_reprovado']= $value->contador;
                $prog = DB::table('programacoes')->where('guia_id',$input['id'])->where('item',$value->item)->get();
                foreach ($prog as $key => $value) {
                    \App\Programacao::find($value->id)->update($insert);
                }
            }
        }else{
            $insert = [];
            foreach ($aprovados as $key => $value) {
                $insert['quantidade_reprovado']= 0;
                $prog = DB::table('programacoes')->where('guia_id',$input['id'])->where('item',$value->item)->get();
                foreach ($prog as $key => $value) {
                    \App\Programacao::find($value->id)->update($insert);
                }
            }
        }
        


        return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
    }

    public function salvarGuia(){
        $input = array_except(Input::all(), array('_token'));
        // dd($input);
        $success = true;
        $proc = array();
        $erros = array();
        $proced = array();
        $reforco = array();
        
        $msg = '';
        foreach($input['item'] as $key => $item)
        {   
            $espessura = str_replace(',', '.', $input['espessura'][$item]);
            if(empty($input['identificacao'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Identificacao do item '.$item." é obrigatorio \n";
            }
            if(empty($input['spool'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Spool do item '.$item." é obrigatorio \n";
            }
            if(empty($input['obra'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Obra do item '.$item." é obrigatorio \n";
            }
            if(empty($input['junta'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Junta do item '.$item." é obrigatorio \n";
            }
            if(empty($input['posicao_soldagem'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Posição de Soldagem do item '.$item." é obrigatorio \n";
            }
            // if(empty($input['reforco'][$item])){
            //     $success = false;
            //     $erros[] = $item;
            //     $msg  .= 'O campo Reforço do item '.$item." é obrigatorio \n";
            // }
            if(empty($input['metal_base'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Metal Base do item '.$item." é obrigatorio \n";
            }
            if(empty($input['diametro'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Diametro do item '.$item." é obrigatorio \n";
            }
            if(empty($input['espessura'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Espessura do item '.$item." é obrigatorio \n";
            }
            if(empty($input['chanfro'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Chanfro do item '.$item." é obrigatorio \n";
            }
            if(empty($input['sinete_raiz'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Sinete Raiz do item '.$item." é obrigatorio \n";
            }
            if(empty($input['processo_raiz'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Processo Raiz do item '.$item." é obrigatorio \n";
            }
            if(empty($input['sinete_acabamento'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Senete Acabamento do item '.$item." é obrigatorio \n";
            }
            if(empty($input['processo_acabamento'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Processo Acabamento do item '.$item." é obrigatorio \n";
            }
            if(empty($input['norma'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Norma do item '.$item." é obrigatorio \n";
            }
            // if(empty($input['aplicacao'][$item])){
            //     $success = false;
            //     $erros[] = $item;
            //     $msg  .= 'O campo Aplicação do item '.$item." é obrigatorio \n";
            // }
             if(empty($input['criterio_aceite'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Criterio de Aceite do item '.$item." é obrigatorio \n";
            }
            if(empty($input['nivel_inspecao'][$item])){
                $success = false;
                $erros[] = $item;
                $msg  .= 'O campo Nivel Inspeção do item '.$item." é obrigatorio \n";
            }
            if(empty($input['tecnica'][$item]) == 'null'){
                $success = false;
                $erros[] = $item;
                $msg  .= 'Selecione uma tecnica para o item '.$item."\n";
            }
            $tec = explode(' - ', $input['tecnica'][$item]);
            // dd($tec);
            $p = DB::table('procedimentos')->select('id')->where('procedimento','like','%'.$tec[1].'%')->get();
            // dd($p);
            // dd($input['norma'][$item][0]);
            // $n = DB::table('normas')->select('norma')->where('id',$input['norma'][$item][0] )->get();
            // $pro = DB::table('normas')->select('procedimento_id')
            //                           ->where('norma','like','%'.$n[0]->norma.'%')
            //                           ->get();

            //                           // dd($pro);
            //                           var_dump($pro);
                     
                    $n = DB::table('normas')->select('norma')->where('id',$input['norma'][$item][0] )->get();
                    if($espessura < 3.1){
                        $esp = $espessura + 1;
                    }else{
                        $esp = $espessura;
                    }      
            $ref = DB::table('normareforcos')->select('reforco_acab','reforco_raiz')
                                      ->where('procedimento_id',$p[0]->id)
                                      ->where('norma','like','%'.$n[0]->norma.'%')
                                      ->where('espessura_ini','<',$esp)
                                      ->where('espessura_fim','>=',$esp)
                                      ->first();
            if(count($ref) > 0){
                $reforco[$item] = $ref->reforco_acab + $ref->reforco_raiz;    
            }else{
                $success = false;
                $msg  .= "Não Foi encontrado Procedimento para Norma informada no item $item \n";
            }       
            
            

            if(count($p) == 0){
                    $success = false;
                    $msg  .= "Não Foi encontrado Procedimento para Norma informada no item $item \n";
            }

            if (!$success) {
                return Response::json(array('success' => $success, 'erros'=>$erros, 'msg' => $msg));
            }
            $proced[$item] = $p[0]->id;
            
        }

        if(empty($input['local_ensaio'])){
                $success = false;
                $msg  .= "O campo Local é obrigatorio \n";
        }
        if(empty($input['contratante_fabricante'])){
                $success = false;
                $msg  .= "O campo Contratante/Fabricante é obrigatorio \n";
        }
        if(empty($input['data_prog'])){
                $success = false;
                $msg  .= "O campo Data Programação é obrigatorio \n";
        }
        if(empty($input['programacao_cliente'])){
                $success = false;
                $msg  .= "O campo Controle Cliete é obrigatorio \n";
        }

        if (!$success) {
            return Response::json(array('success' => $success, 'erros'=>$erros, 'msg' => $msg));
        }

        foreach($input['item'] as $key => $item)
        {
            /*substituido dia 01/04/15*/
            // $proc[$item] = DB::table('normasprocedimentos')
            //                 ->select('id','diametro_ini')
            //                 ->where('norma','like','%'.$input['norma'][$item].'%')
            //                 ->where('aplicacao','like','%'.$input['aplicacao'][$item].'%')
            //                 ->where('metal_base','like','%'.$input['metal_base'][$item].'%')
            //                 ->where('diametro_ini','<=',$input['diametro'][$item])
            //                 ->where('diametro_fim','>=',$input['diametro'][$item])
            //                 ->where('espessura_ini','<=',$input['espessura'][$item])
            //                 ->where('espessura_fim','>=',$input['espessura'][$item])
            //                 ->first(); 

            $espessura = str_replace(',', '.', $input['espessura'][$item]);
            

            if($input['tipo'][$item] == 0){
                if($input['mili_ou_pol']  != 'false'){
                    $diametro = $input['diametro'][$item];
                }else{
                    $diametro = preg_replace("/[^0-9 \/]/", "",$input['diametro'][$item]);
                    $diametro = DB::table('polegadasmilimetros')->select('milimetro')->where('polegada', $diametro)->get();
                    $diametro = $diametro[0]->milimetro;
                }

                $tec = explode(' - ', $input['tecnica'][$item]);
                $proc[$item] = DB::table('parametrosprocedimentos')
                            ->leftjoin('procedimentos','procedimentos.id','=','parametrosprocedimentos.procedimento_id')
                            ->select('procedimentos.procedimento', 'parametrosprocedimentos.*')
                            ->where('parametrosprocedimentos.procedimento_id',$proced[$item])
                            ->where('parametrosprocedimentos.diametro_ini','<',$diametro)
                            ->where('parametrosprocedimentos.diametro_fim','>=',$diametro)
                            ->where('parametrosprocedimentos.espessura_ini','<',$espessura+$reforco[$item])
                            ->where('parametrosprocedimentos.espessura_fim','>=',$espessura+$reforco[$item])
                            ->where('parametrosprocedimentos.tecnica','=',$tec[0])
                            ->first();

            }
            if($input['tipo'][$item] == 1){

                $tec = explode(' - ', $input['tecnica'][$item]);
                $proc[$item] = DB::table('parametrosprocedimentos')
                            ->leftjoin('procedimentos','procedimentos.id','=','parametrosprocedimentos.procedimento_id')
                            ->select('procedimentos.procedimento', 'parametrosprocedimentos.*')
                            ->where('parametrosprocedimentos.procedimento_id','=',$proced[$item])
                            ->where('parametrosprocedimentos.comprimento_ini','<=',$input['diametro'][$item])
                            ->where('parametrosprocedimentos.comprimento_fim','>=',$input['diametro'][$item])
                            ->where('parametrosprocedimentos.espessura_ini','<=',$espessura+$reforco[$item])
                            ->where('parametrosprocedimentos.espessura_fim','>=',$espessura+$reforco[$item])
                            ->where('parametrosprocedimentos.tecnica','=',$tec[0])
                            ->first();
            }

            
            
            if (count($proc[$item]) == 0) {
                if(count($proc[$item]) > 1){
                     $msg .= 'Selecione uma tecnica para o item '.$item."\n";
                }
                $success = false;
                $erros[] = $item;
            }
        }
        
        if (!$success) {
            return Response::json(array('success' => $success, 'erros'=>$erros,'msg' => $msg));
        }
        if($input['idguia']){
            $guia = \App\Guia::find($input['idguia']);
            $guia->fonte_id = $input['fonte_id'];
            $guia->fonte_atividade = $input['fonte_atividade'];
            $guia->save();
            $equipe = \App\Equipe::where('guia_id',$input['idguia'])->first();
            $equipe->updated_by = Auth::user()->id;
            // \App\Programacao::where('guia_id',$input['idguia'])->delete();
        }else{
            $guia = $this->guia->create($input);
            $equipe = new \App\Equipe;
            $equipe->created_by = Auth::user()->id;
            $equipe->unidade = $guia->unidade;
        }

 
        $equipe->guia_id = $guia->id;
        $equipe->op1 = $input['op1'];
        $equipe->op2 = $input['op2'];
        $equipe->aux = isset($input['aux']) ? $input['aux'] : '';
        $equipe->ins = $input['ins'];
        $equipe->save();

        
        foreach($input['item'] as $key => $item)
        {           
            $espessura = str_replace(',', '.', $input['espessura'][$item]);
            $norma = implode('+', $input['norma'][$item]);
            if($espessura < 3.1){
                $esp = $espessura + 1;
            }else{
                $esp = $espessura;
            }
            
            $procedimento = \App\Parametrosprocedimentos::find($proc[$item]->id);
            $programacao = \App\Programacao::where('guia_id',$input['idguia'])->where('item',$item)->get();
            
            $insert = array();
            $insert['item'] = $input['item'][$key];
            $insert['guia_id'] =  $guia->id;
            $insert['unidade'] =  $guia->unidade;
            $insert['identificacao'] = $input['identificacao'][$item];
            $insert['spool'] = $input['spool'][$item];
            $insert['obra'] = $input['obra'][$item];
            $insert['junta'] = $input['junta'][$item];
            $insert['posicao_soldagem'] = $input['posicao_soldagem'][$item];
            $insert['metal_base'] = $input['metal_base'][$item];
            
            $insert['espessura'] = $espessura;
            $insert['sinete_raiz'] = $input['sinete_raiz'][$item];
            $insert['processo_raiz'] = $input['processo_raiz'][$item];
            $insert['sinete_acabamento'] = $input['sinete_acabamento'][$item];
            $insert['processo_acabamento'] = $input['processo_acabamento'][$item];
            $insert['norma'] = $norma;
            $insert['nivel_inspecao'] = $input['nivel_inspecao'][$item];
            $insert['procedimento'] = $proc[$item]->procedimento;
            $insert['tecnica'] = $proc[$item]->tecnica;
            $insert['dff'] = $proc[$item]->dff;
            $insert['tipo_iqi'] = $proc[$item]->tipo_iqi;
            $insert['fio_iqi'] = $proc[$item]->fio_iqi;
            // $insert['aplicacao'] = $input['aplicacao'][$item];
            $insert['chanfro'] = $input['chanfro'][$item];            
            $insert['filme_classe'] = $proc[$item]->classe_filme;

            if($input['tipo'][$item] == 0){
                if($input['mili_ou_pol']  != 'false'){
                    $diametro = $input['diametro'][$item];
                    $insert['diametro'] = preg_replace("/[^0-9 \/ .]/", "",$input['diametro'][$item]).'mm';
                }else{
                    $diametro = preg_replace("/[^0-9 \/]/", "",$input['diametro'][$item]);
                    $diametro = DB::table('polegadasmilimetros')->select('milimetro')->where('polegada', $diametro)->get();
                    $diametro = $diametro[0]->milimetro;
                    $insert['diametro'] = preg_replace("/[^0-9 \/]/", "",$input['diametro'][$item]).'"';
                }

                $auxdq = DB::table('parametrosprocedimentos')
                            ->select('quantidade_filmes','dimensao_filme')
                            ->where('parametrosprocedimentos.procedimento_id','=',$procedimento->procedimento_id)
                            ->where('parametrosprocedimentos.diametro_ini','<',$diametro)
                            ->where('parametrosprocedimentos.diametro_fim','>=',$diametro)
                            ->where('parametrosprocedimentos.espessura_ini','<=',$esp)
                            ->where('parametrosprocedimentos.espessura_fim','>=',$esp)
                            ->where('parametrosprocedimentos.tecnica','like', '%'.$proc[$item]->tecnica.'%')
                            ->first();
                    
                $quant_filme = $auxdq->quantidade_filmes;
                $dimensao_filme = $auxdq->dimensao_filme;

            }
            if ($input['tipo'][$item] == 1) {
                $auxdq = DB::table('parametrosprocedimentos')
                            ->select('quantidade_filmes','dimensao_filme')
                            ->where('parametrosprocedimentos.procedimento_id','=',$procedimento->procedimento_id)
                            ->where('parametrosprocedimentos.comprimento_ini','<',$input['diametro'][$item])
                            ->where('parametrosprocedimentos.comprimento_fim','>=',$input['diametro'][$item])
                            ->where('parametrosprocedimentos.espessura_ini','<=',$esp)
                            ->where('parametrosprocedimentos.espessura_fim','>=',$esp)
                            ->where('parametrosprocedimentos.tecnica','like', '%'.$proc[$item]->tecnica.'%')
                            ->first();
                $quant_filme = $auxdq->quantidade_filmes;
                $dimensao_filme = $auxdq->dimensao_filme;
                
                $insert['diametro'] = preg_replace("/[^0-9 \/ .]/", "",$input['diametro'][$item]).'mm';
            }
            if ($input['tipo'][$item] == 2) {
               $quant_filme = $proc[$item]->quantidade_filmes;
               $dimensao_filme = $proc[$item]->dimensao_filme;
            }

            $insert['filme_dimensao'] = $dimensao_filme;
            $insert['quantidade_filmes'] = $quant_filme;

            $insert['criterio_aceitacao'] = $input['criterio_aceite'][$item];
            $insert['observacao'] = $input['observacao'][$item];
            $insert['reforco']    = $reforco[$item];
            $insert['tipo'] = $input['tipo'][$item];


            if(isset($programacao[0])){
            if($programacao[0]->quantidade_filmes > $insert['quantidade_filmes']){
                for ($i = $programacao[0]->quantidade_filmes ; $i > $insert['quantidade_filmes']; $i--) { 
                    \App\Programacao::where('guia_id',$input['idguia'])->where('item',$item)->where('numero_filme',$i)->delete();
                }    
            }
            }
            // dd($programacao[0]->quantidade_filmes);


            $id_cliente = DB::table('guias')->select('cliente_id')->where('id',$guia->id)->first();
            
            $seqfilme = \App\Sequencialfilmes::where('id_cliente','=',$id_cliente->cliente_id)->first();
            

            if(!is_null($seqfilme)){
               $seqfilme =  \App\Sequencialfilmes::find($seqfilme->id);
               $auxseq = $seqfilme->sequencial+1;
               for ($i=1; $i <= $insert['quantidade_filmes']; $i++) {
                    if(count($programacao) > 0){
                        if(isset($programacao[$i-1])){
                            $insert['updated_by'] = Auth::user()->id;
                            $programacao[$i-1]->update($insert);
                        }else{
                            $insert['isometrico'] = $auxseq;
                            $insert['numero_filme'] = $i;
                            $insert['created_by'] = Auth::user()->id;
                            $this->programacao->create($insert);
                            $auxseq++;
                            $seqfilme->sequencial = $seqfilme->sequencial+1;
                        }
                        
                    }else{
                        $insert['isometrico'] = $auxseq;
                        $insert['numero_filme'] = $i;
                        $insert['created_by'] = Auth::user()->id;
                        $this->programacao->create($insert);
                        $auxseq++;
                        $seqfilme->sequencial = $seqfilme->sequencial+1;
                    }
                    
                }
                
               // $seqfilme->sequencial = $seqfilme->sequencial+$insert['quantidade_filmes'];
               $seqfilme->save();
            }else{
                
                for ($i=1; $i <= $insert['quantidade_filmes']; $i++) {
                    if(count($programacao) > 0){
                        $insert['updated_by'] = Auth::user()->id;
                        $programacao[$i-1]->update($insert);
                    }else{
                        $insert['isometrico'] = $i;
                        $insert['numero_filme'] = $i;
                        $insert['created_by'] = Auth::user()->id;
                        $this->programacao->create($insert);
                    }
                }
                $seqfilme = new \App\Sequencialfilmes;
                $seqfilme->id_cliente = $id_cliente->cliente_id;
                $seqfilme->sequencial = $insert['quantidade_filmes'];
                $seqfilme->unidade = $guia->unidade;
                $seqfilme->save();
            }

            // for ($i=1; $i <= $insert['quantidade_filmes']; $i++) {
            //     if(count($programacao) > 0){
            //         $insert['updated_by'] = Auth::user()->id;
            //         $programacao[$i-1]->update($insert);
            //     }else{
            //         $insert['numero_filme'] = $i;
            //         $insert['created_by'] = Auth::user()->id;
            //         $this->programacao->create($insert);
            //     }
            // }
        }

        return Response::json(array('success' => $success, 'erros'=>$erros, 'idguia' =>  $guia->id));
    }
    public function carregarProcedimento(){
        $input = array_except(Input::all(), array('_token'));
        
        $success = true;
        $proc = array();
        $erros = array();
        $proced = array();
        $reforco = array();
        
        $msg = '';
        foreach($input['item'] as $key => $item){
            $n = DB::table('normas')->select('norma')->where('id',$input['norma'][$item][0] )->get();
            $procedimento = DB::table('normas')->select('procedimento_id')
                        ->where('norma','like','%'.$n[0]->norma.'%')
                        ->groupby('procedimento_id')
                        ->get();

            if(count($procedimento) == 0){
                    $success = false;
                    $erros[] = $item; 
                    $msg  .= "Não Foi encontrado Procedimento para Norma informada no item $item \n";
            }

            if(count($procedimento) > 0){
                $aux = [];
                foreach ($procedimento as $key => $pro) {
                    $aux[] = $pro->procedimento_id;
                }
            }

            if (!$success) {
                return Response::json(array('success' => $success, 'erros'=>$erros, 'msg' => $msg));
            }
            $proced[$item] = $aux;
        }
    
        foreach($input['item'] as $key => $item){
            $espessura = str_replace(',', '.', $input['espessura'][$item]);
            $espessura = str_replace(' ', '', $input['espessura'][$item]);
            $espessura = explode('+',$espessura);
            $espessura = min($espessura);

                if($input['tipo'][$item] == 0){
                    if(count($proced[$item]) > 1){
                        $proc[$item] = array();
                        foreach ($proced[$item] as $key => $value) {
                            $n = DB::table('normas')->select('norma')->where('id',$input['norma'][$item][0] )->get();
                             if($espessura < 3.1){
                                    $esp = $espessura + 1;
                                }else{
                                    $esp = $espessura;
                                }
                            $ref = DB::table('normareforcos')->select('reforco_acab','reforco_raiz')
                                        ->where('procedimento_id',$value)
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('espessura_ini','<=', $esp)
                                        ->where('espessura_fim','>', $esp)
                                        ->get();

                           if(count($ref) > 0){
                                $fonte = $input['tipo_fonte'];
                                $fonte = explode(" ", $fonte);
                                // dd($input['mili_ou_pol']);

                                if($input['mili_ou_pol']  != 'false'){
                                    $diametro = $input['diametro'][$item];
                                }else{
                                    $diametro = preg_replace("/[^0-9 \/ ,]/", "",$input['diametro'][$item]);

                                    $diametro = DB::table('polegadasmilimetros')->select('milimetro')->where('polegada', $diametro)->get();
                                    if($diametro){
                                        $diametro = $diametro[0]->milimetro;
                                    }else{
                                        $success = false;
                                        $erros[] = $item; 
                                        $msg  .= "Polegada não cadastrada no item:  $item \n";
                                        return Response::json(array('success' => $success, 'erros'=>$erros, 'msg' => $msg));
                                    }
                                    
                                    
                                }
                                $p = DB::table('parametrosprocedimentos')
                                        ->leftjoin('procedimentos','procedimentos.id','=','parametrosprocedimentos.procedimento_id')
                                        ->select('procedimentos.procedimento', 'parametrosprocedimentos.*')
                                        ->where('parametrosprocedimentos.procedimento_id',$value)
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('parametrosprocedimentos.diametro_ini','<=',$diametro)
                                        ->where('parametrosprocedimentos.diametro_fim','>',$diametro)
                                        ->where('parametrosprocedimentos.espessura_ini','<=', $espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->where('parametrosprocedimentos.espessura_fim','>', $espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->get();
                                      
                                if($espessura < 3.1){
                                    $espessura += 1;
                                }
                                foreach ($p as $key => $value) {
                                    $quant_filme = DB::table('parametrosprocedimentos')
                                        ->select('quantidade_filmes')
                                        ->where('parametrosprocedimentos.procedimento_id','=',$value->procedimento_id)
                                        ->where('parametrosprocedimentos.diametro_ini','<=',$diametro)
                                        ->where('parametrosprocedimentos.diametro_fim','>',$diametro)
                                        ->where('parametrosprocedimentos.espessura_ini','<=',$espessura)
                                        ->where('parametrosprocedimentos.espessura_fim','>',$espessura)
                                        ->where('parametrosprocedimentos.tecnica','like', '%'.$value->tecnica.'%')
                                        ->first();
                                    if($quant_filme){ 

                                    }else{
                                        unset($p[$key]);
                                    }
                                }
                                $proc[$item] = array_merge($proc[$item],$p);
                            }
                        }
                    }else{
                        if($espessura < 3.1){
                            $esp = $espessura + 1;
                        }else{
                            $esp = $espessura;
                        }
                        $n = DB::table('normas')->select('norma')->where('id',$input['norma'][$item][0] )->get();
                        $ref = DB::table('normareforcos')->select('reforco_acab','reforco_raiz')
                                        ->where('procedimento_id',$proced[$item])
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('espessura_ini','<=',$esp)
                                        ->where('espessura_fim','>',$esp)
                                        ->get();
                        if(count($ref) > 0){
                            $fonte = $input['tipo_fonte'];
                            $fonte = explode(" ", $fonte);

                            // dd($input['mili_ou_pol']);
                                
                                if($input['mili_ou_pol'] != 'false'){
                                    $diametro = $input['diametro'][$item];
                                }else{
                                    $diametro = preg_replace("/[^0-9 \/ ,]/", "",$input['diametro'][$item]);
                                    $diametro = DB::table('polegadasmilimetros')->select('milimetro')->where('polegada', $diametro)->get();
                                    $diametro = $diametro[0]->milimetro;
                                }
                                
                            if($espessura < 3.1){
                                $espessura += 1;
                            }
                            $proc[$item] = DB::table('parametrosprocedimentos')
                                        ->leftjoin('procedimentos','procedimentos.id','=','parametrosprocedimentos.procedimento_id')
                                        ->select('procedimentos.procedimento', 'parametrosprocedimentos.*')
                                        ->where('parametrosprocedimentos.procedimento_id','=',$proced[$item][0])
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('parametrosprocedimentos.diametro_ini','<=',$diametro)
                                        ->where('parametrosprocedimentos.diametro_fim','>',$diametro)
                                        ->where('parametrosprocedimentos.espessura_ini','<=',$espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->where('parametrosprocedimentos.espessura_fim','>',$espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->get();
                        }
                    } 
                }

                if($input['tipo'][$item] == 1){

                    if(count($proced[$item]) > 1){
                        $proc[$item] = array();
                        foreach ($proced[$item] as $key => $value) {
                             $n = DB::table('normas')->select('norma')->where('id',$input['norma'][$item][0] )->get();

                            $ref = DB::table('normareforcos')->select('reforco_acab','reforco_raiz')
                                        ->where('procedimento_id',$value)
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('espessura_ini','<',$espessura)
                                        ->where('espessura_fim','>=',$espessura)
                                        ->get(); 

                            if(count($ref) > 0){
                                $fonte = $input['tipo_fonte'];
                                $fonte = explode(" ", $fonte);

                                
                                    $diametro = $input['diametro'][$item];
                               

                                $p = DB::table('parametrosprocedimentos')
                                        ->leftjoin('procedimentos','procedimentos.id','=','parametrosprocedimentos.procedimento_id')
                                        ->select('procedimentos.procedimento', 'parametrosprocedimentos.*')
                                        ->where('parametrosprocedimentos.procedimento_id',$value)
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('parametrosprocedimentos.comprimento_ini','<',$diametro)
                                        ->where('parametrosprocedimentos.comprimento_fim','>=',$diametro)
                                        // ->where('parametrosprocedimentos.tipo_fonte','like','%'.$fonte[0].'%')
                                        ->where('parametrosprocedimentos.espessura_ini','<',$espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->where('parametrosprocedimentos.espessura_fim','>=',$espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->get();

                                        
                                $proc[$item] = array_merge($proc[$item],$p);
                            }
                        }
                    }else{
                        $n = DB::table('normas')->select('norma')->where('id',$input['norma'][$item][0] )->get();
                        $ref = DB::table('normareforcos')->select('reforco_acab','reforco_raiz')
                                        ->where('procedimento_id',$proced[$item])
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('espessura_ini','<',$espessura)
                                        ->where('espessura_fim','>=',$espessura)
                                        ->get();
                        if(count($ref) > 0){
                            $fonte = $input['tipo_fonte'];
                            $fonte = explode(" ", $fonte);

                            
                                    $diametro = $input['diametro'][$item];
                               
                            $proc[$item] = DB::table('parametrosprocedimentos')
                                        ->leftjoin('procedimentos','procedimentos.id','=','parametrosprocedimentos.procedimento_id')
                                        ->select('procedimentos.procedimento', 'parametrosprocedimentos.*')
                                        ->where('parametrosprocedimentos.procedimento_id','=',$proced[$item][0])
                                        ->where('norma','like','%'.$n[0]->norma.'%')
                                        ->where('parametrosprocedimentos.comprimento_ini','<',$diametro)
                                        ->where('parametrosprocedimentos.comprimento_fim','>=',$diametro)
                                        // ->where('parametrosprocedimentos.tipo_fonte','like','%'.$fonte[0].'%')
                                        ->where('parametrosprocedimentos.espessura_ini','<',$espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->where('parametrosprocedimentos.espessura_fim','>=',$espessura+$ref[0]->reforco_acab+$ref[0]->reforco_raiz)
                                        ->get();
                        }
                    }           
                }

            if (count($proc[$item]) == 0) {
                // $success = false;
                $erros[] = $item;
                $msg  .= "Não Foi encontrado tecnica para o item: $item \n";
            }
        }

        return Response::json(array('success' => $success, 'procedimentos' => $proc, 'erros'=>$erros, 'msg' => $msg));
        
    }

    public function cadastroLayout(){
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['guias'] = $this->guia->get();
        $data['clientes'] = DB::table('clientes')
        ->select('id','nome')
        ->lists('nome','id');
        $data['usuario'] = Auth::user();
        $data['layouts'] = \App\Layout::all();

        return view('ensaio.guia.homecadastrolayout')->with($data);
    }
    public function cadastrarLayout(){
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['guias'] = $this->guia->get();
        $data['clientes'] = ['' => 'Selecione'] + DB::table('clientes')
        ->select('id','nome')
        ->lists('nome','id');
        $data['usuario'] = Auth::user();
        $data['layouts'] = \App\Layout::all();
        return view('ensaio.guia.cadastrarlayout')->with($data);
    }

    public function editarLayout($id){
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['guias'] = $this->guia->get();
        $data['clientes'] = ['' => 'Selecione'] + DB::table('clientes')
        ->select('id','nome')
        ->lists('nome','id');
        $data['usuario'] = Auth::user();
        $data['layout'] = \App\Layout::find($id);
        return view('ensaio.guia.editarlayout')->with($data);
    }

    public function salvarLayout(){
        $input = Input::all();
        if(isset($input['idLay'])){
             $layout = \App\Layout::find($input['idLay']);
             $layout->updated_by =  Auth::user()->id;
        }else{
            $layout = new \App\Layout; 
            $layout->created_by = Auth::user()->id;   
        }
        $layout->cliente_id = $input['cliente_id'];
        $layout->programacao  = $input['programacao'];
        $layout->obra  = $input['obra'];
        $layout->local_ensaio  = $input['local_ensaio'];
        $layout->data  = $input['data'];
        $layout->contratante_fabricante  = $input['contratante_fabricante'];
        $layout->item  = $input['item'];
        $layout->iden_dese_iso  = $input['iden_dese_iso'];
        $layout->mapa_spool  = $input['mapa_spool'];
        $layout->prog_fabri  = $input['prog_fabri'];
        $layout->junta  = $input['junta'];
        $layout->posicao_soldagem  = $input['posicao_soldagem'];
        $layout->metal_base  = $input['metal_base'];
        $layout->diametro  = $input['diametro'];
        $layout->espessura  = $input['espessura'];
        $layout->reforco  = $input['reforco'];
        $layout->espessura_menor  = $input['espessura_menor'];
        $layout->espessura_maior  = $input['espessura_maior'];
        $layout->Sold_sinete_raiz  = $input['Sold_sinete_raiz'];
        $layout->processo_raiz  = $input['processo_raiz'];
        $layout->sold_sinete_acab  = $input['sold_sinete_acab'];
        $layout->processo_acab  = $input['processo_acab'];
        $layout->metal_adicao  = $input['metal_adicao'];
        $layout->chanfro  = $input['chanfro'];
        $layout->nivel_inspecao  = $input['nivel_inspecao'];
        $layout->norma  = $input['norma'];
        $layout->observacao  = $input['observacao'];
        $layout->inicio_descricao  = $input['inicio_descricao'];
        $layout->criterio_aceite = $input['criterio_aceite'];
        
        
        $layout->save();
        return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
    }

    public function excluirLayout(){
        $id = Input::get('id');
        $layout = \App\Layout::find($id);
        $layout->delete();
        return Response::json(array('success' => true, 'mensagem' =>'Excluido com sucesso'));
    }

    public function carregar(){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%ensaio%')
                    ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $data['clientes'] = ['' => 'Selecione'] + $this->cliente
                        ->select(DB::raw('concat (cnpj ," - ",nome," - ",estado," - ",municipio)  as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');
        
        $data['funcionarios'] = ['0' => 'Selecione'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');

        $data['fontes'] = ['' => 'Selecione'] + $this->fonte->lists('numero','id'); 

        $input = array_except(Input::all(), array('_token'));
        // dd($input);

        $layout = DB::table('layouts')
                    ->where('cliente_id',$input['cliente_id'])
                    ->first();
        $data['propostas'] = $this->proposta
                        ->select(DB::raw('concat ("código atual: ",cod_proposta," / antiga: ",cod_proposta_anterior) as proposta,id'))
                        ->where('cliente_id', $input['cliente_id'])
                        ->lists('proposta','id');
        // $data['projeto'] = DB::table('projetos')
        //                 ->select(DB::raw('concat (servico," : ",estado) as projeto,id'))
        //                 ->where('id',$input['projeto_id'])->where('proposta_id',$input['proposta_id'])->lists('projeto','id');
        $data['projeto'] = $input['projeto'];
        $data['normas'] =  ['0' => 'Normas'] + \App\Normas::groupby('norma')->lists('norma','id');
        $data['unidades'] = \App\Unidade::select('unidade')->lists('unidade','unidade');
        $file = Input::file('arquivo');

        $path = public_path();

        if(isset($file))
        {
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $data['files'] = public_path().'/arquivos/'.$filename;
            $file->move($path.'/arquivos', $filename);

            $file = "/arquivos/".$filename;

            if(File::exists($path.$file))
            {  
                $objPHPExcel = \PHPExcel_IOFactory::load($path.$file);
                
                $worksheet = $objPHPExcel->getSheet(0);

                $highestRow         = $worksheet->getHighestRow(); 
                $highestColumn      = $worksheet->getHighestColumn(); 
                $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);

                if(!empty($layout->local_ensaio))           $data['local_ensaio'] = $worksheet->getCell($layout->local_ensaio)->getValue();

                if(!empty($layout->obra)){
                    $campo_obra = preg_match("/[^a-zA-Z\s]/", $layout->obra);  
                    if($campo_obra == 1){
                        $data['obra'] = $worksheet->getCell($layout->obra)->getValue();
                    }else{
                         $data['obra'] = $worksheet->getCell($layout->obra.$layout->inicio_descricao)->getValue();
                    }
                }


                               

                if(!empty($layout->contratante_fabricante)) $data['contratante_fabricante'] = $worksheet->getCell($layout->contratante_fabricante)->getValue();
                if(!empty($layout->data))                   $data['data_prog'] = $worksheet->getCell($layout->data)->getValue();
                if(!empty($layout->programacao))            $data['controle_cliente'] = $worksheet->getCell($layout->programacao)->getValue();

                if(isset($data['data_prog'])){
                    $auxd = substr($data['data_prog'],0,2);
                    $auxm = substr($data['data_prog'],3,2);
                    $auxa = substr($data['data_prog'],6,4);

                    if ($auxa && $auxm && $auxd) {
                        $data['data_prog'] = $auxa.'-'.$auxm.'-'.$auxd;
                    }else{
                        $data['data_prog'] = null;
                    }
                }else{
                    $data['data_prog'] = null;
                }
               
                
                

               $fields = array('item','iden_dese_iso','mapa_spool','obra','junta','posicao_soldagem','reforco','metal_base','diametro','espessura',
                'sold_sinete_raiz','sold_sinete_acab','processo_raiz','processo_acab','norma','nivel_inspecao','observacao','chanfro','criterio_aceite');
                
                $programacoes = array();
                $limite = $layout->inicio_descricao + $input['itens'];

                for ($row = $layout->inicio_descricao; $row < $limite ; $row++ ){   
                    $programacao = array();
                    foreach ($fields as $field) 
                    {
                        if($field == 'obra'){
                            $campo_obra = preg_match("/[^a-zA-Z\s]/", $layout->obra);  
                            if($campo_obra == 1){
                                $programacao[$field] = !empty($layout->$field) ? $worksheet->getCell($layout->$field)->getValue() : null;
                            }else{
                                $programacao[$field] = !empty($layout->$field) ? $worksheet->getCell($layout->$field.$row)->getValue() : null;
                            }
                        }else{
                            $programacao[$field] = !empty($layout->$field) ? $worksheet->getCell($layout->$field.$row)->getValue() : null;    
                        }
                    }
                    $programacao['item'] = intval($programacao['item']);
                    $id_norma =  DB::table('normas')->select('id')
                           ->where('norma','like','%'. $programacao['norma'].'%')
                           ->first();

                    if(count($id_norma) > 0 ){
                        $programacao['norma'] = $id_norma->id;
                    }else{ $programacao['norma'] = 0;}
                    
                    $programacoes[] = $programacao;
                } 
            }

        }
        $data['importprogramacao'] = $programacoes;
        
        $data['input'] = $input;
        return view('ensaio.guia.cadastrar')->with($data);
    }

    public function getguias(){
        $input = Input::all();
        $cliente = $input['cliente'];
        $cliente = DB::table('clientes')
        ->where('nome','like','%'.$cliente.'%')
        ->select('id','nome')
        ->first();

        $guias = DB::table('guias')->where('cliente_id',$cliente->id)->get();       
        return Response::json(array('success' => true, 'guias' =>$guias, 'cliente'=>$cliente));
    }


    public function filmereparo(){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%ensaio%')
                    ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();


        $data['juntasreprovadas'] =\App\Programacao::where('resultado','R')->groupby('guia_id','item')->get();

        $data['nomescliente'] = DB::table('guias')
                            ->leftjoin('clientes','guias.cliente_id','=','clientes.id')
                            ->select('clientes.nome', 'guias.id')
                            ->lists('clientes.nome', 'guias.id');

        // dd($data['filmesreprovados']);

        return view('ensaio.guia.filmereparo')->with($data);
    }
    public function gofilmereparo($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%ensaio%')
                    ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();


        $data['programacao'] = \App\Programacao::find($id);
        $data['filmesreprovados'] = \App\Programacao::where('guia_id', $data['programacao']->guia_id)
                                                    ->where('item', $data['programacao']->item)
                                                    ->where('resultado','R')
                                                    ->get();

        $data['filmes'] = \App\Programacao::where('guia_id', $data['programacao']->guia_id)
                                                    ->where('item', $data['programacao']->item)
                                                    ->get();




        $data['dadoscliente'] = DB::table('guias')
                            ->leftjoin('clientes','guias.cliente_id','=','clientes.id')
                            ->select('clientes.*')
                            ->where('guias.id', $data['programacao']->guia_id)
                            ->first();

        $data['proposta'] = DB::table('guias')
                            ->leftjoin('propostas','guias.proposta_id','=','propostas.id')
                            ->select('propostas.cod_proposta')
                            ->where('guias.id', $data['programacao']->guia_id)
                            ->first();

        $data['funcionarios'] = $data['funcionarios'] = ['0' => 'Selecione'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');

        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->where('local_atual',$unidade_id_user)
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('id');
            
        $data['fontes'] = ['0' => 'Selecione'] + $this->fonte
            ->where('status','ATIVO')
            ->whereIn('irradiador',$data['irradiadores'])
            ->lists('numero','id');; 

        return view('ensaio.guia.gofilmereparo')->with($data);
    }

    public function salvarguiafr(){
        $success = true;
        $input = array_except(Input::all(), array('_token'));

        $arrayfilmes = explode(',',$input['stringfilmes']);
        // dd($arrayfilmes);
        // dd($input);
        $dados_programacao = \App\Programacao::where('id',$input['programacao'])->first();
        
        $dadosguia = DB::table('guias')
                            ->leftjoin('propostas','guias.proposta_id','=','propostas.id')
                            ->where('guias.id', $dados_programacao->guia_id)
                            ->first();
        
        // dd( $dados_programacao['filme_classe']);
        // dd($input);

        $guia = new \App\Guia;
        $guia->data =  $input['data'];
        $guia->unidade =$dados_programacao->unidade;
        $guia->proposta_id = $dadosguia->proposta_id;
        $guia->cliente_id =$dadosguia->cliente_id;
        $guia->fonte_id = $input['fonte_id'];
        $guia->fonte_atividade = $input['fonte_atividade'];
        $guia->programacao_cliente = '';
        $guia->local_ensaio = '' ;
        $guia->data_prog = '' ;
        $guia->contratante_fabricante ='' ;

        $guia->save();

        $equipe = new \App\Equipe;
        $equipe->created_by = Auth::user()->id;
 
        $equipe->guia_id = $guia->id;
        $equipe->op1 = $input['op1'];
        $equipe->op2 = $input['op2'];
        $equipe->aux = isset($input['aux']) ? $input['aux'] : '';
        $equipe->ins = $input['ins'];
        $equipe->unidade = $dados_programacao->unidade;
        $equipe->save();

        foreach ($arrayfilmes as $key => $value) {
        
            // $procedimento = \App\Parametrosprocedimentos::find($proc[$item]->id);
            $dados_prog = \App\Programacao::where('guia_id',$dados_programacao->guia_id)
                                            ->where('item',$dados_programacao->item)
                                            ->where('numero_filme',str_replace('F', '',$value))
                                            ->first();
            $insert = array();
            $insert['item'] = 1;
            $insert['guia_id'] =  $guia->id;
            $insert['unidade'] =  $guia->unidade;
            $insert['identificacao'] = $dados_prog->identificacao ;
            $insert['spool'] = $dados_prog->spool;
            $insert['obra'] = $dados_prog->obra;
            $insert['junta'] = $dados_prog->junta . ' - NR';
            $insert['posicao_soldagem'] = $dados_prog->posicao_soldagem;
            $insert['metal_base'] = $dados_prog->metal_base;
            $insert['diametro'] = $dados_prog->diametro;
            $insert['espessura'] = $dados_prog->espessura;
            $insert['sinete_raiz'] = $dados_prog->sinete_raiz;
            $insert['processo_raiz'] = $dados_prog->processo_raiz;
            $insert['sinete_acabamento'] = $dados_prog->sinete_acabamento;
            $insert['processo_acabamento'] = $dados_prog->processo_acabamento;
            $insert['norma'] = $dados_prog->norma;
            $insert['nivel_inspecao'] = $dados_prog->nivel_inspecao;
            $insert['procedimento'] = $dados_prog->procedimento;
            $insert['tecnica'] = $dados_prog->tecnica;
            $insert['dff'] =  $dados_prog->dff;
            $insert['tipo_iqi'] =  $dados_prog->tipo_iqi;
            $insert['fio_iqi'] =  $dados_prog->fio_iqi;
            $insert['chanfro'] = $dados_prog->chanfro;
            $insert['filme_dimensao'] = $dados_prog->filme_dimensao;
            $insert['filme_classe'] = $dados_prog['filme_classe'];
            $insert['quantidade_filmes'] = count($arrayfilmes);
            $insert['criterio_aceitacao'] = $dados_prog->criterio_aceitacao;
            $insert['observacao'] = $dados_prog->observacao;
            $insert['reforco']    = $dados_prog->reforco;
            $insert['tipo'] = $dados_prog->tipo;



            $id_cliente = DB::table('guias')->select('cliente_id')->where('id',$guia->id)->first();
            
            $seqfilme = \App\Sequencialfilmes::where('id_cliente','=',$id_cliente->cliente_id)->first();
            $seqfilme =  \App\Sequencialfilmes::find($seqfilme->id);
            $auxseq = $seqfilme->sequencial+1;
            $insert['isometrico'] = $auxseq;
            $insert['numero_filme'] = $dados_prog->numero_filme;
            $insert['created_by'] = Auth::user()->id;
            $this->programacao->create($insert);
            $seqfilme->sequencial = $seqfilme->sequencial+1;
            $seqfilme->save();

            $nr = \App\Programacao::find($dados_prog->id);
            $nr->resultado = 'NR';
            $nr->save();
            
        }
            return Response::json(array('success' => $success, 'idguia' =>  $guia->id));
    }
    public function getdadosnormas(){
        $success = true;
        $normas = \App\Normas::groupby('norma')->lists('norma','id');

         return Response::json(array('success' => $success, 'normas' =>  $normas));
    }

    public function salvarrelatorio(){
        $success = true;
        $input = Input::all();
        $num_rel_cli = $input['num_rel_cli'];
        $rel = \App\Relatorio::where('guia_id',$input['guia_id'])->get();
        $guia = \App\Guia::find($input['guia_id']);
        
        if(count($rel) == 0){
            foreach ($input['numeros_relatorios'] as $key => $value) {
                $num_rel_cli++;
                $relatorio = new \App\Relatorio;
                $relatorio->guia_id = $input['guia_id'];
                $relatorio->numero_relatorio = $value;
                $relatorio->numero_rel_cliente = $num_rel_cli;
                $relatorio->versao = 0;
                $relatorio->itens = $input['itens_relatorio'][$value];
                $relatorio->solicitacao_cliente = $guia->programacao_cliente;
                $relatorio->contratante_fabricante = $guia->contratante_fabricante;
                $relatorio->projeto = $guia->projeto;
                $relatorio->local = $guia->local_ensaio;

                $relatorio->unidade = $guia->unidade;
                $relatorio->save();

                $aux = explode('+',$input['itens_relatorio'][$value]);

                foreach ($aux as $key => $value) {
                    $programacao = \App\Programacao::where('guia_id',$relatorio->guia_id)->where('item',$value)->get();
                    foreach ($programacao as $key => $pro) {
                        $insert = $pro->toArray();
                        unset($insert['id']);
                        $insert['id_relatorio'] = $relatorio->numero_relatorio;
                        $insert['versao'] = 0;
                        $progrel = new \App\Programacaorelatorio;
                        $progrel->insert($insert);
                    }
                }
                
            }
        }else{
            foreach ($rel as $key => $value) {
                
            }
        }

        return Response::json(array('success' => $success));
        
    }

    public function getRelatorios(){
        $success = true;
        $input = Input::all();
        $guia = \App\Guia::where('id',$input['guia_id'])->first();
        $relatorios = \App\Relatorio::where('guia_id',$input['guia_id'])->where('unidade',$guia->unidade)->get()->toArray();
        $item = [];
        foreach ($relatorios as $key => $value) {
            $itens = explode('+', $value['itens']);
            $it = [];
            foreach ($itens as $key => $i) {
                $it[] =\App\Programacaorelatorio::where('id_relatorio',$value['numero_relatorio'])->where('unidade',$guia->unidade)->where('item',$i)->where('versao',$value['versao'])->first();
            }
            $item[$value['numero_relatorio']] = $it;
        }
        return Response::json(array('success' => $success, 'relatorios' =>  $relatorios, 'item' => $item, 'guia'=>$guia));
    }

    public function salvarrelatorioeditado(){
        $input = Input::all();
        $relatorio = \App\Relatorio::find($input['id_relatorio']);
        $relatorio->numero_rel_cliente = $input['num_rel_cliente'];
        $relatorio->solicitacao_cliente = $input['prog_cliente'];
        $relatorio->contratante_fabricante = $input['contrafabri'];
        $relatorio->projeto = $input['projeto'];
        $relatorio->local = $input['local'];
        $relatorio->data = $input['datarel'];
        $relatorio->versao = $relatorio->versao+1;
        $relatorio->save();

        foreach ($input['arrayitem'] as $key => $item) {

            $prograrel = \App\Programacaorelatorio::where('id_relatorio',$relatorio->numero_relatorio)->where('item',$item)->where('versao',$relatorio->versao-1)->get();
            foreach ($prograrel as $key => $pro) {
                $insert = $pro->toArray();
                // dd($insert);
                unset($insert['id']);
                $insert['versao'] =  $relatorio->versao;
                $insert['identificacao'] = $input['arrayidentificacao'][$item] ;
                $insert['spool'] = $input['arrayspool'][$item] ;
                $insert['junta'] = $input['arrayjunta'][$item];
                $insert['posicao_soldagem'] = $input['arrayposicao'][$item];
                $insert['metal_base'] = $input['arraymetal_base'][$item];
                $insert['sinete_raiz'] = $input['arraysinete_raiz'][$item];
                $insert['sinete_acabamento'] = $input['arraysinete_ench'][$item];

                $aux = explode('/',$input['arrayprocesso'][$item]);
                $insert['processo_raiz'] = $aux[0];
                $insert['processo_acabamento'] = isset($aux[1])? $aux[1] :$aux[0];

                $progrel = new \App\Programacaorelatorio;
                $progrel->insert($insert);
            }
        }
        echo "OK";
    }

   


    public function novorel($id){
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        // $normas = ['0' => 'Selecione Norma'] + DB::table('programacoes')
        //                 ->leftjoin('normas','normas.id','=','programacoes.norma')
        //                 ->select('normas.norma as n','normas.id as i')
        //                 ->where('programacoes.guia_id','=',$id)
        //                 ->where('programacoes.executado','ok')
        //                 ->groupby('programacoes.norma')
        //                 ->lists('n','i'); 
        //                 dd($normas);


        $guia = $this->guia->with(['cliente','funcionarios','proposta','fonte'])->findOrFail($id);
        $data['guia'] = $guia;



        $normas = DB::table('programacoes')
                        ->where('guia_id','=',$id)
                        ->where('executado','ok')
                        ->groupby('norma')
                        ->get();


        if($guia->status == 2){              
        $normaids = [];
        foreach ($normas as $norma) {
            foreach (explode('+', $norma->norma) as $key => $value) {
                $normaids[] = $value;
            }
        }

        if($normas[0]->tipo == 2){
            $selectnormas = ['0' => 'Selecione Norma'];
            foreach ($normas as $key => $value) {
                $aux = explode('+',$value->norma);
                foreach ($aux as $key => $a) {
                    $selectnormas[$a] = $a;
                }
                
            }
        }else{


            $selectnormas = ['0' => 'Selecione Norma'] + DB::table('normas')
                            ->whereIn('id',$normaids)
                            ->lists('norma','id'); 

           
        }
        
        $data['normas'] = $selectnormas;

        $data['nomenormas'] = \App\Normas::lists('norma','id');
        
            $data['itens'] = $this->programacao
                            ->where('guia_id', $guia->id)
                            ->where('unidade', $guia->unidade)
                            ->where('executado','OK')
                            ->groupby('item')
                            ->get()->toArray();

        $data['num_rel_cli'] = \DB::table('relatorios')
                                ->join('guias','guias.id','=','relatorios.guia_id')
                                // ->select(\DB::raw('MAX(relatorios.numero_rel_cliente) as numero'))
                                // ->select(\DB::raw('MAX(CAST(relatorios.numero_rel_cliente as UNSIGNED INT)) as numero'))
                                ->where('guias.cliente_id',$guia->cliente_id)
                                ->orderby('relatorios.id','desc')
                                ->first();
                                // ->pluck('numero');


        if(isset($data['num_rel_cli']->numero_rel_cliente)){
            $numero = preg_replace("/[^0-9\s]/", "", $data['num_rel_cli']->numero_rel_cliente);
            $letra =  preg_replace("/[^a-zA-Z\s]/", "", $data['num_rel_cli']->numero_rel_cliente);
        
            $num_rel = str_replace($numero, $numero+1,$data['num_rel_cli']->numero_rel_cliente);
            $data['num_rel_cli'] =  $num_rel;
        }else{
            $data['num_rel_cli'] =  0;
        }
        return view('ensaio.guia.relatorio.criarrelatorio')->with($data);
        }
        $data['rota'] = '/guia/'.$id.'/visualizar'; 
        $data['mensagem'] = 'Guia Operacional sem resultados, inclua os resultados para Criar os relatorios';
        return view('erros')->with($data);
    
    }

    public function getdadosrelatorios(){
        $input  = Input::all();
        $dadositem = $this->programacao
                            ->where('id', $input['id'])
                            ->get();

        $filmes = $this->programacao
                            ->where('guia_id', $dadositem[0]->guia_id)
                            ->where('item', $dadositem[0]->item)
                            ->get();

        return Response::json(array('success' => true, 'filmes' =>$filmes));
    }

    public function getprogramacoesrelatorios(){
        $input  = Input::all();
        $programacoes =  $this->programacao
                            ->where('guia_id', $input['guia_id'])
                            ->where('norma','like','%'.$input['norma'].'%')
                            ->where('executado','OK')
                            ->groupby('item')
                            ->get();

        return Response::json(array('success' => true, 'programacoes' =>$programacoes));
    }

    public function salvarNovoRelatorio(){
        $success  = true;
        $input    = Input::all();
        $guia = \App\Guia::find($input['guia_id']);
        
        $sequencial  = \App\Sequencialunidade::where('unidade',$guia->unidade)->get();
        if(count($sequencial) > 0){
            $sequencial  = $sequencial[0];
        }else{
            $sequencial = new \App\Sequencialunidade;
            $sequencial->unidade    = $guia->unidade;
            $sequencial->sequencial = 0;
            $sequencial->save();
        }
        
        $num_rel_cli = $input['numerorelatoriocliente'];
        
        
        $auxItens = [];
        foreach ($input['filmes'] as $key => $value) {
           $auxItens[] = \App\Programacao::where('id',$value)->pluck('item');
        }
        $auxItens = array_unique($auxItens);
        $string_itens = implode('+', $auxItens);
        

        $relatorio                           = new \App\Relatorio;
        $relatorio->guia_id                  = $input['guia_id'];
        $relatorio->numero_relatorio         = $sequencial->sequencial+1;
        $relatorio->numero_rel_cliente       = $num_rel_cli;
        $relatorio->versao                   = 0;
        $relatorio->itens                    = $string_itens;
        $relatorio->solicitacao_cliente      = $input['programacao_cliente'];
        $relatorio->contratante_fabricante   = $input['contratante'];
        $relatorio->projeto                  = $input['projeto'];
        $relatorio->local                    = $input['local'];
        $relatorio->data                     = $input['datarel'];
        $relatorio->norma                    = $input['norma_id'];
        $relatorio->criterio                 = $input['criterio'];
        $relatorio->unidade                  = $guia->unidade;
        $relatorio->save();

        foreach ($input['filmes'] as $key => $value) {
            $programacao = \App\Programacao::where('id',$value)->get();
            foreach ($programacao as $key => $pro) {
                $insert = $pro->toArray();
                unset($insert['id']);
                $insert['id_relatorio'] = $relatorio->numero_relatorio;
                $insert['versao'] = $relatorio->versao;
                $progrel = new \App\Programacaorelatorio;
                $progrel->insert($insert);
            }
        }
               

        $sequencial->sequencial = $sequencial->sequencial+1;
        $sequencial->save();

        $gorelatorios = \App\Gorelatorios::where('id_go',$input['guia_id'])->get();
        if(count($gorelatorios) > 0){
            $gorelatorios[0]->idsrelatorios = $gorelatorios[0]->idsrelatorios.';'.$relatorio->numero_relatorio;
            $gorelatorios[0]->save();
        }else{
            $gorel = new \App\Gorelatorios;
            $gorel->id_go         = $input['guia_id'];
            $gorel->idsrelatorios = $relatorio->numero_relatorio;
            $gorel->unidade       = $guia->unidade;
            $gorel->save();
        }

        return Response::json(array('success' => $success));
    }

    public function gonaoreal($id){
        $data['success'] = true;
        
        $guia         = \DB::table('guias')->where('id',$id)->first();
        $equipe       = \DB::table('equipes')->where('guia_id',$id)->first(); 
        $programacoes = \DB::table('programacoes')->where('guia_id',$id)->where('executado','N Realizada')->get(); 


        $nova_guia = new \App\Guia;
        $nova_guia->data = date("Y-m-d");
        $nova_guia->unidade = $guia->unidade;
        $nova_guia->proposta_id = $guia->proposta_id;
        $nova_guia->cliente_id = $guia->cliente_id;
        $nova_guia->fonte_id = $guia->fonte_id;
        $nova_guia->fonte_atividade = $guia->fonte_atividade;
        $nova_guia->programacao_cliente = $guia->programacao_cliente;
        $nova_guia->local_ensaio = $guia->local_ensaio;
        $nova_guia->data_prog = $guia->data_prog;
        $nova_guia->contratante_fabricante = $guia->contratante_fabricante;
        $nova_guia->programacao_fabricante = $guia->programacao_fabricante;
        $nova_guia->projeto = $guia->projeto;
        $nova_guia->ocorrencias = 'juntas reprogramadas da guia '.$guia->id;
        $nova_guia->save();
    
        $up['ocorrencias'] = 'juntas reprogramadas na guia '.$nova_guia->id;
        \DB::table('guias')->where('id',$id)->update($up);

        $nova_equipe = new \App\equipe;     
        $nova_equipe->guia_id = $nova_guia->id;
        $nova_equipe->op1 = $equipe->op1;
        $nova_equipe->op2 = $equipe->op2;
        $nova_equipe->aux = $equipe->aux;
        $nova_equipe->ins = $equipe->ins;
        $nova_equipe->unidade = $equipe->unidade;
        $nova_equipe->save();
        
        foreach ($programacoes as $key => $value) {
            // $up['observacao'] = 'junta reprogramada na guia '.$nova_guia->id;
            // \DB::table('programacoes')->where('id',$value->id)->update($up);
            $aux = (array) $value;
            $aux['guia_id'] = $nova_guia->id;
            unset($aux['id']);
            unset($aux['executado']);
            unset($aux['observacao']);

            // $aux ['observacao'] = 'junta reprogramada da guia '.$value->guia_id;
            $prog = new \App\Programacao;
            $prog->insert($aux);
        }

        $data['id'] = $nova_guia->id;

        return Response::json($data);
    }


    public function getfontes(){
        $data['success'] = true;
        $input    = Input::all();
        $unidade_id = \DB::table('unidade')->where('unidade','like', $input['uni'])->pluck('id');
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->where('local_atual',$unidade_id)
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('id');
        $data['fontes'] = $this->fonte
            ->where('status','ATIVO')
            ->whereIn('irradiador',$data['irradiadores'])
            ->lists('numero','id');

        return Response::json($data);
    }
}
