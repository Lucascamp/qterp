<?php namespace App\Http\Controllers\ensaio;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Permissaousuario;
use App\Guia;
use App\Cliente;
use App\Funcionario;
use App\Proposta;
use App\Fonte;
use App\Programacao;
use App\Projeto;
use Carbon\Carbon;
use App\Filme;

class BoletimController extends Controller 
{
        public function __construct(
                                    Guia $guia,
                                    Cliente $cliente,
                                    Funcionario $funcionario,
                                    Proposta $proposta,
                                    Fonte $fonte,
                                    Programacao $programacao,
                                    Projeto $projeto,
                                    Filme $filme,
                                    \App\Junta $junta
                                    )
        {
                $this->middleware('auth');
                $this->guia = $guia;
                $this->cliente = $cliente;
                $this->funcionario = $funcionario;
                $this->proposta = $proposta;
                $this->fonte = $fonte;
                $this->programacao = $programacao;
                $this->projeto = $projeto;
                $this->filme = $filme;
                $this->junta = $junta;
        }

    public function index()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%ensaio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

       
        $data['usuario'] = Auth::user();
        $data['bms'] = \App\Bm::all();
        $clientes = $this->cliente
                        ->select(DB::raw('concat (nome_reduz) as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');
        $data['clientes'] = $clientes;


        return view('ensaio.boletim.home')->with($data);
    }

    public function criar()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%ensaio%')
                    ->pluck('id');

        $clientes = $this->cliente
                        ->select(DB::raw('concat (nome_reduz," - ",estado," - ",municipio) as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');

        $filmes = $this->filme->groupBy('legenda')->get();

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();
        $data['clientes'] = ['' => 'Selecione o cliente'] + $clientes;
        $data['filmes'] = $filmes;



        return view('ensaio.boletim.criar')->with($data);
    }

    public function salvar()
    {
        $input = array_except(Input::all(), array('_token'));
       
    
        $bm = new \App\bm;
        $bm->guia_id     = $input['idsguia'];
        $bm->cliente_id  = $input['cliente_id'];
        $bm->periodo_ini = $input['data_ini'];
        $bm->periodo_fim = $input['data_fim'];
        $bm->valor_total = $input['valor_total'];
        $bm->proposta_id = $input['proposta_id'];
        $bm->unidade = Auth::user()->unidade;
        $bm->save();

        return Response::json(array('success' => true, 'msg' => 'Salvo com sucesso'));
    }

    public function carregar(){
        $input = Input::all();
       
        $id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%ensaio%')
                    ->pluck('id');

        $filmes = $this->filme->select('legenda','descricao')->groupBy('legenda')->lists('descricao','legenda');   
        $juntas = $this->junta->select('id','descricao')->groupBy('id')->lists('descricao','id');   

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();

        $clientes = ['' => 'Selecione o Cliente'] + $this->cliente
                        ->select(DB::raw('concat (nome_reduz) as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');

        if ( empty($input['cliente_id']) || empty($input['proposta_id']) || empty($input['inicio']) || empty($input['fim'])) {
            $data['erro'] = true;
            $data['msg'] = 'Todos os itens são obrigatorios';
            $data['clientes'] = $clientes;
            return view('ensaio.boletim.criar')->with($data);
        }

        $proposta = $this->proposta->with(['filme', 'jornada'])->findOrFail($input['proposta_id']);
        $data_inicio = Carbon::createFromFormat('Y-m-d', $input['inicio']);
        $data_fim = Carbon::createFromFormat('Y-m-d', $input['fim']);

        $cliente = $this->cliente->findOrFail($input['cliente_id']);

        $projeto = $this->projeto->where('proposta_id', $input['proposta_id'])->first();

        $quantidades = [];
        $total = 0;
        $quant_diaria_dom = 0;
        $quant_extra_dom  = 0;  
        $quant_diaria_normal = 0;  
        $quant_extra_normal = 0;  
        $valores_diarias = [];
        $valores_diarias['domingo'] = 0;
        $valores_diarias['normal'] = 0;
        $total_domingo = 0;  
        $total_extra_domingo = 0;  
        $total_normal = 0;  
        $total_extra_normal = 0;  
        $diametros = [];

        foreach ($proposta->filme as $key => $filme) 
        {
            $valores[$filme->legenda] =  DB::table('proposta_filme')
                                            ->where('proposta_id', $proposta->id)
                                            ->where('filme_id', $filme->id)
                                            ->pluck('valor');

        }
        
        $guias = $this->guia->where('cliente_id', $cliente->id)
                             ->where('proposta_id', $proposta->id)
                             ->where('status',2)
                             ->whereBetween('data', [$data_inicio, $data_fim])
                             ->orderBy('data')
                             ->get();

        if (count($guias) == 0) {
            $data['erro'] = true;
            $data['msg'] = 'Nenhuma Guia para o periodo';
            $data['clientes'] = $clientes;
            return view('ensaio.boletim.criar')->with($data);
        }

        $valor_km = DB::table('proposta_servico')->select('valor')->where('servico_id',1)->where('proposta_id',$proposta->id)->first();

        $valores_diametros = DB::table('proposta_junta')
                            ->select('junta_id','valor')
                            ->where('proposta_id',$proposta->id)
                            ->groupby('junta_id')
                            ->lists('valor','junta_id');
        
        $arrayids  = [];

        $valor_diaria = DB::table('proposta_jornada')
                                    ->select('valor_normal', 'valor_extra')
                                    ->where('proposta_id', $proposta->id)
                                    ->where('jornada_id', 1)
                                    ->first();

        $valor_diaria_domingo = DB::table('proposta_jornada')
                        ->select('valor_normal', 'valor_extra')
                        ->where('proposta_id', $proposta->id)
                        ->where('jornada_id', 3)
                        ->first();

        $valores_diarias['normal'] =  $valor_diaria;

       
        $valores_diarias['domingo'] =  $valor_diaria_domingo ;



        $array_forma = explode(';', $proposta->forma_cobranca);
        $array_dimensoes = [];
        $array_diametros = [];
        $programacoes = [];
        foreach ($guias as $guia)
        {   
            $arrayids[] = $guia->id;  
            if($guia->local_ensaio == 'QUALITEC'){
                if(in_array('diaQ', $array_forma)){
                    $now = Carbon::now();

                    $now->hour=0;
                    $now->minute=0;
                    $now->second=0;  

                    $minutos_total = $guia->entrada->diffInMinutes($guia->saida);
                    $minutos_total -= 60;//1 hora de almoço

                    $contratadas = Carbon::createFromFormat('H:i:s', $guia->horas_contratadas);
                    $minutos_contratados = $now->diffInMinutes($contratadas);

                    $extra = $minutos_total - $minutos_contratados;
                     
                    if($guia->data->dayOfWeek == Carbon::SUNDAY)
                    {
                        $id_jornada = 3;
                        $guia['extra_dom'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_dom'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_dom +=  $guia['diaria_dom'];
                        $quant_extra_dom +=  $guia['extra_dom'];
                    }   
                    else  
                    {  
                        $id_jornada = 1;
                        $guia['extra_normal'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_normal'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_normal +=  $guia['diaria_normal'];
                        $quant_extra_normal +=  $guia['extra_normal'];
                    }

                    if($guia->data->dayOfWeek == Carbon::SUNDAY and !isset($guia['valor_domingo']))
                    {
                        $valores_diarias['domingo'] = $valor_diaria_domingo;
                        $guia['total_diaria'] = $guia['diaria_dom']*$valor_diaria_domingo->valor_normal;    
                        $guia['total_extra'] = $guia['extra_dom']*$valor_diaria_domingo->valor_extra;

                        $total_domingo +=  $guia['total_diaria'];
                        $total_extra_domingo += $guia['total_extra'];
                    } 

                    elseif(!isset($guia['valor_normal'])) 
                    {   
                        $valores_diarias['normal'] = $valor_diaria;
                        $guia['total_diaria'] = $guia['diaria_normal']*$valor_diaria->valor_normal;
                        $guia['total_extra'] = $guia['extra_normal']*$valor_diaria->valor_extra;

                        $total_normal +=  $guia['total_diaria'];
                        $total_extra_normal += $guia['total_extra'];
                    }                

                    $total += $guia['total_diaria'];
                    $total += $guia['total_extra'];
                }
                if(in_array('filmeQ', $array_forma)){
                    $programacoes[$guia->id] =  DB::table('programacoes')
                                ->select('quantidade_filmes', 'filme_dimensao')
                                ->where('guia_id', $guia->id)
                                ->where('executado', 'OK')
                                ->groupby('item')
                                ->get();
                    foreach ($programacoes[$guia->id] as $key => $value) {
                        $array_dimensoes[] = $value->filme_dimensao;
                    }
                    
                }
                if(in_array('juntaQ', $array_forma)){
                    $diametros[$guia->id] = DB::table('guiaproducao')
                            ->select('junta_id','quantidade')
                            ->where('guiaproducao.guia_id',$guia->id)
                            ->where("guiaproducao.quantidade",'>',0)
                            ->get();

                    foreach ($diametros[$guia->id] as $key => $value) {
                        $array_diametros[] = $value->junta_id;
                    }
                }
            }
            if($guia->local_ensaio == 'CLIENTE'){
                if(in_array('diaC', $array_forma)){
                    $now = Carbon::now();

                    $now->hour=0;
                    $now->minute=0;
                    $now->second=0;  

                    $minutos_total = $guia->entrada->diffInMinutes($guia->saida);
                    $minutos_total -= 60;//1 hora de almoço

                    $contratadas = Carbon::createFromFormat('H:i:s', $guia->horas_contratadas);
                    $minutos_contratados = $now->diffInMinutes($contratadas);

                    $extra = $minutos_total - $minutos_contratados;
                     
                    if($guia->data->dayOfWeek == Carbon::SUNDAY)
                    {
                        $id_jornada = 3;
                        $guia['extra_dom'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_dom'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_dom +=  $guia['diaria_dom'];
                        $quant_extra_dom +=  $guia['extra_dom'];
                    }   
                    else  
                    {  
                        $id_jornada = 1;
                        $guia['extra_normal'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_normal'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_normal +=  $guia['diaria_normal'];
                        $quant_extra_normal +=  $guia['extra_normal'];
                    }

                    if($guia->data->dayOfWeek == Carbon::SUNDAY and !isset($guia['valor_domingo']))
                    {
                        $valores_diarias['domingo'] = $valor_diaria_domingo;
                        $guia['total_diaria'] = $guia['diaria_dom']*$valor_diaria_domingo->valor_normal;    
                        $guia['total_extra'] = $guia['extra_dom']*$valor_diaria_domingo->valor_extra;

                        $total_domingo +=  $guia['total_diaria'];
                        $total_extra_domingo += $guia['total_extra'];
                    } 

                    elseif(!isset($guia['valor_normal'])) 
                    {   
                        $valores_diarias['normal'] = $valor_diaria;
                        $guia['total_diaria'] = $guia['diaria_normal']*$valor_diaria->valor_normal;
                        $guia['total_extra'] = $guia['extra_normal']*$valor_diaria->valor_extra;

                        $total_normal +=  $guia['total_diaria'];
                        $total_extra_normal += $guia['total_extra'];
                    }                

                    $total += $guia['total_diaria'];
                    $total += $guia['total_extra'];
                }
                if(in_array('filmeC', $array_forma)){
                    $programacoes[$guia->id] =  DB::table('programacoes')
                                ->select('quantidade_filmes', 'filme_dimensao')
                                ->where('guia_id', $guia->id)
                                ->where('executado', 'OK')
                                ->groupby('item')
                                ->get();
                    foreach ($programacoes[$guia->id] as $key => $value) {
                        $array_dimensoes[] = $value->filme_dimensao;
                    }
                }
                if(in_array('juntaC', $array_forma)){
                    $diametros[$guia->id] = DB::table('guiaproducao')
                            ->select('junta_id','quantidade')
                            ->where('guiaproducao.guia_id',$guia->id)
                            ->where("guiaproducao.quantidade",'>',0)
                            ->get();
                    foreach ($diametros[$guia->id] as $key => $value) {
                        $array_diametros[] = $value->junta_id;
                    }
                }
                
            }
            // $arrayids[] = $guia->id;
            // $programacoes[$guia->id] =  DB::table('programacoes')
            //                     ->select('quantidade_filmes', 'filme_dimensao')
            //                     ->where('guia_id', $guia->id)
            //                     ->where('executado', 'OK')
            //                     ->groupby('item')
            //                     ->get();

            // $diametros[$guia->id] = DB::table('guiaproducao')
            //                 ->select('junta_id','quantidade')
            //                 ->where('guiaproducao.guia_id',$guia->id)
            //                 ->where("guiaproducao.quantidade",'>',0)
            //                 ->get();

            // $now = Carbon::now();

            // $now->hour=0;
            // $now->minute=0;
            // $now->second=0;  

            // $minutos_total = $guia->entrada->diffInMinutes($guia->saida);
            // $minutos_total -= 60;//1 hora de almoço

            // $contratadas = Carbon::createFromFormat('H:i:s', $guia->horas_contratadas);
            // $minutos_contratados = $now->diffInMinutes($contratadas);

            // $extra = $minutos_total - $minutos_contratados;
            // if($guia->local_ensaio == 'CLIENTE'){
            //     if($guia->data->dayOfWeek == Carbon::SUNDAY)
            //     {
            //         $id_jornada = 3;
            //         $guia['extra_dom'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
            //         $guia['diaria_dom'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
            //         $quant_diaria_dom +=  $guia['diaria_dom'];
            //         $quant_extra_dom +=  $guia['extra_dom'];
            //     }   
            //     else  
            //     {  
            //         $id_jornada = 1;
            //         $guia['extra_normal'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
            //         $guia['diaria_normal'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
            //         $quant_diaria_normal +=  $guia['diaria_normal'];
            //         $quant_extra_normal +=  $guia['extra_normal'];
            //     }
            // }
            // $valor_diaria = DB::table('proposta_jornada')
            //                 ->select('valor_normal', 'valor_extra')
            //                 ->where('proposta_id', $proposta->id)
            //                 ->where('jornada_id', 1)
            //                 ->first();


            // $valor_diaria_domingo = DB::table('proposta_jornada')
            //                 ->select('valor_normal', 'valor_extra')
            //                 ->where('proposta_id', $proposta->id)
            //                 ->where('jornada_id', 3)
            //                 ->first();

            // if($guia->data->dayOfWeek == Carbon::SUNDAY and !isset($guia['valor_domingo']))
            // {
            //     $valores_diarias['domingo'] = $valor_diaria_domingo;
            //     $guia['total_diaria'] = $guia['diaria_dom']*$valor_diaria_domingo->valor_normal;    
            //     $guia['total_extra'] = $guia['extra_dom']*$valor_diaria_domingo->valor_extra;

            //     $total_domingo +=  $guia['total_diaria'];
            //     $total_extra_domingo += $guia['total_extra'];
            // } 

            // elseif(!isset($guia['valor_normal'])) 
            // {   
            //     $valores_diarias['normal'] = $valor_diaria;
            //     $guia['total_diaria'] = $guia['diaria_normal']*$valor_diaria->valor_normal;
            //     $guia['total_extra'] = $guia['extra_normal']*$valor_diaria->valor_extra;

            //     $total_normal +=  $guia['total_diaria'];
            //     $total_extra_normal += $guia['total_extra'];
            // }                

            // $total += $guia['total_diaria'];
            // $total += $guia['total_extra'];
        }
        
        $stringids = implode(';',$arrayids);

        $data['inicio'] = $data_inicio;
        $data['fim'] = $data_fim;
        $data_inicio = $data_inicio->format('d/m/Y');
        
        $data_fim  = $data_fim->format('d/m/Y');

        $data['stringids'] = $stringids;
        $data['cliente'] = $cliente;
        $data['proposta'] = $proposta;
        $data['projeto'] = $projeto;
        $data['guias'] = $guias;
        // $data['quantidades'] = $quantidades;
        $data['valores'] = $valores;
        $data['valores_diarias'] = $valores_diarias;
        $data['programacoes'] = $programacoes;
        // $data['resultado'] = $resultado;
        $data['total'] = $total;
        $data['total_domingo'] = $total_domingo;
        $data['total_extra_domingo'] = $total_extra_domingo;
        $data['total_normal'] = $total_normal;
        $data['total_extra_normal'] = $total_extra_normal;
        $data['quant_diaria_dom'] = $quant_diaria_dom;
        $data['quant_extra_dom'] = $quant_extra_dom;
        $data['quant_diaria_normal'] = $quant_diaria_normal;
        $data['quant_extra_normal'] = $quant_extra_normal;
        $data['data_inicio'] = $data_inicio;
        $data['data_fim'] = $data_fim;
        $data['valor_km'] = $valor_km->valor;
        $data['diametros'] = $diametros;
        $data['valores_diametros'] = $valores_diametros;
        $data['valor_diaria_domingo'] = $valor_diaria_domingo;
        $data['clientes'] = ['' => 'Selecione o cliente'] + $clientes;
        $data['filmes'] = $filmes;
        $array_dimensoes = array_unique($array_dimensoes);
        array_multisort($array_dimensoes,SORT_ASC);
        $data['array_dimensoes'] = $array_dimensoes;
        $array_diametros = array_unique($array_diametros);
        array_multisort($array_diametros,SORT_ASC);
        $data['array_diametros'] = $array_diametros ;
        $data['juntas'] = $juntas;
        // dd($data['array_diametros']);
        
        return view('ensaio.boletim.carregar')->with($data);
    }

    public function visualizar($id){
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%ensaio%')
                    ->pluck('id');

        $clientes = $this->cliente
                        ->select(DB::raw('concat (nome_reduz) as cliente, id'))
                        ->whereNull('deleted_at')
                        ->lists('cliente','id');

        $filmes = $this->filme->select('legenda','descricao')->groupBy('legenda')->lists('descricao','legenda');   
        $juntas = $this->junta->select('id','descricao')->groupBy('id')->lists('descricao','id'); 

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();
        
        $bm = \App\Bm::find($id);
        $proposta = $this->proposta->with(['filme', 'jornada'])->findOrFail($bm->proposta_id);
        
        $ids_guias = explode(';',$bm->guia_id);       
        $cliente_id = $bm->cliente_id;
        $proposta_id =  $bm->proposta_id;

        
        $data_inicio = Carbon::createFromFormat('Y-m-d H:i:s', $bm->periodo_ini);
        $data_fim = Carbon::createFromFormat('Y-m-d H:i:s', $bm->periodo_fim);

        $cliente = $this->cliente->findOrFail($cliente_id); 

        $projeto = $this->projeto->where('proposta_id', $proposta_id)->first();

        $quantidades = [];
        $total = 0;
        $quant_diaria_dom = 0;
        $quant_extra_dom  = 0;  
        $quant_diaria_normal = 0;  
        $quant_extra_normal = 0;  
        $valores_diarias = [];
        
        
        $total_domingo = 0;  
        $total_extra_domingo = 0;  
        $total_normal = 0;  
        $total_extra_normal = 0;  
        $diametros = [];

        foreach ($proposta->filme as $key => $filme) 
        {
            $valores[$filme->legenda] =  DB::table('proposta_filme')
                                            ->where('proposta_id', $proposta->id)
                                            ->where('filme_id', $filme->id)
                                            ->pluck('valor');

        }
        
        $guias = $this->guia->where('cliente_id', $cliente->id)
                             ->where('proposta_id', $proposta->id)
                             ->where('status',2)
                             ->whereIn('id',$ids_guias)
                             ->orderBy('data')
                             ->get();


        $valor_km = DB::table('proposta_servico')->select('valor')->where('servico_id',1)->where('proposta_id',$proposta->id)->first();

        $valores_diametros = DB::table('proposta_junta')
                            ->select('junta_id','valor')
                            ->where('proposta_id',$proposta->id)
                            ->groupby('junta_id')
                            ->lists('valor','junta_id');

        $valor_diaria = DB::table('proposta_jornada')
                                    ->select('valor_normal', 'valor_extra')
                                    ->where('proposta_id', $proposta->id)
                                    ->where('jornada_id', 1)
                                    ->first();
        $valores_diarias['normal'] =  $valor_diaria;

        $valor_diaria_domingo = DB::table('proposta_jornada')
                        ->select('valor_normal', 'valor_extra')
                        ->where('proposta_id', $proposta->id)
                        ->where('jornada_id', 3)
                        ->first();

        $valores_diarias['domingo'] =  $valor_diaria_domingo ;

        $array_forma = explode(';', $proposta->forma_cobranca);
        $array_dimensoes = [];
        $array_diametros = [];
        $programacoes = [];
        foreach ($guias as $guia)
        {   
            if($guia->local_ensaio == 'QUALITEC'){
                if(in_array('diaQ', $array_forma)){
                    $now = Carbon::now();

                    $now->hour=0;
                    $now->minute=0;
                    $now->second=0;  

                    $minutos_total = $guia->entrada->diffInMinutes($guia->saida);
                    $minutos_total -= 60;//1 hora de almoço

                    $contratadas = Carbon::createFromFormat('H:i:s', $guia->horas_contratadas);
                    $minutos_contratados = $now->diffInMinutes($contratadas);

                    $extra = $minutos_total - $minutos_contratados;
                     
                    if($guia->data->dayOfWeek == Carbon::SUNDAY)
                    {
                        $id_jornada = 3;
                        $guia['extra_dom'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_dom'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_dom +=  $guia['diaria_dom'];
                        $quant_extra_dom +=  $guia['extra_dom'];
                    }   
                    else  
                    {  
                        $id_jornada = 1;
                        $guia['extra_normal'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_normal'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_normal +=  $guia['diaria_normal'];
                        $quant_extra_normal +=  $guia['extra_normal'];
                    }

                    if($guia->data->dayOfWeek == Carbon::SUNDAY and !isset($guia['valor_domingo']))
                    {
                        $valores_diarias['domingo'] = $valor_diaria_domingo;
                        $guia['total_diaria'] = $guia['diaria_dom']*$valor_diaria_domingo->valor_normal;    
                        $guia['total_extra'] = $guia['extra_dom']*$valor_diaria_domingo->valor_extra;

                        $total_domingo +=  $guia['total_diaria'];
                        $total_extra_domingo += $guia['total_extra'];
                    } 

                    elseif(!isset($guia['valor_normal'])) 
                    {   
                        $valores_diarias['normal'] = $valor_diaria;
                        $guia['total_diaria'] = $guia['diaria_normal']*$valor_diaria->valor_normal;
                        $guia['total_extra'] = $guia['extra_normal']*$valor_diaria->valor_extra;

                        $total_normal +=  $guia['total_diaria'];
                        $total_extra_normal += $guia['total_extra'];
                    }                

                    $total += $guia['total_diaria'];
                    $total += $guia['total_extra'];
                }
                if(in_array('filmeQ', $array_forma)){
                    $programacoes[$guia->id] =  DB::table('programacoes')
                                ->select('quantidade_filmes', 'filme_dimensao')
                                ->where('guia_id', $guia->id)
                                ->where('executado', 'OK')
                                ->groupby('item')
                                ->get();
                    foreach ($programacoes[$guia->id] as $key => $value) {
                        $array_dimensoes[] = $value->filme_dimensao;
                    }
                }
                if(in_array('juntaQ', $array_forma)){
                    $diametros[$guia->id] = DB::table('guiaproducao')
                            ->select('junta_id','quantidade')
                            ->where('guiaproducao.guia_id',$guia->id)
                            ->where("guiaproducao.quantidade",'>',0)
                            ->get();
                     foreach ($diametros[$guia->id] as $key => $value) {
                        $array_diametros[] = $value->junta_id;
                    }
                }
            }
            if($guia->local_ensaio == 'CLIENTE'){
                if(in_array('diaC', $array_forma)){
                    $now = Carbon::now();

                    $now->hour=0;
                    $now->minute=0;
                    $now->second=0;  

                    $minutos_total = $guia->entrada->diffInMinutes($guia->saida);
                    $minutos_total -= 60;//1 hora de almoço

                    $contratadas = Carbon::createFromFormat('H:i:s', $guia->horas_contratadas);
                    $minutos_contratados = $now->diffInMinutes($contratadas);

                    $extra = $minutos_total - $minutos_contratados;
                     
                    if($guia->data->dayOfWeek == Carbon::SUNDAY)
                    {
                        $id_jornada = 3;
                        $guia['extra_dom'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_dom'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_dom +=  $guia['diaria_dom'];
                        $quant_extra_dom +=  $guia['extra_dom'];
                    }   
                    else  
                    {  
                        $id_jornada = 1;
                        $guia['extra_normal'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
                        $guia['diaria_normal'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
                        $quant_diaria_normal +=  $guia['diaria_normal'];
                        $quant_extra_normal +=  $guia['extra_normal'];
                    }

                    if($guia->data->dayOfWeek == Carbon::SUNDAY and !isset($guia['valor_domingo']))
                    {
                        $valores_diarias['domingo'] = $valor_diaria_domingo;
                        $guia['total_diaria'] = $guia['diaria_dom']*$valor_diaria_domingo->valor_normal;    
                        $guia['total_extra'] = $guia['extra_dom']*$valor_diaria_domingo->valor_extra;

                        $total_domingo +=  $guia['total_diaria'];
                        $total_extra_domingo += $guia['total_extra'];
                    } 

                    elseif(!isset($guia['valor_normal'])) 
                    {   
                        $valores_diarias['normal'] = $valor_diaria;
                        $guia['total_diaria'] = $guia['diaria_normal']*$valor_diaria->valor_normal;
                        $guia['total_extra'] = $guia['extra_normal']*$valor_diaria->valor_extra;

                        $total_normal +=  $guia['total_diaria'];
                        $total_extra_normal += $guia['total_extra'];
                    }                

                    $total += $guia['total_diaria'];
                    $total += $guia['total_extra'];
                }
                if(in_array('filmeC', $array_forma)){
                     $programacoes[$guia->id] =  DB::table('programacoes')
                                ->select('quantidade_filmes', 'filme_dimensao')
                                ->where('guia_id', $guia->id)
                                ->where('executado', 'OK')
                                ->groupby('item')
                                ->get();
                    foreach ($programacoes[$guia->id] as $key => $value) {
                        $array_dimensoes[] = $value->filme_dimensao;
                    }
                }
                if(in_array('juntaC', $array_forma)){
                    $diametros[$guia->id] = DB::table('guiaproducao')
                            ->select('junta_id','quantidade')
                            ->where('guiaproducao.guia_id',$guia->id)
                            ->where("guiaproducao.quantidade",'>',0)
                            ->get();
                     foreach ($diametros[$guia->id] as $key => $value) {
                        $array_diametros[] = $value->junta_id;
                    }
                }
                
            }
            
            // $now = Carbon::now();

            // $now->hour=0;
            // $now->minute=0;
            // $now->second=0;  

            // $minutos_total = $guia->entrada->diffInMinutes($guia->saida);
            // $minutos_total -= 60;//1 hora de almoço

            // $contratadas = Carbon::createFromFormat('H:i:s', $guia->horas_contratadas);
            // $minutos_contratados = $now->diffInMinutes($contratadas);

            // $extra = $minutos_total - $minutos_contratados;
             
            // if($guia->local_ensaio == 'CLIENTE'){
            //     if($guia->data->dayOfWeek == Carbon::SUNDAY)
            //     {
            //         $id_jornada = 3;
            //         $guia['extra_dom'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
            //         $guia['diaria_dom'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
            //         $quant_diaria_dom +=  $guia['diaria_dom'];
            //         $quant_extra_dom +=  $guia['extra_dom'];
            //     }   
            //     else  
            //     {  
            //         $id_jornada = 1;
            //         $guia['extra_normal'] = (ceil($extra/60) < 0) ? 0 : ceil($extra/60) ;
            //         $guia['diaria_normal'] = (floor($minutos_total/60/8) == 0) ? 1 : floor($minutos_total/60/8);
            //         $quant_diaria_normal +=  $guia['diaria_normal'];
            //         $quant_extra_normal +=  $guia['extra_normal'];
            //     }
            // }
            // $valor_diaria = DB::table('proposta_jornada')
            //                 ->select('valor_normal', 'valor_extra')
            //                 ->where('proposta_id', $proposta->id)
            //                 ->where('jornada_id', 1)
            //                 ->first();


            // $valor_diaria_domingo = DB::table('proposta_jornada')
            //                 ->select('valor_normal', 'valor_extra')
            //                 ->where('proposta_id', $proposta->id)
            //                 ->where('jornada_id', 3)
            //                 ->first();

            // if($guia->data->dayOfWeek == Carbon::SUNDAY and !isset($guia['valor_domingo']))
            // {
            //     $valores_diarias['domingo'] = $valor_diaria_domingo;
            //     $guia['total_diaria'] = $guia['diaria_dom']*$valor_diaria_domingo->valor_normal;    
            //     $guia['total_extra'] = $guia['extra_dom']*$valor_diaria_domingo->valor_extra;

            //     $total_domingo +=  $guia['total_diaria'];
            //     $total_extra_domingo += $guia['total_extra'];
            // } 

            // elseif(!isset($guia['valor_normal'])) 
            // {   
            //     $valores_diarias['normal'] = $valor_diaria;
            //     $guia['total_diaria'] = $guia['diaria_normal']*$valor_diaria->valor_normal;
            //     $guia['total_extra'] = $guia['extra_normal']*$valor_diaria->valor_extra;

            //     $total_normal +=  $guia['total_diaria'];
            //     $total_extra_normal += $guia['total_extra'];
            // }                

            // $total += $guia['total_diaria'];
            // $total += $guia['total_extra'];
        }

        // foreach ($programacoes as $programacao) 
        // {
        //     if(isset($quantidades[$programacao->filme_dimensao]))
        //         $quantidades[$programacao->filme_dimensao] += $programacao->quantidade_filmes;
        //     else
        //         $quantidades[$programacao->filme_dimensao] = $programacao->quantidade_filmes;

        // }

        // foreach($quantidades as $dimensao => $quantidade) 
        // {
        //     if(isset($resultado[$dimensao]))
        //         $resultado[$dimensao] += floatval ($quantidade) * floatval ($valores[$dimensao]);

        //     else
        //         $resultado[$dimensao] = floatval ($quantidade) * floatval ($valores[$dimensao]);


        //     $total += $resultado[$dimensao];
        // }

        $data_inicio = $data_inicio->format('d/m/Y');
        
        $data_fim  = $data_fim->format('d/m/Y');

        $data['cliente'] = $cliente;
        $data['proposta'] = $proposta;
        $data['projeto'] = $projeto;
        $data['guias'] = $guias;
        // $data['quantidades'] = $quantidades;
        $data['valores'] = $valores;
        $data['valores_diarias'] = $valores_diarias;
        $data['programacoes'] = $programacoes;
        // $data['resultado'] = $resultado;
        $data['total'] = $total;
        $data['total_domingo'] = $total_domingo;
        $data['total_extra_domingo'] = $total_extra_domingo;
        $data['total_normal'] = $total_normal;
        $data['total_extra_normal'] = $total_extra_normal;
        $data['quant_diaria_dom'] = $quant_diaria_dom;
        $data['quant_extra_dom'] = $quant_extra_dom;
        $data['quant_diaria_normal'] = $quant_diaria_normal;
        $data['quant_extra_normal'] = $quant_extra_normal;
        $data['data_inicio'] = $data_inicio;
        $data['data_fim'] = $data_fim;
        $data['valor_km'] = $valor_km->valor;
        $data['diametros'] = $diametros;
        $data['valores_diametros'] = $valores_diametros;
        $data['valor_diaria_domingo'] = $valor_diaria_domingo;
        $data['clientes'] = ['' => 'Selecione o cliente'] + $clientes;
        $data['filmes'] = $filmes;
        $data['id_bm'] = $id;
        $data['inicio'] = $data_inicio;
        $data['fim'] = $data_fim;
        $array_dimensoes = array_unique($array_dimensoes);
        array_multisort($array_dimensoes,SORT_ASC);
        $data['array_dimensoes'] = $array_dimensoes;
        $array_diametros = array_unique($array_diametros);
        array_multisort($array_diametros,SORT_ASC);
        $data['array_diametros'] = $array_diametros ;
        $data['juntas'] = $juntas;

        return view('ensaio.boletim.visualizar')->with($data);

    }

    public function dadosPropostas()
    {   
        $id = Input::get('id');

        $proposta = $this->proposta
                        ->where('cliente_id', $id)
                        ->lists('cod_proposta','id');

        return Response::json($proposta);
    }

    public function carregarBM()
    {   
        $cliente_id = Input::get('cliente_id');
        $proposta_id = Input::get('proposta_id');
        $data_inicio = Carbon::createFromFormat('Y-m-d', Input::get('data_inicio'));
        $data_fim = Carbon::createFromFormat('Y-m-d', Input::get('data_fim'));

        $cliente = $this->cliente->findOrFail($cliente_id); 

        $projeto = $this->projeto->where('proposta_id', $proposta_id)->first();

        $proposta = $proposta = $this->proposta->with(['filme', 'jornada'])->findOrFail($proposta_id);

        $quantidades = [];
        $total = 0;
        $quant_diaria_dom = 0;
        $quant_extra_dom  = 0;  
        $quant_diaria_normal = 0;  
        $quant_extra_normal = 0;  
        $valores_diarias = [];
        $total_domingo = 0;  
        $total_extra_domingo = 0;  
        $total_normal = 0;  
        $total_extra_normal = 0;  
        $diametros = [];

        foreach ($proposta->filme as $key => $filme) 
        {
            $valores[$filme->legenda] =  DB::table('proposta_filme')
                                            ->where('proposta_id', $proposta->id)
                                            ->where('filme_id', $filme->id)
                                            ->pluck('valor');

        }
        $guias = $this->guia->where('cliente_id', $cliente->id)
                             ->where('proposta_id', $proposta->id)
                             ->where('status',2)
                             ->whereBetween('data', [$data_inicio, $data_fim])
                             ->orderBy('data')
                             ->get();

        $valor_km = DB::table('proposta_servico')->select('valor')->where('servico_id',1)->where('proposta_id',$proposta->id)->first();

        $valores_diametros = DB::table('proposta_junta')
                            ->select('junta_id','valor')
                            ->where('proposta_id',$proposta->id)
                            ->groupby('junta_id')
                            ->lists('valor','junta_id');

        foreach ($guias as $guia)
        {
            $programacoes[] =  DB::table('programacoes')
                                ->select('quantidade_filmes', 'filme_dimensao')
                                ->where('guia_id', $guia->id)
                                ->where('unidade', $guia->unidade)
                                ->first();

            $diametros[$guia->id] = DB::table('guiaproducao')
                            ->select('junta_id','quantidade')
                            ->where('guiaproducao.guia_id',$guia->id)
                            ->where("guiaproducao.quantidade",'>',0)
                            ->get();

            $now = Carbon::now();

            $now->hour=0;
            $now->minute=0;
            $now->second=0;  

            $minutos_total = $guia->entrada->diffInMinutes($guia->saida);
            $minutos_total -= 60;//1 hora de almoço

            $contratadas = Carbon::createFromFormat('H:i:s', $guia->horas_contratadas);
            $minutos_contratados = $now->diffInMinutes($contratadas);

            $extra = abs($minutos_contratados-$minutos_total);
            
            if($guia->data->dayOfWeek == Carbon::SUNDAY)
            {
                $id_jornada = 3;
                $guia['extra_dom'] = ceil($extra/60);
                $guia['diaria_dom'] = ceil($minutos_total/60);
                $quant_diaria_dom +=  $guia['diaria_dom'];
                $quant_extra_dom +=  $guia['extra_dom'];
            }   
            else  
            {  
                $id_jornada = 1;
                $guia['extra_normal'] = ceil($extra/60);
                $guia['diaria_normal'] = ceil($minutos_total/60);
                $quant_diaria_normal +=  $guia['diaria_normal'];
                $quant_extra_normal +=  $guia['extra_normal'];
            }

            $valor_diaria = DB::table('proposta_jornada')
                            ->select('valor_normal', 'valor_extra')
                            ->where('proposta_id', $proposta->id)
                            ->where('jornada_id', 1)
                            ->first();


            $valor_diaria_domingo = DB::table('proposta_jornada')
                            ->select('valor_normal', 'valor_extra')
                            ->where('proposta_id', $proposta->id)
                            ->where('jornada_id', 3)
                            ->first();

            if($guia->data->dayOfWeek == Carbon::SUNDAY and !isset($guia['valor_domingo']))
            {
                $valores_diarias['domingo'] = $valor_diaria_domingo;
                $guia['total_diaria'] = $guia['diaria_dom']*$valor_diaria_domingo->valor_normal;    
                $guia['total_extra'] = $guia['extra_dom']*$valor_diaria_domingo->valor_extra;

                $total_domingo +=  $guia['total_diaria'];
                $total_extra_domingo += $guia['total_extra'];
            } 

            elseif(!isset($guia['valor_normal'])) 
            {   
                $valores_diarias['normal'] = $valor_diaria;
                $guia['total_diaria'] = $guia['diaria_normal']*$valor_diaria->valor_normal;
                $guia['total_extra'] = $guia['extra_normal']*$valor_diaria->valor_extra;

                $total_normal +=  $guia['total_diaria'];
                $total_extra_normal += $guia['total_extra'];
            }                

            $total += $guia['total_diaria'];
            $total += $guia['total_extra'];
        }

        foreach ($programacoes as $programacao) 
        {
            if(isset($quantidades[$programacao->filme_dimensao]))
                $quantidades[$programacao->filme_dimensao] += $programacao->quantidade_filmes;
            else
                $quantidades[$programacao->filme_dimensao] = $programacao->quantidade_filmes;

        }

        foreach($quantidades as $dimensao => $quantidade) 
        {
            if(isset($resultado[$dimensao]))
                $resultado[$dimensao] += floatval ($quantidade) * floatval ($valores[$dimensao]);

            else
                $resultado[$dimensao] = floatval ($quantidade) * floatval ($valores[$dimensao]);


            $total += $resultado[$dimensao];
        }

        $data_inicio = $data_inicio->format('d/m/Y');
        
        $data_fim  = $data_fim->format('d/m/Y');



        return Response::json(array('success' => true, 
                                    'cliente' => $cliente,
                                    'proposta' => $proposta,
                                    'projeto' => $projeto,
                                    'guias' => $guias,
                                    'quantidades' => $quantidades,
                                    'valores' => $valores,
                                    'valores_diarias' => $valores_diarias,
                                    'programacoes' => $programacoes,
                                    'resultado' => $resultado,
                                    'total' => $total,
                                    'total_domingo' => $total_domingo,
                                    'total_extra_domingo' => $total_extra_domingo,
                                    'total_normal' => $total_normal,
                                    'total_extra_normal' => $total_extra_normal,
                                    'quant_diaria_dom' => $quant_diaria_dom,
                                    'quant_extra_dom' => $quant_extra_dom,
                                    'quant_diaria_normal' => $quant_diaria_normal,
                                    'quant_extra_normal' => $quant_extra_normal,
                                    'data_inicio' => $data_inicio,
                                    'data_fim' => $data_fim,
                                    'valor_km' => $valor_km->valor,
                                    'diametros' => $diametros,
                                    'valores_diametros' => $valores_diametros,
                                    ));
    }

}
