<?php namespace App\Http\Controllers\ensaio;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Permissaousuario;
use Carbon\Carbon;

class PropostaController extends Controller {
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function index(){
		$id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        
        $id_modulo = DB::table('modulos')
        ->where('rota','like','%ensaio%')
        ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();
        $data['propostas'] = \App\Proposta::all();
        $data['clientes'] = DB::table('clientes')
        ->select('id','nome')
        ->lists('nome','id');
		return view('ensaio.proposta.proposta')->with($data);
	}

    public function cadastrar(){
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        
        $id_modulo = DB::table('modulos')
        ->where('rota','like','%ensaio%')
        ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['clientes'] = \App\Cliente::all()->toArray();
        $data['jornadas'] = \App\Jornada::all()->toArray();
        $data['filmes'] = \App\Filme::all()->toArray();
        $data['juntas'] = \App\Junta::all()->toArray();
        $data['servicos'] = \App\Servico::all()->toArray();
        $data['usuario'] = Auth::user();
        return view('ensaio.proposta.cadastroproposta')->with($data);
    }

    public function editarProposta($id){
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        
        $id_modulo = DB::table('modulos')
        ->where('rota','like','%ensaio%')
        ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['clientes'] = \App\Cliente::all()->toArray();
        $data['jornadas'] = \App\Jornada::all()->toArray();
        $data['filmes'] = \App\Filme::all()->toArray();
        $data['juntas'] = \App\Junta::all()->toArray();
        $data['servicos'] = \App\Servico::all()->toArray();
        $data['proposta'] = \App\Proposta::find($id)->toArray();

        $data['formas_cobranca'] = explode(';', $data['proposta']['forma_cobranca']);
        
        $projetos = \App\Projeto::where('proposta_id', '=', $id)->get();
        $pro = [];
        foreach ($projetos as $key => $value) {
            $pro[] = $value['attributes'];
        }
        $data['projetos'] = $pro;


        $prop_jornadas = \App\Propostajornada::where('proposta_id', '=', $id)->get();
        $pro = [];
        foreach ($prop_jornadas as $key => $value) {
            $pro[$value['attributes']['jornada_id']] = $value['attributes'];
        }
        $data['prop_jornadas'] = $pro;


        $prop_filmes = \App\Propostafilme::where('proposta_id', '=', $id)->get();
        $pro = [];
        foreach ($prop_filmes as $key => $value) {
            $pro[$value['attributes']['filme_id']] = $value['attributes'];
        }
        $data['prop_filmes'] = $pro;

        $prop_juntas = \App\Propostajunta::where('proposta_id', '=', $id)->get();
        $pro = [];
        foreach ($prop_juntas as $key => $value) {
            $pro[$value['attributes']['junta_id']] = $value['attributes'];
        }
        $data['prop_juntas'] = $pro;

        $prop_servicos = \App\Propostaservico::where('proposta_id', '=', $id)->get();
        $pro = [];
        foreach ($prop_servicos as $key => $value) {
            $pro[$value['attributes']['servico_id']] = $value['attributes'];
        }
        $data['prop_servicos'] = $pro;

        $data['usuario'] = Auth::user();
        return view('ensaio.proposta.editarproposta')->with($data);
    }
    
    public function getPropostas(){
        $input = Input::all();
        if($input['id_proposta'] != '')
            $propostas = \App\Proposta::find($input['id_proposta'])->toArray();
        else $propostas = \App\Proposta::all()->toArray();

        $clientes = DB::table('clientes')
        ->select('id','nome')
        ->lists('nome','id');
        return Response::json(array('proposta' => $propostas, 'clientes' => $clientes));
    }


    public function salvarProposta(){
        $input = Input::all();
        if(isset($input['id_proposta'])){
            $proposta = \App\Proposta::find($input['id_proposta']);
            $proposta->updated_by = Auth::user()->id;

            $projeto = \App\Projeto::find($input['id_proposta']);
            $projeto->updated_by = Auth::user()->id;
        }else{
            $proposta = new \App\Proposta;
            $proposta->created_by = Auth::user()->id;
            $proposta->unidade = Auth::user()->unidade;

            $projeto = new \App\Projeto;
            $projeto->created_by = Auth::user()->id;
            $projeto->unidade = Auth::user()->unidade;
        }  

        $proposta->cliente_id = $input['cliente_id'];
        $proposta->data = $input['data'];
        $proposta->validade = $input['validade'];
        $proposta->cod_proposta = $input['cod_proposta'];
        $proposta->cod_proposta_anterior = $input['cod_proposta_anterior'];
        $proposta->filmedereparo = $input['filmedereparo'];
        $proposta->iss = $input['iss'];
        if(!empty($input['arrayforma_pagamento'])){
            $proposta->forma_cobranca = implode(';',$input['arrayforma_pagamento']);
        }else{
            $proposta->forma_cobranca = 0;            
        }
        // $proposta->valorchapa = $input['valorchapa'];
        $proposta->save();

        $projeto->proposta_id = $proposta->id;
        $projeto->servico= $input['servico'];
        $projeto->estado= $input['estado'];
        $projeto->municipio= $input['municipio'];
        $projeto->solicitacao_contato= $input['solicitacao_contato'];
        $projeto->solicitacao_email= $input['solicitacao_email'];
        $projeto->solicitacao_tel= $input['solicitacao_tel'];
        $projeto->aprovacao_contato= $input['aprovacao_contato'];
        $projeto->aprovacao_email= $input['aprovacao_email'];
        $projeto->aprovacao_tel= $input['aprovacao_tel'];
        $projeto->projeto_contato= $input['projeto_contato'];
        $projeto->projeto_email= $input['projeto_email'];
        $projeto->projeto_tel= $input['projeto_tel'];
        $projeto->save();

        
        $jornadaN = $input['arrayjornadanormal'];
        $jornadaE = $input['arrayjornadaextra'];


        $idsJornadas = [];
        $idsFilmes = [];
        $idsJuntas = [];
        $idsServicos = [];
        
        if(isset($input['id_proposta'])){
            $idsJornadas =  DB::table('proposta_jornada')
                ->select('id','jornada_id')
                ->where('proposta_id','=',$input['id_proposta'])
                ->lists('jornada_id','id');
            $idsFilmes =  DB::table('proposta_filme')
                ->select('id','filme_id')
                ->where('proposta_id','=',$input['id_proposta'])
                ->lists('filme_id','id');
            $idsJuntas =  DB::table('proposta_junta')
                ->select('id','junta_id')
                ->where('proposta_id','=',$input['id_proposta'])
                ->lists('junta_id','id');
            $idsServicos =  DB::table('proposta_servico')
                ->select('id','servico_id')
                ->where('proposta_id','=',$input['id_proposta'])
                ->lists('servico_id','id');
        }
        foreach ($jornadaN as $key => $value) {
            if($key != 0){
                if(in_array($key, $idsJornadas)){
                    foreach($idsJornadas as $chave => $valor){
                        if($valor == $key)
                            $chaveJornada = $chave;
                    }
                    $prop_jornada = \App\Propostajornada::find($chaveJornada);
                }else{
                    $prop_jornada = new \App\Propostajornada;    
                }
                $prop_jornada->proposta_id  = $proposta->id;
                $prop_jornada->jornada_id   = $key;
                $prop_jornada->valor_normal = $value;
                $prop_jornada->valor_extra  = $jornadaE[$key];
                $prop_jornada->unidade = Auth::user()->unidade;
                $prop_jornada->save();    
            }
        }

        $filmes = $input['arrayfilmes'];

        foreach ($filmes as $key => $value) {
            if($key != 0){
                if(in_array($key, $idsFilmes)){
                    foreach($idsFilmes as $chave => $valor){
                        if($valor == $key)
                            $chaveFilme = $chave;
                    }
                    $prop_Filme = \App\Propostafilme::find($chaveFilme);
                }else{
                    $prop_Filme = new \App\Propostafilme;   
                    $prop_Filme->proposta_id  = $proposta->id;
                    $prop_Filme->filme_id   = $key;
                }
                $prop_Filme->valor = $value;
                $prop_Filme->unidade = Auth::user()->unidade;
                $prop_Filme->save();    
            }
        }

        $juntas = $input['arrayjuntas'];
        foreach ($juntas as $key => $value) {
            if($key != 0){
                if(in_array($key, $idsJuntas)){
                    foreach($idsJuntas as $chave => $valor){
                        if($valor == $key)
                            $chaveJunta = $chave;
                    }
                    $prop_Junta = \App\Propostajunta::find($chaveJunta);
                }else{
                    $prop_Junta = new \App\Propostajunta;  
                }
               
                $prop_Junta->proposta_id  = $proposta->id;
                $prop_Junta->junta_id   = $key;
                $prop_Junta->valor = $value;
                $prop_Junta->unidade = Auth::user()->unidade;
                $prop_Junta->save();    
            }
        }
        if(isset($input['arrayservico'])){
            $servicos = $input['arrayservico'];          
        foreach ($servicos as $key => $value) {
            if($key != 0){
                if(in_array($key, $idsServicos)){
                    foreach($idsServicos as $chave => $valor){
                        if($valor == $key)
                            $chaveServico = $chave;
                    }
                    $prop_Servico = \App\Propostaservico::find($chaveServico);
                }else{
                    $prop_Servico = new \App\Propostaservico;    
                }
                $prop_Servico->proposta_id  = $proposta->id;
                $prop_Servico->servico_id   = $key;
                $prop_Servico->valor = $value;
                $prop_Servico->unidade = Auth::user()->unidade;
                $prop_Servico->save();    
            }
        }
        }

        return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
    }

    public function dadosCliente(){
        $id = Input::get('id');
        if(isset($id)){
            $cliente = \App\Cliente::find($id)->toArray();
            return Response::json(array('success' => true,'cliente'=>$cliente));    
        }
        
    }
}
