<?php namespace App\Http\Controllers\ensaio;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Permissaousuario;
use App\Procedimentosnormas;
use Carbon\Carbon;

class ProcedimentoController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$id_user = Auth::user()->id;
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        $id_modulo = DB::table('modulos')
        ->where('rota','like','%ensaio%')
        ->pluck('id');
        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();

        $data['procedimentos'] = \App\Procedimento::lists('procedimento','id');
        $data['grupos'] = \App\Gruposmaterialbase::all();
        $data['normas'] = \App\Normas::all();
        $data['parametros'] = \App\Parametrosprocedimentos::all();
        
        return view('ensaio.procedimentos.home')->with($data);
	}

	public function salvarProcedimento(){
		$input = Input::all();
		if(isset($input['id'])){
			$procedimento = \App\Procedimento::find($input['id']);
			$procedimento->updated_by = Auth::user()->id;
		}else{
			$procedimento = new \App\Procedimento;
			$procedimento->created_by = Auth::user()->id;	
		}
		$procedimento->procedimento = $input['procedimento'];
		$procedimento->save();
		return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
	}

	public function salvarGrupo(){
		$input = Input::all();
		if(isset($input['id'])){
			$grupo = \App\Gruposmaterialbase::find($input['id']);
			$grupo->updated_by = Auth::user()->id;
		}else{
			$grupo = new \App\Gruposmaterialbase;
			$grupo->created_by = Auth::user()->id;
		}
		$grupo->grupo = $input['grupo'];
		$grupo->save();
		return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
	}

	public function salvarNorma(){
		$input = Input::all();
		if(isset($input['id'])){
			$normas = \App\Normas::find($input['id']);
			$normas->updated_by = Auth::user()->id;
		}else{
			$normas = new \App\Normas;
			$normas->created_by = Auth::user()->id;
		}
		$normas->procedimento_id = $input['procedimento_id'];
		$normas->norma = $input['norma'];
		$normas->criterio_aceite = $input['criterio_aceite'];
		$normas->objeto = $input['objeto'];
		$normas->aplicacao = $input['aplicacao'];
		$normas->grupo_material_base = $input['grupo_material_base'];
		$normas->save();

		return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
	}

	public function salvarparametroprocedimento(){
		$input = Input::all();
		if(isset($input['id'])){
			$parametro = \App\Parametrosprocedimentos::find($input['id']);
			$parametro->updated_by = Auth::user()->id;
		}else{
			$parametro = new \App\Parametrosprocedimentos;
			$parametro->created_by = Auth::user()->id;
		}
		$parametro->procedimento_id = $input['procedimento_id'];
		$parametro->diametro_ini = $input['diametro_ini'];
		$parametro->diametro_fim = $input['diametro_fim'];
		$parametro->comprimento_ini = $input['comprimento_ini'];
		$parametro->comprimento_fim = $input['comprimento_fim'];
		$parametro->espessura_ini = $input['espessura_ini'];
		$parametro->espessura_fim = $input['espessura_fim'];
		$parametro->tipo_fonte = $input['tipo_fonte'];
		$parametro->foco_fonte = $input['foco_fonte'];
		$parametro->tecnica = $input['tecnica'];
		$parametro->dff = $input['dff'];
		$parametro->tipo_iqi = $input['tipo_iqi'];
		$parametro->fio_iqi = $input['fio_iqi'];
		$parametro->quantidade_filmes = $input['quantidade_filmes'];
		$parametro->classe_filme = $input['classe_filme'];
		$parametro->dimensao_filme = $input['dimensao_filme'];
		$parametro->save();		
		return Response::json(array('success' => true, 'mensagem' =>'Salvo com sucesso'));
	}

	public function dadosnormas(){
		$input = Input::all();
		$procedimentos = \App\Procedimento::all();
		$grupos = \App\Gruposmaterialbase::all();
		if(isset($input['id'])){
			$norma = \App\Normas::find($input['id']);
			return Response::json(array('success' => true, 'procedimentos' => $procedimentos, 'grupos' => $grupos, 'norma' => $norma));
		}
		return Response::json(array('success' => true, 'procedimentos' => $procedimentos, 'grupos' => $grupos));
	}

	public function dadosparametro(){
		$input = Input::all();
		$procedimentos = \App\Procedimento::all();
		$filmes = \App\Filme::all()->groupby('legenda')->toArray();
		if(isset($input['id'])){
			$parametro = \App\Parametrosprocedimentos::find($input['id']);
			return Response::json(array('success' => true, 'procedimentos' => $procedimentos, 'parametro' => $parametro, 'filmes' => $filmes));
		}
		return Response::json(array('success' => true, 'procedimentos' => $procedimentos, 'filmes' => $filmes));
	}
}
