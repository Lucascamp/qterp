<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Filesystem\Filesystem;
use App\Permissaousuario;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check())
		{
			$id = Auth::user()->id;
			$modulos = Permissaousuario::Modulo($id);
			$data['modulos'] = $modulos;
			$data['usuario'] = Auth::user();

			$permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id)->lists('permissao_opcoes_id');
	        $permissoes_opcoes = (array) $permissoes_opcoes;
	        $data['permissoes_opcoes'] = $permissoes_opcoes;

	        $data['usuario'] = Auth::user();
	        $data['id_user'] = $id;

	        $data['unidades'] = \DB::table('unidade')->lists('unidade','id'); 
	        $data['insp_aud'] = \DB::connection('MG')->table('inspecoes_auditorias')->whereNull('deleted_at')->lists('descricao','id');
	        $data['funcionarios'] = \App\Funcionario::whereNull('deleted_at')->lists('nome','id');

	        $data['formularios_marcacao'] =  \DB::connection('MG')->table('insp_auds_unidade')->whereNotnull('id_marcacao')->lists('id','id_marcacao');

	        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');

	        if(in_array(5, $permissoes_opcoes)){
	        	$data['marcacoes'] = \DB::connection('MG')->table('insp_auds_marcacao')->whereNull('deleted_at')->whereIn('status',[0,1])->orderby('data_ini')->get();
	        }else{
	        	$data['marcacoes'] = \DB::connection('MG')->table('insp_auds_marcacao')->whereNull('deleted_at')->where('status',[0,1])->where('unidade_id',$unidade_id_user)->orderby('data_ini')->get();
	        }
			
			return view('home')->with($data);
		}

		return view('login');
	}

	public function arquivoferias() 
    {
        $input = array_except(Input::all(), array('_token'));

        $file = Input::file('arquivo_ferias');

        if(isset($file))
        {
        	\File::delete('arquivos/arquivo_ferias.xlsx');
            $file->move('arquivos','arquivo_ferias.xlsx');
        }

        return redirect()->route('home');
    }

}
