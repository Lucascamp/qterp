<?php namespace App\Http\Controllers\sgi;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Permissaousuario;
use App\Equipamento;
use App\Centrocusto;
use App\Localizacaoequipamento;
use App\Funcionario;
use App\User;
use App\Calibracao;
use Carbon\Carbon;
use App\Http\Requests\CreateEquipamento;
use App\Http\Requests\CreateLocalizacao;
// use Carbon\Carbon;

class sgiController extends Controller {
	public function __construct(){
        $this->middleware('auth');                
    }
	
	public function index(){
		$id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%sgi%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();

    

        return view('sgi.home')->with($data);
	}

	public function inspecoesauditorias(){
		$id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%sgi%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();
        $data['id_user'] = $id_user;

        $data['inspe_aud'] = \App\Inspecoes_auditorias::whereNull('deleted_at')->get();        

        if(in_array(5,$permissoes_opcoes)){
        	return view('sgi.inspecoesauditorias')->with($data);
        }else{
        	$data['unidades'] = \DB::table('unidade')->lists('unidade','id'); 
	        $data['insp_aud'] = \App\Inspecoes_auditorias::whereNull('deleted_at')->lists('descricao','id');
	        $data['funcionarios'] = \App\Funcionario::whereNull('deleted_at')->lists('nome','id');

	        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');

	        if(in_array(5, $permissoes_opcoes)){
	        	$data['marcacoes'] = \App\Insp_aud_marcacao::whereNull('deleted_at')->orderby('data_ini')->get();
	        }else{
	        	$data['marcacoes'] = \App\Insp_aud_marcacao::whereNull('deleted_at')->where('unidade_id',$unidade_id_user)->orderby('data_ini')->get();
	        }
	        $data['unidade_id_user'] = $unidade_id_user;
	        $this->valida_status_calendario($data['marcacoes']);
        	return view('sgi.calendario')->with($data);
        }
	}
	
	public function salvar_formulario(){
		$input = Input::all();

		if($input['id_form']){
			$insp_aud = \App\Inspecoes_auditorias::find($input['id_form']);
			$insp_aud->updated_at = date('Y-m-d H:i:s');
			$insp_aud->updated_by = Auth::user()->id;
		}else{
			$insp_aud = new \App\Inspecoes_auditorias;
			$insp_aud->tipo  = $input['tipo'];
			$insp_aud->created_by = Auth::user()->id;
		}
		$insp_aud->descricao = $input['descricao'];
		$insp_aud->save();

		if(count($input['itens']) > 0){
			\DB::table('itens_insp_aud')->where('insp_aud_id',$insp_aud->id)->delete();
			foreach ($input['itens'] as $key => $it) {
				$item = new \App\Itens_ins_aud;
				$item->insp_aud_id = $insp_aud->id;
				$item->numero = $it['numero_item'];
				$item->inspecao_verificacao = $it['insp_veri'];
				$item->caracterizacao = $it['carac'];
				$item->documentos_evidenciais = $it['doc_evid'];
				$item->created_by = Auth::user()->id;
				$item->save();
			}
		}
		
		$data['success'] = true;
		return Response::json($data);
	}

	public function carregar_formulario_edt(){
		$input = Input::all();
		$data['success'] = true;
		$data['formulario'] = \DB::table('inspecoes_auditorias')->where('id',$input['id_form'])->first();
		$data['itens'] = \DB::table('itens_insp_aud')->where('insp_aud_id',$data['formulario']->id)->get();
		return Response::json($data);
	}

	public function visualizar_formulario($id, $marcacao = null){
		$id_user = Auth::user()->id;
		
        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%sgi%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();
        $data['id_user'] = $id_user;
		
		$data['unidades'] = [ 0 => 'Unidade'] + DB::table('unidade')->lists('unidade','id');

        $data['itens']         = \DB::table('itens_insp_aud')->where('insp_aud_id',$id)->get();
        $data['insp_aud']      = \DB::table('inspecoes_auditorias')->where('id',$id)->first();
        $data['id_formulario'] = $id;
        $data['funcionarios'] = \App\Funcionario::whereNull('deleted_at')->lists('nome','id');
        if(isset($marcacao)){
        	$data['marcacao'] = \DB::table('insp_auds_marcacao')->where('id',$marcacao)->first();
        	$data['id_marcacao'] = $marcacao;
        }
		


		return view('sgi.formulario')->with($data);
	}

	public function salvarFormularioUnidade(){
		$input = Input::all();

		if($input['id_marcacao']){
			$id_marcacao = $input['id_marcacao'];
			$marcacao = \App\Insp_aud_marcacao::find($id_marcacao);
			$marcacao->status = 2;
			$marcacao->updated_by = Auth::user()->id;
			$marcacao->save();

		}else{
			$id_marcacao = null;
		}
		
		$insp_aud_unidade = new \App\Insp_aud_unidade;
		$insp_aud_unidade->insp_aud_id      = $input['id_form'];
		$insp_aud_unidade->projeto          = $input['projeto'];
		$insp_aud_unidade->local            = $input['local'];
		$insp_aud_unidade->responsavel      = $input['responsavel'];
		$insp_aud_unidade->equipe           = $input['equipe'];
		$insp_aud_unidade->verificado_por   = $input['verificado_por'];
		$insp_aud_unidade->doc_referencia   = $input['documentos_referencia'];
		$insp_aud_unidade->unidade          = $input['unidade'];
		$insp_aud_unidade->data             = $input['data'];
		$insp_aud_unidade->created_by       = Auth::user()->id;
		$insp_aud_unidade->id_marcacao      = $id_marcacao;

		$insp_aud_unidade->save();

		$insp_aud = \DB::table('itens_insp_aud')->where('insp_aud_id',$input['id_form'])->get();

		$na = isset($input['respostas']['na']) ? $input['respostas']['na'] : null;
		$nc = isset($input['respostas']['nc']) ? $input['respostas']['nc'] : null;
		$c  = isset($input['respostas']['c'])  ? $input['respostas']['c']  : null;


		$aux_nc = [];
		if($nc){
			foreach ($nc as $key => $value) {
				$index = array_keys($value);
				$aux_nc[$index[0]] = $value[$index[0]];
			}	
		}
		
		foreach ($insp_aud as $key => $item) {

			$itens_insp_aud_unidade = new \App\Itens_insp_aud_unidade;
			$itens_insp_aud_unidade->insp_aud_id = $insp_aud_unidade->id;
			if(isset($na)){
				if(in_array($item->numero, $na)){
					$itens_insp_aud_unidade->item_numero = $item->numero;
					$itens_insp_aud_unidade->na          = 'X';
				}	
			}
			
			if(isset($aux_nc[$item->numero])){
				$itens_insp_aud_unidade->item_numero = $item->numero;
				$itens_insp_aud_unidade->nc          = $aux_nc[$item->numero];
			}
			if(isset($c)){
				if(in_array($item->numero, $c)){
					$itens_insp_aud_unidade->item_numero = $item->numero;
					$itens_insp_aud_unidade->c           = '2';
				}
			}
		
			
			$itens_insp_aud_unidade->nota = $input['notas'][$item->numero];
			$itens_insp_aud_unidade->created_by = Auth::user()->id;
			$itens_insp_aud_unidade->save();
		}



		$data['success'] = true;
		return Response::json($data);
	}

	public function formularios(){
		$id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%sgi%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();

        $data['unidades'] = [0 => 'Todos'] + \DB::table('unidade')->lists('unidade','id');



        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');
        if(!$unidade_id_user){
            $unidade_id_user = 0;
        }
        if(in_array(5,$permissoes_opcoes)){
        	$data['insp_aud'] = DB::table('insp_auds_unidade')->get();	
        }else{
        	$data['insp_aud'] = DB::table('insp_auds_unidade')->where('unidade',$unidade_id_user)->get();	
        }
        $data['unidade_id_user'] = $unidade_id_user;


        $data['insp_aud_nomes'] = DB::table('inspecoes_auditorias')->lists('descricao','id');


        return view('sgi.formularios')->with($data);
	}

	public function visualizar_formulario_preenchido($id){
		$id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%sgi%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();
        $data['id_user'] = $id_user;
		

        $data['formulario'] = DB::table('insp_auds_unidade')->where('id',$id)->first();

        $data['itens']            = \DB::table('itens_insp_aud')->where('insp_aud_id',$data['formulario']->insp_aud_id)->get();


        $data['insp_aud']         = \DB::table('inspecoes_auditorias')->where('id',$data['formulario']->insp_aud_id)->first();
        $data['unidades']         = \DB::table('unidade')->lists('unidade','id');

        $data['id_formulario']    = $data['formulario']->insp_aud_id;

        $auxitens = \DB::table('itens_insp_auds_unidade')->where('insp_aud_id',$data['formulario']->id)->lists('item_numero');
        
        $itens = [];
        foreach ($auxitens as $key => $value) {
			$itens[$value] = \DB::table('itens_insp_auds_unidade')->where('insp_aud_id',$data['formulario']->id)->where('item_numero',$value)->first(); 
        }
        
        $data['itens_formulario'] = $itens;       

		return view('sgi.formulario')->with($data);
	}

	public function carregar_tabela_preenchido(){
		$input = Input::all();
		
		$insp_aud_uni = new \App\Insp_aud_unidade;

		if($input['unidade'] != 0 && $input['unidade'] != null){
			$insp_aud_uni = $insp_aud_uni->where('unidade',$input['unidade']);
		}
		if($input['data_ini'] != 0 && $input['data_ini'] != null){
			$insp_aud_uni = $insp_aud_uni->where('data','>=',$input['data_ini']);
		}
		if($input['data_fim'] != 0 && $input['data_fim'] != null){
			$insp_aud_uni = $insp_aud_uni->where('data','<=',$input['data_fim']);
		}
		
		$data['insp_aud'] = $insp_aud_uni->get();
		


        $data['insp_aud_nomes'] = DB::table('inspecoes_auditorias')->lists('descricao','id');
        $data['unidades']         = \DB::table('unidade')->lists('unidade','id');
		$data['success'] = true;
		return Response::json($data);
	}

	public function excluir_formulario(){
		$input = Input::all();
		$insp_aud = \App\Inspecoes_auditorias::find($input['id']);
		$insp_aud->deleted_by = Auth::user()->id;
		$insp_aud->deleted_at = date('Y-m-d H:i:s');
		$insp_aud->save();
		$data['success'] = true;
		return Response::json($data);
	}

	public function calendario(){
		$id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%sgi%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();
        $data['id_user'] = $id_user;
        $data['unidades'] = \DB::table('unidade')->lists('unidade','id'); 
        $data['insp_aud'] = \App\Inspecoes_auditorias::whereNull('deleted_at')->lists('descricao','id');
        $data['funcionarios'] = \App\Funcionario::whereNull('deleted_at')->lists('nome','id');

        $data['formularios_marcacao'] =  DB::table('insp_auds_unidade')->whereNotnull('id_marcacao')->lists('id','id_marcacao');


        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');

        if(in_array(5, $permissoes_opcoes)){
        	$data['marcacoes'] = \App\Insp_aud_marcacao::whereNull('deleted_at')->orderby('data_ini')->get();
        }else{
        	$data['marcacoes'] = \App\Insp_aud_marcacao::whereNull('deleted_at')->where('unidade_id',$unidade_id_user)->orderby('data_ini')->get();
        }
        

        $data['unidade_id_user'] = $unidade_id_user;
        $this->valida_status_calendario($data['marcacoes']);
        
        return view('sgi.calendario')->with($data);
	}

	public function salvar_marcacao(){
		$input = Input::all();
		
		if(isset($input['id_marcacao'])){
			$marcacao = \App\Insp_aud_marcacao::find($input['id_marcacao']);
			$marcacao->updated_by     = Auth::user()->id;
		}else{
			$marcacao = new \App\Insp_aud_marcacao;	
			$marcacao->created_by     = Auth::user()->id;
			$marcacao->status         = 0;
		}
	
		$marcacao->insp_aud_id    = $input['insp_aud'];
		$marcacao->unidade_id     = $input['unidade'];
		$marcacao->responsavel_id = $input['responsavel'];
		$marcacao->data_ini       = $input['data_ini'];
		$marcacao->data_fim       = $input['data_fim'];

		$marcacao->save();
		
		$data['success'] = true;
		return Response::json($data);
	}

	public function valida_status_calendario($marcacoes){
		foreach ($marcacoes as $key => $marcacao) {
			$data_ini = Carbon::createFromFormat('Y-m-d H:i:s', $marcacao->data_ini);
			$data_fim = Carbon::createFromFormat('Y-m-d H:i:s', $marcacao->data_fim);
			$teste = Carbon::createFromFormat('Y-m-d H:i:s','2015-10-08 00:00:00' );
			$hoje	  =	Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
			$diferenca = $hoje->diffInDays($data_fim,false);
			if($marcacao->status != 2){
				if($diferenca >= 0){
					$marcacao->status = 0;
				}
				if($diferenca < 0){
					$marcacao->status = 1;
				}
				$marcacao->save();
			}
		}
	}

	public function carregar_marcacao(){
		$input = Input::all();
		$data['marcacao'] = \App\Insp_aud_marcacao::whereNull('deleted_at')->where('id',$input['id'])->first();
		$data['id_marcacao'] = $input['id'];
		$data['success'] = true;
		return Response::json($data);
	}

	public function excluir_marcacao(){
		$input = Input::all();
		$insp_aud = \App\Insp_aud_marcacao::find($input['id']);
		$insp_aud->deleted_by = Auth::user()->id;
		$insp_aud->deleted_at = date('Y-m-d H:i:s');
		$insp_aud->save();
		$data['success'] = true;
		return Response::json($data);
	}

	public function carregar_tabela_marcacao(){
		$id_user = Auth::user()->id;
		$input = Input::all();

		$insp_aud_uni = new \App\Insp_aud_marcacao;
		
		if($input['unidade'] != 0 && $input['unidade'] != null){
			$insp_aud_uni = $insp_aud_uni->where('unidade_id',$input['unidade']);
		}
		if($input['data_ini'] != 0 && $input['data_ini'] != null){
			$insp_aud_uni = $insp_aud_uni->where('data_ini','>=',$input['data_ini']);
		}
		if($input['data_fim'] != 0 && $input['data_fim'] != null){
			$insp_aud_uni = $insp_aud_uni->where('data_fim','<=',$input['data_fim']);
		}
		
		$data['insp_aud'] = $insp_aud_uni->whereNull('deleted_at')->get();
		
	
        $data['insp_aud_nomes'] = DB::table('inspecoes_auditorias')->lists('descricao','id');
        $data['unidades']         = \DB::table('unidade')->lists('unidade','id');
        $data['funcionarios'] = \App\Funcionario::whereNull('deleted_at')->lists('nome','id');
        $data['formularios_marcacao'] =  DB::table('insp_auds_unidade')->whereNotnull('id_marcacao')->lists('id','id_marcacao');

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;
        if(in_array(5,$permissoes_opcoes)){
        	$data['permi_sgi'] = true;
        }else{
        	$data['permi_sgi'] = false;
        }

		$data['success'] = true;
		return Response::json($data);
	}

	// public function verifica_marcacao($id,$data,$unidade){

	// 	$marcacao = \App\Insp_aud_marcacao::whereNull('deleted_at')
	// 		->where('insp_aud_id',$id)
	// 		->where('unidade_id',$unidade)
	// 		->where('data_ini','>=',$data)
	// 		->where('data_fim','<=',$data)
	// 		->first();
	// 	$marcacao->status = 2;
	// 	$marcacao->updated_by = Auth::user()->id;
	// 	$marcacao->save();
	// 	return $marcacao->id;

	// }
}
