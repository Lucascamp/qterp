<?php namespace App\Http\Controllers\patrimonio;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Permissaousuario;
use App\ItemAlmoxarifado;
use App\Almoxarifado;
use App\AlmoxarifadoHistorico;
use App\Centrocusto;
use App\Localizacaoequipamento;
use App\Funcionario;
use App\Unidade;
use App\User;
use App\Permissaoopcoesusuario;
use Carbon\Carbon;
use App\Http\Requests\CreateEquipamento;
use App\Http\Requests\CreateLocalizacao;

class AlmoxarifadoController extends Controller 
{
        public function __construct(
                                    ItemAlmoxarifado $item,
                                    Funcionario $funcionario,
                                    User $usuario,
                                    Almoxarifado $almoxarifado,
                                    AlmoxarifadoHistorico $historico,
                                    Centrocusto $centrocusto,
                                    Unidade $unidade
                                    )
        {
                $this->middleware('auth');
                $this->item = $item;
                $this->funcionario = $funcionario;
                $this->usuario = $usuario;
                $this->almoxarifado = $almoxarifado;
                $this->historico = $historico;
                $this->centrocusto = $centrocusto;
                $this->unidade = $unidade;
                $this->usuario = $usuario;
        }

    public function index()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        

        $data['usuario'] = Auth::user();

        $data['descricao']   = Input::get('filter_descricao');
        $data['unidade'] = Input::get('filter_unidade');

        $data['filter_descricao'] = ['' =>'Descrição'] + $this->item->lists('descricao', 'id');
        $data['filter_unidade'] = ['' =>'Unidade'] + $this->unidade->lists('unidade', 'id');

        $almoxarifadoQuery = $this->almoxarifado->with('unidade', 'item');

        if ($data['descricao'])
        {               
            $almoxarifadoQuery->where('item_id', $data['descricao']);
        }    

        if ($data['unidade'])
        {               
            $almoxarifadoQuery->where('unidade_id', $data['unidade']);
        }       

        $data['unidades'] = $this->unidade->lists('unidade', 'id');

        if(in_array(4,$permissoes_opcoes)){
            $data['almoxarifados'] = $almoxarifadoQuery->get();
        }else{
            $unidade_usuario =  Auth::user()->unidade;

            $id_unidade = \DB::table('unidade')->where('unidade',$unidade_usuario)->first();
            if($id_unidade){
                $almoxarifadoQuery->where('unidade_id',$id_unidade->id );    
            }else{
                $almoxarifadoQuery->where('unidade_id',999 );    
            }
            $data['almoxarifados'] = $almoxarifadoQuery->get();
        }
        return view('patrimonio.almoxarifado.index')->with($data);
    }

    public function salvarItem() 
    {
        $input = array_except(Input::all(), array('_token'));
        $input['created_by'] = Auth::user()->id;
        $this->item->create($input); 

        return redirect()->route('almoxarifado.index');
    }

    public function adicionarItem() 
    {
        $input = array_except(Input::all(), array('_token'));

        $item = $this->almoxarifado->find($input['id']);

        $data['total'] = intval($item->total + $input['adicionar']);
        $data['valor'] = $data['total']*$this->item->where('id',$item->item_id)->pluck('valor');

        $item->update(['total' => $data['total'], 'updated_by' => Auth::user()->id]);

        $this->historico->create([
                                    'unidade_id' => $item->unidade_id,
                                    'item_id' => $item->item_id,
                                    'total' => $item->total,
                                    'quantidade' => $input['adicionar'],
                                    'operacao' =>  'Adicionar',
                                    'created_by' => Auth::user()->id,
                                ]);

        $data['id'] = $input['id'];
        $data['success'] = true;

        return Response::json($data);
    }

    public function subtrairItem() 
    {
        $input = array_except(Input::all(), array('_token'));

        $item = $this->almoxarifado->find($input['id']);

        $data['total'] = intval($item->total - $input['subtrair']);
        $data['valor'] = $data['total']*$this->item->where('id',$item->item_id)->pluck('valor');

        if($data['total'] < 0)
        {
            $data['success'] = false;
            return Response::json($data);
        }

        $item->update(['total' => $data['total'], 'updated_by' => Auth::user()->id]);

        $this->historico->create([
                                    'unidade_id' => $item->unidade_id,
                                    'item_id' => $item->item_id,
                                    'total' => $item->total,
                                    'quantidade' => $input['subtrair'],
                                    'operacao' =>  'Subtrair',
                                    'created_by' => Auth::user()->id,
                                ]);

        $data['id'] = $input['id'];
        $data['success'] = true;

        return Response::json($data);
    }

    public function totalTransf() 
    {
        $input = array_except(Input::all(), array('_token'));

        $item = $this->almoxarifado->find($input['id']);

        $data['total'] = $item->total;

        return Response::json($data);
    }

    public function transferirItem() 
    {
        $input = array_except(Input::all(), array('_token'));

        $item = $this->almoxarifado->find($input['id']);

        $data['totalorigem'] = intval($item->total - $input['transferir']);
        $data['valororigem'] = $data['totalorigem']*$this->item->where('id',$item->item_id)->pluck('valor'); 

        if($data['totalorigem'] < 0)
        {
            $data['success'] = false;
            return Response::json($data);
        }

        $item->update(['total' => $data['totalorigem'], 'updated_by' => Auth::user()->id]);

        $this->historico->create([
                                    'unidade_id' => $item->unidade_id,
                                    'item_id' => $item->item_id,
                                    'total' => $item->total,
                                    'quantidade' => $input['transferir'],
                                    'operacao' =>  'Envio',
                                    'created_by' => Auth::user()->id,
                                ]);

        $id_transf = $this->almoxarifado->where('item_id', $item->item_id)
                                    ->where('unidade_id', $input['itensdestino'])
                                    ->pluck('id');

        if(isset($id_transf))
        {    
            $itemtransf = $this->almoxarifado->find($id_transf);
            $data['totaldestino'] = intval($itemtransf->total + $input['transferir']);
            $data['valordestino'] = $data['totaldestino']*$this->item->where('id', $itemtransf->item_id)->pluck('valor'); 
            $itemtransf->update(['total' => $data['totaldestino'], 'updated_by' => Auth::user()->id]);
        }
        else
        {
            $itemtransf = $this->almoxarifado->create([
                                            'unidade_id' => $input['itensdestino'],
                                            'item_id' => $item->item_id,
                                            'total' => $input['transferir'],
                                        ]);   

            $data['item_novo'] = $this->almoxarifado->with('unidade', 'item')->where('id', $itemtransf->id)->first();
            $data['item_novo']['valor'] = $data['item_novo']->total * $data['item_novo']->item->valor;
        }

        $this->historico->create([
                                    'unidade_id' => $itemtransf->unidade_id,
                                    'item_id' => $itemtransf->item_id,
                                    'total' => $itemtransf->total,
                                    'quantidade' => $input['transferir'],
                                    'operacao' =>  'Recebido',
                                    'created_by' => Auth::user()->id,
                                ]);

        $data['origemid'] = $item->id;
        $data['destinoid'] = $itemtransf->id;
        $data['success'] = true;

        return Response::json($data);
    }

    public function additmalm()
    {   
        $unidade = Input::get('unidade');

        $itens_cad_unidade = $this->almoxarifado->where('unidade_id', $unidade)->lists('item_id');

        $data['itens'] = $this->item->whereNotIn('id', $itens_cad_unidade)->lists('descricao','id'); 

        return Response::json($data);
    }

    public function additmalmsave() 
    {
        $input = array_except(Input::all(), array('_token'));

        $data['item_novo'] = $this->almoxarifado->create([
                                    'unidade_id' => $input['unidade'],
                                    'item_id' => $input['item'],
                                    'total' => $input['total'],
                                ]);
        
        $this->historico->create([
                                    'unidade_id' => $input['unidade'],
                                    'item_id' => $input['item'],
                                    'total' => $input['total'],
                                    'quantidade' => $input['total'],
                                    'operacao' =>  'Novo Item Almoxarifado',
                                    'created_by' => Auth::user()->id,
                                ]);

        

        $data['item_novo'] = $this->almoxarifado->with('unidade', 'item')->where('id', $data['item_novo']->id)->first();
        $data['valor'] = $input['total'] * $this->item->where('id',  $input['item'])->pluck('valor');

        $data['success'] = true;

        return Response::json($data);
    }

    public function historico()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['usuario'] = Auth::user();

        $data['unidades'] = ['' => 'Selecione a Unidade'] + $this->unidade->lists('unidade', 'id');

        return view('patrimonio.almoxarifado.historico')->with($data);
    }

    public function carregarHistorico()
    {   
        $unidade = Input::get('unidade');
        $item = Input::get('item');

        $historico = $this->historico->with('unidade', 'item')->orderby('created_at');

        if($unidade)
            $historico->where('unidade_id', $unidade);

        if($item)
            $historico->where('item_id', $item);

        $data['historico'] = $historico->get();
        $unidades = \DB::table('unidade')->lists('unidade','id');
        
        foreach ($data['historico'] as $key => $hist) 
        {
            $data['historico'][$key]['data_criacao'] = $hist->created_at->format('d/m/Y');
            if($hist->operacao == 'Envio'){
                $destino = \DB::table('almoxarifado_historico')
                        ->where('item_id',$hist->item_id)
                        ->where('quantidade',$data['historico'][$key]['quantidade'])
                        ->where('operacao','Recebido')
                        ->first();
                if($destino){
                    $data['historico'][$key]['unidade_destino'] = $unidades[$destino->unidade_id];
                }else{
                    $data['historico'][$key]['unidade_destino'] = '-';
                }
            }else{
                $data['historico'][$key]['unidade_destino'] = '-';
            }
        }

        $data['success']  = true;

        return Response::json($data);
    }

    public function dadosItemUnidade()
    {   
        $unidade = Input::get('unidade');

        $itens = $this->almoxarifado->where('unidade_id', $unidade)->lists('item_id'); 

        $data['itens'] = $this->item->whereIn('id', $itens)->lists('descricao','id');

        if(empty($data['itens']))
            $data['itens'] = [''=>'Não existem itens!'];                                                          

        return Response::json($data);
    }

     public function relatorioconsumo()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        
        $data['unidades'] = [0 => 'Todos' ] + \DB::table('unidade')->lists('unidade','id');
        $data['usuario'] = Auth::user();

        return view('patrimonio.almoxarifado.relatorioconsumo')->with($data);
    }

    public function carregatabelaconsumo(){
        $input = array_except(Input::all(), array('_token'));
        $data['success'] = true;
        $data['mensagem'] = 'ok';
        if($input['unidade'] == 0){
            $consumo = \DB::connection('CENTRAL')->table('almoxarifado_historico')
                ->select(\DB::raw('item_id,unidade_id, SUM(quantidade) as quantidade'))
                ->where('operacao','Subtrair')
                ->where('created_at','>=',$input['data_ini'])
                ->where('created_at','<=',$input['data_fim'])
                ->groupby('item_id' )
                ->groupby('unidade_id')
                ->get();
        }else{
            $consumo = \DB::connection('CENTRAL')->table('almoxarifado_historico')
                ->select(\DB::raw('item_id,unidade_id, SUM(quantidade) as quantidade'))
                ->where('operacao','Subtrair')
                ->where('created_at','>=',$input['data_ini'])
                ->where('created_at','<=',$input['data_fim'])
                ->where('unidade_id',$input['unidade'])
                ->groupby('item_id' )
                ->groupby('unidade_id')
                ->get();
        }
        $data['unidades'] = \DB::table('unidade')->lists('unidade','id');
        $row = [];
        foreach ($consumo as $key => $value) {
            $row[$key] = (array) $value;
            $item_almoxarifado = \DB::table('item_almoxarifado')->where('id',$value->item_id)->first();
            $row[$key]['descricao'] = $item_almoxarifado->descricao;
            $row[$key]['codigo_protheus'] = $item_almoxarifado->codigo;
            $row[$key]['valor_medio'] = $item_almoxarifado->valor;
        }
        $data['row'] = $row;
        return Response::json($data);
    }

}
