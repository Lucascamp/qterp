<?php namespace App\Http\Controllers\patrimonio;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Permissaousuario;
use App\Equipamento;
use App\Centrocusto;
use App\Localizacaoequipamento;
use App\Funcionario;
use App\User;
use Carbon\Carbon;
use App\Http\Requests\CreateEquipamento;
use App\Http\Requests\CreateLocalizacao;
use Excel;

class EquipamentoController extends Controller 
{
        public function __construct(
                                    Equipamento $equipamento,
                                    Funcionario $funcionario,
                                    User $usuario,
                                    Localizacaoequipamento $localizacao,
                                    Centrocusto $centrocusto
                                    )
        {
                $this->middleware('auth');
                $this->equipamento = $equipamento;
                $this->localizacao = $localizacao;
                $this->centrocusto = $centrocusto;
                $this->funcionario = $funcionario;
                $this->usuario = $usuario;
        }

    public function index()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();

        $descricao   = Input::get('filter_descricao');
        $patrimonio = Input::get('filter_patrimonio');
        $serial = Input::get('filter_serial');
        $modelo = Input::get('filter_modelo');
        $local_equip = Input::get('filter_local');

        $filter_descricao = $this->equipamento->orderBy('descricao')->groupBy('descricao')->lists('descricao', 'descricao');

        $locais = $this->localizacao->groupBy('centrocusto_id')->lists('centrocusto_id');

        $filter_local = $this->centrocusto
                                 ->whereNull('deleted_at')
                                 ->whereIn('id', $locais)
                                 ->lists('CTD_DESC01','id');   

        if($descricao)
            $filter_patrimonio = $this->equipamento->where('descricao', $descricao)->orderBy('patrimonio')->groupBy('patrimonio')->lists('patrimonio', 'patrimonio');
        
        else
            $filter_patrimonio = $this->equipamento->orderBy('patrimonio')->groupBy('patrimonio')->lists('patrimonio', 'patrimonio');
        
        if($descricao)
            $filter_serial = $this->equipamento->where('descricao', $descricao)->orderBy('serial')->groupBy('serial')->lists('serial', 'serial');
       
        else
            $filter_serial = $this->equipamento->orderBy('serial')->groupBy('serial')->lists('serial', 'serial');
       
        if($descricao)
            $filter_modelo = $this->equipamento->where('descricao', $descricao)->orderBy('modelo')->groupBy('modelo')->lists('modelo', 'modelo');
       
        else
            $filter_modelo = $this->equipamento->orderBy('modelo')->groupBy('modelo')->lists('modelo', 'modelo');

        if($local_equip)
        {
            $equips = $this->equipamento->filterLocalizacao($local_equip);
            
            $equipamentosQuery = $this->equipamento->whereIn('id', $equips)->orderby('descricao', 'ASC')->whereNull('deleted_at')->orderby('aplicacao', 'ASC');
        }
        else
            $equipamentosQuery = $this->equipamento->orderby('descricao', 'ASC')->orderby('aplicacao', 'ASC')->whereNull('deleted_at');

        if ($descricao)
        {               
            $equipamentosQuery->where('descricao', $descricao);
        } 

        if ($patrimonio)
        {               
            $equipamentosQuery->where('patrimonio', $patrimonio);
        }

        if ($serial)
        {               
            $equipamentosQuery->where('serial', $serial);
        }

        if ($modelo)
        {               
            $equipamentosQuery->where('modelo', $modelo);
        }

        $data['total_equips'] = count($equipamentosQuery->get());

        $data['equipamentos'] = $equipamentosQuery->paginate(100);

        $data['centro_custo'] = $this->centrocusto->lists('CTD_DESC01', 'id');

        foreach ($data['equipamentos'] as $equipamento) 
        {
            $localizacao[$equipamento->id] = $this->localizacao
                                                    ->where('equipamento_id', $equipamento->id)
                                                    ->orderBy('data_movimentacao', 'DESC')
                                                    ->orderBy('updated_at', 'DESC')
                                                    ->whereNull('deleted_at')
                                                    ->first();
        } 

        if(isset($localizacao))
            $data['localizacao'] = $localizacao;

        $data['funcionarios'] = ['' => 'Selecione o funcionário'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id'); 

        $data['usuarios'] = ['' => 'Selecione o usuário'] + $this->usuario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');

        $data['centro_custo_novo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');                
        $data['patrimonio'] = $patrimonio;
        $data['descricao'] = $descricao;
        $data['serial'] = $serial;
        $data['modelo'] = $modelo;
        $data['local_equip'] = $local_equip;
        $data['filter_descricao'] = ['' =>'Descrição'] + $filter_descricao;
        $data['filter_patrimonio'] = ['' =>'Patrimônio'] + $filter_patrimonio;
        $data['filter_serial'] = ['' =>'Serial'] + $filter_serial;
        $data['filter_modelo'] = ['' =>'Modelo'] + $filter_modelo;
        $data['filter_local'] = ['' =>'Localização'] + $filter_local;

        return view('patrimonio.equipamento.index')->with($data);
    }






















    public function equipamentos(){
        $id_user = Auth::user()->id;
        $data['id_user'] = $id_user;
        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();
         


        $data['funcionarios'] = [0 => 'Selecione o funcionário'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id'); 

        $data['usuarios'] = ['' => 'Selecione o usuário'] + $this->usuario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');

        
        $data['unidades'] = [ '0' => 'Todos'] + \DB::table('unidade')->lists('unidade','id');
        $data['unidades_movi'] = \DB::table('unidade')->lists('unidade','id');
        $unidade = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');

        if(!$unidade || $unidade == 1){
            $unidade = 0;
        }
        $data['unidade'] =  $unidade;
        return view('patrimonio.equipamento.novo_index')->with($data);
    }


    public function movimentar_equipamento(){
        $input = array_except(Input::all(), array('_token'));
        if(isset($input['obs'])){
            $loc_fonte = new \App\Fontes_localizacao;
            $loc_fonte->fonte_id = $input['fonte_id'];
            $loc_fonte->unidade =  $input['unidade_destino'];
            $loc_fonte->data = $input['data_movimento'];
            $loc_fonte->observacoes = $input['obs'];
            $loc_fonte->save();
        }
        
        $equipamento = \DB::table('equipamentos')->where('id',$input['id'])->first();

        $historico  = new \App\Equipamentohistorico;

        $historico->usuario_id =  Auth::user()->id;
        $historico->unidade_remetente = $equipamento->local_atual;  

        $historico->funcionario_id = $input['funcinario_destino'];     
        $historico->data_movimentacao = $input['data_movimento'];     
        $historico->equipamento_id = $input['id'];     
        $historico->unidade_destino = $input['unidade_destino'];     
        $historico->motivo = $input['motivo'];     
        $historico->projeto = $input['projeto'];
        $historico->tipo = 'ENVIO';
        $historico->save();

        $update['status'] = 1;
        $update['updated_at'] = date('Y-m-d H:i:s');
        $update['updated_by'] = Auth::user()->id;
        \DB::table('equipamentos')->where('id',$input['id'])->update($update);
        $data['mensagem'] = 'Equipamento Enviado';
        $data['id'] = $input['id'];
        $data['data_movimentacao'] = $input['data_movimento'];
        $data['destino'] = \DB::table('unidade')->where('id',$historico->unidade_destino)->pluck('unidade');
        $data['nome_user'] = Auth::user()->nome;
        $data['funcionario_nome'] = \DB::table('funcionarios')->where('id',$input['funcinario_destino'])->pluck('nome');
        $data['success'] = true;
        return Response::json($data);

    }

    public function receber_equipamento(){
        $input = array_except(Input::all(), array('_token')); 
        
        $equipamento = \DB::table('equipamentos')->where('id',$input['id'])->first();
        $id_historico = \DB::table('equipamento_historico')->where('equipamento_id',$input['id'])
                ->where('tipo','ENVIO')->max('id');
        $historico_envio = \DB::table('equipamento_historico')->where('id', $id_historico)
                    ->first();


        $historico  = new \App\Equipamentohistorico;

        $historico->usuario_id =  Auth::user()->id;
        $historico->unidade_remetente = $historico_envio->unidade_remetente;  

        $historico->funcionario_id = $historico_envio->funcionario_id;
        $historico->data_movimentacao = $historico_envio->data_movimentacao;     
        $historico->equipamento_id = $input['id'];     
        $historico->unidade_destino = $historico_envio->unidade_destino;   
        $historico->motivo = $historico_envio->motivo; 
        $historico->projeto = $historico_envio->projeto;   
        $historico->tipo = 'RECEBIMENTO';

        
        $historico->save();

        $update['status'] = 0;
        $update['local_atual'] = $historico_envio->unidade_destino;
        $update['updated_at'] = date('Y-m-d H:i:s');
        $update['updated_by'] = Auth::user()->id;
        \DB::table('equipamentos')->where('id',$input['id'])->update($update);

        $data['mensagem'] = 'Equipamento Recebido';
        $data['id'] = $input['id'];
        $data['data_movimentacao'] = $historico->data_movimentacao;
        $data['destino'] = \DB::table('unidade')->where('id',$historico->unidade_destino)->pluck('unidade');
        $data['nome_user'] = Auth::user()->nome;
        $data['funcionario_nome'] = \DB::table('funcionarios')->where('id',$historico_envio->funcionario_id)->pluck('nome');
        $data['success'] = true;
        return Response::json($data);

    }

    public function carregarTable(){
        $input = array_except(Input::all(), array('_token'));
        $data['success'] = true;
        $data['mensagem'] = 'ok';
        

        if($input['filtro'] == 'false'){
            if($input['unidade'] == 0){
                $data['equipamentos'] = DB::table('equipamentos')->whereNull('deleted_at')->get();
                $aux_equip =  DB::table('equipamentos')->whereNull('deleted_at')->lists('id');      
            }else{
                $data['equipamentos'] = DB::table('equipamentos')->whereNull('deleted_at')->where('local_atual',$input['unidade'])->get();
                $aux_equip =  DB::table('equipamentos')->whereNull('deleted_at')->where('local_atual',$input['unidade'])->lists('id');
            }
        }else{
            if($input['unidade'] == 0){
                $data['equipamentos'] = DB::table('equipamentos')->whereNull('deleted_at')->where('status',1)->get();
                $aux_equip =  DB::table('equipamentos')->whereNull('deleted_at')->where('status',1)->lists('id');
            }else{
                $data['equipamentos'] = DB::table('equipamentos')->whereNull('deleted_at')->where('local_atual',$input['unidade'])->where('status',1)->get();
                $aux_equip =  DB::table('equipamentos')->whereNull('deleted_at')->where('local_atual',$input['unidade'])->where('status',1)->lists('id');
            }
        }
        // dd($input);
        // dd($data['equipamentos']);
        // dd($aux_equip);

        $equipamentos = DB::table('equipamentos')->whereNull('deleted_at')->get();
        foreach ($equipamentos as $equipamento) 
        {
            if($equipamento->status == 1){
                $id_historico = \DB::table('equipamento_historico')->where('equipamento_id',$equipamento->id)
                ->where('tipo','ENVIO')->max('id');

                $st = \DB::table('equipamento_historico')->where('id', $id_historico)->first();
                if($st){ $status[$equipamento->id] = $st; }
                $responsaveis_unidade = \DB::table('responsaveis_unidade')->where('unidade_id',$status[$equipamento->id]->unidade_destino)->first();                
                $responsaveis[$equipamento->id] = explode(';', $responsaveis_unidade->responsaveis);
            }else{
                $id_historico = \DB::table('equipamento_historico')->where('equipamento_id',$equipamento->id)
                ->where('tipo','RECEBIMENTO')->max('id');

                $st = \DB::table('equipamento_historico')->where('id', $id_historico)->first();
                if($st){ $status[$equipamento->id] = $st;}
            }
            
        }
        
        if($input['unidade'] != 0){
            foreach ($status as $key => $st) {
                if(isset($st) && $st->tipo == 'ENVIO' && $st->unidade_destino == $input['unidade']){
                    if(!in_array($st->equipamento_id, $aux_equip)){
                        array_push($data['equipamentos'],  DB::table('equipamentos')->where('id',$st->equipamento_id)->first());
                    } 
                    
                }
            }
        }
        // dd($data['equipamentos']);
        if(isset($status)) $data['status'] = $status;
        if(isset($responsaveis)) $data['responsaveis'] = $responsaveis;
        
        $data['unidades'] = \DB::table('unidade')->lists('unidade','id');
        $data['funcionarios'] = ['' => 'Selecione o funcionário'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id'); 

        $data['usuarios'] = $this->usuario->whereNull('deleted_at')->lists('nome','id');

        $data['user_id'] = ''.Auth::user()->id;
        $data['unidade'] = Auth::user()->unidade;
        $data['unidade_filtro'] = $input['unidade'];
        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id', Auth::user()->id)->lists('permissao_opcoes_id');
        // $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $unidade_id_user = \DB::table('unidade')->where('unidade','like', Auth::user()->unidade)->pluck('id');

        if(!$unidade_id_user){
            $unidade_id_user = 0;
        }
        $data['unidade_id_user'] =  $unidade_id_user;

        return Response::json($data);
    }

    public function carregarHistoricoModal()
    {   
        $equipamento_id = Input::get('equipamento_id');

        $equipamento = \DB::table('equipamentos')->where('id',$equipamento_id)->first();
        $historico = \DB::table('equipamento_historico')->where('equipamento_id',$equipamento_id)->orderby('created_at','desc')->get();

        $funcionarios = $this->funcionario->lists('nome','id'); 

        $usuarios = $this->usuario->lists('nome','id');
         $unidades = \DB::table('unidade')->lists('unidade','id');


        return Response::json([ 'success' => true,
                                'equipamento' => $equipamento,
                                'historico' => $historico,
                                'funcionarios' => $funcionarios,
                                'usuarios' => $usuarios,
                                'unidades' => $unidades
                            ]);
    }

    public function enviar_manutencao(){
        $input = array_except(Input::all(), array('_token'));
        
        $equipamento = \DB::table('equipamentos')->where('id',$input['id'])->first();

        $historico  = new \App\Equipamentohistorico;

        $historico->usuario_id =  Auth::user()->id;
        $historico->unidade_remetente = $equipamento->local_atual;  

        $historico->funcionario_id = 0;
        $historico->data_movimentacao = $input['data_movimentacao'];
        $historico->equipamento_id = $input['id'];     
        $historico->unidade_destino = $equipamento->local_atual;
        $historico->motivo = $input['obs'];     
        $historico->projeto = 'MANUTENÇÂO';
        $historico->tipo = 'ENVIO';
        $historico->save();

        $update['status'] = 2;
        $update['updated_at'] = date('Y-m-d H:i:s');
        $update['updated_by'] = Auth::user()->id;
        \DB::table('equipamentos')->where('id',$input['id'])->update($update);
        
        $data['mensagem'] = 'Equipamento Enviado para Manutenção';

        return Response::json($data);

    }












    public function duplicados()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%patrimonio%')
                    ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();

        $equipamentos = $this->localizacao->whereNull('deleted_at')->groupBy('equipamento_id')->lists('equipamento_id');

        foreach ($equipamentos as $equipamento)
        {
            $local[$equipamento] = $this->localizacao->whereNull('deleted_at')->where('equipamento_id', $equipamento)->get();

            if(count($local[$equipamento]) > 2)
            {
               $duplicados[$equipamento] = array();
               $equip_duplicados[$equipamento] = array(); 

               foreach ($local[$equipamento] as $local)
               {
                    if(!in_array($local->data_movimentacao->format('d/m/Y'), $duplicados[$equipamento]))
                    {
                        $data = $local->data_movimentacao->format('d/m/Y');
                        array_push($duplicados[$equipamento], $data); 
                    }    
                    else
                        array_push($equip_duplicados[$equipamento], $equipamento); 
               }

               if(!empty($equip_duplicados[$equipamento]))
                   var_dump($equip_duplicados[$equipamento]);
            }   
        }
        
        dd('fim');

        return view('patrimonio.equipamento.cadastrar')->with($data);
    }

    public function cadastrar()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%patrimonio%')
                    ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();

        $data['centro_custo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');

        $data['funcionarios'] = ['' => 'Selecione o funcionário'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id'); 

        $data['usuarios'] = ['' => 'Selecione o usuário'] + $this->usuario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');
        
        return view('patrimonio.equipamento.cadastrar')->with($data);
    }

    public function salvar(CreateEquipamento $Vequip, CreateLocalizacao $VLocal) 
    {
        $input = array_except(Input::all(), array('_token'));

        $equipamento = $this->equipamento->create($Vequip->all());
        

        // $localizacao = [
        //                 'equipamento_id' => $equipamento->id,
        //                 'centrocusto_id' => $input['centrocusto_id'],
        //                 'projeto' =>  $input['projeto'],
        //                 'data_movimentacao' =>  $input['data_movimentacao']
        //                 ];

        // $this->localizacao->create(array_merge($VLocal->all(), $localizacao)); 

        return redirect()->route('equipamento.index');
    }

    public function editar($id)
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 

        $data['usuario'] = Auth::user();

        $data['equipamento'] = $this->equipamento->findOrFail($id);

        $data['localizacao'] = $this->localizacao->where('equipamento_id', $id)->orderBy('data_movimentacao', 'DESC')->first();

        $data['centro_custo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');

        $data['funcionarios'] = ['' => 'Selecione o funcionário'] + $this->funcionario
                        ->whereNull('deleted_at')
                        ->lists('nome','id'); 

        $data['usuarios'] = ['' => 'Selecione o usuário'] + $this->usuario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');

        return view('patrimonio.equipamento.editar')->with($data);
    }

    public function atualizar($id , CreateEquipamento $Vequip, CreateLocalizacao $VLocal) 
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $equipamento = $this->equipamento->findOrFail($id);

        $equipamento->update($Vequip->all());

        // $localizacao = $this->localizacao->where('equipamento_id', $equipamento->id)->orderBy('data_movimentacao', 'DESC')->first();

        // if(isset($localizacao))
        // {
        //     $localizacao->centrocusto_id = $input['centrocusto_id'];
        //     $localizacao->projeto = $input['projeto'];
        //     $localizacao->data_movimentacao = $input['data_movimentacao'];

        //     $localizacao->save();
        // }

        // else
        // {
        //     $localizacao = [
        //                 'equipamento_id' => $equipamento->id,
        //                 'centrocusto_id' => $input['centrocusto_id'],
        //                 'projeto' =>  $input['projeto'],
        //                 'data_movimentacao' =>  $input['data_movimentacao']
        //                 ];

        //     $this->localizacao->create(array_merge($VLocal->all(), $localizacao));     
        // }

        return redirect()->route('equipamento.index');
    }

    public function movimentar(CreateLocalizacao $VLocal)
    {
        $input = array_except(Input::all(), array('_token'));

        $localizacao = [
                        'equipamento_id' => $input['equipamento_id'],
                        'centrocusto_id' => $input['centrocusto_id'],
                        'projeto' =>  $input['projeto'],
                        'data_movimentacao' =>  $input['data_movimentacao']
                        ];

        $this->localizacao->create(array_merge($VLocal->all(), $localizacao));     

        return redirect()->route('equipamento.index');
    }

    public function destroy($id)
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $this->equipamento->find($input['equipamento_id'])->delete();

        return redirect()->route('equipamento.index');
    }

    public function historico()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['usuario'] = Auth::user();

        $data['descricoes'] = ['' => 'Selecione a Descição'] + $this->equipamento->groupBy('descricao')->lists('descricao', 'descricao');

        return view('patrimonio.equipamento.historico')->with($data);
    }

    public function dadosDescricao()
    {   
        $descricao = Input::get('descricao');

        $data['patrimonio'] = $this->equipamento->where('descricao', $descricao)->lists('patrimonio','id'); 

        if(empty($data['patrimonio']))
            $data['patrimonio'] = $this->equipamento->lists('patrimonio','id');
                                        
        $data['serial'] = $this->equipamento->where('descricao', $descricao)->groupBy('serial')->lists('serial','id');  

        if(empty($data['serial']))
            $data['serial'] = $this->equipamento->groupBy('serial')->lists('serial','id');

        $data['modelo'] = $this->equipamento->where('descricao', $descricao)->groupBy('modelo')->lists('modelo','id');

         if(empty($data['modelo']))
            $data['modelo'] = $this->equipamento->groupBy('modelo')->lists('modelo','id');                                                            

        return Response::json($data);
    }

    public function carregarHistorico()
    {   
        $descricao = Input::get('descricao');
        $aplicacao = Input::get('aplicacao');
        $patrimonio = Input::get('patrimonio');

        $equipamento = $this->equipamento->where('descricao', $descricao)
                                          ->where('aplicacao', $aplicacao)
                                          ->where('patrimonio', $patrimonio) 
                                          ->first();     

        $locais = $this->localizacao->where('equipamento_id', $equipamento->id)->orderby('data_movimentacao', 'DESC')->get();

        $centrocusto =  $this->centrocusto->lists('CTD_DESC01', 'id');

        $funcionarios = $this->funcionario
                                ->whereNull('deleted_at')
                                ->lists('nome','id'); 

        $usuarios = $this->usuario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');
        $localizacao = array();

        foreach ($locais as $key => $local)
        {
            if(isset($local->data_movimentacao))
                $localizacao[$key]['data'] = $local->data_movimentacao->format('d/m/Y');
            else
                $localizacao[$key]['data'] = '-';

            $localizacao[$key]['centrocusto'] = $centrocusto[$local->centrocusto_id];

            if($local->funcionario_responsavel != 0)
                $localizacao[$key]['funcionario'] = $funcionarios[$local->funcionario_responsavel];
            else
                $localizacao[$key]['funcionario'] = "N/A";

            if($local->usuario_receptor != 0)
                $localizacao[$key]['usuario'] = $usuarios[$local->usuario_receptor];
            else
                $localizacao[$key]['usuario'] = "N/A";

            $localizacao[$key]['projeto'] = $local->projeto;
            $localizacao[$key]['motivo'] = $local->motivo;
            $localizacao[$key]['recebido'] = $local->recebido;
        }        

        return Response::json(['success' => true,
                               'equipamento' => $equipamento,
                               'localizacao' => $localizacao        
                            ]);
    }

    public function carregarHistoricoModal2()
    {   
        $equipamento_id = Input::get('equipamento_id');

        $equipamento = $this->equipamento->where('id', $equipamento_id)->first();   
        
        $locais = $this->localizacao->whereNull('deleted_at')->where('equipamento_id', $equipamento->id)->orderby('data_movimentacao', 'DESC')->orderby('updated_at', 'DESC')->orderby('created_at', 'DESC')->get();

        $centrocusto =  $this->centrocusto->lists('CTD_DESC01', 'id');


        $funcionarios = $this->funcionario->lists('nome','id'); 

        $usuarios = $this->usuario->lists('nome','id');

        $localizacao = array();

        foreach ($locais as $key => $local)
        {
            if(isset($local->data_movimentacao))
                $localizacao[$key]['data'] = $local->data_movimentacao->format('d/m/Y');
            else
                $localizacao[$key]['data'] = '-';

            $localizacao[$key]['centrocusto'] = $centrocusto[$local->centrocusto_id];

            if($local->funcionario_responsavel != 0)
                $localizacao[$key]['funcionario'] = $funcionarios[$local->funcionario_responsavel];
            else
                $localizacao[$key]['funcionario'] = "N/A";

            if($local->usuario_receptor != 0)
                $localizacao[$key]['usuario'] = $usuarios[$local->usuario_receptor];
            else
                $localizacao[$key]['usuario'] = "N/A";

            $localizacao[$key]['projeto'] = $local->projeto;
            $localizacao[$key]['motivo'] = $local->motivo;
            $localizacao[$key]['recebido'] = $local->recebido;
        }        

        if(!empty($localizacao))
            $success = true;
        else
            $success = false;

        return Response::json(['success' => $success,
                               'equipamento' => $equipamento,
                               'localizacao' => $localizacao        
                            ]);
    }

    public function receber()
    {
        $id = Input::get('id');

        $this->localizacao->where('id', $id)->update(array('recebido' => '1'));
    }


    public function excel()
    {   
        set_time_limit(0);

        $equipamentos = $this->equipamento
                                ->whereNull('deleted_at')
                                ->orderby('descricao', 'ASC')
                                ->orderby('aplicacao', 'ASC')
                                ->get();
     
        Excel::create('Todos equipamentos' , function ($excel) use ($equipamentos) {

            $excel->sheet('Equipamentos', function ($sheet) use ($equipamentos) {

                $sheet->row(1, array(
                            'Descrição',
                            'Aplicação',
                            'Marca',
                            'Modelo',
                            'Serial',
                            'Patrimônio',
                            'Descrição de uso',
                            'Fornecedor',
                            'Localização',
                            'Propietário',
                            'Regime'
                        ));

                $sheet->row(1, function($row) {
                    $row->setBackground('#2A8005');
                    $row->setFontColor('#ffffff');
                    $row->setFontWeight('bold');
                });

                $i = 2;
                foreach ($equipamentos as $equipamento) 
                {
                    $centro_custo = $this->centrocusto->where('deleted_at', null)->lists('CTD_DESC01','id');

                    $localizacao = $this->localizacao
                                        ->where('equipamento_id', $equipamento->id)
                                        ->orderBy('data_movimentacao', 'DESC')
                                        ->whereNull('deleted_at')
                                        ->first();

                    if(!$localizacao)
                        $localizacao = '-';
                    else     
                        $localizacao = $centro_custo[$localizacao->centrocusto_id];                  

                        $sheet->row($i, array(
                            $equipamento->descricao,
                            $equipamento->aplicacao,
                            $equipamento->marca,
                            $equipamento->modelo,
                            $equipamento->serial,
                            $equipamento->patrimonio,
                            $equipamento->descricao_uso,
                            $equipamento->fornecedor,
                            $localizacao,
                            $equipamento->proprietario,
                            $equipamento->regime
                        ));
                        if($i % 2 != 0)
                        {
                        $sheet->row($i, function($row) {
                            $row->setBackground('#D1D1D1');
                            });
                        }    
                
                        $i++;
                    }
            });

            })->export('xls');
            
    }


}
