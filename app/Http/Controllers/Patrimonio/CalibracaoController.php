<?php namespace App\Http\Controllers\patrimonio;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Permissaousuario;
use App\Equipamento;
use App\Centrocusto;
use App\Localizacaoequipamento;
use App\Funcionario;
use App\User;
use App\Calibracao;
use Carbon\Carbon;
use App\Http\Requests\CreateEquipamento;
use App\Http\Requests\CreateLocalizacao;

class CalibracaoController extends Controller 
{
        public function __construct(
                                    Equipamento $equipamento,
                                    Funcionario $funcionario,
                                    User $usuario,
                                    Localizacaoequipamento $localizacao,
                                    Centrocusto $centrocusto,
                                    Calibracao $calibracao
                                    )
        {
                $this->middleware('auth');
                $this->equipamento = $equipamento;
                $this->localizacao = $localizacao;
                $this->centrocusto = $centrocusto;
                $this->funcionario = $funcionario;
                $this->usuario = $usuario;
                $this->calibracao = $calibracao;
        }

    public function index()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['usuario'] = Auth::user();

        $descricao   = Input::get('filter_descricao');
        $patrimonio = Input::get('filter_patrimonio');
        $serial = Input::get('filter_serial');
        $vencimento = Input::get('filter_vencimento');
        $local_equip = Input::get('filter_local');
        
        $locais = $this->localizacao->groupBy('centrocusto_id')->lists('centrocusto_id');

        $filter_local = $this->centrocusto
                                 ->whereNull('deleted_at')
                                 ->whereIn('id', $locais)
                                 ->lists('CTD_DESC01','id');

        $filter_descricao = $this->equipamento->where('calibravel', 1)->orderBy('descricao')->groupBy('descricao')->lists('descricao', 'descricao');
        
        if($descricao)
            $filter_patrimonio = $this->equipamento->where('calibravel', 1)->where('descricao', $descricao)->orderBy('patrimonio')->groupBy('patrimonio')->lists('patrimonio', 'patrimonio');
        
        else
            $filter_patrimonio = $this->equipamento->where('calibravel', 1)->orderBy('patrimonio')->groupBy('patrimonio')->lists('patrimonio', 'patrimonio');
        
        if($descricao)
            $filter_serial = $this->equipamento->where('calibravel', 1)->where('descricao', $descricao)->orderBy('serial')->groupBy('serial')->lists('serial', 'serial');
       
        else
            $filter_serial = $this->equipamento->where('calibravel', 1)->orderBy('serial')->groupBy('serial')->lists('serial', 'serial');
       
        
        $equipamentosQuery = $this->equipamento->whereNull('deleted_at')->orderby('id', 'ASC'); //->with('localizacao')

        if($local_equip)
        {
            $equips = $this->equipamento->filterLocalizacao($local_equip);
            
            $equipamentosQuery = $this->equipamento->whereIn('id', $equips)->orderby('descricao', 'ASC')->orderby('aplicacao', 'ASC');
        }

        if ($descricao)
        {               
            $equipamentosQuery->where('descricao', $descricao);
        } 

        if ($patrimonio)
        {               
            $equipamentosQuery->where('patrimonio', $patrimonio);
        }

        if ($serial)
        {               
            $equipamentosQuery->where('serial', $serial);
        }

        if ($vencimento)
        {               
            $dia_vencimento = Carbon::now()->addDays($vencimento);

            $vencidos = $this->calibracao->filtrovencimento($vencimento, $dia_vencimento);

            $equipamentosQuery->whereIn('id', $vencidos)->where('frequencia', '<>', 'UNICA');
        }

        $equipamentosQuery->where('calibravel', 1);

        $equipamentos = $equipamentosQuery->get();

        $latestCalibracao = $this->calibracao->latest($equipamentos);

        foreach ($equipamentos as $equipamento) 
        {
            $localizacao[$equipamento->id] = $this->localizacao
                                                    ->where('equipamento_id', $equipamento->id)
                                                    ->orderBy('data_movimentacao', 'DESC')
                                                    ->whereNull('deleted_at')
                                                    ->first();
        } 

        if(isset($localizacao))
            $data['localizacao'] = $localizacao;

        $filter_vencimento = array('' => 'Vencimento em Dias',
                                   'zero' => 'vencido',
                                   '30' => '30 dias',
                                   '60' => '60 dias',
                                   'calibrados' => 'Calibrados');

        $data['patrimonio']  = $patrimonio;
        $data['descricao']  = $descricao;
        $data['serial']  = $serial;
        $data['vencimento'] = $vencimento;
        $data['local_equip'] = $local_equip;
        $data['filter_descricao'] = [0 =>'Selecione uma descrição'] + $filter_descricao;
        $data['filter_patrimonio'] = [0 =>'Selecione um patrimônio'] + $filter_patrimonio;
        $data['filter_serial'] = [0 =>'Selecione um serial'] + $filter_serial;
        $data['filter_vencimento'] = $filter_vencimento;
        $data['filter_local'] = ['' =>'Localização'] + $filter_local;
        $data['equipamentos'] = $equipamentos;
        $data['centro_custo'] = $this->centrocusto->lists('CTD_DESC01', 'id');
        $data['latestCalibracao'] = $latestCalibracao;

        return view('patrimonio.calibracao.index')->with($data); 
    }

    public function cadastrar($id)
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%patrimonio%')
                    ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();

        $data['equipamento'] = $this->equipamento->findOrFail($id);

        return view('patrimonio.calibracao.cadastrar')->with($data);
    }

    public function salvar() 
    {
        $input = array_except(Input::all(), array('_token'));

        $file = Input::file('arquivo_calibracao');

        $equipamento = $this->equipamento->findOrFail($input['equipamento_id']);

        if(isset($file))
        {
            $input['filepath'] = 'arquivos_calibracao/'.$file->getClientOriginalName();
            $file->move('arquivos_calibracao/', $file->getClientOriginalName());
        }

        if(!empty($input['primeiro_uso']))
        {
            $primeiro_uso = clone(Carbon::createFromFormat('Y-m-d', $input['primeiro_uso']));

            if(is_numeric($equipamento->frequencia))
            {
                $input['vencimento'] = $primeiro_uso->addMonths($this->equipamento->where('id', $input['equipamento_id'])->pluck('frequencia'));
                $input['vencimento'] = $input['vencimento']->format('Y-m-d');
            }
            else
                $input['vencimento']='';
        }

        $this->calibracao->create($input);

        return redirect()->route('calibracao.index');
    }

    public function editar($id)
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 

        $data['usuario'] = Auth::user();

        $data['calibracao'] = $this->calibracao->with('equipamento')->findOrFail($id);   

        if($data['calibracao']->created_by != 0)
            $data['usuario_resp'] = $this->usuario->findOrFail($data['calibracao']->created_by);
        else     
            $data['usuario_resp'] = null;

        return view('patrimonio.calibracao.editar')->with($data);
    }

    public function atualizar($id) 
    {
        $input = array_except(Input::all(), array('_method', '_token'));

        $file = Input::file('arquivo_calibracao');

        $calibracao = $this->calibracao->with('equipamento')->findOrFail($id);

        if(isset($file))
        {
            $input['filepath'] = 'arquivos_calibracao/'.$file->getClientOriginalName();
            $file->move('arquivos_calibracao/', $file->getClientOriginalName());
        }

        if(!empty($input['primeiro_uso']))
        {
            $primeiro_uso = clone(Carbon::createFromFormat('Y-m-d', $input['primeiro_uso']));

            if(is_numeric($calibracao->equipamento->frequencia))
            {
                $input['vencimento'] = $primeiro_uso->addMonths($calibracao->equipamento->where('id', $calibracao->equipamento_id)->pluck('frequencia'));
                $input['vencimento'] = $input['vencimento']->format('Y-m-d');  
            }
            else
                $input['vencimento']='';
        }

        $calibracao->update($input);

        return redirect()->route('calibracao.index');
    }

    public function dadosDescricao()
    {   
        $descricao = Input::get('descricao');

        $data['patrimonio'] = $this->equipamento->where('descricao', $descricao)->lists('patrimonio','id'); 

        if(empty($data['patrimonio']))
            $data['patrimonio'] = $this->equipamento->lists('patrimonio','id');
                                        
        $data['serial'] = $this->equipamento->where('descricao', $descricao)->groupBy('serial')->lists('serial','id');  

        if(empty($data['serial']))
            $data['serial'] = $this->equipamento->groupBy('serial')->lists('serial','id');

        $data['modelo'] = $this->equipamento->where('descricao', $descricao)->groupBy('modelo')->lists('modelo','id');

         if(empty($data['modelo']))
            $data['modelo'] = $this->equipamento->groupBy('modelo')->lists('modelo','id');                                                            

        return Response::json($data);
    }

    public function carregarHistoricoModal()
    {   
        $equipamento_id = Input::get('equipamento_id');

        $equipamento = $this->equipamento->where('id', $equipamento_id)->first();   
        
        $usuarios = $this->usuario->lists('nome','id');

        $calibracao = $this->calibracao->where('equipamento_id', $equipamento_id)->get();

        foreach ($calibracao as $key => $value)
        {
            if(isset($value->data_calibracao))
                $calibracao[$key]['data_calib'] = $value->data_calibracao->format('d/m/Y');
            else
                $calibracao[$key]['data_calib'] = '-';

            if(isset($value->primeiro_uso))
                $calibracao[$key]['uso'] = $value->primeiro_uso->format('d/m/Y');
            else
                $calibracao[$key]['uso'] = '-';

            if(isset($value->vencimento))
                $calibracao[$key]['venc'] = $value->vencimento->format('d/m/Y');
            else
                $calibracao[$key]['venc'] = '-';
        }

        if(!empty($calibracao))
            $success = true;
        else
            $success = false;

        return Response::json(['success' => $success,
                               'equipamento' => $equipamento,
                               'calibracao' => $calibracao        
                            ]);
    }

    public function updateUso()
    {
        $input = array_except(Input::all(), array('_method', '_token'));

        $primeiro_uso = clone(Carbon::createFromFormat('Y-m-d', $input['primeiro_uso']));
        
        $input['vencimento'] = $primeiro_uso->addMonths($this->equipamento->where('id', $input['cod_equipamento'])->pluck('frequencia'));
        
        $input['vencimento'] = $input['vencimento']->format('Y-m-d');

        $calibracao = $this->calibracao->find($input['cod_calibracao']);
        unset($input['cod_calibracao'], $input['cod_equipamento']);
        
        $calibracao->update($input);

        return redirect()->route('calibracao.index');
    }
}
