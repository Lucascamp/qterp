<?php namespace App\Http\Controllers\rh;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Funcao;
use App\Permissaousuario;
use App\Equipamento;
use App\Centrocusto;
use App\Localizacaoequipamento;
use App\Funcionario;
use App\User;
use Carbon\Carbon;
use App\Http\Requests\CreateEquipamento;
use App\Http\Requests\CreateLocalizacao;
use Excel;

class FuncaoController extends Controller 
{
    public function __construct(
                                Funcao $funcao,
                                Funcionario $funcionario,
                                User $usuario,
                                Centrocusto $centrocusto
                                )
    {
        $this->middleware('auth');
        $this->funcao = $funcao;
        $this->centrocusto = $centrocusto;
        $this->funcionario = $funcionario;
        $this->usuario = $usuario;
    }

    public function index()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%rh%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['usuario'] = Auth::user();

        $data['funcoes'] = $this->funcao->get();

        return view('rh.funcao.index')->with($data);
    }


    // public function index()
    // {
    //     $id_user = Auth::user()->id;

    //     $data['modulos'] = Permissaousuario::Modulo($id_user);

    //     $id_modulo = DB::table('modulos')
    //                       ->where('rota','like','%patrimonio%')
    //                       ->pluck('id');

    //     $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
    //     $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

    //     $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
    //     $permissoes_opcoes = (array) $permissoes_opcoes;
    //     $data['permissoes_opcoes'] = $permissoes_opcoes;

    //     $data['usuario'] = Auth::user();

    //     $descricao   = Input::get('filter_descricao');
    //     $patrimonio = Input::get('filter_patrimonio');
    //     $serial = Input::get('filter_serial');
    //     $modelo = Input::get('filter_modelo');
    //     $local_equip = Input::get('filter_local');

    //     $filter_descricao = $this->equipamento->orderBy('descricao')->groupBy('descricao')->lists('descricao', 'descricao');

    //     $locais = $this->localizacao->groupBy('centrocusto_id')->lists('centrocusto_id');

    //     $filter_local = $this->centrocusto
    //                              ->whereNull('deleted_at')
    //                              ->whereIn('id', $locais)
    //                              ->lists('CTD_DESC01','id');   

    //     if($descricao)
    //         $filter_patrimonio = $this->equipamento->where('descricao', $descricao)->orderBy('patrimonio')->groupBy('patrimonio')->lists('patrimonio', 'patrimonio');
        
    //     else
    //         $filter_patrimonio = $this->equipamento->orderBy('patrimonio')->groupBy('patrimonio')->lists('patrimonio', 'patrimonio');
        
    //     if($descricao)
    //         $filter_serial = $this->equipamento->where('descricao', $descricao)->orderBy('serial')->groupBy('serial')->lists('serial', 'serial');
       
    //     else
    //         $filter_serial = $this->equipamento->orderBy('serial')->groupBy('serial')->lists('serial', 'serial');
       
    //     if($descricao)
    //         $filter_modelo = $this->equipamento->where('descricao', $descricao)->orderBy('modelo')->groupBy('modelo')->lists('modelo', 'modelo');
       
    //     else
    //         $filter_modelo = $this->equipamento->orderBy('modelo')->groupBy('modelo')->lists('modelo', 'modelo');

    //     if($local_equip)
    //     {
    //         $equips = $this->equipamento->filterLocalizacao($local_equip);
            
    //         $equipamentosQuery = $this->equipamento->whereIn('id', $equips)->orderby('descricao', 'ASC')->whereNull('deleted_at')->orderby('aplicacao', 'ASC');
    //     }
    //     else
    //         $equipamentosQuery = $this->equipamento->orderby('descricao', 'ASC')->orderby('aplicacao', 'ASC')->whereNull('deleted_at');

    //     if ($descricao)
    //     {               
    //         $equipamentosQuery->where('descricao', $descricao);
    //     } 

    //     if ($patrimonio)
    //     {               
    //         $equipamentosQuery->where('patrimonio', $patrimonio);
    //     }

    //     if ($serial)
    //     {               
    //         $equipamentosQuery->where('serial', $serial);
    //     }

    //     if ($modelo)
    //     {               
    //         $equipamentosQuery->where('modelo', $modelo);
    //     }

    //     $data['total_equips'] = count($equipamentosQuery->get());

    //     $data['equipamentos'] = $equipamentosQuery->paginate(100);

    //     $data['centro_custo'] = $this->centrocusto->lists('CTD_DESC01', 'id');

    //     foreach ($data['equipamentos'] as $equipamento) 
    //     {
    //         $localizacao[$equipamento->id] = $this->localizacao
    //                                                 ->where('equipamento_id', $equipamento->id)
    //                                                 ->orderBy('data_movimentacao', 'DESC')
    //                                                 ->orderBy('updated_at', 'DESC')
    //                                                 ->whereNull('deleted_at')
    //                                                 ->first();
    //     } 

    //     if(isset($localizacao))
    //         $data['localizacao'] = $localizacao;

    //     $data['funcionarios'] = ['' => 'Selecione o funcionário'] + $this->funcionario
    //                     ->whereNull('deleted_at')
    //                     ->lists('nome','id'); 

    //     $data['usuarios'] = ['' => 'Selecione o usuário'] + $this->usuario
    //                     ->whereNull('deleted_at')
    //                     ->lists('nome','id');

    //     $data['centro_custo_novo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');                
    //     $data['patrimonio'] = $patrimonio;
    //     $data['descricao'] = $descricao;
    //     $data['serial'] = $serial;
    //     $data['modelo'] = $modelo;
    //     $data['local_equip'] = $local_equip;
    //     $data['filter_descricao'] = ['' =>'Descrição'] + $filter_descricao;
    //     $data['filter_patrimonio'] = ['' =>'Patrimônio'] + $filter_patrimonio;
    //     $data['filter_serial'] = ['' =>'Serial'] + $filter_serial;
    //     $data['filter_modelo'] = ['' =>'Modelo'] + $filter_modelo;
    //     $data['filter_local'] = ['' =>'Localização'] + $filter_local;

    //     return view('patrimonio.equipamento.index')->with($data);
    // }

    public function cadastrar()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%rh%')
                    ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();

        return view('rh.funcao.cadastrar')->with($data);
    }

    public function salvar() 
    {
        $input = array_except(Input::all(), array('_token'));

        $funcao = $this->funcao->create($input);
        
        return redirect()->route('funcao.index');
    }

    public function editar($id)
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 

        $data['usuario'] = Auth::user();

        $data['funcao'] = $this->funcao->findOrFail($id);

        return view('rh.funcao.editar')->with($data);
    }

    public function atualizar($id) 
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $funcao = $this->funcao->findOrFail($id);

        $funcao->update($input); //($Vequip->all());

        return redirect()->route('funcao.index');
    }

    public function destroy($id)
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $this->funcao->find($input['funcao_id'])->delete();

        return redirect()->route('funcao.index');
    }


    public function perfil($id)
    {
       $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 

        $data['usuario'] = Auth::user();

        $data['funcao'] = $this->funcao->findOrFail($id);

        return view('rh.funcao.perfil')->with($data);
    }

}
