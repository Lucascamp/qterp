<?php namespace App\Http\Controllers\rh;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use App\Permissaousuario;

class RhController extends Controller 
{
    public function __construct()
    {
        $this->middleware('auth');
    }
        
    public function index()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        
        $id_modulo = DB::table('modulos')
                        ->where('rota','like','%rh%')
                        ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();
        
        return view('rh.home')->with($data);
    }
}
