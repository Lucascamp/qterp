<?php namespace App\Http\Controllers\patrimonio;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Permissaousuario;
use App\Centrocusto;
use App\Funcionario;
use App\Carro;
use App\Localizacaocarro;
use App\Segurocarro;
use App\Impostocarro;
use App\Manutencaocarro;
use App\Ecofrota;
use App\User;
use App\Unidade;
use Carbon\Carbon;

class FrotaController extends Controller 
{
        public function __construct(
                                    Funcionario $funcionario,
                                    User $usuario,
                                    Centrocusto $centrocusto,
                                    Carro $carro,
                                    Unidade $unidade,
                                    Localizacaocarro $localizacao,
                                    Segurocarro $seguro,
                                    Impostocarro $imposto,
                                    Manutencaocarro $manutencao,
                                    Ecofrota $ecofrota
                                    )
        {
            $this->middleware('auth');
            $this->centrocusto = $centrocusto;
            $this->funcionario = $funcionario;
            $this->usuario = $usuario;
            $this->carro = $carro;
            $this->unidade = $unidade;
            $this->localizacao = $localizacao;
            $this->seguro = $seguro;
            $this->imposto = $imposto;
            $this->manutencao = $manutencao;
            $this->ecofrota = $ecofrota;
        }

    public function index()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

         $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();

        $data['carros'] = $this->carro->with('seguro','manutencao','imposto','ecofrota','localizacao')->get();
        $data['centro_custo'] = ['' => 'Selecione'] + $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');
        $data['unidades'] = ['' => 'Selecione'] + $this->unidade->lists('unidade','unidade');
        
        return view('patrimonio.frota.index')->with($data); 
    }

    public function cadastrar()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%patrimonio%')
                    ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();
        $data['centro_custo'] = ['' => 'Selecione'] + $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');
        $data['unidades'] = ['' => 'Selecione'] + $this->unidade->lists('unidade','unidade');

        return view('patrimonio.frota.cadastrar')->with($data);
    }

    public function salvar() 
    {
        $input = array_except(Input::all(), array('_token'));

        if(isset($input['alugado']))
            $input_unset = ['data_compra','valor_compra'];
        else
            $input_unset = ['data_aluguel','prazo_contrato','termino_contrato','valor_locacao','multa_recisao',
                            'data_reajuste','tipo_reajuste','mes_reajuste','ano_reajuste'];

        foreach ($input_unset as $unset)
        {
            unset($input[$unset]);
        }   

        DB::beginTransaction();

        try 
        {
            $carro = $this->carro->create($input);

            $input['carro_id'] = $carro->id;

            $this->localizacao->create($input);

            $this->seguro->create($input);

            $this->ecofrota->create($input);

            foreach ($input['tipo_imposto'] as $key => $value) 
            {   
                $imposto_input = [
                                    'carro_id' => $input['carro_id'],
                                    'tipo_imposto' => $input['tipo_imposto'][$key],
                                    'valor_imposto' => $input['valor_imposto'][$key],
                                    'data_pagamento_imposto' => $input['data_pagamento_imposto'][$key],
                                    'restricao' => $input['restricao'][$key],
                                    'tipo_restricao' => $input['tipo_restricao'][$key],
                                    'created_by' => $input['created_by']
                                ];

                $this->imposto->create($imposto_input);
            }

            foreach ($input['descricao_manutencao'] as $key => $value) 
            {   
                $manutencao_input = [
                                        'carro_id' => $input['carro_id'],
                                        'descricao_manutencao' => $input['descricao_manutencao'][$key],
                                        'valor_manutencao' => $input['valor_manutencao'][$key],
                                        'data_manutencao' => $input['data_manutencao'][$key],
                                        'created_by' => $input['created_by']
                                    ];

                $this->manutencao->create($manutencao_input);
            }

            DB::commit();

        } catch (\Exception $e) { 
            DB::rollback(); 
            return redirect()->route('frota.cadastrar');
        }

        return redirect()->route('frota.index');
    }

    public function editar($id)
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%patrimonio%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 

        $data['usuario'] = Auth::user();

        $data['carro'] = $this->carro->with('seguro','manutencao','imposto','ecofrota','localizacao')->findOrFail($id);
        $ultimo_local = end($data['carro']->localizacao);
        $data['localizacao'] = reset($ultimo_local);

        $data['centro_custo'] = ['' => 'Selecione'] + $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');
        $data['unidades'] = ['' => 'Selecione'] + $this->unidade->lists('unidade','unidade');
        
        return view('patrimonio.frota.editar')->with($data);
    }

    public function atualizar($id) 
    {
        $input = array_except(Input::all(), array('_method', '_token'));

        $input_carro = Input::only(['placa','marca','modelo','ano','cor','chassi','km','data_compra','valor_compra',
                              'rastreador','renavan','responsavel','proprietario','alugado','data_aluguel',
                              'prazo_contrato','termino_contrato','valor_locacao','multa_recisao','data_reajuste',
                              'tipo_reajuste','mes_reajuste','ano_reajuste']);

        if(isset($input_carro['alugado']))
            $input_null = ['data_compra','valor_compra'];
        else
            $input_null = ['data_aluguel','prazo_contrato','termino_contrato','valor_locacao','multa_recisao',
                            'data_reajuste','tipo_reajuste','mes_reajuste','ano_reajuste'];

        foreach ($input_null as $null)
        {
            $input_carro[$null] = NULL;
        }   

        $carro = $this->carro->findOrFail($id);

        $carro->update($input_carro);

        $this->localizacao->where('id', $input['localizacao_id'])
                              ->update([
                                        'data' => $input['data'],
                                        'unidade' => $input['unidade'],
                                        'centrocusto_id' => $input['centrocusto_id'],
                                      ]);

        $this->seguro->where('carro_id', $carro->id)
                     ->update([
                                'seguradora' => $input['seguradora'],
                                'corretor' => $input['corretor'],
                                'apolice' => $input['apolice'],
                                'valor_seguro' => $input['valor_seguro'],
                                'data_inicial' => $input['data_inicial'],
                                'data_final' => $input['data_final'],
                                'vistoria' => $input['vistoria'],
                             ]); 

        $this->ecofrota->where('carro_id', $carro->id)
                       ->update([
                                'numero' => $input['numero'],
                                'tipo' => $input['tipo'],
                                'recarga' => $input['recarga'],
                               ]);    

        foreach ($input['tipo_imposto_edit'] as $key => $value) 
        {   
            $imposto_input =[
                            'carro_id' => $carro->id,
                            'tipo_imposto' => reset($input['tipo_imposto_edit'][$key]),
                            'valor_imposto' => reset($input['valor_imposto_edit'][$key]),
                            'data_pagamento_imposto' => reset($input['data_pagamento_imposto_edit'][$key]),
                            'restricao' => reset($input['restricao_edit'][$key]),
                            'tipo_restricao' => reset($input['tipo_restricao_edit'][$key]),
                            ];

            $this->imposto->where('id', $key)->update($imposto_input);
        }       

        if(isset($input['tipo_imposto']))
        {
            foreach ($input['tipo_imposto'] as $key => $value) 
            {   
                $imposto_input =[
                                'carro_id' => $carro->id,
                                'tipo_imposto' => $input['tipo_imposto'][$key],
                                'valor_imposto' => $input['valor_imposto'][$key],
                                'data_pagamento_imposto' => $input['data_pagamento_imposto'][$key],
                                'restricao' => $input['restricao'][$key],
                                'tipo_restricao' => $input['tipo_restricao'][$key],
                                ];

                $this->imposto->create($imposto_input);
            }
        }

        foreach ($input['descricao_manutencao_edit'] as $key => $value) 
        {   
            $manutencao_input = [
                                'carro_id' => $carro->id,
                                'descricao_manutencao' => reset($input['descricao_manutencao_edit'][$key]),
                                'valor_manutencao' => reset($input['valor_manutencao_edit'][$key]),
                                'data_manutencao' => reset($input['data_manutencao_edit'][$key]),
                                ];

            $this->manutencao->where('id', $key)->update($manutencao_input);
        }

        if(isset($input['descricao_manutencao']))
        {
            foreach ($input['descricao_manutencao'] as $key => $value) 
            {   
                $manutencao_input = [
                'carro_id' => $carro->id,
                'descricao_manutencao' => $input['descricao_manutencao'][$key],
                'valor_manutencao' => $input['valor_manutencao'][$key],
                'data_manutencao' => $input['data_manutencao'][$key],
                ];

                $this->manutencao->create($manutencao_input);
            }
        }

        return redirect()->route('frota.index');
    }

    public function infoFrota()
    {   
        $carro = $this->carro->with('seguro','manutencao','imposto','ecofrota','localizacao')->findOrFail(Input::get('id'));

        return Response::json([
                               'success' => true,
                               'carro' => $carro
                              ]);
    }

    public function movimentar()
    {
        $input = array_except(Input::all(), array('_token'));

        $localizacao = [
                        'carro_id' => $input['carro_id'],    
                        'centrocusto_id' => $input['centrocusto_id'],
                        'unidade' => $input['unidade'],
                        'data' =>  $input['data']
                        ];

        $this->localizacao->create($localizacao);     

        return redirect()->route('frota.index');
    }

    public function destroy()
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $this->carro->find($input['frota_id'])->delete();

        return redirect()->route('frota.index');
    }
}
