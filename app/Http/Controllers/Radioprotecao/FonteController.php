<?php namespace App\Http\Controllers\radioprotecao;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use App\Permissaousuario;
use App\Centrocusto;
use App\Funcionario;
use App\Fonte;
use App\Localizacaofonte;
use App\User;
use App\Unidade;
use Carbon\Carbon;
use App\Http\Requests\CreateFonte;
use App\Http\Requests\CreateLocalizacaoFonte;

class FonteController extends Controller 
{
        public function __construct(
                                    Fonte $fonte,
                                    User $usuario,
                                    Localizacaofonte $localizacao,
                                    Centrocusto $centrocusto,
                                    Unidade $unidade
                                    )
        {
                $this->middleware('auth');
                $this->fonte = $fonte;
                $this->localizacao = $localizacao;
                $this->usuario = $usuario;
                $this->centrocusto = $centrocusto; 
                $this->unidade = $unidade; 
        }

    public function index()
    {
        $id_user = Auth::user()->id;
        $data['id_user'] = $id_user;
        $data['unidades_movi'] = \DB::table('unidade')->lists('unidade','id');
        $data['funcionarios'] = \DB::table('funcionarios')
                        ->whereNull('deleted_at')
                        ->lists('nome','id'); 

        $data['usuarios'] = ['' => 'Selecione o usuário'] + $this->usuario
                        ->whereNull('deleted_at')
                        ->lists('nome','id');

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%radioprotecao%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['usuario'] = Auth::user();

        $isotopo = Input::get('filter_isotopo');
        $modelo  = Input::get('filter_modelo');
        $unidade = Input::get('filter_unidade');
        
        $fonteQuery = $this->fonte->whereNull('deleted_at')->orderby('irradiador', 'ASC');

        $fontes = $this->fonte->all();

        $now  = Carbon::now();
        
        foreach ($fontes as $fonte) 
        {
            if($fonte->data_inicial != null)
            {
                $datainicio = Carbon::createFromFormat('Y-m-d H:i:s', $fonte->data_inicial);
                
                $totaldias = $datainicio->diffInDays($now)+1;
                
                $carga_inicial = floatval($fonte->atividade_inicial);

                $meiavida= floatval($fonte->meia_vida);

                $carga  = number_format($carga_inicial, 2);
                
                $carga = number_format($carga/pow(2,($totaldias-1)/$meiavida) ,2);

                $cargasAtuais[$fonte->id] = $carga;
            }

            $localizacao[$fonte->id] = $this->localizacao
                                                    ->where('fonte_id', $fonte->id)
                                                    ->orderBy('data', 'DESC')
                                                    ->first(); 
        }

        if ($isotopo)
        {               
            $fonteQuery->where('isotopo', $isotopo);
        }

        if ($modelo)
        {               
            $fonteQuery->where('modelo', $modelo);
        }

        $array_fonte_local = array();

        if ($unidade)
        {       
            foreach ($localizacao as $local) 
            {
                if(isset($local))
                {
                    if($local->unidade == $unidade)
                        array_push($array_fonte_local, $local->fonte_id);
                }
            }

            $fonteQuery->whereIn('id', $array_fonte_local);
        }

        $data['isotopo'] = $isotopo;
        $data['modelo']  =  $modelo;
        $data['unidade']  = $unidade;

        $data['fontes'] = $fonteQuery->get();
        $data['cargasAtuais'] = $cargasAtuais;
        $data['localizacao'] = $localizacao;
        $data['centro_custo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');
        $data['centro_custo_novo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');
        $data['unidades'] = $this->unidade->lists('unidade','unidade');

        $filter_isotopo = $this->fonte->whereNull('deleted_at')->groupBy('isotopo')->lists('isotopo', 'isotopo'); 
        $data['filter_isotopo'] = ['' =>'Isótopo'] + $filter_isotopo;
        
        $filter_modelo = $this->fonte->whereNull('deleted_at')->groupBy('modelo')->lists('modelo', 'modelo');
        $data['filter_modelo'] = ['' =>'Modelo'] + $filter_modelo;
        
        $data['filter_unidades'] = ['' =>'Unidades'] + $data['unidades'];
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');
        $data['unid'] = \DB::table('unidade')->lists('unidade','id');
        $data['local_irradiador'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('local_atual','id');
        $data['status_irradiador'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('status','id');

        $movimentacao = [];
        foreach ($data['irradiadores'] as $key => $value) {
            $id_historico = \DB::table('equipamento_historico')->where('equipamento_id',$key)->max('id');
            $movimentacao[$key] = \DB::table('equipamento_historico')->where('id', $id_historico)
                ->first();
        }
        $data['movimentacao'] = $movimentacao;

        return view('radioprotecao.fonte.index')->with($data);
    }

    public function cadastrar()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
                
        $id_modulo = DB::table('modulos')
                    ->where('rota','like','%radioprotecao%')
                    ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 
        $data['usuario'] = Auth::user();

        $data['unidades'] = $this->unidade->lists('unidade','unidade');
        $data['centro_custo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');
        $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');
        
        return view('radioprotecao.fonte.cadastrar')->with($data);
    }

    public function salvar(CreateFonte $Vfonte) 
    {
        $input = array_except(Input::all(), array('_token'));

        if(!isset($input['meia_vida']) && !isset($input['atividade_inicial']) && !isset($input['limite_uso']) && !isset($input['limite_troca']))
        {
            if(strpos($input['meia_vida'], ','))
                $input['meia_vida'] = str_replace(',', '.', $input['meia_vida']);

            $input['meia_vida'] = floatval($input['meia_vida']);
            $input['atividade_inicial'] = floatval($input['atividade_inicial']);
            $input['limite_uso'] = floatval($input['limite_uso']);
            $input['limite_troca'] = floatval($input['limite_troca']);
        }

        if(empty($input['data_inicial']))
            unset($input['data_inicial']);

        if(empty($input['data_vistoria']))
            unset($input['data_vistoria']);

        if(isset($input['data_inicial']))
        {
            $uso = $this->fonte->getLimite($input['limite_uso'], $input['atividade_inicial'], $input['meia_vida'], $input['data_inicial']);

            $troca = $this->fonte->getLimite($input['limite_troca'], $input['atividade_inicial'], $input['meia_vida'], $input['data_inicial']);
                
            $input['data_limite_uso'] = $uso;

            $input['data_limite_troca'] = $troca;
        }
            
        $fonte = $this->fonte->create($Vfonte->all());

        // $localizacao = [
        //                 'fonte_id' => $fonte->id,
        //                 'unidade' =>  $input['unidade'],
        //                 'data' =>  $input['data'],
        //                 'observacoes' =>  $input['observacoes']
        //                 ];

        // $this->localizacao->create($localizacao);  

        return redirect()->route('fonte.index');
    }

    public function editar($id)
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%radioprotecao%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo); 

        $data['usuario'] = Auth::user();

        $data['fonte'] = $this->fonte->findOrFail($id);
        $data['localizacao'] = $this->localizacao->where('fonte_id', $id)->orderBy('data', 'DESC')->first();
        $data['unidades'] = $this->unidade->lists('unidade','unidade');
        $data['centro_custo'] = $this->centrocusto->whereNull('deleted_at')->lists('CTD_DESC01', 'id');
         $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');
        return view('radioprotecao.fonte.editar')->with($data);
    }

    public function atualizar($id, CreateFonte $Vfonte) 
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $fonte = $this->fonte->findOrFail($id);

        // $localizacao = $this->localizacao->where('fonte_id', $fonte->id)->orderBy('data', 'DESC')->first();

        // $localizacao_input = [
        //                     'fonte_id' => $fonte->id,
        //                     'unidade' =>  $input['unidade'],
        //                     'data' =>  $input['data'],
        //                     'observacoes' =>  $input['observacoes']
        //                     ];


        $input_fonte = array_except($Vfonte->all(), array('_token', '_method', 'unidade','data','observacoes'));
                             
        if(!isset($input['meia_vida']) && !isset($input['atividade_inicial']) && !isset($input['limite_uso']) && !isset($input['limite_troca']))
        {
            if(strpos($input['meia_vida'], ','))
                $input['meia_vida'] = str_replace(',', '.', $input['meia_vida']);

            $input['meia_vida'] = floatval($input['meia_vida']);
            $input['atividade_inicial'] = floatval($input['atividade_inicial']);
            $input['limite_uso'] = floatval($input['limite_uso']);
            $input['limite_troca'] = floatval($input['limite_troca']);
        }

        if(empty($input['data_inicial']))
            unset($input_fonte['data_inicial']);

        if(empty($input['data_vistoria']))
            unset($input_fonte['data_vistoria']);

        if(isset($input_fonte['data_inicial']))
        {
            $uso = $this->fonte->getLimite($input['limite_uso'], $input['atividade_inicial'], $input['meia_vida'], $input['data_inicial']);

            $troca = $this->fonte->getLimite($input['limite_troca'], $input['atividade_inicial'], $input['meia_vida'], $input['data_inicial']);
                
            $input['data_limite_uso'] = $uso;

            $input['data_limite_troca'] = $troca;
        }

        $fonte->update($input_fonte);

        // if(!isset($localizacao))
        //     $this->localizacao->create($localizacao_input);
        // else        
        //     $this->localizacao->where('id', $localizacao->id)->update($localizacao_input);

        return redirect()->route('fonte.index');
    }

    public function movimentar(CreateLocalizacaoFonte $FLocal)
    {
        $input = array_except($FLocal->all(), array('_token'));

        $this->localizacao->create($input);     

        return redirect()->route('fonte.index');
    }

    public function destroy($id)
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $this->fonte->find($input['fonte_id'])->delete();

        return redirect()->route('fonte.index');
    }

    public function carregarHistoricoModal()
    {   
        $fonte_id = Input::get('fonte_id');

        $fonte = $this->fonte->where('id', $fonte_id)->first();   

        $locais = $this->localizacao->where('fonte_id', $fonte->id)->orderby('data', 'DESC')->get();

        $centrocusto =  $this->centrocusto->lists('CTD_DESC01', 'id');

        $localizacao = array();

        foreach ($locais as $key => $local)
        {
            $localizacao[$key]['data'] = $local->data->format('d/m/Y');
            $localizacao[$key]['unidade'] = $local->unidade;
            $localizacao[$key]['observacoes'] = $local->observacoes;
        }        

        if(!empty($localizacao))
            $success = true;
        else
            $success = false;

        $irradiadores = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');

        return Response::json(['success' => $success,
                               'fonte' => $fonte,
                               'localizacao' => $localizacao,
                               'irradiadores' => $irradiadores,
                            ]);
    }

    public function fonteDados()
    {   
        $fonte_id = Input::get('fonte_id_calc');


        $fonte = $this->fonte->where('id', $fonte_id)->first();

        $fonte['data'] = $fonte->data_inicial->format('d/m/Y');   

        if(!empty($fonte))
            $success = true;
        else
            $success = false;

        return Response::json(['success' => $success,
                               'fonte' => $fonte,
                            ]);
    }

    public function calculo()
    {   
        $fonte_id = Input::get('fonte_id');
        $data = Input::get('data');

        $data = Carbon::createFromFormat('Y-m-d', $data);
        
        $fonte = $this->fonte->where('id', $fonte_id)->first();   
        
        if($fonte['data_inicial'] != null)
        {
            $datainicio = Carbon::createFromFormat('Y-m-d H:i:s', $fonte->data_inicial);

            $totaldias = $datainicio->diffInDays($data)+1;

            $carga_inicial = floatval($fonte->atividade_inicial);

            $meiavida= floatval($fonte->meia_vida);

            $carga  = number_format($carga_inicial, 2);

            $carga = number_format($carga/pow(2,($totaldias-1)/$meiavida) ,2);
        }

        if(!empty($carga))
            $success = true;
        else
            $success = false;

        return Response::json(['success' => $success,
                               'carga' => $carga        
                            ]);
    }

    public function folha()
    {
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%radioprotecao%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $data['usuario'] = Auth::user();

        $isotopo = Input::get('filter_isotopo');
        $modelo  = Input::get('filter_modelo');
        $unidade = Input::get('filter_unidade');

        $fonteQuery = $this->fonte->whereNull('deleted_at')
                                    ->where('status', '<>' ,'DESATIVADO')
                                    ->where('status', '<>' ,'EXCLUIDO')
                                    ->orderby('irradiador', 'ASC');

        $fontes = $this->fonte->all();

        $now  = Carbon::now();
        
        foreach ($fontes as $fonte) 
        {
            if($fonte->data_inicial != null)
            {
                $datainicio = Carbon::createFromFormat('Y-m-d H:i:s', $fonte->data_inicial);
                
                $totaldias = $datainicio->diffInDays($now)+1;
                
                $carga_inicial = floatval($fonte->atividade_inicial);

                $meiavida= floatval($fonte->meia_vida);

                $carga_inicial  = number_format($carga_inicial, 2);
                
                $carga = number_format($carga_inicial/pow(2,($totaldias-1)/$meiavida) ,2);

                $carga30dias = number_format($carga_inicial/pow(2,($totaldias+30-1)/$meiavida) ,2);

                $cargasAtuais[$fonte->id] = $carga;
                $cargasAtuais30dias[$fonte->id] = $carga30dias;
            }

            $localizacao[$fonte->id] = $this->localizacao
                                                    ->where('fonte_id', $fonte->id)
                                                    ->orderBy('data', 'DESC')
                                                    ->first(); 
            $local_irradiador[$fonte->id] = \DB::table('equipamentos')->where('id',$fonte->irradiador)->pluck('local_atual');
            
        }

        uasort($cargasAtuais, array($this->fonte, 'sort')); 

        if ($isotopo)
        {               
            $fonteQuery->where('isotopo', $isotopo);
        }

        if ($modelo)
        {               
            $fonteQuery->where('modelo', $modelo);
        }

        $array_fonte_local = array();

        if ($unidade)
        {       
            foreach ($localizacao as $local) 
            {
                if(isset($local))
                {
                    if($local->unidade == $unidade)
                        array_push($array_fonte_local, $local->fonte_id);
                }
            }

            $fonteQuery->whereIn('id', $array_fonte_local);
        }

        $data['isotopo'] = $isotopo;
        $data['modelo']  =  $modelo;
        $data['unidade']  = $unidade;

        $data['fontes'] = $fonteQuery->get();
        $data['cargasAtuais'] = $cargasAtuais;
        $data['cargasAtuais30dias'] = $cargasAtuais30dias;
        $data['localizacao'] = $localizacao;
        $data['local_irradiador'] = $local_irradiador;
        $data['unidade_fonte'] = $this->unidade->lists('unidade','id');
        $data['unidades'] = $this->unidade->lists('unidade','unidade');
        $data['now'] = Carbon::now(); 
        $data['em30dias'] = Carbon::now()->addDays(30); 
         $data['irradiadores'] = \DB::table('equipamentos')->where('descricao','like','%IRRADIADOR%')
            ->orWhere('descricao','like','%COMANDO CRAWLER%')
            ->orWhere('descricao','like','%AMPOLA RX%')
            ->lists('serial','id');

        

        $filter_isotopo = $this->fonte->whereNull('deleted_at')->groupBy('isotopo')->lists('isotopo', 'isotopo'); 
        $data['filter_isotopo'] = ['' =>'Isótopo'] + $filter_isotopo;
        
        $filter_modelo = $this->fonte->whereNull('deleted_at')->groupBy('modelo')->lists('modelo', 'modelo');
        $data['filter_modelo'] = ['' =>'Modelo'] + $filter_modelo;
        
        $data['filter_unidades'] = ['' =>'Unidades'] + $data['unidades'];

        return view('radioprotecao.fonte.folha')->with($data);
    }
}
