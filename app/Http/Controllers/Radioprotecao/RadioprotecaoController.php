<?php namespace App\Http\Controllers\radioprotecao;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use App\Permissaousuario;

class RadioprotecaoController extends Controller 
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $id_user = Auth::user()->id;
        
        $data['modulos'] = Permissaousuario::Modulo($id_user);
        
        $id_modulo = DB::table('modulos')
                        ->where('rota','like','%radioprotecao%')
                        ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);
        $data['usuario'] = Auth::user();
        
        return view('radioprotecao.home')->with($data);
    }

    public function relatorio_fontes_frente(){
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%radioprotecao%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();
        return view('radioprotecao.fontesporfrente.home')->with($data);
    }

    public function gerar_relatorio(){
        $id_user = Auth::user()->id;

        $data['modulos'] = Permissaousuario::Modulo($id_user);

        $id_modulo = DB::table('modulos')
                          ->where('rota','like','%radioprotecao%')
                          ->pluck('id');

        $data['areas'] = Permissaousuario::Area($id_user,$id_modulo);
        $data['permissoes'] = Permissaousuario::Permissao($id_user,$id_modulo);

        $permissoes_opcoes = DB::table('permissao_opcoes_usuario')->select('permissao_opcoes_id')->where('usuario_id',$id_user)->lists('permissao_opcoes_id');
        $permissoes_opcoes = (array) $permissoes_opcoes;
        $data['permissoes_opcoes'] = $permissoes_opcoes;

        $data['usuario'] = Auth::user();
        return view('radioprotecao.fontesporfrente.gerar')->with($data);
    }

    public function carregar_relatorio(){
        $input    = Input::all();
        $data['success'] = true;
        
        $fontes_irradiadores = \DB::connection('CENTRAL')->table('fontes')->lists('irradiador','id');
        $fontes_numeros = \DB::connection('CENTRAL')->table('fontes')->lists('numero','id');
        $monitores = \DB::connection('CENTRAL')->table('equipamentos')->where('descricao','MONITOR AREA')->lists('serial','id');

        $empresas = \DB::connection('CENTRAL')->table('guias')
        ->join('clientes','clientes.id','=','guias.cliente_id')
        ->select(\DB::raw('clientes.id as cli_id, clientes.nome as cli_nome, clientes.endereco as cli_endereco, '.
            'clientes.municipio as cli_municipio, clientes.estado as cli_estado, guias.proposta_id as gui_proposta_id,'.
            'guias.id as gui_id, guias.data as data, guias.local_ensaio as local'))
        ->where('data','>=',$input['data_ini'])
        ->where('data','<=',$input['data_fim'])
        ->where('local_ensaio','like','CLIENTE')
        ->groupby('clientes.id')
        ->get();


        $qualitec = \DB::connection('CENTRAL')->table('guias')
        ->join('clientes','clientes.id','=','guias.cliente_id')
        ->select(\DB::raw('clientes.id as cli_id, clientes.nome as cli_nome, clientes.endereco as cli_endereco, '.
            'clientes.municipio as cli_municipio, clientes.estado as cli_estado, guias.proposta_id as gui_proposta_id,'.
            'guias.id as gui_id, guias.data as data, guias.local_ensaio as local, guias.unidade as guia_unidade'))
        ->where('data','>=',$input['data_ini'])
        ->where('data','<=',$input['data_fim'])
        ->where('local_ensaio','like','QUALITEC')
        ->groupby('guias.unidade')
        ->get();


        
        

        $data['empresas'] = $empresas;
        $data['qualitec'] = $qualitec;

        $funcionarios = \DB::connection('CENTRAL')->table('funcionarios')->lists('nome','id');

        $responsaveis_empre = [];
        $responsaveis_prestadora = [];
        $fontes_clientes = [];
        $outros_dados_clientes = [];
        $monitores_clientes = [];
        
        foreach ($empresas as $key => $value) {
            $outros_dados = [];
            
            $responsavel_emp = \DB::connection('CENTRAL')->table('projetos')->where('proposta_id',$value->gui_proposta_id)->pluck('projeto_contato');
            $responsaveis_empre[$value->cli_id] = $responsavel_emp;

            $guias_cliente = \DB::connection('CENTRAL')->table('guias')
                ->where('cliente_id',$value->cli_id)
                ->where('data','>=',$input['data_ini'])
                ->where('data','<=',$input['data_fim'])
                ->where('local_ensaio','CLIENTE')
                ->get();

            $dados_fontes = [];
            $aux_fontes = [];
            $aux_resp_pres = [];
            $aux_monitores = [];

            foreach ($guias_cliente as $key => $guia) {
                $aux_fontes[] = $guia->fonte_id;
                $aux_resp_pres[] = \DB::connection('CENTRAL')->table('equipes')->where('guia_id',$guia->id)->pluck('ins');
                if($guia->monitor_id){
                    $aux_monitores[] = $guia->monitor_id;    
                }
            }
            

            if(count($aux_monitores) > 0){

                $aux_monitores = array_unique($aux_monitores);    
                $aux_str_monitores = [];
                foreach ($aux_monitores as $key => $monitor) {
                    $aux_str_monitores[] = $monitores[$monitor];
                }  
                $monitores_clientes[$value->cli_id] = implode(' / ', $aux_str_monitores );
            }


            $aux_resp_pres = array_unique($aux_resp_pres);
            $aux_str_pres = [];
            foreach ($aux_resp_pres as $key => $ins) {
                $aux_str_pres[] =  $funcionarios[$ins];
            }

            $responsaveis_prestadora[$value->cli_id] = implode(' / ',  $aux_str_pres);
           
            $aux_fontes = array_unique($aux_fontes);
            $array_irradiadores = [];
            $array_numeros = [];

            foreach ($aux_fontes as $key => $id_fonte) {
                $array_irradiadores[] = $fontes_irradiadores[$id_fonte];
                $array_numeros[]       = $fontes_numeros[$id_fonte];
            }

            $dados_fontes['irradiador'] = implode(' ', $array_irradiadores);
            $dados_fontes['numero']     = implode(' ', $array_numeros);

            $fontes_clientes[$value->cli_id] = $dados_fontes;

            $outros_dados['data_fim'] = \DB::connection('CENTRAL')->table('guias')->select(\DB::raw('MAX(data) as data'))
                ->where('cliente_id',$value->cli_id)
                ->where('data','>=',$input['data_ini'])
                ->where('data','<=',$input['data_fim'])
                ->pluck('data');
            $outros_dados['data_ini'] = \DB::connection('CENTRAL')->table('guias')->select(\DB::raw('MIN(data) as data'))
                ->where('cliente_id',$value->cli_id)
                ->where('data','>=',$input['data_ini'])
                ->where('data','<=',$input['data_fim'])
                ->pluck('data');

            $array_periodo = [];
            foreach ($guias_cliente as $key => $guia) {
                $entrada = substr($guia->entrada, 11,2);
                $entrada = (int) $entrada;
                
                if($entrada >= 6 && $entrada <= 17){
                    $array_periodo[] = 'D';
                }
                if($entrada >= 0 && $entrada < 6){
                    $array_periodo[] = 'N';
                }
                if($entrada > 17){
                    $array_periodo[] = 'N';
                }
            }
            
            $array_periodo = array_unique($array_periodo);
            $outros_dados['periodo'] = implode('/', $array_periodo);         
            if($outros_dados['periodo'] == 'N/D'){
                $outros_dados['periodo'] = 'D/N';
            }
            $outros_dados_clientes[$value->cli_id] = $outros_dados;



        }
        $data['fontes_clientes'] = $fontes_clientes;
        $data['responsaveis_empresa'] = $responsaveis_empre;
        $data['outros_dados_clientes'] = $outros_dados_clientes;
        $data['responsaveis_prestadora'] = $responsaveis_prestadora;
        $data['monitores_clientes'] = $monitores_clientes;



        $responsaveis_prestadora_qt = [];
        $fontes_qualitec = [];
        $outros_dados_qualtiec = [];
        $monitores_qualitec = [];
        
        foreach ($qualitec as $key => $value) {
            $outros_dados = [];

            $guias_cliente = \DB::connection('CENTRAL')->table('guias')
                ->where('unidade',$value->guia_unidade)
                ->where('data','>=',$input['data_ini'])
                ->where('data','<=',$input['data_fim'])
                ->where('local_ensaio','QUALITEC')
                ->get();

            $dados_fontes = [];
            $aux_fontes = [];
            $aux_resp_pres = [];
            $aux_monitores = [];

            foreach ($guias_cliente as $key => $guia) {
                $aux_fontes[] = $guia->fonte_id;
                $aux_resp_pres[] = \DB::connection('CENTRAL')->table('equipes')->where('guia_id',$guia->id)->where('unidade',$guia->unidade)->pluck('ins');
                if($guia->monitor_id){
                    $aux_monitores[] = $guia->monitor_id;    
                }
            }

            if(count($aux_monitores) > 0){

                $aux_monitores = array_unique($aux_monitores);    
                $aux_str_monitores = [];
                foreach ($aux_monitores as $key => $monitor) {
                    $aux_str_monitores[] = $monitores[$monitor];
                }  
                $monitores_qualitec[$value->cli_id] = implode(' / ', $aux_str_monitores );
            }

            $aux_resp_pres = array_unique($aux_resp_pres);

            $aux_str = [];
            foreach ($aux_resp_pres as $key => $ins) {
                $aux_str[] =  $funcionarios[$ins];
            }

            $responsaveis_prestadora_qt[$value->cli_id] = implode(' / ',  $aux_str);
           


            $aux_fontes = array_unique($aux_fontes);

            $array_irradiadores = [];
            $array_numeros = [];
            foreach ($aux_fontes as $key => $id_fonte) {
                $array_irradiadores[] = $fontes_irradiadores[$id_fonte];
                $array_numeros[]       = $fontes_numeros[$id_fonte];
            }

            $dados_fontes['irradiador'] = implode(' ', $array_irradiadores);
            $dados_fontes['numero']     = implode(' ', $array_numeros);

            $fontes_qualitec[$value->cli_id] = $dados_fontes;

            $outros_dados['data_fim'] = \DB::connection('CENTRAL')->table('guias')->select(\DB::raw('MAX(data) as data'))
                ->where('cliente_id',$value->cli_id)
                ->where('data','>=',$input['data_ini'])
                ->where('data','<=',$input['data_fim'])
                ->pluck('data');
            $outros_dados['data_ini'] = \DB::connection('CENTRAL')->table('guias')->select(\DB::raw('MIN(data) as data'))
                ->where('cliente_id',$value->cli_id)
                ->where('data','>=',$input['data_ini'])
                ->where('data','<=',$input['data_fim'])
                ->pluck('data');

            $array_periodo = [];
            foreach ($guias_cliente as $key => $guia) {
                $entrada = substr($guia->entrada, 11,2);
                $entrada = (int) $entrada;
                
                if($entrada >= 6 && $entrada <= 17){
                    $array_periodo[] = 'D';
                }
                if($entrada >= 0 && $entrada < 6){
                    $array_periodo[] = 'N';
                }
                if($entrada > 17){
                    $array_periodo[] = 'N';
                }
            }
            $array_periodo = array_unique($array_periodo);
            $outros_dados['periodo'] = implode('/', $array_periodo);
            if($outros_dados['periodo'] == 'N/D'){
                $outros_dados['periodo'] = 'D/N';
            }

            $outros_dados_qualitec[$value->cli_id] = $outros_dados; 
        }
        $data['fontes_qualitec'] = $fontes_qualitec;
        $data['outros_dados_qualitec'] = $outros_dados_qualitec;
        $data['responsaveis_prestadora_qt'] = $responsaveis_prestadora_qt;
        $data['monitores_qualitec'] = $monitores_qualitec;
        
        $data['msg'] = 'testestes';
        return Response::json($data);    
                
    }
}
