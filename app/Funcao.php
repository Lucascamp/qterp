<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcao extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'funcao';

	protected $fillable = array('funcao', 'macroprocesso', 'dependencia', 'funcao_operativa', 'funcao_sms', 'responsabilidade_operativa',
								'responsabilidade_sms',	'risco_fisico', 'risco_quimico', 'risco_biologico', 'risco_ergonomico',
								'risco_acidente', 'epi', 'medidas_preventivas','created_by', 'updated_by');

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);
}
