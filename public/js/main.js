var $ = jQuery.noConflict();
//main function
jQuery(function($){ 

  /*Masks*/
  $(".mask-date").mask("99/99/9999");
  $(".mask-time").mask("99:99");
  $(".mask-money").mask("9999,99");
  $(".mask-time2").mask("99:99:99");
  $(".mask-codproposta").mask("aa aa 999a/9999");

  $('[data-tooltip="tooltip"]').tooltip();

  /*Select Funcionario*/    
  $('.chosen-select').chosen({
    placeholder_text_single: "Selecione",
    no_results_text: "Nenhum resultado encontrado!",
    width:"100%",
    allow_single_deselect: true
  }).addClass('form-control');

  

});//end main
