$(document).ready(function() {
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	// $('.flashError').fadeOut(0);
 //  	$('.flashSuccess').fadeOut(0);  

 $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

 cauculaKM();
	var token     = $("#token").val();
	var id_guia   = $("#id_guia").val();
	var idLay     = 'null';
	var normassistema = new Array();


 	$('#tabelaListaGuias').DataTable({responsive: true,
                "iDisplayLength": 100,
                "dom": 'T<"clear">lfrtip',
                "aoColumnDefs": [
		          { 'bSortable': false, 'aTargets': [ 4 ] }
		       	],
                "tableTools": {
                    "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [ ],
                }});

 $(".radio").click(function(event) {
 	if($(this).val() == 'pol'){
 		$("#cabDiCo").text('Diâmetro / Comp. (pol)');
 		$('.diametro').attr('placeholder','EX.: 1 1/2');
 	}
 	if($(this).val() == 'mili'){
 		$("#cabDiCo").text('Diâmetro / Comp. (mm)');
 		$('.diametro').attr('placeholder','EX.: 60.3');
 	}
 });

 	$("#unidade").change(function(event) {
 		var uni = $(this).val();
 		$.post('/guia/getfontes', {
			_token  : token,
			uni: uni,
			}, function(data) {  
				if(data.success){
	            	items = "<option value=''>Selecione a Fonte</option>"; 

		            $.each(data.fontes,function(index,item){
		            	items+="<option value='"+index+"'>"+item+"</option>";
		            });

		      		$("#fonte").html(items); 
		       		$('#fonte').trigger("chosen:updated");
		       	}
	    });	
 	});
	
	$("#bt_ler_arquivo").hide();
	carregaClienteLay();
	var idcli = $("#cliente").val();
	if(idcli){
		carregacliente(idcli);
	}
	var idfon = $("#fonte").val();
	if(idfon){
		carregafonte(idfon);
	}

	$(".mask-date").mask("99/99/9999");
  	$(".mask-time").mask("99:99");
 	$(".mask-money").mask("9999,99");
 	$(".mask-time2").mask("99:99:99");
 	$(".mask-time3").mask("99:99:99");
 	$(".mask-datetime").mask("99-99-9999 99:99");
  	
  	$(".mask-placa").mask("aaa 9999");

	$("#cliente").change(function(event) {
		var id = $(this).val();
		carregacliente(id,true);
	});	

	$('#fonte').change(function(){
    	var id = this.value;
    	carregafonte(id); 
 	});
	$("#bt_refresh").click(function(event) {
		location.href="/ensaio/guia";
	});
 	$("#bt_filtro").click(function(event) {
 		var cliente = $("#filtrocliente").val();
	 	$('#listaPropostasH').empty();
	 	$.post('/guia/getguias', {
	    	_token  : token,
	    	cliente : cliente
	    }, function(data) {    
	    	if(data.success){
	    		var table = '';
	    		$.each(data.guias,function(index,item){
		    		table +='<tr>';
		    		table += '<td class="tblabel">'+item.id+'</td>';
		    		table += '<td class="tblabel">'+item.unidade+'</td>';
		    		table += '<td class="tblabel">'+data.cliente.nome+'</td>';
		    		table += '<td class="tblabel">'+item.data+'</td>';
		    		table += '<td class="tblabel">';

		    		table += '<a href = "/guia/'+item.id+'/editar" data-toggle="tooltip" title="Editar" data-placement="top" class="legenda btn btn-info dim"><i class="fa fa-pencil"></i></a>';
		    		table += '<a href = "/guia/'+item.id+'/visualizar" data-toggle="tooltip" title="Visualizar" data-placement="top" class="legenda btn btn-success dim"><i class="fa fa-eye"></i></a>';

		    		table += '</td>';
		    		table +='</tr>';

		  		});
                 $('#listaPropostasH').append(table);	                          
	    	}
	  		
	  		
	    });

 	});

	// 	$.post('/guia/getnormas', {
	//     	_token  : token,
	//     }, function(data) {    
	//     	if(data.success){
	//     		normassistema = data.normas;                      
	//     	}
	//     });

	
	
	
	$('#quantitens').bind('keydown', function(e) {
		var normassistema;
	    if ( e.keyCode === 9 ) 
	    { // 13 is enter key
	    	$.post('/guia/getnormas', {
		    	_token  : token,
		    }, function(data) {    
		    	if(data.success){
		    		normassistema = data.normas;                      
		    		
	    			
	    			var itenscadastrados =  $("#itenscadastrados").val();
	        		var itens =$('#quantitens').val();
	        		
	        		if(itenscadastrados > 0)
	        		{
	        			var itens = Number(+itens + +itenscadastrados);
	        			var inicio = itenscadastrados;
	        		}else{
	        			inicio = 0;
	        		}
	        		// $('.flashError').fadeOut(500);
	        		// $('.flashSuccess').fadeOut(500);
	        		$("#tableprogs").empty();	

	        		var table ='';

			        for (var i = inicio; i < itens; i++) 
			        {
				        table += '<tr id="linha'+Number(+i+1)+'">';
				        table += '<input type="hidden" id="item'+Number(+i+1)+'" class="item" value="'+Number(+i+1)+'" name="item[]">';
				        table += '<td><select class="tipo" id="tipo'+Number(+i+1)+'"><option value="0">Tubular</option><option value="1">Plano</option></select></td>';
		                table += '<td>'+Number(+i+1)+'</td>';
				        table += '<td><input size="17" id="identificacao'+Number(+i+1)+'" class="identificacao" name="identificacao[] type="text" class="identificacao" /></td>';
				       	table += '<td><input size="3" id="spool'+Number(+i+1)+'" class="spool" name="spool[] type="text" class="spool" /></td>';
				        table += '<td><input size="10" id="obra'+Number(+i+1)+'" class="obra" name="obra[] type="text" class="obra" /></td>';
				        table += '<td><input size="5" id="junta'+Number(+i+1)+'" class="junta" name="junta[] type="text" class="junta" /></td>';
				        table += '<td><input size="3" id="posicao_soldagem'+Number(+i+1)+'" class="posicao_soldagem" name="posicao_soldagem[] type="text" class="posicao_soldagem" /></td>';
				        // table += '<td><input size="5" id="reforco'+Number(+i+1)+'" class="reforco" name="reforco[] type="text" class="reforco" /></td>';
				        table += '<td><input size="5" id="metal_base'+Number(+i+1)+'" class="metal_base" name="metal_base[] type="text" class="metal_base" /></td>';
				        table += '<td><input size="8" id="diametro'+Number(+i+1)+'" class="diametro" name="diametro[] type="text" placeholder="Ex.: 60.3"/></td>';
				        table += '<td><input size="5" id="espessura'+Number(+i+1)+'" class="espessura" name="espessura[] type="text" class="espessura" /></td>';
				        table += '<td><input size="5" id="chanfro'+Number(+i+1)+'" class="chanfro" name="chanfro[] type="text" class="chanfro" /></td>';
				        table += '<td><input size="5" id="sinete_raiz'+Number(+i+1)+'" class="sinete_raiz" name="sinete_raiz[] type="text" class="sinete_raiz" /></td>';
				        table += '<td><input size="5" id="processo_raiz'+Number(+i+1)+'" class="processo_raiz" name="processo_raiz[] type="text" class="processo_raiz" /></td>';
				        table += '<td><input size="5" id="sinete_acabamento'+Number(+i+1)+'" class="sinete_acabamento" name="sinete_acabamento[] type="text" class="sinete_acabamento" /></td>';
				        table += '<td><input size="5" id="processo_acabamento'+Number(+i+1)+'" class="processo_acabamento" name="processo_acabamento[] type="text" class="processo_acabamento" /></td>';	
				        table += '<td><select class="chosen-select norma" id="norma'+Number(+i+1)+'" multiple >';
				        	$.each(normassistema, function(index, value){
					           table += '<option value="'+index+'">'+value+'</option>';
					        });
						table += '</select></td>';		        	
				        // table += '<td><input size="15" id="norma'+Number(+i+1)+'" class="norma" name="norma[] type="text"/></td>';	
				        // table += '<td><input size="10" id="aplicacao'+Number(+i+1)+'" class="aplicacao" name="aplicacao[] type="text" class="aplicacao" /></td>';	
				        table += '<td><input size="15" id="criterio_aceite'+Number(+i+1)+'" class="criterio_aceite" name="criterio_aceite[] type="text" class="criterio_aceite" /></td>';	
				        table += '<td><input size="5" id="nivel_inspecao'+Number(+i+1)+'" class="nivel_inspecao" name="nivel_inspecao[] type="text" class="nivel_inspecao" /></td>';	
				        table += '<td><input size="15" id="observacao'+Number(+i+1)+'" class="observacao" name="observacao[] type="text" class="observacao" /></td>';
				        table += '<td id="tecnica'+Number(+i+1)+'" class="tecnica" /></td>';
				       	table += '</tr>';
			        }

			$("#tableprogs").append(table);
			$(".chosen-select").chosen({width: "100%"});
			$(".chosen-select").chosen({max_selected_options: 4});

			$(".tipo").change(function(event) {
		 		var tipo = $(this).val();
		 	});
		 	}
		    });
	 	}
	 	
	});
	
	$(".repetidor").click(function(){
  		var id = $(this).attr('id');
  		$("."+id).val($(this).val()) 
	});
	$(".repetidorNorma").chosen().change(function(){
  		var id = $(this).attr('id');
  		


  		$("."+id).val(0);
  		$(".norma").trigger("chosen:updated"); 
  		$("."+id).val($(this).val());
  		$(".norma").trigger("chosen:updated");
  		

	});

	$('#proposta').change(function(){
    	var id = $("#proposta").val();
	    $.post('/guia/dadosProjeto', {
	    	id: id, _token  : token
	    }, function(data) {    
	    	items = "<option value=''>Selecione o Projeto</option>"; 

	  		$.each(data,function(index,item){
	    		items+="<option value='"+index+"'>"+item+"</option>";
	  		});
	  		$("#projeto").html(items);
	    });
 	});

	$('#btetiqueta').click(function(event) {
	    $(".area_impressao").printArea({
		   	mode       : "iframe",
		   	standard   : "html5",
		   	popTitle   : 'relatorio',
		   	popClose   : false,
		    extraCss   : '/css/impressao_capa.css',
	    	extraHead  : '',
	    	retainAttr : ["id","class","style"],
	        printDelay : 900, // tempo de atraso na impressao
	        printAlert : true,
		    printMsg   : 'Aguarde a impressão'
	    });
    });

 	
	$('#btcapa').click(function(event) {
	    $(".area_impressao").printArea({
		   	mode       : "iframe",
		   	standard   : "html5",
		   	popTitle   : 'relatorio',
		   	popClose   : false,
		    extraCss   : '/css/impressao_capa.css',
	    	extraHead  : '',
	    	retainAttr : ["id","class","style"],
	        printDelay : 900, // tempo de atraso na impressao
	        printAlert : true,
		    printMsg   : 'Aguarde a impressão'
	    });
	});

	$('#btRDO').click(function(event) {
	    $(".area_impressao").printArea({
		   	mode       : "iframe",
		   	standard   : "html5",
		   	popTitle   : 'relatorio',
		   	popClose   : false,
		    extraCss   : '/css/impressao_capa.css',
	    	extraHead  : '',
	    	retainAttr : ["id","class","style"],
	        printDelay : 900, // tempo de atraso na impressao
	        printAlert : true,
		    printMsg   : 'Aguarde a impressão'
	    });
	});

	$('#impriGO').click(function(event) {
	    $(".area_impressao").printArea({
		   	mode       : "iframe",
		   	standard   : "html5",
		   	popTitle   : 'relatorio',
		   	popClose   : false,
		    extraCss   : '/css/impressao_paisagem.css',
	    	extraHead  : '',
	    	retainAttr : ["id","class","style"],
	        printDelay : 900, // tempo de atraso na impressao
	        printAlert : true,
		    printMsg   : 'Aguarde a impressão'
	    });
	});
	$('#impriRelatorio').click(function(event) {
	    $(".area_impressao").printArea({
		   	mode       : "iframe",
		   	standard   : "html5",
		   	popTitle   : 'relatorio',
		   	popClose   : false,
		    extraCss   : '/css/impressao_paisagem.css',
	    	extraHead  : '',
	    	retainAttr : ["id","class","style"],
	        printDelay : 900, // tempo de atraso na impressao
	        printAlert : true,
		    printMsg   : 'Aguarde a impressão'
	    });
	});
	$("#bt_salvar_dados").click(function(event) {
		var idguia = $("#idguia").val();
		var hora_entrada = $("#hora_entrada").val();
		var hora_saida = $("#hora_saida").val();
		var horas_contratadas = $("#horas_contratadas").val();
		
		var arrayfilmes = {};
		var filmes_batidos = {};
		var filmes_perdidos = {};
		var situacao = {};
		var tempofonte = {};
		var tipoex = {};
		var ind = 0;
	    $.each($(".programacao"), function(id , val){

	  		var idprogramacao = $(val).prop('id');
	  		arrayfilmes[ind] = idprogramacao;
	  		filmes_batidos[idprogramacao]  = $("#filmes_batidos"+idprogramacao).val();
	  		filmes_perdidos[idprogramacao] = $("#filmes_perdidos"+idprogramacao).val();
	  		situacao[idprogramacao]        = $("#situacao"+idprogramacao).val();
	  		tempofonte[idprogramacao]      = $("#tempofonte"+idprogramacao).val();
	  		tipoex[idprogramacao]		   = $("#tipoex"+idprogramacao).val();
	  		ind++;
	    });

	    	

		var arrayjuntas ={};
	    $.each($(".junta"), function(id , val){
	    	if($(val).val() <= 0){

	    	}else{
	    		var idjunta = $(val).prop('id').replace('junta','');
	      		var valor = $(val).val();
	        	arrayjuntas[idjunta] = valor;	
	    	}
	    });

	    if(arrayjuntas.length == 0){
	    	var juntas = 0;
	    }else{
	    	var juntas = arrayjuntas;
	    }

		var placa = $("#placa").val();
		var km_inicial = $("#km_inicial").val();
		var km_final = $("#km_final").val();
		var Ocorrencias = $("#Ocorrencias").val();
		var Relatorios = $("#Relatorios").val();
		var Protocolos = $("#Protocolos").val();
		var observacoes = $("#observacoes").val();
		var monitor_id = $("#monitor_id").val();

		var validacao = false;
		console.log(monitor_id);
		if(hora_entrada && hora_saida && horas_contratadas && typeof(monitor_id) != "undefined" && monitor_id !== null && monitor_id !== '0') {
			validacao = true;
		}else{
			var mensagem = '';
			if(!hora_entrada)       mensagem += '-Hora de Entrada é obrigatorio\n';
			if(!hora_saida)         mensagem += '-Hora de Saida é obrigatorio\n';
			if(!horas_contratadas)  mensagem += '-Horas Contratadas é obrigatorio\n';

			if(typeof(monitor_id) != "undefined" && monitor_id !== null && monitor_id !== '0'){
	            
	        }else{
	             mensagem += '-Monitor de Area é obrigatorio\n';
	        }
			alert(mensagem);
		}

		if(validacao){
			$.post('/guia/salvardadosretorno', {
		    	id                : idguia,
		    	_token            : token,
		    	hora_entrada      : hora_entrada,
		    	hora_saida        : hora_saida,
		    	horas_contratadas : horas_contratadas,
		    	filmes            : arrayfilmes,
		    	filmes_batidos    : filmes_batidos,
		    	filmes_perdidos   : filmes_perdidos,
		    	situacao          : situacao,
		    	tempofonte        : tempofonte,
		    	juntas            : juntas,
		    	placa             : placa,
		    	km_inicial        : km_inicial,
		    	km_final          : km_final,
		    	ocorrencias       : Ocorrencias,
				relatorios        : Relatorios,
				protocolos        : Protocolos,
				observacoes       : observacoes,
				tipoex            : tipoex,
				monitor_id        : monitor_id,

		    }, function(data) {   
		    	 

		    	alert(data.mensagem);
				location.href="/guia/"+idguia+"/visualizar";	
		    });	
		}
	    
	});

	$("#km_final").change(function(event) {
		cauculaKM();
	});
	$("#km_inicial").change(function(event) {
		cauculaKM();
	});
	$.each($(".programacao"), function(id , val){
		var idprogramacao = $(val).prop('id');
		$('#filmes_batidos'+idprogramacao).change(function(event) {
			cauculafilmeperdido(idprogramacao);
		});
    });

	$(".bt_excluir_layout").click(function(event) {
		id = $(this).prop('id');
		$.post('/guia/excluirlayout', {
	    	id              : id,
	    	_token          : token,
	    }, function(data) {    
	    	alert(data.mensagem);
			location.href="/guia/cadastrolayout";	
	    });
	});

	function cauculaKM(){
		var ini = $("#km_inicial").val();
		var fim = $("#km_final").val();
		var res = fim-ini;
		$("#km_percorrido").val(res);
	}
	function cauculafilmeperdido(id){
		var q 	= $("#qfil"+id).val();
		var fb = $("#filmes_batidos"+id).val();
		var res = 0;
		if(fb < q){
			$("#filmes_batidos"+id).parent().addClass('has-error');
		}else{
			$("#filmes_batidos"+id).parent().removeClass('has-error');
			res = fb-q;
		}
		
		$("#filmes_perdidos"+id).val(res);
	}

	$(".situacao").change(function(event) {
		var id = $(this).prop('id').replace('situacao','');
		if($(this).val() == 'N Realizada'){
			$("#filmes_batidos"+id).val(0);
			$("#tempofonte"+id).val('00:00:00');
		}
	});
	

	$("#bt_salvar_resultados").click(function(event) {
		var validacao = true;
		var idguia = $("#idguia").val();
		var arraydesc = {};
	    $.each($(".programacao"), function(id , val){
	  		var idprogramacao = $(val).prop('id');
	  		var descontinuidade = $("#descontinuidade"+idprogramacao).val();
	  		

	  		if(descontinuidade.length == 0){
	  			validacao = false;
	  		}else{
	  			arraydesc[idprogramacao] = descontinuidade;	
	  		}
	  		
	    });
	    

		// var arrayinsp = new Array();
	 //    $.each($(".programacao"), function(id , val){
	 //  		var idprogramacao = $(val).prop('id');
	 //  		var inspecao = $("#cod_inspecao"+idprogramacao).val();
	 //  		arrayinsp[idprogramacao] = inspecao;
	 //    });
	


	    var arrayres = {};
	    $.each($(".programacao"), function(id , val){
	  		var idprogramacao = $(val).prop('id');
	  		var resultado = $("#resultado"+idprogramacao).val();
	  		if(resultado.length == 0){
	  			validacao = false;
	  		}else{
	  			arrayres[idprogramacao] = resultado;
	  		}
	  		
	    });
	    var arrayobs = {};
	    $.each($(".programacao"), function(id , val){
	  		var idprogramacao = $(val).prop('id');
	  		var obs = $("#observacao"+idprogramacao).val();
	  		arrayobs[idprogramacao] = obs;
	  		

	    });

	    var arraytam = {};
	    $.each($(".programacao"), function(id , val){
	  		var idprogramacao = $(val).prop('id');
	  		var tam = $("#tam_desc"+idprogramacao).val();
	  		arraytam[idprogramacao] = tam;
	  		

	    });

	    var quantidadefilmescassete = $("#quantidadefilmescassete").val();
		var ecranant = $("#ecranant").val();
		var ecranpos = $("#ecranpos").val();
		var tempec = $("#tempec").val();
		var condsupe = $("#condsupe").val();
		var temrev = $("#temrev").val();
		var temporev = $("#temporev").val();
		if(validacao){
			$.post('/guia/salvardadosresultado', {
		    	id              : idguia,
		    	_token          : token,
		    	arraydesc       : arraydesc,
		    	arrayobs        : arrayobs,
		    	// arrayinsp       : arrayinsp,
		    	arrayres        : arrayres,
		    	quantidadefilmescassete :quantidadefilmescassete,
				ecranant : ecranant,
				ecranpos: ecranpos,
				tempec: tempec,
				condsupe :condsupe,
				temrev :temrev,
				temporev :temporev,
				arraytam : arraytam,

		    }, function(data) {    
		    	alert(data.mensagem);
				location.href="/guia/"+idguia+"/visualizar";	
		    });
		}else{
			alert('Por Favor inclua resultado para todos os filmes antes de tentar salvar');
		}
	    
	});

	var files;
	$('#arquivo').on('change', prepareUpload);
	function prepareUpload(event){
	

		if($("#quantitens").val() && $("#cliente").val() != 0){
			$("#bt_ler_arquivo").show();
		}else{
			var mensagem = '';
			if(!$("#quantitens").val()) mensagem +='-Informe quantidade de itens quer importar\n';
			if($("#cliente").val() == 0) mensagem +='-Informe o cliente para importar o arquivo\n';
			alert(mensagem);
			$('input[type=file]').val('');
		}
	  	
	}

	
	$("#bt_carregar_procedimento").click(function(event) {
		var validacao = true;
		var arrayitem = [];
		
		$.each($(".item"), function(id , val){
	      	var item = $(val).prop('id').replace('item','');
	      	arrayitem.push(item);
	    });
	    var arraydiametro = [];
		$.each($(".diametro"), function(id , val){
	      	var diametro = $(val).prop('id').replace('diametro','');
	      	var valorn = $(val).val();
	      	arraydiametro[diametro]= valorn;
	    });
	    var arrayespessura = [];
		$.each($(".espessura"), function(id , val){
	      	var espessura = $(val).prop('id').replace('espessura','');
	      	var valorn = $(val).val();
	      	arrayespessura[espessura]= valorn;
	    });
	    var arraynorma = [];
		$.each($(".norma"), function(id , val){
	      	var norma = $(val).prop('id').replace('norma','');
	      	var valorn = $(val).val();
	      	if(valorn == 0){
	      		validacao = false;	
	      	}
	      	arraynorma[norma]= valorn;
	    });

		

		var arraycriterio_aceite = [];
		$.each($(".criterio_aceite"), function(id , val){
	      	var criterio_aceite = $(val).prop('id').replace('criterio_aceite','');
	      	var valorn = $(val).val();
	      	arraycriterio_aceite[criterio_aceite]= valorn;
	    });
	    var arraytipo = [];
		$.each($(".tipo"), function(id , val){
	      	var tipo = $(val).prop('id').replace('tipo','');
	      	var valorn = $(val).val();
	      	arraytipo[tipo]= valorn;
	    });

	    var tipo_fonte = $("#isotopo").text();
	    var mili_ou_pol = $("#mili").is(":checked"); //true = milimetros false = polegadas
	 

	    if((arrayitem.length == 0)||(!validacao)){
	    	alert('Dados inconsistentes, verifique os dados da programação');
	    }else{
		    $.post('/guia/carregarprocedimento', {
			    	_token          : token,
					item            : arrayitem,
					diametro        : arraydiametro,
					espessura       : arrayespessura,
					norma           : arraynorma,
					criterio_aceite : arraycriterio_aceite,
					tipo            : arraytipo,
					tipo_fonte      : tipo_fonte,
					mili_ou_pol     : mili_ou_pol,

			    }, function(data) {
			    	if(data.success == true){
			    		var procedimentos = data.procedimentos;
			    		var mensagemerro = false;
			    		var aux_array_rep = [];
			    		$.each(procedimentos, function(id , val){
			    			$("#tecnica"+id).empty();
			    			$("#linha"+id).removeClass('erroproc');
			    			if(val.length == 0){
			    				mensagemerro = true;
			    			}
			    			if(val.length == 1){
			    				aux_array_rep.push(val[0]);
			    				$("#tecnica"+id).empty();
			    				var sel = '<select id="sel'+id+'" class="tecn"><option value="null">selecione uma tecnica</option>';
			    				$.each(val, function(id , value){
			    					aux_array_rep.push(value);
			    					sel += '<option value="'+value['tecnica']+' - '+value['procedimento']+'">'+value['tecnica']+' - '+value['procedimento']+'</option>';
			    				});
			    				sel += '</select>';
								$("#tecnica"+id).append(sel);
			    				$("#tecnica"+id).prop('title',val[0]['procedimento']);
			    			}
			    			if(val.length > 1){
			    				$("#tecnica"+id).empty();
			    				var sel = '<select id="sel'+id+'" class="tecn"><option value="null">selecione uma tecnica</option>';
			    				$.each(val, function(id , value){
			    					aux_array_rep.push(value);
			    					sel += '<option value="'+value['tecnica']+' - '+value['procedimento']+'">'+value['tecnica']+' - '+value['procedimento']+'</option>';
			    				});
			    				sel += '</select>';
								$("#tecnica"+id).append(sel);
			    			}
					    });


					    
					    var repTec = '<select id="selRep"><option value="null">selecione uma tecnica</option>';
					    $.each(aux_array_rep, function(id , value){
			    			repTec += '<option value="'+value['tecnica']+' - '+value['procedimento']+'">'+value['tecnica']+' - '+value['procedimento']+'</option>';
			    		});
			    		repTec += '</select>';
			    		$("#repTec").empty();
					    $("#repTec").append(repTec);

						$("#selRep").change(function(event) {
							var tec = $(this).val();
							$.each(procedimentos, function(id , val){
						    	$('#sel'+id).val(tec);
						    });
						});					    

					    
					    if(mensagemerro){
					    	alert(data.msg);
					    }
					    $("#bt_salvar_guia").removeAttr('disabled');	
						
					    var erros = data.erros;
					    erros.forEach(function(element, index){
		    				$("#linha"+element).addClass('erroproc');
		    			});
			    	}
			    	if(data.success == false){		
		    			alert(data.msg);
		    			var erros = data.erros;
					    erros.forEach(function(element, index){
		    				$("#linha"+element).addClass('erroproc');
		    			});
			    	}
				}
			);
		}
	});

	
	$("#bt_salvar_guia").click(function(event) {
		$("#bt_salvar_guia").hide();
		var validacao = false;
		if(id_guia){
			var idguia = id_guia;		
		}else{
			var idguia = null;		
		}
		console.log('guia');
		console.log(idguia);
		var unidade = $("#unidade").val();
		var data = $("#data").val();
		var cliente_id = $("#cliente").val();
		var proposta_id = $("#proposta").val();
		var fonte_id = $("#fonte").val();
		var fonte_atividade = $("#fonte_atividade").val();
		var op1 = $("#op1").val();
		var op2 = $("#op2").val();
		var aux = $("#aux").val();
		var ins = $("#ins").val();
		var programacao_cliente = $("#controle_cliente").val();
		var local_ensaio = $("#local").val();
		var data_prog = $("#data_prog").val();
		var contratante_fabricante = $("#contratante_fabricante").val();
		var projeto = $("#projeto").val();

		var arrayitem = [];
		$.each($(".item"), function(id , val){
	      	var item = $(val).prop('id').replace('item','');
	      	arrayitem.push(item);
	    });
	    var arrayidentificacao = [];
		$.each($(".identificacao"), function(id , val){
	      	var identificacao = $(val).prop('id').replace('identificacao','');
	      	var valorn = $(val).val();
	      	arrayidentificacao[identificacao] = valorn;
	    });
	    var arrayspool = [];
		$.each($(".spool"), function(id , val){
	      	var spool = $(val).prop('id').replace('spool','');
	      	var valorn = $(val).val();
	      	arrayspool[spool] = valorn;
	    });
	    var arrayobra = [];
		$.each($(".obra"), function(id , val){
	      	var obra = $(val).prop('id').replace('obra','');
	      	var valorn = $(val).val();
	      	arrayobra[obra] = valorn;
	    });
		var arrayjunta = [];
		$.each($(".junta"), function(id , val){
	      	var junta = $(val).prop('id').replace('junta','');
	      	var valorn = $(val).val();
	      	arrayjunta[junta]= valorn;
	    });
	    var arrayposicao_soldagem = [];
		$.each($(".posicao_soldagem"), function(id , val){
	      	var posicao_soldagem = $(val).prop('id').replace('posicao_soldagem','');
	      	var valorn = $(val).val();
	      	arrayposicao_soldagem[posicao_soldagem]= valorn;
	    });
	    var arrayreforco = [];
		$.each($(".reforco"), function(id , val){
	      	var reforco = $(val).prop('id').replace('reforco','');
	      	var valorn = $(val).val();
	      	arrayreforco[reforco]= valorn;
	    });
	    var arraymetal_base = [];
		$.each($(".metal_base"), function(id , val){
	      	var metal_base = $(val).prop('id').replace('metal_base','');
	      	var valorn = $(val).val();
	      	arraymetal_base[metal_base]= valorn;
	    });
	    var arraydiametro = [];
		$.each($(".diametro"), function(id , val){
	      	var diametro = $(val).prop('id').replace('diametro','');
	      	var valorn = $(val).val();
	      	arraydiametro[diametro]= valorn;
	    });
	    var arrayespessura = [];
		$.each($(".espessura"), function(id , val){
	      	var espessura = $(val).prop('id').replace('espessura','');
	      	var valorn = $(val).val();
	      	arrayespessura[espessura]= valorn;
	    });
	    var arraychanfro = [];
		$.each($(".chanfro"), function(id , val){
	      	var chanfro = $(val).prop('id').replace('chanfro','');
	      	var valorn = $(val).val();
	      	arraychanfro[chanfro]= valorn;
	    });
	    var arraysinete_raiz = [];
		$.each($(".sinete_raiz"), function(id , val){
	      	var sinete_raiz = $(val).prop('id').replace('sinete_raiz','');
	      	var valorn = $(val).val();
	      	arraysinete_raiz[sinete_raiz]= valorn;
	    });
	    var arrayprocesso_raiz = [];
		$.each($(".processo_raiz"), function(id , val){
	      	var processo_raiz = $(val).prop('id').replace('processo_raiz','');
	      	var valorn = $(val).val();
	      	arrayprocesso_raiz[processo_raiz]= valorn;
	    });
		var arraysinete_acabamento = [];
		$.each($(".sinete_acabamento"), function(id , val){
	      	var sinete_acabamento = $(val).prop('id').replace('sinete_acabamento','');
	      	var valorn = $(val).val();
	      	arraysinete_acabamento[sinete_acabamento]= valorn;
	    });
	    var arrayprocesso_acabamento = [];
		$.each($(".processo_acabamento"), function(id , val){
	      	var processo_acabamento = $(val).prop('id').replace('processo_acabamento','');
	      	var valorn = $(val).val();
	      	arrayprocesso_acabamento[processo_acabamento]= valorn;
	    });
	    var arraynorma = [];
		$.each($(".norma"), function(id , val){
	      	var norma = $(val).prop('id').replace('norma','');
	      	var valorn = $(val).val();
	      	arraynorma[norma]= valorn;
	    });
	 //    var arrayaplicacao = [];
		// $.each($(".aplicacao"), function(id , val){
	 //      	var aplicacao = $(val).prop('id').replace('aplicacao','');
	 //      	var valorn = $(val).val();
	 //      	arrayaplicacao[aplicacao]= valorn;
	 //    });
		var arraycriterio_aceite = [];
		$.each($(".criterio_aceite"), function(id , val){
	      	var criterio_aceite = $(val).prop('id').replace('criterio_aceite','');
	      	var valorn = $(val).val();
	      	arraycriterio_aceite[criterio_aceite]= valorn;
	    });
	    var arraynivel_inspecao = [];
		$.each($(".nivel_inspecao"), function(id , val){
	      	var nivel_inspecao = $(val).prop('id').replace('nivel_inspecao','');
	      	var valorn = $(val).val();
	      	arraynivel_inspecao[nivel_inspecao]= valorn;
	    });
		var arrayobservacao = [];
		$.each($(".observacao"), function(id , val){
	      	var observacao = $(val).prop('id').replace('observacao','');
	      	var valorn = $(val).val();
	      	arrayobservacao[observacao]= valorn;
	    });
	    var arraytecnica = [];
		$.each($(".tecn"), function(id , val){
	      	var tecnica = $(val).prop('id').replace('sel','');
	      	var valorn = $(val).val();
	      

	      	arraytecnica[tecnica]= valorn;
	      	console.log(arraytecnica);
	    });   

	    var arraytipo = [];
		$.each($(".tipo"), function(id , val){
	      	var tipo = $(val).prop('id').replace('tipo','');
	      	var valorn = $(val).val();
	      	arraytipo[tipo]= valorn;
	    });

		// var unidade = $("#").val();
		if(projeto && unidade && data && cliente_id != 0 && proposta_id != 0 && fonte_id != 0 	&& op1 != 0 && op2 != 0 && ins != 0 && arrayitem.length > 0){
			validacao = true;
		}else{
			var mensagem = '';
			if(!projeto)         mensagem += '-Projeto é obrigatorio\n';
			if(!unidade)         mensagem += '-Unidade é obrigatorio\n';
			if(!data)            mensagem += '-Data é obrigatorio\n';
			if(cliente_id == 0)  mensagem += '-Cliente é obrigatorio\n';
			if(proposta_id == 0) mensagem += '-Proposta é obrigatorio\n';
			if(fonte_id == 0)    mensagem += '-Fonte é obrigatorio\n';
			if(arrayitem.length == 0) mensagem += '-Nenhum item inserido na GO\n';
			if(op1 == 0) mensagem += '-Operado 1 é obrigatorio\n';
			if(op2 == 0) mensagem += '-Operado 2 é obrigatorio\n';
			if(ins == 0) mensagem += '-Inspetor é obrigatorio\n';
			alert(mensagem);
			$("#bt_salvar_guia").show();
		}
		var mili_ou_pol = $("#mili").is(":checked"); //true = milimetros false = polegadas
		if(validacao){
			$.post('/guia/salvarguia', {
		    	_token          : token,
		    	projeto : projeto,
		    	unidade:unidade,
		    	data:data,
		    	cliente_id:cliente_id,
		    	proposta_id:proposta_id,
		    	fonte_id:fonte_id,
		    	fonte_atividade:fonte_atividade,
		    	op1 : op1,
				op2 : op2,
				aux : aux,
				ins : ins,
				programacao_cliente : programacao_cliente,
				local_ensaio : local_ensaio,
				data_prog : data_prog,
				contratante_fabricante : contratante_fabricante,
				item: arrayitem,
				identificacao :	arrayidentificacao,
				spool :	arrayspool,
				obra :	arrayobra,
				junta :	arrayjunta,
				posicao_soldagem :	arrayposicao_soldagem,
				reforco :	arrayreforco,
				metal_base :	arraymetal_base,
				diametro :	arraydiametro,
				espessura :	arrayespessura,
				chanfro :	arraychanfro,
				sinete_raiz :	arraysinete_raiz,
				processo_raiz :	arrayprocesso_raiz,
				sinete_acabamento  :	arraysinete_acabamento,
				processo_acabamento :	arrayprocesso_acabamento,
				norma :	arraynorma,
				// aplicacao :	arrayaplicacao,
				criterio_aceite : arraycriterio_aceite,
				nivel_inspecao :	arraynivel_inspecao,
				observacao :	arrayobservacao,
				idguia : idguia,
				tecnica : arraytecnica,
				tipo : arraytipo,
				mili_ou_pol:mili_ou_pol, 
		    }, function(data) {
		    	var erros = data.erros;
		    	if(data.success == false){
		    		var mensagem = 'Não foram encontrados procedimentos para os seguintes itens: ';
		    		erros.forEach(function(element, index){
		    			mensagem += element+'\n';
		    			$("#linha"+element).addClass('erroproc');
		    		});
		    	

		    		if(data.msg != ''){
		    			alert(data.msg);
		    		}else{
		    			alert(mensagem+'\n');	
		    		}
		    		$("#bt_salvar_guia").show();
		    	}
		    	if(data.success == true){
		    		alert('Guia Operacional Salva com sucesso!');
		    		location.href="/guia/"+data.idguia+"/visualizar";
		    	}
		    });
		}
	    
	});

	$("#clientes").change(function(event) {
		var id = $(this).val();
		$.post('/guia/dadoscliente', {
			_token  : token,
			id      : id,
		}, function(data) {
			if(data.success){
				var cliente = data.cliente;
				var cliente_id = data.cliente['id'];
				$("#nome_reduz").html('Nome: <b>'+cliente.nome_reduz+'</b>');
				$("#estado").html('Estado: <b>'+cliente.estado+'</b>');
				$("#municipio").html('Municipio: <b>'+cliente.municipio+'</b>');
			}
		});
	});

	$("#bt_salvar_layout").click(function(event) {
		idLay = $("#idLay").val();
		var cliente_id = $("#clientes").val();
		var programacao = $("#programacao").val();
		var obra = $("#obra").val();
		var local_ensaio = $("#local_ensaio").val();
		var data = $("#data").val();
		var contratante_fabricante = $("#contratante_fabricante").val();
		var item = $("#item").val();
		var iden_dese_iso = $("#iden_dese_iso").val();
		var mapa_spool = $("#mapa_spool").val();
		var prog_fabri = $("#prog_fabri").val();
		var junta = $("#junta").val();
		var posicao_soldagem = $("#posicao_soldagem").val();
		var metal_base = $("#metal_base").val();
		var diametro = $("#diametro").val();
		var espessura = $("#espessura").val();
		var reforco = $("#reforco").val();
		var espessura_menor = $("#espessura_menor").val();
		var espessura_maior = $("#espessura_maior").val();
		var Sold_sinete_raiz = $("#Sold_sinete_raiz").val();
		var processo_raiz = $("#processo_raiz").val();
		var sold_sinete_acab = $("#sold_sinete_acab").val();
		var processo_acab = $("#processo_acab").val();
		var metal_adicao = $("#metal_adicao").val();
		var chanfro = $("#chanfro").val();
		var nivel_inspecao = $("#nivel_inspecao").val();
		var norma = $("#norma").val();
		var observacao = $("#observacao").val();
		var inicio_descricao = $("#inicio_descricao").val();
		var criterio_aceite =  $("#criterio_aceite").val();
		$.post('/guia/salvarlayout', {
			_token  : token,
			idLay : idLay,
			cliente_id : cliente_id,
			programacao : programacao,
			obra : obra,
			local_ensaio : local_ensaio,
			data : data,
			contratante_fabricante : contratante_fabricante,
			item : item,
			iden_dese_iso : iden_dese_iso,
			mapa_spool : mapa_spool,
			prog_fabri : prog_fabri,
			junta : junta,
			posicao_soldagem : posicao_soldagem,
			metal_base : metal_base,
			diametro : diametro,
			espessura : espessura,
			reforco : reforco,
			espessura_menor : espessura_menor,
			espessura_maior : espessura_maior,
			Sold_sinete_raiz : Sold_sinete_raiz,
			processo_raiz : processo_raiz,
			sold_sinete_acab : sold_sinete_acab,
			processo_acab : processo_acab,
			metal_adicao : metal_adicao,
			chanfro : chanfro,
			nivel_inspecao : nivel_inspecao,
			norma : norma,
			observacao : observacao,
			inicio_descricao : inicio_descricao,
			criterio_aceite : criterio_aceite,
		}, function(data) {
			if(data.success){
				alert(data.mensagem);
				location.href="/guia/cadastrolayout";
			}
		});
	});

	function carregaClienteLay(){
		var id =$("#idLay").val();
		if(id){
			$.post('/guia/dadoscliente', {
				_token  : token,
				id      : id,
			}, function(data) {
				if(data.success){
					var cliente = data.cliente;
					var cliente_id = data.cliente['id'];
					$("#nome_reduz").html('Nome: <b>'+cliente.nome_reduz+'</b>');
					$("#estado").html('Estado: <b>'+cliente.estado+'</b>');
					$("#municipio").html('Municipio: <b>'+cliente.municipio+'</b>');
				}
			});
		}
	}
	function carregacliente(id,pro){
		$.post('/guia/dadoscliente', {
			_token  : token,
			id      : id,

		}, function(data) {
			if(data.success){
				
				var cliente = data.cliente;
				var cliente_id = data.cliente['id'];
				$("#nome_reduz").text(cliente.nome_reduz);
				$("#codigo").text(cliente.codigo);
				$("#cnpj").text(cliente.cnpj);
				$("#estado").text(cliente.estado);
				$("#endereco").text(cliente.endereco+' - '+cliente.bairro+' - '+cliente.municipio);
				$("#contato").text('('+cliente.ddd+') - '+cliente.tel);
				if(pro){
					$.post('/guia/dadosproposta', {
						_token  : token,
						cliente_id: cliente_id,

					}, function(data) {  

	              		items = "<option value=''>Selecione a Proposta</option>"; 

	              		$.each(data,function(index,item){
	                		items+="<option value='"+index+"'>"+item+"</option>";
	              		});

	              		$("#proposta").html(items); 
	              		$('#proposta').trigger("chosen:updated");
	        		});	
				}
				
			}
		});
	}
	function carregafonte(id){
 		$.post('/guia/dadosfonte', {
	    	id: id, 
	    	_token  : token
	    }, function(response) {      
	        $.each(response, function(index, value){
	            $('#'+index).html(value);

	         	if(index == 'atividade')
	          	{
	            	$('#fonte_atividade').val(value);
	          	}
	        });
	    });
 	}
 	$(".tipo").change(function(event) {
 		var tipo = $(this).val();
 	
 	});
	
$("#bt_go_manual").click(function(event) {
		$('#titulom').empty();
		$('#titulom').text('Alerta');
		$('#bodym').empty();
		
        $('#bodym').html('<div class="alert alert-danger alert-dismissible fade in" role="alert"><center><h1>“Este método somente pode ser utilizado em situações não cobertas por nossos procedimentos.</h1><h1> Este dados serão verificados em auditorias internas”</h1></center></div>');
		$('#ModalMnual').modal('toggle');

		
	});

var textFilmesReparo = new Array();
$(".frbt").click(function(event) {
	var id = $(this).prop('id').replace('frbt','');
	

	if($.inArray('F'+id, textFilmesReparo) < 0){
		

		textFilmesReparo.push('F'+id);
	}else{
		

		textFilmesReparo.splice($.inArray('F'+id, textFilmesReparo),1);
	}
	$("#fresco").empty();
	$("#fresco").text(textFilmesReparo.toString());
	

});
			

$("#bt_salvar_guia_fr").click(function(event) {
	var validacao = false;
	var programacao = $('#idFilme').val();
	

		// if(id_guia){
		// 	var idguia = id_guia;		
		// }else{
		// 	var idguia = null;		
		// }
		
		var data = $("#data").val();
		var fonte_id = $("#fonte").val();
		var fonte_atividade = $("#fonte_atividade").val();
		var op1 = $("#op1").val();
		var op2 = $("#op2").val();
		var aux = $("#aux").val();
		var ins = $("#ins").val();
		var programacao_cliente = $("#controle_cliente").val();
		var local_ensaio = $("#local").val();
		var data_prog = $("#data_prog").val();
		var contratante_fabricante = $("#contratante_fabricante").val();
		var stringfilmes = $("#fresco").text();
		


		
		
		if(data && fonte_id != 0 && op1 != 0 && op2 != 0 && ins != 0 && programacao_cliente && local_ensaio && data_prog && contratante_fabricante && stringfilmes ){
			validacao = true;
		}else{
			var mensagem = '';
			if(!data)            mensagem += '-Data é obrigatorio\n';
			if(fonte_id == 0)    mensagem += '-Fonte é obrigatorio\n';
			if(op1 == 0) mensagem += '-Operador 1 é obrigatorio\n';
			if(op2 == 0) mensagem += '-Operador 2 é obrigatorio\n';
			if(ins == 0) mensagem += '-Inspetor é obrigatorio\n';
			alert(mensagem);
		}

		if(validacao){
			$.post('/guia/salvarguiafr', {
		    	_token          : token,
		    	programacao : programacao,
		     	data : data,
		     	fonte_id : fonte_id,
		    	fonte_atividade : fonte_atividade,
		    	op1 : op1,
				op2 : op2,
				aux : aux,
				ins : ins,
				programacao_cliente : programacao_cliente,
				local_ensaio : local_ensaio,
				data_prog : data_prog,
				contratante_fabricante : contratante_fabricante,
				stringfilmes : stringfilmes,
		    }, function(data) {
		    	var erros = data.erros;
		    	if(data.success == false){
		    		var mensagem = 'Não foram encontrados procedimentos para os seguintes itens: ';
		    		erros.forEach(function(element, index){
		    			mensagem += element+'\n';
		    			$("#linha"+element).addClass('erroproc');
		    		});
		    		

		    		if(data.msg != ''){
		    			alert(data.msg);
		    		}else{
		    			alert(mensagem+'\n');
		    		}
		    	}
		    	if(data.success == true){
		    		alert('Guia Operacional Salva com sucesso!');
		    		location.href="/guia/"+data.idguia+"/visualizar";
		    	}
		    });
		}
	    
});

$("#bt_salvar_relatorio").click(function(event) {
	var guia_id = $("#guia_id").val();
	var arrayrelatorios = [];
	$.each($(".numero_relatorio"), function(id , val){
      	var num_rel = $(val).val();
	   

	    arrayrelatorios.push(num_rel);
	}); 

	var arrayitens = [];
	$.each($(".itensrelatorio"), function(id , val){
      	var relatorio = $(val).prop('id').replace('itensrelatorio','');
      	var valorn = $(val).val();
      	arrayitens[relatorio]= valorn;
    });

    var num_rel_cli = $("#num_rel_cli").val()

	

	$.post('/guia/salvarrelatorio', {
	    	_token          : token,
	    	guia_id : guia_id,
	    	numeros_relatorios : arrayrelatorios,
	    	itens_relatorio : arrayitens,
	    	num_rel_cli : num_rel_cli,
	    }, function(data) {
	    	if(data.success == true){
	    		alert('Relatorio Salvo!');
	    		location.href="/guia/"+guia_id+"/relatorio";
	    	}
	    });
});

$("#bt_editar_relatorio").click(function(event) {
	var guia_id = $("#guia_id").val();
	$.post('/guia/getRelatorios', {
	    	_token          : token,
	    	guia_id : guia_id,
	    }, function(data) {
	    	if(data.success){
	    		$('#titulom').empty();
				$('#titulom').text('Editar Relatorios');
				$('#bodym').empty();
				var relatorios ='';
				
				$.each(data.relatorios, function(id , val){
			      	relatorios += '<button class="btn btn-primary" id="'+val.numero_relatorio+'">Relatorio '+val.numero_relatorio+'</button> ';
			    });
				$('#bodym').html('<center>'+relatorios+'</center>');
				$('#Modalrelatorio').modal('toggle');
				
				$.each(data.relatorios, function(id , val){
				
					$("#"+val.numero_relatorio).click(function(event) {

						$('#bodym').empty();
						var table = '';
						table += '<table class="table table-bordered table_procedimento">';
                        	table += '<tr>';
                       			table += '<td colspan="8">Relatorio '+val.numero_relatorio+'</td>';
                        	table += '</tr>';
                       		table += '<tr>';
                        		table += '<td colspan="2">';
                        			table += 'Num. Rel. Cliente: </br>';
                        			table += '<input type="text" class="editpos" id="num_rel_cliente" value="'+val.numero_rel_cliente+'">';
                        		table += '</td>';
                        		table += '<td colspan="2">';
                        			table += 'Sol. Cliente: </br>';
                        			table += '<input type="text" class="editmat" id="prog_cliente" value="'+val.solicitacao_cliente+'">';
                        		table += '</td>';
                        		table += '<td >';
                        			table += 'Cont. Fabricante: </br>';
                        			table += '<input type="text" class="editmat" id="contrafabri" value="'+val.contratante_fabricante+'">';
                        		table += '</td>';
                        		table += '<td>';
                        			table += 'Projeto: </br>';
                        			table += '<input type="text"  class="editmat" id="projeto" value="'+val.projeto+'">';
                        		table += '</td>';
                        		table += '<td >';
                        			table += 'Local: </br>';
                        			table += '<input type="text" class="editmat" id="local" value="'+val.local+'">';
                        		table += '</td>';
                        		table += '<td >';
                        			table += 'Data: </br>';
                        			table += '<input type="date" class="editdata" id="datarel" value="'+val.data+'">';
                        		table += '</td>';
                        	table += '</tr>';
                        	$.each(data.item[val.numero_relatorio], function(id , val){
                   				table += '<tr>';

	                        		table += '<td> <input type="hidden" class="item" id="item'+val.item+'" value="'+val.item+'">';
	                        			table += 'Identificação: </br>';
	                        			table += '<input type="text" class="editjunta identificacao" id="identificacao'+val.item+'" value="'+val.identificacao+'">';
	                        		table += '</td>';
	                        		table += '<td>';
	                        			table += 'Spool: </br>';
	                        			table += '<input type="text" class="editjunta spool" id="spool'+val.item+'" value="'+val.spool+'">';
	                        		table += '</td>';
	                        		table += '<td>';
	                        			table += 'Junta: </br>';
	                        			table += '<input type="text" class="editjunta junta" id="junta'+val.item+'" value="'+val.junta+'">';
	                        		table += '</td>';
	                        		table += '<td>';
	                        			table += 'Posição: </br>';
	                        			table += '<input type="text"  class=" editpos posicao" id="posicao'+val.item+'" value="'+val.posicao_soldagem+'">';
	                        		table += '</td>';
	                        		table += '<td>';
	                        			table += 'Metal Base: </br>';
	                        			table += '<input type="text" class="editmat metal_base" id="metal_base'+val.item+'" value="'+val.metal_base+'">';
	                        		table += '</td>';
	                        		table += '<td>';
	                        			table += 'Sinete Raiz: </br>';
	                        			table += '<input type="text" class="editpos sinete_raiz" id="sinete_raiz'+val.item+'" value="'+val.sinete_raiz+'">';
	                        		table += '</td>';
	                        		table += '<td>';
	                        			table += 'Sin. Ench / Acab: </br>';
	                        			table += '<input type="text" class="editsin sinete_ench" id="sinete_ench'+val.item+'" value="'+val.sinete_acabamento+'">';
	                        		table += '</td>';
	                        		table += '<td>';
	                        			table += 'Processo: </br>';
	                        			table += '<input type="text" class="editpro processo" id="processo'+val.item+'" value="'+val.processo_raiz+'/'+val.processo_acabamento+'">';
	                        		table += '</td>';

	                        	table += '</tr>';
	                        });

                   		table += '</table><input type="hidden" id="id_relatorio" value="'+val.id+'">';
                		$('#bodym').html(table);

                		
					});
				});

	    	}
	});
	
});
	$('#bt_edt_rel').click(function(event) {
      	var guia_id = $("#guia_id").val();
		var num_rel_cliente = $("#num_rel_cliente ").val();
		var prog_cliente = $("#prog_cliente ").val();
		var contrafabri = $("#contrafabri ").val();
		var projeto = $("#projeto ").val();
		var local = $("#local ").val();
		var datarel = $("#datarel ").val();

		var identificacao = $("#identificacao ").val();
		var junta = $("#junta ").val();
		var posicao = $("#posicao ").val();
		var metal_base = $("#metal_base ").val();
		var sinete_raiz = $("#sinete_raiz ").val();
		var sinete_ench = $("#sinete_ench ").val();
		var processo = $("#processo ").val();

		var id_relatorio = $("#id_relatorio").val();
		

		var arrayitem = [];
		$.each($(".item"), function(id , val){
	      	var item = $(val).prop('id').replace('item','');
	      	var valorn = $(val).val();
	      	arrayitem[item]= valorn;
	    });
	    arrayitem = $.grep(arrayitem, function(v) {
			return v != null;
		});


		var arrayidentificacao = [];
		$.each($(".identificacao"), function(id , val){
	      	var item = $(val).prop('id').replace('identificacao','');
	      	var valorn = $(val).val();
	      	arrayidentificacao[item]= valorn;
	    });

		var arrayspool = [];
		$.each($(".spool"), function(id , val){
	      	var item = $(val).prop('id').replace('spool','');
	      	var valorn = $(val).val();
	      	arrayspool[item]= valorn;
	    });

	   var arrayjunta = [];
		$.each($(".junta"), function(id , val){
	      	var item = $(val).prop('id').replace('junta','');
	      	var valorn = $(val).val();
	      	arrayjunta[item]= valorn;
	    });
	    var arrayposicao = [];
		$.each($(".posicao"), function(id , val){
	      	var item = $(val).prop('id').replace('posicao','');
	      	var valorn = $(val).val();
	      	arrayposicao[item]= valorn;
	    });
	    var arraymetal_base = [];
		$.each($(".metal_base"), function(id , val){
	      	var item = $(val).prop('id').replace('metal_base','');
	      	var valorn = $(val).val();
	      	arraymetal_base[item]= valorn;
	    });
	    var arraysinete_raiz = [];
		$.each($(".sinete_raiz"), function(id , val){
	      	var item = $(val).prop('id').replace('sinete_raiz','');
	      	var valorn = $(val).val();
	      	arraysinete_raiz[item]= valorn;
	    });
	    var arraysinete_ench = [];
		$.each($(".sinete_ench"), function(id , val){
	      	var item = $(val).prop('id').replace('sinete_ench','');
	      	var valorn = $(val).val();
	      	arraysinete_ench[item]= valorn;
	    });
	    var arrayprocesso = [];
		$.each($(".processo"), function(id , val){
	      	var item = $(val).prop('id').replace('processo','');
	      	var valorn = $(val).val();
	      	arrayprocesso[item]= valorn;
	    });
		
		$.post('/guia/salvarrelatorioeditado', {
	    	_token          : token,
	    	num_rel_cliente : num_rel_cliente,
	    	prog_cliente : prog_cliente,
	    	contrafabri : contrafabri,
	    	projeto : projeto,
	    	local : local,
	    	datarel : datarel,
			identificacao : identificacao ,
			junta : junta ,
			posicao : posicao ,
			metal_base : metal_base ,
			sinete_raiz : sinete_raiz ,
			sinete_ench : sinete_ench ,
			processo : processo ,
			id_relatorio : id_relatorio,
			arrayitem : arrayitem,
			arrayidentificacao : arrayidentificacao,
			arrayspool : arrayspool,
			arrayjunta : arrayjunta,
			arrayposicao : arrayposicao,
			arraymetal_base : arraymetal_base,
			arraysinete_raiz : arraysinete_raiz,
			arraysinete_ench : arraysinete_ench,
			arrayprocesso : arrayprocesso,
			

	    }, function(data) {
	    	alert('Relatorio salvo com sucesso!');
			location.href="/guia/"+guia_id+"/relatorio";
	    });
	}); 













	var id_itens = '';
	var filmes_relatorio = [];
 	$(".itens").mousedown(function(event) {
 		id_itens = $(this).prop('id').replace('item','');
 	});
    $.each($(".itens"), function(id , val){
      	var iditem = $(val).prop('id').replace('item','');
      	$("#item"+iditem ).draggable({ revert: "valid" });
    });

    $( ".drop" ).droppable({
    	drop: function( event, ui ) {
       		preenchegrid();
      	}
  	});

    function preenchegrid(id){
    	if(id){
    		var idit = id;	
    	}else{
    		var idit = id_itens;
    	}
    	
    	$('#item'+id_itens).draggable('disable');
    	$('#item'+id_itens).css({
    		color: 'green',
    	});
    	$("#titulo"+id_itens).append('<div class="ibox-tools"><a class="close-link" id="close'+idit+'"><i class="fa fa-times"></i></a></div>')
    	$(".close-link").click(function(event) {
    		var idc = $(this).prop('id').replace('close','');
    		

    		tirarJunta(idc);
    	});

    	$( ".exc" ).text('CARREGANDO FILMES...');

    	$.post('/guia/getdadosrelatorios', {
	    	_token  : token,
	    	id : idit,
	    }, function(data) {    
	    	if(data.success){
	    		$( ".exc" ).remove();
	    		var body = $('#tbody').html();
	    		$('#tbody').empty();
	    		var tbody = '';

	    		$.each(data.filmes,function(index,item){
		    		tbody += '<tr class="'+idit+'">';				
					tbody += '<td style="font-size: 8px">'+item['isometrico']+'</td>';
					tbody += '<td style="font-size: 8px">'+item['numero_filme']+'</td>';
					tbody += '<td style="font-size: 8px">'+item['identificacao']+'</td>';
					tbody += '<td style="font-size: 8px">'+item['spool']+'</td>';
					tbody += '<td style="font-size: 8px">'+item['junta']+'</td>';
					tbody += '<td style="font-size: 8px">'+item['procedimento']+'</td>';
					tbody += '<td style="font-size: 8px">'+item['descontinuidade']+'</td>';
					tbody += '<td style="font-size: 8px">'+item['resultado']+'</td>';
					tbody += '</tr>';
					filmes_relatorio.push(item['id']);
		  		});
    			tbody += '<tr>';
				tbody += '<td colspan="20" height="70" class="drop exc">Arraste um item para preencher o relatorio</td>';
				tbody += '</tr>';

				$('#tbody').append(body+tbody);
				$( ".drop" ).droppable({
			    	drop: function( event, ui ) {
			       		preenchegrid();
			      	}
			  	});
			  	

	  		};
	    }	  		
	    );
    }

    function limpar(){
    	filmes_relatorio = [];
    	$('#tbody').empty();
    	var tbody = '';
    	tbody += '<tr>';
		tbody += '<td colspan="20" height="70" class="drop exc">Arraste um item para preencher o relatorio</td>';
		tbody += '</tr>';
		$('#tbody').append(tbody);
		$( ".drop" ).droppable({
	    	drop: function( event, ui ) {
	       		preenchegrid();
	      	}
	  	});
	  	
	  	$.each($(".itens"), function(id , val){
	      	var iditem = $(val).prop('id').replace('item','');
	      	$("#item"+iditem ).draggable({ revert: "valid" });
	      	$('#item'+iditem).draggable( "option", "disabled", false );
	      	$('#item'+iditem).css({
	    		color: 'black',
	    	});
	    	$('#item'+iditem+' .ibox-tools').remove();
	    });
    }

    function tirarJunta(id){
    	$( ".exc" ).text('RETIRANDO FILMES AGUARDE...');
    	$.post('/guia/getdadosrelatorios', {
	    	_token  : token,
	    	id : id_itens,
	    }, function(data) {    
	    	if(data.success){
	    		$.each(data.filmes,function(index,item){
					filmes_relatorio = $.grep(filmes_relatorio, function(v) {
						return v != item['id'];
					});
		  		});
		  		$( "."+id).remove();
		    	$("#item"+id ).draggable({ revert: "valid" });
		      	$('#item'+id).draggable( "option", "disabled", false );
		      	$('#item'+id).css({
		    		color: 'black',
		    	});
		    	$('#item'+id+' .ibox-tools').remove();
		  		$( ".exc" ).text('Arraste um item para preencher o relatorio');
	  		};
	    }	  		
	    );
    }

    $("#limpar").click(function(event) {
    	limpar();
    });

    var iditem_juntas = [];
   	var nIntervId = '';
    function incluirtodas() {
		// nIntervId = setTimeout(preenchegrid(iditem_juntas[0]), 5000);
		preenchegrid(iditem_juntas[0])
		console.log(iditem_juntas[0]);
		iditem_juntas.splice(0, 1);
		if (iditem_juntas[0] == null && iditem_juntas[0] == undefined) {
        	clearInterval(nIntervId);
        	alert('Todas as Juntas foram incluidas');
    	}
	}

	$("#bt_incluir_todas").click(function(event) {
		limpar();
    	$.each($(".itens"), function(id , val){
	      	iditem_juntas.push($(val).prop('id').replace('item',''));
	    });
	    nIntervId = setInterval(incluirtodas,1000);
	    
    });
    

    $("#select_norma").change(function(event) {
    	limpar();
    	$("#area_itens").empty();
    	$("#area_itens").append('<div class="ibox float-e-margins"><div class="ibox-title"><h5>Carregando Itens...</h5></div></div>');


		var norma = $(this).val();
		var guia  = $("#guia_id").val();
		$.post('/guia/getprogramacoesrelatorios', {
	    	_token  : token,
	    	guia_id : guia,
	    	norma : norma,
	    }, function(data) {    
	    	if(data.success){
	    		$("#area_itens").empty();
	    		var lista_itens = '';
	    		$.each(data.programacoes,function(index,item){
		    		lista_itens += '<div class="ibox float-e-margins itens" id="item'+item['id']+'">';
	                lista_itens += '<div class="ibox-title" id="titulo'+item['id']+'">';
	                lista_itens += '<h5>'+item['identificacao']+' - '+item['junta']+'</h5>';
	                lista_itens += '</div>';
	                lista_itens += '</div>';
	            });
	            $("#area_itens").append(lista_itens);
	            $(".itens").mousedown(function(event) {
			 		id_itens = $(this).prop('id').replace('item','');
			 	});
			    $.each($(".itens"), function(id , val){
			      	var iditem = $(val).prop('id').replace('item','');
			      	$("#item"+iditem ).draggable({ revert: "valid" });
			    });
	  		};
	    }	  		
	    );
    });

	$("#bt_salvar_novorelatorio").click(function(event) {
		$("#bt_salvar_novorelatorio").attr('disabled', 'disabled');
		var success = true;
		var guia_id     			= $("#guia_id").val();
		var norma_id    			= $("#select_norma").val();
		var projeto     			= $("#projeto").val();
		var contratante 			= $("#contratante").val();
		var local       			= $("#local").val();
		var criterio    			= $("#criterio").val();
		var numerorelatoriocliente  = $("#numerorelatoriocliente").val();
		var programacao_cliente     = $("#progcliente").val();
		var datarel  				= $("#datarel").val();
		var filmes                  = filmes_relatorio;


		var msg = '';
		if(norma_id == 0){
			msg += 'O campo Norma é obrigatorio.\n'
			success = false;
		}
		if(datarel == ''){
			msg += 'O campo Data é obrigatorio.\n'
			success = false;
		}
		if(filmes.length == 0){
			msg += 'Nenhum Item escolhido .\n'
			success = false;
		}

		if(success){
			$.post('/guia/salvarNovoRelatorio', {
		    	_token: 				token,
		    	guia_id: 				guia_id,
				norma_id: 				norma_id,
				projeto: 				projeto,
				contratante: 			contratante,
				local: 					local,
				criterio: 				criterio,
				numerorelatoriocliente: numerorelatoriocliente,
				datarel: 				datarel,
				filmes: 				filmes,
				programacao_cliente:    programacao_cliente,
		    }, function(data) {    
		    	if(data.success){
		    		alert('Relatorio Salvo!');
		    		location.href="/guia/"+guia_id+"/relatorio";
		  		};
		    });
		}else{
			alert(msg);
			$("#bt_salvar_novorelatorio").removeAttr('disabled');
		}
	});
	
	$("#colunas").change(function(event) {
		var id = $("#guia").val();
		var colu = $(this).val();
		var modelo = $("#modelo").val();
		console.log(colu);
		$.post('/guia/carregaretiquetas', {
		    	_token : token,
		    	id     : id,
		    }, function(data) {    
		    	if(data.success){
		    		$("#table").empty();
		    		var table = '';
		    		
					if(modelo == 1){
						if(colu == 2){
							location.href="/guia/"+id+"/etiqueta";
						}

						if(colu == 1){
			    			var dt = new Date(data.guia['data']);
			    			var d = dt.getDate();
			    			var m = dt.getMonth()+1;
			    			var a = dt.getFullYear();
			    			
			    			var contfilmes = 1
			    			console.log(contfilmes);
			    			table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
				    		$.each(data.filmes,function(index,item){

table +='<tr><td>';
table +='<div class="bordaredonda" style="border-style:solid; border-width:1px ; position: relative; margin-left: 35px; width: 8cm; height: 1.6cm; z-index: 2;" id="etiqueta">';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left:   3px; top:  2px;" id="label_logo"><img width="61" height="13" src="/img/img_logo_etiqueta.png"></div>';
table +='<div style="position: absolute; width: 120px; height: 22px; z-index: 15; left:  67px; top:  1px; font-size: 6pt" id="label_cliente"><b>CLIENTE: </b>'+data.guia['cliente']['nome'].substr(0,31)+'</div>';
table +='<div style="position: absolute; width: 110px; height: 22px; z-index: 15; left: 190px; top:  1px; font-size: 6pt" id="label_obra"><b>OBRA: </b>'+item['obra'].substr(0,25)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 260px; top:  1px; font-size: 6pt" id="label_filme"></div>';
table +='<div style="position: absolute; width: 127px; height: 13px; z-index: 15; left:   3px; top: 24px; font-size: 6pt" id="label_data"><b>DATA: </b>'+d+'/'+m+'/'+a+'</div>';
table +='<div style="position: absolute; width: 100px; height: 13px; z-index: 15; left:  75px; top: 24px; font-size: 6pt" id="label_espessura"><b>ESP: </b>'+item['espessura']+'</div>';
table +='<div style="position: absolute; width: 205px; height: 13px; z-index: 15; left: 125px; top: 24px; font-size: 5pt" id="label_identificacao"><b>IDENT / SPOOL: </b>'+(item['identificacao']+' / '+item['spool']).substr(0,33)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left:   3px; top: 34px ; font-size: 6pt" id="label_posicao"><b>POS. S.: </b>'+item['posicao_soldagem']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  65px; top: 34px; font-size: 6pt" id="label_junta"><b>JUN: </b>'+item['junta']+'</div>';
table +='<div style="position: absolute; width: 128px; height: 13px; z-index: 15; left: 145px; top: 34px; font-size: 6pt" id="label_soldador"><b>SOLD: </b>'+item['sinete_raiz']+' | '+item['sinete_acabamento']+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left: 249px; top: 34px; font-size: 6pt" id="label_diametro"><b>Ø: </b>'+item['diametro']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  3px;  top: 46px; font-size: 6pt" id="label_operador"><b>OPER: </b>'+data.funcionarios[data.equipe[0]['op1']]+'</div>';
table +='<div style="position: absolute; width:  109px; height: 13px; z-index: 15; left: 65px; top: 46px; font-size: 6pt" id="label_material"><b>MAT: </b>'+item['metal_base'].substr(0,19)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 170px; top: 46px; font-size: 6pt" id="label_filme"><b>GO: '+data.guia['id']+'/'+data.guia['unidade']+'/'+a+' </div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 238px; top: 46px; font-size: 6pt" id="label_go"><b>SEQ.</b>'+item['isometrico']+'</div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 280px; top: 46px; font-size: 6pt" id="label_go"><b>F </b>'+item['numero_filme']+'</div>';
table +='</div>';
table +='</td></tr>';
								

								if(contfilmes % 14 == 0){
									table+= '</table>';
									table +='<div class="page-break"></div></br>';
									table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';

								}
								contfilmes++;
	                                
	            			});	
						}
					}
					if(modelo == 2){
						var dt = new Date(data.guia['data']);
		    			var d = dt.getDate();
		    			var m = dt.getMonth()+1;
		    			var a = dt.getFullYear();
		    			
		    			var contfilmes = 1
		    			if(colu == 1){
		    				table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
			    			$.each(data.filmes,function(index,item){

table +='<tr><td>';
table +='<div class="bordaredonda" style="border-style:solid; border-width:1px ; position: relative; margin-left: 35px; width: 8cm; height: 1.6cm; z-index: 2;" id="etiqueta">';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left:   3px; top:  2px;" id="label_logo"><img width="61" height="13" src="/img/img_logo_etiqueta.png"></div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  1px; font-size: 6pt" id="label_cliente"><b>CLIENTE: </b>'+data.guia['cliente']['nome'].substr(0,23)+'</div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  12px; font-size: 6pt" id="label_obra"><b>OBRA: </b>'+item['obra'].substr(0,25)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 260px; top:  1px; font-size: 6pt" id="label_filme"></div>';
table +='<div style="position: absolute; width: 180px; height: 13px; z-index: 15; left: 230px; top: 1px; font-size: 6pt" id="label_data"><b>DATA: </b>'+d+'/'+m+'/'+a+'</div>';
table +='<div style="position: absolute; width: 100px; height: 13px; z-index: 15; left:  210px; top: 34px; font-size: 6pt" id="label_espessura"><b>ESP: </b>'+item['espessura']+'</div>';
table +='<div style="position: absolute; width: 300px; height: 13px; z-index: 15; left:   3px; top: 24px; font-size: 6pt"  id="label_identificacao"><b>IDENT / SPOOL: </b>'+(item['identificacao']+' / '+item['spool']).substr(0,50)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left:   3px; top: 34px ; font-size: 6pt" id="label_posicao"><b>POS. S.: </b>'+item['posicao_soldagem']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  52px; top: 34px; font-size: 6pt" id="label_junta"><b>JUN: </b>'+item['junta']+'</div>';
table +='<div style="position: absolute; width: 128px; height: 13px; z-index: 15; left: 110px; top: 34px; font-size: 6pt" id="label_soldador"><b>SOLD: </b>'+(item['sinete_raiz']+' | '+item['sinete_acabamento']).substr(0,15)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left: 249px; top: 34px; font-size: 6pt" id="label_diametro"><b>Ø: </b>'+item['diametro']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  3px;  top: 46px; font-size: 6pt" id="label_operador"><b>OPER: </b>'+data.funcionarios[data.equipe[0]['op1']]+'</div>';
table +='<div style="position: absolute; width:  109px; height: 13px; z-index: 15; left: 65px; top: 46px; font-size: 6pt" id="label_material"><b>MAT: </b>'+item['metal_base'].substr(0,19)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 170px; top: 46px; font-size: 6pt" id="label_filme"><b>GO: '+data.guia['id']+'/'+data.guia['unidade']+'/'+a+' </div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 238px; top: 46px; font-size: 6pt" id="label_go"><b>SEQ.</b>'+item['isometrico']+'</div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 280px; top: 46px; font-size: 6pt" id="label_go"><b>F </b>'+item['numero_filme']+'</div>';
table +='</div>';
table +='</td></tr>';

								if(contfilmes % 14 == 0){
									table+= '</table>';
									table +='<div class="page-break"></div></br>';
									table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
								}
								contfilmes++;
            				});
							table+= '</table>';
							$("#table").append(table);
						}
						if(colu == 2){
							var contcolu = 1;
							table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
			    			$.each(data.filmes,function(index,item){
			    				if(contcolu == 1){table+='<tr>';}
table +='<td>';
table +='<div class="bordaredonda" style="border-style:solid; border-width:1px ; position: relative; margin-left: 35px; width: 8cm; height: 1.6cm; z-index: 2;" id="etiqueta">';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left:   3px; top:  2px;" id="label_logo"><img width="61" height="13" src="/img/img_logo_etiqueta.png"></div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  1px; font-size: 6pt" id="label_cliente"><b>CLIENTE: </b>'+data.guia['cliente']['nome'].substr(0,23)+'</div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  12px; font-size: 6pt" id="label_obra"><b>OBRA: </b>'+item['obra'].substr(0,25)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 260px; top:  1px; font-size: 6pt" id="label_filme"></div>';
table +='<div style="position: absolute; width: 180px; height: 13px; z-index: 15; left: 230px; top: 1px; font-size: 6pt" id="label_data"><b>DATA: </b>'+d+'/'+m+'/'+a+'</div>';
table +='<div style="position: absolute; width: 100px; height: 13px; z-index: 15; left:  210px; top: 34px; font-size: 6pt" id="label_espessura"><b>ESP: </b>'+item['espessura']+'</div>';
table +='<div style="position: absolute; width: 300px; height: 13px; z-index: 15; left:   3px; top: 24px; font-size: 6pt"  id="label_identificacao"><b>IDENT / SPOOL: </b>'+(item['identificacao']+' / '+item['spool']).substr(0,50)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left:   3px; top: 34px ; font-size: 6pt" id="label_posicao"><b>POS. S.: </b>'+item['posicao_soldagem']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  52px; top: 34px; font-size: 6pt" id="label_junta"><b>JUN: </b>'+item['junta']+'</div>';
table +='<div style="position: absolute; width: 128px; height: 13px; z-index: 15; left: 110px; top: 34px; font-size: 6pt" id="label_soldador"><b>SOLD: </b>'+(item['sinete_raiz']+' | '+item['sinete_acabamento']).substr(0,15)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left: 249px; top: 34px; font-size: 6pt" id="label_diametro"><b>Ø: </b>'+item['diametro']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  3px;  top: 46px; font-size: 6pt" id="label_operador"><b>OPER: </b>'+data.funcionarios[data.equipe[0]['op1']]+'</div>';
table +='<div style="position: absolute; width:  109px; height: 13px; z-index: 15; left: 65px; top: 46px; font-size: 6pt" id="label_material"><b>MAT: </b>'+item['metal_base'].substr(0,19)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 170px; top: 46px; font-size: 6pt" id="label_filme"><b>GO: '+data.guia['id']+'/'+data.guia['unidade']+'/'+a+' </div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 238px; top: 46px; font-size: 6pt" id="label_go"><b>SEQ.</b>'+item['isometrico']+'</div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 280px; top: 46px; font-size: 6pt" id="label_go"><b>F </b>'+item['numero_filme']+'</div>';
table +='</div>';
								

								if(contfilmes % 28 == 0){
									console.log('asdasdasdasdas');
									table+= '</td> </tr></table>';
									table +='<div class="page-break"></div></br>';
									table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
									contcolu=1;
								}else{
									table+= '</td>';
									if(contcolu > 1){table+= '</tr>';contcolu=1;}
	                                else if(contcolu < 2){table+= ' </td>';contcolu++;}
                              	}     
								contfilmes++;
								console.log(contfilmes);
            				});
							table+= '</table>';
							$("#table").append(table);
						}
					}
					table+= '</table>';
					$("#table").append(table);
		  		};
		    });
	});
	$("#modelo").change(function(event) {
		var id = $("#guia").val();
		var modelo = $(this).val();
		var colu = $("#colunas").val();
		console.log(colu);
		$.post('/guia/carregaretiquetas', {
		    	_token : token,
		    	id     : id,
		    }, function(data) {    
		    	if(data.success){
		    		$("#table").empty();
		    		var table = '';
		    		if(modelo == 1){
						location.href="/guia/"+id+"/etiqueta";
					}
		    		if(modelo == 2){
		    			var dt = new Date(data.guia['data']);
		    			var d = dt.getDate();
		    			var m = dt.getMonth()+1;
		    			var a = dt.getFullYear();
		    			
		    			var contfilmes = 1
		    			if(colu == 1){
		    				table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
			    			$.each(data.filmes,function(index,item){

table +='<tr><td>';
table +='<div class="bordaredonda" style="border-style:solid; border-width:1px ; position: relative; margin-left: 35px; width: 8cm; height: 1.6cm; z-index: 2;" id="etiqueta">';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left:   3px; top:  2px;" id="label_logo"><img width="61" height="13" src="/img/img_logo_etiqueta.png"></div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  1px; font-size: 6pt" id="label_cliente"><b>CLIENTE: </b>'+data.guia['cliente']['nome'].substr(0,23)+'</div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  12px; font-size: 6pt" id="label_obra"><b>OBRA: </b>'+item['obra'].substr(0,25)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 260px; top:  1px; font-size: 6pt" id="label_filme"></div>';
table +='<div style="position: absolute; width: 180px; height: 13px; z-index: 15; left: 230px; top: 1px; font-size: 6pt" id="label_data"><b>DATA: </b>'+d+'/'+m+'/'+a+'</div>';
table +='<div style="position: absolute; width: 100px; height: 13px; z-index: 15; left:  210px; top: 34px; font-size: 6pt" id="label_espessura"><b>ESP: </b>'+item['espessura']+'</div>';
table +='<div style="position: absolute; width: 300px; height: 13px; z-index: 15; left:   3px; top: 24px; font-size: 6pt"  id="label_identificacao"><b>IDENT / SPOOL: </b>'+(item['identificacao']+' / '+item['spool']).substr(0,50)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left:   3px; top: 34px ; font-size: 6pt" id="label_posicao"><b>POS. S.: </b>'+item['posicao_soldagem']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  52px; top: 34px; font-size: 6pt" id="label_junta"><b>JUN: </b>'+item['junta']+'</div>';
table +='<div style="position: absolute; width: 128px; height: 13px; z-index: 15; left: 110px; top: 34px; font-size: 6pt" id="label_soldador"><b>SOLD: </b>'+(item['sinete_raiz']+' | '+item['sinete_acabamento']).substr(0,15)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left: 249px; top: 34px; font-size: 6pt" id="label_diametro"><b>Ø: </b>'+item['diametro']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  3px;  top: 46px; font-size: 6pt" id="label_operador"><b>OPER: </b>'+data.funcionarios[data.equipe[0]['op1']]+'</div>';
table +='<div style="position: absolute; width:  109px; height: 13px; z-index: 15; left: 65px; top: 46px; font-size: 6pt" id="label_material"><b>MAT: </b>'+item['metal_base'].substr(0,19)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 170px; top: 46px; font-size: 6pt" id="label_filme"><b>GO: '+data.guia['id']+'/'+data.guia['unidade']+'/'+a+' </div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 238px; top: 46px; font-size: 6pt" id="label_go"><b>SEQ.</b>'+item['isometrico']+'</div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 280px; top: 46px; font-size: 6pt" id="label_go"><b>F </b>'+item['numero_filme']+'</div>';
table +='</div>';
table +='</td></tr>';

								if(contfilmes % 14 == 0){
									table+= '</table>';
									table +='<div class="page-break"></div></br>';
									table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
								}
								contfilmes++;
            				});
							table+= '</table>';
							$("#table").append(table);
						}
						if(colu == 2){
							var contcolu = 1;
							table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
			    			$.each(data.filmes,function(index,item){
			    				if(contcolu == 1){table+='<tr>';}
table +='<td>';
table +='<div class="bordaredonda" style="border-style:solid; border-width:1px ; position: relative; margin-left: 35px; width: 8cm; height: 1.6cm; z-index: 2;" id="etiqueta">';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left:   3px; top:  2px;" id="label_logo"><img width="61" height="13" src="/img/img_logo_etiqueta.png"></div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  1px; font-size: 6pt" id="label_cliente"><b>CLIENTE: </b>'+data.guia['cliente']['nome'].substr(0,23)+'</div>';
table +='<div style="position: absolute; width: 240px; height: 22px; z-index: 15; left:  67px; top:  12px; font-size: 6pt" id="label_obra"><b>OBRA: </b>'+item['obra'].substr(0,25)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 260px; top:  1px; font-size: 6pt" id="label_filme"></div>';
table +='<div style="position: absolute; width: 180px; height: 13px; z-index: 15; left: 230px; top: 1px; font-size: 6pt" id="label_data"><b>DATA: </b>'+d+'/'+m+'/'+a+'</div>';
table +='<div style="position: absolute; width: 100px; height: 13px; z-index: 15; left:  210px; top: 34px; font-size: 6pt" id="label_espessura"><b>ESP: </b>'+item['espessura']+'</div>';
table +='<div style="position: absolute; width: 300px; height: 13px; z-index: 15; left:   3px; top: 24px; font-size: 6pt"  id="label_identificacao"><b>IDENT / SPOOL: </b>'+(item['identificacao']+' / '+item['spool']).substr(0,50)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left:   3px; top: 34px ; font-size: 6pt" id="label_posicao"><b>POS. S.: </b>'+item['posicao_soldagem']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  52px; top: 34px; font-size: 6pt" id="label_junta"><b>JUN: </b>'+item['junta']+'</div>';
table +='<div style="position: absolute; width: 128px; height: 13px; z-index: 15; left: 110px; top: 34px; font-size: 6pt" id="label_soldador"><b>SOLD: </b>'+(item['sinete_raiz']+' | '+item['sinete_acabamento']).substr(0,15)+'</div>';
table +='<div style="position: absolute; width:  94px; height: 13px; z-index: 15; left: 249px; top: 34px; font-size: 6pt" id="label_diametro"><b>Ø: </b>'+item['diametro']+'</div>';
table +='<div style="position: absolute; width:  84px; height: 13px; z-index: 15; left:  3px;  top: 46px; font-size: 6pt" id="label_operador"><b>OPER: </b>'+data.funcionarios[data.equipe[0]['op1']]+'</div>';
table +='<div style="position: absolute; width:  109px; height: 13px; z-index: 15; left: 65px; top: 46px; font-size: 6pt" id="label_material"><b>MAT: </b>'+item['metal_base'].substr(0,19)+'</div>';
table +='<div style="position: absolute; width:  80px; height: 13px; z-index: 15; left: 170px; top: 46px; font-size: 6pt" id="label_filme"><b>GO: '+data.guia['id']+'/'+data.guia['unidade']+'/'+a+' </div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 238px; top: 46px; font-size: 6pt" id="label_go"><b>SEQ.</b>'+item['isometrico']+'</div>';
table +='<div style="position: absolute; width:  37px; height: 13px; z-index: 15; left: 280px; top: 46px; font-size: 6pt" id="label_go"><b>F </b>'+item['numero_filme']+'</div>';
table +='</div>';
								

								if(contfilmes % 28 == 0){
									console.log('asdasdasdasdas');
									table+= '</td> </tr></table>';
									table +='<div class="page-break"></div></br>';
									table += '<table class="etiqueta" cellspacing="0" cellpadding="0">';
									contcolu=1;
								}else{
									table+= '</td>';
									if(contcolu > 1){table+= '</tr>';contcolu=1;}
	                                else if(contcolu < 2){table+= ' </td>';contcolu++;}
                              	}     
								contfilmes++;
								console.log(contfilmes);
            				});
							table+= '</table>';
							$("#table").append(table);
						}
					}				
					
		  		};
		    });
	});

	$("#bt_sinc_gamagrafia").click(function(event) {
		$.post('/gamagrafia/sincronizacaogama', {
		// $.post('/gamagrafia/getdumpgama', {
		    	_token: 				token,
		    }, function(data) {   
		    	if(data.success){
		   //  		var dump = data.dump;
		   //  		$.ajax({
					// 	type: "POST",
					// 	url: "http://portalwebapp.com/sinc/",
					// 	data: dump,
					// 	success: function( data )
					// 	{
					// 		alert( data );
					// 	}
					// });
		   //  		// $.post('http://portalwebapp.com/sinc/', {
					   //  	_token: 				token,
					   //  	dump : dump
					   //  }, function(dat) {   
					   //  	if(dat.success){
					   //  		alert(dat.mensagem);
					   //  	}
					   //  });
		alert(data.mensagem);
		    	}
		    	if(!data.success){
		    		alert(data.mensagem);
		    	}
		    });
	});

	$("#bt_sinc_fontes").click(function(event) {
		$.post('/gamagrafia/sincronizacaofontes', {
		    	_token: 				token,
		    }, function(data) {   
		    	if(data.success){
		    		alert(data.mensagem);
		    	}
		    	if(!data.success){
		    		alert(data.mensagem);
		    	}
		    });
	});

	$("#bt_sinc_dados").click(function(event) {
		$.post('/gamagrafia/sincronizacaodados', {
		    	_token: 				token,
		    }, function(data) {   
		    	if(data.success){
		    		alert(data.mensagem);
		    	}
		    	if(!data.success){
		    		alert(data.mensagem);
		    	}
		    });
	});

	$("#bt_go_n_real").click(function(event) {
		var idguia = $("#idguia").val();
		console.log(idguia);
		$.post('/guia/'+idguia+'/gonaoreal', {
		    	_token: 				token,
		    }, function(data) {   
		    	if(data.success){
		    		location.href="/guia/"+data.id+"/visualizar";
		    	}
		    });
	});
});// end document


