$(document).ready(function() {
	
	// $('.flashError').fadeOut(0);
 //  	$('.flashSuccess').fadeOut(0);  
	
	var token    = $("#token").val();
	var stringids= $("#stringids").val();
	var data_ini = $("#inicio").val();
	var	data_fim = $("#fim").val();
	var	cliente_id = $("#cliente_id").val();
	var valor_total = $("#valor_total").val();
	var	proposta_id = $("#proposta_id").val();

	$("#bt_carregar_boletim").click(function(event) {
		var cliente_id = $("#clienteBM").val();
		var proposta_id = $("#propostaBM").val();
		var data_inicio = $("#data_inicio").val();
		var data_fim = $("#data_fim").val();

		console.log(cliente_id, proposta_id, data_inicio, data_fim);

		$.post('/boletim/carregarBM', {

			_token  : token,
			cliente_id      : cliente_id,
			proposta_id 	: proposta_id,
			data_inicio		: data_inicio,
			data_fim		: data_fim,

 		}, function(data) {
			if(data.success){
				console.log('total: '+data.total);
				console.log(data);
				var cliente = data.cliente;
				var proposta = data.proposta;

				var guias = data.guias;
				var programacoes = data.programacoes;
				var quantidades = data.quantidades;
				var valores = data.valores;
				var resultado = data.resultado;
				var diametros = data.diametros;
				var valores_diametros = data.valores_diametros;

				console.log(resultado);
				
				
				/* table BM */
				//$("#id_bm").text(bm.id_bm);
				//$("#id_pr").text(cliente.id_pr);

				/* table Cliente */
				$("#nome").text(cliente.nome);
				$("#endereco").text(cliente.endereco+' - '+cliente.bairro+' - '+cliente.municipio);
				$("#projeto").text(data.projeto.servico);
				$("#local").text(data.projeto.estado+' - '+data.projeto.municipio);
				$("#referencia").text(" - ");
				//$("#local").text(data.cliente.local);

				/* table Projeto */
				$("#nome_solicitante").text(data.projeto.solicitacao_contato);
				$("#email_solicitante").text(data.projeto.solicitacao_email);
				$("#tel_solicitante").text(data.projeto.solicitacao_tel);

				$("#nome_contato").text(data.projeto.aprovacao_contato);
				$("#email_contato").text(data.projeto.aprovacao_email);
				$("#tel_contato").text(data.projeto.aprovacao_tel);

				/* table Projeto */
				console.log(proposta.cod_proposta);
				$("#proposta").text(proposta.cod_proposta);
				$("#id_pr").html(proposta.cod_proposta);

				// /*tratamento no controler para exibição*/
				$("#inicio").text(data.data_inicio);
				$("#fim").text(data.data_fim);

				$("#tableboletim").empty();	
				
				var table ='';
				var total_km = 0;
				var somadiametroA = 0;
				var somadiametroB = 0;
				var somadiametroC = 0;
				var somadiametroD = 0;
				var somadiametroE = 0;
				var somadiametroF = 0;
				var somadiametroG = 0;
				var somadiametroH = 0;

				
				var cliente_id = cliente.id;
				var proposta_id = proposta.id;

				$.each(programacoes,function(index,item)
				{	
					idguia.push(guias[index].id);
					console.log(idguia);
					d = guias[index].data.substr(0, 10).split("-");
					d = d[2] + "/" + d[1] + "/" + d[0];
					table += '<tr>';
					table += '<td>'+d+'</td>';
					table += '<td>'+guias[index].id+'</td>';

					if(guias[index].diaria_normal)
						table += '<td>'+guias[index].diaria_normal+'</td>';
					else
						table += '<td>-</td>';

					if(guias[index].diaria_dom)
						table += '<td>'+guias[index].diaria_dom+'</td>';
					else
						table += '<td>-</td>';

					if(guias[index].extra_normal)
						table += '<td>'+guias[index].extra_normal+'</td>';
					else
						table += '<td>-</td>';

					if(guias[index].extra_dom)
						table += '<td>'+guias[index].extra_dom+'</td>';
					else
						table += '<td>-</td>';
					var km =(guias[index].km_final - guias[index].km_inicial);
					total_km += km;
					table += '<td>'+km+'</td>';

					if(item.filme_dimensao == 'A')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'B')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'C')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'D')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'E')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'F')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'G')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'H')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';

					if(item.filme_dimensao == 'I')
						table += '<td>'+item.quantidade_filmes+'</td>';
					else
						table += '<td>-</td>';
					if(diametros[guias[index].id][0]){
						if(diametros[guias[index].id][0].junta_id >= 1 && diametros[guias[index].id][0].junta_id <= 7){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroA += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
						if(diametros[guias[index].id][0].junta_id >= 8 && diametros[guias[index].id][0].junta_id <= 9){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroB += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
						if(diametros[guias[index].id][0].junta_id == 10){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroC += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
						if(diametros[guias[index].id][0].junta_id >= 11 && diametros[guias[index].id][0].junta_id <= 14){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroD += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
						if(diametros[guias[index].id][0].junta_id >= 15 && diametros[guias[index].id][0].junta_id <= 18){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroE += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
						if(diametros[guias[index].id][0].junta_id >= 19 && diametros[guias[index].id][0].junta_id <= 20){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroF += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
						if(diametros[guias[index].id][0].junta_id == 21){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroG += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
						if(diametros[guias[index].id][0].junta_id == 22){
							table += '<td>'+diametros[guias[index].id][0].quantidade+'</td>';
							somadiametroH += parseInt(diametros[guias[index].id][0].quantidade);
						}else{
							table += '<td>-</td>';
						}
					}else{
						table += '<td>-</td>';
						table += '<td>-</td>';
						table += '<td>-</td>';
						table += '<td>-</td>';
						table += '<td>-</td>';
						table += '<td>-</td>';
						table += '<td>-</td>';
						table += '<td>-</td>';
					}
					table += '</tr>';
			
	  			});
				
				console.log(idguia);
				
				$("#tableboletim").append(table);
				

				/*preco unitario*/
				$("#quantss").text(data.quant_diaria_normal);
				$("#quantdf").text(data.quant_diaria_dom);
				$("#quantsse").text(data.quant_extra_normal);
				$("#quantdfe").text(data.quant_extra_dom);
				$("#quantkm").text(total_km);
				if(quantidades.A){
					$("#quantA").text(quantidades.A);	
				}else{
					$("#quantA").text('0');	
				}
				if(quantidades.B){
					$("#quantB").text(quantidades.B);	
				}else{
					$("#quantB").text('0');	
				}
				if(quantidades.C){
					$("#quantC").text(quantidades.C);	
				}else{
					$("#quantC").text('0');	
				}
				if(quantidades.D){
					$("#quantD").text(quantidades.D);	
				}else{
					$("#quantD").text('0');	
				}
				if(quantidades.E){
					$("#quantE").text(quantidades.E);	
				}else{
					$("#quantE").text('0');	
				}
				if(quantidades.F){
					$("#quantF").text(quantidades.F);	
				}else{
					$("#quantF").text('0');	
				}
				if(quantidades.G){
					$("#quantG").text(quantidades.G);	
				}else{
					$("#quantG").text('0');	
				}
				if(quantidades.H){
					$("#quantH").text(quantidades.H);	
				}else{
					$("#quantH").text('0');	
				}
				if(quantidades.I){
					$("#quantI").text(quantidades.I);	
				}else{
					$("#quantI").text('0');	
				}

				$("#diametroA").text(somadiametroA);	
				$("#diametroB").text(somadiametroB);	
				$("#diametroC").text(somadiametroC);	
				$("#diametroD").text(somadiametroD);	
				$("#diametroE").text(somadiametroE);	
				$("#diametroF").text(somadiametroF);	
				$("#diametroG").text(somadiametroG);	
				$("#diametroH").text(somadiametroH);	
				

				/*preco unitario*/
				if(data.valores_diarias['domingo']){
					$("#valordf").text('R$ '+data.valores_diarias['domingo'].valor_normal);	
					$("#valordfe").text('R$ '+data.valores_diarias['domingo'].valor_extra);
				}
				$("#valorss").text('R$ '+data.valores_diarias['normal'].valor_normal);
				$("#valorsse").text('R$ '+data.valores_diarias['normal'].valor_extra);
				
				$("#valorkm").text('R$ '+data.valor_km);
				$("#valorA").text('R$ '+valores.A);
				$("#valorB").text('R$ '+valores.B);
				$("#valorC").text('R$ '+valores.C);
				$("#valorD").text('R$ '+valores.D);
				$("#valorE").text('R$ '+valores.E);
				$("#valorF").text('R$ '+valores.F);
				$("#valorG").text('R$ '+valores.G);
				$("#valorH").text('R$ '+valores.H);
				$("#valorI").text('R$ '+valores.I);


				$("#valordiametroA").text('R$ '+valores_diametros[7]);
				$("#valordiametroB").text('R$ '+valores_diametros[9]);
				$("#valordiametroC").text('R$ '+valores_diametros[10]);
				$("#valordiametroD").text('R$ '+valores_diametros[14]);
				$("#valordiametroE").text('R$ '+valores_diametros[18]);
				$("#valordiametroF").text('R$ '+valores_diametros[20]);
				$("#valordiametroG").text('R$ '+valores_diametros[21]);
				$("#valordiametroH").text('R$ '+valores_diametros[22]);

				/*somatorio resultados*/
				$("#resultadoss").text('R$ '+data.total_normal);
				$("#resultadodf").text('R$ '+data.total_domingo);
				$("#resultadosse").text('R$ '+data.total_extra_normal);
				$("#resultadodfe").text('R$ '+data.total_extra_domingo);
				var valor_total_km = data.valor_km * total_km;
				$("#resultadokm").text('R$ '+valor_total_km);
				if(resultado.A){
					$("#resultadoA").text('R$ '+resultado.A);	
				}else{
					$("#resultadoA").text('R$ 00.00');	
				}
				if(resultado.B){
					$("#resultadoB").text('R$ '+resultado.B);	
				}else{
					$("#resultadoB").text('R$ 00.00');	
				}
				if(resultado.C){
					$("#resultadoC").text('R$ '+resultado.C);	
				}else{
					$("#resultadoC").text('R$ 00.00');	
				}
				if(resultado.D){
					$("#resultadoD").text('R$ '+resultado.D);	
				}else{
					$("#resultadoD").text('R$ 00.00');	
				}
				if(resultado.E){
					$("#resultadoE").text('R$ '+resultado.E);	
				}else{
					$("#resultadoE").text('R$ 00.00');	
				}
				if(resultado.F){
					$("#resultadoF").text('R$ '+resultado.F);	
				}else{
					$("#resultadoF").text('R$ 00.00');	
				}
				if(resultado.G){
					$("#resultadoG").text('R$ '+resultado.G);	
				}else{
					$("#resultadoG").text('R$ 00.00');	
				}
				if(resultado.H){
					$("#resultadoH").text('R$ '+resultado.H);	
				}else{
					$("#resultadoH").text('R$ 00.00');	
				}
				if(resultado.I){
					$("#resultadoI").text('R$ '+resultado.I);	
				}else{
					$("#resultadoI").text('R$ 00.00');	
				}
				var total_valores_diametros = 0
				var valor_total_A = valores_diametros[7] * somadiametroA;
				total_valores_diametros += valor_total_A;
				$("#totaldiametroA").text('R$ '+valor_total_A);

				var valor_total_B = valores_diametros[9] * somadiametroB;
				total_valores_diametros += valor_total_B;
				$("#totaldiametroB").text('R$ '+valor_total_B);

				var valor_total_C = valores_diametros[10] * somadiametroC;
				total_valores_diametros += valor_total_C;
				$("#totaldiametroC").text('R$ '+valor_total_C);

				var valor_total_D = valores_diametros[14] * somadiametroD;
				total_valores_diametros += valor_total_D;
				$("#totaldiametroD").text('R$ '+valor_total_D);

				var valor_total_E = valores_diametros[18] * somadiametroE;
				total_valores_diametros += valor_total_E;
				$("#totaldiametroE").text('R$ '+valor_total_E);

				var valor_total_F = valores_diametros[20] * somadiametroF;
				total_valores_diametros += valor_total_F;
				$("#totaldiametroF").text('R$ '+valor_total_F);

				var valor_total_G = valores_diametros[21] * somadiametroG;
				total_valores_diametros += valor_total_G;
				$("#totaldiametroG").text('R$ '+valor_total_G);

				var valor_total_H = valores_diametros[22] * somadiametroH;
				total_valores_diametros += valor_total_H;
				$("#totaldiametroH").text('R$ '+valor_total_H);

				var valor_total = data.total + valor_total_km + total_valores_diametros;
				$("#total").text('R$ '+(valor_total));
				$("#bt_s").empty();
				$("#bt_s").append('<a href="#" id="bt_salvar_bm" class="btn btn-primary">salvar</a>');

				
			}
		});
	});	
	
	$("#bt_salvar_bm").click(function(event) {
		$.post('/boletim/salvar', {
			_token  : token,
			idsguia : stringids,
			data_ini : data_ini,
			data_fim : data_fim,
			cliente_id : cliente_id,
			valor_total : valor_total,
			proposta_id : proposta_id,
		}, function(data) {  
			if(data.success){
				alert(data.msg);
				location.href="/ensaio/boletim";	
			}		           
        });
	});

	$("#clienteBM").change(function(event) {
		var id = $(this).val();

		$.post('/boletim/dadosproposta', {
					_token  : token,
					id: id,

				}, function(data) {  

              		items = "<option value=''>Selecione a Proposta</option>"; 

              		$.each(data,function(index,item){
                		items+="<option value='"+index+"'>"+item+"</option>";
              		});

              		$("#propostaBM").html(items); 
        });
	});	

	$('#btboletim').click(function(event) {
	    $(".area_impressao").printArea({
		   	mode       : "iframe",
		   	standard   : "html5",
		   	popTitle   : 'relatorio',
		   	popClose   : false,
		    extraCss   : '/css/impressao_paisagem.css',
	    	extraHead  : '',
	    	retainAttr : ["id","class","style"],
	        printDelay : 900, // tempo de atraso na impressao
	        printAlert : true,
		    printMsg   : 'Aguarde a impressão'
	    });
	});

});// end document