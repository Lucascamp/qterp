$(document).ready(function() {
	
  // $('.flashError').fadeOut(0);
  // $('.flashSuccess').fadeOut(0); 
  var url = window.location.href;
    var pagina = url.split('/');
    if(pagina[3] == 'fonte'){ 
	
	var token = $("#token").val();

  $('.excluirBtn').click(function() {
      $("#fonte_id_excluir").val($(this).attr('data_value'));
      $("#fontedescricao").text($(this).attr('data_value2'));
  });

  $('#limparfontes').click(function(){
      $('#isotopo').val('');
      $('#modelo').val('');
      $('#unidade').val('');
     
      $('#isotopo').trigger("chosen:updated");
      $('#modelo').trigger("chosen:updated");
      $('#unidade').trigger("chosen:updated");

      window.location.href = "/fonte";
    });

  $('.localizacaoFonteBtn').click(function() {
      $("#bunda").val($(this).attr('data_value'));
  });

  $('.historicoFonteBtn').click(function() {

    var fonte_id = $(this).attr('data_value');
    
    console.log(fonte_id);

    $.post('/modalHistoricoFonte', {

      _token   : token,
      fonte_id : fonte_id,

    }, function(data) {
      if(data.success == true){
        var fonte = data.fonte;
        var localizacao = data.localizacao;

        $("#numero_fonte").text(fonte.numero);
        $("#irradiador_fonte").text(data.irradiadores[fonte.irradiador]);

        $("#tablehistorico").empty(); 
        
        var table ='';

        $.each(localizacao,function(index,item)
        { 
          table += '<tr>';

            table += '<td>'+localizacao[index].data+'</td>';
            table += '<td>'+localizacao[index].unidade+'</td>';
            table += '<td>'+localizacao[index].observacoes+'</td>';

          table += '</tr>';
          });
        $("#tablehistorico").append(table);
      }
      else{
         $("#tablehistorico").empty(); 
         var table ='';
         table += '<tr>';
         table += '<td colspan="7"><font color="red" size="2"><center>Não existe histórico para esse fonte</center></font></td>';
         table += '</tr>';
         $("#tablehistorico").append(table);
      }
    });
  }); 

  $('.calculoFonteBtn').click(function() {

      $("#calc").val($(this).attr('data_value'));

      var fonte_id_calc = $("#calc").val();

      $.post('/fonteDados', {

      _token   : token,
      fonte_id_calc : fonte_id_calc,

      }, function(data) {
        if(data.success == true){
          var fonte = data.fonte;

          $("#numero_calculo").text(fonte.numero);
          $("#irradiador_calculo").text(fonte.irradiador);
          $("#atividade_calculo").text(fonte.atividade_inicial);
          $("#data_calculo").text(fonte.data);
          $("#meiavida_calculo").text(fonte.meia_vida);
        }
    });

  });

 $('#calculoAtividadeBtn').click(function() {

    var fonte_calc = $("#calc").val();
    var data = $("#data").val();

    $.post('/fonte/calculo', {

      _token   : token,
      fonte_id : fonte_calc,
      data : data,

    }, function(data) {
      if(data.success == true){
        var carga = data.carga;

        $("#atividade").text(carga);
      }
    });
  }); 

$('#btfolha').click(function(event) {
      $(".area_impressao").printArea({
        mode       : "iframe",
        standard   : "html5",
        popTitle   : 'relatorio',
        popClose   : false,
        extraCss   : '/css/impressao_retrato.css',
        extraHead  : '',
        retainAttr : '',
          printDelay : 900, // tempo de atraso na impressao
          printAlert : true,
        printMsg   : 'Aguarde a impressão'
      });
  });

$("#tableindex").DataTable({
                    "responsive": true,
                    "iDisplayLength": 100,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
                    "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ 14 ] }
                    ],
                });

$(".receberBtn").click(function(event) {
    var id_equip = $(this).prop('id').replace('btrec',''); ;
    $.post('/equipamento/receber_equipamento', {
            _token             : token,
            id                 : id_equip,
        }, function(data) {   
            if(data.success){
                alert(data.mensagem);
                location.href="/fonte";
            }
        }
    );
});
        $('.localizacaoBtn').click(function() {
            $("#equipamento_id").val($(this).attr('data_value'));
            var id = $(this).attr('id').replace('fonte_id','');
            $("#id_fonte").val(id);
        });
    
        $("#bt_salvar_movimentacao").click(function(event) {
            var id_equip =  $("#equipamento_id").val();
            var unidade_destino = $("#unidade_destino").val();
            var projeto = $("#projeto").val();
            var data_movimento = $("#data_movimento").val();
            var funcinario_destino = $("#funcinario_destino").val();
            var motivo = $("#motivo").val();
            var obs = $("#observacao").val();
            var fonte_id = $("#id_fonte").val();

            $.post('/equipamento/movimentar_equipamento', {
                    _token             : token,
                    id                 : id_equip,
                    unidade_destino    : unidade_destino,
                    projeto            : projeto,
                    data_movimento      : data_movimento,
                    funcinario_destino : funcinario_destino,
                    motivo             : motivo,
                    obs                : obs,
                    fonte_id           : fonte_id,
                }, function(data) {   
                    if(data.success){
                        alert(data.mensagem);
                        window.location.reload();
                    }
                }
            );
        });
    }
});// end document