$(document).ready(function($) {
	var token     = $("#token").val();

	listarmodulos();
	atbtdesativar();
	atbtativar();

	$("#bt_salvar_modulo").click(function(event) {
		var descricao = $("#descricao").val();
		var rota = $("#rota").val();
		$.post('/adm/modulos/create', {
			_token : token,
			modulo : descricao,
			rota   :rota,
		}, function(data) {
			$("#mensagens").empty();
			if(data.success){
				$("#mensagens").append('<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+data.mensagem+'</div');
			}else{
				$("#mensagens").append('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+data.mensagem+'</div');
			}
			listarmodulos();
		});
	});
	
	$("#bt_salvar_area").click(function(event){
		var id   = $("#idmodulo").val();
		var area = $("#descricao_area").val();
		var rota = $("#descricao_rota").val();
		$.post('/adm/modulos/salvararea', {
			_token  : token,
			id      : id,
			area    : area,
			rota    : rota
		}, function(data) {
			location.reload();			
		});
	});

	$(".bt_excluir_area").click(function(event){
		var id = $(this).attr('value');
		var res = confirm('Tem certeza que deseja excluir o item?');
		if(res){
			$.post('/adm/modulos/excluirarea', {
				_token  : token,
				id      : id,
			}, function(data) {
				location.reload();		
			});	
		}else{
			alert('Item não excluido!');
		}
	});

	$(".bt_salvar_editar_area").click(function(event){
		var id = $(this).attr('value');
		var area = $("#descricao_area"+id).val();
		$.post('/adm/modulos/alterararea', {
			_token  : token,
			id      : id,
			area    : area
		}, function(data) {
			location.reload();		
		});
	});

	$(".bt_permissoes").click(function(event) {
		var id = $(this).attr('value');
		$("#titulom").empty();
		$("#titulom").text('Permissões');
		listarPermissoes(id)
	});

	$("#bt_salvar_editar_modulo").click(function(event){
		var id     = $("#idmodulo").val();
		var modulo = $("#descricaoEditar").val();
		var status = $("#statusmodulo").val();
		var rota = $("#rotaEditar").val();
		$.post('/adm/modulos/alterarModulo', {
			_token  : token,
			id      : id,
			modulo  : modulo,
			status  : status,
			rota    : rota,
		}, function(data) {
			$("#mensagens").append('<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+data.mensagem+'</div');
		});
	});

	
	
	

	function atbtativar(){
		$("#bt_ativar_modulo").click(function(event){
			var id     = $("#idmodulo").val();
			var modulo = $("#descricaoEditar").val();
			var status = 1;
			var rota   = $("#rotaEditar").val();
			alterarmodulo(id,modulo,status,rota);
			$("#btstatus").empty();
			$("#btstatus").append('<div class="btn btn-danger" id="bt_desativar_modulo">Desativar</div>');
			atbtdesativar();
		});	
	}

	function atbtdesativar(){
		$("#bt_desativar_modulo").click(function(event){
			var id     = $("#idmodulo").val();
			var modulo = $("#descricaoEditar").val();
			var status = 0;
			var rota   = $("#rotaEditar").val();
			alterarmodulo(id,modulo,status,rota);
			$("#btstatus").empty();
			$("#btstatus").append('<div class="btn btn-primary" id="bt_ativar_modulo">Ativar</div>');
			atbtativar();
		});	
	}
	
	function listarmodulos(){
		$.post('/adm/modulos/listarmodulos', {
			_token  : token,
		}, function(data) {
			var modulos = data.modulos;
			$("#listaModulos").empty();
			modulos.forEach(function(element, index){
				link='';
				switch(element.status){
					case '0':
						status = 'Desativado';
						link = '<a href="#" id="lk_ativar_desativar'+element.id+'">Ativar</a>';
						break;
					case '1':
						status = 'Ativado';
						link = '<a href="#" id="lk_ativar_desativar'+element.id+'">Desativar</a>';
						break;
				}
				
				$("#listaModulos").append('<tr>'+
					'<td class="tblabel">'+element.id+'</td>'+
					'<td class="tblabel">'+element.descricao+'</td>'+
					'<td class="tblabel">'+element.created_at+'</td>'+
					'<td class="tblabel">'+status+'</td>'+
					'<td class="tblabel">'+element.rota+'</td>'+
					'<td class="tblabel"><a href="/adm/modulos/editar/'+element.id+'">Editar</a> | '+link+'</td>'+
				'</tr>');	
				$("#lk_ativar_desativar"+element.id).click(function(event) {
					if(element.status == 0){
						alterarmodulo(element.id,element.descricao,1,element.rota);	
					}else{
						alterarmodulo(element.id,element.descricao,0,element.rota);
					}
				});
			});
			
		});
	}

	function alterarmodulo(id,modulo,status,rota){
		$("#statusmodulo").val(status);
		$.post('/adm/modulos/alterarModulo', {
			_token  : token,
			id      : id,
			modulo  : modulo,
			status  : status,
			rota    : rota,
		}, function(data) {
			listarmodulos();
		});
	}

	function listarPermissoes(id){
		$("#bodym").empty();
		$.post('/adm/modulos/listarpermissoes', {
			_token : token,
			id     : id,
		}, function(data) {
			var permissoes = data.permissoes;
			var table ='';
			table+='<table class="table table-bordered tabela">'+
		            '<tr>'+
		                '<td class="tbtitulo" colspan="7">Permissões</td>'+
		            '</tr>';
			permissoes.forEach(function(element, index){
				table+='<tr>'+
						'<td class="tblabel">Descrição</td>'+
                        '<td><input type="text" class="form-control" id="permissao'+element.id+'" value="'+element.descricao+'"></td>'+
                        '<td class="tblabel">Rota</td>'+
                        '<td><input type="text" class="form-control" id="rota_permissao'+element.id+'" value="'+element.rota+'"></td>'+
                        '<td class="tblabel">'+
                           ' <div class="btn btn-primary bt_salvar_editar_permissao"  value="'+element.id+'" >Salvar</div>'+
                            '<div class="btn btn-danger bt_excluir_permissao"  value="'+element.id+'">Excluir</div>'+
                        '</td>'+
                        '</tr>';
			});
			table+='<tr><td class=" tblabel">Descrição</td>'+
	                '<td class=""><input type="text" class="form-control" id="descricao_permissao"></td>'+
	                '<td class="tblabel ">Rota</td>'+
	                '<td class=""> <input type="text" class="form-control" id="descricao_rota_permissao"></td>'+
	                '<td class="tblabel "><div class="btn btn-primary" value="'+id+'" id="bt_salvar_permissao">Inserir</div></td></tr></table>';
	        $("#bodym").append(table);

			$("#bt_salvar_permissao").click(function(event) {
				var id = $(this).attr('value');
				salvarpermissao(id);
			});
			$(".bt_salvar_editar_permissao").click(function(event) {
				var id = $(this).attr('value');
				console.log(id);
				alterarpermissao(id);
			});
			$(".bt_excluir_permissao").click(function(event) {
				var id = $(this).attr('value');
				var res = confirm('Tem certeza que deseja excluir o item?');
				if(res){
					$.post('/adm/modulos/excluirpermissao', {
						_token  : token,
						id      : id,
					}, function(data) {
						alert(data.mensagem);
						location.reload();		
					});	
				}else{
					alert('Item não excluido!');
				}
			});
			$("#footerm").empty();
			$('#myModal').modal('toggle');
		});
	}
	function salvarpermissao(id){
		var descricao = $("#descricao_permissao").val();
		var rota =  $("#descricao_rota_permissao").val();
		$.post('/adm/modulos/salvarpermissao', {
			_token    : token,
			id        : id,
			permissao : descricao,
			rota      : rota
		}, function(data) {
			alert(data.mensagem);
			location.reload();
		});
	}
	function alterarpermissao(id){
		console.log(id);
		var descricao = $("#permissao"+id).val();
		var rota =  $("#rota_permissao"+id).val();
		console.log(rota);
		$.post('/adm/modulos/alterarpermissao', {
			_token    : token,
			id        : id,
			permissao : descricao,
			rota      : rota
		}, function(data) {
			alert(data.mensagem);
			location.reload();
		});
	}

});