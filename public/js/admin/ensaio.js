$(document).ready(function($) {
	var token     = $("#token").val();

	$("#bt_salva_jornada").click(function(event) {
		var descricao = $("#descricao").val();
		$.post('/adm/ensaio/jornada/cadastrar', {
			_token : token,
			jornada : descricao,
		}, function(data) {
			alert(data.mensagem);
			location.reload();	
		});
	});
	$(".bt_alterar_jornada").click(function(event){
		var id = $(this).attr('value');
		var jornada = $("#descricao_jornada"+id).val();
		$.post('/adm/ensaio/jornada/alterar', {
			_token  : token,
			id      : id,
			jornada : jornada,
		}, function(data) {
			alert(data.mensagem);
			location.reload();		
		});
	});
	$(".bt_excluir_jornada").click(function(event){
		var id = $(this).attr('value');
		var res = confirm('Tem certeza que deseja excluir o item?');
		if(res){
			$.post('/adm/ensaio/jornada/excluir', {
				_token  : token,
				id      : id,
			}, function(data) {
				alert(data.mensagem);
				location.reload();		
			});	
		}else{
			alert('Item não excluido!');
		}
	});



	$("#bt_salva_filme").click(function(event) {
		var descricao = $("#descricao").val();
		var classe = $("#classe").val();
		$.post('/adm/ensaio/filmes/cadastrar', {
			_token : token,
			filme : descricao,
			classe   :classe,
		}, function(data) {
			alert(data.mensagem);
			location.reload();	
		});
	});
	$(".bt_alterar_filme").click(function(event){
		var id = $(this).attr('value');
		var filme = $("#descricao_filme"+id).val();
		var classe 	= $("#classe"+id).val();
		$.post('/adm/ensaio/filmes/alterar', {
			_token  : token,
			id      : id,
			filme   : filme,
			classe  : classe, 
		}, function(data) {
			alert(data.mensagem);
			location.reload();		
		});
	});
	$(".bt_excluir_filme").click(function(event){
		var id = $(this).attr('value');
		var res = confirm('Tem certeza que deseja excluir o item?');
		if(res){
			$.post('/adm/ensaio/filmes/excluir', {
				_token  : token,
				id      : id,
			}, function(data) {
				alert(data.mensagem);
				location.reload();		
			});	
		}else{
			alert('Item não excluido!');
		}
	});



	$("#bt_salva_junta").click(function(event) {
		var descricao = $("#descricao").val();
		$.post('/adm/ensaio/juntas/cadastrar', {
			_token : token,
			junta : descricao,
		}, function(data) {
			alert(data.mensagem);
			location.reload();	
		});
	});

	$(".bt_alterar_junta").click(function(event){
		var id = $(this).attr('value');
		var junta = $("#descricao_junta"+id).val();
		$.post('/adm/ensaio/juntas/alterar', {
			_token  : token,
			id      : id,
			junta   : junta,
		}, function(data) {
			alert(data.mensagem);
			location.reload();		
		});
	});

	$(".bt_excluir_junta").click(function(event){
		var id = $(this).attr('value');
		var res = confirm('Tem certeza que deseja excluir o item?');
		if(res){
			$.post('/adm/ensaio/juntas/excluir', {
				_token  : token,
				id      : id,
			}, function(data) {
				alert(data.mensagem);
				location.reload();		
			});	
		}else{
			alert('Item não excluido!');
		}
	});
	


	$("#bt_salva_servico").click(function(event) {
		var descricao = $("#descricao").val();
		$.post('/adm/ensaio/servicos/cadastrar', {
			_token : token,
			servico : descricao,
		}, function(data) {
			alert(data.mensagem);
			location.reload();	
		});
	});

	$(".bt_alterar_servico").click(function(event){
		var id = $(this).attr('value');
		var servico = $("#descricao_servico"+id).val();
		$.post('/adm/ensaio/servicos/alterar', {
			_token  : token,
			id      : id,
			servico   : servico,
		}, function(data) {
			alert(data.mensagem);
			location.reload();		
		});
	});

	$(".bt_excluir_servico").click(function(event){
		var id = $(this).attr('value');
		var res = confirm('Tem certeza que deseja excluir o item?');
		if(res){
			$.post('/adm/ensaio/servicos/excluir', {
				_token  : token,
				id      : id,
			}, function(data) {
				alert(data.mensagem);
				location.reload();		
			});	
		}else{
			alert('Item não excluido!');
		}
	});

	$("#bt_salva_descontinuidade").click(function(event) {
		var descricao = $("#descricao").val();
		$.post('/adm/ensaio/descontinuidades/cadastrar', {
			_token : token,
			descontinuidade : descricao,
		}, function(data) {
			alert(data.mensagem);
			location.reload();	
		});
	});

	$(".bt_alterar_descontinuidade").click(function(event){
		var id = $(this).attr('value');
		var descontinuidade = $("#descricao_descontinuidade"+id).val();
		$.post('/adm/ensaio/descontinuidades/alterar', {
			_token  : token,
			id      : id,
			descontinuidade   : descontinuidade,
		}, function(data) {
			alert(data.mensagem);
			location.reload();		
		});
	});

	$(".bt_excluir_descontinuidade").click(function(event){
		var id = $(this).attr('value');
		var res = confirm('Tem certeza que deseja excluir o item?');
		if(res){
			$.post('/adm/ensaio/descontinuidades/excluir', {
				_token  : token,
				id      : id,
			}, function(data) {
				alert(data.mensagem);
				location.reload();		
			});	
		}else{
			alert('Item não excluido!');
		}
	});
});