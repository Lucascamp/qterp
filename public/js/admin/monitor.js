$(document).ready(function($) {
	var token = $("#token").val();	
	verificaStatus();
	setInterval(verificaStatus, 10000);
	verificaBackup();
	verificaCriarDump();
	verificaCriarBackup();
	verificaSincGama();
	verificaSincDados();
	varificaerros();

	

	function verificaStatus(){
		$.post('/adm/monitor/verificastatus', {
	    	_token          : token,
	    }, function(data) {
	    	if(data.success){
	    		$.each(data.arraystatus,function(index,item){
	    			if(item == 1){
			    		$('#server'+index).removeClass('text-danger');
			    		$('#server'+index).addClass('text-navy');
			    		$('#server'+index).empty();
			    		$('#server'+index).append('<i class="fa fa-play fa-rotate-270"></i> ON');
			    	}
			    	if(item == 0){
			    		$('#server'+index).removeClass('text-navy');
			    		$('#server'+index).addClass('text-danger');
			    		$('#server'+index).empty();
			    		$('#server'+index).append('<i class="fa fa-play fa-rotate-90"></i> OFF');
			    	}
	    		});
	    	}
	    	
	    });
	}

	function verificaBackup(){
		$.post('/adm/monitor/verificabackup', {
	    	_token          : token,
	    }, function(data) {
	    	if(data.success){
	    		$.each(data.res,function(index,item){
	    			$("#bu_"+index).empty();
	    			$("#bu_"+index).append(index+' - '+item);
	    			var t = new Date();
	    			var t2 = new Date(item);
					t = t.getDate();
					t2 = t2.getDate();
					console.log(t2);
	    			if(t != t2){
	    				$("#bu_"+index).addClass('text-danger');
	    				$("#bu_c_"+index).addClass('text-danger');
	    			}else{
	    				$("#bu_"+index).addClass('text-navy');
	    				$("#bu_c_"+index).addClass('text-navy');
	    			}
	    		});	
	    	}
	    });
	}

	function verificaCriarDump(){
		$.post('/adm/monitor/verificacriardump', {
	    	_token          : token,
	    }, function(data) {
	    	if(data.success){
	    		var logs = [];
	    		$.each(data.logs,function(index,item){
	    			logs.push(item['tempo_total']);
	    		});
	    		$("#sparkline1").sparkline(logs, {
			        type: 'line',
			        lineColor: '#17997f',
			        fillColor: false,
			        width : 200,
			        height : 100,
			    });
	    	}
	    });
	}
	
	function verificaCriarBackup(){
		$.post('/adm/monitor/verificacriarbackup', {
	    	_token          : token,
	    }, function(data) {
	    	if(data.success){
	    		var logs = [];
	    		$.each(data.logs,function(index,item){
	    			logs.push(item['tempo_total']);
	    		});
	    		$("#sparkline2").sparkline(logs, {
			        type: 'line',
			        lineColor: '#17997f',
			        fillColor: false,
			        width : 200,
			        height : 100,
			    });
	    	}
	    });
	}

	function verificaSincGama(){
		$.post('/adm/monitor/verificasincgama', {
	    	_token          : token,
	    }, function(data) {
	    	if(data.success){
	    		var logs = [];
	    		$.each(data.logs,function(index,item){
	    			logs.push(item['tempo_total']);
	    		});
	    		$("#sparkline3").sparkline(logs, {
			        type: 'line',
			        lineColor: '#17997f',
			        fillColor: false,
			        width : 200,
			        height : 100,
			    });
	    	}
	    });
	}

	function verificaSincDados(){
		$.post('/adm/monitor/verificasincdados', {
	    	_token          : token,
	    }, function(data) {
	    	if(data.success){
	    		var logs = [];
	    		$.each(data.logs,function(index,item){
	    			logs.push(item['tempo_total']);
	    		});
	    		$("#sparkline4").sparkline(logs, {
			        type: 'line',
			        lineColor: '#17997f',
			        fillColor: false,
			        width : 200,
			        height : 100,
			    });
	    	}
	    });
	}

	function varificaerros(){
		$.post('/adm/monitor/verificaerros', {
		    _token          : token,
		    }, function(data) {
		    	if(data.success){
	    			toastr.options = {
			            closeButton: true,
			            progressBar: true,
			            showMethod: 'slideDown',
			            timeOut: 16000
			        };
		        	$('#tbodyerros').empty();
		        	var table ='';
	    			$.each(data.logs,function(ind,item){
		    			$.each(item,function(index,item){
		    				toastr.error(item['unidade']+' - '+item['datam']+' - '+ind);
		    				table += '<tr class="text-danger"><td><i class="fa fa-circle text-danger"></i></td><td><span>'+item['unidade']+' - '+item['datam']+' - '+ind+'</span></td></tr>';
		    			});
	    			});
	    			$('#tbodyerros').append(table);
	    		}
	    	}
	    );
	}
	

});