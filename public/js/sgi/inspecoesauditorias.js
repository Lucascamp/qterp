$(document).ready(function() {
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	var token           = $("#token").val();
	var contadorlinha   = 1;
	var array_itens     = [];
	var array_respostas = {};
	var array_notas     = {};

	var id_unidade_formulario_preenchido = $("#filtro_unidade").val();
	var data_ini_formulario_preenchido = 0;
	var data_fim_formulario_preenchido = 0;

	var id_unidade_marcacao = $("#filtro_unidade_marcacao").val();
	var data_ini_marcacao = 0;
	var data_fim_marcacao = 0;

	$(".btnviewformulario").click(function(event) {
		var id = $(this).attr('id').replace('btnview','');
		location.href="/sgi/formulario/"+id;
	});

	$('#modalNovoFormulario').on('shown.bs.modal', function (e) {
		$("#itens_novo_formulario").empty();
		$("#descricao_formu").val('');

		contadorlinha = 1;
		$("#itens_novo_formulario").append('<tr class="linhaItemNF" id="linha'+contadorlinha+'">'+
				'<td><div class="numero_item">'+contadorlinha+'</div></td>'+
				'<td><input type="text" class="insp_veri form-control"/></td>'+
				'<td><input type="text" class="carac form-control"/></td>'+
				'<td><input type="text" class="doc_evid form-control"/></td>'+
				'<td><button type="button" class="btn btn-danger bt_remove_item" id="bt_remove_'+contadorlinha+'"><span aria-hidden="true"><i class="fa fa-close"></i></span></button></td>'+
			'</tr>');
	})

	$("#bt_salvar_formulario").click(function(event) {
		salvarFormulario();
		$("#modalNovoFormulario").modal('toggle');
	});

	$(".btneditarformulario").click(function(event) {
		var id = $(this).attr('id').replace('btnedt','');
		carregarFormularioEdt(id);
	});


	$("#bt_add_item").click(function(event) {
		contadorlinha += 1;
		$("#bt_remove_1").removeAttr('disabled');
		$("#itens_novo_formulario").append('<tr class="linhaItemNF" id="linha'+contadorlinha+'">'+
				'<td><div class="numero_item">'+contadorlinha+'</div></td>'+
				'<td><input type="text" class="insp_veri form-control"/></td>'+
				'<td><input type="text" class="carac form-control"/></td>'+
				'<td><input type="text" class="doc_evid form-control"/></td>'+
				'<td><button type="button" class="btn btn-danger bt_remove_item" id="bt_remove_'+contadorlinha+'"><span aria-hidden="true"><i class="fa fa-close"></i></span></button></td>'+
			'</tr>');
		$(".bt_remove_item").off();
		$(".bt_remove_item").click(function(event) {
			var id_linha = $(this).attr('id').replace('bt_remove_','');
			$("#linha"+id_linha).remove();
			contadorlinha -= 1;
			reordenarLinhas();
		});
	});

	$('#modaleditarFormulario').on('hidden.bs.modal', function (e) {
		$("#bt_add_item_edt").off();
	});

	$(".na").click(function(event) {
		var id = $(this).attr('id').replace('na','');

			$("#nc"+id).empty();
			$("#nc"+id).removeClass('marcado0');
			$("#nc"+id).removeClass('marcado1');
			$("#nc"+id).addClass('desmarcado');

			$("#c"+id).empty();
			$("#c"+id).removeClass('marcado2');
			$("#c"+id).addClass('desmarcado');

		if($(this).hasClass("marcado")){
			$("#na"+id).empty();
			$("#na"+id).removeClass('marcado');
			$("#na"+id).addClass('desmarcado');
		}else if($(this).hasClass("desmarcado")){
			$("#na"+id).append('<span>X</span>')
			$("#na"+id).removeClass('desmarcado');
			$("#na"+id).addClass('marcado');
		}
	});

	$(".nc").click(function(event) {
		var id = $(this).attr('id').replace('nc','');

			$("#na"+id).empty();
			$("#na"+id).removeClass('marcado');
			$("#na"+id).addClass('desmarcado');

			$("#c"+id).empty();
			$("#c"+id).removeClass('marcado2');
			$("#c"+id).addClass('desmarcado');

		if($(this).hasClass("desmarcado")){
			$("#nc"+id).empty();
			$("#nc"+id).removeClass('desmarcado');
			$("#nc"+id).addClass('marcado0');
			$("#nc"+id).append('<span>0</span>')
		}else if($(this).hasClass("marcado0")){
			$("#nc"+id).empty();
			$("#nc"+id).removeClass('marcado0');
			$("#nc"+id).addClass('marcado1');
			$("#nc"+id).append('<span>1</span>')
		}else if($(this).hasClass("marcado1")){
			$("#nc"+id).empty();
			$("#nc"+id).removeClass('marcado1');
			$("#nc"+id).addClass('desmarcado');
		}
	});

	$(".c").click(function(event) {
		var id = $(this).attr('id').replace('c','');
			
			$("#nc"+id).empty();
			$("#nc"+id).removeClass('marcado0');
			$("#nc"+id).removeClass('marcado1');
			$("#nc"+id).addClass('desmarcado');

			$("#na"+id).empty();
			$("#na"+id).removeClass('marcado');
			$("#na"+id).addClass('desmarcado');

		if($(this).hasClass("desmarcado")){
			$("#c"+id).empty();
			$("#c"+id).removeClass('desmarcado');
			$("#c"+id).addClass('marcado2');
			$("#c"+id).append('<span>2</span>')
		} else if($(this).hasClass("marcado2")){
			$("#c"+id).empty();
			$("#c"+id).removeClass('marcado2');
			$("#c"+id).addClass('desmarcado');
		}
	});

	$("#bt_salvar_formulario_unidade").click(function(event) {
		salvarFormularioUnidade();
	});
	
	$("#filtro_unidade").change(function(event) {
		id_unidade_formulario_preenchido = $(this).val();
		carregar_tabela_formularios_preenchidos(id_unidade_formulario_preenchido,data_ini_formulario_preenchido,data_fim_formulario_preenchido);
	});

	$("#data_ini").change(function(event) {
		data_ini_formulario_preenchido = $(this).val();
		carregar_tabela_formularios_preenchidos(id_unidade_formulario_preenchido,data_ini_formulario_preenchido,data_fim_formulario_preenchido);
	});

	$("#data_fim").change(function(event) {
		data_fim_formulario_preenchido = $(this).val();
		carregar_tabela_formularios_preenchidos(id_unidade_formulario_preenchido,data_ini_formulario_preenchido,data_fim_formulario_preenchido);
	});

	$(".btnexcluirformulario").click(function(event) {
		var id = $(this).attr('id').replace('btnexc','');
		excluir_formulario(id);
	});

	$("#bt_salvar_marcacao_insp_aud").click(function(event) {
		salvar_marcacao();
	});


	$(".btneditarmarcacao").click(function(event) {
		var id = $(this).attr('id').replace('btnedtmarcacao','');
		carregar_editar_marcacao(id);
	});

	$("#bt_salvar_marcacao_insp_aud_edt").click(function(event) {
		salvar_marcacao_edt();
	});

	$(".btnexcluirmarcacao").click(function(event) {
		var id = $(this).attr('id').replace('btnexcluirmarcacao','');
		excluir_marcacao(id);
	});

	$("#filtro_unidade_marcacao").change(function(event) {
		id_unidade_marcacao = $(this).val();
		carregar_tabela_marcacoes(id_unidade_marcacao,data_ini_marcacao,data_fim_marcacao);
	});

	$("#data_ini_marcacao").change(function(event) {
		data_ini_marcacao = $(this).val();
		carregar_tabela_marcacoes(id_unidade_marcacao,data_ini_marcacao,data_fim_marcacao);
	});

	$("#data_fim_marcacao").change(function(event) {
		data_fim_marcacao = $(this).val();
		carregar_tabela_marcacoes(id_unidade_marcacao,data_ini_marcacao,data_fim_marcacao);
	});


	function pegarItens(){
		array_itens = [];
		for (var i = 1 ; i <= contadorlinha; i++) {
			console.log('i = '+i);
			var item            = {};
			item['numero_item'] = $("#linha"+i+" .numero_item").html();
			console.log(item['numero_item'] );
			item['insp_veri']   = $("#linha"+i+" .insp_veri").val();
			item['carac']       = $("#linha"+i+" .carac").val();
			item['doc_evid']    = $("#linha"+i+" .doc_evid").val();
			console.log('asd');
			console.log(item);
			array_itens.push(item);
		};
	}

	function salvarFormulario(){
		pegarItens();
		var id_form = $("#id_formulario").val();
		var tipo = '';
		var descricao ='';
		var itens = array_itens;
		if(id_form == ''){
			tipo = $("#tipo").val();
			descricao = $("#descricao_formu").val();
		}else{
			descricao = $("#descricaoedt").val();
		}
		console.log(itens);
		
		$.post('/sgi/inspecoesauditorias/salvar_formulario', {
			_token      : token,
			tipo        : tipo,
			descricao   : descricao,
			id_form     : id_form,
			itens       : itens,
			}, function(data) {  
				if(data.success){
					// $('#lnk-menu').off();
					alert('OK');
					location.href="/sgi/inspecoesauditorias";
		       	}
	    	}
	    );

	}

	function carregarFormularioEdt(id){
		var id_form = id;
		$("#itens_novo_formulario").empty();
		$.post('/sgi/inspecoesauditorias/carregar_formulario_edt', {
			_token    : token,
			id_form      : id_form,
			}, function(data) {  
				if(data.success){
					if(data.itens) var itens = data.itens; else var itens = 0;

					$("#tipoedt").html(data.formulario.tipo);
					$("#descricaoedt").val(data.formulario.descricao);
					$("#id_formulario").val(data.formulario.id);

					if(itens != 0){
						$("#itens_editar_formulario").empty();
						$.each(itens,function(index,el){
							console.log('numero = ');
							console.log(el['numero']);

							$("#itens_editar_formulario").append('<tr class="linhaItemNF" id="linha'+parseInt(el['numero'])+'">'+
									'<td><div class="numero_item">'+parseInt(el['numero'])+'</div></td>'+
									'<td><input type="text" class="insp_veri form-control" value="'+el['inspecao_verificacao']+'"/></td>'+
									'<td><input type="text" class="carac form-control" value="'+el['caracterizacao']+'"/></td>'+
									'<td><input type="text" class="doc_evid form-control" value="'+el['documentos_evidenciais']+'"/></td>'+
									'<td><button type="button" class="btn btn-danger bt_remove_item" id="bt_remove_'+parseInt(el['numero'])+'"><span aria-hidden="true"><i class="fa fa-close"></i></span></button></td>'+
								'</tr>');
							contadorlinha = parseInt(el['numero']);
						});
						$(".bt_remove_item").click(function(event) {
							var id_linha = $(this).attr('id').replace('bt_remove_','');
							$("#linha"+id_linha).remove();
							contadorlinha -= 1;
							reordenarLinhas();
						});

					}

					$("#modaleditarFormulario").modal('toggle');

					$("#bt_add_item_edt").click(function(event) {
						contadorlinha += 1;						
						$("#itens_editar_formulario").append('<tr class="linhaItemNF" id="linha'+contadorlinha+'">'+
								'<td><div class="numero_item">'+contadorlinha+'</div></td>'+
								'<td><input type="text" class="insp_veri form-control"/></td>'+
								'<td><input type="text" class="carac form-control"/></td>'+
								'<td><input type="text" class="doc_evid form-control"/></td>'+
								'<td><button type="button" class="btn btn-danger bt_remove_item" id="bt_remove_'+contadorlinha+'"><span aria-hidden="true"><i class="fa fa-close"></i></span></button></td>'+
							'</tr>');
						$(".bt_remove_item").off();
						$(".bt_remove_item").click(function(event) {
							var id_linha = $(this).attr('id').replace('bt_remove_','');
							$("#linha"+id_linha).remove();
							contadorlinha -= 1;
						});
					});
					$("#bt_salvar_formulario_edt").off();
					$("#bt_salvar_formulario_edt").click(function(event) {
						salvarFormulario();
						$("#modaleditarFormulario").modal('toggle');
					});		
		       	}
	    	}
	    );
		
	}

	function salvarFormularioUnidade(){
		var validacao = true;
		var array_mensagens = [];
		var mensagem  = '';

		pegarRespostas();
		var resp = array_respostas;

		var id_form = $("#id_formulario").val();
		var projeto = $("#projeto").val();
		var local = $("#local").val();
		var responsavel = $("#responsavel").val();
		var equipe = $("#equipe").val();
		var verificado_por = $("#verificado_por").val();
		var data = $("#data").val();
		var documentos_referencia = $("#documentos_referencia").val();
		var unidade = $("#unidade_insp_aud").val();
		var id_marcacao = $("#id_marcacao").val();

		if(projeto.length <= 0){
			array_mensagens.push('-Projeto é um campo obrigatorio;');
			validacao = false;
		}
		if(local.length <= 0){
			array_mensagens.push('-Local é um campo obrigatorio;');
			validacao = false;
		}
		if(responsavel.length <= 0){
			array_mensagens.push('-Responsavel é um campo obrigatorio;');
			validacao = false;
		}
		if(equipe.length <= 0){
			array_mensagens.push('-Equipe é um campo obrigatorio;');
			validacao = false;
		}
		if( verificado_por.length <= 0){
			array_mensagens.push('-Verificado por é um campo obrigatorio;');
			validacao = false;
		}
		if(unidade == 0){
			array_mensagens.push('-Unidade é um campo obrigatorio;');
			validacao = false;
		}
		if(data.length <= 0){
			array_mensagens.push('-Data é um campo obrigatorio;');
			validacao = false;
		}

		$.each($(".linhaItem"),function(index,el){
			var id = $(el).prop('id').replace('linhaItem','');

			if(!$("#na"+id).hasClass("marcado") && !$("#nc"+id).hasClass("marcado0") && !$("#nc"+id).hasClass("marcado1") && !$("#c"+id).hasClass("marcado2") ){
				array_mensagens.push('-Item '+id+' esta sem resposta;');
				validacao = false;
			}

		});


		if(array_mensagens.length > 0){
			array_mensagens.forEach(function(ele){
				mensagem += ele+'\n';
			})
		}

		if(validacao){
			$.post('/sgi/inspecoesauditorias/salvar_formulario_unidade', {
				_token                : token,
				id_form               : id_form,
				projeto               : projeto,
				local                 : local,
				responsavel           : responsavel,
				equipe                : equipe,
				verificado_por        : verificado_por,
				data                  : data,
				documentos_referencia : documentos_referencia,
				unidade               : unidade,
				respostas             : resp,
				notas                 : array_notas,
				id_marcacao           : id_marcacao,

				}, function(data) {  
					if(data.success){
						alert('Formulario Preenchido com sucesso!');
						location.href="/sgi/inspecoesauditorias";
			       	}
		    	}
		    );
		}else{
			alert(mensagem);
		}
	}

	function pegarRespostas(){
		array_respostas = {};
		array_notas = [];

		array_na = [];
		$.each($(".na"),function(index,el){
			if($(el).hasClass("marcado")){
				var id = $(el).prop('id').replace('na','');
				array_na.push(id);
			}
		});

		array_nc = [];
		$.each($(".nc"),function(index,el){
			var nc = {};
			var id = $(el).prop('id').replace('nc','');
			if($(el).hasClass("marcado0")){
				nc[id] = '0';
			}
			if($(el).hasClass("marcado1")){
				nc[id] = '1';
			}
			array_nc.push(nc);
		});

		array_c = [];
		$.each($(".c"),function(index,el){
			if($(el).hasClass("marcado2")){
				var id = $(el).prop('id').replace('c','');
				array_c.push(id);
			}
		});

		array_respostas['na'] = array_na;
		array_respostas['nc'] = array_nc;
		array_respostas['c'] = array_c;

		$.each($(".notas"),function(index,el){
			var id = $(el).prop('id').replace('nota','');
			array_notas[id] = $("#nota"+id).val();
		});
	};

	function carregar_tabela_formularios_preenchidos(unidade,data_ini,data_fim){

		$.post('/sgi/inspecoesauditorias/carregar_tabela_preenchido', {
			_token                : token,
			unidade               : unidade,
			data_ini              : data_ini,
			data_fim              : data_fim,
			}, function(data) {  
				if(data.success){
					$("#tabela_formularios_preenchidos").empty();
					if(data.insp_aud.length > 0){
						$.each(data.insp_aud,function(index, el) {
							var aux_data = el.data.substring(0,10);
							aux_data = aux_data.split('-');
							aux_data = aux_data[2]+'-'+aux_data[1]+'-'+aux_data[0];

							$("#tabela_formularios_preenchidos").append('<tr><td>'+data.unidades[el.unidade]+
								'</td><td>'+data.insp_aud_nomes[el.insp_aud_id]+'</td><td>'+aux_data+
								'</td><td><a href="/sgi/formulariop/'+el.id+'" class="btn btn-info" title="VISUALIZAR"><i class="fa fa-eye"></i></a></td></tr>');	
						});	
					}else{
						$("#tabela_formularios_preenchidos").append('<tr><td colspan="4">Nenhum registro encontrado</td></tr>');	
					}
		       	}
	    	}
	    );
	}

	function reordenarLinhas(){
		var novo_numero = 1;
		$.each($('.linhaItemNF'),function(index,el){
			var id = $(el).prop('id').replace('linha','');
			console.log(novo_numero);
			
			$('#linha'+id+' .numero_item').text(novo_numero);
			$('#linha'+id).attr('id','linha'+novo_numero);
			$('#bt_remove_'+id).attr('id', 'bt_remove_'+novo_numero);
			contadorlinha = novo_numero;
			novo_numero ++;
		});		
	}

	function excluir_formulario(id){
		$.post('/sgi/inspecoesauditorias/excluir_formulario', {
			_token      : token,
			id          : id,
			}, function(data) {  
				if(data.success){
					alert('OK');
					location.href="/sgi/inspecoesauditorias";
		       	}
	    	}
	    );
	}

	function salvar_marcacao(){
		var unidade     = $("#filtro_unidade_marcacao_nova").val();
		var insp_aud    = $("#filtro_insp_aud_marcacao_nova").val();
		var responsavel = $("#filtro_funcionario_marcacao_nova").val();
		var data_ini    = $("#data_ini_marcacao_nova").val();
		var data_fim    = $("#data_fim_marcacao_nova").val();
		$("#modalNovaMarcacao").modal('toggle');
		$.post('/sgi/inspecoesauditorias/salvar_marcacao', {
			_token      : token,
			unidade     : unidade,
			insp_aud    : insp_aud,
			responsavel : responsavel,
			data_ini    : data_ini,
			data_fim    : data_fim,
			}, function(data) {  
				if(data.success){
					alert('Marcação Salva com sucesso');
					location.href="/sgi/inspecoesauditorias/calendario";
		       	}
	    	}
	    );
	}

	function carregar_editar_marcacao(id){
		$.post('/sgi/inspecoesauditorias/carregar_marcacao', {
			_token      : token,
			id          : id,
			}, function(data) {  
				if(data.success){
					
					$("#filtro_unidade_marcacao_edt").val(data.marcacao.unidade_id);
					$('#filtro_unidade_marcacao_edt').trigger("chosen:updated");

					$("#filtro_insp_aud_marcacao_edt").val(data.marcacao.insp_aud_id);
					$('#filtro_insp_aud_marcacao_edt').trigger("chosen:updated");

					$("#filtro_funcionario_marcacao_edt").val(data.marcacao.responsavel_id);
					$('#filtro_funcionario_marcacao_edt').trigger("chosen:updated");

					$("#data_ini_marcacao_edt").val(data.marcacao.data_ini.substr(0,10));
					$("#data_fim_marcacao_edt").val(data.marcacao.data_fim.substr(0,10));

					$("#id_marcacao").val(data.id_marcacao);

					$("#modalEditarMarcacao").modal('toggle');
		       	}
	    	}
	    );
	}

	function salvar_marcacao_edt(){
		var id_marcacao = $("#id_marcacao").val();
		var unidade     = $("#filtro_unidade_marcacao_edt").val();
		var insp_aud    = $("#filtro_insp_aud_marcacao_edt").val();
		var responsavel = $("#filtro_funcionario_marcacao_edt").val();
		var data_ini    = $("#data_ini_marcacao_edt").val();
		var data_fim    = $("#data_fim_marcacao_edt").val();
		$("#modalEditarMarcacao").modal('toggle');
		$.post('/sgi/inspecoesauditorias/salvar_marcacao', {
			_token      : token,
			id_marcacao : id_marcacao,
			unidade     : unidade,
			insp_aud    : insp_aud,
			responsavel : responsavel,
			data_ini    : data_ini,
			data_fim    : data_fim,
			}, function(data) {  
				if(data.success){
					alert('Marcação editada com sucesso');
					location.href="/sgi/inspecoesauditorias/calendario";
		       	}
	    	}
	    );
	}

	function excluir_marcacao(id){
		$.post('/sgi/inspecoesauditorias/excluir_marcacao', {
			_token      : token,
			id          : id,
			}, function(data) {  
				if(data.success){
					alert('OK');
					location.href="/sgi/inspecoesauditorias/calendario";
		       	}
	    	}
	    );
	}

	function carregar_tabela_marcacoes(unidade,data_ini,data_fim){

		$.post('/sgi/inspecoesauditorias/carregar_tabela_marcacao', {
			_token                : token,
			unidade               : unidade,
			data_ini              : data_ini,
			data_fim              : data_fim,
			}, function(data) {  
				if(data.success){
					$("#tabela_marcacoes").empty();
					if(data.insp_aud.length > 0){
						$.each(data.insp_aud,function(index, el) {
							var data_ini = el.data_ini.substr(0,10);
							var data_fim = el.data_fim.substr(0,10);
							data_ini = data_ini.split('-');
							data_ini = data_ini[2]+'-'+data_ini[1]+'-'+data_ini[0];

							data_fim = data_fim.split('-');
							data_fim = data_fim[2]+'-'+data_fim[1]+'-'+data_fim[0];
							
							var status = '';
							var botoes = '';

							if(el.status == 0 ){
								status = 'Em dia';
								botoes = '<a href="/sgi/formulario/'+el.insp_aud_id+'/'+el.id+'" class="btn btn-info" title="PREENCHER"><i class="fa fa-file-text-o"></i></a>';
							}
							if(el.status == 1){
								status = '<font color ="red">Atrasado</font>';
								botoes = '<a href="/sgi/formulario/'+el.insp_aud_id+'/'+el.id+'" class="btn btn-info" title="PREENCHER"><i class="fa fa-file-text-o"></i></a>';
							}
							if(el.status == 2){
								status = '<font color ="green">Realizado</font>';
								botoes = '<a href="/sgi/formulariop/'+data.formularios_marcacao[el.id]+'" class="btn btn-info" title="VISUALIZAR"><i class="fa fa-eye"></i></a>';
							}

							var editar = '';

							if(el.status != 2){
								if(data.permi_sgi){
									editar = ' <button type="button" class="btn btn-success btneditarmarcacao" id="btnedtmarcacao'+el.id+'" title="EDITAR"><i class="fa fa-pencil"></i></button>'+
	                                         ' <button type="button" class="btn btn-danger btnexcluirmarcacao" id="btnexcluirmarcacao'+el.id+'" title="EXCLUIR" ><i class=" fa fa-trash-o"></i></button>';
								}
							} 

							

							$("#tabela_marcacoes").append('<tr>'+
								'<td>'+data.unidades[el.unidade_id]+'</td>'+
								'<td>'+data.funcionarios[el.responsavel_id]+'</td>'+
								'<td>'+data.insp_aud_nomes[el.insp_aud_id]+'</td>'+
								'<td>'+data_ini+' a '+data_fim+'</td>'+
								'<td>'+status+'</td>'+
								'<td>'+botoes+editar+'</td>'+
							'</tr>');	
						});	
						$(".btneditarmarcacao").click(function(event) {
							var id = $(this).attr('id').replace('btnedtmarcacao','');
							carregar_editar_marcacao(id);
						});
						$(".btnexcluirmarcacao").click(function(event) {
							var id = $(this).attr('id').replace('btnexcluirmarcacao','');
							excluir_marcacao(id);
						});
					}else{
						$("#tabela_marcacoes").append('<tr><td colspan="7">Nenhum registro encontrado</td></tr>');	
					}
		       	}
	    	}
	    );
	}

});// end document


