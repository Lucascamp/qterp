$(document).ready(function() {
	
  // $('.flashError').fadeOut(0);
  // $('.flashSuccess').fadeOut(0);  
	
	var token = $("#token").val();

  $("#descricao").change(function(event) {
    var descricao = $(this).val();

    $.post('/dadosDescricao', {
          _token  : token,
          descricao: descricao,

        }, function(data) {  
            console.log(data);

                  var serial = data.serial;
                    items = "<option value=''>Serial</option>"; 
                    $.each(serial,function(index,item){
                      items+="<option value='"+item+"'>"+item+"</option>";
                    });

                  $("#serial").html(items); 

                  $('#serial').trigger("chosen:updated");

                  var patrimonio = data.patrimonio;
                    items2 = "<option value=''>Patrimônio</option>"; 
                    $.each(patrimonio,function(index2,item2){
                      items2+="<option value='"+item2+"'>"+item2+"</option>";
                    });

                  $("#patrimonio").html(items2); 

                  $('#patrimonio').trigger("chosen:updated");

                  var modelo = data.modelo;
                    items3 = "<option value=''>Modelo</option>"; 
                    $.each(modelo,function(index3,item3){
                      items3+="<option value='"+item3+"'>"+item3+"</option>";
                    });

                  $("#modelo").html(items3); 

                  $('#modelo').trigger("chosen:updated");
        });
  }); 

   $('.historicoCalibBtn').click(function() {

     var equipamento_id = $(this).attr('data_value');

     console.log(equipamento_id);

    $.post('modalHistoricoCalib', {

      _token  : token,
      equipamento_id      : equipamento_id,

    }, function(data) {
      if(data.success == true){
        var equipamento = data.equipamento;
        var calibracao = data.calibracao;

        console.log(calibracao);

        $("#descricao_equip").text(equipamento.descricao);
        $("#patrimonio_equip").text(equipamento.patrimonio);
        $("#serial_equip").text(equipamento.serial);
        $("#marca_equip").text(equipamento.marca);
        $("#modelo_equip").text(equipamento.modelo);
        $("#frequencia_equip").text(equipamento.frequencia);

        $("#tablehistorico").empty(); 

        var table ='';

        $.each(calibracao,function(index,item)
        { 
          table += '<tr>';

            table += '<td>'+calibracao[index].laboratorio+'</td>';
            table += '<td>'+calibracao[index].certificado+'</td>';
            table += '<td>'+calibracao[index].data_calib+'</td>';  
            table += '<td>'+calibracao[index].uso+'</td>';
            table += '<td>'+calibracao[index].venc+'</td>';

          table += '</tr>';
          });
        $("#tablehistorico").append(table);
      }
      else{
         $("#tablehistorico").empty(); 
         var table ='';
         table += '<tr>';
         table += '<td colspan="7"><font color="red" size="2"><center>Não existe histórico para esse equipamento</center></font></td>';
         table += '</tr>';
         $("#tablehistorico").append(table);
      }
    });
  }); 

/*calibração*/

  $('.usoBtn').click(function() {
      $("#cod_calibracao").val($(this).attr('data_value'));
      $("#cod_equipamento").val($(this).attr('data_value2'));
  });

  
});// end document