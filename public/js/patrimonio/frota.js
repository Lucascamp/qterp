$(document).ready(function() {
	
  // $('.flashError').fadeOut(0);
  // $('.flashSuccess').fadeOut(0);  

	var token = $("#token").val();

  $('#limparfrota').click(function(){
      window.location.href = "/frota";
  });

  $('.localizacaoBtn').click(function() {
      $("#frota_id").val($(this).attr('data_value'));
  });

  $('.excluirBtn').click(function() {
      $("#frota_id_excluir").val($(this).attr('data_value'));
      $("#frotadescricao").text($(this).attr('data_value2'));
  });

  $(".add_imposto_button").click(function(e){ //on add input button click
    var x = 1;
    var max_fields      = 50; //maximum input boxes allowed
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
      x++; //text box increment

      var table ='';
       
      table += '<tr><td class="tbtitulo" colspan="8"></td></tr>';
      table += '<tr>';
      table += '<td class="tblabel">Tipo do imposto(*)</td>';
      table += '<td><input type="text" class="form-control" name="tipo_imposto[]"/></td>';
      table += '<td class="tblabel">Valor R$(*)</td>';
      table += '<td><input type="text" class="form-control" name="valor_imposto[]"/></td>';
      table += '<td class="tblabel">Data de pagamento(*)</td>';
      table += '<td><input type="date" name="data_pagamento_imposto[]" class="form-control"/></td>';
      table += '</tr>';
      table += '<tr>';
      table += '<td class="tblabel">Restrição(*)</td>';
      table += '<td><input type="text" class="form-control" name="restricao[]"/></td>';
      table += '<td class="tblabel">Tipo de Restrição(*)</td>';
      table += '<td><input type="text" class="form-control" name="tipo_restricao[]"/></td>';
      table += '</tr>';

      $(".add_imposto_table").append(table);
    }
  });

  $(".add_manutencao_button").click(function(e){ //on add input button click
    var x = 1;
    var max_fields      = 50; //maximum input boxes allowed
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
      x++; //text box increment

      var table ='';
       
      table += '<tr><td class="tbtitulo" colspan="8"></td></tr>';
      table += '<tr>';
      table += '<td class="tblabel">Descrição da manutenção(*)</td>';
      table += '<td><input type="text" class="form-control" name="descricao_manutencao[]"/></td>';
      table += '<td class="tblabel">Valor R$(*)</td>';
      table += '<td><input type="text" class="form-control" name="valor_manutencao[]"/></td>';
      table += '<td class="tblabel">Data de manutenção(*)</td>';
      table += '<td><input type="date" name="data_manutencao[]" class="form-control"/></td>';
      table += '</tr>';

      $(".add_manutencao_table").append(table);
    }
  });

  if ($('#alugado').is(':checked'))
    $('.trcomprado').fadeOut(0);
  else
    $('.tralugado').fadeOut(0);

  $('#alugado').click(function(){

    if ($('#alugado').is(':checked')) {
      $('.trcomprado').fadeOut(0);
      $('.tralugado').fadeIn(500);
      $("#data_compra").val('');
      $("#valor_compra").val('');
    }
    else{
      $('.tralugado').fadeOut(0);
      $('.trcomprado').fadeIn(500);
      $("#data_aluguel").val('');
      $("#prazo_contrato").val('');
      $("#termino_contrato").val('');
      $("#valor_locacao").val('');
      $("#multa_recisao").val('');
      $("#data_reajuste").val('');
      $("#tipo_reajuste").val('');
      $("#mes_reajuste").val('');
      $("#ano_reajuste").val('');
    }

    });

    $('.infoFrotaBtn').click(function() {
        var id = $(this).attr('data_value');

        $.post('/modalFrotaInfo', {

        _token : token,
        id : id,

        }, function(data) {

          if(data.success == true){ 

            $("#infofrota").empty(); 
            var table ='';
            var carro = data.carro;
            console.log(carro);

            table += '<tr>';
              table += '<td class="tbtitulo" colspan="8">Dados do veiculo</td>';
            table += '</tr>';

            table += '<tr>';
              table += '<td class="tblabel">Placa:</td>';
              table += '<td>'+carro.placa+'</td>';

              table += '<td class="tblabel">Marca:</td>';
              table += '<td>'+carro.marca+'</td>';

              table += '<td class="tblabel">Modelo:</td>';
              table += '<td>'+carro.modelo+'</td>';

              table += '<td class="tblabel">Ano:</td>';
              table += '<td>'+carro.ano+'</td>';
            table += '</tr>';

            table += '<tr>';
              table += '<td class="tblabel">Cor:</td>';
              table += '<td>'+carro.cor+'</td>';

              table += '<td class="tblabel">Combustível:</td>';
              table += '<td>'+carro.combustivel+'</td>';

              table += '<td class="tblabel">Kilometragem:</td>';
              table += '<td>'+carro.km+'</td>';

              table += '<td class="tblabel">Chassi:</td>';
              table += '<td>'+carro.chassi+'</td>';
            table += '</tr>';

            table += '<tr>';
              table += '<td class="tblabel">Renavan:</td>';
              table += '<td>'+carro.renavan+'</td>';

              table += '<td class="tblabel">Responsável:</td>';
              table += '<td>'+carro.responsavel+'</td>';

              table += '<td class="tblabel">Proprietário:</td>';
              table += '<td>'+carro.proprietario+'</td>';

              table += '<td class="tblabel">Rastreador:</td>';
              if(carro.rastreador == 1)
                table += '<td>SIM</td>';
              else
                table += '<td>NÃO</td>';
            table += '</tr>';

            if(carro.alugado == 1)
            {
              table += '<tr>';
                table += '<td class="tblabel">Data de Aluguel:</td>';
                table += '<td>'+carro.data_aluguel+'</td>';

                table += '<td class="tblabel">Prazo Contratual:</td>';
                table += '<td>'+carro.prazo_contrato+'</td>';

                table += '<td class="tblabel">Termino Contrato:</td>';
                table += '<td>'+carro.termino_contrato+'</td>';

                table += '<td class="tblabel">Valor Locação:</td>';
                table += '<td>'+carro.valor_locacao+'</td>';
              table += '</tr>';

              table += '<tr>';
                table += '<td class="tblabel">Multa Recisão:</td>';
                table += '<td>'+carro.multa_recisao+'</td>';

                table += '<td class="tblabel">Data Reajuste:</td>';
                table += '<td>'+carro.data_reajuste+'</td>';

                table += '<td class="tblabel">Tipo Reajuste:</td>';
                table += '<td>'+carro.tipo_reajuste+'</td>';

                table += '<td class="tblabel">Mês/Ano Reajuste(*)</td>';
                table += '<td>'+carro.mes_reajuste+'/'+carro.ano_reajuste+'</td>';
              table += '</tr>';
            }

            else
            {
              table += '<tr>';
                table += '<tr>';
                table += '<td class="tblabel">Data Compra:</td>';
                table += '<td>'+carro.data_compra+'</td>';

                table += '<td class="tblabel">Valor Compra:</td>';
                table += '<td>'+carro.valor_compra+'</td>';

                table += '<td colspan="4"></td>';
              table += '</tr>';
            }

            table += '<tr>';
              table += '<td class="tbtitulo" colspan="8">Localização</td>';
            table += '</tr>';

            $.each(carro.localizacao,function(index,item)
            {
              table += '<tr>';
                table += '<td class="tblabel">Unidade:</td>';
                table += '<td>'+item.unidade+'</td>';

                table += '<td class="tblabel">Centro de custo:</td>';
                table += '<td>'+item.centrocusto_id+'</td>';

                table += '<td class="tblabel">Data:</td>';
                table += '<td colspan ="3">'+item.data+'</td>';
              table += '</tr>';
            });

            table += '<tr>';
              table += '<td class="tbtitulo" colspan="8">Seguro</td>';
            table += '</tr>';

            table += '<tr>';
              table += '<td class="tblabel">Seguradora:</td>';
              table += '<td>'+carro.seguro.seguradora+'</td>';

              table += '<td class="tblabel">Corretor:</td>';
              table += '<td>'+carro.seguro.corretor+'</td>';

              table += '<td class="tblabel">Número da apólice:</td>';
              table += '<td>'+carro.seguro.apolice+'</td>';

              table += '<td class="tblabel">Valor do seguro:</td>';
              table += '<td>'+carro.seguro.valor_seguro+'</td>';
            table += '</tr>';

            table += '<tr>';
              table += '<td class="tblabel">Data Inicial:</td>';
              table += '<td>'+carro.seguro.data_inicial+'</td>';

              table += '<td class="tblabel">Data Final:</td>';
              table += '<td>'+carro.seguro.data_final+'</td>';

              table += '<td class="tblabel">Vistoria:</td>';
              table += '<td>'+carro.seguro.vistoria+'</td>';
              
              table += '<td colspan="2"></td>';
            table += '</tr>';

            table += '<tr>';
              table += '<td class="tbtitulo" colspan="8">Ecofrotas</td>';
            table += '</tr>';
             
            table += '<tr>';
              table += '<td class="tblabel">Número do cartão:</td>';
              table += '<td colspan="2">'+carro.ecofrota.numero+'</td>';

              table += '<td class="tblabel">Tipo:</td>';
              table += '<td colspan="2">'+carro.ecofrota.tipo+'</td>';

              table += '<td class="tblabel">Recarga:</td>';
              table += '<td>'+carro.ecofrota.recarga+'</td>';
            table += '</tr>';

            table += '<tr>';
              table += '<td class="tbtitulo" colspan="8">Impostos</td>';
            table += '</tr>';

            $.each(carro.imposto,function(index,item)
            {
              if(index != 0)
              {
                table += '<tr>';
                  table += '<td class="tbtitulo" colspan="8"></td>';
                table += '</tr>';
              }

              table += '<tr>';
                table += '<td class="tblabel">Tipo do imposto:</td>';
                table += '<td colspan ="2">'+item.tipo_imposto+'</td>';

                table += '<td class="tblabel">Valor R$:</td>';
                table += '<td>'+item.valor_imposto+'</td>';

                table += '<td class="tblabel">Data de pagamento:</td>';
                table += '<td colspan ="2">'+item.data_pagamento_imposto+'</td>';
                
              table += '</tr>';

              table += '<tr>';  
                table += '<td class="tblabel">Restrição:</td>';
                table += '<td>'+item.restricao+'</td>';

                table += '<td class="tblabel">Tipo de Restrição:</td>';
                table += '<td colspan ="2">'+item.tipo_restricao+'</td>';
                table += '<td colspan ="3">';

              table += '</tr>';
              
            });
            
            table += '<tr>';
                table += '<td class="tbtitulo" colspan="8">Manutenção</td>';
            table += '</tr>';
            

            $.each(carro.manutencao,function(index,item)
            {
              table += '<tr>';
                table += '<td class="tblabel">Descrição da manutenção:</td>';
                table += '<td colspan ="2">'+item.descricao_manutencao+'</td>';

                table += '<td class="tblabel">Valor R$:</td>';
                table += '<td>'+item.valor_manutencao+'</td>';

                table += '<td class="tblabel">Data de Manutenção:</td>';
                table += '<td colspan ="2">'+item.data_manutencao+'</td>';
                
              table += '</tr>';

              table += '<tr>';
                table += '<td class="tbtitulo" colspan="8"></td>';
              table += '</tr>';
            });

            $("#infofrota").append(table);
          }
        });
    });

});// end document