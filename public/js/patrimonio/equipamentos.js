$(document).ready(function() {
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
  // $('.flashError').fadeOut(0);
  // $('.flashSuccess').fadeOut(0); 

    var url = window.location.href;
    var pagina = url.split('/');
    if(pagina[3] == 'patrimonio'){ 
         var token = $("#token").val();
         $("#bt_sinc_equipamentos").click(function(event) {
        $.post('/patrimonio/sincronizacaoequipamentos', {
          _token:         token,
        }, function(data) {   
          if(data.success){
            alert(data.mensagem);
          }
          if(!data.success){
            alert(data.mensagem);
          }
        });
  });
    }

  var url = window.location.href;
    var pagina = url.split('/');
    if(pagina[3] == 'equipamento' || pagina[3] == 'equipamento#'){ 
	 
    var token = $("#token").val();
    var uni = $("#unidade").val();
    var filtro = false;
    $("#bt_filtro_pendente").click(function(event) {
        filtro = true;
        carregarTable();
    });
    $("#bt_filtro_todos").click(function(event) {
        filtro = false;
        carregarTable();
    });

    $("#tabela_equipamentos").DataTable({
                    "responsive": true,
                    "iDisplayLength": 25,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
                    "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ 9 ] }
                    ],
                });
    
    var url = window.location.href;
    var pagina = url.split('/');

    if(pagina[3] == 'equipamento' || pagina[3] == 'equipamento#'){ 
      console.log(pagina[3]); 
      carregarTable();
      $("#unidade").change(function(event) {
        carregarTable();
      });
    }
    


  $('.localizacaoBtn').click(function() {
      $("#equipamento_id").val($(this).attr('data_value'));
  });

  $('.excluirBtn').click(function() {
      $("#equipamento_id_excluir").val($(this).attr('data_value'));
      $("#equipdescricao").text($(this).attr('data_value2'));
  });

  $('#limparequip').click(function(){
      window.location.href = "/equipamento";
    });

  $("#descricao").change(function(event) {
    var descricao = $(this).val();

    $.post('/dadosDescricao', {
          _token  : token,
          descricao: descricao,

        }, function(data) {  
          var serial = data.serial;
          items = "<option value=''>Serial</option>"; 
          $.each(serial,function(index,item){
            items+="<option value='"+item+"'>"+item+"</option>";
          });

          $("#serial").html(items); 

          $('#serial').trigger("chosen:updated");

          var patrimonio = data.patrimonio;
          items2 = "<option value=''>Patrimônio</option>"; 
          $.each(patrimonio,function(index2,item2){
            items2+="<option value='"+item2+"'>"+item2+"</option>";
          });

          $("#patrimonio").html(items2); 

          $('#patrimonio').trigger("chosen:updated");

          var modelo = data.modelo;
          items3 = "<option value=''>Modelo</option>"; 
          $.each(modelo,function(index3,item3){
            items3+="<option value='"+item3+"'>"+item3+"</option>";
          });

          $("#modelo").html(items3); 

          $('#modelo').trigger("chosen:updated");
        });
  }); 


  $("#bt_carregar_historico").click(function(event) {
    var descricao = $("#descricao").val();
    var aplicacao = $("#aplicacao").val();
    var patrimonio = $("#patrimonio").val();

    console.log(descricao, aplicacao, patrimonio);

    $.post('/carregarHistorico', {

      _token  : token,
      descricao      : descricao,
      aplicacao   : aplicacao,
      patrimonio   : patrimonio,

    }, function(data) {
      if(data.success){
        var equipamento = data.equipamento;
        var localizacao = data.localizacao;

        $("#tablehistorico").empty(); 
        
        var table ='';

        $.each(localizacao,function(index,item)
        { 
          table += '<tr>';

            table += '<td>'+localizacao[index].data+'</td>';
            table += '<td>'+localizacao[index].centrocusto+'</td>';
            table += '<td>'+localizacao[index].projeto+'</td>';  
            table += '<td>'+localizacao[index].funcionario+'</td>';
            table += '<td>'+localizacao[index].usuario+'</td>';

            if(localizacao[index].recebido == 0)
              table += '<td><font color=red>Não</font></td>';
            else
              table += '<td><font color=green>Sim</font></td>';

            table += '<td>'+localizacao[index].motivo+'</td>';

          table += '</tr>';
          });
        $("#tablehistorico").append(table);
      }
    });
  }); 

  // $('.receberBtn').click(function() {
  //     var id = $(this).attr('data_value');
  //     console.log(token);

  //     $.post('/receber', { _token : token, id : id });

  //     location.reload();
  // });

   $('.historicoBtn').click(function() {

     var equipamento_id = $(this).attr('data_value');
    
    console.log(equipamento_id);

    $.post('/modalHistorico', {

      _token  : token,
      equipamento_id      : equipamento_id,

    }, function(data) {
      if(data.success == true){
        var equipamento = data.equipamento;
        var localizacao = data.localizacao;

        $("#tablehistorico").empty(); 
        
        var table ='';

        $.each(localizacao,function(index,item)
        { 
          table += '<tr>';

            table += '<td>'+localizacao[index].data+'</td>';
            table += '<td>'+localizacao[index].centrocusto+'</td>';
            table += '<td>'+localizacao[index].projeto+'</td>';  
            table += '<td>'+localizacao[index].funcionario+'</td>';
            table += '<td>'+localizacao[index].usuario+'</td>';

            if(localizacao[index].recebido == 0)
              table += '<td><font color=red>Não</font></td>';
            else
              table += '<td><font color=green>Sim</font></td>';

            table += '<td>'+localizacao[index].motivo+'</td>';

          table += '</tr>';
          });
        $("#tablehistorico").append(table);
      }
      else{
         $("#tablehistorico").empty(); 
         var table ='';
         table += '<tr>';
         table += '<td colspan="7"><font color="red" size="2"><center>Não existe histórico para esse equipamento</center></font></td>';
         table += '</tr>';
         $("#tablehistorico").append(table);
      }
    });
  }); 



  



  $("#bt_sinc_locequipamentos").click(function(event) {
        $.post('/patrimonio/sincronizacaolocequipamentos', {
          _token:         token,
        }, function(data) {   
          if(data.success){
            alert(data.mensagem);
          }
          if(!data.success){
            alert(data.mensagem);
          }
        });
  });

    $("#bt_sinc_frota").click(function(event) {
        $.post('/patrimonio/sincronizacaofrota', {
              _token:         token,
            }, function(data) {   
              if(data.success){
                alert(data.mensagem);
              }
              if(!data.success){
                alert(data.mensagem);
              }
        });
    });

    $("#bt_salvar_movimentacao").click(function(event) {
        var id_equip =  $("#equipamento_id").val();
        var unidade_destino = $("#unidade_destino").val();
        var projeto = $("#projeto").val();
        var data_movimento = $("#data_movimento").val();
        var funcinario_destino = $("#funcinario_destino").val();
        var motivo = $("#motivo").val();

        $.post('/equipamento/movimentar_equipamento', {
                _token             : token,
                id                 : id_equip,
                unidade_destino    : unidade_destino,
                projeto            : projeto,
                data_movimento      : data_movimento,
                funcinario_destino : funcinario_destino,
                motivo             : motivo,
            }, function(data) {   
                if(data.success){
                    alert(data.mensagem);
                    
                    $("#dtm"+data.id).empty();
                    var data_padrao = data.data_movimentacao.split('-');
                    $("#dtm"+data.id).html(data_padrao[2]+'-'+data_padrao[1]+'-'+data_padrao[0]);
                    $("#locm"+data.id).empty();
                    $("#locm"+data.id).html('Em transito para unidade '+data.destino);
                    $("#usm"+data.id).empty();
                    $("#usm"+data.id).html(data.nome_user);
                    $("#fum"+data.id).empty();
                    $("#fum"+data.id).html(data.funcionario_nome);
                    $("#stm"+data.id).empty();
                    $("#stm"+data.id).html('<font color =\'red\'>Aguardando Recebimento</font>');
                    $("#btm"+data.id).empty();
                    $("#btm"+data.id).html('<font color =\'red\'>Atualize a pagina para ver as opções</font>');
                    $("#unidade_destino").val(1);
                    $("#projeto").val('');
                    $("#data_movimento").val(0);
                    $("#funcinario_destino").val(0);
                    $("#motivo").val('');
                    // carregarTable();
                }
            }
        );
    });

    

    $("#bt_salvar_manutencao").click(function(event) {
        salvar_manutencao();
    });

    function carregarTable(){
        $.blockUI();
        $("#tabela_equipamentos").DataTable().destroy();
        uni = $("#unidade").val();
        $.post('/equipamento/carregartable', {
            _token:         token,
            unidade : uni,
            filtro :filtro,
        }, function(data) {   
            if(data.success){
                $("#tabela_equipamentos tbody").empty();
                
                if(data.unidade_filtro != 0){ $("#titulo").html('<h5>Equipamentos da unidade '+data.unidades[data.unidade_filtro]+'</h5>'); } 
                else { $("#titulo").html('<h5>Todos os Equipamentos </h5>'); }

                $.each(data.equipamentos,function(index,item){
                    
                    var excluir = '';
                    var manutencao = '';
                    var transferir = '';
                    var editar = '';


                    var data_movimentacao = '<td id="dtm'+item.id+'"></td>';
                    if ( typeof data.status[item.id] !== "undefined" && data.status[item.id]) {
                        var data_padrao = data.status[item.id].data_movimentacao.split('-');
                        data_movimentacao = '<td id="dtm'+item.id+'">'+data_padrao[2]+'-'+data_padrao[1]+'-'+data_padrao[0]+'</td>'
                    }
                    var localizacao = '<td id="locm'+item.id+'"></td>';
                    if(item.status == 0){
                        if(item.local_atual)
                            localizacao = '<td id="locm'+item.id+'">'+data.unidades[item.local_atual]+'</td>';
                        else {
                            localizacao = '<td id="locm'+item.id+'">Desconhecido</td>';
                        }
                    }
                    if(item.status == 1){
                        localizacao = '<td id="locm'+item.id+'"> Em transito para unidade ';
                        if( typeof data.status[item.id] !== "undefined" && data.status[item.id]){
                            localizacao += data.unidades[data.status[item.id].unidade_destino]+'</td>';
                        }                        
                    }
                    if(item.status == 2){
                        if(item.local_atual)
                            localizacao = '<td id="locm'+item.id+'">'+data.unidades[item.local_atual]+'</td>';
                        else {
                            localizacao = '<td id="locm'+item.id+'">Desconhecido</td>';
                        }
                    }
                    var usuario_id = '<td id="usm'+item.id+'"></td>';
                    var funcionario_id = '<td id="fum'+item.id+'"></td>';
                    if (typeof data.status[item.id] !== "undefined" && data.status[item.id]){
                        usuario_id = '<td id="usm'+item.id+'">'+data.usuarios[data.status[item.id].usuario_id]+'</td>';
                        if( data.status[item.id].funcionario_id == 0 ){
                             funcionario_id = '<td id="fum'+item.id+'">Unidade</td>';
                        }else{ funcionario_id = '<td id="fum'+item.id+'">'+data.funcionarios[data.status[item.id].funcionario_id]+'</td>'; }

                    }
                                        
                                   
                    var status_equip = '<td id="stm'+item.id+'"></td>';                    
                    if(item.status == 0){
                        status_equip = '<td id="stm'+item.id+'"><font color =\'green\'>Recebido</font></td>';
                        if(data.unidade_id_user == item.local_atual){
                            transferir = ' <button type="button" data-target="#modalLocalizacao" data-toggle="modal" data-tooltip="tooltip" class="btn btn-warning fa fa-automobile localizacaoBtn" style="width: 41px; height:34px;" title="Alterar Localização" data_value="'+item.id+'"></button>';
                        }
                    }
                    if(item.status == 2){
                        status_equip = '<td id="stm'+item.id+'"><font color =\'red\'>Em Manutenção</font></td>';
                    }
                    if(item.status == 1){
                        if (typeof data.responsaveis !== "undefined" && data.responsaveis && $.inArray(data.user_id, data.responsaveis[item.id]) != -1){
                            status_equip = '<td id="stm'+item.id+'"><button data-tooltip="tooltip" class="btn btn-success fa fa-check receberBtn"  title="Receber" id="btrec'+item.id+'"></button></td>';

                        }else{
                            status_equip = '<td id="stm'+item.id+'"><font color =\'red\'>Aguardando Recebimento</font></td>';
                        }
                    }
                    var permissoes = [];
                    $.each(data.permissoes_opcoes,function(ind,i){
                        permissoes.push(''+i);
                    });

                    if ($.inArray('2',permissoes) != -1){
                        excluir    = ' <button type="button" data-target="#modalExcluir" data-toggle="modal" data-tooltip="tooltip" class="btn btn-danger fa fa-close excluirBtn" style="width: 41px; height:34px;" title="Excluir" data_value="'+item.id+'" data_value2="'+item.descricao+'"></button>';
                        if(item.status == 0){
                            manutencao = ' <button type="button" class="btn btn-success fa fa-wrench btnmanutencao" style="width: 41px; height:34px;" title="Mandar para Manutenção" id="manutencao'+item.id+'"></button>';
                        }
                        transferir = ' <button type="button" data-target="#modalLocalizacao" data-toggle="modal" data-tooltip="tooltip" class="btn btn-warning fa fa-automobile localizacaoBtn" style="width: 41px; height:34px;" title="Alterar Localização" data_value="'+item.id+'"></button>';
                        editar = ' <a href="/equipamento/'+item.id+'/editar" target="_blank" data-toggle="tooltip" title="Editar" data-placement="top" style="width: 41px; height:34px;" class="legenda btn btn-info dim"><i class="fa fa-pencil"></i></a>';
                    }

                    $("#tabela_equipamentos tbody").append(
                        '<tr>'+
                            '<td>'+item.descricao+'</td>'+
                            '<td>'+item.modelo+'</td>'+
                            '<td>'+item.serial+'</td>'+
                            '<td>'+item.patrimonio+'</td>'+
                            data_movimentacao+
                            localizacao+
                            usuario_id+
                            funcionario_id+
                            status_equip+                                
                            '<td id="btm'+item.id+'">'+
                                '<button type="button" data-target="#modalHistorico" data-toggle="modal" data-tooltip="tooltip" class="btn btn-primary fa fa-exchange historicoBtn" style="width: 41px; height:34px;" title="Exibir Histórico" data_value="'+item.id+'"></button>'+
                                editar+
                                transferir+
                                excluir+
                                manutencao+
                            '</td>'+
                        '</tr>'
                    );
                });
                $(".btnmanutencao").click(function(event) {
                    var id = $(this).prop('id').replace('manutencao','');
                    $("#equipamento_id_manu").val(id);
                    $("#obs_manu").val('');
                    $("#modalManutencao").modal('toggle');
                });


                // $(".bteditaritem").click(function(event) {
                //     var id = $(this).prop('id').replace('btedt','');
                //     location.href='/equipamento/'+id+'/editar';
                // });
               

                $('.localizacaoBtn').click(function() {
                    $("#equipamento_id").val($(this).attr('data_value'));
                    console.log($("#equipamento_id").val());
                });

                $(".receberBtn").click(function(event) {
                    var id_equip = $(this).prop('id').replace('btrec',''); ;
                    $.post('/equipamento/receber_equipamento', {
                            _token             : token,
                            id                 : id_equip,
                        }, function(data) {   
                            if(data.success){
                                alert(data.mensagem);
                                $("#dtm"+data.id).empty();
                                var data_padrao = data.data_movimentacao.split('-');
                                $("#dtm"+data.id).html(data_padrao[2]+'-'+data_padrao[1]+'-'+data_padrao[0]);
                                $("#locm"+data.id).empty();
                                $("#locm"+data.id).html(data.destino);
                                $("#usm"+data.id).empty();
                                $("#usm"+data.id).html(data.nome_user);
                                $("#fum"+data.id).empty();
                                $("#fum"+data.id).html(data.funcionario_nome);
                                $("#stm"+data.id).empty();
                                $("#stm"+data.id).html('<font color =\'green\'>Recebido</font>');
                                $("#btm"+data.id).empty();
                                $("#btm"+data.id).html('<font color =\'red\'>Atualize a pagina para ver as opções</font>');
                            }
                        }
                    );
                });
                // $(".receberBtn").click(function(event) {
                //     var id_equip = $(this).prop('id').replace('btrec',''); ;
                //     $.post('/equipamento/receber_equipamento', {
                //             _token             : token,
                //             id                 : id_equip,
                //         }, function(data) {   
                //             if(data.success){
                //                 alert(data.mensagem);
                //                 carregarTable();
                //             }
                //             if(!data.success){
                //                 alert(data.mensagem);
                //                 carregarTable();
                //             }
                //         }
                //     );
                // });
                
                $("#bt_carregar_historico").click(function(event) {
                    var descricao = $("#descricao").val();
                    var aplicacao = $("#aplicacao").val();
                    var patrimonio = $("#patrimonio").val();

                    console.log(descricao, aplicacao, patrimonio);

                    $.post('/carregarHistorico', {

                      _token  : token,
                      descricao      : descricao,
                      aplicacao   : aplicacao,
                      patrimonio   : patrimonio,

                    }, function(data) {
                      if(data.success){
                        var equipamento = data.equipamento;
                        var localizacao = data.localizacao;

                        $("#tablehistorico").empty(); 
                        
                        var table ='';

                        $.each(localizacao,function(index,item)
                        { 
                          table += '<tr>';

                            table += '<td>'+localizacao[index].data+'</td>';
                            table += '<td>'+localizacao[index].centrocusto+'</td>';
                            table += '<td>'+localizacao[index].projeto+'</td>';  
                            table += '<td>'+localizacao[index].funcionario+'</td>';
                            table += '<td>'+localizacao[index].usuario+'</td>';

                            if(localizacao[index].recebido == 0)
                              table += '<td><font color=red>Não</font></td>';
                            else
                              table += '<td><font color=green>Sim</font></td>';

                            table += '<td>'+localizacao[index].motivo+'</td>';

                          table += '</tr>';
                          });
                        $("#tablehistorico").append(table);
                      }
                    });
                  });
                $('.historicoBtn').click(function() {

                var equipamento_id = $(this).attr('data_value');
                
                console.log(equipamento_id);

                $.post('/modalHistorico', {

                  _token  : token,
                  equipamento_id      : equipamento_id,

                }, function(data) {
                  if(data.success == true){
                    var equipamento = data.equipamento;
                    var historico = data.historico;

                    $("#tablehistorico").empty(); 
                    
                    var table ='';

                    $.each(historico,function(index,item)
                    { 
                      table += '<tr>';

                        table += '<td>'+item.created_at+'</td>';
                        table += '<td>'+data.unidades[item.unidade_remetente]+'</td>';
                        table += '<td>'+data.unidades[item.unidade_destino]+'</td>';  
                        table += '<td>'+data.usuarios[item.usuario_id]+'</td>';
                        if(item.funcionario_id == 0){
                            table += '<td>Unidade</td>';
                        }else{
                            table += '<td>'+data.funcionarios[item.funcionario_id]+'</td>';
                        }
                        table += '<td>'+item.tipo+'</td>';
                        table += '<td>'+item.motivo+'</td>';

                      table += '</tr>';
                      });
                    $("#tablehistorico").append(table);
                  }
                  else{
                     $("#tablehistorico").empty(); 
                     var table ='';
                     table += '<tr>';
                     table += '<td colspan="7"><font color="red" size="2"><center>Não existe histórico para esse equipamento</center></font></td>';
                     table += '</tr>';
                     $("#tablehistorico").append(table);
                  }
                });
              });


                $("#tabela_equipamentos").DataTable({
                    "responsive": true,
                    "iDisplayLength": 25,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
                    "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ 9 ] }
                    ],
                });
                
            }

            if(!data.success){
                alert(data.mensagem);
            }
        });
    }

    function salvar_manutencao(){
        var id = $("#equipamento_id_manu").val();
        var obs = $("#obs_manu").val();
        var data_movimentacao = $("#data_movimento_manu").val();
        console.log(id);
        console.log(obs);
        console.log(data_movimentacao);

        $.post('/equipamento/enviar_manutencao', {
                _token             : token,
                id                 : id,
                obs : obs ,
                data_movimentacao : data_movimentacao,
            }, function(data) {   
                if(data.success){
                    alert(data.mensagem);
                    $("#modalManutencao").modal('toggle');
                }
                if(!data.success){
                    alert(data.mensagem);
                    $("#modalManutencao").modal('toggle');
                }
            }
        );

    }
}
});// end document