$(document).ready(function() {
	
  // $('.flashError').fadeOut(0);
  // $('.flashSuccess').fadeOut(0);  
    var url = window.location.href;
    var pagina = url.split('/');

  $('#limparalm').click(function(){
      window.location.href = "/almoxarifado";
    });

	var token = $("#token").val();

  $('.subBtn').click(function() {
      $(".messagesub").fadeOut(0);
      $("#almoxarifadosub").val($(this).attr('data_value'));
      $("#subtrair").val('');
      $("#dispsub").html('');
      
      var id = $("#almoxarifadosub").val();

      $.post('/totalTransf', {

      _token    : token,
      id        : id,
      
      }, function(data) {
          $("#dispsub").html(data.total);
      });
  });

  $('.addBtn').click(function() {
      $("#almoxarifadoadd").val($(this).attr('data_value'));
      $("#adicionar").val('');
  });

  $('.transfBtn').click(function() {
      $(".messagesub").fadeOut(0);
      $("#totalitem").html('');
      $("#almoxarifadotransf").val($(this).attr('data_value'));
      $("#origemtransf").html($(this).attr('data_value2'));
      $("#itensorigem").html($(this).attr('data_value3'));
      $("#transferir").val('');

      var id = $("#almoxarifadotransf").val();
      var valor = $("#total_"+id);

      $.post('/totalTransf', {

      _token    : token,
      id        : id,
      
      }, function(data) {
          $("#totalitem").html(data.total);
      });
  });

   $("#tablealmoxarifado").DataTable({
                    "responsive": true,
                    "iDisplayLength": 25,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
                    "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ 7,8,9]}
                    ],
                });

  $('.addsave').click(function() {
      var id = $("#almoxarifadoadd").val();
      var adicionar = $("#adicionar").val();
      $('.td_'+id).attr('class', 'td_'+id);

      $.post('/adicionar', {

      _token    : token,
      id        : id,
      adicionar : adicionar,

      }, function(data) {
        if(data.success == true){
          $("#total_"+data.id).html(data.total);
          $("#valor_"+data.id).html(data.valor);
          $(".td_"+data.id).addClass('adicionar');
          $('#modalAdd').modal('hide');
        }
      });
  });

  $('.subsave').click(function() {
      var id = $("#almoxarifadosub").val();
      var subtrair = $("#subtrair").val();
      $('.td_'+id).attr('class', 'td_'+id);

      $.post('/subtrair', {

      _token    : token,
      id        : id,
      subtrair : subtrair,

      }, function(data) {
        if(data.success == true){
          $("#total_"+data.id).html(data.total);
          $("#valor_"+data.id).html(data.valor);
          $(".td_"+data.id).addClass('subtrair');
          $('#modalSub').modal('hide');
        }
        if(data.success == false){
          $(".messagesub").fadeIn(200);
        }
      });
  });

  $('.transfsave').click(function() {
      var id = $("#almoxarifadotransf").val();
      var transferir   = $("#transferir").val();
      var itensdestino = $("#itensdestino").val();

      $('.td_'+id).attr('class', 'td_'+id);

      $.post('/transferir', {

      _token    : token,
      id        : id,
      transferir : transferir,
      itensdestino : itensdestino,

      }, function(data) {
        if(data.success == true){ 

          $('.td_'+data.origemid).attr('class', 'td_'+data.origemid);
          $('.td_'+data.destinoid).attr('class', 'td_'+data.destinoid);

          if(data.item_novo)
          {
            var table ='';

            var classtd = 'td_'+data.item_novo.id;
            var idspantotal = 'total_'+data.item_novo.id;
            var idspanvalor = 'valor_'+data.item_novo.id;

            table += '<tr>';
              table += '<td>'+data.item_novo.unidade.unidade+'</td>';
              table += '<td>'+data.item_novo.item.codigo+'</td>';
              table += '<td>'+data.item_novo.item.descricao+'</td>';
              table += '<td>'+data.item_novo.item.unidade_medida+'</td>';
              table += '<td>R$ '+data.item_novo.item.valor+'</td>';
              table += '<td class='+classtd+'><span id='+idspantotal+'>'+data.item_novo.total+'</span></td>'; 
              table += '<td class='+classtd+'>R$ &nbsp;<span id='+idspanvalor+'>'+data.item_novo.valor+'</span></td>';  
              table += '<td>-</td>'; 
              table += '<td>-</td>'; 
              table += '<td>-</td>'; 
            table += '</tr>';
          
            $("#tablealmoxarifado").append(table);
            $("#tablealmoxarifado").DataTable({
                    "responsive": true,
                    "iDisplayLength": 25,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
                    "aoColumnDefs": [
                        { 'bSortable': false,}
                    ],
                });
          } 
       
          $("#total_"+data.origemid).html(data.totalorigem);
          $("#valor_"+data.origemid).html(data.valororigem);
          $(".td_"+data.origemid).addClass('envio');

          $("#total_"+data.destinoid).html(data.totaldestino);
          $("#valor_"+data.destinoid).html(data.valordestino);
          $(".td_"+data.destinoid).addClass('recebido');

          $('#modalTransf').modal('hide');
        }
        if(data.success == false){
          $(".messagesub").fadeIn(200);
        }
      });
  });

  $('.additmBtn').click(function() {
    items = "<option value=''>Selecione a Unidade</option>"; 
    $("#itemaddalm").html(items); 
    $('#itemaddalm').trigger("chosen:updated");
    $("#unidalm").val('');
    $('#unidalm').trigger("chosen:updated");
    $("#totalalm").val('');
  });

  $("#unidalm").change(function(event) {
    var unidade = $(this).val();

    $.post('/additmalm', {
          _token  : token,
          unidade: unidade,

        }, function(data) {  
            console.log(data);

            var itemaddalm = data.itens;

            items = "<option value=''>Itens</option>"; 
            $.each(itemaddalm,function(index,item){
              items+="<option value='"+index+"'>"+item+"</option>";
            });

            $("#itemaddalm").html(items); 

            $('#itemaddalm').trigger("chosen:updated");
        });
  }); 


$('.additmalmsave').click(function() {
      var unidade = $("#unidalm").val();
      var item   = $("#itemaddalm").val();
      var total = $("#totalalm").val();

      $.post('/additmalmsave', {

      _token : token,
      unidade : unidade,
      item : item,
      total : total,

      }, function(data) {

        if(data.success == true){ 
          var table ='';

          var classtd = 'td_'+data.item_novo.id;
          var idspantotal = 'total_'+data.item_novo.id;
          var idspanvalor = 'valor_'+data.item_novo.id;

          table += '<tr>';
          table += '<td>'+data.item_novo.unidade.unidade+'</td>';
          table += '<td>'+data.item_novo.item.codigo+'</td>';
          table += '<td>'+data.item_novo.item.descricao+'</td>';
          table += '<td>'+data.item_novo.item.unidade_medida+'</td>';
          table += '<td>R$ '+data.item_novo.item.valor+'</td>';
          table += '<td class='+classtd+'><span id='+idspantotal+'>'+data.item_novo.total+'</span></td>';  
          table += '<td class='+classtd+'>R$ &nbsp;<span id='+idspanvalor+'>'+data.valor+'</span></td>';  
          table += '<td>-</td>'; 
          table += '<td>-</td>'; 
          table += '<td>-</td>'; 
          table += '</tr>';
          
          $("#tablealmoxarifado").append(table);
          $("#tablealmoxarifado").DataTable({
                    "responsive": true,
                    "iDisplayLength": 25,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
                    "aoColumnDefs": [
                        { 'bSortable': false,}
                    ],
                });
          $('#modalItemAlmoxarifado').modal('hide');
        }
      });
  });

$('#bthistalm').click(function() {
  var unidade = $("#unidhisttalm").val();
  var item   = $("#itemhisttalm").val();

  $.post('/almoxarifado/historico', {

    _token : token,
    unidade : unidade,
    item : item,

  }, function(data) {
    if(data.success == true){ 

      var historico = data.historico;

      $("#tbhistalm").empty(); 
        
      var table ='';

      $.each(historico,function(index,item)
      { 
        table += '<tr>';
          table += '<td>'+historico[index].data_criacao+'</td>';
          table += '<td>'+historico[index].unidade_destino+'</td>';
          table += '<td>'+historico[index].item.descricao+'</td>';
          table += '<td>'+historico[index].quantidade+'</td>';  
          table += '<td>'+historico[index].operacao+'</td>';
          table += '<td>'+historico[index].total+'</td>';
        table += '</tr>';
      });

      $("#tbhistalm").append(table);
    }
  });
});

$("#unidhisttalm").change(function(event) {
  var unidade = $(this).val();

  $.post('/dadosItemUnidade', {
    _token  : token,
    unidade: unidade,

  }, function(data) {  
    console.log(data);

    var itens = data.itens;
    items = "<option value=''>Items</option>"; 
    $.each(itens,function(index,item){
      items+="<option value='"+index+"'>"+item+"</option>";
    });

    $("#itemhisttalm").html(items); 

    $('#itemhisttalm').trigger("chosen:updated");
  });
}); 

  $("#bt_sinc_almoxarifado").click(function(event) {
    $.post('/patrimonio/sincronizacaoalmoxarifado', {
          _token:         token,
        }, function(data) {   
          if(data.success){
            alert(data.mensagem);
          }
          if(!data.success){
            alert(data.mensagem);
          }
        });
  });

 $("#bt_sinc_histoalmoxarifado").click(function(event) {
    $.post('/patrimonio/sincronizacaohistoricoalmoxarifado', {
          _token:         token,
        }, function(data) {   
          if(data.success){
            alert(data.mensagem);
          }
          if(!data.success){
            alert(data.mensagem);
          }
        });
  });



    

    if(pagina[4] == 'relatorioconsumo' || pagina[4] == 'relatorioconsumo#'){ 
        $("#tabelaConsumo").DataTable({
            "responsive": true,
                    "iDisplayLength": 25,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
        });

        $("#bt_gerar_rel_consumo").click(function(event) {
            carregatabelaconsumo();
        });
    }


    function carregatabelaconsumo(){
        var data_ini = $("#data_ini").val();
        var data_fim = $("#data_fim").val();
        var unidade = $("#unidade_rel_consumo").val();
        $("#tabelaConsumo").DataTable().destroy();
        $("#bodyconsumo").empty();
        $.post('/almoxarifado/carregatabelaconsumo', {
            _token:         token,
            data_ini : data_ini,
            data_fim : data_fim,
            unidade : unidade,
        }, function(data) {   
            if(data.success){
                $.each(data.row,function(index,item){
                    console.log(item.descricao);
                    $("#bodyconsumo").append('<tr>'+
                            '<td>'+item.descricao+'</td>'+
                            '<td>'+item.codigo_protheus+'</td>'+
                            '<td>'+item.quantidade+'</td>'+
                            '<td>'+item.valor_medio+'</td>'+
                            '<td>'+data.unidades[item.unidade_id]+'</td>'+
                        '</tr>');
                });
                $("#tabelaConsumo").DataTable({
                    "responsive": true,
                            "iDisplayLength": 25,
                            "dom": 'T<"clear">lfrtip',
                            "tableTools": {
                                "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                                "aButtons": ['xls'],
                            },
                });
            }
        });
    }


});// end document