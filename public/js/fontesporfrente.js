var $ = jQuery.noConflict();
//main function
jQuery(function($){
	var token = $("#token").val();
    var dados_unidades = [];
    var MG ={
            'UF'        : 'MG',
            'municipio' : 'IBIRITÉ',
            'end'       : 'RUA PETROVALE,450'
        }
    var MC ={
            'UF'        : 'RJ',
            'municipio' : 'MACAE',
            'end'       : 'RUA TEOFILO MONTEIRO, QUADRA 20 LOTE 12 - SÃO JOSE DO BARRETO'
        }
    var RJ ={
            'UF'        : 'RJ',
            'municipio' : 'RIO DE JANEIRO',
            'end'       : '******'
        }
    var SG ={
            'UF'        : 'RJ',
            'municipio' : 'ITABORAI',
            'end'       : 'FRANKLIN SILVA LOTE 4388 - Gebara'
        }
    var BA ={
            'UF'        : 'BA',
            'municipio' : 'DIAS D\'AVILA',
            'end'       : 'RUA ANTONIO CONSELHEIRO, 315'
        }
    var AL ={
            'UF'        : 'AL',
            'municipio' : 'MARECHAL DEODORO',
            'end'       : 'RODOVIA DIVALDO SRUAGY, KM 12- VIA 6'
        }
    var PE ={
            'UF'        : 'PE',
            'municipio' : 'CABO DE SANTO AGOSTINHO',
            'end'       : 'MARGEM ROD BR 101 ZONA RURAL'
        }
    var RN ={
            'UF'        : 'RN',
            'municipio' : 'ALTO DOS RODRIGUES',
            'end'       : '*******'
        }

    dados_unidades['MG'] = MG;
    dados_unidades['MC'] = MC;
    dados_unidades['RJ'] = RJ;
    dados_unidades['SG'] = SG;
    dados_unidades['BA'] = BA;
    dados_unidades['AL'] = AL;
    dados_unidades['PE'] = PE;
    dados_unidades['RN'] = RN;

    console.log(dados_unidades);

    $('#table_rel_fpft').DataTable({
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 3 ] }
        ],
    });

    $("#bt_carregar").click(function(event) {
    	var data_ini = $("#data_ini").val();
    	var data_fim = $("#data_fim").val();
    	
	 	$.post('/fontesporfrente/carregarrelatorio', {
	    	_token  : token,
	    	data_ini  : data_ini,
	    	data_fim  : data_fim,
	    	
	    }, function(data) {    
	    	if(data.success){
                $('#table_gerar_relatorio').DataTable().destroy();
	    		$('#table_gerar_relatorio tbody').empty();
                var header = $('#table_gerar_relatorio tbody').empty();
	    		$.each(data.empresas,function(index,item){
                    var fontes_cliente = data.fontes_clientes[item.cli_id];
                    var outros_dados = data.outros_dados_clientes[item.cli_id];
                    var monitor = '';
                    if(typeof(data.monitores_clientes[item.cli_id]) != "undefined" && data.monitores_clientes[item.cli_id] !== null){
                        monitor =  data.monitores_clientes[item.cli_id];
                    }else{
                        monitor = 'Não Definido';
                    }
	    			$('#table_gerar_relatorio tbody').append(
                        '<tr>'+
                            '<td>'+item.cli_nome+'</td>'+
                            '<td>'+item.cli_endereco+'</td>'+
                            '<td>'+item.cli_municipio+'</td>'+
                            '<td>'+item.cli_estado+'</td>'+
                            '<td>'+data.responsaveis_empresa[item.cli_id]+'</td>'+
                            '<td>'+data.responsaveis_prestadora[item.cli_id]+'</td>'+
                            '<td>'+fontes_cliente['irradiador']+'</td>'+
                            '<td>'+monitor+'</td>'+
                            '<td>'+outros_dados['data_ini']+'</td>'+
                            '<td>'+outros_dados['data_fim']+'</td>'+
                            '<td>M</td>'+
                            '<td>'+fontes_cliente['numero']+'</td>'+
                            '<td>'+outros_dados['periodo']+'</td>'+
                        '</tr>'
                    );	
	    		});
                

                $.each(data.qualitec,function(index,item){
                    console.log(item);
                    var dados = dados_unidades[item.guia_unidade];
                    console.log(dados);
                    var fontes_cliente = data.fontes_qualitec[item.cli_id];
                    var outros_dados = data.outros_dados_qualitec[item.cli_id];


                    if(typeof(data.monitores_qualitec[item.cli_id]) != "undefined" && data.monitores_qualitec[item.cli_id] !== null){
                        monitor =  data.monitores_qualitec[item.cli_id];
                    }else{
                        monitor = 'Não Definido';
                    }
                    $('#table_gerar_relatorio tbody').append(
                        '<tr>'+
                            '<td>QUALITEC ENG</td>'+
                            '<td>'+dados.end+'</td>'+
                            '<td>'+dados.municipio+'</td>'+
                            '<td>'+dados.UF+'</td>'+
                            '<td>COORDENAÇÃO</td>'+
                            '<td>'+data.responsaveis_prestadora_qt[item.cli_id]+'</td>'+
                            '<td>'+fontes_cliente['irradiador']+'</td>'+
                            '<td>'+monitor+'</td>'+
                            '<td>'+outros_dados['data_ini']+'</td>'+
                            '<td>'+outros_dados['data_fim']+'</td>'+
                            '<td>F</td>'+
                            '<td>'+fontes_cliente['numero']+'</td>'+
                            '<td>'+outros_dados['periodo']+'</td>'+
                        '</tr>'
                    );  

                });


                $('#table_gerar_relatorio').DataTable({
                    "responsive": true,
                    "iDisplayLength": 100,
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons": ['xls'],
                    },
                });
	    	}	  		
	    });
 	});
    
});//end main
