$(document).ready(function($) {
	var token     = $("#token").val();
	listarUsuarios();
	$("#bt_salvar_Usuario").click(function(event) {
		var permissoes = [];
		$.each($(".checkpermi"), function(id , val){
	      if($(val).is(":checked")){
	      	permissoes.push($(val).prop('id'));
	      };
	    });
	    var permop = [];
	    $.each($(".checkop"), function(id , val){
	      if($(val).is(":checked")){
	      	permop.push($(val).prop('id').replace('perop',''));
	      };
	    });
	    
	    var funcionario = $("#funcionario").val();
	    var login       = $("#login").val();
	    var email       = $("#email").val();
	    var senha       = $("#senha").val();
	    var confsenha   = $("#confsenha").val();
	    var un          = $("#un").val();
	    $.post('/adm/user/salvarusuario', {
				_token     : token,
				nome       : funcionario,
				login      : login,
				email      : email,
				password   : confsenha,
				unidade    : un,
				permissoes : permissoes,
				permop     : permop,
			}, function(data) {
				alert(data.mensagem);
				location.href="/adm";
			}
		);
	});
	$("#bt_alterar_Usuario").click(function(event) {
		var permissoes = [];
		$.each($(".checkpermi"), function(id , val){
	      if($(val).is(":checked")){
	      	permissoes.push($(val).prop('id'));
	      };
	    });
	    var permop = [];
	    $.each($(".checkop"), function(id , val){
	      if($(val).is(":checked")){
	      	permop.push($(val).prop('id').replace('perop',''));
	      };
	    });
	    
	    var funcionario = $("#funcionario").val();
	    var email       = $("#email").val();
	    var login       = $("#login").val();
	    var senha       = $("#senha").val();
	    var confsenha   = $("#confsenha").val();
	    var un          = $("#un").val();
	    var id          = $("#id").val();

	    $.post('/adm/user/alterarusuario', {
				_token     : token,
				nome       : funcionario,
				login      : login,
				email      : email,
				password   : confsenha,
				unidade    : un,
				permissoes : permissoes,
				id         : id,
				permop     : permop,
			}, function(data) {
				alert(data.mensagem);
				location.href="/adm";	
			}
		);
	});
	

	function listarUsuarios(){
		$.post('/adm/user/listarusuarios', {
			_token  : token,
		}, function(data) {
			var users = data.users;
			$("#listaUsuarios").empty();
			users.forEach(function(element, index){

				if(element.deleted_by == null){
					$("#listaUsuarios").append('<tr>'+
					'<td class="tblabel">'+element.id+'</td>'+
					'<td class="tblabel">'+element.nome+'</td>'+
					'<td class="tblabel">'+element.email+'</td>'+
					'<td class="tblabel">'+element.login+'</td>'+
					'<td class="tblabel">'+element.created_at+'</td>'+
					'<td class="tblabel"><a href="/adm/user/editar/'+element.id+'">Editar</a> | <a href="" class="usuarios" id="'+element.id+'">Excluir</a></td>'+
				'</tr>');
				}
			});
			$('.usuarios').click(function(event) {
				var id = $(this).prop('id');

				var confi = confirm('Confirmar Exclussão');
				if (confi) {
					var tokenn     = $("#token").val();
					$.post('/adm/excluirUsuario', {
						_token  : tokenn,
						id      : id,
					}, function(data) {
						if(data.success){
							alert(data.mensagem);
							location.href="/adm/user/editar";	
						}
					});
				};
			});
		});
	}
	
});