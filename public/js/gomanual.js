$(document).ready(function() {
	var token     = $("#token").val();
	var id_guia   = $("#id_guia").val();
	var handsontable = '';
	var $container = $("#example1");
	
    if(id_guia){
    	carregaGOManual();
    }else{
	    $container.handsontable({
	      	startRows: 7,
	      	maxRows: 40,
	      	startCols: 28,
	      	colHeaders: ["Item", "Identificação Desenho/Isométrico","Mapa/Spool", "Junta", "Posição de Soldagem", "Material/Metal Base", "Ø / Comprimento", "Espessura Nominal", "Chanfro", "Sinete Raiz", "Sinete Ench/Acab","Metal Adição", "Processo Raiz",  "Processo Ench/Acab",   "Nível/Classe Inspeção", "Norma", "Criterio de Aceitação","Obra", "Procedimento", "DFF", "Técnica", "Tipo IQI","Fio IQI", "Quantidade de Filmes", "Filme Classe", "Filme Dimensão","Reforço", "Observação"],
	      	columns: [
		        {},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}
	    	],
	    });
        handsontable = $container.data('handsontable');
	}
    $("#save").click(function() {
    	var t = $("#token").val();
    	var unidade = $("#unidade").val();
    	var datat = $("#data").val();
    	var cliente_id = $("#cliente").val();
    	var proposta_id = $("#proposta").val();
    	var op1 = $("#op1").val();
    	var op2 = $("#op2").val();
    	var aux = $("#aux").val();
    	var ins = $("#ins").val();
    	var fonte_id = $("#fonte").val();
    	var fonte_atividade = $("#atividade").text();
    	var local = $("#local").val();
    	var contratante_fabricante = $("#contratante_fabricante").val();
		var data_prog = $("#data_prog").val();
		var controle_cliente = $("#controle_cliente").val();
		var dados = new Array();
		var obsmanual = $("#obsmanual").val();
		var projeto = $("#projeto").val();
		if(obsmanual.length == 0){
			alert('Favor preencher o motivo!');
		}else{
			for(var i = 0; i < 40; i++ ) 
          	{	
          		var data = handsontable.getDataAtRow(i);
          		if(!data[0]){
          		}else{
          			dados[data[0]] = data;
          		}
         	 }
         	if(dados.length == 0){
				alert('Programação em branco');
			}else{
         	 	$.post('/guia/salvarmanual', {
            		data: dados,
            		_token  : t,
            		guia_id : id_guia,
            		unidade : unidade,
            		datat : datat,
            		cliente_id : cliente_id,
            		proposta_id : proposta_id,
            		op1:op1,
            		op2:op2,
            		aux:aux,
            		ins:ins,
            		fonte_id:fonte_id,
            		fonte_atividade : fonte_atividade,
            		local:local,
            		contratante_fabricante:contratante_fabricante,
            		data_prog : data_prog,
            		controle_cliente : controle_cliente,
            		obsmanual : obsmanual,
            		projeto:projeto,
            	},function(data){
            		if(data.success){
            			alert('Guia Operacional Salva com sucesso!');
	    				location.href="/guia/"+data.idguia+"/visualizar";
            		}
            		
            	});
            }
		}
    });

	function carregaGOManual(){
		$.post('/guia/getguiamanual', {
	    	id              : id_guia,
	    	_token          : token,
	    }, function(data) {
	    	var prog = [];

	    	$.each(data.programacoes,function(index,item){
	    		console.log(item);
	    		var itens = [item.item,item.identificacao,item.spool,item.junta,item.posicao_soldagem,item.metal_base,item.diametro,
	    			item.espessura,item.chanfro,item.sinete_raiz,item.sinete_acabamento,item.metal_adicao,item.processo_raiz,
	    			item.processo_acabamento,item.nivel_inspecao,item.norma,item.criterio_aceitacao,item.obra,item.procedimento,
	    			item.dff,item.tecnica,item.tipo_iqi,item.fio_iqi,item.quantidade_filmes,item.filme_classe,item.filme_dimensao,item.reforco,item.observacao];
	    		prog.push(itens);
	    	});          
	    	$container.handsontable({
	    		data : prog,
		      	startRows: 7,
		      	maxRows: 40,
		      	startCols: 28,
		      	colHeaders: ["Item", "Identificação Desenho/Isométrico","Mapa/Spool", "Junta", "Posição de Soldagem", "Material/Metal Base", "Ø / Comprimento", "Espessura Nominal", "Chanfro", "Sinete Raiz", "Sinete Ench/Acab","Metal Adição", "Processo Raiz",  "Processo Ench/Acab",   "Nível/Classe Inspeção", "Norma", "Criterio de Aceitação","Obra", "Procedimento", "DFF", "Técnica", "Tipo IQI","Fio IQI", "Quantidade de Filmes", "Filme Classe", "Filme Dimensão","Reforço", "Observação"],
		    });
		    handsontable = $container.data('handsontable');
	    });
	}
});