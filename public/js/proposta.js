$(document).ready(function() { 
	var id = '';  
	var token     = $("#token").val();
		
	// $(".mask-cod_proposta").mask("aa aa 999a/9999");
	
	$(".mask-telefone").mask("(99) 9999-9999");

	carregarCliente();

	$("#cliente").change(function(event) {
		var id = $(this).val();
		$.post('/proposta/dadoscliente', {
			_token  : token,
			id      : id,
		}, function(data) {
			if(data.success){
				var cliente = data.cliente;
				$("#nome_reduz").text(cliente.nome_reduz);
				$("#estado").text(cliente.estado);
				$("#municipio").text(cliente.municipio);
			}
		});
	});	
    $("#bt_salvar_proposta").click(function(event) {
    	var validacao = false;
    	var id_proposta = $("#id_proposta").val();
    	var codproposta = $("#codproposta").val();
    	var data = $("#data").val();
    	var validade = $("#validade").val();
    	var propostaantiga = $("#propostaantiga").val();
    	var idCliente = $("#cliente").val();
    	console.log(idCliente);
    	var servico = $("#servico").val();
		var serEstado = $("#serEstado").val();
		var serMunicipio = $("#serMunicipio").val();
		var contatosoli = $("#contatosoli").val();
		var contatoapro = $("#contatoapro").val();
		var contatopro = $("#contatopro").val();
		var emailsoli = $("#emailsoli").val();
		var emailapro = $("#emailapro").val();
		var emailpro = $("#emailpro").val();
		var telsoli = $("#telsoli").val();
		var telapro = $("#telapro").val();
		var telpro = $("#telpro").val();
		var iss = $("#iss").val();

		

		var arrayforma_pagamento = new Array();
		if( $('.check:checked').length == 0 ){
			arrayforma_pagamento = null;
		}else{
			$('.check:checked').each(function(){
				arrayforma_pagamento.push($(this).val());
			});
		}


		if(codproposta && data && validade &&  idCliente != 0){
			validacao = true;
		}else{
			var mensagem = '';
			if(!codproposta)  mensagem += '-Codigo de proposta é obrigatorio\n';
			if(!data)  mensagem += '-Data é obrigatorio\n';
			if(!validade)  mensagem += '-Validade é obrigatorio\n';
			if(idCliente == 0)  mensagem += '-Cliente é obrigatorio\n';
			alert(mensagem);
		}

		var arrayjornadanormal = [];
		$.each($(".jornadanormal"), function(id , val){
	      	var idjornadaNormal = $(val).prop('id').replace('normal','');
	      	var valorn = $(val).val();
	      	arrayjornadanormal[idjornadaNormal] = valorn;
	    });
		console.log(arrayjornadanormal);
	    var arrayjornadaextra = [];
	    $.each($(".jornadaextra"), function(id , val){
	      	
	      	var idjornadaExtra = $(val).prop('id').replace('extra','');
	      	var valore = $(val).val();
	        arrayjornadaextra[idjornadaExtra] = valore;
	    });

	    var filmedereparo = $("#filmedereparo").val();
	    var arrayfilmes = [];
	    $.each($(".filme"), function(id , val){
	      	var idfilme = $(val).prop('id').replace('filme','');
	      	var valor = $(val).val();
	        arrayfilmes[idfilme] = valor;
	    });

	    // var valorchapa = $("#valorchapa").val();
	    var arrayjuntas = [];
	    $.each($(".junta"), function(id , val){
	      	var idjunta = $(val).prop('id').replace('junta','');
	      	var valor = $(val).val();
	        arrayjuntas[idjunta] = valor;
	    });

	    var arrayservico = [];
	    $.each($(".servico"), function(id , val){
	      	var idservico = $(val).prop('id').replace('servico','');
	      	var valor = $(val).val();
	        arrayservico[idservico] = valor;
	    });
	    if(validacao){
	    	$.post('/proposta/salvar', {
				_token  : token,
				id_proposta : id_proposta,
				cod_proposta : codproposta,
				data : data,
				validade : validade,
				cod_proposta_anterior : propostaantiga,
				cliente_id : idCliente,
				servico : servico,
				estado : serEstado,
				municipio : serMunicipio,
				solicitacao_contato : contatosoli,
				aprovacao_contato : contatoapro,
				projeto_contato : contatopro,
				solicitacao_email : emailsoli,
				aprovacao_email : emailapro,
				projeto_email : emailpro,
				solicitacao_tel : telsoli,
				aprovacao_tel : telapro,
				projeto_tel : telpro,
				arrayjornadanormal : arrayjornadanormal,
				arrayjornadaextra : arrayjornadaextra,
				filmedereparo : filmedereparo,
				arrayfilmes : arrayfilmes,
				// valorchapa : valorchapa,
				arrayjuntas : arrayjuntas,
				arrayservico : arrayservico,
				iss : iss,
				arrayforma_pagamento : arrayforma_pagamento,
			}, function(data) {
				if(data.success){
					alert(data.mensagem);
					location.href="/ensaio/proposta";		
				}
			});
	    }
    });

	function carregarCliente(){
		var id_cliente = $("#cliente_id").val();
		$.post('/proposta/dadoscliente', {
			_token  : token,
			id      : id_cliente,
		}, function(data) {
			if(data.success){
				var cliente = data.cliente;
				$("#cliente").val(id_cliente).change();
				$("#nome_reduz").text(cliente.nome_reduz);
				$("#estado").text(cliente.estado);
				$("#municipio").text(cliente.municipio);
			}
		});
	}
	
 });