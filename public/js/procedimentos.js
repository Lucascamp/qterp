$(document).ready(function($) {
	var token = $("#token").val();

	$("#bt_novo_procedimento").click(function(event) {
		$('#titulom').empty();
		$('#titulom').text('Cadastro de Procedimento');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Procedimento</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<input type="text" class="form-control" id="procedimento" placeholder="Nome Procedimento">'+
                            '</td>'+                                            
                        '</tr>'+
                    '</table>';
        $('#bodym').html(table);
        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
        			 '<button type="button" class="btn btn-primary" id="bt_salvar_procedimento">Salvar</button>';
       	$('#footerm').html(footer);
		$('#myModal').modal('toggle');

		$("#bt_salvar_procedimento").click(function(event) {
			var procedimento = $("#procedimento").val();
			$.post('/procedimento/salvarprocedimento',{
				_token 			 : token,
				procedimento 	 : procedimento,
			},
			function(data){
				if(data.success){
					alert(data.mensagem);
					location.href="/ensaio/procedimento";
				}
			}
			);
		});
	});

	$(".bt_editar_procedimento").click(function(event) {
		var id = $(this).prop('id');
		var textoPro = $(".textoPro"+id).text();
		$('#titulom').empty();
		$('#titulom').text('Cadastro de Procedimento');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Procedimento</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<input type="text" class="form-control" id="procedimento" value="'+textoPro+'">'+
                            '</td>'+                                            
                        '</tr>'+
                    '</table>';
        $('#bodym').html(table);
        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
        			 '<button type="button" class="btn btn-primary" id="bt_editar_procedimento">Salvar Procedimento</button>';
       	$('#footerm').html(footer);
		$('#myModal').modal('toggle');

		$("#bt_editar_procedimento").click(function(event) {
			var procedimento = $("#procedimento").val();
			$.post('/procedimento/salvarprocedimento',{
				_token 			 : token,
				procedimento 	 : procedimento,
				id               : id
			},
			function(data){
				if(data.success){
					alert(data.mensagem);
					location.href="/ensaio/procedimento";
				}
			}
			);
		});
	});

	$("#bt_novo_grupo").click(function(event) {
		$('#titulom').empty();
		$('#titulom').text('Cadastro de Grupos de Marerial Base');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Grupo</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<input type="text" class="form-control" id="grupo" placeholder="Nome Grupo">'+
                            '</td>'+                                            
                        '</tr>'+
                    '</table>';
        $('#bodym').html(table);
        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
        			 '<button type="button" class="btn btn-primary" id="bt_salvar_grupo">Salvar</button>';
       	$('#footerm').html(footer);
		$('#myModal').modal('toggle');

		$("#bt_salvar_grupo").click(function(event) {
			var grupo = $("#grupo").val();
			$.post('/procedimento/salvargrupo',{
				_token 			 : token,
				grupo 	         : grupo,
			},
			function(data){
				if(data.success){
					alert(data.mensagem);
					location.href="/ensaio/procedimento";
				}
			}
			);
		});
	});
	
	$(".bt_editar_grupo").click(function(event) {
		var id = $(this).prop('id');
		var textoGru = $(".textoGru"+id).text();
		$('#titulom').empty();
		$('#titulom').text('Cadastro de Grupos de Marerial Base');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Grupo</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<input type="text" class="form-control" id="grupo" value="'+textoGru+'">'+
                            '</td>'+                                            
                        '</tr>'+
                    '</table>';
        $('#bodym').html(table);
        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
        			 '<button type="button" class="btn btn-primary" id="bt_editar_grupo">Salvar Grupo</button>';
       	$('#footerm').html(footer);
		$('#myModal').modal('toggle');

		$("#bt_editar_grupo").click(function(event) {
			var grupo = $("#grupo").val();
			$.post('/procedimento/salvargrupo',{
				_token 			 : token,
				grupo 	         : grupo,
				id               : id
			},
			function(data){
				if(data.success){
					alert(data.mensagem);
					location.href="/ensaio/procedimento";
				}
			}
			);
		});
	});

	$("#bt_nova_norma").click(function(event) {
		$('#titulom').empty();
		$('#titulom').text('Cadastro de Norma');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Procedimento</td>'+
                            '<td>Norma</td>'+
                            '<td>Criterio Aceite</td>'+
                            '<td>Objeto</td>'+
                            '<td>Aplicação</td>'+
                            '<td>Grupo Material Base</td>'+
                        '</tr>'+
                        '<tr>';

        $.post('/procedimento/dadosnormas',{
				_token 			    : token,
			},
			function(data){
				if(data.success){
					table +='<td><select class="form-control select_norma" id="procedimento">';
					console.log(data.procedimentos);
					$.each(data.procedimentos,function(index,item){
			    		table += '<option value="'+item.id+'">'+item.procedimento+'</option>'
			  		});
					table +='</select></td>';
					table +='<td><input type="text" class="form-control" id="norma"></td>'+
		                '<td><input type="text" class="form-control" id="criterio_aceite"></td>'+
		                '<td><input type="text" class="form-control" id="objeto"></td>'+
		                '<td><input type="text" class="form-control" id="aplicacao"></td>';
		           	table += '<td><select class="form-control tbinput" id="grupo_material_base">';
		           	$.each(data.grupos,function(index,item){
			    		table += '<option value="'+item.id+'">'+item.grupo+'</option>'
			  		});
			  		table +='</select></td>'+
			            '</tr>'+
			        '</table>';
			        $('#bodym').html(table);
			        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
			        			 '<button type="button" class="btn btn-primary" id="bt_salvar_norma">Salvar</button>';
			       	$('#footerm').html(footer);
					$('#myModal').modal('toggle');
					$("#bt_salvar_norma").click(function(event) {
						var procedimento = $("#procedimento").val();
						var norma = $("#norma").val();
						var criterio_aceite = $("#criterio_aceite").val();
						var objeto = $("#objeto").val();
						var aplicacao = $("#aplicacao").val();
						var grupo_material_base = $("#grupo_material_base").val();

						$.post('/procedimento/salvarnorma',{
							_token 			    : token,
							procedimento_id     : procedimento,
							norma               : norma,
							criterio_aceite     : criterio_aceite,
							objeto              : objeto,
							aplicacao           : aplicacao,
							grupo_material_base : grupo_material_base,
						},
						function(data){
							if(data.success){
								alert(data.mensagem);
								location.href="/ensaio/procedimento";
							}
						}
						);
					});
				}
			}
		);
	});

	$(".bt_editar_norma").click(function(event) {
		var id = $(this).prop('id');
		var textoGru = $(".textoNor"+id).text();
		$('#titulom').empty();
		$('#titulom').text('Editar Norma');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Procedimento</td>'+
                            '<td>Norma</td>'+
                            '<td>Criterio Aceite</td>'+
                            '<td>Objeto</td>'+
                            '<td>Aplicação</td>'+
                            '<td>Grupo Material Base</td>'+
                        '</tr>'+
                        '<tr>';
        $.post('/procedimento/dadosnormas',{
				_token 			    : token,
				id                  : id,
			},
			function(data){
				if(data.success){
					table +='<td><select class="form-control select_norma" id="procedimento">';
					if(data.norma){
						table += '<option value="'+data.norma.procedimento_id+'">'+data.procedimentos[data.norma.procedimento_id-1].procedimento+'</option>'
					}
					$.each(data.procedimentos,function(index,item){
			    		table += '<option value="'+item.id+'">'+item.procedimento+'</option>'
			  		});
					table +='</select></td>';
					table +='<td><input type="text" class="form-control" id="norma" value="'+data.norma.norma+'"></td>'+
		                '<td><input type="text" class="form-control" id="criterio_aceite"  value="'+data.norma.criterio_aceite+'"></td>'+
		                '<td><input type="text" class="form-control" id="objeto"  value="'+data.norma.objeto+'"></td>'+
		                '<td><input type="text" class="form-control" id="aplicacao"  value="'+data.norma.aplicacao+'"></td>';
		           	table += '<td><select class="form-control tbinput" id="grupo_material_base">';
		           	if(data.norma){
						table += '<option value="'+data.norma.grupo_material_base+'">'+data.grupos[data.norma.grupo_material_base-1].grupo+'</option>'
					}
		           	$.each(data.grupos,function(index,item){
			    		table += '<option value="'+item.id+'">'+item.grupo+'</option>'
			  		});
			  		table +='</select></td>'+
			            '</tr>'+
			        '</table>';
			        $('#bodym').html(table);
			        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
			        			 '<button type="button" class="btn btn-primary" id="bt_editar_norma">Salvar</button>';
			       	$('#footerm').html(footer);
					$('#myModal').modal('toggle');

					$("#bt_editar_norma").click(function(event) {
						var procedimento = $("#procedimento").val();
						var norma = $("#norma").val();
						var criterio_aceite = $("#criterio_aceite").val();
						var objeto = $("#objeto").val();
						var aplicacao = $("#aplicacao").val();
						var grupo_material_base = $("#grupo_material_base").val();

						$.post('/procedimento/salvarnorma',{
							_token 			    : token,
							procedimento_id     : procedimento,
							norma               : norma,
							criterio_aceite     : criterio_aceite,
							objeto              : objeto,
							aplicacao           : aplicacao,
							grupo_material_base : grupo_material_base,
							id                  : id
						},
						function(data){
							if(data.success){
								alert(data.mensagem);
								location.href="/ensaio/procedimento";
							}
						}
						);
					});
				}
			}
		);
	});

	$("#bt_novo_parametroprocedimento").click(function(event) {
		$('#titulom').empty();
		$('#titulom').text('Cadastro de Norma');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Procedimento</td>'+
                            '<td>Ø Inicial</td>'+
                            '<td>Ø Final</td>'+
                            '<td>Comprimento Inicial</td>'+
                            '<td>Comprimento Final</td>'+
                            '<td>Espessura Inicial</td>'+
                            '<td>Espessura Final</td>'+
                        '</tr>'+
                        '<tr>';

        $.post('/procedimento/dadosparametro',{
				_token 			    : token,
			},
			function(data){
				if(data.success){
					table +='<td><select class="form-control select_norma" id="procedimento">';
					$.each(data.procedimentos,function(index,item){
			    		table += '<option value="'+item.id+'">'+item.procedimento+'</option>'
			  		});
					table +='</select></td>';
					table +='<td><input type="text" class="form-control" id="diametro_ini"></td>'+
		                '<td><input type="text" class="form-control" id="diametro_fim"></td>'+
		                '<td><input type="text" class="form-control" id="comprimento_ini"></td>'+
		                '<td><input type="text" class="form-control" id="comprimento_fim"></td>'+
		           		'<td><input type="text" class="form-control" id="espessura_ini"></td>'+
		           		'<td><input type="text" class="form-control" id="espessura_fim"></td>'+
			            '</tr>'+
			            '<tr>'+
                            '<td>Tipo Fonte</td>'+
                            '<td>Foco Fonte</td>'+
                            '<td>Tecnica</td>'+
                            '<td>DFF</td>'+
                            '<td>Tipo IQI</td>'+
                            '<td>Fio IQI</td>'+
                            '<td>Classe Filme</td>'+
                        '</tr>'+
                        '<tr>'+
                        '<td>'+
                        	'<select class="form-control" id="tipo_fonte">'+
                        		'<option>GAMA</option>'+
                        		'<option>RAIO X</option>'+
                        	'</select>'+
                        '</td>'+
		                '<td><input type="text" class="form-control" id="foco_fonte"></td>'+
		                '<td><input type="text" class="form-control" id="tecnica"></td>'+
		                '<td><input type="text" class="form-control" id="dff"></td>'+
		           		'<td><input type="text" class="form-control" id="tipo_iqi"></td>'+
		           		'<td><input type="text" class="form-control" id="fio_iqi"></td>'+
		           		'<td>'+
                        	'<select class="form-control" id="classe_filme">'+
                        		'<option>I</option>'+
                        		'<option>Esp</option>'+
                        		'<option>II</option>'+
                        	'</select>'+
                        '</td>'+
			            '</tr>'+
			            '<tr>'+
			            	'<td>Dimensão Filme</td>'+
                            '<td>Quantidade FIlmes</td>'+
                        '</tr>'+
                        '<tr>'+
                        '<td><select class="form-control" id="dimensao_filme">';
                        $.each(data.filmes,function(index,item){
                        	
			    			table += '<option value="'+item[0].legenda+'">'+item[0].legenda+' - '+item[0].descricao+'</option>';
			  			});
                        table += '</select></td>'+
		           				'<td><input type="text" class="form-control" id="quantidade_filmes"></td>'+
		           		'</tr>'+
			        '</table>';
			        $('#bodym').html(table);
			        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
			        			 '<button type="button" class="btn btn-primary" id="bt_salvar_parametrizacaoprocedimento">Salvar</button>';
			       	$('#footerm').html(footer);
					$('#myModal').modal('toggle');
					$("#bt_salvar_parametrizacaoprocedimento").click(function(event) {
						var procedimento = $("#procedimento").val();
						var diametro_ini = $("#diametro_ini").val();
						var diametro_fim = $("#diametro_fim").val();
						var comprimento_ini = $("#comprimento_ini").val();
						var comprimento_fim = $("#comprimento_fim").val();
						var espessura_ini = $("#espessura_ini").val();
						var espessura_fim = $("#espessura_fim").val();
						var tipo_fonte = $("#tipo_fonte").val();
						var foco_fonte = $("#foco_fonte").val();
						var tecnica = $("#tecnica").val();
						var dff = $("#dff").val();
						var tipo_iqi = $("#tipo_iqi").val();
						var fio_iqi = $("#fio_iqi").val();
						var classe_filme = $("#classe_filme").val();
						var dimensao_filme = $("#dimensao_filme").val();
						var quantidade_filmes = $("#quantidade_filmes").val();

						$.post('/procedimento/salvarparametroprocedimento',{
							_token 			    : token,
							procedimento_id     : procedimento,
							diametro_ini : diametro_ini,
							diametro_fim : diametro_fim,
							comprimento_ini : comprimento_ini,
							comprimento_fim : comprimento_fim,
							espessura_ini : espessura_ini,
							espessura_fim : espessura_fim,
							tipo_fonte : tipo_fonte,
							foco_fonte : foco_fonte,
							tecnica : tecnica,
							dff : dff,
							tipo_iqi : tipo_iqi,
							fio_iqi : fio_iqi,
							classe_filme : classe_filme,
							dimensao_filme : dimensao_filme,
							quantidade_filmes : quantidade_filmes,
						},
						function(data){
							if(data.success){
								alert(data.mensagem);
								location.href="/ensaio/procedimento";
							}
						}
						);
					});
				}
			}
		);
	});
	
	$(".bt_editar_parametro").click(function(event) {
		var id = $(this).prop('id');
		$('#titulom').empty();
		$('#titulom').text('Editar Parametro');
		$('#bodym').empty();
		$('#footerm').empty();
		var table = '<table class="table table-bordered table_procedimento">'+
                        '<tr>'+
                            '<td>Procedimento</td>'+
                            '<td>Ø Inicial</td>'+
                            '<td>Ø Final</td>'+
                            '<td>Comprimento Inicial</td>'+
                            '<td>Comprimento Final</td>'+
                            '<td>Espessura Inicial</td>'+
                            '<td>Espessura Final</td>'+
                        '</tr>'+
                        '<tr>';
          $.post('/procedimento/dadosparametro',{
				_token 			    : token,
				id                  : id,
			},
			function(data){
				if(data.success){
					table +='<td><select class="form-control select_norma" id="procedimento">';
					table += '<option value="'+data.parametro.procedimento_id+'">'+data.procedimentos[data.parametro.procedimento_id-1].procedimento+'</option>';
					$.each(data.procedimentos,function(index,item){
			    		table += '<option value="'+item.id+'">'+item.procedimento+'</option>'
			  		});
					table +='</select></td>';
					table +='<td><input type="text" class="form-control" id="diametro_ini" value="'+data.parametro.diametro_ini+'"></td>'+
		                '<td><input type="text" class="form-control" id="diametro_fim" value="'+data.parametro.diametro_fim+'"></td>'+
		                '<td><input type="text" class="form-control" id="comprimento_ini" value="'+data.parametro.comprimento_ini+'"></td>'+
		                '<td><input type="text" class="form-control" id="comprimento_fim" value="'+data.parametro.comprimento_fim+'"></td>'+
		           		'<td><input type="text" class="form-control" id="espessura_ini" value="'+data.parametro.espessura_ini+'"></td>'+
		           		'<td><input type="text" class="form-control" id="espessura_fim" value="'+data.parametro.espessura_fim+'"></td>'+
			            '</tr>'+
			            '<tr>'+
                            '<td>Tipo Fonte</td>'+
                            '<td>Foco Fonte</td>'+
                            '<td>Tecnica</td>'+
                            '<td>DFF</td>'+
                            '<td>Tipo IQI</td>'+
                            '<td>Fio IQI</td>'+
                            '<td>Classe Filme</td>'+
                        '</tr>'+
                        '<tr>'+
                        '<td>'+
                        	'<select class="form-control" id="tipo_fonte">'+
                        		'<option>'+data.parametro.tipo_fonte+'</option>'+
                        		'<option>GAMA</option>'+
                        		'<option>RAIO X</option>'+
                        	'</select>'+
                        '</td>'+
		                '<td><input type="text" class="form-control" id="foco_fonte" value="'+data.parametro.foco_fonte+'"></td>'+
		                '<td><input type="text" class="form-control" id="tecnica" value="'+data.parametro.tecnica+'"></td>'+
		                '<td><input type="text" class="form-control" id="dff" value="'+data.parametro.dff+'"></td>'+
		           		'<td><input type="text" class="form-control" id="tipo_iqi" value="'+data.parametro.tipo_iqi+'"></td>'+
		           		'<td><input type="text" class="form-control" id="fio_iqi" value="'+data.parametro.fio_iqi+'"></td>'+
		           		'<td>'+
                        	'<select class="form-control" id="classe_filme">'+
                        		'<option>'+data.parametro.classe_filme+'</option>'+
                        		'<option>I</option>'+
                        		'<option>Esp</option>'+
                        		'<option>II</option>'+
                        	'</select>'+
                        '</td>'+
			            '</tr>'+
			            '<tr>'+
			            	'<td>Dimensão Filme</td>'+
                            '<td>Quantidade FIlmes</td>'+
                        '</tr>'+
                        '<tr>'+
                        '<td><select class="form-control" id="dimensao_filme">'+
                        	'<option>'+data.parametro.dimensao_filme+'</option>';
                        $.each(data.filmes,function(index,item){
			    			table += '<option value="'+item[0].legenda+'">'+item[0].legenda+' - '+item[0].descricao+'</option>';
			  			});
                        table += '</select></td>'+
		           				'<td><input type="text" class="form-control" id="quantidade_filmes"  value="'+data.parametro.quantidade_filmes+'"></td>'+
		           		'</tr>'+
			        '</table>';
			        $('#bodym').html(table);
			        var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
			        			 '<button type="button" class="btn btn-primary" id="bt_salvar_parametrizacaoprocedimento">Salvar</button>';
			       	$('#footerm').html(footer);
					$('#myModal').modal('toggle');
					$("#bt_salvar_parametrizacaoprocedimento").click(function(event) {
						var procedimento = $("#procedimento").val();
						var diametro_ini = $("#diametro_ini").val();
						var diametro_fim = $("#diametro_fim").val();
						var comprimento_ini = $("#comprimento_ini").val();
						var comprimento_fim = $("#comprimento_fim").val();
						var espessura_ini = $("#espessura_ini").val();
						var espessura_fim = $("#espessura_fim").val();
						var tipo_fonte = $("#tipo_fonte").val();
						var foco_fonte = $("#foco_fonte").val();
						var tecnica = $("#tecnica").val();
						var dff = $("#dff").val();
						var tipo_iqi = $("#tipo_iqi").val();
						var fio_iqi = $("#fio_iqi").val();
						var classe_filme = $("#classe_filme").val();
						var dimensao_filme = $("#dimensao_filme").val();
						var quantidade_filmes = $("#quantidade_filmes").val();

						$.post('/procedimento/salvarparametroprocedimento',{
							_token 			    : token,
							procedimento_id     : procedimento,
							diametro_ini : diametro_ini,
							diametro_fim : diametro_fim,
							comprimento_ini : comprimento_ini,
							comprimento_fim : comprimento_fim,
							espessura_ini : espessura_ini,
							espessura_fim : espessura_fim,
							tipo_fonte : tipo_fonte,
							foco_fonte : foco_fonte,
							tecnica : tecnica,
							dff : dff,
							tipo_iqi : tipo_iqi,
							fio_iqi : fio_iqi,
							classe_filme : classe_filme,
							dimensao_filme : dimensao_filme,
							quantidade_filmes : quantidade_filmes,
							id : id
						},
						function(data){
							if(data.success){
								alert(data.mensagem);
								location.href="/ensaio/procedimento";
							}
						}
						);
					});
				}
			}
		);
	});

});