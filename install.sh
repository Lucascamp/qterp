#!/usr/bin/env bash

echo "--- Good morning, master. Let's get to work. Installing now. ---"

echo "--- Updating packages list ---"
sudo apt-get update

echo "--- MySQL time ---"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

echo "--- Installing base packages ---"
sudo apt-get install -y vim curl python-software-properties

echo "--- Updating packages list ---"
sudo apt-get update

echo "--- We want the bleeding edge of PHP, right master? ---"
sudo add-apt-repository -y ppa:ondrej/php5

echo "--- Updating packages list ---"
sudo apt-get update

echo "--- Installing PHP-specific packages ---"
sudo apt-get install -y php5 apache2 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt php5-readline mysql-server-5.5 php5-mysql git-core

echo "--- Installing Node.js ---"
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install -y nodejs

echo "--- Installing and configuring Xdebug ---"
sudo apt-get install -y php5-xdebug

cat << EOF | sudo tee -a /etc/php5/mods-available/xdebug.ini
xdebug.scream=1
xdebug.cli_color=1
xdebug.show_local_vars=1
EOF

# cat << EOF | sudo tee -a /etc/init.d/httpd
# if test -f /usr/sbin/envvars; then
# 	. /usr/sbin/envvars
# fi
# EOF


echo "--- Enabling mod-rewrite ---"
sudo a2enmod rewrite

echo "--- Setting document root ---"
sudo rm -rf /var/www/html
sudo ln -fs /vagrant/public /var/www/html


echo "--- What developer codes without errors turned on? Not you, master. ---"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
sed -i '/$aSetEnv APPLICATION_ENV development' /etc/apache2/apache2.conf

echo "--- You like to tinker, don't you master? ---"
sed -i "s/disable_functions = .*/disable_functions = /" /etc/php5/cli/php.ini

# Set APPLICATION_ENV='development'
sudo sed -i "13iSetEnv APPLICATION_ENV development" /etc/apache2/sites-available/000-default.conf
sudo echo "export APPLICATION_ENV='development'" >> /home/vagrant/.bashrc
bash

echo "--- Restarting Apache ---"
sudo service apache2 restart

echo "--- Composer is the future. But you knew that, did you master? Nice job. ---"
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

#echo "--- Bower is very cool. But you also knew that, did you master? Nice job.  ---"
#sudo npm install -g bower

# -------------
# Load Composer packages
cd /vagrant && composer install --dev

# -------------
# Load Bower packages
bower install

# Set up the database
echo "CREATE DATABASE IF NOT EXISTS qterp" | mysql -uroot -proot
echo "GRANT ALL PRIVILEGES ON qterp.* TO 'admin'@'localhost' IDENTIFIED BY '123'" | mysql -uroot -proot

# Laravel stuff here
php artisan --env=development migrate
php artisan --env=development db:seed

echo "--- All set to go! Would you like to play a game? ---"