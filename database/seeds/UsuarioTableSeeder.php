<?php

use Illuminate\Database\Seeder;
use App\User as Usuario; // to use Eloquent Model 
use Faker\Factory as Faker;
 
class UsuarioTableSeeder extends Seeder {
    public function run() {

        $faker = Faker::create('pt_BR');

        Usuario::truncate(); 

        Usuario::create( [
            'nome' => 'Administrador' ,
            'login' => 'admin' ,
            'password' => Hash::make('qterp2015'),
            'email' => 'administrador@qualitecend.com.br',
            'unidade' => 'QT',
            'created_by' => '1',
        ] );

        Usuario::create( [
            'nome' => 'Anderson Magno' ,
            'login' => 'anderson.magno' ,
            'password' => Hash::make('qterp2015'),
            'email' => 'anderson.miranda@applus.com',
            'unidade' => 'QT',
            'created_by' => '1',
        ] );

        Usuario::create( [
            'nome' => 'Manuel Castro' ,
            'login' => 'manuel.castro' ,
            'password' => Hash::make('qterp2015'),
            'email' => 'manuel.castro@applus.com',
            'unidade' => 'QT',
            'created_by' => '1',
        ] );

        foreach(range(1, 147) as $index)
        {
            $name = $faker->firstName;
            $usuariofake = $name.'.'.$faker->lastname;

            Usuario::create([
                'nome' => $name,
                'login' => $usuariofake,
                'password' => Hash::make('qterp2015'),
                'email' => $faker->email,
                'unidade' => $faker->stateAbbr,
                'created_by' => '1',
            ]);
            
        }
    }
}