<?php

use Illuminate\Database\Seeder;
use App\Modulo as Modulo; // to use Eloquent Model 
use App\Areasmodulos as Areasmodulos; // to use Eloquent Model 
use App\Permissoes as Permissoes; // to use Eloquent Model 
use App\Permissaousuario as Permissaousuario; // to use Eloquent Model 

 
class ModuloTableSeeder extends Seeder 
{
    public function run() 
    {

        Modulo::truncate(); 
        Areasmodulos::truncate(); 
        Permissoes::truncate(); 
        Permissaousuario::truncate(); 

        /* modulo adm */

        Modulo::create( [
            'descricao' => 'Admin' ,
            'status' => '1' ,
            'rota' => '/adm',
            'created_by' => '1',
        ] );
        
        Areasmodulos::create( [
            'descricao' => 'Admin',
            'modulo_id' => '1',
            'rota' => '/adm',
            'created_by' => '1',
        ] );
        
        Permissoes::create( [
            'descricao' => 'Admin' ,
            'areamodulo_id' => '1' ,
            'rota' => '/adm',
            'created_by' => '1',
        ] );
        
        Permissaousuario::create( [
            'usuario_id' => '1' ,
            'permissao_id' => '1' ,
            'created_by' => '1',
        ] );

        /* Gamagrafia */
        Modulo::create( [
            'descricao' => 'Gamagrafia' ,
            'status' => '1' ,
            'rota' => '/ensaio',
            'created_by' => '1',
        ] );

        Areasmodulos::create( [
            'descricao' => 'Guia Operacional',
            'modulo_id' => '2',
            'rota' => '/ensaio/guia',
            'created_by' => '1',
        ] );

        Permissoes::create( [
            'descricao' => 'Cadastrar' ,
            'areamodulo_id' => '2' ,
            'rota' => '/guia/cadastrar',
            'created_by' => '1',
        ] );

        Permissaousuario::create( [
            'usuario_id' => '1' ,
            'permissao_id' => '2' ,
            'created_by' => '1',
        ] );


        Areasmodulos::create( [
            'descricao' => 'Proposta',
            'modulo_id' => '2',
            'rota' => '/ensaio/proposta',
            'created_by' => '1',
        ] );

        Permissoes::create( [
            'descricao' => 'Cadastrar' ,
            'areamodulo_id' => '3' ,
            'rota' => '/proposta/cadastrar',
            'created_by' => '1',
        ] );

        Permissaousuario::create( [
            'usuario_id' => '1' ,
            'permissao_id' => '3' ,
            'created_by' => '1',
        ] );

        Areasmodulos::create( [
            'descricao' => 'Boletim de medição',
            'modulo_id' => '2',
            'rota' => '/ensaio/boletim',
            'created_by' => '1',
        ] );

        Permissoes::create( [
            'descricao' => 'Criar BM' ,
            'areamodulo_id' => '4' ,
            'rota' => '/boletim/criar',
            'created_by' => '1',
        ] );

        Permissaousuario::create( [
            'usuario_id' => '1' ,
            'permissao_id' => '4' ,
            'created_by' => '1',
        ] );

        /* patrimonio */
        Modulo::create( [
            'descricao' => 'Patrimonio' ,
            'status' => '1' ,
            'rota' => '/patrimonio',
            'created_by' => '1',
        ] );

        Areasmodulos::create( [
            'descricao' => 'Equipamentos',
            'modulo_id' => '3',
            'rota' => '/equipamento',
            'created_by' => '1',
        ] );

        Permissoes::create( [
            'descricao' => 'Cadastrar Equipamento' ,
            'areamodulo_id' => '5' ,
            'rota' => '/equipamento/cadastrar',
            'created_by' => '1',
        ] );

        Permissoes::create( [
            'descricao' => 'Histórico de Equipamento' ,
            'areamodulo_id' => '5' ,
            'rota' => '/equipamento/historico',
            'created_by' => '1',
        ] );

        Permissaousuario::create( [
            'usuario_id' => '1' ,
            'permissao_id' => '5' ,
            'created_by' => '1',
        ] );

        Permissaousuario::create( [
            'usuario_id' => '1' ,
            'permissao_id' => '6' ,
            'created_by' => '1',
        ] );

    }
}