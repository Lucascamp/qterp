<?php

use Illuminate\Database\Seeder;
use App\Funcionario as Funcionario; // to use Eloquent Model 
use Faker\Factory as Faker;
 
class FuncionarioTableSeeder extends Seeder {

    public function run() {

        $faker = Faker::create('pt_BR');

        Funcionario::truncate(); 

        foreach(range(1, 300) as $index)
        {
            Funcionario::create([
                'nome' => $faker->firstName.' '.$faker->lastname,
                'unidade' => $faker->stateAbbr,
                'centro_custo' => $faker->numberBetween($min = 1000, $max = 10000),
                'matricula_qt' => $faker->numberBetween($min = 1000, $max = 10000),
                'matricula_a5' => $faker->numberBetween($min = 1000, $max = 10000),
                'funcao_qt' => 'operador',
                'funcao_a5' => 'operador',
            ]);
            
        }
    }
}