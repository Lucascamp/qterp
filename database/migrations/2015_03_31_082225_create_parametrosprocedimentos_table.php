<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametrosprocedimentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parametrosprocedimentos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('procedimento_id');
			$table->string('norma',50);
			$table->float('diametro_ini');
			$table->float('diametro_fim');
			$table->float('comprimento_ini');
			$table->float('comprimento_fim');
			$table->float('espessura_ini');
			$table->float('espessura_fim');
			$table->string('tipo_fonte',50);
			$table->float('foco_fonte');
			$table->string('tecnica',50);
			$table->integer('dff');
			$table->string('tipo_iqi',50);
			$table->integer('fio_iqi');
			$table->string('classe_filme',50);
			$table->integer('quantidade_filmes');
			$table->string('dimensao_filme');

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parametrosprocedimentos');
	}

}
