<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFontesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fontes', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->string('numero')->nullable();
			$table->string('isotopo')->nullable();
			$table->string('irradiador')->nullable();
			$table->string('modelo')->nullable();
			$table->string('cabo')->nullable();
			$table->string('foco')->nullable();
			$table->string('gabarito')->nullable();
			

			$table->string('atividade_inicial')->nullable();
			$table->string('meia_vida')->nullable();

			$table->date('data_inicial')->nullable();

			$table->string('cod_vistoria')->nullable();
			$table->date('data_vistoria')->nullable();

			$table->string('pasta')->nullable();

			$table->string('limite_uso')->nullable();
			$table->date('data_limite_uso')->nullable();

			$table->string('limite_troca')->nullable();
			$table->date('data_limite_troca')->nullable();
			
			$table->string('status')->nullable();
			$table->string('observacoes')->nullable();

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fontes');
	}

}
