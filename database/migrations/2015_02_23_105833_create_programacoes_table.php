<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programacoes', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->integer('guia_id')->unsigned();
			$table->string('unidade', 3);

			$table->integer('item');
			$table->integer('isometrico');
			$table->string('identificacao', 50);
			$table->string('aplicacao', 50);
			$table->string('spool', 50);
			$table->string('junta', 50);
			$table->string('obra', 50);

			$table->string('posicao_soldagem', 10);
			$table->string('diametro', 10);
			$table->string('espessura', 15);
			$table->string('reforco', 50);

			$table->string('metal_base', 50);
			$table->string('metal_adicao', 50);

			$table->string('sinete_raiz', 50);
			$table->string('processo_raiz', 50);

			$table->string('sinete_acabamento', 50);
			$table->string('processo_acabamento', 50);

			$table->string('norma', 20);
			$table->string('nivel_inspecao', 10);
			$table->string('procedimento', 20);
			$table->string('tecnica', 20);
			$table->string('tamanho_descontinuidade');
			
			$table->time('tempo_exposicao');
			$table->string('tipo_exposicao',20);

			$table->string('dff', 10);
			
			$table->string('tipo_iqi', 10);
			$table->string('fio_iqi', 10);
			$table->string('chanfro', 10);

			$table->string('filme_dimensao', 10);
			$table->string('filme_classe', 10);
			$table->integer('quantidade_filmes');
			$table->integer('filmes_batidos');
			$table->integer('filmes_perdidos');

			$table->string('observacao');
			$table->string('executado');
			$table->string('quantidade_aprovado');
			$table->string('quantidade_reprovado');
			$table->string('descontinuidade');
			$table->string('cod_inspecao');
			$table->string('resultado');
			$table->string('criterio_aceitacao',45);
			$table->integer('tipo');
			$table->integer('numero_filme');

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programacoes');
	}

}
