<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostaJornadaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposta_jornada', function(Blueprint $table)
		{
			$table->increments('id');
        	$table->integer('proposta_id')->unsigned()->index();
        	$table->integer('jornada_id')->unsigned()->index();
        	$table->decimal('valor_normal', 10, 2);
			$table->decimal('valor_extra', 10, 2);
			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proposta_jornada');
	}

}
