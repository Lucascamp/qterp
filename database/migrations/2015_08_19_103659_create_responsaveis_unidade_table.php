<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsaveisUnidadeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('responsaveis_unidade', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			$table->integer('centrocusto_id');
			$table->string('responsaveis');
			
			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('responsaveis_unidade');
	}

}
