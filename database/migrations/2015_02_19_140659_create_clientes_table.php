<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->string('codigo');
			$table->string('cnpj');
			$table->string('nome');
			$table->string('nome_reduz');
			$table->string('endereco');
			$table->string('estado', 3);
			$table->string('municipio');
			$table->string('bairro');
			$table->string('cep', 8);
			$table->string('ddd', 2);
			$table->string('tel', 9);
			$table->string('contato');

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}

}
