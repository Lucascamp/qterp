<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatoriosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('relatorios', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->integer('guia_id');
			$table->date('data');
			$table->string('numero_relatorio')->nullable();
			$table->string('numero_rel_cliente')->nullable();
			$table->string('norma')->nullable();
			$table->string('criterio')->nullable();
			$table->string('unidade')->nullable();
			$table->integer('versao');
			$table->string('itens',50)->nullable();
			$table->string('solicitacao_cliente',50)->nullable();
			$table->string('contratante_fabricante',50)->nullable();
			$table->string('projeto',50)->nullable();
			$table->string('local',50)->nullable();

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('relatorios');
	}

}
