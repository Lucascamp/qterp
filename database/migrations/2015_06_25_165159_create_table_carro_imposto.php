<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCarroImposto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carro_imposto', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			$table->integer('carro_id');
			$table->string('tipo_imposto');
			$table->float('valor_imposto');
			$table->date('data_pagamento_imposto');
			$table->string('restricao')->nullable();
			$table->string('tipo_restricao')->nullable();

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carro_imposto');
	}

}
