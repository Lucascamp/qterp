<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('layouts', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			$table->integer('cliente_id');
			$table->string('programacao', 50);
			$table->string('obra', 50);
			$table->string('local_ensaio', 50);
			$table->string('data', 50);
			$table->string('contratante_fabricante', 50);
			$table->string('item', 50);
			$table->string('iden_dese_iso', 50);
			$table->string('mapa_spool', 50);
			$table->string('prog_fabri', 50);
			$table->string('junta', 50);
			$table->string('posicao_soldagem', 50);
			$table->string('metal_base', 50);
			$table->string('diametro', 50);
			$table->string('espessura', 50);
			$table->string('reforco', 50);
			$table->string('espessura_menor', 50);
			$table->string('espessura_maior', 50);
			$table->string('sold_sinete_raiz', 50);
			$table->string('processo_raiz', 50);
			$table->string('sold_sinete_acab', 50);
			$table->string('processo_acab', 50);
			$table->string('metal_adicao', 50);
			$table->string('chanfro', 50);
			$table->string('nivel_inspecao', 50);
			$table->string('norma', 50);
			$table->string('observacao', 50);
			$table->string('inicio_descricao', 50);
			$table->string('criterio_aceite', 50);
	
			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('layouts');
	}

}
