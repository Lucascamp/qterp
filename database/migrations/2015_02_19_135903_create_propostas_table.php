<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('propostas', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			// foreign keys
			$table->integer('cliente_id')->unsigned()->index();
			
			$table->date('data');
			$table->integer('validade');
			$table->string('cod_proposta');
			$table->string('cod_proposta_anterior');
			$table->decimal('filmedereparo', 10, 2);
			// $table->decimal('valorchapa', 10, 2);

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('propostas');
	}

}
