<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBmTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bm', function(Blueprint $table)
		{
			$table->increments('id');
			$table->longtext('guia_id');
			$table->timestamp('periodo_ini');
			$table->timestamp('periodo_fim');
			$table->integer('cliente_id');
			$table->integer('proposta_id');
			$table->float('valor_total');

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bm');
	}

}
