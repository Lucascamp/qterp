<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostaServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposta_servico', function(Blueprint $table)
		{
			$table->increments('id');
        	$table->integer('proposta_id')->unsigned()->index();
        	$table->integer('servico_id')->unsigned()->index();
        	$table->decimal('valor', 10, 2);
        	// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proposta_servico');
	}

}
