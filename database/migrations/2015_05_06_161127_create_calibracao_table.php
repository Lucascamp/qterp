<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalibracaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calibracao', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->integer('equipamento_id');
			$table->string('laboratorio')->nullable();
			$table->string('certificado')->nullable();
			$table->date('data_calibracao')->nullable();
			$table->string('parecer')->nullable();
			$table->string('comentario')->nullable();
			$table->string('documentacao')->nullable();
			$table->date('primeiro_uso')->nullable();
			$table->date('vencimento')->nullable();
			$table->string('filepath')->nullable();

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calibracao');
	}

}
