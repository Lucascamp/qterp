<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasmodulosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('areasmodulos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('descricao');
			$table->integer('modulo_id')->unsigned();
			$table->string('rota');
			// $table->foreign('modulo_id')->references('id')->on('modulos')->onDelete('cascade');
			
			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('areasmodulos');
	}

}
