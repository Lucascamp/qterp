<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNormasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('normas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('procedimento_id');
			$table->string('norma',50);
			$table->string('criterio_aceite',150);
			$table->string('objeto',50);
			$table->string('aplicacao',50);
			$table->integer('grupo_material_base');

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('normas');
	}

}
