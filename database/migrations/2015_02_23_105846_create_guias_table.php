<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guias', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->date('data');
			$table->string('unidade', 3);
			
			$table->integer('proposta_id')->unsigned();
			$table->integer('cliente_id')->unsigned();
			$table->integer('fonte_id')->unsigned();
			$table->string('fonte_atividade',50);
			$table->timestamp('entrada')->nullable();
			$table->timestamp('saida')->nullable();
			$table->time('horas_contratadas')->nullable();
			$table->string('placa',50);
			$table->string('km_inicial',50);
			$table->string('km_final',50);
			$table->longtext('ocorrencias');
			$table->longtext('relatorios');
			$table->string('protocolos',100);
			$table->string('quantidade_filmes_cassete',45);
			$table->string('ecran_anterior',45);
			$table->string('ecran_posterior',45);
			$table->string('temperatura',45);
			$table->string('condicao_superficial',45);
			$table->string('temperatura_revelacao',45);
			$table->string('tempo_revelacao',45);

			$table->string('programacao_cliente',45);	
			$table->string('programacao_fabricante',45);
			$table->string('local_ensaio',45);
			$table->string('data_prog',45);
			$table->string('contratante_fabricante',45);
			$table->string('projeto',100);

			$table->longtext('observacoes');
			$table->integer('status')->unsigned();

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guias');
	}

}
