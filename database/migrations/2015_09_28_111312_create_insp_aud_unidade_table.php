<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspAudUnidadeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('insp_auds_unidade', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			$table->integer('insp_aud_id')->nullable();
			$table->string('projeto',150);
			$table->string('local',150);
			$table->string('responsavel',150);
			$table->string('equipe',150);
			$table->string('verificado_por',150);
			$table->timestamp('data')->nullable();
			$table->string('doc_referencia',150);
			$table->string('unidade',150);

			
			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('insp_auds_unidade');
	}

}
