<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCarro extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carro', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			$table->string('placa');
			$table->string('marca');
			$table->string('modelo');
			$table->string('combustivel');
			$table->string('ano');
			$table->string('cor');
			$table->string('chassi');
			$table->integer('renavan');
			$table->integer('km');
			$table->boolean('rastreador')->nullable();
			$table->string('responsavel')->nullable();
			$table->string('proprietario')->nullable();
			//comprado
			$table->date('data_compra')->nullable();
			$table->float('valor_compra')->nullable();
			//alugado
			$table->boolean('alugado')->nullable();
			$table->date('data_aluguel')->nullable();
			$table->integer('prazo_contrato')->nullable();
			$table->date('termino_contrato')->nullable();
			$table->float('valor_locacao')->nullable();
			$table->float('multa_recisao')->nullable();
			$table->date('data_reajuste')->nullable();
			$table->string('tipo_reajuste')->nullable();
			$table->integer('mes_reajuste')->nullable();
			$table->integer('ano_reajuste')->nullable();

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carro');
	}

}
