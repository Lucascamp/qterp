<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projetos', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			// foreign keys
			$table->integer('proposta_id')->unsigned()->index();

			$table->string('servico');
			$table->string('estado', 3);
			$table->string('municipio');
			$table->string('solicitacao_contato');
			$table->string('solicitacao_email');
			$table->string('solicitacao_tel');
			$table->string('aprovacao_contato');
			$table->string('aprovacao_email');
			$table->string('aprovacao_tel');
			$table->string('projeto_contato');
			$table->string('projeto_email');
			$table->string('projeto_tel');

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projetos');
	}

}
