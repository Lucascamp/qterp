<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equipamentos', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->string('descricao');
			$table->string('aplicacao');
			$table->string('marca');
			$table->string('modelo');
			$table->string('serial');
			$table->string('patrimonio');
			$table->string('descricao_uso');
			$table->date('data_compra');
			$table->string('nota_fiscal');
			$table->string('valor_compra');
			$table->string('fornecedor');
			$table->string('taxa_depreciacao');
			$table->string('calibravel');
			$table->integer('frequencia');
			$table->string('proprietario');
			$table->string('regime');
			$table->string('filepath');

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('equipamentos');
	}

}
