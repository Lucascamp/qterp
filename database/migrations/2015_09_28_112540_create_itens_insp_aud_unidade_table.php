<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspAudUnidadeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_insp_auds_unidade', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			$table->integer('insp_aud_id')->nullable();
			$table->integer('item_numero')->nullable();
			$table->string('na');
			$table->string('nc');
			$table->string('c');
			$table->string('nota',150);
			$table->timestamp('data')->nullable();
						
			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_insp_auds_unidade');
	}

}
