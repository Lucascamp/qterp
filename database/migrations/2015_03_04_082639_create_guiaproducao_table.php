<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiaproducaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guiaproducao', function(Blueprint $table)
		{
			$table->increments('id');
        	$table->integer('guia_id')->unsigned()->index();
        	$table->integer('junta_id')->unsigned()->index();
        	$table->integer('quantidade')->default(0);
        	$table->string('unidade');

        	$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guiaproducao');
	}

}
