<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFontesLocalizacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fontes_localizacao', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			// foreign keys
			$table->integer('fonte_id')->unsigned()->index();

			//*fields
			$table->string('unidade', 20)->nullable();

			$table->string('data')->nullable();

			$table->string('observacoes')->nullable();

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();
			
			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//	
	}

}
