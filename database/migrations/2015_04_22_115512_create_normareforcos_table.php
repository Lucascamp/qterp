<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNormareforcosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('normareforcos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('norma');
			$table->integer('procedimento_id');
			$table->float('espessura_ini');
			$table->float('espessura_fim');
			$table->float('reforco_raiz');
			$table->float('reforco_acab');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('normareforcos');
	}

}
