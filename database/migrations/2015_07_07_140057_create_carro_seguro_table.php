<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarroSeguroTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carro_seguro', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');
			$table->integer('carro_id');
			$table->string('seguradora');
			$table->string('corretor');
			$table->string('apolice');
			$table->float('valor_seguro');
			$table->date('data_inicial');
			$table->date('data_final');
			$table->string('vistoria');

			//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carro_seguro');
	}

}
