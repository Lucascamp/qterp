<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipamentoLocalizacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equipamento_localizacao', function(Blueprint $table)
		{
			$table->increments('id');
        	$table->integer('equipamento_id')->unsigned()->index();
        	$table->integer('centrocusto_id')->unsigned()->index();
        	$table->string('projeto');
        	$table->integer('funcionario_responsavel')->nullable();
        	$table->integer('usuario_receptor')->nullable();
        	$table->boolean('recebido');
        	$table->string('motivo');
        	$table->date('data_movimentacao');

        	//usuarios que efetuaram as operações
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('equipamento_localizacao');
	}

}
